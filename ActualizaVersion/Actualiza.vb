﻿Public Class Actualiza
    Dim thread As Threading.Thread
    Dim iI As Integer
    Dim iMax As Integer
    Dim Mensahe As String = ""

    Private Sub Actualiza_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim RutaOrigen As String = ""
        Dim ArchivosACopiar() As String

        RutaOrigen = Environment.GetCommandLineArgs(1)
        ArchivosACopiar = IO.Directory.GetFiles(RutaOrigen)

        ProgressBar1.Minimum = 0
        ProgressBar1.Maximum = ArchivosACopiar.Length
        ProgressBar1.Value = 0

        thread = New Threading.Thread(AddressOf Actualiza)
        thread.Start()

    End Sub

    Private Sub Actualiza()
        Dim RutaOrigen As String = ""
        Dim RutaDestino As String = ""
        Dim RutaExe As String = ""
        Dim ArchivosACopiar() As String
        Dim startInfo As New ProcessStartInfo
        Dim Sw_Copiar As Boolean
        Try
            RutaOrigen = Environment.GetCommandLineArgs(1)
            RutaDestino = Environment.GetCommandLineArgs(2)
            RutaExe = Environment.GetCommandLineArgs(3)
            ArchivosACopiar = IO.Directory.GetFiles(RutaOrigen, "*", IO.SearchOption.AllDirectories)
            iI = 0
            For Each sArchivo In ArchivosACopiar
                Sw_Copiar = True
                If Not sArchivo.Contains("inicio") Then
                    If IO.File.Exists(sArchivo) And sArchivo.Contains("Data.Desktop") Then
                        Sw_Copiar = False
                    End If
                    If Sw_Copiar Then
                        Try
                            IO.File.Delete(sArchivo.Replace(RutaOrigen, RutaDestino))
                            IO.File.Copy(sArchivo, sArchivo.Replace(RutaOrigen, RutaDestino), True)
                        Catch ex As Exception

                        End Try
                    End If
                    iI = iI + 1
                End If
            Next
            Shell(RutaExe)

            Me.Close()
        Catch ex As Exception
            Mensahe = RutaOrigen & vbNewLine & RutaDestino & vbNewLine & RutaExe & vbNewLine & ex.Message
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            ProgressBar1.Value = iI
        Catch ex As Exception

        End Try
        If Mensahe <> "" Then
            Label4.Text = Mensahe
        End If
    End Sub

End Class
