﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerRegister
{
     Public Class clsConexion
        Private strServer As String = ""
        Private strDb As String = ""
        Private strUser As String = ""
        Private strPass As String = ""
        Public Function cnx_Seguridad(ByVal Server As String, ByVal DB As String, ByVal user As String, ByVal pass As String) As String
            Dim cnx As String
            strServer = Server
            strDb = DB
            strUser = user
            strPass = pass
            cnx = "Data Source =" & strServer.Trim & ";" _
                        & "Initial Catalog=" & strDb.Trim & ";" _
                        & "Persist Security Info=False;" _
                        & "User ID=" & strUser.Trim & ";" _
                        & "Password=" & strPass.Trim & ";" _
                        & "Packet Size = 4096; Max Pool Size=200;" _
                        & "Connect Timeout=45"
            Return cnx
        End Function
        Private Function cnx_Seguridad() As String
            Dim cnx As String
            cnx = "Data Source =" & strServer.Trim & ";" _
                        & "Initial Catalog=" & strDb.Trim & ";" _
                        & "Persist Security Info=False;" _
                        & "User ID=" & strUser.Trim & ";" _
                        & "Password=" & strPass.Trim & ";" _
                        & "Packet Size = 4096; Max Pool Size=200;" _
                        & "Connect Timeout=45"
            Return cnx
        End Function
        Public ReadOnly Property ObtenerConexion() As SqlConnection
            Get
                Dim cnx As SqlConnection
                cnx = New SqlConnection(cnx_Seguridad)
                Return cnx
            End Get
        End Property
    End Class
}
