﻿using DevExpress.XtraBars;
using DevExpress.XtraGrid;
using DPFP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

using DevExpress.Office.Crypto;
using DevExpress.XtraPrinting.Shape.Native;
using DevExpress.XtraSplashScreen;
using DevExpress.Utils.Html.Internal;

namespace FingerRegister
{
    public partial class Form1 : DevExpress.XtraBars.TabForm
    {
        private string connectionString = "Server=#SERVER#;Database=#BASEDATOS#;User Id=#USER#;Password=#PASSWORD#";
        private DPFP.Template Template;
        public string tipohuella = "";
        public string id_huella = "";
        
        public Form1()
        {
            InitializeComponent();

            var xmlDoc = new System.Xml.XmlDocument();
            string XmlRuta = "inicio.ini";
            string sXml = "";
            bool SW_ArchivoINI;

            try
            {
                XmlRuta = Application.StartupPath + "\\" + XmlRuta;
                richTextBox1.LoadFile(XmlRuta);
                sXml = CryptoZ.Decrypt(richTextBox1.Text, "ICC");
                richTextBox1.LoadFile(XmlRuta);
                xmlDoc.LoadXml(sXml);
                SW_ArchivoINI = true;

            }
            catch (Exception e) {
                SW_ArchivoINI = false;
                MessageBox.Show("Error al leer archivo INI. " + e.Message);
                return;
            }
            if (SW_ArchivoINI) {
                connectionString = connectionString.Replace("#SERVER#", xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/server")[0].InnerText);
                connectionString = connectionString.Replace("#BASEDATOS#", xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/basedatos")[0].InnerText);
                connectionString = connectionString.Replace("#USER#", xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/usuario")[0].InnerText);
                connectionString = connectionString.Replace("#PASSWORD#", xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/contrasena")[0].InnerText);
            }

            
        }

        /*private void Form1_Load(object sender, EventArgs e)
        {
            BindDataToGridControl(gridControl1);
        }*/

        public void ReloadData()
        {
            // Lógica para recargar los datos en el formulario principal
            int companyId = 1; // Establece el ID de la empresa según sea necesario
            BindDataToGridControl(gridControl1, Txt_IdEmpresa.Text);
        }

        private void BindDataToGridControl(GridControl gridControl, string empresa)
        {
            DataTable dataTable = GetDataFromStoredProcedure(empresa);
            gridControl.DataSource = dataTable;

            gridView1.ClearSelection();
            gridView1.FocusedRowHandle = GridControl.InvalidRowHandle;
        }
        private string GetPametros()
        {
            string emp = "";
            //string connectionString = "Server=23.98.159.115,8069;Database=VOFICINA_OPE;User Id=UsrSoporteICC;Password=D43q58fq8ht#24;";
            DataTable dataTable = new DataTable();
            int fieldCount = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("select id_empresa,id_sucursal from parametros", connection);
                command.CommandType = CommandType.Text;
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (!reader.HasRows)
                    {
                        // throw exception
                    }
                    while (reader.Read())
                    {
                        emp = reader.GetString(0);
                    }
                }
            }

            return emp;
        }

        private DataTable GetDataFromStoredProcedure(string empresa)
        {
            //string connectionString = "Server=23.98.159.115,8069;Database=VOFICINA_OPE;User Id=UsrSoporteICC;Password=D43q58fq8ht#24;";
            DataTable dataTable = new DataTable();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("[dbo].[sp_Recupera_Empleados_Huella]", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id_empresa", empresa);
                command.Parameters.AddWithValue("@tipo", List_Box_Tipo.CheckedItems[0].ToString());
                command.Parameters.AddWithValue("@id_empleado", txt_IdHuella.Text);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(dataTable);
            }

            return dataTable;
        }

        void OnOuterFormCreating(object sender, OuterFormCreatingEventArgs e)
        {
            Form1 form = new Form1();
            form.TabFormControl.Pages.Clear();
            e.Form = form;
            OpenFormCount++;
        }
        static int OpenFormCount = 1;

        private void tabFormContentContainer1_Click(object sender, EventArgs e)
        {

        }

        private void tabFormControl1_Click(object sender, EventArgs e)
        {

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            bool isRowSelected = e.FocusedRowHandle >= 0;
            ButtonFingerprintRegister.Enabled = isRowSelected;
            ButtonFingerprintRegister.Visible = isRowSelected;
        }

        private void ButtonFingerprintRegister_Click(object sender, EventArgs e)
        {
            // Obtén los datos del registro seleccionado
            string empresa = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_empresa");
            string empleado = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_empleado");
            string nombreEmpleado = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "nombre_completo");

            CaptureFingerprintForm capture = new CaptureFingerprintForm(this, empresa, empleado, nombreEmpleado);
            if (List_Box_Tipo.CheckedItems[0].ToString().ToLower() == "empleados"){
                capture.SW_imagen = true;
            }
            else {
                capture.SW_imagen = false;
            }
            capture.OnTemplate += this.OnTemplate;
            capture.ShowDialog();
        }

        private void OnTemplate(DPFP.Template template)
        {
            this.Invoke(new Function(delegate ()
            {
                Template = template;
                //btnAgregar.Enabled = (Template != null);
                if (Template != null)
                {
                    MessageBox.Show("La huella dactilar ha sido verificada correctamente, puede asignarla al usuario", "Registro de huellas dactilares");
                    //txtHuella.Text = "Huella capturada correctamente";
                }
                else
                {
                    MessageBox.Show("La huella dactilar no es válida, favor de repetir el proceso.", "Registro de huellas dactilares");
                }
            }));
        }

        public void SaveHuella(string empresa, string empleado, string profileImage)
        {
            try
            {
                byte[] streamHuella = Template.Bytes;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand("[dbo].[sp_Upsert_Empleados_Huella]", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@id_empresa", empresa);
                    command.Parameters.AddWithValue("@id_empleado", empleado);
                    command.Parameters.AddWithValue("@huella", streamHuella);
                    command.Parameters.AddWithValue("@tipo", List_Box_Tipo.CheckedItems[0].ToString());

                    SqlParameter imageParam = new SqlParameter("@profileImage", SqlDbType.VarChar);
                    imageParam.Value = profileImage ?? (object)DBNull.Value;
                    imageParam.Size = -1; // Indica que el tamaño es ilimitado
                    command.Parameters.Add(imageParam);

                    //command.Parameters.AddWithValue("@profileImage", profileImage);
                    command.Parameters.AddWithValue("@id_usuario", "ICC");

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }

                MessageBox.Show("Se han registrado los datos de forma correcta");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void List_Box_Tipo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            Txt_IdEmpresa.Text = GetPametros();
            if (id_huella != null)
            {
                txt_IdHuella.Text = id_huella;
            }
            if (tipohuella != null)
            {
                int iI;
                for (iI = 0; iI < List_Box_Tipo.Items.Count - 1; iI++)
                {
                    if (List_Box_Tipo.Items[iI].ToString().ToLower() == tipohuella.ToLower()){
                        List_Box_Tipo.SetItemChecked(iI, true);
                    }else{
                        List_Box_Tipo.SetItemChecked(iI, false);
                    }
                }
            }

            BindDataToGridControl(gridControl1, Txt_IdEmpresa.Text);

            gridView1.ClearSelection();
            gridView1.FocusedRowHandle = GridControl.InvalidRowHandle;

            ButtonFingerprintRegister.Enabled = true;
            ButtonFingerprintRegister.Visible = true;

            if (txt_IdHuella.Text.Length > 0)
            {
                ButtonFingerprintRegister_Click(null, null);
            }
        }
        
        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            BindDataToGridControl(gridControl1, Txt_IdEmpresa.Text);

            gridView1.ClearSelection();
            gridView1.FocusedRowHandle = GridControl.InvalidRowHandle;
        }
    }

    public class CryptoZ
    {
        private static TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();
        private static MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();

        public static byte[] MD5Hash(string value)
        {
            return MD5.ComputeHash(Encoding.ASCII.GetBytes(value));
        }

        public static string Encrypt(string stringToEncrypt, string key)
        {
            DES.Key = MD5Hash(key);
            DES.Mode = CipherMode.ECB;
            byte[] buffer = Encoding.ASCII.GetBytes(stringToEncrypt);
            return Convert.ToBase64String(DES.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length));
        }

        public static string Decrypt(string encryptedString, string key)
        {
            try
            {
                DES.Key = MD5Hash(key);
                DES.Mode = CipherMode.ECB;
                byte[] buffer = Convert.FromBase64String(encryptedString);
                return Encoding.ASCII.GetString(DES.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length));
            }
            catch
            {
                MessageBox.Show("Wrong Key Number, decryption not available!");
                return null;
            }
        }
    }

}
