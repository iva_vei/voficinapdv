namespace FingerRegister
{
    partial class CaptureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PromptLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.Picture = new System.Windows.Forms.PictureBox();
            this.Prompt = new System.Windows.Forms.TextBox();
            this.StatusText = new System.Windows.Forms.TextBox();
            this.StatusLine = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.nombreEmpleadoLbl = new System.Windows.Forms.Label();
            this.empleadoLbl = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.profileImagePicture = new System.Windows.Forms.PictureBox();
            this.ofdSeleccionar = new System.Windows.Forms.OpenFileDialog();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileImagePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // PromptLabel
            // 
            this.PromptLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PromptLabel.AutoSize = true;
            this.PromptLabel.Location = new System.Drawing.Point(406, 103);
            this.PromptLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PromptLabel.Name = "PromptLabel";
            this.PromptLabel.Size = new System.Drawing.Size(108, 20);
            this.PromptLabel.TabIndex = 1;
            this.PromptLabel.Text = "Instrucciones:";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(406, 177);
            this.StatusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(60, 20);
            this.StatusLabel.TabIndex = 3;
            this.StatusLabel.Text = "Status:";
            // 
            // Picture
            // 
            this.Picture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Picture.BackColor = System.Drawing.SystemColors.Window;
            this.Picture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Picture.Location = new System.Drawing.Point(25, 103);
            this.Picture.Margin = new System.Windows.Forms.Padding(0);
            this.Picture.Name = "Picture";
            this.Picture.Size = new System.Drawing.Size(370, 522);
            this.Picture.TabIndex = 0;
            this.Picture.TabStop = false;
            // 
            // Prompt
            // 
            this.Prompt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Prompt.Cursor = System.Windows.Forms.Cursors.No;
            this.Prompt.Location = new System.Drawing.Point(411, 128);
            this.Prompt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Prompt.Name = "Prompt";
            this.Prompt.ReadOnly = true;
            this.Prompt.Size = new System.Drawing.Size(448, 26);
            this.Prompt.TabIndex = 2;
            // 
            // StatusText
            // 
            this.StatusText.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.StatusText.BackColor = System.Drawing.SystemColors.Window;
            this.StatusText.Location = new System.Drawing.Point(411, 202);
            this.StatusText.Margin = new System.Windows.Forms.Padding(0);
            this.StatusText.Multiline = true;
            this.StatusText.Name = "StatusText";
            this.StatusText.ReadOnly = true;
            this.StatusText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.StatusText.Size = new System.Drawing.Size(448, 424);
            this.StatusText.TabIndex = 4;
            // 
            // StatusLine
            // 
            this.StatusLine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.StatusLine.Location = new System.Drawing.Point(13, 693);
            this.StatusLine.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StatusLine.Name = "StatusLine";
            this.StatusLine.Size = new System.Drawing.Size(561, 60);
            this.StatusLine.TabIndex = 5;
            this.StatusLine.Text = "[Status line]";
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(1112, 693);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(112, 35);
            this.CloseButton.TabIndex = 6;
            this.CloseButton.Text = "Cancelar";
            this.CloseButton.UseVisualStyleBackColor = true;
            // 
            // nombreEmpleadoLbl
            // 
            this.nombreEmpleadoLbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nombreEmpleadoLbl.Location = new System.Drawing.Point(143, 39);
            this.nombreEmpleadoLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.nombreEmpleadoLbl.Name = "nombreEmpleadoLbl";
            this.nombreEmpleadoLbl.Size = new System.Drawing.Size(561, 37);
            this.nombreEmpleadoLbl.TabIndex = 7;
            this.nombreEmpleadoLbl.Text = "[Nombre]";
            // 
            // empleadoLbl
            // 
            this.empleadoLbl.AllowDrop = true;
            this.empleadoLbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.empleadoLbl.Location = new System.Drawing.Point(25, 40);
            this.empleadoLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.empleadoLbl.Name = "empleadoLbl";
            this.empleadoLbl.Size = new System.Drawing.Size(110, 35);
            this.empleadoLbl.TabIndex = 8;
            this.empleadoLbl.Text = "[Empleado]";
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SaveButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SaveButton.Enabled = false;
            this.SaveButton.Location = new System.Drawing.Point(991, 693);
            this.SaveButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(112, 35);
            this.SaveButton.TabIndex = 9;
            this.SaveButton.Text = "Guardar";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // profileImagePicture
            // 
            this.profileImagePicture.BackColor = System.Drawing.SystemColors.Window;
            this.profileImagePicture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.profileImagePicture.Location = new System.Drawing.Point(883, 202);
            this.profileImagePicture.Name = "profileImagePicture";
            this.profileImagePicture.Size = new System.Drawing.Size(344, 424);
            this.profileImagePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.profileImagePicture.TabIndex = 10;
            this.profileImagePicture.TabStop = false;
            // 
            // ofdSeleccionar
            // 
            this.ofdSeleccionar.FileName = "openFileDialog1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(1002, 128);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(9, 9, 9, 9);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(225, 26);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "Seleccionar imagen";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // CaptureForm
            // 
            this.AcceptButton = this.CloseButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(1252, 774);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.profileImagePicture);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.empleadoLbl);
            this.Controls.Add(this.nombreEmpleadoLbl);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.StatusLine);
            this.Controls.Add(this.StatusText);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.Prompt);
            this.Controls.Add(this.PromptLabel);
            this.Controls.Add(this.Picture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(589, 431);
            this.Name = "CaptureForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Capture Enrollment";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CaptureForm_FormClosed);
            this.Load += new System.EventHandler(this.CaptureForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileImagePicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture;
        private System.Windows.Forms.TextBox Prompt;
        private System.Windows.Forms.TextBox StatusText;
        private System.Windows.Forms.Label StatusLine;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label PromptLabel;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label nombreEmpleadoLbl;
        private System.Windows.Forms.Label empleadoLbl;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.PictureBox profileImagePicture;
        private System.Windows.Forms.OpenFileDialog ofdSeleccionar;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}