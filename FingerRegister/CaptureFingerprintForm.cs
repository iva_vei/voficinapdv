﻿using DevExpress.XtraEditors;
using DPFP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FingerRegister
{
    public partial class CaptureFingerprintForm : CaptureForm
    {
        public delegate void OnTemplateEventHandler(DPFP.Template template);

        public event OnTemplateEventHandler OnTemplate;

        private DPFP.Processing.Enrollment Enroller;

        private string empresa;
        private string empleado;
        private string nombreEmpleado;

        private Form1 mainForm;

        protected override void Init()
        {
            base.Init();
            base.Text = "Dar de alta Huella";
            Enroller = new DPFP.Processing.Enrollment();            // Create an enrollment.
            UpdateStatus();
            IsReadyForSave();
            UpdateDatosEmpleado();
        }

        protected override void Process(DPFP.Sample Sample)
        {
            base.Process(Sample);

            // Process the sample and create a feature set for the enrollment purpose.
            DPFP.FeatureSet features = ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Enrollment);

            // Check quality of the sample and add to enroller if it's good
            if (features != null) try
                {
                    MakeReport("The fingerprint feature set was created1.");
                    Enroller.AddFeatures(features);     // Add feature set to template.
                }
                finally
                {
                    UpdateStatus();
                    IsReadyForSave();

                    // Check if template has been created.
                    switch (Enroller.TemplateStatus)
                    {
                        case DPFP.Processing.Enrollment.Status.Ready:   // report success and stop capturing
                            OnTemplate(Enroller.Template);
                            SetPrompt("Click Close, and then click Fingerprint Verification.");
                            Stop();
                            break;

                        case DPFP.Processing.Enrollment.Status.Failed:  // report failure and restart capturing
                            Enroller.Clear();
                            Stop();
                            UpdateStatus();
                            IsReadyForSave();
                            OnTemplate(null);
                            Start();
                            break;
                    }
                }
        }

        private void UpdateStatus()
        {
            // Show number of samples needed.
            SetStatus(String.Format("Se necesitan {0} muestras de huellas dactilares", Enroller.FeaturesNeeded));
        }

        private void IsReadyForSave()
        {
            // Show number of samples needed.
            if (Enroller.FeaturesNeeded != 0)
            {
                SetIsReadyForSave(false);
            }
            else
            {
                SetIsReadyForSave(true);
            }
           
        }

        public CaptureFingerprintForm(Form1 form, string empresa, string empleado, string nombreEmpleado) : base(form)
        {
            InitializeComponent();
            mainForm = form;
            this.empresa = empresa;
            this.empleado = empleado;
            this.nombreEmpleado = nombreEmpleado;
        }

        private void UpdateDatosEmpleado()
        {
            // Show number of samples needed.
            SetDatosEmpleado(this.empresa, this.empleado, this.nombreEmpleado);
        }

        private void CaptureFingerprintForm_Load(object sender, EventArgs e)
        {

        }
    }
}