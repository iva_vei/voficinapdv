﻿using DevExpress.Skins;
using DevExpress.UserSkins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FingerRegister
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] parametros)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var Principal = new Form1();
            if (parametros != null) {
                if (parametros.Length > 0){
                    Principal.tipohuella = parametros[0];
                }
                if (parametros.Length > 1){
                    Principal.id_huella = parametros[1];
                }
            }

            Principal.Show();
            Application.Run();
        }
    }
}
