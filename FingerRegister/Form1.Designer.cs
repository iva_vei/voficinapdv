﻿using DevExpress.XtraGrid.Views.Base;

namespace FingerRegister
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            bool isDesignMode = DesignMode;
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            if (--OpenFormCount == 0 && !isDesignMode)
            {
                System.Windows.Forms.Application.Exit();
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabFormControl1 = new DevExpress.XtraBars.TabFormControl();
            this.tabFormDefaultManager1 = new DevExpress.XtraBars.TabFormDefaultManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.registerTab = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer1 = new DevExpress.XtraBars.TabFormContentContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.id_empresa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.id_sucursal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.id_empleado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.nombre_completo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.has_fingerprint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.has_image = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_IdHuella = new System.Windows.Forms.TextBox();
            this.Txt_IdEmpresa = new System.Windows.Forms.TextBox();
            this.List_Box_Tipo = new System.Windows.Forms.CheckedListBox();
            this.ButtonFingerprintRegister = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabFormDefaultManager1)).BeginInit();
            this.tabFormContentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabFormControl1
            // 
            this.tabFormControl1.Location = new System.Drawing.Point(0, 0);
            this.tabFormControl1.Manager = this.tabFormDefaultManager1;
            this.tabFormControl1.Name = "tabFormControl1";
            this.tabFormControl1.Pages.Add(this.registerTab);
            this.tabFormControl1.SelectedPage = this.registerTab;
            this.tabFormControl1.Size = new System.Drawing.Size(1048, 88);
            this.tabFormControl1.TabForm = this;
            this.tabFormControl1.TabIndex = 0;
            this.tabFormControl1.TabStop = false;
            this.tabFormControl1.OuterFormCreating += new DevExpress.XtraBars.OuterFormCreatingEventHandler(this.OnOuterFormCreating);
            this.tabFormControl1.Click += new System.EventHandler(this.tabFormControl1_Click);
            // 
            // tabFormDefaultManager1
            // 
            this.tabFormDefaultManager1.DockControls.Add(this.barDockControlTop);
            this.tabFormDefaultManager1.DockControls.Add(this.barDockControlBottom);
            this.tabFormDefaultManager1.DockControls.Add(this.barDockControlLeft);
            this.tabFormDefaultManager1.DockControls.Add(this.barDockControlRight);
            this.tabFormDefaultManager1.DockingEnabled = false;
            this.tabFormDefaultManager1.Form = this;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 88);
            this.barDockControlTop.Manager = null;
            this.barDockControlTop.Size = new System.Drawing.Size(1048, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 737);
            this.barDockControlBottom.Manager = null;
            this.barDockControlBottom.Size = new System.Drawing.Size(1048, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 88);
            this.barDockControlLeft.Manager = null;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 649);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1048, 88);
            this.barDockControlRight.Manager = null;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 649);
            // 
            // registerTab
            // 
            this.registerTab.ContentContainer = this.tabFormContentContainer1;
            this.registerTab.Name = "registerTab";
            this.registerTab.Text = "Registrar";
            // 
            // tabFormContentContainer1
            // 
            this.tabFormContentContainer1.Controls.Add(this.gridControl1);
            this.tabFormContentContainer1.Controls.Add(this.panel1);
            this.tabFormContentContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer1.Location = new System.Drawing.Point(0, 88);
            this.tabFormContentContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.tabFormContentContainer1.Name = "tabFormContentContainer1";
            this.tabFormContentContainer1.Size = new System.Drawing.Size(1048, 649);
            this.tabFormContentContainer1.TabIndex = 1;
            this.tabFormContentContainer1.Click += new System.EventHandler(this.tabFormContentContainer1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4);
            this.gridControl1.MenuManager = this.tabFormDefaultManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1048, 567);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.id_empresa,
            this.id_sucursal,
            this.id_empleado,
            this.nombre_completo,
            this.has_fingerprint,
            this.has_image});
            this.gridView1.DetailHeight = 525;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // id_empresa
            // 
            this.id_empresa.Caption = "Empresa";
            this.id_empresa.FieldName = "id_empresa";
            this.id_empresa.MinWidth = 30;
            this.id_empresa.Name = "id_empresa";
            this.id_empresa.OptionsColumn.ReadOnly = true;
            this.id_empresa.Visible = true;
            this.id_empresa.VisibleIndex = 0;
            this.id_empresa.Width = 135;
            // 
            // id_sucursal
            // 
            this.id_sucursal.Caption = "Sucursal";
            this.id_sucursal.FieldName = "id_sucursal";
            this.id_sucursal.MinWidth = 30;
            this.id_sucursal.Name = "id_sucursal";
            this.id_sucursal.OptionsColumn.ReadOnly = true;
            this.id_sucursal.Visible = true;
            this.id_sucursal.VisibleIndex = 1;
            this.id_sucursal.Width = 165;
            // 
            // id_empleado
            // 
            this.id_empleado.Caption = "Empleado";
            this.id_empleado.FieldName = "id_empleado";
            this.id_empleado.MinWidth = 30;
            this.id_empleado.Name = "id_empleado";
            this.id_empleado.OptionsColumn.ReadOnly = true;
            this.id_empleado.Visible = true;
            this.id_empleado.VisibleIndex = 2;
            this.id_empleado.Width = 153;
            // 
            // nombre_completo
            // 
            this.nombre_completo.Caption = "Nombre";
            this.nombre_completo.FieldName = "nombre_completo";
            this.nombre_completo.MinWidth = 30;
            this.nombre_completo.Name = "nombre_completo";
            this.nombre_completo.OptionsColumn.ReadOnly = true;
            this.nombre_completo.Visible = true;
            this.nombre_completo.VisibleIndex = 3;
            this.nombre_completo.Width = 604;
            // 
            // has_fingerprint
            // 
            this.has_fingerprint.Caption = "Tiene Huella";
            this.has_fingerprint.FieldName = "has_fingerprint";
            this.has_fingerprint.MinWidth = 30;
            this.has_fingerprint.Name = "has_fingerprint";
            this.has_fingerprint.OptionsColumn.ReadOnly = true;
            this.has_fingerprint.Visible = true;
            this.has_fingerprint.VisibleIndex = 4;
            this.has_fingerprint.Width = 123;
            // 
            // has_image
            // 
            this.has_image.Caption = "Tiene Imagen";
            this.has_image.FieldName = "has_image";
            this.has_image.MinWidth = 30;
            this.has_image.Name = "has_image";
            this.has_image.OptionsColumn.ReadOnly = true;
            this.has_image.Visible = true;
            this.has_image.VisibleIndex = 5;
            this.has_image.Width = 122;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.simpleButton1);
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txt_IdHuella);
            this.panel1.Controls.Add(this.Txt_IdEmpresa);
            this.panel1.Controls.Add(this.List_Box_Tipo);
            this.panel1.Controls.Add(this.ButtonFingerprintRegister);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 567);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1048, 82);
            this.panel1.TabIndex = 4;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(796, 13);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(56, 56);
            this.simpleButton1.TabIndex = 9;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(386, 9);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(103, 52);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            this.richTextBox1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Id Huella";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Empresa";
            // 
            // txt_IdHuella
            // 
            this.txt_IdHuella.Location = new System.Drawing.Point(86, 39);
            this.txt_IdHuella.Name = "txt_IdHuella";
            this.txt_IdHuella.ReadOnly = true;
            this.txt_IdHuella.Size = new System.Drawing.Size(100, 23);
            this.txt_IdHuella.TabIndex = 5;
            // 
            // Txt_IdEmpresa
            // 
            this.Txt_IdEmpresa.Location = new System.Drawing.Point(86, 10);
            this.Txt_IdEmpresa.Name = "Txt_IdEmpresa";
            this.Txt_IdEmpresa.ReadOnly = true;
            this.Txt_IdEmpresa.Size = new System.Drawing.Size(100, 23);
            this.Txt_IdEmpresa.TabIndex = 4;
            // 
            // List_Box_Tipo
            // 
            this.List_Box_Tipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.List_Box_Tipo.FormattingEnabled = true;
            this.List_Box_Tipo.Items.AddRange(new object[] {
            "Dv",
            "ClienteFinal",
            "Empleado",
            "Solicitud"});
            this.List_Box_Tipo.Location = new System.Drawing.Point(659, 3);
            this.List_Box_Tipo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.List_Box_Tipo.Name = "List_Box_Tipo";
            this.List_Box_Tipo.Size = new System.Drawing.Size(121, 76);
            this.List_Box_Tipo.TabIndex = 3;
            this.List_Box_Tipo.SelectedIndexChanged += new System.EventHandler(this.List_Box_Tipo_SelectedIndexChanged);
            // 
            // ButtonFingerprintRegister
            // 
            this.ButtonFingerprintRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonFingerprintRegister.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ButtonFingerprintRegister.ImageOptions.SvgImage")));
            this.ButtonFingerprintRegister.Location = new System.Drawing.Point(872, 13);
            this.ButtonFingerprintRegister.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonFingerprintRegister.Name = "ButtonFingerprintRegister";
            this.ButtonFingerprintRegister.Size = new System.Drawing.Size(163, 56);
            this.ButtonFingerprintRegister.TabIndex = 1;
            this.ButtonFingerprintRegister.Text = "Registrar Huella";
            this.ButtonFingerprintRegister.Click += new System.EventHandler(this.ButtonFingerprintRegister_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 737);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.tabFormContentContainer1);
            this.Controls.Add(this.tabFormControl1);
            this.Name = "Form1";
            this.TabFormControl = this.tabFormControl1;
            this.Text = "Registro de huellas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabFormDefaultManager1)).EndInit();
            this.tabFormContentContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.TabFormControl tabFormControl1;
        private DevExpress.XtraBars.TabFormDefaultManager tabFormDefaultManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer1;
        private DevExpress.XtraBars.TabFormPage registerTab;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn id_empresa;
        private DevExpress.XtraGrid.Columns.GridColumn id_empleado;
        private DevExpress.XtraGrid.Columns.GridColumn nombre_completo;
        private DevExpress.XtraGrid.Columns.GridColumn has_fingerprint;
        private DevExpress.XtraEditors.SimpleButton ButtonFingerprintRegister;
        private DevExpress.XtraGrid.Columns.GridColumn id_sucursal;
        private DevExpress.XtraGrid.Columns.GridColumn has_image;
        private System.Windows.Forms.CheckedListBox List_Box_Tipo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_IdHuella;
        private System.Windows.Forms.TextBox Txt_IdEmpresa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}

