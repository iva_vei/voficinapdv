﻿Public Class entEdocuenta
    Private _cliente As String
    Private _direccion As String
    Private _municipio As String
    Private _ciudad As String
    Private _estado As String
    Private _limite As Double
    Private _disponible As Double
    Private _saldo As Double
    Private _anticipo As Double
    Private _vigente As Double
    Private _vencido As Double
    Private _vencido1 As Double
    Private _vencido2 As Double
    Private _vencido3 As Double
    Private _vencido4 As Double
    Private _vencido5 As Double
    Private _vencido6 As Double
    Private _vendedor As String
    Private _cobrador As String
    Private _telefono As Double
    Private _coordinador As String
    Private _p_vencido1 As Double
    Private _p_vencido2 As Double
    Private _p_vencido3 As Double
    Private _p_vencido4 As Double
    Private _p_vencido5 As Double
    Private _p_vencido6 As Double




    Public Sub Fill(ByVal otabla As System.Data.DataTable)
        Dim renglon1 As entEdodetalle
        _ListDetalle = New List(Of entEdodetalle)
        For Each oreng As System.Data.DataRow In otabla.Rows
            renglon1 = New entEdodetalle
            renglon1.Fecha = oreng.Item("fecha")
            renglon1.Sucursal = oreng.Item("id_sucursal")
            renglon1.Cvedocumento = oreng.Item("cve_documento")
            renglon1.Cargo = oreng.Item("cargo")
            renglon1.Abono = oreng.Item("abono")
            renglon1.Saldodocto = oreng.Item("saldodocto")
            renglon1.Referencia = oreng.Item("referencia")
            renglon1.Fechavence = oreng.Item("fechavence")
            renglon1.Vencido = oreng.Item("Vencido")
            renglon1.Saldoacumulado = oreng.Item("saldo_acumulado")
            renglon1.Vencidos = oreng.Item("vencidos")
            renglon1.Vencer = oreng.Item("vencer")

            _ListDetalle.Add(renglon1)

        Next


    End Sub

    Private _ListDetalle As List(Of entEdodetalle)

    Public Property Cliente As String
        Get
            Return _cliente
        End Get
        Set(value As String)
            _cliente = value
        End Set
    End Property

    Public Property Direccion As String
        Get
            Return _direccion
        End Get
        Set(value As String)
            _direccion = value
        End Set
    End Property

    Public Property Municipio As String
        Get
            Return _municipio
        End Get
        Set(value As String)
            _municipio = value
        End Set
    End Property

    Public Property Estado As String
        Get
            Return _estado
        End Get
        Set(value As String)
            _estado = value
        End Set
    End Property

    Public Property Limite As Double
        Get
            Return _limite
        End Get
        Set(value As Double)
            _limite = value
        End Set
    End Property

    Public Property Disponible As Double
        Get
            Return _disponible
        End Get
        Set(value As Double)
            _disponible = value
        End Set
    End Property

    Public Property Saldo As Double
        Get
            Return _saldo
        End Get
        Set(value As Double)
            _saldo = value
        End Set
    End Property

    Public Property Anticipo As Double
        Get
            Return _anticipo
        End Get
        Set(value As Double)
            _anticipo = value
        End Set
    End Property

    Public Property Vigente As Double
        Get
            Return _vigente
        End Get
        Set(value As Double)
            _vigente = value
        End Set
    End Property

    Public Property Vencido As Double
        Get
            Return _vencido
        End Get
        Set(value As Double)
            _vencido = value
        End Set
    End Property

    Public Property Vencido1 As Double
        Get
            Return _vencido1
        End Get
        Set(value As Double)
            _vencido1 = value
        End Set
    End Property

    Public Property Vencido2 As Double
        Get
            Return _vencido2
        End Get
        Set(value As Double)
            _vencido2 = value
        End Set
    End Property

    Public Property Vencido3 As Double
        Get
            Return _vencido3
        End Get
        Set(value As Double)
            _vencido3 = value
        End Set
    End Property

    Public Property Vencido4 As Double
        Get
            Return _vencido4
        End Get
        Set(value As Double)
            _vencido4 = value
        End Set
    End Property

    Public Property Vencido5 As Double
        Get
            Return _vencido5
        End Get
        Set(value As Double)
            _vencido5 = value
        End Set
    End Property

    Public Property Vencido6 As Double
        Get
            Return _vencido6
        End Get
        Set(value As Double)
            _vencido6 = value
        End Set
    End Property

    Public Property Vendedor As String
        Get
            Return _vendedor
        End Get
        Set(value As String)
            _vendedor = value
        End Set
    End Property

    Public Property Cobrador As String
        Get
            Return _cobrador
        End Get
        Set(value As String)
            _cobrador = value
        End Set
    End Property

    Public Property Telefono As Double
        Get
            Return _telefono
        End Get
        Set(value As Double)
            _telefono = value
        End Set
    End Property

    Public Property ListDetalle As List(Of entEdodetalle)
        Get
            Return _ListDetalle
        End Get
        Set(value As List(Of entEdodetalle))
            _ListDetalle = value
        End Set
    End Property

    Public Property Coordinador As String
        Get
            Return _coordinador
        End Get
        Set(value As String)
            _coordinador = value
        End Set
    End Property

    Public Property P_vencido1 As Double
        Get
            Return _p_vencido1
        End Get
        Set(value As Double)
            _p_vencido1 = value
        End Set
    End Property

    Public Property P_vencido2 As Double
        Get
            Return _p_vencido2
        End Get
        Set(value As Double)
            _p_vencido2 = value
        End Set
    End Property

    Public Property P_vencido3 As Double
        Get
            Return _p_vencido3
        End Get
        Set(value As Double)
            _p_vencido3 = value
        End Set
    End Property

    Public Property P_vencido4 As Double
        Get
            Return _p_vencido4
        End Get
        Set(value As Double)
            _p_vencido4 = value
        End Set
    End Property

    Public Property P_vencido5 As Double
        Get
            Return _p_vencido5
        End Get
        Set(value As Double)
            _p_vencido5 = value
        End Set
    End Property

    Public Property P_vencido6 As Double
        Get
            Return _p_vencido6
        End Get
        Set(value As Double)
            _p_vencido6 = value
        End Set
    End Property

    Public Property Ciudad As String
        Get
            Return _ciudad
        End Get
        Set(value As String)
            _ciudad = value
        End Set
    End Property

    Public Class entEdodetalle
        Private _fecha As Date
        Private _sucursal As String
        Private _cvedocumento As String
        Private _cargo As Double
        Private _abono As Double
        Private _saldodocto As Double
        Private _referencia As String
        Private _fechavence As Date
        Private _vencido As Double
        Private _saldoacumulado As Double
        Private _vencer As Double
        Private _vencidos As Double
        Public Property Fecha As Date
            Get
                Return _fecha
            End Get
            Set(value As Date)
                _fecha = value
            End Set
        End Property

        Public Property Sucursal As String
            Get
                Return _sucursal
            End Get
            Set(value As String)
                _sucursal = value
            End Set
        End Property

        Public Property Cvedocumento As String
            Get
                Return _cvedocumento
            End Get
            Set(value As String)
                _cvedocumento = value
            End Set
        End Property

        Public Property Cargo As Double
            Get
                Return _cargo
            End Get
            Set(value As Double)
                _cargo = value
            End Set
        End Property

        Public Property Abono As Double
            Get
                Return _abono
            End Get
            Set(value As Double)
                _abono = value
            End Set
        End Property

        Public Property Saldodocto As Double
            Get
                Return _saldodocto
            End Get
            Set(value As Double)
                _saldodocto = value
            End Set
        End Property

        Public Property Referencia As String
            Get
                Return _referencia
            End Get
            Set(value As String)
                _referencia = value
            End Set
        End Property

        Public Property Fechavence As Date
            Get
                Return _fechavence
            End Get
            Set(value As Date)
                _fechavence = value
            End Set
        End Property

        Public Property Vencido As Double
            Get
                Return _vencido
            End Get
            Set(value As Double)
                _vencido = value
            End Set
        End Property

        Public Property Saldoacumulado As Double
            Get
                Return _saldoacumulado
            End Get
            Set(value As Double)
                _saldoacumulado = value
            End Set
        End Property

        Public Property Vencer As Double
            Get
                Return _vencer
            End Get
            Set(value As Double)
                _vencer = value
            End Set
        End Property

        Public Property Vencidos As Double
            Get
                Return _vencidos
            End Get
            Set(value As Double)
                _vencidos = value
            End Set
        End Property
    End Class
End Class