﻿Public Class Facturas
    Dim _movimiento As String
    Dim _id_empresa As String
    Dim _id_sucursal As String
    Dim _folio As Integer
    Dim _estatus As String
    Dim _id_cliente As String
    Dim _fecha As Date
    Dim _id_vendedor As String
    Dim _renglones As Integer
    Dim _articulos As String
    Dim _cantidad As Decimal
    Dim _porcdsctogral As Decimal
    Dim _subtotal As Decimal
    Dim _descuentoart As Decimal
    Dim _impuestoart As Decimal
    Dim _neto As Decimal
    Dim _ocargos1 As Decimal
    Dim _ocargos2 As Decimal
    Dim _impuesto As Decimal
    Dim _total As Decimal
    Dim _moneda As String
    Dim _tcambio As Decimal
    Dim _totaltc As Decimal
    Dim _nomfac As String
    Dim _direccionfac As String
    Dim _entrecallesfac As String
    Dim _coloniafac As String
    Dim _ciudadfac As String
    Dim _edofac As String
    Dim _paisfac As String
    Dim _cpostalfac As String
    Dim _rfc As String
    Dim _fechaentrega As Date
    Dim _lugarent1 As String
    Dim _lugarent2 As String
    Dim _lugarent3 As String
    Dim _observ1 As String
    Dim _observ2 As String
    Dim _tipocredito As String
    Dim _plazo As Decimal
    Dim _fechavence As Date
    Dim _tipodocto As String
    Dim _concepto As String
    Dim _autonumsuc As String
    Dim _fum As Date
    Dim _id_usuario As String
    Dim _pedido_mov As String
    Dim _pedido_fecha As Date
    Dim _impretencion As Decimal
    Dim _placas As String
    Dim _valor_declarado As String
    Dim _valor_unitario As String
    Dim _recogera As String
    Dim _chofer As String
    Dim _poliza As String
    Dim _tasa_iva As Decimal
    Dim _impuesto_iva As Decimal
    Dim _tasa_ieps As Decimal
    Dim _impuesto_ieps As Decimal
    Dim _tasa_ivaret As Decimal
    Dim _impuesto_ivaret As Decimal
    Dim _tasa_isrret As Decimal
    Dim _impuesto_isrret As Decimal
    Dim _tasa_otroimp As Decimal
    Dim _impuesto_otroimp As Decimal
    Dim _uuid As String
    Dim _metodo_pago As String
    Dim _forma_pago As String
    Dim _num_cta_pago As String
    Dim _banco As String
    Dim _uso_cfdi As String
    Dim _tipo_relacion As String
    Dim _cfdi_relacionados As String
    Dim _email_enviado As Integer
    Dim _fecha_enviado As Date
    Dim _fecha_cancelacion As Date
    Dim _folio_cancelacion As String
    Dim _xml As String
    Dim _noCertificadoSAT As String
    Dim _selloCFD As String
    Dim _selloSAT As String
    Dim _Codigo_QR As String
    Dim _FacUsoCFDI As String
    Dim _FacFormaPago As String
    Dim _TipoDocto1 As String
    Dim _FacCP As String
    Dim _nom_metodo_pago As String
    Dim _cadenaoriginal As String
    Dim _FechaTimbrado As String
    Dim _cobrador As String
    Dim _RfcProvCertif As String
    Dim _noCertificado As String

    Dim _Letra As String
    Dim _FacEmpresa As String
    Dim _FacDireccion As String
    Dim _FacRFC As String
    Dim _LugExpedicion As String
    Dim _regimen_fiscal As String
    Dim _regimen_fiscal_desc As String
    Dim _regimen_fiscal1 As String
    Dim _exportacion As String

    Dim _sw_factura_global As Boolean
    Dim _Anio As Integer
    Dim _Mes As Integer
    Dim _Periocidad As String

    Private _Detalle As List(Of FacturasDet)

    Public Property Movimiento As String
        Get
            Return _movimiento
        End Get
        Set(value As String)
            _movimiento = value
        End Set
    End Property

    Public Property Id_empresa As String
        Get
            Return _id_empresa
        End Get
        Set(value As String)
            _id_empresa = value
        End Set
    End Property

    Public Property Id_sucursal As String
        Get
            Return _id_sucursal
        End Get
        Set(value As String)
            _id_sucursal = value
        End Set
    End Property

    Public Property Folio As Integer
        Get
            Return _folio
        End Get
        Set(value As Integer)
            _folio = value
        End Set
    End Property

    Public Property Estatus As String
        Get
            Return _estatus
        End Get
        Set(value As String)
            _estatus = value
        End Set
    End Property

    Public Property Id_cliente As String
        Get
            Return _id_cliente
        End Get
        Set(value As String)
            _id_cliente = value
        End Set
    End Property

    Public Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Public Property Id_vendedor As String
        Get
            Return _id_vendedor
        End Get
        Set(value As String)
            _id_vendedor = value
        End Set
    End Property

    Public Property Renglones As Integer
        Get
            Return _renglones
        End Get
        Set(value As Integer)
            _renglones = value
        End Set
    End Property

    Public Property Articulos As String
        Get
            Return _articulos
        End Get
        Set(value As String)
            _articulos = value
        End Set
    End Property

    Public Property Cantidad As Decimal
        Get
            Return _cantidad
        End Get
        Set(value As Decimal)
            _cantidad = value
        End Set
    End Property

    Public Property Porcdsctogral As Decimal
        Get
            Return _porcdsctogral
        End Get
        Set(value As Decimal)
            _porcdsctogral = value
        End Set
    End Property

    Public Property Subtotal As Decimal
        Get
            Return _subtotal
        End Get
        Set(value As Decimal)
            _subtotal = value
        End Set
    End Property

    Public Property Descuentoart As Decimal
        Get
            Return _descuentoart
        End Get
        Set(value As Decimal)
            _descuentoart = value
        End Set
    End Property

    Public Property Impuestoart As Decimal
        Get
            Return _impuestoart
        End Get
        Set(value As Decimal)
            _impuestoart = value
        End Set
    End Property

    Public Property Neto As Decimal
        Get
            Return _neto
        End Get
        Set(value As Decimal)
            _neto = value
        End Set
    End Property

    Public Property Ocargos1 As Decimal
        Get
            Return _ocargos1
        End Get
        Set(value As Decimal)
            _ocargos1 = value
        End Set
    End Property

    Public Property Ocargos2 As Decimal
        Get
            Return _ocargos2
        End Get
        Set(value As Decimal)
            _ocargos2 = value
        End Set
    End Property

    Public Property Impuesto As Decimal
        Get
            Return _impuesto
        End Get
        Set(value As Decimal)
            _impuesto = value
        End Set
    End Property

    Public Property Total As Decimal
        Get
            Return _total
        End Get
        Set(value As Decimal)
            _total = value
        End Set
    End Property

    Public Property Moneda As String
        Get
            Return _moneda
        End Get
        Set(value As String)
            _moneda = value
        End Set
    End Property

    Public Property Tcambio As Decimal
        Get
            Return _tcambio
        End Get
        Set(value As Decimal)
            _tcambio = value
        End Set
    End Property

    Public Property Totaltc As Decimal
        Get
            Return _totaltc
        End Get
        Set(value As Decimal)
            _totaltc = value
        End Set
    End Property

    Public Property Nomfac As String
        Get
            Return _nomfac
        End Get
        Set(value As String)
            _nomfac = value
        End Set
    End Property

    Public Property Direccionfac As String
        Get
            Return _direccionfac
        End Get
        Set(value As String)
            _direccionfac = value
        End Set
    End Property

    Public Property Entrecallesfac As String
        Get
            Return _entrecallesfac
        End Get
        Set(value As String)
            _entrecallesfac = value
        End Set
    End Property

    Public Property Coloniafac As String
        Get
            Return _coloniafac
        End Get
        Set(value As String)
            _coloniafac = value
        End Set
    End Property

    Public Property Ciudadfac As String
        Get
            Return _ciudadfac
        End Get
        Set(value As String)
            _ciudadfac = value
        End Set
    End Property

    Public Property Edofac As String
        Get
            Return _edofac
        End Get
        Set(value As String)
            _edofac = value
        End Set
    End Property

    Public Property Paisfac As String
        Get
            Return _paisfac
        End Get
        Set(value As String)
            _paisfac = value
        End Set
    End Property

    Public Property Cpostalfac As String
        Get
            Return _cpostalfac
        End Get
        Set(value As String)
            _cpostalfac = value
        End Set
    End Property

    Public Property Rfc As String
        Get
            Return _rfc
        End Get
        Set(value As String)
            _rfc = value
        End Set
    End Property

    Public Property Fechaentrega As Date
        Get
            Return _fechaentrega
        End Get
        Set(value As Date)
            _fechaentrega = value
        End Set
    End Property

    Public Property Lugarent1 As String
        Get
            Return _lugarent1
        End Get
        Set(value As String)
            _lugarent1 = value
        End Set
    End Property

    Public Property Lugarent2 As String
        Get
            Return _lugarent2
        End Get
        Set(value As String)
            _lugarent2 = value
        End Set
    End Property

    Public Property Lugarent3 As String
        Get
            Return _lugarent3
        End Get
        Set(value As String)
            _lugarent3 = value
        End Set
    End Property

    Public Property Observ1 As String
        Get
            Return _observ1
        End Get
        Set(value As String)
            _observ1 = value
        End Set
    End Property

    Public Property Observ2 As String
        Get
            Return _observ2
        End Get
        Set(value As String)
            _observ2 = value
        End Set
    End Property

    Public Property Tipocredito As String
        Get
            Return _tipocredito
        End Get
        Set(value As String)
            _tipocredito = value
        End Set
    End Property

    Public Property Plazo As Decimal
        Get
            Return _plazo
        End Get
        Set(value As Decimal)
            _plazo = value
        End Set
    End Property

    Public Property Fechavence As Date
        Get
            Return _fechavence
        End Get
        Set(value As Date)
            _fechavence = value
        End Set
    End Property

    Public Property Tipodocto As String
        Get
            Return _tipodocto
        End Get
        Set(value As String)
            _tipodocto = value
        End Set
    End Property

    Public Property Concepto As String
        Get
            Return _concepto
        End Get
        Set(value As String)
            _concepto = value
        End Set
    End Property

    Public Property Autonumsuc As String
        Get
            Return _autonumsuc
        End Get
        Set(value As String)
            _autonumsuc = value
        End Set
    End Property

    Public Property Fum As Date
        Get
            Return _fum
        End Get
        Set(value As Date)
            _fum = value
        End Set
    End Property

    Public Property Id_usuario As String
        Get
            Return _id_usuario
        End Get
        Set(value As String)
            _id_usuario = value
        End Set
    End Property

    Public Property Pedido_mov As String
        Get
            Return _pedido_mov
        End Get
        Set(value As String)
            _pedido_mov = value
        End Set
    End Property

    Public Property Pedido_fecha As Date
        Get
            Return _pedido_fecha
        End Get
        Set(value As Date)
            _pedido_fecha = value
        End Set
    End Property

    Public Property Impretencion As Decimal
        Get
            Return _impretencion
        End Get
        Set(value As Decimal)
            _impretencion = value
        End Set
    End Property

    Public Property Placas As String
        Get
            Return _placas
        End Get
        Set(value As String)
            _placas = value
        End Set
    End Property

    Public Property Valor_declarado As String
        Get
            Return _valor_declarado
        End Get
        Set(value As String)
            _valor_declarado = value
        End Set
    End Property

    Public Property Valor_unitario As String
        Get
            Return _valor_unitario
        End Get
        Set(value As String)
            _valor_unitario = value
        End Set
    End Property

    Public Property Recogera As String
        Get
            Return _recogera
        End Get
        Set(value As String)
            _recogera = value
        End Set
    End Property

    Public Property Chofer As String
        Get
            Return _chofer
        End Get
        Set(value As String)
            _chofer = value
        End Set
    End Property

    Public Property Poliza As String
        Get
            Return _poliza
        End Get
        Set(value As String)
            _poliza = value
        End Set
    End Property

    Public Property Tasa_iva As Decimal
        Get
            Return _tasa_iva
        End Get
        Set(value As Decimal)
            _tasa_iva = value
        End Set
    End Property

    Public Property Impuesto_iva As Decimal
        Get
            Return _impuesto_iva
        End Get
        Set(value As Decimal)
            _impuesto_iva = value
        End Set
    End Property

    Public Property Tasa_ieps As Decimal
        Get
            Return _tasa_ieps
        End Get
        Set(value As Decimal)
            _tasa_ieps = value
        End Set
    End Property

    Public Property Impuesto_ieps As Decimal
        Get
            Return _impuesto_ieps
        End Get
        Set(value As Decimal)
            _impuesto_ieps = value
        End Set
    End Property

    Public Property Tasa_ivaret As Decimal
        Get
            Return _tasa_ivaret
        End Get
        Set(value As Decimal)
            _tasa_ivaret = value
        End Set
    End Property

    Public Property Impuesto_ivaret As Decimal
        Get
            Return _impuesto_ivaret
        End Get
        Set(value As Decimal)
            _impuesto_ivaret = value
        End Set
    End Property

    Public Property Tasa_isrret As Decimal
        Get
            Return _tasa_isrret
        End Get
        Set(value As Decimal)
            _tasa_isrret = value
        End Set
    End Property

    Public Property Impuesto_isrret As Decimal
        Get
            Return _impuesto_isrret
        End Get
        Set(value As Decimal)
            _impuesto_isrret = value
        End Set
    End Property

    Public Property Tasa_otroimp As Decimal
        Get
            Return _tasa_otroimp
        End Get
        Set(value As Decimal)
            _tasa_otroimp = value
        End Set
    End Property

    Public Property Impuesto_otroimp As Decimal
        Get
            Return _impuesto_otroimp
        End Get
        Set(value As Decimal)
            _impuesto_otroimp = value
        End Set
    End Property

    Public Property Uuid As String
        Get
            Return _uuid
        End Get
        Set(value As String)
            _uuid = value
        End Set
    End Property

    Public Property Metodo_pago As String
        Get
            Return _metodo_pago
        End Get
        Set(value As String)
            _metodo_pago = value
        End Set
    End Property

    Public Property Forma_pago As String
        Get
            Return _forma_pago
        End Get
        Set(value As String)
            _forma_pago = value
        End Set
    End Property

    Public Property Num_cta_pago As String
        Get
            Return _num_cta_pago
        End Get
        Set(value As String)
            _num_cta_pago = value
        End Set
    End Property

    Public Property Banco As String
        Get
            Return _banco
        End Get
        Set(value As String)
            _banco = value
        End Set
    End Property

    Public Property Uso_cfdi As String
        Get
            Return _uso_cfdi
        End Get
        Set(value As String)
            _uso_cfdi = value
        End Set
    End Property

    Public Property Tipo_relacion As String
        Get
            Return _tipo_relacion
        End Get
        Set(value As String)
            _tipo_relacion = value
        End Set
    End Property

    Public Property Cfdi_relacionados As String
        Get
            Return _cfdi_relacionados
        End Get
        Set(value As String)
            _cfdi_relacionados = value
        End Set
    End Property

    Public Property Email_enviado As Integer
        Get
            Return _email_enviado
        End Get
        Set(value As Integer)
            _email_enviado = value
        End Set
    End Property

    Public Property Fecha_enviado As Date
        Get
            Return _fecha_enviado
        End Get
        Set(value As Date)
            _fecha_enviado = value
        End Set
    End Property

    Public Property Fecha_cancelacion As Date
        Get
            Return _fecha_cancelacion
        End Get
        Set(value As Date)
            _fecha_cancelacion = value
        End Set
    End Property

    Public Property Folio_cancelacion As String
        Get
            Return _folio_cancelacion
        End Get
        Set(value As String)
            _folio_cancelacion = value
        End Set
    End Property

    Public Property Xml As String
        Get
            Return _xml
        End Get
        Set(value As String)
            _xml = value
        End Set
    End Property

    Public Property Detalle As List(Of FacturasDet)
        Get
            Return _Detalle
        End Get
        Set(value As List(Of FacturasDet))
            _Detalle = value
        End Set
    End Property

    Public Property NoCertificadoSAT As String
        Get
            Return _noCertificadoSAT
        End Get
        Set(value As String)
            _noCertificadoSAT = value
        End Set
    End Property

    Public Property SelloCFD As String
        Get
            Return _selloCFD
        End Get
        Set(value As String)
            _selloCFD = value
        End Set
    End Property

    Public Property SelloSAT As String
        Get
            Return _selloSAT
        End Get
        Set(value As String)
            _selloSAT = value
        End Set
    End Property

    Public Property FacEmpresa As String
        Get
            Return _FacEmpresa
        End Get
        Set(value As String)
            _FacEmpresa = value
        End Set
    End Property

    Public Property FacDireccion As String
        Get
            Return _FacDireccion
        End Get
        Set(value As String)
            _FacDireccion = value
        End Set
    End Property

    Public Property FacRFC As String
        Get
            Return _FacRFC
        End Get
        Set(value As String)
            _FacRFC = value
        End Set
    End Property

    Public Property LugExpedicion As String
        Get
            Return _LugExpedicion
        End Get
        Set(value As String)
            _LugExpedicion = value
        End Set
    End Property

    Public Property Letra As String
        Get
            Return _Letra
        End Get
        Set(value As String)
            _Letra = value
        End Set
    End Property

    Public Property Codigo_QR As String
        Get
            Return _Codigo_QR
        End Get
        Set(value As String)
            _Codigo_QR = value
        End Set
    End Property

    Public Property FacUsoCFDI As String
        Get
            Return _FacUsoCFDI
        End Get
        Set(value As String)
            _FacUsoCFDI = value
        End Set
    End Property

    Public Property FacFormaPago As String
        Get
            Return _FacFormaPago
        End Get
        Set(value As String)
            _FacFormaPago = value
        End Set
    End Property

    Public Property TipoDocto1 As String
        Get
            Return _TipoDocto1
        End Get
        Set(value As String)
            _TipoDocto1 = value
        End Set
    End Property

    Public Property FacCP As String
        Get
            Return _FacCP
        End Get
        Set(value As String)
            _FacCP = value
        End Set
    End Property

    Public Property Nom_metodo_pago As String
        Get
            Return _nom_metodo_pago
        End Get
        Set(value As String)
            _nom_metodo_pago = value
        End Set
    End Property

    Public Property Cadenaoriginal As String
        Get
            Return _cadenaoriginal
        End Get
        Set(value As String)
            _cadenaoriginal = value
        End Set
    End Property
    Public Property FechaTimbrado As String
        Get
            Return _FechaTimbrado
        End Get
        Set(value As String)
            _FechaTimbrado = value
        End Set
    End Property

    Public Property Cobrador As String
        Get
            Return _cobrador
        End Get
        Set(value As String)
            _cobrador = value
        End Set
    End Property

    Public Property RfcProvCertif As String
        Get
            Return _RfcProvCertif
        End Get
        Set(value As String)
            _RfcProvCertif = value
        End Set
    End Property

    Public Property NoCertificado As String
        Get
            Return _noCertificado
        End Get
        Set(value As String)
            _noCertificado = value
        End Set
    End Property

    Public Property Regimen_fiscal As String
        Get
            Return _regimen_fiscal
        End Get
        Set(value As String)
            _regimen_fiscal = value
        End Set
    End Property

    Public Property Regimen_fiscal1 As String
        Get
            Return _regimen_fiscal1
        End Get
        Set(value As String)
            _regimen_fiscal1 = value
        End Set
    End Property

    Public Property Exportacion As String
        Get
            Return _exportacion
        End Get
        Set(value As String)
            _exportacion = value
        End Set
    End Property

    Public Property Regimen_fiscal_desc As String
        Get
            Return _regimen_fiscal_desc
        End Get
        Set(value As String)
            _regimen_fiscal_desc = value
        End Set
    End Property

    Public ReadOnly Property Anio As Integer
        Get
            Return _fecha.Year
        End Get
    End Property

    Public ReadOnly Property Mes As Integer
        Get
            Return _fecha.Month
        End Get
    End Property

    Public Property Periocidad As String
        Get
            Return _Periocidad
        End Get
        Set(value As String)
            Select Case value
                Case "01" : _Periocidad = "MENSUAL"
                Case "02" : _Periocidad = "BIMESTRAL"
                Case "03" : _Periocidad = "TRIMESTRAL"
                Case "04" : _Periocidad = "CUATRIMESTRAL"
                Case "05" : _Periocidad = "SEMESTRAL"
                Case "06" : _Periocidad = "SEMESTRAL POR LIQUIDACIÓN"
                Case "07" : _Periocidad = "DEL EJERCICIO"
                Case "08" : _Periocidad = "DEL EJERCICIO POR LIQUIDACIÓN"
                Case "09" : _Periocidad = "AJUSTE"
                Case "10" : _Periocidad = "SIN PERIODO"
                Case Else : _Periocidad = "SIN PERIODO"
            End Select
        End Set
    End Property

    Public ReadOnly Property Sw_factura_global As Boolean
        Get
            Return IIf(_nomfac.Trim.ToUpper = “PUBLICO EN GENERAL”, True, False)
        End Get
    End Property

    Public Sub Fill(ByVal dtCab As DataTable, ByVal dtDet As DataTable, ByVal dtOtros As DataTable)
        Dim oDet As FacturasDet
        Try
            _Detalle = New List(Of FacturasDet)
            If dtCab.Rows.Count > 0 Then
                _movimiento = dtCab.Rows(0).Item("movimiento")
                _id_empresa = dtCab.Rows(0).Item("id_empresa")
                _id_sucursal = dtCab.Rows(0).Item("id_sucursal")
                _folio = dtCab.Rows(0).Item("folio")
                _estatus = dtCab.Rows(0).Item("estatus")
                _id_cliente = dtCab.Rows(0).Item("id_cliente")
                _fecha = dtCab.Rows(0).Item("fecha")
                _renglones = dtCab.Rows(0).Item("renglones")
                _articulos = dtCab.Rows(0).Item("articulos")
                _cantidad = dtCab.Rows(0).Item("cantidad")
                _porcdsctogral = dtCab.Rows(0).Item("porcdsctogral")
                _subtotal = dtCab.Rows(0).Item("subtotal")
                _descuentoart = dtCab.Rows(0).Item("descuentoart")
                _impuestoart = dtCab.Rows(0).Item("impuestoart")
                _neto = dtCab.Rows(0).Item("neto")
                _ocargos1 = dtCab.Rows(0).Item("ocargos1")
                _ocargos2 = dtCab.Rows(0).Item("ocargos2")
                _impuesto = dtCab.Rows(0).Item("impuesto")
                _total = dtCab.Rows(0).Item("total")
                _moneda = dtCab.Rows(0).Item("moneda")
                _tcambio = dtCab.Rows(0).Item("tcambio")
                _totaltc = dtCab.Rows(0).Item("totaltc")
                _nomfac = dtCab.Rows(0).Item("nomfac")
                _direccionfac = dtCab.Rows(0).Item("direccionfac")
                _entrecallesfac = dtCab.Rows(0).Item("entrecallesfac")
                _coloniafac = dtCab.Rows(0).Item("coloniafac")
                _ciudadfac = dtCab.Rows(0).Item("ciudadfac")
                _edofac = dtCab.Rows(0).Item("edofac")
                _paisfac = dtCab.Rows(0).Item("paisfac")
                _cpostalfac = dtCab.Rows(0).Item("cpostalfac")
                _rfc = dtCab.Rows(0).Item("rfc")
                _fechaentrega = dtCab.Rows(0).Item("fechaentrega")
                _lugarent1 = dtCab.Rows(0).Item("lugarent1")
                _lugarent2 = dtCab.Rows(0).Item("lugarent2")
                _lugarent3 = dtCab.Rows(0).Item("lugarent3")
                _observ1 = dtCab.Rows(0).Item("observ1")
                _observ2 = dtCab.Rows(0).Item("observ2")
                _tipocredito = dtCab.Rows(0).Item("tipocredito")
                _plazo = dtCab.Rows(0).Item("plazo")
                _fechavence = dtCab.Rows(0).Item("fechavence")
                _tipodocto = dtCab.Rows(0).Item("tipodocto")
                _concepto = dtCab.Rows(0).Item("concepto")
                _autonumsuc = dtCab.Rows(0).Item("autonumsuc")
                _fum = dtCab.Rows(0).Item("fum")
                _id_usuario = dtCab.Rows(0).Item("id_usuario")
                _pedido_mov = dtCab.Rows(0).Item("pedido_mov")
                _pedido_fecha = dtCab.Rows(0).Item("pedido_fecha")
                _impretencion = dtCab.Rows(0).Item("impretencion")
                _placas = dtCab.Rows(0).Item("placas")
                _valor_declarado = dtCab.Rows(0).Item("valor_declarado")
                _valor_unitario = dtCab.Rows(0).Item("valor_unitario")
                _recogera = dtCab.Rows(0).Item("recogera")
                _chofer = dtCab.Rows(0).Item("chofer")
                _poliza = dtCab.Rows(0).Item("poliza")
                _tasa_iva = dtCab.Rows(0).Item("tasa_iva")
                _impuesto_iva = dtCab.Rows(0).Item("impuesto_iva")
                _tasa_ieps = dtCab.Rows(0).Item("tasa_ieps")
                _impuesto_ieps = dtCab.Rows(0).Item("impuesto_ieps")
                _tasa_ivaret = dtCab.Rows(0).Item("tasa_ivaret")
                _impuesto_ivaret = dtCab.Rows(0).Item("impuesto_ivaret")
                _tasa_isrret = dtCab.Rows(0).Item("tasa_isrret")
                _impuesto_isrret = dtCab.Rows(0).Item("impuesto_isrret")
                _tasa_otroimp = dtCab.Rows(0).Item("tasa_otroimp")
                _impuesto_otroimp = dtCab.Rows(0).Item("impuesto_otroimp")
                _uuid = dtCab.Rows(0).Item("uuid")
                _metodo_pago = dtCab.Rows(0).Item("metodo_pago")
                _forma_pago = dtCab.Rows(0).Item("forma_pago")
                _num_cta_pago = dtCab.Rows(0).Item("num_cta_pago")
                _banco = dtCab.Rows(0).Item("banco")
                _uso_cfdi = dtCab.Rows(0).Item("uso_cfdi")
                _tipo_relacion = dtCab.Rows(0).Item("tipo_relacion")
                _cfdi_relacionados = dtCab.Rows(0).Item("cfdi_relacionados")
                _email_enviado = dtCab.Rows(0).Item("email_enviado")
                _fecha_enviado = dtCab.Rows(0).Item("fecha_enviado")
                _fecha_cancelacion = dtCab.Rows(0).Item("fecha_cancelacion")
                _folio_cancelacion = dtCab.Rows(0).Item("folio_cancelacion")
                _xml = dtCab.Rows(0).Item("xml")
                _noCertificadoSAT = dtCab.Rows(0).Item("noCertificadoSAT")
                _selloCFD = dtCab.Rows(0).Item("selloCFD")
                _selloSAT = dtCab.Rows(0).Item("selloSAT")
                _Letra = dtCab.Rows(0).Item("Letra")
                _Codigo_QR = dtCab.Rows(0).Item("codigo_qr")
                _FacUsoCFDI = dtCab.Rows(0).Item("FacUsoCFDI")
                _FacFormaPago = dtCab.Rows(0).Item("FacFormaPago")
                _TipoDocto1 = dtCab.Rows(0).Item("tipodocto1")
                _nom_metodo_pago = dtCab.Rows(0).Item("nom_metodo_pago")
                _cadenaoriginal = dtCab.Rows(0).Item("cadenaoriginal")
                _FechaTimbrado = dtCab.Rows(0).Item("fechatimbrado")
                _RfcProvCertif = dtCab.Rows(0).Item("rfcprovcertif")
                _noCertificado = dtCab.Rows(0).Item("noCertificado")

                _FacEmpresa = dtOtros.Rows(0).Item("FacEmpresa")
                _FacDireccion = dtOtros.Rows(0).Item("FacDireccion")
                _LugExpedicion = dtOtros.Rows(0).Item("LugExpedicion")
                _FacCP = dtOtros.Rows(0).Item("FacCP")
                _id_vendedor = dtOtros.Rows(0).Item("vendedor")
                _cobrador = dtOtros.Rows(0).Item("cobrador")
                Try
                    _regimen_fiscal = dtCab.Rows(0).Item("regimen_fiscal")
                Catch ex As Exception
                    _regimen_fiscal = "M"
                End Try
                Try
                    _regimen_fiscal1 = dtCab.Rows(0).Item("regimen_fiscal1")
                Catch ex As Exception
                    _regimen_fiscal1 = "601"
                End Try
                Try
                    _regimen_fiscal_desc = dtCab.Rows(0).Item("regimen_fiscal_desc")
                Catch ex As Exception
                    _regimen_fiscal1 = ""
                End Try
                Try
                    _exportacion = dtCab.Rows(0).Item("exportacion")
                Catch ex As Exception
                    _exportacion = "01"
                End Try
                Try
                    Periocidad = dtCab.Rows(0).Item("periocidad")
                Catch ex As Exception
                    Periocidad = "10"
                End Try

                For Each oRen As DataRow In dtDet.Rows
                    oDet = New FacturasDet
                    oDet.Movimiento = oRen.Item("movimiento")
                    oDet.Renglon = oRen.Item("renglon")
                    oDet.Renglon1 = oRen.Item("renglon1")
                    oDet.Id_empresa = oRen.Item("id_empresa")
                    oDet.Id_sucursal = oRen.Item("id_sucursal")
                    oDet.Folio = oRen.Item("folio")
                    oDet.Articulo = oRen.Item("articulo")
                    oDet.Concepto = oRen.Item("concepto")
                    oDet.Unidadvta = oRen.Item("unidadvta")
                    oDet.Cantidad = Math.Abs(oRen.Item("cantidad"))
                    oDet.Precionormal = Math.Abs(oRen.Item("precionormal"))
                    oDet.Precioventa = Math.Abs(oRen.Item("precioventa"))
                    oDet.Precioventasiva = Math.Abs(oRen.Item("precioventasiva"))
                    oDet.Porcdscto = Math.Abs(oRen.Item("porcdscto"))
                    oDet.Precio = Math.Abs(oRen.Item("precio"))
                    oDet.Iva = oRen.Item("iva")
                    oDet.Importe = Math.Abs(oRen.Item("importe"))
                    oDet.Descuento = Math.Abs(oRen.Item("descuento"))
                    oDet.Impuesto = Math.Abs(oRen.Item("impuesto"))
                    oDet.Subtotal = Math.Abs(oRen.Item("subtotal"))
                    oDet.Cve_sucursal1 = oRen.Item("cve_sucursal1")
                    oDet.Id_sucursal1 = oRen.Item("id_sucursal1")
                    oDet.Formaentrega = oRen.Item("formaentrega")
                    oDet.Lugarentrega = oRen.Item("lugarentrega")
                    oDet.Cantpendiente = oRen.Item("cantpendiente")
                    oDet.Cantdevuelta = oRen.Item("cantdevuelta")
                    oDet.Claveruta = oRen.Item("claveruta")
                    oDet.Autonumsuc = oRen.Item("autonumsuc")
                    oDet.Fum = oRen.Item("fum")
                    oDet.Id_usuario = oRen.Item("id_usuario")
                    oDet.Retencion = oRen.Item("retencion")
                    oDet.Impretencion = Math.Abs(oRen.Item("impretencion"))
                    oDet.Empaque = Math.Abs(oRen.Item("empaque"))
                    oDet.Tasa_iva = Math.Abs(oRen.Item("tasa_iva"))
                    oDet.Impuesto_iva = Math.Abs(oRen.Item("impuesto_iva"))
                    oDet.Tasa_ieps = Math.Abs(oRen.Item("tasa_ieps"))
                    oDet.Impuesto_ieps = Math.Abs(oRen.Item("impuesto_ieps"))
                    oDet.Tasa_ivaret = Math.Abs(oRen.Item("tasa_ivaret"))
                    oDet.Impuesto_ivaret = Math.Abs(oRen.Item("impuesto_ivaret"))
                    oDet.Tasa_isrret = Math.Abs(oRen.Item("tasa_isrret"))
                    oDet.Impuesto_isrret = Math.Abs(oRen.Item("impuesto_isrret"))
                    oDet.Tasa_otroimp = Math.Abs(oRen.Item("tasa_otroimp"))
                    oDet.Impuesto_otroimp = Math.Abs(oRen.Item("impuesto_otroimp"))
                    oDet.Costopromart = Math.Abs(oRen.Item("costopromart"))
                    oDet.Iva_impuesto = oRen.Item("iva_impuesto")
                    oDet.Iva_tipofactor = oRen.Item("iva_tipofactor")

                    Try
                        oDet.Importe1 = oRen.Item("importe1")
                    Catch ex As Exception
                        oDet.Importe1 = 0.0
                    End Try

                    Try
                        oDet.Subtotal1 = oRen.Item("subtotal1")
                    Catch ex As Exception
                        oDet.Subtotal1 = 0.0
                    End Try

                    Try
                        oDet.Objetoimp = oRen.Item("objetoimp")
                    Catch ex As Exception
                        oDet.Objetoimp = "01"
                    End Try

                    Try
                        oDet.Claveunidad = oRen.Item("claveunidad")
                    Catch ex As Exception
                        oDet.Claveunidad = ""
                    End Try

                    Try
                        oDet.Claveunidaddesc = oRen.Item("claveunidaddesc")
                    Catch ex As Exception
                        oDet.Claveunidaddesc = ""
                    End Try
                    _Detalle.Add(oDet)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
Public Class FacturasDet
    Dim _movimiento As String
    Dim _renglon As Integer
    Dim _renglon1 As Integer
    Dim _id_empresa As String
    Dim _id_sucursal As String
    Dim _folio As Integer
    Dim _articulo As String
    Dim _concepto As String
    Dim _unidadvta As String
    Dim _cantidad As Decimal
    Dim _precionormal As Decimal
    Dim _precioventa As Decimal
    Dim _precioventasiva As Decimal
    Dim _porcdscto As Decimal
    Dim _precio As Decimal
    Dim _iva As String
    Dim _importe As Decimal
    Dim _importe1 As Decimal
    Dim _descuento As Decimal
    Dim _impuesto As Decimal
    Dim _subtotal As Decimal
    Dim _subtotal1 As Decimal
    Dim _cve_sucursal1 As String
    Dim _id_sucursal1 As String
    Dim _formaentrega As String
    Dim _lugarentrega As String
    Dim _cantpendiente As Decimal
    Dim _cantdevuelta As Decimal
    Dim _claveruta As String
    Dim _autonumsuc As String
    Dim _fum As Date
    Dim _id_usuario As String
    Dim _retencion As String
    Dim _impretencion As Decimal
    Dim _empaque As Decimal
    Dim _tasa_iva As Decimal
    Dim _impuesto_iva As Decimal
    Dim _tasa_ieps As Decimal
    Dim _impuesto_ieps As Decimal
    Dim _tasa_ivaret As Decimal
    Dim _impuesto_ivaret As Decimal
    Dim _tasa_isrret As Decimal
    Dim _impuesto_isrret As Decimal
    Dim _tasa_otroimp As Decimal
    Dim _impuesto_otroimp As Decimal
    Dim _costopromart As Decimal
    Dim _iva_impuesto As String
    Dim _iva_tipofactor As String
    Dim _objetoimp As String
    Dim _claveunidad As String
    Dim _claveunidaddesc As String

    Public Property Movimiento As String
        Get
            Return _movimiento
        End Get
        Set(value As String)
            _movimiento = value
        End Set
    End Property

    Public Property Renglon As Integer
        Get
            Return _renglon
        End Get
        Set(value As Integer)
            _renglon = value
        End Set
    End Property

    Public Property Id_empresa As String
        Get
            Return _id_empresa
        End Get
        Set(value As String)
            _id_empresa = value
        End Set
    End Property

    Public Property Id_sucursal As String
        Get
            Return _id_sucursal
        End Get
        Set(value As String)
            _id_sucursal = value
        End Set
    End Property

    Public Property Folio As Integer
        Get
            Return _folio
        End Get
        Set(value As Integer)
            _folio = value
        End Set
    End Property

    Public Property Articulo As String
        Get
            Return _articulo
        End Get
        Set(value As String)
            _articulo = value
        End Set
    End Property

    Public Property Concepto As String
        Get
            Return _concepto
        End Get
        Set(value As String)
            _concepto = value
        End Set
    End Property

    Public Property Unidadvta As String
        Get
            Return _unidadvta
        End Get
        Set(value As String)
            _unidadvta = value
        End Set
    End Property

    Public Property Cantidad As Decimal
        Get
            Return _cantidad
        End Get
        Set(value As Decimal)
            _cantidad = value
        End Set
    End Property

    Public Property Precionormal As Decimal
        Get
            Return _precionormal
        End Get
        Set(value As Decimal)
            _precionormal = value
        End Set
    End Property

    Public Property Precioventa As Decimal
        Get
            Return _precioventa
        End Get
        Set(value As Decimal)
            _precioventa = value
        End Set
    End Property

    Public Property Precioventasiva As Decimal
        Get
            Return _precioventasiva
        End Get
        Set(value As Decimal)
            _precioventasiva = value
        End Set
    End Property

    Public Property Porcdscto As Decimal
        Get
            Return _porcdscto
        End Get
        Set(value As Decimal)
            _porcdscto = value
        End Set
    End Property

    Public Property Precio As Decimal
        Get
            Return _precio
        End Get
        Set(value As Decimal)
            _precio = value
        End Set
    End Property

    Public Property Iva As String
        Get
            Return _iva
        End Get
        Set(value As String)
            _iva = value
        End Set
    End Property

    Public Property Importe As Decimal
        Get
            Return _importe
        End Get
        Set(value As Decimal)
            _importe = value
        End Set
    End Property

    Public Property Importe1 As Decimal
        Get
            Return _importe1
        End Get
        Set(value As Decimal)
            _importe1 = value
        End Set
    End Property
    Public Property Descuento As Decimal
        Get
            Return _descuento
        End Get
        Set(value As Decimal)
            _descuento = value
        End Set
    End Property

    Public Property Impuesto As Decimal
        Get
            Return _impuesto
        End Get
        Set(value As Decimal)
            _impuesto = value
        End Set
    End Property

    Public Property Subtotal As Decimal
        Get
            Return _subtotal
        End Get
        Set(value As Decimal)
            _subtotal = value
        End Set
    End Property
    Public Property Subtotal1 As Decimal
        Get
            Return _subtotal1
        End Get
        Set(value As Decimal)
            _subtotal1 = value
        End Set
    End Property

    Public Property Cve_sucursal1 As String
        Get
            Return _cve_sucursal1
        End Get
        Set(value As String)
            _cve_sucursal1 = value
        End Set
    End Property

    Public Property Id_sucursal1 As String
        Get
            Return _id_sucursal1
        End Get
        Set(value As String)
            _id_sucursal1 = value
        End Set
    End Property

    Public Property Formaentrega As String
        Get
            Return _formaentrega
        End Get
        Set(value As String)
            _formaentrega = value
        End Set
    End Property

    Public Property Lugarentrega As String
        Get
            Return _lugarentrega
        End Get
        Set(value As String)
            _lugarentrega = value
        End Set
    End Property

    Public Property Cantpendiente As Decimal
        Get
            Return _cantpendiente
        End Get
        Set(value As Decimal)
            _cantpendiente = value
        End Set
    End Property

    Public Property Cantdevuelta As Decimal
        Get
            Return _cantdevuelta
        End Get
        Set(value As Decimal)
            _cantdevuelta = value
        End Set
    End Property

    Public Property Claveruta As String
        Get
            Return _claveruta
        End Get
        Set(value As String)
            _claveruta = value
        End Set
    End Property

    Public Property Autonumsuc As String
        Get
            Return _autonumsuc
        End Get
        Set(value As String)
            _autonumsuc = value
        End Set
    End Property

    Public Property Fum As Date
        Get
            Return _fum
        End Get
        Set(value As Date)
            _fum = value
        End Set
    End Property

    Public Property Id_usuario As String
        Get
            Return _id_usuario
        End Get
        Set(value As String)
            _id_usuario = value
        End Set
    End Property

    Public Property Retencion As String
        Get
            Return _retencion
        End Get
        Set(value As String)
            _retencion = value
        End Set
    End Property

    Public Property Impretencion As Decimal
        Get
            Return _impretencion
        End Get
        Set(value As Decimal)
            _impretencion = value
        End Set
    End Property

    Public Property Empaque As Decimal
        Get
            Return _empaque
        End Get
        Set(value As Decimal)
            _empaque = value
        End Set
    End Property

    Public Property Tasa_iva As Decimal
        Get
            Return _tasa_iva
        End Get
        Set(value As Decimal)
            _tasa_iva = value
        End Set
    End Property

    Public Property Impuesto_iva As Decimal
        Get
            Return _impuesto_iva
        End Get
        Set(value As Decimal)
            _impuesto_iva = value
        End Set
    End Property

    Public Property Tasa_ieps As Decimal
        Get
            Return _tasa_ieps
        End Get
        Set(value As Decimal)
            _tasa_ieps = value
        End Set
    End Property

    Public Property Impuesto_ieps As Decimal
        Get
            Return _impuesto_ieps
        End Get
        Set(value As Decimal)
            _impuesto_ieps = value
        End Set
    End Property

    Public Property Tasa_ivaret As Decimal
        Get
            Return _tasa_ivaret
        End Get
        Set(value As Decimal)
            _tasa_ivaret = value
        End Set
    End Property

    Public Property Impuesto_ivaret As Decimal
        Get
            Return _impuesto_ivaret
        End Get
        Set(value As Decimal)
            _impuesto_ivaret = value
        End Set
    End Property

    Public Property Tasa_isrret As Decimal
        Get
            Return _tasa_isrret
        End Get
        Set(value As Decimal)
            _tasa_isrret = value
        End Set
    End Property

    Public Property Impuesto_isrret As Decimal
        Get
            Return _impuesto_isrret
        End Get
        Set(value As Decimal)
            _impuesto_isrret = value
        End Set
    End Property

    Public Property Tasa_otroimp As Decimal
        Get
            Return _tasa_otroimp
        End Get
        Set(value As Decimal)
            _tasa_otroimp = value
        End Set
    End Property

    Public Property Impuesto_otroimp As Decimal
        Get
            Return _impuesto_otroimp
        End Get
        Set(value As Decimal)
            _impuesto_otroimp = value
        End Set
    End Property

    Public Property Costopromart As Decimal
        Get
            Return _costopromart
        End Get
        Set(value As Decimal)
            _costopromart = value
        End Set
    End Property

    Public Property Renglon1 As Integer
        Get
            Return _renglon1
        End Get
        Set(value As Integer)
            _renglon1 = value
        End Set
    End Property

    Public Property Iva_impuesto As String
        Get
            Return _iva_impuesto
        End Get
        Set(value As String)
            _iva_impuesto = value
        End Set
    End Property

    Public Property Iva_tipofactor As String
        Get
            Return _iva_tipofactor
        End Get
        Set(value As String)
            _iva_tipofactor = value
        End Set
    End Property

    Public Property Objetoimp As String
        Get
            Return _objetoimp
        End Get
        Set(value As String)
            _objetoimp = value
        End Set
    End Property

    Public Property Claveunidad As String
        Get
            Return _claveunidad
        End Get
        Set(value As String)
            _claveunidad = value
        End Set
    End Property

    Public Property Claveunidaddesc As String
        Get
            Return _claveunidaddesc
        End Get
        Set(value As String)
            _claveunidaddesc = value
        End Set
    End Property
End Class
