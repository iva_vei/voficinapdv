﻿Public Class entPagare
    Dim _Empresa As String
    Dim _EmpDireccion As String
    Dim _Sucursal As String
    Dim _SucDireccion As String
    Dim _Cliente As String
    Dim _CliDireccion As String
    Dim _Autonumsuc As String
    Dim _Cajero As String
    Dim _Caja As String
    Dim _Fecha As String
    Dim _Mensaje As String

    Public Property Empresa As String
        Get
            Return _Empresa
        End Get
        Set(value As String)
            _Empresa = value
        End Set
    End Property

    Public Property EmpDireccion As String
        Get
            Return _EmpDireccion
        End Get
        Set(value As String)
            _EmpDireccion = value
        End Set
    End Property

    Public Property Sucursal As String
        Get
            Return _Sucursal
        End Get
        Set(value As String)
            _Sucursal = value
        End Set
    End Property

    Public Property SucDireccion As String
        Get
            Return _SucDireccion
        End Get
        Set(value As String)
            _SucDireccion = value
        End Set
    End Property

    Public Property Cliente As String
        Get
            Return _Cliente
        End Get
        Set(value As String)
            _Cliente = value
        End Set
    End Property

    Public Property CliDireccion As String
        Get
            Return _CliDireccion
        End Get
        Set(value As String)
            _CliDireccion = value
        End Set
    End Property

    Public Property Autonumsuc As String
        Get
            Return _Autonumsuc
        End Get
        Set(value As String)
            _Autonumsuc = value
        End Set
    End Property

    Public Property Cajero As String
        Get
            Return _Cajero
        End Get
        Set(value As String)
            _Cajero = value
        End Set
    End Property

    Public Property Caja As String
        Get
            Return _Caja
        End Get
        Set(value As String)
            _Caja = value
        End Set
    End Property

    Public Property Fecha As String
        Get
            Return _Fecha
        End Get
        Set(value As String)
            _Fecha = value
        End Set
    End Property

    Public Property Mensaje As String
        Get
            Return _Mensaje
        End Get
        Set(value As String)
            _Mensaje = value
        End Set
    End Property
End Class
