﻿Public Class entPreciador
    Dim _Renlones As List(Of entPreciadorRen)
    Public Sub New()
        _Renlones = New List(Of entPreciadorRen)
    End Sub
    Public Property Renlones As List(Of entPreciadorRen)
        Get
            Return _Renlones
        End Get
        Set(value As List(Of entPreciadorRen))
            _Renlones = value
        End Set
    End Property
End Class
Public Class entPreciadorRen
    Dim _ColumnaA As Preciador
    Dim _ColumnaB As Preciador
    Public Sub New()
        _ColumnaA = New Preciador
        _ColumnaB = New Preciador
    End Sub
    Public Property ColumnaA As Preciador
        Get
            Return _ColumnaA
        End Get
        Set(value As Preciador)
            _ColumnaA = value
        End Set
    End Property

    Public Property ColumnaB As Preciador
        Get
            Return _ColumnaB
        End Get
        Set(value As Preciador)
            _ColumnaB = value
        End Set
    End Property
End Class

Public Class entPreciador3
    Dim _Renlones As List(Of entPreciadorRen3)
    Public Sub New()
        _Renlones = New List(Of entPreciadorRen3)
    End Sub
    Public Property Renlones As List(Of entPreciadorRen3)
        Get
            Return _Renlones
        End Get
        Set(value As List(Of entPreciadorRen3))
            _Renlones = value
        End Set
    End Property
End Class
Public Class entPreciadorRen3
    Dim _ColumnaA As Preciador
    Dim _ColumnaB As Preciador
    Dim _ColumnaC As Preciador
    Public Sub New()
        _ColumnaA = New Preciador
        _ColumnaB = New Preciador
        _ColumnaC = New Preciador
    End Sub
    Public Property ColumnaA As Preciador
        Get
            Return _ColumnaA
        End Get
        Set(value As Preciador)
            _ColumnaA = value
        End Set
    End Property

    Public Property ColumnaB As Preciador
        Get
            Return _ColumnaB
        End Get
        Set(value As Preciador)
            _ColumnaB = value
        End Set
    End Property

    Public Property ColumnaC As Preciador
        Get
            Return _ColumnaC
        End Get
        Set(value As Preciador)
            _ColumnaC = value
        End Set
    End Property
End Class


Public Class entPreciador4
    Dim _Renlones As List(Of entPreciadorRen4)
    Public Sub New()
        _Renlones = New List(Of entPreciadorRen4)
    End Sub
    Public Property Renlones As List(Of entPreciadorRen4)
        Get
            Return _Renlones
        End Get
        Set(value As List(Of entPreciadorRen4))
            _Renlones = value
        End Set
    End Property
End Class
Public Class entPreciadorRen4
    Dim _ColumnaA As Preciador
    Dim _ColumnaB As Preciador
    Dim _ColumnaC As Preciador
    Dim _ColumnaD As Preciador
    Public Sub New()
        _ColumnaA = New Preciador
        _ColumnaB = New Preciador
        _ColumnaC = New Preciador
        _ColumnaD = New Preciador
    End Sub
    Public Property ColumnaA As Preciador
        Get
            Return _ColumnaA
        End Get
        Set(value As Preciador)
            _ColumnaA = value
        End Set
    End Property

    Public Property ColumnaB As Preciador
        Get
            Return _ColumnaB
        End Get
        Set(value As Preciador)
            _ColumnaB = value
        End Set
    End Property

    Public Property ColumnaC As Preciador
        Get
            Return _ColumnaC
        End Get
        Set(value As Preciador)
            _ColumnaC = value
        End Set
    End Property
    Public Property ColumnaD As Preciador
        Get
            Return _ColumnaD
        End Get
        Set(value As Preciador)
            _ColumnaD = value
        End Set
    End Property
End Class



Public Class Preciador
    Dim _r1Articulo As String
    Dim _r1CodigoBarras As String
    Dim _r1Marca As String
    Dim _r1Estilo As String
    Dim _r1Color As String
    Dim _r1Estructura As String
    Dim _r1Precio1 As String
    Dim _r1Precio2 As String
    Dim _r1Precio3 As String
    Dim _r1Precio4 As String
    Dim _r1Precio5 As String
    Dim _r1Oferta1 As String
    Dim _r1Oferta2 As String
    Dim _r1Oferta3 As String
    Dim _r1Oferta4 As String
    Dim _r1Oferta5 As String
    Dim _r1Titulo1 As String
    Dim _r1Titulo2 As String
    Dim _r1Titulo3 As String
    Dim _r1Titulo4 As String
    Dim _r1Titulo5 As String
    Dim _r1sw_mostar As Boolean

    Dim _r2Articulo As String
    Dim _r2CodigoBarras As String
    Dim _r2Marca As String
    Dim _r2Estilo As String
    Dim _r2Color As String
    Dim _r2Estructura As String
    Dim _r2Precio1 As String
    Dim _r2Precio2 As String
    Dim _r2Precio3 As String
    Dim _r2Precio4 As String
    Dim _r2Precio5 As String
    Dim _r2Oferta1 As String
    Dim _r2Oferta2 As String
    Dim _r2Oferta3 As String
    Dim _r2Oferta4 As String
    Dim _r2Oferta5 As String
    Dim _r2Titulo1 As String
    Dim _r2Titulo2 As String
    Dim _r2Titulo3 As String
    Dim _r2Titulo4 As String
    Dim _r2Titulo5 As String
    Dim _r2sw_mostar As Boolean

    Dim _r3Articulo As String
    Dim _r3CodigoBarras As String
    Dim _r3Marca As String
    Dim _r3Estilo As String
    Dim _r3Color As String
    Dim _r3Estructura As String
    Dim _r3Precio1 As String
    Dim _r3Precio2 As String
    Dim _r3Precio3 As String
    Dim _r3Precio4 As String
    Dim _r3Precio5 As String
    Dim _r3Oferta1 As String
    Dim _r3Oferta2 As String
    Dim _r3Oferta3 As String
    Dim _r3Oferta4 As String
    Dim _r3Oferta5 As String
    Dim _r3Titulo1 As String
    Dim _r3Titulo2 As String
    Dim _r3Titulo3 As String
    Dim _r3Titulo4 As String
    Dim _r3Titulo5 As String
    Dim _r3sw_mostar As Boolean

    Dim _r4Articulo As String
    Dim _r4CodigoBarras As String
    Dim _r4Marca As String
    Dim _r4Estilo As String
    Dim _r4Color As String
    Dim _r4Estructura As String
    Dim _r4Precio1 As String
    Dim _r4Precio2 As String
    Dim _r4Precio3 As String
    Dim _r4Precio4 As String
    Dim _r4Precio5 As String
    Dim _r4Oferta1 As String
    Dim _r4Oferta2 As String
    Dim _r4Oferta3 As String
    Dim _r4Oferta4 As String
    Dim _r4Oferta5 As String
    Dim _r4Titulo1 As String
    Dim _r4Titulo2 As String
    Dim _r4Titulo3 As String
    Dim _r4Titulo4 As String
    Dim _r4Titulo5 As String
    Dim _r4sw_mostar As Boolean

    Dim _r5Articulo As String
    Dim _r5CodigoBarras As String
    Dim _r5Marca As String
    Dim _r5Estilo As String
    Dim _r5Color As String
    Dim _r5Estructura As String
    Dim _r5Precio1 As String
    Dim _r5Precio2 As String
    Dim _r5Precio3 As String
    Dim _r5Precio4 As String
    Dim _r5Precio5 As String
    Dim _r5Oferta1 As String
    Dim _r5Oferta2 As String
    Dim _r5Oferta3 As String
    Dim _r5Oferta4 As String
    Dim _r5Oferta5 As String
    Dim _r5Titulo1 As String
    Dim _r5Titulo2 As String
    Dim _r5Titulo3 As String
    Dim _r5Titulo4 As String
    Dim _r5Titulo5 As String

    Dim _r6Articulo As String
    Dim _r6CodigoBarras As String
    Dim _r6Marca As String
    Dim _r6Estilo As String
    Dim _r6Color As String
    Dim _r6Estructura As String
    Dim _r6Precio1 As String
    Dim _r6Precio2 As String
    Dim _r6Precio3 As String
    Dim _r6Precio4 As String
    Dim _r6Precio5 As String
    Dim _r6Oferta1 As String
    Dim _r6Oferta2 As String
    Dim _r6Oferta3 As String
    Dim _r6Oferta4 As String
    Dim _r6Oferta5 As String
    Dim _r6Titulo1 As String
    Dim _r6Titulo2 As String
    Dim _r6Titulo3 As String
    Dim _r6Titulo4 As String
    Dim _r6Titulo5 As String
    Dim _r6sw_mostar As Boolean

    Dim _r7Articulo As String
    Dim _r7CodigoBarras As String
    Dim _r7Marca As String
    Dim _r7Estilo As String
    Dim _r7Color As String
    Dim _r7Estructura As String
    Dim _r7Precio1 As String
    Dim _r7Precio2 As String
    Dim _r7Precio3 As String
    Dim _r7Precio4 As String
    Dim _r7Precio5 As String
    Dim _r7Oferta1 As String
    Dim _r7Oferta2 As String
    Dim _r7Oferta3 As String
    Dim _r7Oferta4 As String
    Dim _r7Oferta5 As String
    Dim _r7Titulo1 As String
    Dim _r7Titulo2 As String
    Dim _r7Titulo3 As String
    Dim _r7Titulo4 As String
    Dim _r7Titulo5 As String
    Dim _r7sw_mostar As Boolean

    Dim _r8Articulo As String
    Dim _r8CodigoBarras As String
    Dim _r8Marca As String
    Dim _r8Estilo As String
    Dim _r8Color As String
    Dim _r8Estructura As String
    Dim _r8Precio1 As String
    Dim _r8Precio2 As String
    Dim _r8Precio3 As String
    Dim _r8Precio4 As String
    Dim _r8Precio5 As String
    Dim _r8Oferta1 As String
    Dim _r8Oferta2 As String
    Dim _r8Oferta3 As String
    Dim _r8Oferta4 As String
    Dim _r8Oferta5 As String
    Dim _r8Titulo1 As String
    Dim _r8Titulo2 As String
    Dim _r8Titulo3 As String
    Dim _r8Titulo4 As String
    Dim _r8Titulo5 As String
    Dim _r8sw_mostar As Boolean

    Dim _r9Articulo As String
    Dim _r9CodigoBarras As String
    Dim _r9Marca As String
    Dim _r9Estilo As String
    Dim _r9Color As String
    Dim _r9Estructura As String
    Dim _r9Precio1 As String
    Dim _r9Precio2 As String
    Dim _r9Precio3 As String
    Dim _r9Precio4 As String
    Dim _r9Precio5 As String
    Dim _r9Oferta1 As String
    Dim _r9Oferta2 As String
    Dim _r9Oferta3 As String
    Dim _r9Oferta4 As String
    Dim _r9Oferta5 As String
    Dim _r9Titulo1 As String
    Dim _r9Titulo2 As String
    Dim _r9Titulo3 As String
    Dim _r9Titulo4 As String
    Dim _r9Titulo5 As String
    Dim _r9sw_mostar As Boolean

    Dim _r1Caracteristicas As String
    Dim _r2Caracteristicas As String
    Dim _r3Caracteristicas As String
    Dim _r4Caracteristicas As String
    Dim _r5Caracteristicas As String
    Dim _r6Caracteristicas As String
    Dim _r7Caracteristicas As String
    Dim _r8Caracteristicas As String
    Dim _r9Caracteristicas As String
    Public Property R1Articulo As String
        Get
            Return _r1Articulo
        End Get
        Set(value As String)
            _r1Articulo = value
        End Set
    End Property

    Public Property R1CodigoBarras As String
        Get
            Return _r1CodigoBarras
        End Get
        Set(value As String)
            _r1CodigoBarras = value
        End Set
    End Property

    Public Property R1Marca As String
        Get
            Return _r1Marca
        End Get
        Set(value As String)
            _r1Marca = value
        End Set
    End Property

    Public Property R1Estilo As String
        Get
            Return _r1Estilo
        End Get
        Set(value As String)
            _r1Estilo = value
        End Set
    End Property

    Public Property R1Color As String
        Get
            Return _r1Color
        End Get
        Set(value As String)
            _r1Color = value
        End Set
    End Property

    Public Property R1Estructura As String
        Get
            Return _r1Estructura
        End Get
        Set(value As String)
            _r1Estructura = value
        End Set
    End Property

    Public Property R1Precio1 As String
        Get
            Return _r1Precio1
        End Get
        Set(value As String)
            _r1Precio1 = value
        End Set
    End Property

    Public Property R1Precio2 As String
        Get
            Return _r1Precio2
        End Get
        Set(value As String)
            _r1Precio2 = value
        End Set
    End Property

    Public Property R1Precio3 As String
        Get
            Return _r1Precio3
        End Get
        Set(value As String)
            _r1Precio3 = value
        End Set
    End Property

    Public Property R1Precio4 As String
        Get
            Return _r1Precio4
        End Get
        Set(value As String)
            _r1Precio4 = value
        End Set
    End Property

    Public Property R1Precio5 As String
        Get
            Return _r1Precio5
        End Get
        Set(value As String)
            _r1Precio5 = value
        End Set
    End Property

    Public Property R1Oferta1 As String
        Get
            Return _r1Oferta1
        End Get
        Set(value As String)
            _r1Oferta1 = value
        End Set
    End Property

    Public Property R1Oferta2 As String
        Get
            Return _r1Oferta2
        End Get
        Set(value As String)
            _r1Oferta2 = value
        End Set
    End Property

    Public Property R1Oferta3 As String
        Get
            Return _r1Oferta3
        End Get
        Set(value As String)
            _r1Oferta3 = value
        End Set
    End Property

    Public Property R1Oferta4 As String
        Get
            Return _r1Oferta4
        End Get
        Set(value As String)
            _r1Oferta4 = value
        End Set
    End Property

    Public Property R1Oferta5 As String
        Get
            Return _r1Oferta5
        End Get
        Set(value As String)
            _r1Oferta5 = value
        End Set
    End Property

    Public Property R1Titulo1 As String
        Get
            Return _r1Titulo1
        End Get
        Set(value As String)
            _r1Titulo1 = value
        End Set
    End Property

    Public Property R1Titulo2 As String
        Get
            Return _r1Titulo2
        End Get
        Set(value As String)
            _r1Titulo2 = value
        End Set
    End Property

    Public Property R1Titulo3 As String
        Get
            Return _r1Titulo3
        End Get
        Set(value As String)
            _r1Titulo3 = value
        End Set
    End Property

    Public Property R1Titulo4 As String
        Get
            Return _r1Titulo4
        End Get
        Set(value As String)
            _r1Titulo4 = value
        End Set
    End Property

    Public Property R1Titulo5 As String
        Get
            Return _r1Titulo5
        End Get
        Set(value As String)
            _r1Titulo5 = value
        End Set
    End Property

    Public Property R1sw_mostar As Boolean
        Get
            Return _r1sw_mostar
        End Get
        Set(value As Boolean)
            _r1sw_mostar = value
        End Set
    End Property

    Public Property R2Articulo As String
        Get
            Return _r2Articulo
        End Get
        Set(value As String)
            _r2Articulo = value
        End Set
    End Property

    Public Property R2CodigoBarras As String
        Get
            Return _r2CodigoBarras
        End Get
        Set(value As String)
            _r2CodigoBarras = value
        End Set
    End Property

    Public Property R2Marca As String
        Get
            Return _r2Marca
        End Get
        Set(value As String)
            _r2Marca = value
        End Set
    End Property

    Public Property R2Estilo As String
        Get
            Return _r2Estilo
        End Get
        Set(value As String)
            _r2Estilo = value
        End Set
    End Property

    Public Property R2Color As String
        Get
            Return _r2Color
        End Get
        Set(value As String)
            _r2Color = value
        End Set
    End Property

    Public Property R2Estructura As String
        Get
            Return _r2Estructura
        End Get
        Set(value As String)
            _r2Estructura = value
        End Set
    End Property

    Public Property R2Precio1 As String
        Get
            Return _r2Precio1
        End Get
        Set(value As String)
            _r2Precio1 = value
        End Set
    End Property

    Public Property R2Precio2 As String
        Get
            Return _r2Precio2
        End Get
        Set(value As String)
            _r2Precio2 = value
        End Set
    End Property

    Public Property R2Precio3 As String
        Get
            Return _r2Precio3
        End Get
        Set(value As String)
            _r2Precio3 = value
        End Set
    End Property

    Public Property R2Precio4 As String
        Get
            Return _r2Precio4
        End Get
        Set(value As String)
            _r2Precio4 = value
        End Set
    End Property

    Public Property R2Precio5 As String
        Get
            Return _r2Precio5
        End Get
        Set(value As String)
            _r2Precio5 = value
        End Set
    End Property

    Public Property R2Oferta1 As String
        Get
            Return _r2Oferta1
        End Get
        Set(value As String)
            _r2Oferta1 = value
        End Set
    End Property

    Public Property R2Oferta2 As String
        Get
            Return _r2Oferta2
        End Get
        Set(value As String)
            _r2Oferta2 = value
        End Set
    End Property

    Public Property R2Oferta3 As String
        Get
            Return _r2Oferta3
        End Get
        Set(value As String)
            _r2Oferta3 = value
        End Set
    End Property

    Public Property R2Oferta4 As String
        Get
            Return _r2Oferta4
        End Get
        Set(value As String)
            _r2Oferta4 = value
        End Set
    End Property

    Public Property R2Oferta5 As String
        Get
            Return _r2Oferta5
        End Get
        Set(value As String)
            _r2Oferta5 = value
        End Set
    End Property

    Public Property R2Titulo1 As String
        Get
            Return _r2Titulo1
        End Get
        Set(value As String)
            _r2Titulo1 = value
        End Set
    End Property

    Public Property R2Titulo2 As String
        Get
            Return _r2Titulo2
        End Get
        Set(value As String)
            _r2Titulo2 = value
        End Set
    End Property

    Public Property R2Titulo3 As String
        Get
            Return _r2Titulo3
        End Get
        Set(value As String)
            _r2Titulo3 = value
        End Set
    End Property

    Public Property R2Titulo4 As String
        Get
            Return _r2Titulo4
        End Get
        Set(value As String)
            _r2Titulo4 = value
        End Set
    End Property

    Public Property R2Titulo5 As String
        Get
            Return _r2Titulo5
        End Get
        Set(value As String)
            _r2Titulo5 = value
        End Set
    End Property

    Public Property R2sw_mostar As Boolean
        Get
            Return _r2sw_mostar
        End Get
        Set(value As Boolean)
            _r2sw_mostar = value
        End Set
    End Property

    Public Property R3Articulo As String
        Get
            Return _r3Articulo
        End Get
        Set(value As String)
            _r3Articulo = value
        End Set
    End Property

    Public Property R3CodigoBarras As String
        Get
            Return _r3CodigoBarras
        End Get
        Set(value As String)
            _r3CodigoBarras = value
        End Set
    End Property

    Public Property R3Marca As String
        Get
            Return _r3Marca
        End Get
        Set(value As String)
            _r3Marca = value
        End Set
    End Property

    Public Property R3Estilo As String
        Get
            Return _r3Estilo
        End Get
        Set(value As String)
            _r3Estilo = value
        End Set
    End Property

    Public Property R3Color As String
        Get
            Return _r3Color
        End Get
        Set(value As String)
            _r3Color = value
        End Set
    End Property

    Public Property R3Estructura As String
        Get
            Return _r3Estructura
        End Get
        Set(value As String)
            _r3Estructura = value
        End Set
    End Property

    Public Property R3Precio1 As String
        Get
            Return _r3Precio1
        End Get
        Set(value As String)
            _r3Precio1 = value
        End Set
    End Property

    Public Property R3Precio2 As String
        Get
            Return _r3Precio2
        End Get
        Set(value As String)
            _r3Precio2 = value
        End Set
    End Property

    Public Property R3Precio3 As String
        Get
            Return _r3Precio3
        End Get
        Set(value As String)
            _r3Precio3 = value
        End Set
    End Property

    Public Property R3Precio4 As String
        Get
            Return _r3Precio4
        End Get
        Set(value As String)
            _r3Precio4 = value
        End Set
    End Property

    Public Property R3Precio5 As String
        Get
            Return _r3Precio5
        End Get
        Set(value As String)
            _r3Precio5 = value
        End Set
    End Property

    Public Property R3Oferta1 As String
        Get
            Return _r3Oferta1
        End Get
        Set(value As String)
            _r3Oferta1 = value
        End Set
    End Property

    Public Property R3Oferta2 As String
        Get
            Return _r3Oferta2
        End Get
        Set(value As String)
            _r3Oferta2 = value
        End Set
    End Property

    Public Property R3Oferta3 As String
        Get
            Return _r3Oferta3
        End Get
        Set(value As String)
            _r3Oferta3 = value
        End Set
    End Property

    Public Property R3Oferta4 As String
        Get
            Return _r3Oferta4
        End Get
        Set(value As String)
            _r3Oferta4 = value
        End Set
    End Property

    Public Property R3Oferta5 As String
        Get
            Return _r3Oferta5
        End Get
        Set(value As String)
            _r3Oferta5 = value
        End Set
    End Property

    Public Property R3Titulo1 As String
        Get
            Return _r3Titulo1
        End Get
        Set(value As String)
            _r3Titulo1 = value
        End Set
    End Property

    Public Property R3Titulo2 As String
        Get
            Return _r3Titulo2
        End Get
        Set(value As String)
            _r3Titulo2 = value
        End Set
    End Property

    Public Property R3Titulo3 As String
        Get
            Return _r3Titulo3
        End Get
        Set(value As String)
            _r3Titulo3 = value
        End Set
    End Property

    Public Property R3Titulo4 As String
        Get
            Return _r3Titulo4
        End Get
        Set(value As String)
            _r3Titulo4 = value
        End Set
    End Property

    Public Property R3Titulo5 As String
        Get
            Return _r3Titulo5
        End Get
        Set(value As String)
            _r3Titulo5 = value
        End Set
    End Property

    Public Property R3sw_mostar As Boolean
        Get
            Return _r3sw_mostar
        End Get
        Set(value As Boolean)
            _r3sw_mostar = value
        End Set
    End Property

    Public Property R4Articulo As String
        Get
            Return _r4Articulo
        End Get
        Set(value As String)
            _r4Articulo = value
        End Set
    End Property

    Public Property R4CodigoBarras As String
        Get
            Return _r4CodigoBarras
        End Get
        Set(value As String)
            _r4CodigoBarras = value
        End Set
    End Property

    Public Property R4Marca As String
        Get
            Return _r4Marca
        End Get
        Set(value As String)
            _r4Marca = value
        End Set
    End Property

    Public Property R4Estilo As String
        Get
            Return _r4Estilo
        End Get
        Set(value As String)
            _r4Estilo = value
        End Set
    End Property

    Public Property R4Color As String
        Get
            Return _r4Color
        End Get
        Set(value As String)
            _r4Color = value
        End Set
    End Property

    Public Property R4Estructura As String
        Get
            Return _r4Estructura
        End Get
        Set(value As String)
            _r4Estructura = value
        End Set
    End Property

    Public Property R4Precio1 As String
        Get
            Return _r4Precio1
        End Get
        Set(value As String)
            _r4Precio1 = value
        End Set
    End Property

    Public Property R4Precio2 As String
        Get
            Return _r4Precio2
        End Get
        Set(value As String)
            _r4Precio2 = value
        End Set
    End Property

    Public Property R4Precio3 As String
        Get
            Return _r4Precio3
        End Get
        Set(value As String)
            _r4Precio3 = value
        End Set
    End Property

    Public Property R4Precio4 As String
        Get
            Return _r4Precio4
        End Get
        Set(value As String)
            _r4Precio4 = value
        End Set
    End Property

    Public Property R4Precio5 As String
        Get
            Return _r4Precio5
        End Get
        Set(value As String)
            _r4Precio5 = value
        End Set
    End Property

    Public Property R4Oferta1 As String
        Get
            Return _r4Oferta1
        End Get
        Set(value As String)
            _r4Oferta1 = value
        End Set
    End Property

    Public Property R4Oferta2 As String
        Get
            Return _r4Oferta2
        End Get
        Set(value As String)
            _r4Oferta2 = value
        End Set
    End Property

    Public Property R4Oferta3 As String
        Get
            Return _r4Oferta3
        End Get
        Set(value As String)
            _r4Oferta3 = value
        End Set
    End Property

    Public Property R4Oferta4 As String
        Get
            Return _r4Oferta4
        End Get
        Set(value As String)
            _r4Oferta4 = value
        End Set
    End Property

    Public Property R4Oferta5 As String
        Get
            Return _r4Oferta5
        End Get
        Set(value As String)
            _r4Oferta5 = value
        End Set
    End Property

    Public Property R4Titulo1 As String
        Get
            Return _r4Titulo1
        End Get
        Set(value As String)
            _r4Titulo1 = value
        End Set
    End Property

    Public Property R4Titulo2 As String
        Get
            Return _r4Titulo2
        End Get
        Set(value As String)
            _r4Titulo2 = value
        End Set
    End Property

    Public Property R4Titulo3 As String
        Get
            Return _r4Titulo3
        End Get
        Set(value As String)
            _r4Titulo3 = value
        End Set
    End Property

    Public Property R4Titulo4 As String
        Get
            Return _r4Titulo4
        End Get
        Set(value As String)
            _r4Titulo4 = value
        End Set
    End Property

    Public Property R4Titulo5 As String
        Get
            Return _r4Titulo5
        End Get
        Set(value As String)
            _r4Titulo5 = value
        End Set
    End Property

    Public Property R4sw_mostar As Boolean
        Get
            Return _r4sw_mostar
        End Get
        Set(value As Boolean)
            _r4sw_mostar = value
        End Set
    End Property

    Public Property R5Articulo As String
        Get
            Return _r5Articulo
        End Get
        Set(value As String)
            _r5Articulo = value
        End Set
    End Property

    Public Property R5CodigoBarras As String
        Get
            Return _r5CodigoBarras
        End Get
        Set(value As String)
            _r5CodigoBarras = value
        End Set
    End Property

    Public Property R5Marca As String
        Get
            Return _r5Marca
        End Get
        Set(value As String)
            _r5Marca = value
        End Set
    End Property

    Public Property R5Estilo As String
        Get
            Return _r5Estilo
        End Get
        Set(value As String)
            _r5Estilo = value
        End Set
    End Property

    Public Property R5Color As String
        Get
            Return _r5Color
        End Get
        Set(value As String)
            _r5Color = value
        End Set
    End Property

    Public Property R5Estructura As String
        Get
            Return _r5Estructura
        End Get
        Set(value As String)
            _r5Estructura = value
        End Set
    End Property

    Public Property R5Precio1 As String
        Get
            Return _r5Precio1
        End Get
        Set(value As String)
            _r5Precio1 = value
        End Set
    End Property

    Public Property R5Precio2 As String
        Get
            Return _r5Precio2
        End Get
        Set(value As String)
            _r5Precio2 = value
        End Set
    End Property

    Public Property R5Precio3 As String
        Get
            Return _r5Precio3
        End Get
        Set(value As String)
            _r5Precio3 = value
        End Set
    End Property

    Public Property R5Precio4 As String
        Get
            Return _r5Precio4
        End Get
        Set(value As String)
            _r5Precio4 = value
        End Set
    End Property

    Public Property R5Precio5 As String
        Get
            Return _r5Precio5
        End Get
        Set(value As String)
            _r5Precio5 = value
        End Set
    End Property

    Public Property R5Oferta1 As String
        Get
            Return _r5Oferta1
        End Get
        Set(value As String)
            _r5Oferta1 = value
        End Set
    End Property

    Public Property R5Oferta2 As String
        Get
            Return _r5Oferta2
        End Get
        Set(value As String)
            _r5Oferta2 = value
        End Set
    End Property

    Public Property R5Oferta3 As String
        Get
            Return _r5Oferta3
        End Get
        Set(value As String)
            _r5Oferta3 = value
        End Set
    End Property

    Public Property R5Oferta4 As String
        Get
            Return _r5Oferta4
        End Get
        Set(value As String)
            _r5Oferta4 = value
        End Set
    End Property

    Public Property R5Oferta5 As String
        Get
            Return _r5Oferta5
        End Get
        Set(value As String)
            _r5Oferta5 = value
        End Set
    End Property

    Public Property R5Titulo1 As String
        Get
            Return _r5Titulo1
        End Get
        Set(value As String)
            _r5Titulo1 = value
        End Set
    End Property

    Public Property R5Titulo2 As String
        Get
            Return _r5Titulo2
        End Get
        Set(value As String)
            _r5Titulo2 = value
        End Set
    End Property

    Public Property R5Titulo3 As String
        Get
            Return _r5Titulo3
        End Get
        Set(value As String)
            _r5Titulo3 = value
        End Set
    End Property

    Public Property R5Titulo4 As String
        Get
            Return _r5Titulo4
        End Get
        Set(value As String)
            _r5Titulo4 = value
        End Set
    End Property

    Public Property R5Titulo5 As String
        Get
            Return _r5Titulo5
        End Get
        Set(value As String)
            _r5Titulo5 = value
        End Set
    End Property

    Public Property R6Articulo As String
        Get
            Return _r6Articulo
        End Get
        Set(value As String)
            _r6Articulo = value
        End Set
    End Property

    Public Property R6CodigoBarras As String
        Get
            Return _r6CodigoBarras
        End Get
        Set(value As String)
            _r6CodigoBarras = value
        End Set
    End Property

    Public Property R6Marca As String
        Get
            Return _r6Marca
        End Get
        Set(value As String)
            _r6Marca = value
        End Set
    End Property

    Public Property R6Estilo As String
        Get
            Return _r6Estilo
        End Get
        Set(value As String)
            _r6Estilo = value
        End Set
    End Property

    Public Property R6Color As String
        Get
            Return _r6Color
        End Get
        Set(value As String)
            _r6Color = value
        End Set
    End Property

    Public Property R6Estructura As String
        Get
            Return _r6Estructura
        End Get
        Set(value As String)
            _r6Estructura = value
        End Set
    End Property

    Public Property R6Precio1 As String
        Get
            Return _r6Precio1
        End Get
        Set(value As String)
            _r6Precio1 = value
        End Set
    End Property

    Public Property R6Precio2 As String
        Get
            Return _r6Precio2
        End Get
        Set(value As String)
            _r6Precio2 = value
        End Set
    End Property

    Public Property R6Precio3 As String
        Get
            Return _r6Precio3
        End Get
        Set(value As String)
            _r6Precio3 = value
        End Set
    End Property

    Public Property R6Precio4 As String
        Get
            Return _r6Precio4
        End Get
        Set(value As String)
            _r6Precio4 = value
        End Set
    End Property

    Public Property R6Precio5 As String
        Get
            Return _r6Precio5
        End Get
        Set(value As String)
            _r6Precio5 = value
        End Set
    End Property

    Public Property R6Oferta1 As String
        Get
            Return _r6Oferta1
        End Get
        Set(value As String)
            _r6Oferta1 = value
        End Set
    End Property

    Public Property R6Oferta2 As String
        Get
            Return _r6Oferta2
        End Get
        Set(value As String)
            _r6Oferta2 = value
        End Set
    End Property

    Public Property R6Oferta3 As String
        Get
            Return _r6Oferta3
        End Get
        Set(value As String)
            _r6Oferta3 = value
        End Set
    End Property

    Public Property R6Oferta4 As String
        Get
            Return _r6Oferta4
        End Get
        Set(value As String)
            _r6Oferta4 = value
        End Set
    End Property

    Public Property R6Oferta5 As String
        Get
            Return _r6Oferta5
        End Get
        Set(value As String)
            _r6Oferta5 = value
        End Set
    End Property

    Public Property R6Titulo1 As String
        Get
            Return _r6Titulo1
        End Get
        Set(value As String)
            _r6Titulo1 = value
        End Set
    End Property

    Public Property R6Titulo2 As String
        Get
            Return _r6Titulo2
        End Get
        Set(value As String)
            _r6Titulo2 = value
        End Set
    End Property

    Public Property R6Titulo3 As String
        Get
            Return _r6Titulo3
        End Get
        Set(value As String)
            _r6Titulo3 = value
        End Set
    End Property

    Public Property R6Titulo4 As String
        Get
            Return _r6Titulo4
        End Get
        Set(value As String)
            _r6Titulo4 = value
        End Set
    End Property

    Public Property R6Titulo5 As String
        Get
            Return _r6Titulo5
        End Get
        Set(value As String)
            _r6Titulo5 = value
        End Set
    End Property

    Public Property R6sw_mostar As Boolean
        Get
            Return _r6sw_mostar
        End Get
        Set(value As Boolean)
            _r6sw_mostar = value
        End Set
    End Property

    Public Property R7Articulo As String
        Get
            Return _r7Articulo
        End Get
        Set(value As String)
            _r7Articulo = value
        End Set
    End Property

    Public Property R7CodigoBarras As String
        Get
            Return _r7CodigoBarras
        End Get
        Set(value As String)
            _r7CodigoBarras = value
        End Set
    End Property

    Public Property R7Marca As String
        Get
            Return _r7Marca
        End Get
        Set(value As String)
            _r7Marca = value
        End Set
    End Property

    Public Property R7Estilo As String
        Get
            Return _r7Estilo
        End Get
        Set(value As String)
            _r7Estilo = value
        End Set
    End Property

    Public Property R7Color As String
        Get
            Return _r7Color
        End Get
        Set(value As String)
            _r7Color = value
        End Set
    End Property

    Public Property R7Estructura As String
        Get
            Return _r7Estructura
        End Get
        Set(value As String)
            _r7Estructura = value
        End Set
    End Property

    Public Property R7Precio1 As String
        Get
            Return _r7Precio1
        End Get
        Set(value As String)
            _r7Precio1 = value
        End Set
    End Property

    Public Property R7Precio2 As String
        Get
            Return _r7Precio2
        End Get
        Set(value As String)
            _r7Precio2 = value
        End Set
    End Property

    Public Property R7Precio3 As String
        Get
            Return _r7Precio3
        End Get
        Set(value As String)
            _r7Precio3 = value
        End Set
    End Property

    Public Property R7Precio4 As String
        Get
            Return _r7Precio4
        End Get
        Set(value As String)
            _r7Precio4 = value
        End Set
    End Property

    Public Property R7Precio5 As String
        Get
            Return _r7Precio5
        End Get
        Set(value As String)
            _r7Precio5 = value
        End Set
    End Property

    Public Property R7Oferta1 As String
        Get
            Return _r7Oferta1
        End Get
        Set(value As String)
            _r7Oferta1 = value
        End Set
    End Property

    Public Property R7Oferta2 As String
        Get
            Return _r7Oferta2
        End Get
        Set(value As String)
            _r7Oferta2 = value
        End Set
    End Property

    Public Property R7Oferta3 As String
        Get
            Return _r7Oferta3
        End Get
        Set(value As String)
            _r7Oferta3 = value
        End Set
    End Property

    Public Property R7Oferta4 As String
        Get
            Return _r7Oferta4
        End Get
        Set(value As String)
            _r7Oferta4 = value
        End Set
    End Property

    Public Property R7Oferta5 As String
        Get
            Return _r7Oferta5
        End Get
        Set(value As String)
            _r7Oferta5 = value
        End Set
    End Property

    Public Property R7Titulo1 As String
        Get
            Return _r7Titulo1
        End Get
        Set(value As String)
            _r7Titulo1 = value
        End Set
    End Property

    Public Property R7Titulo2 As String
        Get
            Return _r7Titulo2
        End Get
        Set(value As String)
            _r7Titulo2 = value
        End Set
    End Property

    Public Property R7Titulo3 As String
        Get
            Return _r7Titulo3
        End Get
        Set(value As String)
            _r7Titulo3 = value
        End Set
    End Property

    Public Property R7Titulo4 As String
        Get
            Return _r7Titulo4
        End Get
        Set(value As String)
            _r7Titulo4 = value
        End Set
    End Property

    Public Property R7Titulo5 As String
        Get
            Return _r7Titulo5
        End Get
        Set(value As String)
            _r7Titulo5 = value
        End Set
    End Property

    Public Property R7sw_mostar As Boolean
        Get
            Return _r7sw_mostar
        End Get
        Set(value As Boolean)
            _r7sw_mostar = value
        End Set
    End Property

    Public Property R8Articulo As String
        Get
            Return _r8Articulo
        End Get
        Set(value As String)
            _r8Articulo = value
        End Set
    End Property

    Public Property R8CodigoBarras As String
        Get
            Return _r8CodigoBarras
        End Get
        Set(value As String)
            _r8CodigoBarras = value
        End Set
    End Property

    Public Property R8Marca As String
        Get
            Return _r8Marca
        End Get
        Set(value As String)
            _r8Marca = value
        End Set
    End Property

    Public Property R8Estilo As String
        Get
            Return _r8Estilo
        End Get
        Set(value As String)
            _r8Estilo = value
        End Set
    End Property

    Public Property R8Color As String
        Get
            Return _r8Color
        End Get
        Set(value As String)
            _r8Color = value
        End Set
    End Property

    Public Property R8Estructura As String
        Get
            Return _r8Estructura
        End Get
        Set(value As String)
            _r8Estructura = value
        End Set
    End Property

    Public Property R8Precio1 As String
        Get
            Return _r8Precio1
        End Get
        Set(value As String)
            _r8Precio1 = value
        End Set
    End Property

    Public Property R8Precio2 As String
        Get
            Return _r8Precio2
        End Get
        Set(value As String)
            _r8Precio2 = value
        End Set
    End Property

    Public Property R8Precio3 As String
        Get
            Return _r8Precio3
        End Get
        Set(value As String)
            _r8Precio3 = value
        End Set
    End Property

    Public Property R8Precio4 As String
        Get
            Return _r8Precio4
        End Get
        Set(value As String)
            _r8Precio4 = value
        End Set
    End Property

    Public Property R8Precio5 As String
        Get
            Return _r8Precio5
        End Get
        Set(value As String)
            _r8Precio5 = value
        End Set
    End Property

    Public Property R8Oferta1 As String
        Get
            Return _r8Oferta1
        End Get
        Set(value As String)
            _r8Oferta1 = value
        End Set
    End Property

    Public Property R8Oferta2 As String
        Get
            Return _r8Oferta2
        End Get
        Set(value As String)
            _r8Oferta2 = value
        End Set
    End Property

    Public Property R8Oferta3 As String
        Get
            Return _r8Oferta3
        End Get
        Set(value As String)
            _r8Oferta3 = value
        End Set
    End Property

    Public Property R8Oferta4 As String
        Get
            Return _r8Oferta4
        End Get
        Set(value As String)
            _r8Oferta4 = value
        End Set
    End Property

    Public Property R8Oferta5 As String
        Get
            Return _r8Oferta5
        End Get
        Set(value As String)
            _r8Oferta5 = value
        End Set
    End Property

    Public Property R8Titulo1 As String
        Get
            Return _r8Titulo1
        End Get
        Set(value As String)
            _r8Titulo1 = value
        End Set
    End Property

    Public Property R8Titulo2 As String
        Get
            Return _r8Titulo2
        End Get
        Set(value As String)
            _r8Titulo2 = value
        End Set
    End Property

    Public Property R8Titulo3 As String
        Get
            Return _r8Titulo3
        End Get
        Set(value As String)
            _r8Titulo3 = value
        End Set
    End Property

    Public Property R8Titulo4 As String
        Get
            Return _r8Titulo4
        End Get
        Set(value As String)
            _r8Titulo4 = value
        End Set
    End Property

    Public Property R8Titulo5 As String
        Get
            Return _r8Titulo5
        End Get
        Set(value As String)
            _r8Titulo5 = value
        End Set
    End Property

    Public Property R8sw_mostar As Boolean
        Get
            Return _r8sw_mostar
        End Get
        Set(value As Boolean)
            _r8sw_mostar = value
        End Set
    End Property

    Public Property R9Articulo As String
        Get
            Return _r9Articulo
        End Get
        Set(value As String)
            _r9Articulo = value
        End Set
    End Property

    Public Property R9CodigoBarras As String
        Get
            Return _r9CodigoBarras
        End Get
        Set(value As String)
            _r9CodigoBarras = value
        End Set
    End Property

    Public Property R9Marca As String
        Get
            Return _r9Marca
        End Get
        Set(value As String)
            _r9Marca = value
        End Set
    End Property

    Public Property R9Estilo As String
        Get
            Return _r9Estilo
        End Get
        Set(value As String)
            _r9Estilo = value
        End Set
    End Property

    Public Property R9Color As String
        Get
            Return _r9Color
        End Get
        Set(value As String)
            _r9Color = value
        End Set
    End Property

    Public Property R9Estructura As String
        Get
            Return _r9Estructura
        End Get
        Set(value As String)
            _r9Estructura = value
        End Set
    End Property

    Public Property R9Precio1 As String
        Get
            Return _r9Precio1
        End Get
        Set(value As String)
            _r9Precio1 = value
        End Set
    End Property

    Public Property R9Precio2 As String
        Get
            Return _r9Precio2
        End Get
        Set(value As String)
            _r9Precio2 = value
        End Set
    End Property

    Public Property R9Precio3 As String
        Get
            Return _r9Precio3
        End Get
        Set(value As String)
            _r9Precio3 = value
        End Set
    End Property

    Public Property R9Precio4 As String
        Get
            Return _r9Precio4
        End Get
        Set(value As String)
            _r9Precio4 = value
        End Set
    End Property

    Public Property R9Precio5 As String
        Get
            Return _r9Precio5
        End Get
        Set(value As String)
            _r9Precio5 = value
        End Set
    End Property

    Public Property R9Oferta1 As String
        Get
            Return _r9Oferta1
        End Get
        Set(value As String)
            _r9Oferta1 = value
        End Set
    End Property

    Public Property R9Oferta2 As String
        Get
            Return _r9Oferta2
        End Get
        Set(value As String)
            _r9Oferta2 = value
        End Set
    End Property

    Public Property R9Oferta3 As String
        Get
            Return _r9Oferta3
        End Get
        Set(value As String)
            _r9Oferta3 = value
        End Set
    End Property

    Public Property R9Oferta4 As String
        Get
            Return _r9Oferta4
        End Get
        Set(value As String)
            _r9Oferta4 = value
        End Set
    End Property

    Public Property R9Oferta5 As String
        Get
            Return _r9Oferta5
        End Get
        Set(value As String)
            _r9Oferta5 = value
        End Set
    End Property

    Public Property R9Titulo1 As String
        Get
            Return _r9Titulo1
        End Get
        Set(value As String)
            _r9Titulo1 = value
        End Set
    End Property

    Public Property R9Titulo2 As String
        Get
            Return _r9Titulo2
        End Get
        Set(value As String)
            _r9Titulo2 = value
        End Set
    End Property

    Public Property R9Titulo3 As String
        Get
            Return _r9Titulo3
        End Get
        Set(value As String)
            _r9Titulo3 = value
        End Set
    End Property

    Public Property R9Titulo4 As String
        Get
            Return _r9Titulo4
        End Get
        Set(value As String)
            _r9Titulo4 = value
        End Set
    End Property

    Public Property R9Titulo5 As String
        Get
            Return _r9Titulo5
        End Get
        Set(value As String)
            _r9Titulo5 = value
        End Set
    End Property

    Public Property R9sw_mostar As Boolean
        Get
            Return _r9sw_mostar
        End Get
        Set(value As Boolean)
            _r9sw_mostar = value
        End Set
    End Property

    Public Property R1Caracteristicas As String
        Get
            Return _r1Caracteristicas
        End Get
        Set(value As String)
            _r1Caracteristicas = value
        End Set
    End Property

    Public Property R2Caracteristicas As String
        Get
            Return _r2Caracteristicas
        End Get
        Set(value As String)
            _r2Caracteristicas = value
        End Set
    End Property

    Public Property R3Caracteristicas As String
        Get
            Return _r3Caracteristicas
        End Get
        Set(value As String)
            _r3Caracteristicas = value
        End Set
    End Property

    Public Property R4Caracteristicas As String
        Get
            Return _r4Caracteristicas
        End Get
        Set(value As String)
            _r4Caracteristicas = value
        End Set
    End Property

    Public Property R5Caracteristicas As String
        Get
            Return _r5Caracteristicas
        End Get
        Set(value As String)
            _r5Caracteristicas = value
        End Set
    End Property

    Public Property R6Caracteristicas As String
        Get
            Return _r6Caracteristicas
        End Get
        Set(value As String)
            _r6Caracteristicas = value
        End Set
    End Property

    Public Property R7Caracteristicas As String
        Get
            Return _r7Caracteristicas
        End Get
        Set(value As String)
            _r7Caracteristicas = value
        End Set
    End Property

    Public Property R8Caracteristicas As String
        Get
            Return _r8Caracteristicas
        End Get
        Set(value As String)
            _r8Caracteristicas = value
        End Set
    End Property

    Public Property R9Caracteristicas As String
        Get
            Return _r9Caracteristicas
        End Get
        Set(value As String)
            _r9Caracteristicas = value
        End Set
    End Property
End Class
