﻿Public Class entClienteFinal
    Private _Cliente As String
    Private _Nombre As String
    Private _RFC As String
    Private _Telefono As String
    Private _Telefono2 As String
    Private _EMail As String
    Private _Calle As String
    Private _NumExt As String
    Private _NumInt As String
    Private _CP As Integer
    Private _Colonia As String
    Private _Municipio As String
    Private _Estado As String
    Private _Bancos As String
    Private _Banco_Tipo As String
    Private _Banco_Cuenta As String
    Private _Beneficiario As String
    Private _Parentesco As String
    Private _Fum As Date

    Public Property Cliente As String
        Get
            Return _Cliente
        End Get
        Set(value As String)
            _Cliente = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _Nombre
        End Get
        Set(value As String)
            _Nombre = value
        End Set
    End Property

    Public Property RFC As String
        Get
            Return _RFC
        End Get
        Set(value As String)
            _RFC = value
        End Set
    End Property

    Public Property Telefono As String
        Get
            Return _Telefono
        End Get
        Set(value As String)
            _Telefono = value
        End Set
    End Property

    Public Property Telefono2 As String
        Get
            Return _Telefono2
        End Get
        Set(value As String)
            _Telefono2 = value
        End Set
    End Property

    Public Property EMail As String
        Get
            Return _EMail
        End Get
        Set(value As String)
            _EMail = value
        End Set
    End Property

    Public Property Calle As String
        Get
            Return _Calle
        End Get
        Set(value As String)
            _Calle = value
        End Set
    End Property

    Public Property NumExt As String
        Get
            Return _NumExt
        End Get
        Set(value As String)
            _NumExt = value
        End Set
    End Property

    Public Property NumInt As String
        Get
            Return _NumInt
        End Get
        Set(value As String)
            _NumInt = value
        End Set
    End Property

    Public Property CP As Integer
        Get
            Return _CP
        End Get
        Set(value As Integer)
            _CP = value
        End Set
    End Property

    Public Property Colonia As String
        Get
            Return _Colonia
        End Get
        Set(value As String)
            _Colonia = value
        End Set
    End Property

    Public Property Municipio As String
        Get
            Return _Municipio
        End Get
        Set(value As String)
            _Municipio = value
        End Set
    End Property

    Public Property Estado As String
        Get
            Return _Estado
        End Get
        Set(value As String)
            _Estado = value
        End Set
    End Property

    Public Property Bancos As String
        Get
            Return _Bancos
        End Get
        Set(value As String)
            _Bancos = value
        End Set
    End Property

    Public Property Banco_Tipo As String
        Get
            Return _Banco_Tipo
        End Get
        Set(value As String)
            _Banco_Tipo = value
        End Set
    End Property

    Public Property Banco_Cuenta As String
        Get
            Return _Banco_Cuenta
        End Get
        Set(value As String)
            _Banco_Cuenta = value
        End Set
    End Property

    Public Property Beneficiario As String
        Get
            Return _Beneficiario
        End Get
        Set(value As String)
            _Beneficiario = value
        End Set
    End Property

    Public Property Parentesco As String
        Get
            Return _Parentesco
        End Get
        Set(value As String)
            _Parentesco = value
        End Set
    End Property

    Public Property Fum As Date
        Get
            Return _Fum
        End Get
        Set(value As Date)
            _Fum = value
        End Set
    End Property
End Class
