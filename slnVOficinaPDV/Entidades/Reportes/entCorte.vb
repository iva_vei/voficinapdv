﻿Public Class entCorte
    Private _movimiento As String
    Private _id_empresa As String
    Private _empresa As String
    Private _rfc As String
    Private _direccion As String
    Private _direccion2 As String
    Private _id_sucursal As String
    Private _fecha As Date
    Private _hora As String
    Private _cajero As String
    Private _caja As String
    Private _tipo As String
    Private _sw_final As Boolean
    Private _corte As Integer
    Private _numero As Integer
    Private _Detalle As List(Of entCorteDet)

    Public Sub New()
        Detalle = New List(Of entCorteDet)
    End Sub

    Public Sub Fill(ByVal dtDatos As DataTable)
        Dim det As entCorteDet
        Try
            If dtDatos.Rows.Count > 0 Then
                For Each Renglon As DataRow In dtDatos.Rows
                    det = New entCorteDet

                    ''_movimiento = Renglon.Item("movimiento")
                    ''_id_empresa = Renglon.Item("id_empresa")
                    ''_numero = Renglon.Item("numero")
                    ''_corte = Renglon.Item("corte")
                    ''_caja = Renglon.Item("caja")
                    ''_cajero = Renglon.Item("cajero")
                    ''_id_sucursal = Renglon.Item("id_sucursal")
                    _fecha = Renglon.Item("fecha")
                    _hora = Renglon.Item("hora")
                    _tipo = Renglon.Item("tipo").ToString.Substring(2)
                    If _tipo = "CORTE Z" Then
                        _sw_final = True
                    Else
                        _sw_final = False
                    End If

                    det.Tipo = Renglon.Item("tipo")
                    det.Concepto = Renglon.Item("concepto")
                    det.Referencia = Renglon.Item("referencia")
                    If det.Concepto.Trim = "------------:" Or det.Concepto.Trim = "-----FIN----" Or det.Concepto.Trim = "------------" Then

                        det.Numero = ""
                        det.Importe = ""
                    Else
                        det.Numero = Format(Renglon.Item("numero"), "N0")
                        det.Importe = Format(Renglon.Item("importe"), "N2")
                    End If

                    Detalle.Add(det)
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Property Detalle As List(Of entCorteDet)
        Get
            Return _Detalle
        End Get
        Set(value As List(Of entCorteDet))
            _Detalle = value
        End Set
    End Property

    Public Property Movimiento As String
        Get
            Return _movimiento
        End Get
        Set(value As String)
            _movimiento = value
        End Set
    End Property

    Public Property Id_empresa As String
        Get
            Return _id_empresa
        End Get
        Set(value As String)
            _id_empresa = value
        End Set
    End Property

    Public Property Empresa As String
        Get
            Return _empresa
        End Get
        Set(value As String)
            _empresa = value
        End Set
    End Property

    Public Property Rfc As String
        Get
            Return _rfc
        End Get
        Set(value As String)
            _rfc = value
        End Set
    End Property

    Public Property Direccion As String
        Get
            Return _direccion
        End Get
        Set(value As String)
            _direccion = value
        End Set
    End Property

    Public Property Direccion2 As String
        Get
            Return _direccion2
        End Get
        Set(value As String)
            _direccion2 = value
        End Set
    End Property

    Public Property Id_sucursal As String
        Get
            Return _id_sucursal
        End Get
        Set(value As String)
            _id_sucursal = value
        End Set
    End Property

    Public Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Public Property Hora As String
        Get
            Return _hora
        End Get
        Set(value As String)
            _hora = value
        End Set
    End Property

    Public Property Cajero As String
        Get
            Return _cajero
        End Get
        Set(value As String)
            _cajero = value
        End Set
    End Property

    Public Property Caja As String
        Get
            Return _caja
        End Get
        Set(value As String)
            _caja = value
        End Set
    End Property

    Public Property Sw_final As Boolean
        Get
            Return _sw_final
        End Get
        Set(value As Boolean)
            _sw_final = value
        End Set
    End Property

    Public Property Numero As Integer
        Get
            Return _numero
        End Get
        Set(value As Integer)
            _numero = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property

    Public Property Corte As Integer
        Get
            Return _corte
        End Get
        Set(value As Integer)
            _corte = value
        End Set
    End Property
End Class
Public Class entCorteDet
    Private _Tipo As String
    Private _Concepto As String
    Private _Referencia As String
    Private _Numero As String
    Private _Importe As String

    Public Property Concepto As String
        Get
            Return _Concepto
        End Get
        Set(value As String)
            _Concepto = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _Numero
        End Get
        Set(value As String)
            _Numero = value
        End Set
    End Property

    Public Property Importe As String
        Get
            Return _Importe
        End Get
        Set(value As String)
            _Importe = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _Tipo
        End Get
        Set(value As String)
            _Tipo = value
        End Set
    End Property

    Public Property Referencia As String
        Get
            Return _Referencia
        End Get
        Set(value As String)
            _Referencia = value
        End Set
    End Property
End Class
