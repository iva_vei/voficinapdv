﻿Imports Utils.Collections
Public Class entTicketVenta
    Private _autonumsuc As String = ""
    Private _id_empresa As String = ""
    Private _empresa As String = ""
    Private _rfc As String = ""
    Private _curp As String = ""
    Private _direccion As String = ""
    Private _direccion2 As String = ""
    Private _transaccion As String = ""
    Private _id_sucursal As String = ""
    Private _fecha As Date
    Private _hora As String = ""
    Private _cajero As String = ""
    Private _caja As String = ""
    Private _lealtad_nombre As String = ""
    Private _puntos_acumulados As Double
    Private _de_acumulados As Double
    Private _puntos_utilizados As Double
    Private _de_utilizados As Double
    Private _puntos_saldo_ant As Double
    Private _de_saldo_ant As Double
    Private _puntos_saldo_act As Double
    Private _de_saldo_act As Double
    Private _descuento As Double

    Private _FacEmpresa As String = ""
    Private _FacDireccion As String = ""
    Private _LugExpedicion As String = ""
    Private _Id_cliente As String = ""
    Private _nomfac As String = ""
    Private _direccionfac As String = ""
    Private _entrecallesfac As String = ""
    Private _coloniafac As String = ""
    Private _ciudadfac As String = ""
    Private _edofac As String = ""
    Private _paisfac As String = ""
    Private _cpostalfac As String = ""
    Private _rfc2 As String = ""
    Private _plazo As Decimal
    Private _Detalle As List(Of entTicketVentaDet)
    Private _Referencia As String = ""
    Private _Movimiento As String = ""
    Private _Autorizacion As String = ""
    Private _mensaje As String = ""
    Private _mensaje2 As String = ""
    Private _nom_cliente As String = ""
    Private _telefono As String = ""

    Private _Id_cliente1 As String = ""
    Private _nom_cliente1 As String = ""
    Private _Beneficiario As String = ""
    Private _Parentesco As String = ""
    Private _NoVale As String = ""
    Private _cuenta_odp As String = ""

    Public Sub New()
        Detalle = New List(Of entTicketVentaDet)
        _Referencia = ""
        _Movimiento = ""
        _Autorizacion = ""
    End Sub
    Public Sub Fill(ByVal dtDatos As DataTable, ByVal dtDatos2 As DataTable, ByVal dtOtros As DataTable)
        Dim det As entTicketVentaDet
        Try
            Dim syncObject As New Object() ' Objeto de sincronización para proteger la lista

            If dtDatos.Rows.Count > 0 Then
                If Not dtOtros Is Nothing Then
                    If dtOtros.Rows.Count > 0 Then
                        Try
                            FacEmpresa = dtOtros.Rows(0).Item("FacEmpresa")
                        Catch ex As Exception
                            FacEmpresa = ""
                        End Try
                        Try
                            FacDireccion = dtOtros.Rows(0).Item("FacDireccion")
                        Catch ex As Exception
                            FacDireccion = ""
                        End Try
                        Try
                            LugExpedicion = dtOtros.Rows(0).Item("LugExpedicion")
                        Catch ex As Exception
                            LugExpedicion = ""
                        End Try
                        Try
                            Nomfac = dtOtros.Rows(0).Item("nomfac")
                        Catch ex As Exception
                            Nomfac = dtOtros.Rows(0).Item("nombre")
                        End Try
                        Try
                            Nom_cliente = dtOtros.Rows(0).Item("nomfac")
                        Catch ex As Exception
                            Nom_cliente = dtOtros.Rows(0).Item("nombre_cli")
                        End Try
                        Try
                            Direccionfac = dtOtros.Rows(0).Item("direccionfac")
                        Catch ex As Exception
                            Direccionfac = dtOtros.Rows(0).Item("Direccion")
                        End Try
                        Try
                            Entrecallesfac = dtOtros.Rows(0).Item("entrecallesfac")
                        Catch ex As Exception
                            Entrecallesfac = dtOtros.Rows(0).Item("entrecalles")
                        End Try
                        Try
                            Coloniafac = dtOtros.Rows(0).Item("coloniafac")
                        Catch ex As Exception
                            Coloniafac = dtOtros.Rows(0).Item("colonia")
                        End Try
                        Try
                            Ciudadfac = dtOtros.Rows(0).Item("ciudadfac")
                        Catch ex As Exception
                            Ciudadfac = dtOtros.Rows(0).Item("ciudad")
                        End Try
                        Try
                            Edofac = dtOtros.Rows(0).Item("edofac")
                        Catch ex As Exception
                            Edofac = dtOtros.Rows(0).Item("edo")
                        End Try
                        Try
                            Paisfac = dtOtros.Rows(0).Item("paisfac")
                        Catch ex As Exception
                            Paisfac = dtOtros.Rows(0).Item("pais")
                        End Try
                        Try
                            Cpostalfac = dtOtros.Rows(0).Item("cpostalfac")
                        Catch ex As Exception
                            Cpostalfac = dtOtros.Rows(0).Item("cpostal")
                        End Try
                        Try
                            Rfc2 = dtOtros.Rows(0).Item("rfc")
                        Catch ex As Exception
                            Rfc2 = ""
                        End Try
                        Try
                            Curp = dtOtros.Rows(0).Item("curp")
                        Catch ex As Exception
                            Curp = ""
                        End Try
                        Try
                            Plazo = dtOtros.Rows(0).Item("plazo")
                        Catch ex As Exception
                            Plazo = 0
                        End Try

                        Try
                            Telefono = dtOtros.Rows(0).Item("telefono")
                        Catch ex As Exception
                            Telefono = ""
                        End Try
                        Try
                            Id_cliente = dtOtros.Rows(0).Item("id_cliente")
                        Catch ex As Exception
                            Id_cliente = ""
                        End Try
                        Try
                            Beneficiario = dtOtros.Rows(0).Item("beneficiario")
                        Catch ex As Exception
                            Beneficiario = ""
                        End Try
                        Try
                            Parentesco = dtOtros.Rows(0).Item("parentesco")
                        Catch ex As Exception
                            Parentesco = ""
                        End Try

                    End If
                End If

                Dim Renglon As DataRow
                If dtDatos.Rows.Count > 0 Then

                    Renglon = dtDatos.Rows(0)
                    _autonumsuc = Renglon.Item("autonumsuc")
                    _id_empresa = Renglon.Item("id_empresa")
                    _transaccion = Renglon.Item("transaccion").ToString
                    _id_sucursal = Renglon.Item("id_sucursal")
                    _fecha = Renglon.Item("fecha")
                    _hora = Renglon.Item("hora")
                    Try
                        _cuenta_odp = Renglon.Item("cuenta_odp")
                    Catch ex As Exception
                        _cuenta_odp = ""
                    End Try
                    Try
                        _NoVale = Renglon.Item("referencia")
                    Catch ex As Exception
                        NoVale = ""
                    End Try

                    Detalle = dtDatos.ToList(Of entTicketVentaDet)
                End If
            End If
            If dtDatos2.Rows.Count Then
                Dim Renglon2 As DataRow
                Renglon2 = dtDatos2.Rows(0)
                ''_lealtad_nombre = Renglon2.Item("fecha")
                _puntos_acumulados = Renglon2.Item("puntos_acumular")
                _de_acumulados = Renglon2.Item("de_acumular")
                _puntos_utilizados = Renglon2.Item("puntos_utilizados")
                _de_utilizados = Renglon2.Item("de_utilizados")
                _descuento = Renglon2.Item("descuento")
                Try
                    _Referencia = Renglon2.Item("referencia")
                    _Movimiento = Renglon2.Item("movimiento")
                    _Autorizacion = Renglon2.Item("autorizacion")
                Catch ex As Exception

                End Try

                Try
                    If Nom_cliente = "" Then
                        Nom_cliente = Renglon2.Item("nom_cliente")
                    End If
                Catch ex As Exception
                    Nom_cliente = ""
                End Try

                Try
                    _Id_cliente1 = Renglon2.Item("id_cliente1")
                Catch ex As Exception
                    _Id_cliente1 = ""
                End Try

                Try
                    _nom_cliente1 = Renglon2.Item("nom_cliente1")
                Catch ex As Exception
                    _nom_cliente1 = ""
                End Try

                Try
                    Mensaje = Renglon2.Item("mensaje")
                Catch ex As Exception
                    Mensaje = ""
                End Try

                Try
                    Mensaje2 = Renglon2.Item("mensaje2")
                Catch ex As Exception
                    Mensaje2 = ""
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub Fill_v3(ByVal dtDatos As DataTable, ByVal dtDatos2 As DataTable, ByVal dtOtros As DataTable)
        Dim det As entTicketVentaDet
        Try
            Dim syncObject As New Object() ' Objeto de sincronización para proteger la lista

            If dtDatos.Rows.Count > 0 Then
                If Not dtOtros Is Nothing Then
                    If dtOtros.Rows.Count > 0 Then
                        Try
                            FacEmpresa = dtOtros.Rows(0).Item("FacEmpresa")
                        Catch ex As Exception
                            FacEmpresa = ""
                        End Try
                        Try
                            FacDireccion = dtOtros.Rows(0).Item("FacDireccion")
                        Catch ex As Exception
                            FacDireccion = ""
                        End Try
                        Try
                            LugExpedicion = dtOtros.Rows(0).Item("LugExpedicion")
                        Catch ex As Exception
                            LugExpedicion = ""
                        End Try
                        Try
                            Nomfac = dtOtros.Rows(0).Item("nomfac")
                        Catch ex As Exception
                            Nomfac = dtOtros.Rows(0).Item("nombre")
                        End Try
                        Try
                            Nom_cliente = dtOtros.Rows(0).Item("nomfac")
                        Catch ex As Exception
                            Nom_cliente = dtOtros.Rows(0).Item("nombre_cli")
                        End Try
                        Try
                            Direccionfac = dtOtros.Rows(0).Item("direccionfac")
                        Catch ex As Exception
                            Direccionfac = dtOtros.Rows(0).Item("Direccion")
                        End Try
                        Try
                            Entrecallesfac = dtOtros.Rows(0).Item("entrecallesfac")
                        Catch ex As Exception
                            Entrecallesfac = dtOtros.Rows(0).Item("entrecalles")
                        End Try
                        Try
                            Coloniafac = dtOtros.Rows(0).Item("coloniafac")
                        Catch ex As Exception
                            Coloniafac = dtOtros.Rows(0).Item("colonia")
                        End Try
                        Try
                            Ciudadfac = dtOtros.Rows(0).Item("ciudadfac")
                        Catch ex As Exception
                            Ciudadfac = dtOtros.Rows(0).Item("ciudad")
                        End Try
                        Try
                            Edofac = dtOtros.Rows(0).Item("edofac")
                        Catch ex As Exception
                            Edofac = dtOtros.Rows(0).Item("edo")
                        End Try
                        Try
                            Paisfac = dtOtros.Rows(0).Item("paisfac")
                        Catch ex As Exception
                            Paisfac = dtOtros.Rows(0).Item("pais")
                        End Try
                        Try
                            Cpostalfac = dtOtros.Rows(0).Item("cpostalfac")
                        Catch ex As Exception
                            Cpostalfac = dtOtros.Rows(0).Item("cpostal")
                        End Try
                        Try
                            Rfc2 = dtOtros.Rows(0).Item("rfc")
                        Catch ex As Exception
                            Rfc2 = ""
                        End Try
                        Try
                            Curp = dtOtros.Rows(0).Item("curp")
                        Catch ex As Exception
                            Curp = ""
                        End Try
                        Try
                            Plazo = dtOtros.Rows(0).Item("plazo")
                        Catch ex As Exception
                            Plazo = 0
                        End Try

                        Try
                            Telefono = dtOtros.Rows(0).Item("telefono")
                        Catch ex As Exception
                            Telefono = ""
                        End Try
                        Try
                            Id_cliente = dtOtros.Rows(0).Item("id_cliente")
                        Catch ex As Exception
                            Id_cliente = ""
                        End Try
                        Try
                            Beneficiario = dtOtros.Rows(0).Item("beneficiario")
                        Catch ex As Exception
                            Beneficiario = ""
                        End Try
                        Try
                            Parentesco = dtOtros.Rows(0).Item("parentesco")
                        Catch ex As Exception
                            Parentesco = ""
                        End Try

                    End If
                End If
                Parallel.ForEach(dtDatos.AsEnumerable(), Sub(Renglon)
                                                             ' Crear un nuevo objeto Registro desde la fila
                                                             det = New entTicketVentaDet

                                                             det.Autonumsuc = Renglon.Item("autonumsuc")
                                                             det.Id_empresa = Renglon.Item("id_empresa")
                                                             det.Renglon = Renglon.Item("renglon")
                                                             det.Transaccion = Renglon.Item("transaccion")
                                                             det.Id_sucursal = Renglon.Item("id_sucursal")
                                                             det.Fecha = Renglon.Item("fecha")
                                                             det.Articulo = Renglon.Item("articulo")
                                                             det.Division = Renglon.Item("division")
                                                             det.Depto = Renglon.Item("depto")
                                                             det.Familia = Renglon.Item("familia")
                                                             det.Marca = Renglon.Item("marca")
                                                             det.Nombre = Renglon.Item("nombre")
                                                             det.Cantidad = Renglon.Item("cantidad")
                                                             det.Precionormal = Renglon.Item("precionormal")
                                                             det.Precioventa = Renglon.Item("precioventa")
                                                             det.Preciofinal = Renglon.Item("preciofinal")
                                                             det.Descuento = Renglon.Item("descuento")
                                                             det.Iva = Renglon.Item("iva")
                                                             det.Impiva = Renglon.Item("impiva")
                                                             det.Tienda = Renglon.Item("tienda")
                                                             det.Impcanc = Renglon.Item("impcanc")
                                                             det.Ivacanc = Renglon.Item("ivacanc")
                                                             det.Descanc = Renglon.Item("descanc")
                                                             det.Impdev = Renglon.Item("impdev")
                                                             det.Ivadev = Renglon.Item("ivadev")
                                                             det.Desdev = Renglon.Item("desdev")
                                                             det.Preciocontable = Renglon.Item("preciocontable")
                                                             det.Preciopublico = Renglon.Item("preciopublico")
                                                             det.Financiamiento = Renglon.Item("financiamiento")
                                                             det.Tipocredito = Renglon.Item("tipocredito")
                                                             det.Concepto = Renglon.Item("concepto")
                                                             det.Vendedor = Renglon.Item("vendedor")
                                                             det.Lcredito = Renglon.Item("lcredito")
                                                             det.Tiponc = Renglon.Item("tiponc")
                                                             det.Referencia = Renglon.Item("referencia")
                                                             det.Movimiento = Renglon.Item("movimiento")
                                                             det.Porimpuesto1 = Renglon.Item("porimpuesto1")
                                                             det.Porimpuesto2 = Renglon.Item("porimpuesto2")
                                                             det.Porimpuesto3 = Renglon.Item("porimpuesto3")
                                                             det.Totimpuesto0 = Renglon.Item("totimpuesto0")
                                                             det.Totimpuesto1 = Renglon.Item("totimpuesto1")
                                                             det.Totimpuesto2 = Renglon.Item("totimpuesto2")
                                                             det.Nomcorto = Renglon.Item("nomcorto")
                                                             det.Total = Renglon.Item("total")
                                                             det.Tipo = Renglon.Item("tipo")

                                                             Try
                                                                 det.DE_Otorgado = Renglon.Item("de_otorgado")
                                                             Catch ex As Exception

                                                             End Try

                                                             Try
                                                                 det.PrecioBruto = Renglon.Item("precio_bruto")
                                                             Catch ex As Exception

                                                             End Try


                                                             ' Agregar el objeto a la lista de manera segura
                                                             SyncLock syncObject
                                                                 Detalle.Add(det)
                                                             End SyncLock
                                                         End Sub)
            End If
            If dtDatos2.Rows.Count Then
                Dim Renglon2 As DataRow
                Renglon2 = dtDatos2.Rows(0)
                ''_lealtad_nombre = Renglon2.Item("fecha")
                _puntos_acumulados = Renglon2.Item("puntos_acumular")
                _de_acumulados = Renglon2.Item("de_acumular")
                _puntos_utilizados = Renglon2.Item("puntos_utilizados")
                _de_utilizados = Renglon2.Item("de_utilizados")
                _descuento = Renglon2.Item("descuento")
                Try
                    _Referencia = Renglon2.Item("referencia")
                    _Movimiento = Renglon2.Item("movimiento")
                    _Autorizacion = Renglon2.Item("autorizacion")
                Catch ex As Exception

                End Try

                Try
                    If Nom_cliente = "" Then
                        Nom_cliente = Renglon2.Item("nom_cliente")
                    End If
                Catch ex As Exception
                    Nom_cliente = ""
                End Try

                Try
                    _Id_cliente1 = Renglon2.Item("id_cliente1")
                Catch ex As Exception
                    _Id_cliente1 = ""
                End Try

                Try
                    _nom_cliente1 = Renglon2.Item("nom_cliente1")
                Catch ex As Exception
                    _nom_cliente1 = ""
                End Try

                Try
                    Mensaje = Renglon2.Item("mensaje")
                Catch ex As Exception
                    Mensaje = ""
                End Try

                Try
                    Mensaje2 = Renglon2.Item("mensaje2")
                Catch ex As Exception
                    Mensaje2 = ""
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub Fill_V2(ByVal dtDatos As DataTable, ByVal dtDatos2 As DataTable, ByVal dtOtros As DataTable)
        Dim det As entTicketVentaDet
        Try
            If dtDatos.Rows.Count > 0 Then
                If Not dtOtros Is Nothing Then
                    If dtOtros.Rows.Count > 0 Then
                        Try
                            FacEmpresa = dtOtros.Rows(0).Item("FacEmpresa")
                        Catch ex As Exception
                            FacEmpresa = ""
                        End Try
                        Try
                            FacDireccion = dtOtros.Rows(0).Item("FacDireccion")
                        Catch ex As Exception
                            FacDireccion = ""
                        End Try
                        Try
                            LugExpedicion = dtOtros.Rows(0).Item("LugExpedicion")
                        Catch ex As Exception
                            LugExpedicion = ""
                        End Try
                        Try
                            Nomfac = dtOtros.Rows(0).Item("nomfac")
                        Catch ex As Exception
                            Nomfac = dtOtros.Rows(0).Item("nombre")
                        End Try
                        Try
                            Nom_cliente = dtOtros.Rows(0).Item("nomfac")
                        Catch ex As Exception
                            Nom_cliente = dtOtros.Rows(0).Item("nombre_cli")
                        End Try
                        Try
                            Direccionfac = dtOtros.Rows(0).Item("direccionfac")
                        Catch ex As Exception
                            Direccionfac = dtOtros.Rows(0).Item("Direccion")
                        End Try
                        Try
                            Entrecallesfac = dtOtros.Rows(0).Item("entrecallesfac")
                        Catch ex As Exception
                            Entrecallesfac = dtOtros.Rows(0).Item("entrecalles")
                        End Try
                        Try
                            Coloniafac = dtOtros.Rows(0).Item("coloniafac")
                        Catch ex As Exception
                            Coloniafac = dtOtros.Rows(0).Item("colonia")
                        End Try
                        Try
                            Ciudadfac = dtOtros.Rows(0).Item("ciudadfac")
                        Catch ex As Exception
                            Ciudadfac = dtOtros.Rows(0).Item("ciudad")
                        End Try
                        Try
                            Edofac = dtOtros.Rows(0).Item("edofac")
                        Catch ex As Exception
                            Edofac = dtOtros.Rows(0).Item("edo")
                        End Try
                        Try
                            Paisfac = dtOtros.Rows(0).Item("paisfac")
                        Catch ex As Exception
                            Paisfac = dtOtros.Rows(0).Item("pais")
                        End Try
                        Try
                            Cpostalfac = dtOtros.Rows(0).Item("cpostalfac")
                        Catch ex As Exception
                            Cpostalfac = dtOtros.Rows(0).Item("cpostal")
                        End Try
                        Try
                            Rfc2 = dtOtros.Rows(0).Item("rfc")
                        Catch ex As Exception
                            Rfc2 = ""
                        End Try
                        Try
                            Curp = dtOtros.Rows(0).Item("curp")
                        Catch ex As Exception
                            Curp = ""
                        End Try
                        Try
                            Plazo = dtOtros.Rows(0).Item("plazo")
                        Catch ex As Exception
                            Plazo = 0
                        End Try

                        Try
                            Telefono = dtOtros.Rows(0).Item("telefono")
                        Catch ex As Exception
                            Telefono = ""
                        End Try
                        Try
                            Id_cliente = dtOtros.Rows(0).Item("id_cliente")
                        Catch ex As Exception
                            Id_cliente = ""
                        End Try
                        Try
                            Beneficiario = dtOtros.Rows(0).Item("beneficiario")
                        Catch ex As Exception
                            Beneficiario = ""
                        End Try
                        Try
                            Parentesco = dtOtros.Rows(0).Item("parentesco")
                        Catch ex As Exception
                            Parentesco = ""
                        End Try

                    End If
                End If
                For Each Renglon As DataRow In dtDatos.Rows
                    det = New entTicketVentaDet

                    _autonumsuc = Renglon.Item("autonumsuc")
                    _id_empresa = Renglon.Item("id_empresa")
                    _transaccion = Renglon.Item("transaccion").ToString
                    _id_sucursal = Renglon.Item("id_sucursal")
                    _fecha = Renglon.Item("fecha")
                    _hora = Renglon.Item("hora")

                    Try
                        _NoVale = Renglon.Item("referencia")
                    Catch ex As Exception
                        NoVale = ""
                    End Try


                    det.Autonumsuc = Renglon.Item("autonumsuc")
                    det.Id_empresa = Renglon.Item("id_empresa")
                    det.Renglon = Renglon.Item("renglon")
                    det.Transaccion = Renglon.Item("transaccion")
                    det.Id_sucursal = Renglon.Item("id_sucursal")
                    det.Fecha = Renglon.Item("fecha")
                    det.Articulo = Renglon.Item("articulo")
                    det.Division = Renglon.Item("division")
                    det.Depto = Renglon.Item("depto")
                    det.Familia = Renglon.Item("familia")
                    det.Marca = Renglon.Item("marca")
                    det.Nombre = Renglon.Item("nombre")
                    det.Cantidad = Renglon.Item("cantidad")
                    det.Precionormal = Renglon.Item("precionormal")
                    det.Precioventa = Renglon.Item("precioventa")
                    det.Preciofinal = Renglon.Item("preciofinal")
                    det.Descuento = Renglon.Item("descuento")
                    det.Iva = Renglon.Item("iva")
                    det.Impiva = Renglon.Item("impiva")
                    det.Tienda = Renglon.Item("tienda")
                    det.Impcanc = Renglon.Item("impcanc")
                    det.Ivacanc = Renglon.Item("ivacanc")
                    det.Descanc = Renglon.Item("descanc")
                    det.Impdev = Renglon.Item("impdev")
                    det.Ivadev = Renglon.Item("ivadev")
                    det.Desdev = Renglon.Item("desdev")
                    det.Preciocontable = Renglon.Item("preciocontable")
                    det.Preciopublico = Renglon.Item("preciopublico")
                    det.Financiamiento = Renglon.Item("financiamiento")
                    det.Tipocredito = Renglon.Item("tipocredito")
                    det.Concepto = Renglon.Item("concepto")
                    det.Vendedor = Renglon.Item("vendedor")
                    det.Lcredito = Renglon.Item("lcredito")
                    det.Tiponc = Renglon.Item("tiponc")
                    det.Referencia = Renglon.Item("referencia")
                    det.Movimiento = Renglon.Item("movimiento")
                    det.Porimpuesto1 = Renglon.Item("porimpuesto1")
                    det.Porimpuesto2 = Renglon.Item("porimpuesto2")
                    det.Porimpuesto3 = Renglon.Item("porimpuesto3")
                    det.Totimpuesto0 = Renglon.Item("totimpuesto0")
                    det.Totimpuesto1 = Renglon.Item("totimpuesto1")
                    det.Totimpuesto2 = Renglon.Item("totimpuesto2")
                    det.Nomcorto = Renglon.Item("nomcorto")
                    det.Total = Renglon.Item("total")
                    det.Tipo = Renglon.Item("tipo")

                    Try
                        det.DE_Otorgado = Renglon.Item("de_otorgado")
                    Catch ex As Exception

                    End Try

                    Try
                        det.PrecioBruto = Renglon.Item("precio_bruto")
                    Catch ex As Exception

                    End Try

                    Detalle.Add(det)
                Next
            End If

            If dtDatos2.Rows.Count Then
                Dim Renglon2 As DataRow
                Renglon2 = dtDatos2.Rows(0)
                ''_lealtad_nombre = Renglon2.Item("fecha")
                _puntos_acumulados = Renglon2.Item("puntos_acumular")
                _de_acumulados = Renglon2.Item("de_acumular")
                _puntos_utilizados = Renglon2.Item("puntos_utilizados")
                _de_utilizados = Renglon2.Item("de_utilizados")
                _descuento = Renglon2.Item("descuento")
                Try
                    _Referencia = Renglon2.Item("referencia")
                    _Movimiento = Renglon2.Item("movimiento")
                    _Autorizacion = Renglon2.Item("autorizacion")
                Catch ex As Exception

                End Try

                Try
                    If Nom_cliente = "" Then
                        Nom_cliente = Renglon2.Item("nom_cliente")
                    End If
                Catch ex As Exception
                    Nom_cliente = ""
                End Try

                Try
                    _Id_cliente1 = Renglon2.Item("id_cliente1")
                Catch ex As Exception
                    _Id_cliente1 = ""
                End Try

                Try
                    _nom_cliente1 = Renglon2.Item("nom_cliente1")
                Catch ex As Exception
                    _nom_cliente1 = ""
                End Try

                Try
                    Mensaje = Renglon2.Item("mensaje")
                Catch ex As Exception
                    Mensaje = ""
                End Try

                Try
                    Mensaje2 = Renglon2.Item("mensaje2")
                Catch ex As Exception
                    Mensaje2 = ""
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Property Autonumsuc As String
        Get
            Return _autonumsuc
        End Get
        Set(value As String)
            _autonumsuc = value
        End Set
    End Property

    Public Property Id_empresa As String
        Get
            Return _id_empresa
        End Get
        Set(value As String)
            _id_empresa = value
        End Set
    End Property

    Public Property Transaccion As String
        Get
            Return _transaccion
        End Get
        Set(value As String)
            _transaccion = value
        End Set
    End Property

    Public Property Id_sucursal As String
        Get
            Return _id_sucursal
        End Get
        Set(value As String)
            _id_sucursal = value
        End Set
    End Property

    Public Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Public Property Detalle As List(Of entTicketVentaDet)
        Get
            Return _Detalle
        End Get
        Set(value As List(Of entTicketVentaDet))
            _Detalle = value
        End Set
    End Property

    Public Property Lealtad_nombre As String
        Get
            Return _lealtad_nombre
        End Get
        Set(value As String)
            _lealtad_nombre = value
        End Set
    End Property

    Public Property Puntos_acumulados As Double
        Get
            Return _puntos_acumulados
        End Get
        Set(value As Double)
            _puntos_acumulados = value
        End Set
    End Property

    Public Property De_acumulados As Double
        Get
            Return _de_acumulados
        End Get
        Set(value As Double)
            _de_acumulados = value
        End Set
    End Property

    Public Property De_utilizados As Double
        Get
            Return _de_utilizados
        End Get
        Set(value As Double)
            _de_utilizados = value
        End Set
    End Property

    Public Property Puntos_saldo_ant As Double
        Get
            Return _puntos_saldo_ant
        End Get
        Set(value As Double)
            _puntos_saldo_ant = value
        End Set
    End Property

    Public Property De_saldo_ant As Double
        Get
            Return _de_saldo_ant
        End Get
        Set(value As Double)
            _de_saldo_ant = value
        End Set
    End Property

    Public Property Puntos_saldo_act As Double
        Get
            Return _puntos_saldo_act
        End Get
        Set(value As Double)
            _puntos_saldo_act = value
        End Set
    End Property

    Public Property De_saldo_act As Double
        Get
            Return _de_saldo_act
        End Get
        Set(value As Double)
            _de_saldo_act = value
        End Set
    End Property

    Public Property Puntos_utilizados As Double
        Get
            Return _puntos_utilizados
        End Get
        Set(value As Double)
            _puntos_utilizados = value
        End Set
    End Property

    Public Property Empresa As String
        Get
            Return _empresa
        End Get
        Set(value As String)
            _empresa = value
        End Set
    End Property

    Public Property Rfc As String
        Get
            Return _rfc
        End Get
        Set(value As String)
            _rfc = value
        End Set
    End Property

    Public Property Direccion As String
        Get
            Return _direccion
        End Get
        Set(value As String)
            _direccion = value
        End Set
    End Property
    Public Property Direccion2 As String
        Get
            Return _direccion2
        End Get
        Set(value As String)
            _direccion2 = value
        End Set
    End Property

    Public Property Hora As String
        Get
            Return _hora
        End Get
        Set(value As String)
            _hora = value
        End Set
    End Property

    Public Property Cajero As String
        Get
            Return _cajero
        End Get
        Set(value As String)
            _cajero = value
        End Set
    End Property

    Public Property Caja As String
        Get
            Return _caja
        End Get
        Set(value As String)
            _caja = value
        End Set
    End Property

    Public Property Descuento As Double
        Get
            Return _descuento
        End Get
        Set(value As Double)
            _descuento = value
        End Set
    End Property

    Public Property FacEmpresa As String
        Get
            Return _FacEmpresa
        End Get
        Set(value As String)
            _FacEmpresa = value
        End Set
    End Property

    Public Property FacDireccion As String
        Get
            Return _FacDireccion
        End Get
        Set(value As String)
            _FacDireccion = value
        End Set
    End Property

    Public Property LugExpedicion As String
        Get
            Return _LugExpedicion
        End Get
        Set(value As String)
            _LugExpedicion = value
        End Set
    End Property

    Public Property Id_cliente As String
        Get
            Return _Id_cliente
        End Get
        Set(value As String)
            _Id_cliente = value
        End Set
    End Property

    Public Property Nomfac As String
        Get
            Return _nomfac
        End Get
        Set(value As String)
            _nomfac = value
        End Set
    End Property

    Public Property Direccionfac As String
        Get
            Return _direccionfac
        End Get
        Set(value As String)
            _direccionfac = value
        End Set
    End Property

    Public Property Entrecallesfac As String
        Get
            Return _entrecallesfac
        End Get
        Set(value As String)
            _entrecallesfac = value
        End Set
    End Property

    Public Property Coloniafac As String
        Get
            Return _coloniafac
        End Get
        Set(value As String)
            _coloniafac = value
        End Set
    End Property

    Public Property Ciudadfac As String
        Get
            Return _ciudadfac
        End Get
        Set(value As String)
            _ciudadfac = value
        End Set
    End Property

    Public Property Edofac As String
        Get
            Return _edofac
        End Get
        Set(value As String)
            _edofac = value
        End Set
    End Property

    Public Property Paisfac As String
        Get
            Return _paisfac
        End Get
        Set(value As String)
            _paisfac = value
        End Set
    End Property

    Public Property Cpostalfac As String
        Get
            Return _cpostalfac
        End Get
        Set(value As String)
            _cpostalfac = value
        End Set
    End Property

    Public Property Rfc2 As String
        Get
            Return _rfc2
        End Get
        Set(value As String)
            _rfc2 = value
        End Set
    End Property

    Public Property Plazo As Decimal
        Get
            Return _plazo
        End Get
        Set(value As Decimal)
            _plazo = value
        End Set
    End Property

    Public Property Movimiento As String
        Get
            Return _Movimiento
        End Get
        Set(value As String)
            _Movimiento = value
        End Set
    End Property

    Public Property Autorizacion As String
        Get
            Return _Autorizacion
        End Get
        Set(value As String)
            _Autorizacion = value
        End Set
    End Property

    Public Property Referencia As String
        Get
            Return _Referencia
        End Get
        Set(value As String)
            _Referencia = value
        End Set
    End Property

    Public Property Mensaje As String
        Get
            Return _mensaje
        End Get
        Set(value As String)
            _mensaje = value
        End Set
    End Property

    Public Property Nom_cliente As String
        Get
            Return _nom_cliente
        End Get
        Set(value As String)
            _nom_cliente = value
        End Set
    End Property

    Public Property Id_cliente1 As String
        Get
            Return _Id_cliente1
        End Get
        Set(value As String)
            _Id_cliente1 = value
        End Set
    End Property

    Public Property Nom_cliente1 As String
        Get
            Return _nom_cliente1
        End Get
        Set(value As String)
            _nom_cliente1 = value
        End Set
    End Property

    Public Property Curp As String
        Get
            Return _curp
        End Get
        Set(value As String)
            _curp = value
        End Set
    End Property

    Public Property Telefono As String
        Get
            Return _telefono
        End Get
        Set(value As String)
            _telefono = value
        End Set
    End Property

    Public Property Beneficiario As String
        Get
            Return _Beneficiario
        End Get
        Set(value As String)
            _Beneficiario = value
        End Set
    End Property

    Public Property Parentesco As String
        Get
            Return _Parentesco
        End Get
        Set(value As String)
            _Parentesco = value
        End Set
    End Property

    Public Property NoVale As String
        Get
            Return _NoVale
        End Get
        Set(value As String)
            _NoVale = value
        End Set
    End Property

    Public Property Mensaje2 As String
        Get
            Return _mensaje2
        End Get
        Set(value As String)
            _mensaje2 = value
        End Set
    End Property

    Public Property Cuenta_odp As String
        Get
            Return _cuenta_odp
        End Get
        Set(value As String)
            _cuenta_odp = value
        End Set
    End Property
End Class
Public Class entTicketVentaDet
    Private _autonumsuc As String
    Private _id_empresa As String
    Private _renglon As Integer
    Private _transaccion As String
    Private _id_sucursal As String
    Private _fecha As Date
    Private _articulo As String
    Private _division As String
    Private _depto As String
    Private _familia As String
    Private _marca As String
    Private _nombre As String
    Private _cantidad As Double
    Private _precionormal As Double
    Private _precioventa As Double
    Private _preciofinal As Double
    Private _descuento As Double
    Private _iva As Double
    Private _impiva As Double
    Private _tienda As String
    Private _impcanc As Double
    Private _ivacanc As Double
    Private _descanc As Double
    Private _impdev As Double
    Private _ivadev As Double
    Private _desdev As Double
    Private _preciocontable As Double
    Private _preciopublico As Double
    Private _financiamiento As Double
    Private _tipocredito As String
    Private _concepto As String
    Private _vendedor As String
    Private _lcredito As String
    Private _tiponc As String
    Private _referencia As String
    Private _movimiento As String
    Private _porimpuesto1 As Double
    Private _porimpuesto2 As Double
    Private _porimpuesto3 As Double
    Private _totimpuesto0 As Double
    Private _totimpuesto1 As Double
    Private _totimpuesto2 As Double
    Private _nomcorto As String
    Private _total As Double
    Private _tipo As String
    Private _PrecioBruto As Double
    Private _DE_Otorgado As Double
    Public Property Autonumsuc As String
        Get
            Return _autonumsuc
        End Get
        Set(value As String)
            _autonumsuc = value
        End Set
    End Property

    Public Property Id_empresa As String
        Get
            Return _id_empresa
        End Get
        Set(value As String)
            _id_empresa = value
        End Set
    End Property

    Public Property Renglon As Integer
        Get
            Return _renglon
        End Get
        Set(value As Integer)
            _renglon = value
        End Set
    End Property

    Public Property Transaccion As String
        Get
            Return _transaccion
        End Get
        Set(value As String)
            _transaccion = value
        End Set
    End Property

    Public Property Id_sucursal As String
        Get
            Return _id_sucursal
        End Get
        Set(value As String)
            _id_sucursal = value
        End Set
    End Property

    Public Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Public Property Articulo As String
        Get
            Return _articulo
        End Get
        Set(value As String)
            _articulo = value
        End Set
    End Property

    Public Property Division As String
        Get
            Return _division
        End Get
        Set(value As String)
            _division = value
        End Set
    End Property

    Public Property Depto As String
        Get
            Return _depto
        End Get
        Set(value As String)
            _depto = value
        End Set
    End Property

    Public Property Familia As String
        Get
            Return _familia
        End Get
        Set(value As String)
            _familia = value
        End Set
    End Property

    Public Property Marca As String
        Get
            Return _marca
        End Get
        Set(value As String)
            _marca = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Cantidad As Double
        Get
            Return _cantidad
        End Get
        Set(value As Double)
            _cantidad = value
        End Set
    End Property

    Public Property Precionormal As Double
        Get
            Return _precionormal
        End Get
        Set(value As Double)
            _precionormal = value
        End Set
    End Property

    Public Property Precioventa As Double
        Get
            Return _precioventa
        End Get
        Set(value As Double)
            _precioventa = value
        End Set
    End Property

    Public Property Preciofinal As Double
        Get
            Return _preciofinal
        End Get
        Set(value As Double)
            _preciofinal = value
        End Set
    End Property

    Public Property Descuento As Double
        Get
            Return _descuento
        End Get
        Set(value As Double)
            _descuento = value
        End Set
    End Property

    Public Property Iva As Double
        Get
            Return _iva
        End Get
        Set(value As Double)
            _iva = value
        End Set
    End Property

    Public Property Impiva As Double
        Get
            Return _impiva
        End Get
        Set(value As Double)
            _impiva = value
        End Set
    End Property

    Public Property Tienda As String
        Get
            Return _tienda
        End Get
        Set(value As String)
            _tienda = value
        End Set
    End Property

    Public Property Impcanc As Double
        Get
            Return _impcanc
        End Get
        Set(value As Double)
            _impcanc = value
        End Set
    End Property

    Public Property Ivacanc As Double
        Get
            Return _ivacanc
        End Get
        Set(value As Double)
            _ivacanc = value
        End Set
    End Property

    Public Property Descanc As Double
        Get
            Return _descanc
        End Get
        Set(value As Double)
            _descanc = value
        End Set
    End Property

    Public Property Impdev As Double
        Get
            Return _impdev
        End Get
        Set(value As Double)
            _impdev = value
        End Set
    End Property

    Public Property Ivadev As Double
        Get
            Return _ivadev
        End Get
        Set(value As Double)
            _ivadev = value
        End Set
    End Property

    Public Property Desdev As Double
        Get
            Return _desdev
        End Get
        Set(value As Double)
            _desdev = value
        End Set
    End Property

    Public Property Preciocontable As Double
        Get
            Return _preciocontable
        End Get
        Set(value As Double)
            _preciocontable = value
        End Set
    End Property

    Public Property Preciopublico As Double
        Get
            Return _preciopublico
        End Get
        Set(value As Double)
            _preciopublico = value
        End Set
    End Property

    Public Property Financiamiento As Double
        Get
            Return _financiamiento
        End Get
        Set(value As Double)
            _financiamiento = value
        End Set
    End Property

    Public Property Tipocredito As String
        Get
            Return _tipocredito
        End Get
        Set(value As String)
            _tipocredito = value
        End Set
    End Property

    Public Property Concepto As String
        Get
            Return _concepto
        End Get
        Set(value As String)
            _concepto = value
        End Set
    End Property

    Public Property Vendedor As String
        Get
            Return _vendedor
        End Get
        Set(value As String)
            _vendedor = value
        End Set
    End Property

    Public Property Lcredito As String
        Get
            Return _lcredito
        End Get
        Set(value As String)
            _lcredito = value
        End Set
    End Property

    Public Property Tiponc As String
        Get
            Return _tiponc
        End Get
        Set(value As String)
            _tiponc = value
        End Set
    End Property

    Public Property Referencia As String
        Get
            Return _referencia
        End Get
        Set(value As String)
            _referencia = value
        End Set
    End Property

    Public Property Movimiento As String
        Get
            Return _movimiento
        End Get
        Set(value As String)
            _movimiento = value
        End Set
    End Property

    Public Property Porimpuesto1 As Double
        Get
            Return _porimpuesto1
        End Get
        Set(value As Double)
            _porimpuesto1 = value
        End Set
    End Property

    Public Property Porimpuesto2 As Double
        Get
            Return _porimpuesto2
        End Get
        Set(value As Double)
            _porimpuesto2 = value
        End Set
    End Property

    Public Property Porimpuesto3 As Double
        Get
            Return _porimpuesto3
        End Get
        Set(value As Double)
            _porimpuesto3 = value
        End Set
    End Property

    Public Property Totimpuesto0 As Double
        Get
            Return _totimpuesto0
        End Get
        Set(value As Double)
            _totimpuesto0 = value
        End Set
    End Property

    Public Property Totimpuesto1 As Double
        Get
            Return _totimpuesto1
        End Get
        Set(value As Double)
            _totimpuesto1 = value
        End Set
    End Property

    Public Property Totimpuesto2 As Double
        Get
            Return _totimpuesto2
        End Get
        Set(value As Double)
            _totimpuesto2 = value
        End Set
    End Property

    Public Property Nomcorto As String
        Get
            Return _nomcorto
        End Get
        Set(value As String)
            _nomcorto = value
        End Set
    End Property

    Public Property Total As Double
        Get
            Return _total
        End Get
        Set(value As Double)
            _total = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property
    Public Property PrecioBruto As Double
        Get
            Return _PrecioBruto
        End Get
        Set(value As Double)
            _PrecioBruto = value
        End Set
    End Property

    Public Property DE_Otorgado As Double
        Get
            Return _DE_Otorgado
        End Get
        Set(value As Double)
            _DE_Otorgado = value
        End Set
    End Property
End Class
