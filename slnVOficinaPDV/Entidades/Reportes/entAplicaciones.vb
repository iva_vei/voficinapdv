﻿Public Class entAplicaciones
    Dim _detalle As List(Of entAplicacion)

    Public Property Detalle As List(Of entAplicacion)
        Get
            Return _detalle
        End Get
        Set(value As List(Of entAplicacion))
            _detalle = value
        End Set
    End Property

    Public Sub Fill(ByVal otabla As DataTable)
        Dim renglon1 As entAplicacion
        _detalle = New List(Of entAplicacion)
        For Each oreng As System.Data.DataRow In otabla.Rows
            renglon1 = New entAplicacion
            renglon1.fecha_aplico = oreng.Item("fecha_aplico")
            renglon1.fecha = oreng.Item("fecha")
            renglon1.cve_documento = oreng.Item("cve_documento")
            renglon1.serie = oreng.Item("serie")
            renglon1.numero = oreng.Item("numero")
            renglon1.total = oreng.Item("total")
            renglon1.saldodocto = oreng.Item("saldodocto")
            renglon1.uuid = oreng.Item("uuid")
            renglon1.id_cliente = oreng.Item("id_cliente")
            renglon1.nombre = oreng.Item("nombre")
            renglon1.flujo = oreng.Item("flujo")
            renglon1.ecve_documento = oreng.Item("ecve_documento")
            renglon1.eserie = oreng.Item("eserie")
            renglon1.enumero = oreng.Item("enumero")
            renglon1.eautonumsuc = oreng.Item("eautonumsuc")
            renglon1.eimporte_aplicad = oreng.Item("eimporte_aplicad")
            renglon1.usuario = oreng.Item("usuario")
            _detalle.Add(renglon1)
        Next
    End Sub
End Class
Public Class entAplicacion
    Dim _fecha_aplico As Date
    Dim _fecha As Date
    Dim _cve_documento As String
    Dim _serie As String
    Dim _numero As Integer
    Dim _total As Double
    Dim _saldodocto As Double
    Dim _uuid As String
    Dim _id_cliente As String
    Dim _nombre As String
    Dim _flujo As String
    Dim _ecve_documento As String
    Dim _eserie As String
    Dim _enumero As Integer
    Dim _eautonumsuc As String
    Dim _eimporte_aplicad As Double
    Dim _usuario As String


    Public Property Fecha_aplico As Date
        Get
            Return _fecha_aplico
        End Get
        Set(value As Date)
            _fecha_aplico = value
        End Set
    End Property

    Public Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Public Property Cve_documento As String
        Get
            Return _cve_documento
        End Get
        Set(value As String)
            _cve_documento = value
        End Set
    End Property

    Public Property Serie As String
        Get
            Return _serie
        End Get
        Set(value As String)
            _serie = value
        End Set
    End Property

    Public Property Numero As Integer
        Get
            Return _numero
        End Get
        Set(value As Integer)
            _numero = value
        End Set
    End Property

    Public Property Total As Double
        Get
            Return _total
        End Get
        Set(value As Double)
            _total = value
        End Set
    End Property

    Public Property Saldodocto As Double
        Get
            Return _saldodocto
        End Get
        Set(value As Double)
            _saldodocto = value
        End Set
    End Property

    Public Property Uuid As String
        Get
            Return _uuid
        End Get
        Set(value As String)
            _uuid = value
        End Set
    End Property

    Public Property Id_cliente As String
        Get
            Return _id_cliente
        End Get
        Set(value As String)
            _id_cliente = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Flujo As String
        Get
            Return _flujo
        End Get
        Set(value As String)
            _flujo = value
        End Set
    End Property

    Public Property Ecve_documento As String
        Get
            Return _ecve_documento
        End Get
        Set(value As String)
            _ecve_documento = value
        End Set
    End Property

    Public Property Eserie As String
        Get
            Return _eserie
        End Get
        Set(value As String)
            _eserie = value
        End Set
    End Property

    Public Property Enumero As Integer
        Get
            Return _enumero
        End Get
        Set(value As Integer)
            _enumero = value
        End Set
    End Property

    Public Property Eautonumsuc As String
        Get
            Return _eautonumsuc
        End Get
        Set(value As String)
            _eautonumsuc = value
        End Set
    End Property

    Public Property Eimporte_aplicad As Double
        Get
            Return _eimporte_aplicad
        End Get
        Set(value As Double)
            _eimporte_aplicad = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)
            _usuario = value
        End Set
    End Property
End Class
