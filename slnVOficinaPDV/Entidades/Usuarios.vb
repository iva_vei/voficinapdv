﻿Imports System.Data.SqlClient
'Imports SVC_Encrypt
Imports EncryptNet

Namespace Globales
    ''' <summary>
    ''' Entidad para validar el usuario
    ''' </summary>
    ''' <remarks></remarks>
    Public Class clsUsuarios
        Private Const PA_WMS_USUARIO_SEL As String = "pa_WMS_Usuario_Sel"
        Private Const PA_WMS_PERMISOS_PANTALLAS_SEL As String = "pa_PDV_Permisos_Pantallas_Sel"
        Private Const ESQUEMA As String = "seguridad."

        Public Id_usuario As String
        Public Nombre_Usuario As String
        Public Pass As String
        Public Tipo_perfil As String
        Public Perfil As String
        Public Vence As Date
        Public Fecha_alta As Date
        Public Fecha_vence As Date

        Public Id_empleado As Integer



        'Private _usr_cnx As Per_Usuario_ConexionesCollection

        'Private _pantallas As DataTable
        Private _pantallas_perfiles As DataTable
        'Private _permisos As DataTable
        'Private _usuarios_permisos As DataTable

        'Private _rubros As RubrosCollection
        'Private _areas As Cat_AreasCollection
        'Private _empresas As Cat_EmpresasCollection
        'Private _plazas As PlazasCollection
        'Private _sucursales As SucursalesCollection
        'Private _grupo As Boolean

        Public Todas_pantallas As Boolean
        Public Todas_empresas As Boolean

        'Private _SW_tienda_todas_Regiones As Boolean
        'Private _SW_tienda_todas_SubRegiones As Boolean
        'Private _SW_tienda_todas_Plazas As Boolean
        'Private _SW_tienda_todas_Sucursales As Boolean

        'Private _id_depto_todo_list As Integer
        'Private _depto_todo_list As String
        'Private _id_area_todo_list As Integer
        'Private _area_todo_list As String
        Public dtEstructura As DataTable
        'Public dtMECs As DataTable

        Public Sub New()
            Id_usuario = 0
            Pass = ""

            Fecha_alta = CDate("1900/01/01")
            Fecha_vence = CDate("1900/01/01")
            Id_empleado = 0
            Tipo_perfil = 0
            Perfil = ""
            Nombre_Usuario = ""

            '_pantallas = Nothing
            '_permisos = Nothing
        End Sub
        Public Function EsUsuarioValido(ByVal usuario1 As String, ByVal pass1 As String, ByVal cnx_Seguridad As SqlConnection, ByRef Mensaje As String) As Boolean
            Dim Clave_Crypt As String = ""
            Dim Resultado As Boolean = False
            Dim cn As SqlConnection = Nothing
            Dim strCommand As New SqlCommand()
            Dim sADa As New SqlDataAdapter
            Dim dts As New DataSet

            Try
                Mensaje = ""
                cn = cnx_Seguridad
                strCommand.CommandText = PA_WMS_USUARIO_SEL
                strCommand.CommandType = CommandType.StoredProcedure
                strCommand.Connection = cn

                strCommand.Parameters.Add("@usuario", SqlDbType.VarChar)
                strCommand.Parameters("@usuario").Value = usuario1
                strCommand.Parameters.Add("@pass", SqlDbType.VarChar)
                strCommand.Parameters("@pass").Value = pass1

                cn.Open()

                sADa.SelectCommand = strCommand
                sADa.Fill(dts)

                If dts.Tables.Count > 0 Then
                    Mensaje = dts.Tables(0).Rows(0).Item("msj")

                    If dts.Tables(0).Rows(0).Item("regresa") = 1 Then
                        Resultado = True
                        Id_usuario = dts.Tables(0).Rows(0).Item("id_usuario")
                        Nombre_Usuario = dts.Tables(0).Rows(0).Item("nombre_usuario")
                        Tipo_perfil = dts.Tables(0).Rows(0).Item("tipo_perfil")
                        Perfil = dts.Tables(0).Rows(0).Item("perfil")
                        Fecha_alta = dts.Tables(0).Rows(0).Item("fecha_alta")
                        Fecha_vence = dts.Tables(0).Rows(0).Item("fecha_vence")
                        Id_empleado = dts.Tables(0).Rows(0).Item("id_empleado")
                        Globales.oAmbientes.Id_Empresa = dts.Tables(0).Rows(0).Item("id_empresa")
                        Globales.oAmbientes.Empresa = dts.Tables(0).Rows(0).Item("id_empresa")
                        Globales.oAmbientes.Id_Sucursal = dts.Tables(0).Rows(0).Item("id_sucursal")
                    Else
                        Resultado = False
                    End If
                End If
            Catch ex As Exception
                Mensaje = ex.Message
                Resultado = False
            Finally

                If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                    cn.Close()
                    cn.Dispose()
                End If
                If strCommand IsNot Nothing Then
                    strCommand.Dispose()
                End If


                strCommand = Nothing
                cn = Nothing
            End Try
            Return Resultado
        End Function
        Public Function Per_Pantallas_Permisos_SEL(ByVal glb_cnnControl As SqlConnection, ByRef Excepcion As String) As Boolean
            Dim Resultado As Boolean = False
            Dim cn As SqlConnection = Nothing
            Dim strCommand As New SqlCommand()
            Dim sADa As New SqlDataAdapter
            Dim dts As New DataSet

            Try
                cn = glb_cnnControl
                strCommand.CommandText = PA_WMS_PERMISOS_PANTALLAS_SEL
                strCommand.CommandType = CommandType.StoredProcedure
                strCommand.Connection = cn

                strCommand.Parameters.Add("@perfil", SqlDbType.VarChar)
                strCommand.Parameters("@perfil").Value = Perfil
                strCommand.Parameters.Add("@tipo_perfil", SqlDbType.VarChar)
                strCommand.Parameters("@tipo_perfil").Value = Tipo_perfil

                cn.Open()

                sADa.SelectCommand = strCommand
                sADa.Fill(dts)

                If dts.Tables.Count > 0 Then
                    _pantallas_perfiles = dts.Tables(0)
                    '_usuarios_permisos = dts.Tables(3)
                End If
                Resultado = True
                Excepcion = "NO_ERROR"

            Catch ex As Exception
                Excepcion = ex.Message
                Resultado = False
            Finally

                If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                    cn.Close()
                    cn.Dispose()
                End If
                If strCommand IsNot Nothing Then
                    strCommand.Dispose()
                End If

                strCommand = Nothing
                cn = Nothing
            End Try
            Return Resultado
        End Function
        Public Function ValidaPermiso(ByVal sPantalla As String, ByVal sPermiso As String) As Boolean
            Dim sw_permiso = False
            Try
                sPermiso = sPermiso.ToUpper

                If Todas_pantallas Then
                    Return True
                Else

                    If sPantalla.IndexOf(".") > 0 Then
                        sPantalla = sPantalla.Split(".")(1).ToUpper()
                    Else
                        sPantalla = sPantalla.ToUpper()
                    End If

                    Dim list2 = From p In _pantallas_perfiles
                                Select pantalla = p.Item("pantalla").ToString.ToUpper _
                                      , permiso = p.Item("permiso").ToString.ToUpper


                    Dim list1 = From p In list2
                                Where p.pantalla = sPantalla.ToString.ToUpper _
                                And (p.permiso = sPermiso.ToString.ToUpper Or p.permiso = "TODOS")
                                Select pantalla = p.pantalla, permiso = p.permiso

                    sw_permiso = False

                    If list1.Count > 0 Then
                        sw_permiso = True
                    End If
                End If
            Catch ex As Exception
                sw_permiso = False
            End Try

            If Globales.oAmbientes.Id_Empresa = "SIXTY" Then
                sw_permiso = True
            End If
            Return sw_permiso
        End Function

    End Class
End Namespace