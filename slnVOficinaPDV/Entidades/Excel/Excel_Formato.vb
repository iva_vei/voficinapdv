﻿Imports System
Imports System.Collections.Generic
Imports System.Text

''' <summary>
''' Entidad que almacena los formato del Excel
''' </summary>
''' <remarks></remarks>


Public Class Excel_Formato
    Private _renglon As Integer
    Private _columna As Integer
    Private _formato As Integer
    Private _letraColor As Integer
    Private _colorFondo As Integer
    Private _aplicaRenglon As Boolean
    Private _aplicaColumna As Boolean
    Private _imagen As System.Drawing.Image
    Public Property Renglon() As Integer
        Get
            Return _renglon
        End Get
        Set(ByVal value As Integer)
            _renglon = value
        End Set
    End Property
    Public Property Columna() As Integer
        Get
            Return _columna
        End Get
        Set(ByVal value As Integer)
            _columna = value
        End Set
    End Property
    Public Property Formato() As Integer
        Get
            Return _formato
        End Get
        Set(ByVal value As Integer)
            _formato = value
        End Set
    End Property
    Public Property LetraColor() As Integer
        Get
            Return _letraColor
        End Get
        Set(ByVal value As Integer)
            _letraColor = value
        End Set
    End Property
    Public Property ColorFondo() As Integer
        Get
            Return _colorFondo
        End Get
        Set(ByVal value As Integer)
            _colorFondo = value
        End Set
    End Property
    Public Property AplicaRenglon() As Boolean
        Get
            Return _aplicaRenglon
        End Get
        Set(ByVal value As Boolean)
            _aplicaRenglon = value
        End Set
    End Property
    Public Property AplicaColumna() As Boolean
        Get
            Return _aplicaColumna
        End Get
        Set(ByVal value As Boolean)
            _aplicaColumna = value
        End Set
    End Property

    Public Property Imagen() As System.Drawing.Image
        Get
            Return _imagen
        End Get
        Set(ByVal value As System.Drawing.Image)
            _imagen = value
        End Set
    End Property
    Public Sub New()
        Renglon = 0
        Columna = 0
        Formato = 0
        LetraColor = 0
        ColorFondo = 0
        AplicaRenglon = False
        AplicaColumna = False
        Imagen = Nothing

    End Sub
End Class
