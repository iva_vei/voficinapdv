﻿Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Text

Public Class Excel_FormatoCollection
    Inherits CollectionBase
    Implements IList

    Public Sub New()
        ' 
        ' TODO: Add constructor logic here 
        ' 
    End Sub
    ''' <summary> 
    ''' Propiedad índice que obtiene un elemento especifico 
    ''' en base a su índice 
    ''' </summary> 
    Default Public Property Item(ByVal index As Integer) As Excel_Formato
        Get
            Return DirectCast(List(index), Excel_Formato)
        End Get
        Set(ByVal value As Excel_Formato)
            List(index) = value
        End Set
    End Property
    ''' <summary> 
    ''' Permite Copiar un arreglo de Parametros a la colección Actual 
    ''' </summary> 
    ''' <param name="array">arreglo de elementos a copiar</param> 
    ''' <param name="index">Indice en donde la copia comienza</param> 
    Public Sub CopyTo(ByVal array As Excel_Formato(), ByVal index As Integer)
        List.CopyTo(array, index)
    End Sub

    ''' <summary> 
    ''' Permite obtener el indice en base a un objeto específico 
    ''' </summary> 
    ''' <param name="value">instancia del cual obtener su índice</param> 
    ''' <returns>el índice basado en 0 del elemento encontrado, 
    ''' en caso contrario regresa menor a 0</returns> 
    Public Function IndexOf(ByVal value As Excel_Formato) As Integer
        Return (List.IndexOf(value))
    End Function
    ''' <summary> 
    ''' Agrega una instancia al final de la colección 
    ''' </summary> 
    ''' <param name="value">Instancia a agregar</param> 
    ''' <returns>índice del elemento agragado</returns> 
    Public Function Add(ByVal value As Excel_Formato) As Integer
        Return (List.Add(value))
    End Function
    ''' <summary> 
    ''' Inserta una instancia a la colección, en la posición del índice 
    ''' asignado 
    ''' </summary> 
    ''' <param name="index">Posición donde será insertado la instancia</param> 
    ''' <param name="value">La instancia a Insertar</param> 
    Public Sub Insert(ByVal index As Integer, ByVal value As Excel_Formato)
        List.Insert(index, value)
    End Sub
    ''' <summary> 
    ''' Permite verificar si una instancia esta contenida dentro de 
    ''' la colección 
    ''' </summary> 
    ''' <param name="value">La instancia a evaluar</param> 
    ''' <returns>true la instancia existe en la colección, false no existe 
    ''' la instancia en la colección.</returns> 
    Public Function Contains(ByVal value As Excel_Formato) As Boolean
        Return (List.Contains(value))
    End Function
    ''' <summary> 
    ''' Remueve una instancia de la colección 
    ''' </summary> 
    ''' <param name="value">La instancia a remover de la colección</param> 
    Public Sub Remove(ByVal value As Excel_Formato)
        List.Remove(value)
    End Sub
    ''' <summary> 
    ''' Metodo sobrecargado que verifica un elemento antes 
    ''' de insertarlo a la colección 
    ''' </summary> 
    ''' <param name="index">La posición de la instancia a insertar</param> 
    ''' <param name="value">La instancia a insertar</param> 
    Protected Overloads Overrides Sub OnInsert(ByVal index As Integer, ByVal value As [Object])
        If (TypeOf value Is Excel_Formato) = False Then
            Throw New ArgumentException("value must be of type Excel_FormatoCollection.", "value")
        End If
    End Sub
    ''' <summary> 
    ''' Metodo sobrecargado que se invoca antes de remover una 
    ''' instancia de la colección 
    ''' </summary> 
    ''' <param name="index">La posición de la instancia a remover</param> 
    ''' <param name="value">La instancia a remover</param> 
    Protected Overloads Overrides Sub OnRemove(ByVal index As Integer, ByVal value As [Object])
        If (TypeOf value Is Excel_Formato) = False Then
            Throw New ArgumentException("value must be of type Excel_FormatoCollection.", "value")
        End If
    End Sub
    ''' <summary> 
    ''' Metodo sobrecargado que se invoca antes de asignar un elemento 
    ''' en una posición especifica. 
    ''' </summary> 
    ''' <param name="index">La posición donde se inserta el nuevo elemento</param> 
    ''' <param name="oldValue">La instancia anterior a sobreescribir</param> 
    ''' <param name="newValue">La instancia a asignar</param> 
    Protected Overloads Overrides Sub OnSet(ByVal index As Integer, ByVal oldValue As [Object], ByVal newValue As [Object])
        If (TypeOf newValue Is Excel_Formato) = False Then
            Throw New ArgumentException("newValue must be of type Excel_FormatoCollection.", "newValue")
        End If
    End Sub
    ''' <summary> 
    ''' Metodo sobrecargado que se invoca antes de asignar un valor para validar 
    ''' el tipo de la instancia a insertar 
    ''' </summary> 
    ''' <param name="value">La instancia a validar</param> 
    Protected Overloads Overrides Sub OnValidate(ByVal value As [Object])
        If (TypeOf value Is Excel_Formato) = False Then
            Throw New ArgumentException("value must be of type Excel_FormatoCollection.", "value")
        End If
    End Sub
End Class
