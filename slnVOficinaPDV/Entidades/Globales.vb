﻿Imports System.Data.SqlClient
'Imports Microsoft.Office.Interop

Namespace Globales
    Enum e_TIPODATO
        ALFANUMERICO = 0
        NUMERICO = 1
        FECHA = 2
        NUMERICO1 = 3
    End Enum
    Enum TipoABC
        Indice
        Nuevo
        Consulta
        Modificar
        Eliminar
        Autorizar
    End Enum
    Public Enum TipoCombo
        Areas
        Fecha
        Proveedores
        Pedidos
        Ruta
        Sucursales
        Sucursales_CEDIS
        Ubicaciones_Mesa
        Asentamientos
    End Enum

    Public Class oAmbientes
        Public Shared oControl As clsConexion
        Public Shared oViscoi As clsConexion
        Public Shared oUsuario As clsUsuarios
        Public Shared oFormulario As clsFormularios
        Public Shared oPinPadBanorte As clsPinPadBanorte
        Public Shared PinPadBanco As String
        Public Shared Sistema As String
        Public Shared Version As String
        Public Shared SW_Version As Boolean
        Public Shared SW_Produccion As Boolean
        Public Shared FechaEjecucion As Date = Today
        Public Shared Servidor As String
        Public Shared Id_Empresa As String
        Public Shared Empresa As String
        Public Shared BaseDatos As String
        Public Shared Id_Sucursal As String
        Public Shared Jerarquia01 As String = ""
        Public Shared Jerarquia02 As String = ""
        Public Shared Jerarquia03 As String = ""
        Public Shared Jerarquia04 As String = ""
        Public Shared ImagenAvisosTicket As String
        Public Shared TipoEmpresa As String
        Public Shared PaletaColores(10) As Color

        Public Shared Function Redondear(ByVal Valor As Decimal, ByVal Decimales As Integer, Optional ByVal HaciaArriba As Boolean = False) As Decimal
            Dim iRedondeo6 As Decimal = 0.000001
            Dim iRedondeo5 As Decimal = 0.000001
            Dim iRedondeo4 As Decimal = 0.0001
            Dim iRedondeo2 As Decimal = 0.01
            Dim NewValor As Decimal = 0.0

            If HaciaArriba Then
                NewValor = Math.Ceiling(Valor * 100.0) / 100.0
            Else
                NewValor = Math.Round(Valor, 2)
            End If
            Select Case Decimales
                Case 6
                    NewValor += iRedondeo6
                    NewValor -= iRedondeo6
                Case 5
                    NewValor += iRedondeo5
                    NewValor -= iRedondeo5
                Case 4
                    NewValor += iRedondeo4
                    NewValor -= iRedondeo4
                Case 2
                    NewValor += iRedondeo2
                    NewValor -= iRedondeo2
                Case Else
                    NewValor += iRedondeo6
                    NewValor -= iRedondeo6
            End Select

            Return NewValor
        End Function
        Public Shared Function Redondear4(ByVal Valor As Decimal, ByVal Decimales As Integer, Optional ByVal HaciaArriba As Boolean = False) As Decimal
            Dim iRedondeo6 As Decimal = 0.000001
            Dim iRedondeo5 As Decimal = 0.000001
            Dim iRedondeo4 As Decimal = 0.0001
            Dim iRedondeo2 As Decimal = 0.01
            Dim NewValor As Decimal = 0.0

            If HaciaArriba Then
                NewValor = Math.Ceiling(Valor * 10000.0) / 10000.0
            Else
                NewValor = Math.Round(Valor, 4)
            End If
            Select Case Decimales
                Case 6
                    NewValor += iRedondeo6
                    NewValor -= iRedondeo6
                Case 5
                    NewValor += iRedondeo5
                    NewValor -= iRedondeo5
                Case 4
                    NewValor += iRedondeo4
                    NewValor -= iRedondeo4
                Case 2
                    NewValor += iRedondeo2
                    NewValor -= iRedondeo2
                Case Else
                    NewValor += iRedondeo6
                    NewValor -= iRedondeo6
            End Select

            Return NewValor
        End Function
        Public Shared Function Redondear6(ByVal Valor As Decimal, ByVal Decimales As Integer, Optional ByVal HaciaArriba As Boolean = False) As Decimal
            Dim iRedondeo6 As Decimal = 0.000001
            Dim iRedondeo5 As Decimal = 0.000001
            Dim iRedondeo4 As Decimal = 0.0001
            Dim iRedondeo2 As Decimal = 0.01
            Dim NewValor As Decimal = 0.0

            If HaciaArriba Then
                NewValor = Math.Ceiling(Valor * 1000000.0) / 1000000.0
            Else
                NewValor = Math.Round(Valor, 6)
            End If
            Select Case Decimales
                Case 6
                    NewValor += iRedondeo6
                    NewValor -= iRedondeo6
                Case 5
                    NewValor += iRedondeo5
                    NewValor -= iRedondeo5
                Case 4
                    NewValor += iRedondeo4
                    NewValor -= iRedondeo4
                Case 2
                    NewValor += iRedondeo2
                    NewValor -= iRedondeo2
                Case Else
                    NewValor += iRedondeo6
                    NewValor -= iRedondeo6
            End Select

            Return NewValor
        End Function
        Public Shared Function hexToRbgNew(ByVal Hex As String) As Color
            Hex = Replace(Hex, "#", "")
            Dim red As String = "&H" & Hex.Substring(0, 2)
            'Hex = Replace(Hex, red, "", , 1)
            Dim green As String = "&H" & Hex.Substring(2, 2)
            'Hex = Replace(Hex, green, "", , 1)
            Dim blue As String = "&H" & Hex.Substring(4, 2)
            'Hex = Replace(Hex, blue, "", , 1)
            Return System.Drawing.Color.FromArgb(red, green, blue)
        End Function
        Public Shared Function Crypt(ByVal Cadena As String, Optional ByVal Factor As Double = 34567) As String
            'Definir la Clase de Encriptamiento
            'que proviene del Encrypt.Dll
            'que se encuentra en \VisCoi\Clases\Svc\Encrypt
            Dim Encrypt = New EncryptNet.cEncrypt
            With Encrypt
                .Texto = Cadena
                .Accion = 1
                .Clave = Factor '34567 'Segunda Parte de la Clave
                Crypt = .f_Encrypt
            End With
            Encrypt = Nothing
        End Function
        ''' <summary>
        ''' Función que actualiza en regedit la ruta de la ultima version de click once para el inicio automatico al arrancar Windows
        ''' </summary>
        ''' <remarks>IVA</remarks>        ''' 
        Public Shared Sub SaveSettingInicioAutomatico(ByVal opcion As Integer, ByVal appName As String, ByVal section As String, ByVal key As String, ByVal setting As String)
            ' Los datos se guardan en: 

            Const RAMA_WINDOWS_RUN As String = "Software\Microsoft\Windows\CurrentVersion\Run\"
            Const RAMA_WINDOWS_RUNCITRIX As String = "Software\Microsoft\Windows NT\CurrentVersion\Run\"

            If opcion = 0 Then
                Dim rk1 As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(RAMA_WINDOWS_RUN)
                rk1.SetValue(key, setting)
            Else
                Dim rk2 As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(RAMA_WINDOWS_RUNCITRIX)
                rk2.SetValue(key, setting)
            End If

        End Sub
        ''' <summary>
        ''' Función que actualiza en regedit la ruta de la ultima version de click once para el inicio automatico al arrancar Windows
        ''' </summary>
        ''' <remarks>IVA</remarks>        ''' 
        Public Shared Function Valor(ByVal Cantidad As Object) As Double
            If IsDBNull(Cantidad) Then
                Valor = 0.0
            Else
                If Cantidad.ToString.Contains(",") Then
                    Cantidad = Cantidad.ToString().Replace(",", "")
                End If
                If Cantidad.ToString.Contains("%") Then
                    Cantidad = Cantidad.ToString().Replace("%", "")
                End If
                If Cantidad.ToString.Contains("$") Then
                    Cantidad = Cantidad.ToString().Replace("$", "")
                End If
                If IsNumeric(Cantidad) Then
                    Valor = Val(Cantidad)
                Else
                    Valor = 0.0
                End If
            End If
        End Function
        Public Shared Function LeerArchivoBin(ByVal sRuta As String) As Byte()
            Dim bDatos As Byte()
            Dim fInfo As New System.IO.FileInfo(sRuta)
            Dim numBytes As Long = fInfo.Length
            Using ArchivoStream = New System.IO.FileStream(sRuta, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Using ArchivoBin As New System.IO.BinaryReader(ArchivoStream)
                    bDatos = Nothing
                    bDatos = ArchivoBin.ReadBytes(numBytes)
                    ArchivoBin.Close()
                    Return bDatos
                End Using
            End Using
        End Function
        Public Shared Function LeerArchivoBin(ByVal Imagen As Image) As Byte()
            Dim bDatos As Byte()
            Using ms As System.IO.MemoryStream = New System.IO.MemoryStream
                Imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg) ' Use appropriate format here
                bDatos = ms.ToArray()
                Return bDatos
            End Using
        End Function
        Public Shared Function LeerImagenSegura(ByVal sRuta As String) As Image
            Using ArchivoStream = New System.IO.FileStream(sRuta, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Return Image.FromStream(ArchivoStream)
            End Using
        End Function
        Public Shared Function EnviarArchivosFTP(ftpAddress As String, ftpUser As String, ftpPassword As String,
                               fileToUpload As String, targetFileName As String,
                               deleteAfterUpload As Boolean,
                               ExceptionInfo As Exception) As Boolean

            Dim credential As System.Net.NetworkCredential

            Try
                credential = New System.Net.NetworkCredential(ftpUser, ftpPassword)

                If ftpAddress.EndsWith("/") = False Then ftpAddress = ftpAddress & "/"

                Dim sFtpFile As String = ftpAddress & fileToUpload

                Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(sFtpFile), System.Net.FtpWebRequest)

                request.KeepAlive = False
                request.Method = System.Net.WebRequestMethods.Ftp.UploadFile
                request.Credentials = credential
                request.UsePassive = False
                request.Timeout = (60 * 1000) * 3 '3 mins

                Using reader As New IO.FileStream(fileToUpload, IO.FileMode.Open)

                    Dim buffer(Convert.ToInt32(reader.Length - 1)) As Byte
                    reader.Read(buffer, 0, buffer.Length)
                    reader.Close()

                    request.ContentLength = buffer.Length
                    Dim stream As IO.Stream = request.GetRequestStream
                    stream.Write(buffer, 0, buffer.Length)
                    stream.Close()

                    Using response As System.Net.FtpWebResponse = DirectCast(request.GetResponse, System.Net.FtpWebResponse)

                        If deleteAfterUpload Then
                            My.Computer.FileSystem.DeleteFile(fileToUpload)
                        End If

                        response.Close()
                    End Using

                End Using

                Return True

            Catch ex As Exception
                ExceptionInfo = ex
                Return False
            Finally

            End Try

        End Function
        Public Shared Function ImagenResize(vImagen As Image, vWidth As Integer, vHeight As Integer, Sw_MantenerAspecto As Boolean) As Image
            Dim Imagen As Image
            Dim newWidth As Integer
            Dim newHeight As Integer
            Dim originalWidth As Integer
            Dim originalHeight As Integer
            Dim percentWidth As Double
            Dim percentHeight As Double
            Dim percent As Double
            If Sw_MantenerAspecto Then
                originalWidth = vImagen.Width
                originalHeight = vImagen.Height
                percentWidth = CDbl(vWidth) / CDbl(originalWidth)
                percentHeight = CDbl(vHeight) / CDbl(originalHeight)
                percent = IIf(percentHeight < percentWidth, percentHeight, percentWidth)
                newWidth = CInt(originalWidth * percent)
                newHeight = CInt(originalHeight * percent)
            Else
                newWidth = vWidth
                newHeight = vHeight
            End If

            Imagen = New Bitmap(vImagen, New Size(newWidth, newHeight))
            Return Imagen.Clone()
        End Function
        Public Shared Function ImportarExcel(ByVal cxn_str As String, ByVal hoja As String, ByVal rango As String, ByRef dtTabla As DataTable, ByRef Msj As String) As Boolean
            Dim regresa As Boolean = False
            Try
                'declarando las variables
                Dim MyConnection As System.Data.OleDb.OleDbConnection
                Dim DtSet As System.Data.DataSet
                Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
                'declarando variable de conexión
                MyConnection = New System.Data.OleDb.OleDbConnection _
                (cxn_str)
                'creando consulta para extraer contenido del archivo de excel y ejecutandola
                MyCommand = New System.Data.OleDb.OleDbDataAdapter _
                    ("select * from [" & hoja & "$" & rango & "]", MyConnection)
                MyCommand.TableMappings.Add("Table", "TestTable")
                DtSet = New System.Data.DataSet
                MyCommand.Fill(DtSet)
                'tabla.DataSet = DtSet.
                'ubicando el resultado en el datagridview
                dtTabla = DtSet.Tables(0)
                'cerrando conexión
                MyConnection.Close()

                regresa = True
                Msj = "Exito"
            Catch ex As Exception
                'imprimiendo mensaje de error
                regresa = False
                Msj = ex.ToString
            End Try
            Return regresa
        End Function
    End Class
    Public Class clsConexion
        Private strServer As String = ""
        Private strDb As String = ""
        Private strUser As String = ""
        Private strPass As String = ""
        Public Function cnx_Seguridad(ByVal Server As String, ByVal DB As String, ByVal user As String, ByVal pass As String) As String
            Dim cnx As String
            strServer = Server
            strDb = DB
            strUser = user
            strPass = pass
            cnx = "Data Source =" & strServer.Trim & ";" _
                        & "Initial Catalog=" & strDb.Trim & ";" _
                        & "Persist Security Info=False;" _
                        & "User ID=" & strUser.Trim & ";" _
                        & "Password=" & strPass.Trim & ";" _
                        & "Packet Size = 4096; Max Pool Size=200;" _
                        & "Connect Timeout=45"
            Return cnx
        End Function
        Private Function cnx_Seguridad() As String
            Dim cnx As String
            cnx = "Data Source =" & strServer.Trim & ";" _
                        & "Initial Catalog=" & strDb.Trim & ";" _
                        & "Persist Security Info=False;" _
                        & "User ID=" & strUser.Trim & ";" _
                        & "Password=" & strPass.Trim & ";" _
                        & "Packet Size = 4096; Max Pool Size=200;" _
                        & "Connect Timeout=45"
            Return cnx
        End Function
        Public ReadOnly Property ObtenerConexion() As SqlConnection
            Get
                Dim cnx As SqlConnection
                cnx = New SqlConnection(cnx_Seguridad)
                Return cnx
            End Get
        End Property
    End Class
    Public Class clsFormularios
        Friend WithEvents Sfd_Dialogo As System.Windows.Forms.SaveFileDialog
        Friend WithEvents Ofd_Dialogo As System.Windows.Forms.OpenFileDialog
        Friend WithEvents Link As DevExpress.XtraPrinting.PrintableComponentLink
        Friend WithEvents Ps As DevExpress.XtraPrinting.PrintingSystemBase

        Private Shared _msj_ultimonivel As String
        Private Shared _msj_error As String
        Private Shared _tit_collapse As String
        Private Shared _tit_aviso As String
        Private Shared _frm_Actual As String
        Private Shared _frm_Anterior As String
        Private Shared _frm_Principal As String
        Private Shared _msj_sinregistros As String
        Private Shared _msj_excepcion As String
        Private Shared _msj_error_combo As String

        Private ReadOnly Sw_Grid_Filtros As Boolean = False

        Private ReadOnly Formatos As New Excel_FormatoCollection
        Private ImagenEjemplo As System.Drawing.Image
        Public Sub New()
            Msj_UltimoNivel = "No hay más profundidades para esta opción"
            Tit_Collapse = "De CLICK para Desplegar u Ocultar Filtros"
            Msj_Error = "Se generó una excepción:"
            Tit_Aviso = "Aviso del Sistema"
            Frm_Anterior = ""
            Frm_Actual = ""
            Frm_Principal = "IND_TABDIRECCIONH"
            Msj_Excepcion = "Hubo un error al buscar la información: "
            Msj_SinRegistros = "No se encontró información"
            Msj_error_combo = "El valor #COMBO# no se encuentra en la lista "
            'Msj_sinacceso = "El usuario #USUARIO# con el perfil de seguridad #PERFIL#," & vbNewLine & _
            '                "No tiene acceso a la forma #FORMA# del Sistema " & Globales.oAmbientes.Sistema & vbNewLine & _
            '                "Consulte con Sistemas Operaciones, los permisos asignandos para esta pantalla."

        End Sub

        Public Property Msj_Excepcion() As String
            Get
                Return _msj_excepcion
            End Get
            Set(ByVal value As String)
                _msj_excepcion = value
            End Set
        End Property

        Public Property Msj_SinRegistros() As String
            Get
                Return _msj_sinregistros
            End Get
            Set(ByVal value As String)
                _msj_sinregistros = value
            End Set
        End Property


        Public Property Frm_Principal() As String
            Get
                Return _frm_Principal
            End Get
            Set(ByVal value As String)
                _frm_Principal = value
            End Set
        End Property

        Public Property Frm_Anterior() As String
            Get
                Return _frm_Anterior
            End Get
            Set(ByVal value As String)
                _frm_Anterior = value
            End Set
        End Property

        Public Property Frm_Actual() As String
            Get
                Return _frm_Actual
            End Get
            Set(ByVal value As String)
                _frm_Actual = value
            End Set
        End Property

        Public Property Msj_Error() As String
            Get
                Return _msj_error
            End Get
            Set(ByVal value As String)
                _msj_error = value
            End Set
        End Property

        Public Property Msj_UltimoNivel() As String
            Get
                Return _msj_ultimonivel
            End Get
            Set(ByVal value As String)
                _msj_ultimonivel = value
            End Set
        End Property

        Public Property Msj_error_combo() As String
            Get
                Return _msj_error_combo
            End Get
            Set(ByVal value As String)
                _msj_error_combo = value
            End Set
        End Property
        Public Property Tit_Collapse() As String
            Get
                Return _tit_collapse
            End Get
            Set(ByVal value As String)
                _tit_collapse = value
            End Set
        End Property

        Public Property Tit_Aviso() As String
            Get
                Return _tit_aviso
            End Get
            Set(ByVal value As String)
                _tit_aviso = value
            End Set
        End Property
        Public ReadOnly Property Msj_sinacceso(ByVal Pantalla As String) As String
            Get
                Dim Msj As String = "El usuario #ID_USUARIO# con el perfil de seguridad #PERFIL#," & vbNewLine &
                            "No tiene acceso a la forma #FORMA# del Sistema " & Globales.oAmbientes.Sistema & vbNewLine &
                            "Consulte con Sistemas Operaciones, los permisos asignandos para esta pantalla."

                Msj = Msj.Replace("#ID_USUARIO#", Globales.oAmbientes.oUsuario.Id_usuario)
                Msj = Msj.Replace("#PERFIL#", Globales.oAmbientes.oUsuario.Perfil)
                Msj = Msj.Replace("#FORMA#", Pantalla)

                Return Msj
            End Get
        End Property
        Public ReadOnly Property Msj_sinacceso(ByVal Pantalla As String, ByVal Permiso As String) As String
            Get
                Dim Msj As String = "El usuario #ID_USUARIO# con el perfil de seguridad #PERFIL#," & vbNewLine &
                            "No cuenta con el permiso #PERMISO# en la forma #FORMA# del Sistema " & Globales.oAmbientes.Sistema & vbNewLine &
                            "Consulte con Sistemas Operaciones, los permisos asignandos para esta pantalla."

                Msj = Msj.Replace("#ID_USUARIO#", Globales.oAmbientes.oUsuario.Id_usuario)
                Msj = Msj.Replace("#PERFIL#", Globales.oAmbientes.oUsuario.Perfil)
                Msj = Msj.Replace("#FORMA#", Pantalla)
                Msj = Msj.Replace("#PERMISO#", Permiso)

                Return Msj
            End Get
        End Property
        Public Property ImagenEjemplo1 As Image
            Get
                Return ImagenEjemplo
            End Get
            Set(value As Image)
                ImagenEjemplo = value
            End Set
        End Property

        ''' <summary>
        ''' Obtiene el valor de la celda seleccionada del grid, espeficando el nombre de la columna (fieldname)
        ''' </summary>
        ''' <param name="Name">Nombre de la Forma</param>
        ''' <param name="Text">Text de la Forma</param>
        ''' <param name="Orientacion"> 1 - Vertical
        '''                            0 - Horizontal </param>
        ''' <param name="Tipo_Hoja">Tamaño de la Hoja</param>
        ''' <param name="parm_sCabecero_Izq">Cabecero situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sCabecero_Cen">Cabecero situado en la parte central de la hoja</param>
        ''' <param name="parm_sCabecero_Der">Cabecero situado en la parte derecha de la hoja</param>
        ''' <param name="parm_sPiePag_Izq">Pie de Pagina situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sPiePag_Cen">Pie de Pagina situado en la parte central de la hoja</param>
        ''' <param name="parm_sPiePag_Der">Pie de Pagina situado en la parte derecha de la hoja</param>
        ''' <returns>True - si genero la reporte</returns>
        ''' <remarks>Ing. Ivan Valdes Acosta</remarks>
        Public Function Imprimir(ByVal Name As String, ByVal Text As String, ByVal Grid As DevExpress.XtraGrid.GridControl _
                             , Optional ByVal Orientacion As Integer = 1 _
                             , Optional ByVal Tipo_Hoja As System.Drawing.Printing.PaperKind = Printing.PaperKind.Tabloid _
                             , Optional ByVal parm_sCabecero_Izq As String = "" _
                             , Optional ByVal parm_sCabecero_Cen As String = "" _
                             , Optional ByVal parm_sCabecero_Der As String = "" _
                             , Optional ByVal parm_sPiePag_Izq As String = "" _
                             , Optional ByVal parm_sPiePag_Cen As String = "" _
                             , Optional ByVal parm_sPiePag_Der As String = "" _
                             , Optional ByVal Magen_Top As Integer = 50 _
                             , Optional ByVal Magen_Bottom As Integer = 5 _
                             , Optional ByVal Magen_Left As Integer = 10 _
                             , Optional ByVal Magen_Right As Integer = 10 _
                             , Optional ByVal Escala As Single = 0.9) As Boolean

            'If Not Entidades.CN_Globales.Usuario.ValidaPermiso(Name, "IMPRIMIR") Then
            '    MsgBox(Entidades.Formularios.Msj_sinacceso(Name, "IMPRIMIR"), MsgBoxStyle.OkOnly, Name)
            '    Return False
            'End If
            Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
            Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink

            Dim sPage_N_M As String = "Pág [Page #]"
            Dim sDate As String = Format(Now, "dd/MMM/yyyy")
            Dim sTime As String = Format(Now, "hh:mm:ss")
            Dim sUser As String = "[User Name]"

            Dim Cabecero_Izq As String = IIf(parm_sCabecero_Izq <> "", parm_sCabecero_Izq, (Name))
            Dim Cabecero_Cen As String = IIf(parm_sCabecero_Cen <> "", ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text & vbNewLine & parm_sCabecero_Cen), ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text))
            Dim Cabecero_Der As String = IIf(parm_sCabecero_Der <> "", (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime) & vbNewLine & parm_sCabecero_Der, (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime))

            Dim PiePag_Izq As String = IIf(parm_sPiePag_Izq <> "", parm_sPiePag_Izq, (""))
            Dim PiePag_Cen As String = IIf(parm_sPiePag_Cen <> "", parm_sPiePag_Cen, (""))
            Dim PiePag_Der As String = IIf(parm_sPiePag_Der <> "", parm_sPiePag_Der, (sUser))

            Dim GridAllowCellMerge As Boolean
            Try
                'Ps.Begin()
                Select Case Grid.MainView.GetType.Name
                    Case "BandedGridView"
                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
                        ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
                    Case "GridView"
                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
                        ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
                    Case "AdvBandedGridView"
                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
                        'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
                End Select
                Dim phf_encabezado_pie = New DevExpress.XtraPrinting.PageHeaderFooter(New DevExpress.XtraPrinting.PageHeaderArea(New String() {Cabecero_Izq, Cabecero_Cen, Cabecero_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near), New DevExpress.XtraPrinting.PageFooterArea(New String() {PiePag_Izq, PiePag_Cen, PiePag_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near))
                'Link.Images.Add(My.Resources.Requisicion)
                'Dim Brick As DevExpress.XtraPrinting.TextBrick = e.Graph.DrawString(reportHeader, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)

                If Orientacion = 1 Then
                    Link.Landscape = True
                Else
                    Link.Landscape = False
                End If

                Ps.Document.ScaleFactor = Escala

                Link.PaperKind = Tipo_Hoja
                Link.Margins.Top = Magen_Top
                Link.Margins.Bottom = Magen_Bottom
                Link.Margins.Left = Magen_Left
                Link.Margins.Right = Magen_Right
                Link.PrintingSystem = Ps
                Link.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
                Link.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
                Link.PrintingSystem.PreviewFormEx.LookAndFeel.SkinName = ""

                Link.Component = Grid
                Link.PageHeaderFooter = phf_encabezado_pie


                Link.CreateDocument()
                'Ps.End()
                Link.PrintingSystem.PreviewFormEx.Show()

                'Link.ShowPreview()

                Select Case Grid.MainView.GetType.Name
                    Case "BandedGridView"
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                    Case "GridView"
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                    Case "AdvBandedGridView"
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                End Select
            Catch ex As Exception
                MsgBox("Error al Imprimir. " & ex.Message, MsgBoxStyle.OkOnly, Name)
                Ps.Dispose()
                Link.Dispose()
            Finally

            End Try

            Return True
        End Function

        ''' <summary>
        ''' Obtiene el valor de la celda seleccionada del grid, espeficando el nombre de la columna (fieldname)
        ''' </summary>
        ''' <param name="Name">Nombre de la Forma</param>
        ''' <param name="Text">Text de la Forma</param>
        ''' <param name="Orientacion"> 1 - Vertical
        '''                            0 - Horizontal </param>
        ''' <param name="Tipo_Hoja">Tamaño de la Hoja</param>
        ''' <param name="parm_sCabecero_Izq">Cabecero situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sCabecero_Cen">Cabecero situado en la parte central de la hoja</param>
        ''' <param name="parm_sCabecero_Der">Cabecero situado en la parte derecha de la hoja</param>
        ''' <param name="parm_sPiePag_Izq">Pie de Pagina situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sPiePag_Cen">Pie de Pagina situado en la parte central de la hoja</param>
        ''' <param name="parm_sPiePag_Der">Pie de Pagina situado en la parte derecha de la hoja</param>
        ''' <returns>True - si genero la reporte</returns>
        ''' <remarks>Ing. Ivan Valdes Acosta</remarks>
        Public Function Imprimir2(ByVal Name As String, ByVal Text As String, ByVal Grids As Object() _
                             , Optional ByVal Orientacion As Integer = 1 _
                             , Optional ByVal Tipo_Hoja As System.Drawing.Printing.PaperKind = Printing.PaperKind.Letter _
                             , Optional ByVal parm_sCabecero_Izq As String = "" _
                             , Optional ByVal parm_sCabecero_Cen As String = "" _
                             , Optional ByVal parm_sCabecero_Der As String = "" _
                             , Optional ByVal parm_sPiePag_Izq As String = "" _
                             , Optional ByVal parm_sPiePag_Cen As String = "" _
                             , Optional ByVal parm_sPiePag_Der As String = "" _
                             , Optional ByVal Magen_Top As Integer = 50 _
                             , Optional ByVal Magen_Bottom As Integer = 5 _
                             , Optional ByVal Magen_Left As Integer = 10 _
                             , Optional ByVal Magen_Right As Integer = 10 _
                             , Optional ByVal Escala As Single = 0.9) As Boolean

            Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink()
            Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
            Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink



            Dim sPage_N_M As String = "Pág [Page #]"
            Dim sDate As String = Format(Now, "dd/MMM/yyyy")
            Dim sTime As String = Format(Now, "hh:mm:ss")
            Dim sUser As String = "[User Name]"

            Dim Cabecero_Izq As String = IIf(parm_sCabecero_Izq <> "", parm_sCabecero_Izq, (Name))
            Dim Cabecero_Cen As String = IIf(parm_sCabecero_Cen <> "", ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text & vbNewLine & parm_sCabecero_Cen), ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text))
            Dim Cabecero_Der As String = IIf(parm_sCabecero_Der <> "", (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime) & vbNewLine & parm_sCabecero_Der, (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime))

            Dim PiePag_Izq As String = IIf(parm_sPiePag_Izq <> "", parm_sPiePag_Izq, (""))
            Dim PiePag_Cen As String = IIf(parm_sPiePag_Cen <> "", parm_sPiePag_Cen, (""))
            Dim PiePag_Der As String = IIf(parm_sPiePag_Der <> "", parm_sPiePag_Der, (sUser))

            Dim GridAllowCellMerge As Boolean
            Try
                'Ps.Begin()
                compositeLink.PrintingSystem = Ps
                For Each Grid As Object In Grids
                    Link = New DevExpress.XtraPrinting.PrintableComponentLink()

                    If Orientacion = 1 Then
                        Link.Landscape = True
                    Else
                        Link.Landscape = False
                    End If
                    Link.PaperKind = Tipo_Hoja
                    Link.Margins.Top = Magen_Top
                    Link.Margins.Bottom = Magen_Bottom
                    Link.Margins.Left = Magen_Left
                    Link.Margins.Right = Magen_Right
                    Link.PrintingSystem = Ps
                    Link.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
                    Link.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
                    Link.PrintingSystem.PreviewFormEx.LookAndFeel.SkinName = ""

                    Ps.Document.ScaleFactor = Escala

                    If Grid.GetType.Name = "DevExpress.XtraGrid.GridControl" Then
                        'DevExpress.XtraGrid.GridControl In Grids


                        Select Case Grid.MainView.GetType.Name
                            Case "BandedGridView"
                                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
                                ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
                            Case "GridView"
                                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
                                ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = Sw_Grid_Filtros
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
                            Case "AdvBandedGridView"
                                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
                                'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
                        End Select

                        Link.Component = Grid
                        compositeLink.Links.Add(Link)
                    Else
                        Link.Component = Grid
                        compositeLink.Links.Add(Link)
                    End If


                Next

                Dim phf_encabezado_pie = New DevExpress.XtraPrinting.PageHeaderFooter(New DevExpress.XtraPrinting.PageHeaderArea(New String() {Cabecero_Izq, Cabecero_Cen, Cabecero_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near), New DevExpress.XtraPrinting.PageFooterArea(New String() {PiePag_Izq, PiePag_Cen, PiePag_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near))
                Link.PageHeaderFooter = phf_encabezado_pie

                If Orientacion = 1 Then
                    compositeLink.Landscape = True
                Else
                    compositeLink.Landscape = False
                End If
                compositeLink.PaperKind = Tipo_Hoja
                compositeLink.Margins.Top = Magen_Top
                compositeLink.Margins.Bottom = Magen_Bottom
                compositeLink.Margins.Left = Magen_Left
                compositeLink.Margins.Right = Magen_Right
                compositeLink.PrintingSystem = Ps
                compositeLink.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
                compositeLink.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
                compositeLink.PrintingSystem.PreviewFormEx.LookAndFeel.SkinName = ""

                compositeLink.PageHeaderFooter = phf_encabezado_pie
                compositeLink.CreateDocument()
                'Ps.End()
                Link.PrintingSystem.PreviewFormEx.Show()

                For Each Grid As DevExpress.XtraGrid.GridControl In Grids
                    Select Case Grid.MainView.GetType.Name
                        Case "BandedGridView"
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                        Case "GridView"
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                        Case "AdvBandedGridView"
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                    End Select
                Next
            Catch ex As Exception
                MsgBox("Error al Imprimir. " & ex.Message, MsgBoxStyle.OkOnly, Name)
                Ps.Dispose()
                Link.Dispose()
            Finally

            End Try

            Return True
        End Function

        Public Function ExportarXLS(ByVal Name As String, ByVal Text As String, ByVal Grid As DevExpress.XtraGrid.GridControl _
                                , ByVal sAplicacion As String _
                                , ByVal sProductName As String _
                                , ByVal sCompanyName As String _
                                , Optional ByRef SW_Abrir_Automatico As Boolean = True _
                                , Optional ByRef Nombre_arch As String = "" _
                                , Optional ByRef sRuta_Guardar As String = "C:\ViscoiWMS\Temporal") As Boolean

            Dim sArchivo As String = ""

            If Not System.IO.Directory.Exists(sRuta_Guardar) Then
                System.IO.Directory.CreateDirectory(sRuta_Guardar)
            End If

            Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
            Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink
            Dim GridAllowCellMerge As Boolean

            'If Not Entidades.CN_Globales.Usuario.ValidaPermiso(Name, "EXCEL") Then
            '    MsgBox(Entidades.Formularios.Msj_sinacceso(Name, "EXCEL"), MsgBoxStyle.OkOnly, Name)
            '    Return False
            'End If

            Try
                Select Case Grid.MainView.GetType.Name
                    Case "BandedGridView"
                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
                        ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                    Case "GridView"
                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
                        ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
                    Case "AdvBandedGridView"
                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
                        'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                End Select

                'Me.Sfd_Dialogo = New System.Windows.Forms.SaveFileDialog
                'Me.Sfd_Dialogo.CheckFileExists = False
                'Me.Sfd_Dialogo.FileName = "SFD_Exporta01"

                'Sfd_Dialogo.Filter = "Archivo Excel|*.xls"
                'Sfd_Dialogo.FileName = Name & Format(Now(), "yyyyMMdd") & Format(Now(), "hhmmss")
                'If Sfd_Dialogo.ShowDialog() = Windows.Forms.DialogResult.OK And Sfd_Dialogo.FileName <> "" Then
                sArchivo = Name & Format(Now(), "yyyyMMdd") & Format(Now(), "hhmmss") & ".xls"

                Dim ps As New DevExpress.XtraPrinting.PrintingSystem()
                ps.ExportOptions.Pdf.DocumentOptions.Application = sProductName
                ps.ExportOptions.Pdf.DocumentOptions.Author = sCompanyName
                ps.ExportOptions.Pdf.DocumentOptions.Title = Name
                ''ps.ExportOptions.Image.Resolution
                Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(ps)

                link.Component = Grid
                link.Landscape = True
                link.PaperKind = System.Drawing.Printing.PaperKind.LetterExtra
                link.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
                link.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D

                link.CreateDocument()
                link.PrintingSystem.ExportToXls(sRuta_Guardar & "\" & sArchivo)

                'Dim appExcel As New Microsoft.Office.Interop.Excel.Application
                'sAplicacion = appExcel.Path & "\EXCEL.EXE"

                If SW_Abrir_Automatico Then
                    'Shell(sAplicacion & " """ & sRuta_Guardar & "\" & sArchivo & """", vbNormalFocus)
                    System.Diagnostics.Process.Start(sRuta_Guardar & "\" & sArchivo)
                End If
                Nombre_arch = sRuta_Guardar & "\" & sArchivo
                'End If

                Select Case Grid.MainView.GetType.Name
                    Case "BandedGridView"
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                    Case "GridView"
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                    Case "AdvBandedGridView"
                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                End Select
            Catch ex As Exception
                MsgBox("Error al Exportar a Excel. " & ex.Message, MsgBoxStyle.OkOnly, Name)
                Ps.Dispose()
                Link.Dispose()
            Finally
                'Me.Sfd_Dialogo.Dispose()
            End Try
            Return True
        End Function
        Public Function ExportarXLS2(ByVal Name As String, ByVal Text As String, ByVal Grids As DevExpress.XtraGrid.GridControl() _
                                , ByVal sAplicacion As String _
                                , ByVal sProductName As String _
                                , ByVal sCompanyName As String _
                                , Optional ByRef sArchivo As String = "" _
                                , Optional ByRef SW_Abrir_Automatico As Boolean = True _
                                , Optional ByRef Nombre_arch As String = "" _
                                , Optional ByRef sRuta_Guardar As String = "C:\ViscoiWMS\Temporal") As Boolean

            'Dim sArchivo As String = ""

            If Not System.IO.Directory.Exists(sRuta_Guardar) Then
                System.IO.Directory.CreateDirectory(sRuta_Guardar)
            End If

            Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
            Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink
            Dim GridAllowCellMerge As Boolean

            'If Not Entidades.CN_Globales.Usuario.ValidaPermiso(Name, "EXCEL") Then
            '    MsgBox(Entidades.Formularios.Msj_sinacceso(Name, "EXCEL"), MsgBoxStyle.OkOnly, Name)
            '    Return False
            'End If

            Try
                sArchivo = Name & Format(Now(), "yyyyMMdd") & Format(Now(), "hhmmss") & ".xls"
                Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink()
                Dim ps As New DevExpress.XtraPrinting.PrintingSystem()
                ps.ExportOptions.Pdf.DocumentOptions.Application = sProductName
                ps.ExportOptions.Pdf.DocumentOptions.Author = sCompanyName
                ps.ExportOptions.Pdf.DocumentOptions.Title = Name


                compositeLink.PrintingSystem = ps

                For Each Grid As DevExpress.XtraGrid.GridControl In Grids

                    Select Case Grid.MainView.GetType.Name
                        Case "BandedGridView"
                            GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
                            ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                        Case "GridView"
                            GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
                            ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
                        Case "AdvBandedGridView"
                            GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
                            'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
                    End Select



                    Dim link As New DevExpress.XtraPrinting.PrintableComponentLink()


                    link.Component = Grid
                    link.Landscape = True
                    link.PaperKind = System.Drawing.Printing.PaperKind.LetterExtra


                    compositeLink.Links.Add(link)



                Next

                compositeLink.CreateDocument()
                compositeLink.PrintingSystem.ExportToXlsx(sRuta_Guardar & "\" & sArchivo)

                'Dim appExcel As New Microsoft.Office.Interop.Excel.Application
                'sAplicacion = appExcel.Path & "\EXCEL.EXE"

                If SW_Abrir_Automatico Then
                    'Shell(sAplicacion & " """ & sRuta_Guardar & "\" & sArchivo & """", vbNormalFocus)
                    System.Diagnostics.Process.Start(sRuta_Guardar & "\" & sArchivo)
                End If
                Nombre_arch = sRuta_Guardar & "\" & sArchivo
                'End If


                For Each Grid As DevExpress.XtraGrid.GridControl In Grids
                    Select Case Grid.MainView.GetType.Name
                        Case "BandedGridView"
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                        Case "GridView"
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                        Case "AdvBandedGridView"
                            TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
                    End Select
                Next



            Catch ex As Exception
                MsgBox("Error al Exportar a Excel. " & ex.Message, MsgBoxStyle.OkOnly, Name)
                Ps.Dispose()
                Link.Dispose()
            Finally
                'Me.Sfd_Dialogo.Dispose()
            End Try
            Return True
        End Function

        Public Sub Estblece8020(ByVal Columna80 As DevExpress.XtraGrid.Columns.GridColumn, ByVal Columna As DevExpress.XtraGrid.Columns.GridColumn, ByRef GridC As DevExpress.XtraGrid.GridControl)
            Dim Total As Double = 0.0
            Dim Total80 As Double = 0.0
            Dim prc80 As Double = 0.0
            'Dim GridV As DevExpress.XtraGrid.Views.Base.BaseView
            'GridV = GridC.MainView
            Try
                Select Case GridC.MainView.GetType.Name
                    Case "BandedGridView"
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).ClearSorting()
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).Columns(Columna.FieldName).SortIndex = 0
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).Columns(Columna.FieldName).SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).Columns(Columna.FieldName).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
                    Case "GridView"
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).ClearSorting()
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).Columns(Columna.FieldName).SortIndex = 0
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).Columns(Columna.FieldName).SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).Columns(Columna.FieldName).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
                    Case "AdvBandedGridView"
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).ClearSorting()
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).Columns(Columna.FieldName).SortIndex = 0
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).Columns(Columna.FieldName).SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).Columns(Columna.FieldName).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
                End Select

                For iI = 0 To GridC.MainView.RowCount - 1
                    Select Case GridC.MainView.GetType.Name
                        Case "BandedGridView"
                            Total += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).GetRowCellValue(iI, Columna)
                            TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).SetRowCellValue(iI, Columna80, 0)
                        Case "GridView"
                            Total += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).GetRowCellValue(iI, Columna)
                            TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).SetRowCellValue(iI, Columna80, 0)
                        Case "AdvBandedGridView"
                            Total += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).GetRowCellValue(iI, Columna)
                            TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).SetRowCellValue(iI, Columna80, 0)
                    End Select
                Next
                prc80 = Total * 0.8

                For iI = 0 To GridC.MainView.RowCount - 1
                    If Total80 <= prc80 Then
                        Select Case GridC.MainView.GetType.Name
                            Case "BandedGridView"
                                Total80 += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).GetRowCellValue(iI, Columna)
                                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).SetRowCellValue(iI, Columna80, 1)
                            Case "GridView"
                                Total80 += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).GetRowCellValue(iI, Columna)
                                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).SetRowCellValue(iI, Columna80, 1)
                            Case "AdvBandedGridView"
                                Total80 += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).GetRowCellValue(iI, Columna)
                                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).SetRowCellValue(iI, Columna80, 1)
                        End Select
                    End If
                Next
                Select Case GridC.MainView.GetType.Name
                    Case "BandedGridView"
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).FormatRules.Item("Format_8020").ColumnApplyTo = Columna
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).FocusedRowHandle = 0
                    Case "GridView"
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).FormatRules.Item("Format_8020").ColumnApplyTo = Columna
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).FocusedRowHandle = 0
                    Case "AdvBandedGridView"
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).FormatRules.Item("Format_8020").ColumnApplyTo = Columna
                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).FocusedRowHandle = 0
                End Select

            Catch ex As Exception

            End Try
        End Sub
        Public Shared Sub Grid_Prevenir_Tab(ByRef sender As Object, ByRef e As KeyEventArgs, ByRef Next1 As Object)
            Dim GridC As DevExpress.XtraGrid.GridControl
            Try
                If (e.KeyCode = Keys.Tab) Then
                    'GridC = TryCast(sender, DevExpress.XtraGrid.GridControl)
                    e.Handled = False
                    If Not Next1 Is Nothing Then
                        Next1.focus()
                    End If
                End If
            Catch ex As Exception

            End Try
        End Sub
    End Class
    Public Class oPaqueterias
        Public Class clsDatosDireccion
            Dim _NoCliente As String
            Dim _empresa As String
            Dim _contacto As String
            Dim _direccion1 As String
            Dim _direccion2 As String
            Dim _ciudad As String
            Dim _Estado As String
            Dim _CodigoPostal As String
            Dim _Colonia As String
            Dim _Telefono As String
            Dim _Celular As String
            Public Sub New()
                _NoCliente = ""
                _empresa = ""
                _contacto = ""
                _direccion1 = ""
                _direccion2 = ""
                _ciudad = ""
                _Estado = ""
                _CodigoPostal = ""
                _Colonia = ""
                _Telefono = ""
                _Celular = ""
            End Sub
            Public Property NoCliente As String
                Get
                    Return _NoCliente
                End Get
                Set(value As String)
                    _NoCliente = value
                End Set
            End Property

            Public Property Empresa As String
                Get
                    Return _empresa
                End Get
                Set(value As String)
                    _empresa = value
                End Set
            End Property

            Public Property Contacto As String
                Get
                    Return _contacto
                End Get
                Set(value As String)
                    _contacto = value
                End Set
            End Property

            Public Property Direccion1 As String
                Get
                    Return _direccion1
                End Get
                Set(value As String)
                    _direccion1 = value
                End Set
            End Property

            Public Property Direccion2 As String
                Get
                    Return _direccion2
                End Get
                Set(value As String)
                    _direccion2 = value
                End Set
            End Property

            Public Property Ciudad As String
                Get
                    Return _ciudad
                End Get
                Set(value As String)
                    _ciudad = value
                End Set
            End Property

            Public Property Estado As String
                Get
                    Return _Estado
                End Get
                Set(value As String)
                    _Estado = value
                End Set
            End Property

            Public Property CodigoPostal As String
                Get
                    Return _CodigoPostal
                End Get
                Set(value As String)
                    _CodigoPostal = value
                End Set
            End Property

            Public Property Colonia As String
                Get
                    Return _Colonia
                End Get
                Set(value As String)
                    _Colonia = value
                End Set
            End Property

            Public Property Telefono As String
                Get
                    Return _Telefono
                End Get
                Set(value As String)
                    _Telefono = value
                End Set
            End Property

            Public Property Celular As String
                Get
                    Return _Celular
                End Get
                Set(value As String)
                    _Celular = value
                End Set
            End Property
        End Class

        '        Public Class oEstafeta
        '            Public Shared Function NuevaGuia(ByVal NoClientePaqueteria As String _
        '                                          , ByVal UserPaqueteria As String _
        '                                          , ByVal PassPaqueteria As String _
        '                                          , ByVal IdPaqueteria As String _
        '                                          , ByVal EtiquetaZebra As Boolean _
        '                                          , ByVal Origen As clsDatosDireccion _
        '                                          , ByVal Destino As clsDatosDireccion _
        '                                          , ByVal Contenido As String _
        '                                          , ByVal Referencia As String _
        '                                          , ByVal InformacionAdicional As String _
        '                                          , ByRef GuiaPaqueteria As String _
        '                                          , ByRef Msj As String) As Boolean
        '                Dim Resultado As Boolean = False
        '#If DEBUG Then
        '                Dim Paq_Solicitud As New Paqueteria_Pruebas.EstafetaLabelRequest
        '                Dim Paq_Response As New Paqueteria_Pruebas.EstafetaLabelResponse
        '                Dim Paq_DescripcionLista = New Paqueteria_Pruebas.LabelDescriptionList
        '                Dim Paq_Destino As New Paqueteria_Pruebas.DestinationInfo
        '                Dim Paq_Origen As New Paqueteria_Pruebas.OriginInfo
        '                Dim Paq_Locator As New Paqueteria_Pruebas.EstafetaLabelClient
        '                Dim Paq_Guia As New Paqueteria_Pruebas.Waybill
        '                Dim Paq_Printer As New Paqueteria_Pruebas.PrinterSystem
        '                Dim listArray(0) As Paqueteria_Pruebas.LabelDescriptionList
        '#Else
        '                Dim Paq_Solicitud As New Paqueteria.EstafetaLabelRequest
        '                Dim Paq_Response As New Paqueteria.EstafetaLabelResponse
        '                Dim Paq_DescripcionLista = New Paqueteria.LabelDescriptionList
        '                Dim Paq_Destino As New Paqueteria.DestinationInfo
        '                Dim Paq_Origen As New Paqueteria.OriginInfo
        '                Dim Paq_Locator As New Paqueteria.EstafetaLabelClient
        '                Dim Paq_Guia As New Paqueteria.Waybill
        '                Dim listArray(0) As Paqueteria.LabelDescriptionList
        '#End If
        '                Try
        '                    Paq_Solicitud.customerNumber = NoClientePaqueteria      ''"0000000"
        '                    Paq_Solicitud.login = UserPaqueteria                    ''"prueba1"
        '                    Paq_Solicitud.password = PassPaqueteria                 ''"lAbeL_K_11"
        '                    Paq_Solicitud.suscriberId = IdPaqueteria                ''"28"
        '                    Paq_Solicitud.quadrant = 0                              ''0
        '                    Paq_Solicitud.paperType = IIf(EtiquetaZebra, 2, 1)      ''2

        '                    ''Informacion Origen       
        '                    Paq_Origen.address1 = Origen.Direccion1         ''AV. JUAREZ 4170
        '                    Paq_Origen.address2 = Origen.Direccion2         ''SALTILLO 400 Y CALLE MEXICO
        '                    Paq_Origen.city = Origen.Ciudad                 ''TORREON
        '                    Paq_Origen.contactName = Origen.Contacto        ''PASCUAL LUCIO
        '                    Paq_Origen.corporateName = Origen.Empresa       ''CONFIASHOP
        '                    Paq_Origen.customerNumber = Origen.NoCliente    ''8668827
        '                    Paq_Origen.neighborhood = Origen.Colonia        ''NUEVA CALIFORNIA
        '                    Paq_Origen.phoneNumber = Origen.Telefono        ''8715724240
        '                    Paq_Origen.cellPhone = Origen.Celular           ''
        '                    Paq_Origen.state = Origen.Estado                ''COAHUILA
        '                    Paq_Origen.zipCode = Origen.CodigoPostal        ''27089

        '                    ''Informacion Destino        
        '                    Paq_Destino.address1 = Destino.Direccion1
        '                    Paq_Destino.address2 = Destino.Direccion2
        '                    Paq_Destino.city = Destino.Ciudad
        '                    Paq_Destino.contactName = Destino.Contacto
        '                    Paq_Destino.corporateName = Destino.Empresa
        '                    Paq_Destino.customerNumber = Destino.NoCliente
        '                    Paq_Destino.neighborhood = Destino.Colonia
        '                    Paq_Destino.phoneNumber = Destino.Telefono
        '                    Paq_Destino.cellPhone = Destino.Celular
        '                    Paq_Destino.state = Destino.Estado
        '                    Paq_Destino.zipCode = Destino.CodigoPostal

        '                    Paq_DescripcionLista.originInfo = Paq_Origen
        '                    Paq_DescripcionLista.destinationInfo = Paq_Destino

        '                    ''Información adicional       
        '                    Paq_DescripcionLista.aditionalInfo = InformacionAdicional
        '                    If Paq_DescripcionLista.aditionalInfo.Length > 25 Then
        '                        Paq_DescripcionLista.aditionalInfo = Paq_DescripcionLista.aditionalInfo.Substring(0, 25)
        '                    End If
        '                    ''Contenido       
        '                    Paq_DescripcionLista.content = Contenido
        '                    ''Referencia      
        '                    Paq_DescripcionLista.reference = IIf(Referencia.Length > 0, Referencia, " ")
        '                    ''Centro de costos       
        '                    Paq_DescripcionLista.costCenter = "VENTALINEA"
        '                    ''Ocurre       
        '                    Paq_DescripcionLista.deliveryToEstafetaOffice = False
        '                    ''En caso de envio a otro pais, solo siglas
        '                    Paq_DescripcionLista.destinationCountryId = "MX"

        '                    ''Tipo de envio 1=SOBRE 4=PAQUETE       
        '                    Paq_DescripcionLista.parcelTypeId = 4
        '                    ''Peso      
        '                    Paq_DescripcionLista.weight = 1
        '                    ''Número de etiquetas solicitadas       
        '                    Paq_DescripcionLista.numberOfLabels = 1
        '                    ''Código postal de Origen para enrutamiento       
        '                    Paq_DescripcionLista.originZipCodeForRouting = Paq_Origen.zipCode
        '                    ''Servicio que se usará      
        '                    Paq_DescripcionLista.serviceTypeId = "70"
        '                    ''Numero de oficina que corresponde al cliente       
        '                    Paq_DescripcionLista.officeNum = "130"
        '                    ''Documento de retorno       
        '                    Paq_DescripcionLista.returnDocument = False
        '                    ''Servicio del documento de retorno       
        '                    Paq_DescripcionLista.serviceTypeIdDocRet = "50"
        '                    ''Fecha de vigencia       
        '                    Paq_DescripcionLista.effectiveDate = (Today.Year * 10000 _
        '                                                                        + Today.Month * 100 _
        '                                                                        + Today.Day * 1).ToString
        '                    ''Descripcion del contenido       
        '                    Paq_DescripcionLista.contentDescription = InformacionAdicional

        '                    'Paq_Printer.systemName = Globales.oAmbientes.Sistema
        '                    'Paq_Printer.systemVersion = Globales.oAmbientes.Version
        '                    'Paq_Printer.systemReleasedDate = "20200101"

        '                    listArray(0) = Paq_DescripcionLista
        '                    Paq_Solicitud.labelDescriptionList = listArray
        '                    'Paq_Solicitud.printerSystem = Paq_Printer

        '                    Paq_Response = Paq_Locator.createLabel(Paq_Solicitud)

        '                    If (Paq_Response.globalResult IsNot Nothing And Paq_Response.labelPDF IsNot Nothing) Then

        '                        ''Preparamos la salida del PDF         
        '                        Dim fout As System.IO.FileStream = Nothing
        '                        Dim aBytes As Byte() = Nothing
        '                        GuiaPaqueteria = Paq_Response.labelResultList(0).resultDescription

        '                        If Not IO.Directory.Exists("C:\Confia\") Then
        '                            IO.Directory.CreateDirectory("C:\Confia")
        '                        End If

        '                        If Not IO.Directory.Exists("C:\Confia\Guias") Then
        '                            IO.Directory.CreateDirectory("C:\Confia\Guias")
        '                        End If

        '                        If Not IO.Directory.Exists("C:\Confia\Guias\Estafeta") Then
        '                            IO.Directory.CreateDirectory("C:\Confia\Guias\Estafeta")
        '                        End If
        '                        fout = System.IO.File.Create("C:\Confia\Guias\Estafeta\" & GuiaPaqueteria & ".pdf", 1024)

        '                        aBytes = Paq_Response.labelPDF
        '                        fout.Write(aBytes, 0, aBytes.Length)
        '                        fout.Close()
        '                        Resultado = True
        '                    Else
        '                        ''Hubo un error y debe ser mostrado  
        '                        Resultado = False
        '                        Msj = Paq_Response.globalResult.resultDescription
        '                    End If
        '                Catch ex As Exception
        '                    Resultado = False
        '                    Msj = ex.Message
        '                End Try
        '                Return Resultado
        '            End Function
        '        End Class
    End Class

    Public Class clsPinPadBanorte
        Private _UrlBanorte As String
        Private _Puerto As String
        Private _User As String
        Private _Pswd As String
        Private _IdMerchant As String
        Private _Modo As String
        Private _Caja As String
        Private _PinPad As Banorte.PinPad.Vx820Segura

        Private _id_empresa As String
        Private _empresa As String
        Private _rfc As String
        Private _direccion As String
        Private _direccion2 As String
        Private _tipo As String
        Private _transaccion As Integer
        Private _id_sucursal As String
        Private _fecha As Date
        Private _hora As String
        Private _Banco As String
        Private _Respuesta As String
        Private _Afiliacion As String
        Private _TerminalId As String
        Private _NumControl As String
        Private _NumTarjeta As String
        Private _TipTarjeta As String
        Private _VigTarjeta As String
        Private _TitTarjeta As String
        Private _BancoEmisor As String
        Private _CodigoAut As String
        Private _Referencia As String
        Private _Entry_Mode As String
        Private _Importe As Double
        Private _MesesDiferido As String
        Private _NumPagos As String
        Private _TipoPlan As String 
        Public Sub New(ByVal sPuerto As String, ByVal sUser As String, ByVal sPswd As String, ByVal sIdMerchant As String, ByVal sModo As String, ByVal sUrl As String, ByVal sCaja As String)
            _Puerto = sPuerto
            _User = sUser
            _Pswd = sPswd
            _IdMerchant = sIdMerchant
            _Modo = sModo
            _Caja = sCaja
            _UrlBanorte = sUrl
            _Banco = Globales.oAmbientes.PinPadBanco
        End Sub
        Public Sub Disposed()
            Dim Msj As String = ""
            Call Me.fn_Pinpad_Banorte_Liberar(Msj)
        End Sub
        Public Function fn_Pinpad_Banorte_Inicializa(ByRef msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim config As Hashtable

            Try
                'DECLARA UN OBJETO PINPAD
                _PinPad = New Banorte.PinPad.Vx820Segura("EN")

                'CREAR TABLA DE PARÁMETROS DE CONFIGURACIÓN DEL DISPOSITIVO
                config = New Hashtable()
                config.Add("PORT", _Puerto)
                config.Add("BAUD_RATE", "19200")
                config.Add("PARITY", "N")
                config.Add("STOP_BITS", "1")
                config.Add("DATA_BITS", "8")
                _PinPad.prepareDevice(config)
                sw_regresa = True
            Catch ex As Exception
                msj = "PUERTO: " + _Puerto + vbNewLine + "[Inicializa] Falla al iniciar la transacción: " + ex.Message
                Return sw_regresa
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_Liberar(ByRef msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim salidaInformacion As New Hashtable()
            Try
                If _PinPad Is Nothing Then
                    _PinPad = New Banorte.PinPad.Vx820Segura("EN")
                End If
                _PinPad.releaseDevice()
                sw_regresa = True
            Catch ex As Exception
                If ex.Message.Substring(0, 8) = "[ERR005]" Then
                    sw_regresa = True
                End If
                msj = "[Liberar] Falla al iniciar la transacción: " + ex.Message
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_CrearLlaves_Internet(ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim _numeroSerie As String
            Dim entradaCarga As New Hashtable()

            Try
                _numeroSerie = Me.NumeroSerie()
                '' _PinPad.startTransaction()
                'LLENAMOS EL HASHTABLE
                entradaCarga.Add("USER", _User)
                entradaCarga.Add("PASSWORD", _Pswd)
                entradaCarga.Add("MERCHANT_ID", _IdMerchant)
                entradaCarga.Add("CONTROL_NUMBER", _numeroSerie & Format(Now, "yyMMddHHmmss"))
                entradaCarga.Add("RESPONSE_LANGUAGE", "EN")
                entradaCarga.Add("SERIAL_NUMBER", _numeroSerie)
                entradaCarga.Add("BANORTE_URL", _UrlBanorte)
                'REALIZAMOS LA CARGA DE LA LLAVE
                Try
                    _PinPad.updateMasterKey(entradaCarga)
                    sw_regresa = True
                Catch ex As Exception
                    sw_regresa = False
                    Msj = "[CrearLlaves_Internet] Falla al cargar la llave en el pinpad: " + ex.Message
                End Try

            Catch ex As Exception
                Msj = "[CrearLlaves_Internet] Falla al cargar la llave en el pinpad: " + ex.Message
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_CrearLlaves(ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim _numeroSerie As String

            Try
                'SE CREA EL HASHTABLE PARA LA OBTENCIÓN DEL SELECTOR
                Dim salidaSelector As New Hashtable()
                Dim selector As String = ""

                _numeroSerie = Me.NumeroSerie()
                'SE SOLICITA EL SELECTOR
                Try
                    _PinPad.getSelector(salidaSelector)
                    selector = salidaSelector("SELECTOR")
                Catch ex As Exception
                    Msj = "[CrearLlaves] Falla al obtener el selector: " + ex.Message
                    Return sw_regresa
                End Try

                'SE CREAN LOS HASHTABLES PARA SOLICITAR LA LLAVE
                Dim entradaEnviar As New Hashtable()
                Dim salidaEnviar As New Hashtable()
                entradaEnviar.Add("CMD_TRANS", "GET_KEY")
                entradaEnviar.Add("USER", _User)
                entradaEnviar.Add("PASSWORD", _Pswd)
                entradaEnviar.Add("MERCHANT_ID", _IdMerchant)
                entradaEnviar.Add("CONTROL_NUMBER", "CARGALLAVE0001")
                entradaEnviar.Add("SELECTOR", selector)
                entradaEnviar.Add("RESPONSE_LANGUAGE", "EN")
                entradaEnviar.Add("BANORTE_URL", _UrlBanorte)
                'SE ENVÍA EL COMANDO A BANORTE PARA LA SOLICITUD DE LA LLAVE
                Try
                    Banorte.ConectorBanorte.sendTransaction(entradaEnviar, salidaEnviar)
                Catch ex As Exception
                    Msj = "[CrearLlaves] Falla al enviar la transacción: " + ex.Message
                    Return sw_regresa
                End Try

                Dim resultadoPayw, paywCode As String
                resultadoPayw = salidaEnviar.Item("PAYW_RESULT")
                paywCode = salidaEnviar.Item("PAYW_CODE")

                'SE VALIDA QUE SE HAYA OBTENIDO CORRECTAMENTE LA LLAVE
                If resultadoPayw <> "A" Then
                    Dim entradaCancelar As New Hashtable()
                    entradaCancelar.Add("SERIAL_NUMBER", _numeroSerie)
                    Try
                        _PinPad.cancelLoadKey(entradaCancelar)
                    Catch ex As Exception
                        Msj = "[CrearLlaves] Falla al cancelar la carga de llave: " + ex.Message
                        Return sw_regresa
                    End Try
                Else
                    Dim entradaCarga As New Hashtable()
                    Dim llaveMaestra As String
                    llaveMaestra = salidaEnviar.Item("TEXT")
                    entradaCarga.Add("SERIAL_NUMBER", _numeroSerie)
                    entradaCarga.Add("MASTER_KEY", llaveMaestra)
                    Try
                        _PinPad.loadMasterKey(entradaCarga)
                    Catch ex As Exception
                        Msj = "[CrearLlaves] Falla al realizar la carga de llave: " + ex.Message
                        Return sw_regresa
                    End Try
                End If

                sw_regresa = True
            Catch ex As Exception
                Msj = "[CrearLlaves] Falla al cargar la llave en el pinpad: " + ex.Message
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_Cobrar(ByVal Folio As String _
                                         , ByVal vImporte As String _
                                         , ByRef codigoAut As String _
                                         , ByRef sReferencia As String _
                                         , ByRef MesesDiferido As String _
                                         , ByRef NumPagos As String _
                                         , ByRef TipoPlan As String _
                                         , ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim parametrosEntrada As New Hashtable()
            Dim parametrosSalida As New Hashtable()
            Dim NumSerie As String = ""
            Try
                MesesDiferido = MesesDiferido.Replace(" ", "")
                NumPagos = NumPagos.Replace(" ", "")
                TipoPlan = TipoPlan.Replace(" ", "")

                _MesesDiferido = MesesDiferido
                _NumPagos = NumPagos
                _TipoPlan = TipoPlan
                Try
                    _PinPad.startTransaction()
                    NumSerie = Me.NumeroSerie

                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al iniciar la transacción: " + ex.Message
                    Return sw_regresa
                End Try
                'sw_regresa = fn_Pinpad_Banorte_CrearLlaves(Msj)
                'If sw_regresa Then
                _PinPad.displayText("Iniciando Cobro VOFi5+ " & oAmbientes.Version)
                sw_regresa = False
                ''6. CREAMOS LOS HASHTABLE DE PARAMETROS PARA LA TRANSACCION
                'PARAMETROS DE ENTRADA PARA LA TRANSACCION
                parametrosEntrada.Add("MERCHANT_ID", _IdMerchant)
                parametrosEntrada.Add("USER", _User)
                parametrosEntrada.Add("PASSWORD", _Pswd)
                parametrosEntrada.Add("CMD_TRANS", "AUTH")
                parametrosEntrada.Add("TERMINAL_ID", NumSerie)
                parametrosEntrada.Add("CONTROL_NUMBER", Folio)
                parametrosEntrada.Add("MODE", _Modo)
                parametrosEntrada.Add("AMOUNT", vImporte.Replace("$", "").Replace(",", ""))
                parametrosEntrada.Add("RESPONSE_LANGUAGE", "EN")
                parametrosEntrada.Add("BANORTE_URL", _UrlBanorte)
                parametrosEntrada.Add("INITIAL_DEFERMENT", _MesesDiferido)
                parametrosEntrada.Add("PAYMENTS_NUMBER", _NumPagos)
                parametrosEntrada.Add("PLAN_TYPE", _TipoPlan)
                'METODO PROCESAR TRANSACCION
                Dim CodigoBanorte, declinadaOffline As String
                Try
                    _PinPad.processTransaction(parametrosEntrada, parametrosSalida)
                    'OBTENEMOS LOS DATOS PARA IDENTIFICAR EL RESULTADO
                    CodigoBanorte = parametrosSalida.Item("PAYW_RESULT")
                    declinadaOffline = parametrosSalida.Item("CHIP_DECLINED")
                    'REVISAMOS SI FUE UN DECLINADO OFFLINE
                    If declinadaOffline = "1" Then
                        Msj = "[Banorte_Cobrar] Declinada Offline"
                        Return sw_regresa
                    Else
                        If CodigoBanorte = "A" Then
                            codigoAut = parametrosSalida.Item("AUTH_CODE")
                            Msj = "[Banorte_Cobrar] Transacción aprobada: " + codigoAut
                            _PinPad.displayText(Msj)
                        Else
                            Msj = "[Banorte_Cobrar] Transacción declinada. " + parametrosSalida.Item("TEXT")
                            _NumControl = parametrosSalida.Item("CONTROL_NUMBER")
                            _Respuesta = parametrosSalida.Item("TEXT")
                            _Referencia = parametrosSalida.Item("REFERENCE")

                            Dim _dia1 As Integer
                            Dim _mes1 As Integer
                            Dim _anio1 As Integer
                            _dia1 = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(6, 2)
                            _mes1 = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(4, 2)
                            _anio1 = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(0, 4)
                            _fecha = New Date(_anio1, _mes1, _dia1)
                            _hora = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(9, parametrosSalida.Item("CUST_REQ_DATE").ToString.Length - 1 - 9)
                            _Importe = vImporte.Replace("$", "").Replace(",", "")

                            _Banco = "BANORTE"
                            _Afiliacion = _IdMerchant
                            _TerminalId = NumSerie
                            _NumTarjeta = ""
                            _VigTarjeta = ""
                            _TitTarjeta = ""
                            _TipTarjeta = ""
                            _BancoEmisor = ""
                            _CodigoAut = ""
                            _PinPad.displayText(Msj)
                            Return sw_regresa
                        End If
                    End If
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al procesar transacción: " + ex.Message
                    _PinPad.displayText(Msj)
                    Return sw_regresa
                End Try

                Try
                    _PinPad.endTransaction()
                    Dim Emv_Result As String
                    Emv_Result = parametrosSalida.Item("EMV_RESULT") ''EMV_RESULT
                    If parametrosSalida.Item("ENTRY_MODE") = "CONTACTLESSCHIP" And Emv_Result Is Nothing Then
                        Emv_Result = "A"
                    End If
                    If parametrosSalida.Item("ENTRY_MODE") = "CHIP" And Emv_Result = "" Then
                        Emv_Result = "A"
                    End If
                    If Emv_Result = "A" Then
                        If (CodigoBanorte = "A") Then
                            _PinPad.displayText("Aprobada: " + codigoAut)
                        Else
                            _PinPad.displayText("Declinada")
                        End If

                        _Banco = "BANORTE"
                        _Afiliacion = parametrosSalida.Item("MERCHANT_ID")
                        _TerminalId = NumSerie
                        _NumControl = parametrosSalida.Item("CONTROL_NUMBER")
                        _NumTarjeta = parametrosSalida.Item("CARD_NUMBER")
                        If _NumTarjeta IsNot Nothing Then
                            _NumTarjeta = "**** **** **** " & _NumTarjeta.Substring(_NumTarjeta.Length - 4, 4)
                        Else
                            _NumTarjeta = "**** **** **** ****"
                        End If
                        _VigTarjeta = parametrosSalida.Item("CARD_EXP")
                        _TitTarjeta = parametrosSalida.Item("CARD_HOLDER")
                        _TipTarjeta = parametrosSalida.Item("APN")
                        _BancoEmisor = parametrosSalida.Item("ISSUING_BANK")
                        _Referencia = parametrosSalida.Item("REFERENCE")
                        _Entry_Mode = parametrosSalida.Item("ENTRY_MODE") ''
                        If sReferencia IsNot Nothing Then
                            sReferencia = _Referencia
                        End If
                        _Respuesta = parametrosSalida.Item("TEXT")
                        _CodigoAut = parametrosSalida.Item("AUTH_CODE")
                        Double.TryParse(vImporte.Replace("$", "").Replace(",", ""), _Importe)
                        Dim _dia As Integer
                        Dim _mes As Integer
                        Dim _anio As Integer
                        _dia = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(6, 2)
                        _mes = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(4, 2)
                        _anio = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(0, 4)
                        _fecha = New Date(_anio, _mes, _dia)
                        _hora = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(9, parametrosSalida.Item("CUST_REQ_DATE").ToString.Length - 1 - 9)

                        _PinPad.releaseDevice()

                        sw_regresa = True
                    Else
                        sw_regresa = False
                        _PinPad.displayText("Declinada")
                        _Respuesta = parametrosSalida.Item("TEXT")
                        _CodigoAut = parametrosSalida.Item("AUTH_CODE")
                    End If
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla: " + ex.Message
                    Return sw_regresa
                End Try
                'End If
            Catch ex As Exception
                Msj = ex.Message
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_Verificar(ByVal Folio As String _
                                         , ByVal vImporte As String _
                                         , ByRef codigoAut As String _
                                         , ByRef sReferencia As String _
                                         , ByRef MesesDiferido As String _
                                         , ByRef NumPagos As String _
                                         , ByRef TipoPlan As String _
                                         , ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim parametrosEntrada As New Hashtable()
            Dim parametrosSalida As New Hashtable()
            Dim NumSerie As String = ""
            Try
                MesesDiferido = MesesDiferido.Replace(" ", "")
                NumPagos = NumPagos.Replace(" ", "")
                TipoPlan = TipoPlan.Replace(" ", "")

                _MesesDiferido = MesesDiferido
                _NumPagos = NumPagos
                _TipoPlan = TipoPlan
                Try
                    _PinPad.startTransaction()
                    NumSerie = Me.NumeroSerie

                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al iniciar la transacción: " + ex.Message
                    Return sw_regresa
                End Try
                'sw_regresa = fn_Pinpad_Banorte_CrearLlaves(Msj)
                'If sw_regresa Then
                sw_regresa = False
                ''6. CREAMOS LOS HASHTABLE DE PARAMETROS PARA LA TRANSACCION
                'PARAMETROS DE ENTRADA PARA LA TRANSACCION
                parametrosEntrada.Add("MERCHANT_ID", _IdMerchant)
                parametrosEntrada.Add("USER", _User)
                parametrosEntrada.Add("PASSWORD", _Pswd)
                parametrosEntrada.Add("CMD_TRANS", "VERIFY")
                parametrosEntrada.Add("TERMINAL_ID", NumSerie)
                parametrosEntrada.Add("CONTROL_NUMBER", Folio)
                parametrosEntrada.Add("MODE", _Modo)
                parametrosEntrada.Add("AMOUNT", vImporte.Replace("$", "").Replace(",", ""))
                parametrosEntrada.Add("RESPONSE_LANGUAGE", "EN")
                parametrosEntrada.Add("BANORTE_URL", _UrlBanorte)
                parametrosEntrada.Add("INITIAL_DEFERMENT", _MesesDiferido)
                parametrosEntrada.Add("PAYMENTS_NUMBER", _NumPagos)
                parametrosEntrada.Add("PLAN_TYPE", _TipoPlan)
                'METODO PROCESAR TRANSACCION
                Dim CodigoBanorte, declinadaOffline As String
                Try
                    _PinPad.processTransaction(parametrosEntrada, parametrosSalida)
                    'OBTENEMOS LOS DATOS PARA IDENTIFICAR EL RESULTADO
                    CodigoBanorte = parametrosSalida.Item("PAYW_RESULT")
                    declinadaOffline = parametrosSalida.Item("CHIP_DECLINED")
                    'REVISAMOS SI FUE UN DECLINADO OFFLINE
                    If declinadaOffline = "1" Then
                        Msj = "[Banorte_Cobrar] Declinada Offline"
                        Return sw_regresa
                    Else
                        If CodigoBanorte = "A" Then
                            codigoAut = parametrosSalida.Item("AUTH_CODE")
                            Msj = "[Banorte_Cobrar] Transacción aprobada: " + codigoAut
                        Else
                            Msj = "[Banorte_Cobrar] Transacción declinada. " + parametrosSalida.Item("TEXT")
                            _NumControl = parametrosSalida.Item("CONTROL_NUMBER")
                            _Respuesta = parametrosSalida.Item("TEXT")
                            _Referencia = parametrosSalida.Item("REFERENCE")

                            Dim _dia1 As Integer
                            Dim _mes1 As Integer
                            Dim _anio1 As Integer
                            _dia1 = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(6, 2)
                            _mes1 = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(4, 2)
                            _anio1 = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(0, 4)
                            _fecha = New Date(_anio1, _mes1, _dia1)
                            _hora = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(9, parametrosSalida.Item("CUST_REQ_DATE").ToString.Length - 1 - 9)
                            _Importe = vImporte.Replace("$", "").Replace(",", "")

                            _Banco = "BANORTE"
                            _Afiliacion = _IdMerchant
                            _TerminalId = NumSerie
                            _NumTarjeta = ""
                            _VigTarjeta = ""
                            _TitTarjeta = ""
                            _TipTarjeta = ""
                            _BancoEmisor = ""
                            _CodigoAut = ""
                            Return sw_regresa
                        End If
                    End If
                Catch ex As Exception
                    Msj = "[Banorte_Verificacion] Falla al procesar transacción: " + ex.Message
                    _PinPad.displayText(Msj)
                    Return sw_regresa
                End Try

                Try
                    _PinPad.endTransaction()

                    If (CodigoBanorte = "A") Then
                        _PinPad.displayText("Verificada: " + codigoAut)
                    Else
                        _PinPad.displayText("Declinada")
                    End If

                    _Banco = "BANORTE"
                    Dim pSalida() = parametrosSalida.Item("TEXT").ToString.Split("|")

                    If pSalida.Length > 1 Then
                        If pSalida(5) = "A" And pSalida(4) = "C" Then
                            _Referencia = pSalida(1)
                            _NumTarjeta = pSalida(2)
                            _Importe = pSalida(3)
                            _Respuesta = IIf(pSalida(5) = "A", "Approved", pSalida(5))
                            _CodigoAut = pSalida(7)
                            _BancoEmisor = pSalida(12)
                            _TipTarjeta = pSalida(13)
                        Else
                            Dim Codigo As String = ""
                            If Not pSalida(6) Is Nothing Then
                                Codigo = pSalida(6)
                            End If
                            Msj = "DECLINADO. Codigo " & Codigo & vbNewLine & ". Declinado por el Banco Emisor."
                            _PinPad.displayText(Msj)
                            _PinPad.releaseDevice()

                            Return sw_regresa
                        End If
                    Else
                        Msj = "" & pSalida(0) & vbNewLine & "Veifique que se esta usando la misma terminal (aficialion y serie)."
                        _PinPad.displayText(Msj)
                        _PinPad.releaseDevice()

                        Return sw_regresa
                    End If
                    _PinPad.releaseDevice()


                    sw_regresa = True
                Catch ex As Exception
                    Msj = "[Banorte_Verificacion] Falla: " + ex.Message
                    _PinPad.displayText(Msj)
                    Return sw_regresa
                End Try
                'End If
            Catch ex As Exception
                Msj = ex.Message
                _PinPad.displayText(Msj)
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_Reversa(ByVal Folio As String _
                                         , ByVal vImporte As String _
                                         , ByRef codigoAut As String _
                                         , ByRef sReferencia As String _
                                         , ByRef MesesDiferido As String _
                                         , ByRef NumPagos As String _
                                         , ByRef TipoPlan As String _
                                         , ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim parametrosEntrada As New Hashtable()
            Dim parametrosSalida As New Hashtable()
            Dim NumSerie As String = ""
            Try
                MesesDiferido = MesesDiferido.Replace(" ", "")
                NumPagos = NumPagos.Replace(" ", "")
                TipoPlan = TipoPlan.Replace(" ", "")

                _MesesDiferido = MesesDiferido
                _NumPagos = NumPagos
                _TipoPlan = TipoPlan
                Try
                    _PinPad.startTransaction()
                    NumSerie = Me.NumeroSerie

                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al iniciar la transacción: " + ex.Message
                    Return sw_regresa
                End Try
                'sw_regresa = fn_Pinpad_Banorte_CrearLlaves(Msj)
                'If sw_regresa Then
                sw_regresa = False
                ''6. CREAMOS LOS HASHTABLE DE PARAMETROS PARA LA TRANSACCION
                'PARAMETROS DE ENTRADA PARA LA TRANSACCION
                parametrosEntrada.Add("MERCHANT_ID", _IdMerchant)
                parametrosEntrada.Add("USER", _User)
                parametrosEntrada.Add("PASSWORD", _Pswd)
                parametrosEntrada.Add("CMD_TRANS", "REVERSAL")
                parametrosEntrada.Add("TERMINAL_ID", NumSerie)
                parametrosEntrada.Add("CONTROL_NUMBER", Folio)
                parametrosEntrada.Add("MODE", _Modo)
                parametrosEntrada.Add("AMOUNT", vImporte.Replace("$", "").Replace(",", ""))
                parametrosEntrada.Add("RESPONSE_LANGUAGE", "EN")
                parametrosEntrada.Add("BANORTE_URL", _UrlBanorte)
                parametrosEntrada.Add("INITIAL_DEFERMENT", _MesesDiferido)
                parametrosEntrada.Add("PAYMENTS_NUMBER", _NumPagos)
                parametrosEntrada.Add("PLAN_TYPE", _TipoPlan)
                'METODO PROCESAR TRANSACCION
                Dim CodigoBanorte, declinadaOffline As String
                Try
                    _PinPad.processTransaction(parametrosEntrada, parametrosSalida)
                    'OBTENEMOS LOS DATOS PARA IDENTIFICAR EL RESULTADO
                    CodigoBanorte = parametrosSalida.Item("PAYW_RESULT")
                    declinadaOffline = parametrosSalida.Item("CHIP_DECLINED")
                    'REVISAMOS SI FUE UN DECLINADO OFFLINE
                    If declinadaOffline = "1" Then
                        Msj = "[Banorte_Cobrar] Declinada Offline"
                        Return sw_regresa
                    Else
                        If CodigoBanorte = "A" Then
                            codigoAut = parametrosSalida.Item("AUTH_CODE")
                            Msj = "[Banorte_Cobrar] Transacción aprobada: " + codigoAut
                        Else
                            Msj = "[Banorte_Cobrar] Transacción declinada. " + parametrosSalida.Item("TEXT")
                            Return sw_regresa
                        End If
                    End If
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al procesar transacción: " + ex.Message
                    Return sw_regresa
                End Try

                Try
                    _PinPad.endTransaction()

                    If (CodigoBanorte = "A") Then
                        _PinPad.displayText("Aprobada: " + codigoAut)
                    Else
                        _PinPad.displayText("Declinada")
                    End If

                    _Banco = "BANORTE"
                    _Afiliacion = parametrosSalida.Item("MERCHANT_ID")
                    _TerminalId = NumSerie
                    _NumControl = parametrosSalida.Item("CONTROL_NUMBER")
                    _NumTarjeta = "" ''parametrosSalida.Item("CARD_NUMBER")
                    ''_NumTarjeta = "**** **** **** " & _NumTarjeta.Substring(_NumTarjeta.Length - 4, 4)
                    _VigTarjeta = "" ''parametrosSalida.Item("CARD_EXP")
                    _TitTarjeta = "" ''parametrosSalida.Item("CARD_HOLDER")
                    _TipTarjeta = "" ''parametrosSalida.Item("APN")
                    _BancoEmisor = "" ''parametrosSalida.Item("ISSUING_BANK")
                    _Referencia = parametrosSalida.Item("REFERENCE")
                    sReferencia = _Referencia
                    _Respuesta = parametrosSalida.Item("TEXT")
                    _CodigoAut = parametrosSalida.Item("AUTH_CODE")
                    Double.TryParse(vImporte.Replace("$", "").Replace(",", ""), _Importe)
                    Dim _dia As Integer
                    Dim _mes As Integer
                    Dim _anio As Integer
                    _dia = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(6, 2)
                    _mes = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(4, 2)
                    _anio = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(0, 4)
                    _fecha = New Date(_anio, _mes, _dia)
                    _hora = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(9, parametrosSalida.Item("CUST_REQ_DATE").ToString.Length - 1 - 9)

                    _PinPad.releaseDevice()

                    sw_regresa = True
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla: " + ex.Message
                    Return sw_regresa
                End Try
                'End If
            Catch ex As Exception
                Msj = ex.Message
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_Devolucion(ByVal Folio As String _
                                         , ByVal vImporte As String _
                                         , ByRef codigoAut As String _
                                         , ByRef sReferencia As String _
                                         , ByRef MesesDiferido As String _
                                         , ByRef NumPagos As String _
                                         , ByRef TipoPlan As String _
                                         , ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim parametrosEntrada As New Hashtable()
            Dim parametrosSalida As New Hashtable()
            Dim NumSerie As String = ""
            Try
                MesesDiferido = MesesDiferido.Replace(" ", "")
                NumPagos = NumPagos.Replace(" ", "")
                TipoPlan = TipoPlan.Replace(" ", "")

                _MesesDiferido = MesesDiferido
                _NumPagos = NumPagos
                _TipoPlan = TipoPlan
                Try
                    _PinPad.startTransaction()
                    NumSerie = Me.NumeroSerie

                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al iniciar la transacción: " + ex.Message
                    Return sw_regresa
                End Try
                'sw_regresa = fn_Pinpad_Banorte_CrearLlaves(Msj)
                'If sw_regresa Then
                sw_regresa = False
                ''6. CREAMOS LOS HASHTABLE DE PARAMETROS PARA LA TRANSACCION
                'PARAMETROS DE ENTRADA PARA LA TRANSACCION
                parametrosEntrada.Add("MERCHANT_ID", _IdMerchant)
                parametrosEntrada.Add("USER", _User)
                parametrosEntrada.Add("PASSWORD", _Pswd)
                parametrosEntrada.Add("CMD_TRANS", "REFUND")
                parametrosEntrada.Add("TERMINAL_ID", NumSerie)
                parametrosEntrada.Add("CONTROL_NUMBER", Folio)
                parametrosEntrada.Add("REFERENCE", sReferencia)
                parametrosEntrada.Add("MODE", _Modo)
                parametrosEntrada.Add("AMOUNT", vImporte.Replace("$", "").Replace(",", ""))
                parametrosEntrada.Add("RESPONSE_LANGUAGE", "EN")
                parametrosEntrada.Add("BANORTE_URL", _UrlBanorte)
                parametrosEntrada.Add("INITIAL_DEFERMENT", _MesesDiferido)
                parametrosEntrada.Add("PAYMENTS_NUMBER", _NumPagos)
                parametrosEntrada.Add("PLAN_TYPE", _TipoPlan)
                'METODO PROCESAR TRANSACCION
                Dim CodigoBanorte, declinadaOffline As String
                Try
                    _PinPad.processTransaction(parametrosEntrada, parametrosSalida)
                    'OBTENEMOS LOS DATOS PARA IDENTIFICAR EL RESULTADO
                    CodigoBanorte = parametrosSalida.Item("PAYW_RESULT")
                    declinadaOffline = parametrosSalida.Item("CHIP_DECLINED")
                    'REVISAMOS SI FUE UN DECLINADO OFFLINE
                    If declinadaOffline = "1" Then
                        Msj = "[Banorte_Cobrar] Declinada Offline"
                        Return sw_regresa
                    Else
                        If CodigoBanorte = "A" Then
                            codigoAut = parametrosSalida.Item("AUTH_CODE")
                            Msj = "[Banorte_Cobrar] Transacción aprobada: " + codigoAut
                        Else
                            Msj = "[Banorte_Cobrar] Transacción declinada. " + parametrosSalida.Item("TEXT")
                            Return sw_regresa
                        End If
                    End If
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al procesar transacción: " + ex.Message
                    Return sw_regresa
                End Try

                Try
                    _PinPad.endTransaction()

                    If (CodigoBanorte = "A") Then
                        _PinPad.displayText("Aprobada: " + codigoAut)
                    Else
                        _PinPad.displayText("Declinada")
                    End If

                    _Banco = "BANORTE"
                    _Afiliacion = parametrosSalida.Item("MERCHANT_ID")
                    _TerminalId = NumSerie
                    _NumControl = parametrosSalida.Item("CONTROL_NUMBER")
                    _NumTarjeta = parametrosSalida.Item("REFERRED_CARD")
                    _NumTarjeta = "**** **** **** " & parametrosSalida.Item("REFERRED_CARD")
                    _VigTarjeta = "" ''parametrosSalida.Item("CARD_EXP")
                    _TitTarjeta = "" ''parametrosSalida.Item("CARD_HOLDER")
                    _TipTarjeta = "" ''parametrosSalida.Item("APN")
                    _BancoEmisor = "" ''parametrosSalida.Item("ISSUING_BANK")
                    _Referencia = parametrosSalida.Item("REFERENCE")
                    sReferencia = _Referencia
                    _Respuesta = parametrosSalida.Item("TEXT")
                    _CodigoAut = parametrosSalida.Item("AUTH_CODE")
                    Double.TryParse(vImporte.Replace("$", "").Replace(",", ""), _Importe)
                    Dim _dia As Integer
                    Dim _mes As Integer
                    Dim _anio As Integer
                    _dia = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(6, 2)
                    _mes = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(4, 2)
                    _anio = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(0, 4)
                    _fecha = New Date(_anio, _mes, _dia)
                    _hora = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(9, parametrosSalida.Item("CUST_REQ_DATE").ToString.Length - 1 - 9)

                    _PinPad.releaseDevice()

                    sw_regresa = True
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla: " + ex.Message
                    Return sw_regresa
                End Try
                'End If
            Catch ex As Exception
                Msj = ex.Message
            End Try
            Return sw_regresa
        End Function
        Public Function fn_Pinpad_Banorte_Cancelacion(ByVal Folio As String _
                                         , ByVal vImporte As String _
                                         , ByRef codigoAut As String _
                                         , ByRef sReferencia As String _
                                         , ByRef MesesDiferido As String _
                                         , ByRef NumPagos As String _
                                         , ByRef TipoPlan As String _
                                         , ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Dim parametrosEntrada As New Hashtable()
            Dim parametrosSalida As New Hashtable()
            Dim NumSerie As String = ""
            Try
                MesesDiferido = MesesDiferido.Replace(" ", "")
                NumPagos = NumPagos.Replace(" ", "")
                TipoPlan = TipoPlan.Replace(" ", "")

                _MesesDiferido = MesesDiferido
                _NumPagos = NumPagos
                _TipoPlan = TipoPlan
                Try
                    _PinPad.startTransaction()
                    NumSerie = Me.NumeroSerie

                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al iniciar la transacción: " + ex.Message
                    Return sw_regresa
                End Try
                'sw_regresa = fn_Pinpad_Banorte_CrearLlaves(Msj)
                'If sw_regresa Then
                sw_regresa = False
                ''6. CREAMOS LOS HASHTABLE DE PARAMETROS PARA LA TRANSACCION
                'PARAMETROS DE ENTRADA PARA LA TRANSACCION
                parametrosEntrada.Add("MERCHANT_ID", _IdMerchant)
                parametrosEntrada.Add("USER", _User)
                parametrosEntrada.Add("PASSWORD", _Pswd)
                parametrosEntrada.Add("CMD_TRANS", "VOID")
                parametrosEntrada.Add("TERMINAL_ID", NumSerie)
                parametrosEntrada.Add("CONTROL_NUMBER", Folio)
                parametrosEntrada.Add("REFERENCE", sReferencia)
                parametrosEntrada.Add("MODE", _Modo)
                parametrosEntrada.Add("AMOUNT", vImporte.Replace("$", "").Replace(",", ""))
                parametrosEntrada.Add("RESPONSE_LANGUAGE", "EN")
                parametrosEntrada.Add("BANORTE_URL", _UrlBanorte)
                parametrosEntrada.Add("INITIAL_DEFERMENT", _MesesDiferido)
                parametrosEntrada.Add("PAYMENTS_NUMBER", _NumPagos)
                parametrosEntrada.Add("PLAN_TYPE", _TipoPlan)
                'METODO PROCESAR TRANSACCION
                Dim CodigoBanorte, declinadaOffline As String
                Try
                    _PinPad.processTransaction(parametrosEntrada, parametrosSalida)
                    'OBTENEMOS LOS DATOS PARA IDENTIFICAR EL RESULTADO
                    CodigoBanorte = parametrosSalida.Item("PAYW_RESULT")
                    declinadaOffline = parametrosSalida.Item("CHIP_DECLINED")
                    'REVISAMOS SI FUE UN DECLINADO OFFLINE
                    If declinadaOffline = "1" Then
                        Msj = "[Banorte_Cobrar] Declinada Offline"
                        Return sw_regresa
                    Else
                        If CodigoBanorte = "A" Then
                            codigoAut = parametrosSalida.Item("AUTH_CODE")
                            Msj = "[Banorte_Cobrar] Transacción aprobada: " + codigoAut
                        Else
                            Msj = "[Banorte_Cobrar] Transacción declinada. " + parametrosSalida.Item("TEXT")
                            Return sw_regresa
                        End If
                    End If
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla al procesar transacción: " + ex.Message
                    Return sw_regresa
                End Try

                Try
                    _PinPad.endTransaction()

                    If (CodigoBanorte = "A") Then
                        _PinPad.displayText("Aprobada: " + codigoAut)
                    Else
                        _PinPad.displayText("Declinada")
                    End If

                    _Banco = "BANORTE"
                    _Afiliacion = parametrosSalida.Item("MERCHANT_ID")
                    _TerminalId = NumSerie
                    _NumControl = parametrosSalida.Item("CONTROL_NUMBER")
                    _NumTarjeta = parametrosSalida.Item("REFERRED_CARD")
                    _NumTarjeta = "**** **** **** " & parametrosSalida.Item("REFERRED_CARD")
                    _VigTarjeta = "" ''parametrosSalida.Item("CARD_EXP")
                    _TitTarjeta = "" ''parametrosSalida.Item("CARD_HOLDER")
                    _TipTarjeta = "" ''parametrosSalida.Item("APN")
                    _BancoEmisor = "" ''parametrosSalida.Item("ISSUING_BANK")
                    _Referencia = parametrosSalida.Item("REFERENCE")
                    sReferencia = _Referencia
                    _Respuesta = parametrosSalida.Item("TEXT")
                    _CodigoAut = parametrosSalida.Item("AUTH_CODE")
                    Double.TryParse(vImporte.Replace("$", "").Replace(",", ""), _Importe)
                    Dim _dia As Integer
                    Dim _mes As Integer
                    Dim _anio As Integer
                    _dia = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(6, 2)
                    _mes = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(4, 2)
                    _anio = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(0, 4)
                    _fecha = New Date(_anio, _mes, _dia)
                    _hora = parametrosSalida.Item("CUST_REQ_DATE").ToString.Substring(9, parametrosSalida.Item("CUST_REQ_DATE").ToString.Length - 1 - 9)

                    _PinPad.releaseDevice()

                    sw_regresa = True
                Catch ex As Exception
                    Msj = "[Banorte_Cobrar] Falla: " + ex.Message
                    Return sw_regresa
                End Try
                'End If
            Catch ex As Exception
                Msj = ex.Message
            End Try
            Return sw_regresa
        End Function
        Public Function FillDatos(P_UrlBanorte As String _
               , P_Puerto As String _
               , P_User As String _
               , P_Pswd As String _
               , P_IdMerchant As String _
               , P_Modo As String _
               , P_Caja As String _
               , P_id_empresa As String _
               , P_empresa As String _
               , P_rfc As String _
               , P_direccion As String _
               , P_direccion2 As String _
               , P_tipo As String _
               , P_transaccion As Integer _
               , P_id_sucursal As String _
               , P_fecha As Date _
               , P_hora As String _
               , P_Banco As String _
               , P_Respuesta As String _
               , P_Afiliacion As String _
               , P_TerminalId As String _
               , P_NumControl As String _
               , P_NumTarjeta As String _
               , P_TipTarjeta As String _
               , P_VigTarjeta As String _
               , P_TitTarjeta As String _
               , P_BancoEmisor As String _
               , P_CodigoAut As String _
               , P_Referencia As String _
               , P_Importe As Double _
               , P_MesesDiferido As String _
               , P_NumPagos As String _
               , P_TipoPlan As String _
               , ByRef Msj As String) As Boolean
            Dim sw_regresa As Boolean = False
            Msj = ""
            Try
                _UrlBanorte = P_UrlBanorte
                _Puerto = P_Puerto
                _User = P_User
                _Pswd = P_Pswd
                _IdMerchant = P_IdMerchant
                _Modo = P_Modo
                _Caja = P_Caja

                _id_empresa = P_id_empresa
                _empresa = P_empresa
                _rfc = P_rfc
                _direccion = P_direccion
                _direccion2 = P_direccion2
                _tipo = P_tipo
                _transaccion = P_transaccion
                _id_sucursal = P_id_sucursal
                _fecha = P_fecha
                _hora = P_hora
                _Banco = P_Banco
                _Respuesta = P_Respuesta
                _Afiliacion = P_Afiliacion
                _TerminalId = P_TerminalId
                _NumControl = P_NumControl
                _NumTarjeta = P_NumTarjeta
                _TipTarjeta = P_TipTarjeta
                _VigTarjeta = P_VigTarjeta
                _TitTarjeta = P_TitTarjeta
                _BancoEmisor = P_BancoEmisor
                _CodigoAut = P_CodigoAut
                _Referencia = P_Referencia
                _Importe = P_Importe
                _MesesDiferido = P_MesesDiferido
                _NumPagos = P_NumPagos
                _TipoPlan = P_TipoPlan
                sw_regresa = True
            Catch ex As Exception
                sw_regresa = False
                Msj = ex.Message
            End Try
            Return sw_regresa
        End Function
        Private Function ValidaNothing(ByVal sValor As String) As String
            Return IIf(sValor IsNot Nothing, IIf(Not IsDBNull(sValor), sValor, ""), "")
        End Function
        Public ReadOnly Property NumeroSerie As String
            Get
                ''1. CREAMOS EL HASHTABLE PARA OBTENER LA INFORMACIÓN
                Dim salidaInformacion As New Hashtable()
                Dim NS As String
                Try
                    _PinPad.getInformation(salidaInformacion)
                    NS = salidaInformacion.Item("SERIAL_NUMBER")
                Catch ex As Exception
                    NS = ""
                End Try
                'OBTENEMOS LA INFORMACIÓN DEL DISPOSITIVO
                Return NS
            End Get
        End Property

        Public Property Id_empresa As String
            Get
                Return ValidaNothing(_id_empresa)
            End Get
            Set(value As String)
                _id_empresa = value
            End Set
        End Property

        Public Property Empresa As String
            Get
                Return ValidaNothing(_empresa)
            End Get
            Set(value As String)
                _empresa = value
            End Set
        End Property

        Public Property Rfc As String
            Get
                Return ValidaNothing(_rfc)
            End Get
            Set(value As String)
                _rfc = value
            End Set
        End Property

        Public Property Direccion As String
            Get
                Return ValidaNothing(_direccion)
            End Get
            Set(value As String)
                _direccion = value
            End Set
        End Property

        Public Property Direccion2 As String
            Get
                Return ValidaNothing(_direccion2)
            End Get
            Set(value As String)
                _direccion2 = value
            End Set
        End Property

        Public ReadOnly Property Transaccion As Integer
            Get
                Return ValidaNothing(_transaccion)
            End Get
        End Property

        Public ReadOnly Property Id_sucursal As String
            Get
                Return ValidaNothing(_id_sucursal)
            End Get
        End Property

        Public ReadOnly Property Fecha As Date
            Get
                Return IIf(_fecha.Year = 1, Today.Date, _fecha)
            End Get
        End Property

        Public ReadOnly Property Hora As String
            Get
                Return ValidaNothing(_hora)
            End Get
        End Property

        Public ReadOnly Property Banco As String
            Get
                Return ValidaNothing(_Banco)
            End Get
        End Property

        Public ReadOnly Property Afiliacion As String
            Get
                Return ValidaNothing(_Afiliacion)
            End Get
        End Property

        Public ReadOnly Property TerminalId As String
            Get
                Return ValidaNothing(_TerminalId)
            End Get
        End Property

        Public Property NumControl As String
            Set(value As String)
                _NumControl = value
            End Set
            Get
                Return ValidaNothing(_NumControl)
            End Get
        End Property

        Public ReadOnly Property NumTarjeta As String
            Get
                Return ValidaNothing(_NumTarjeta)
            End Get
        End Property

        Public ReadOnly Property VigTarjeta As String
            Get
                Return ValidaNothing(_VigTarjeta)
            End Get
        End Property

        Public ReadOnly Property TitTarjeta As String
            Get
                Return ValidaNothing(_TitTarjeta)
            End Get
        End Property

        Public ReadOnly Property BancoEmisor As String
            Get
                Return ValidaNothing(_BancoEmisor)
            End Get
        End Property

        Public ReadOnly Property CodigoAut As String
            Get
                Return ValidaNothing(_CodigoAut)
            End Get
        End Property

        Public Property Referencia As String
            Get
                Return ValidaNothing(_Referencia)
            End Get
            Set(value As String)
                _Referencia = value
            End Set
        End Property
        Public ReadOnly Property Importe As Double
            Get
                Return ValidaNothing(_Importe)
            End Get
        End Property

        Public ReadOnly Property Respuesta As String
            Get
                Return ValidaNothing(_Respuesta)
            End Get
        End Property

        Public ReadOnly Property TipTarjeta As String
            Get
                Return ValidaNothing(_TipTarjeta)
            End Get
        End Property

        Public Property Tipo As String
            Set(value As String)
                _tipo = value
            End Set
            Get
                Return ValidaNothing(_tipo)
            End Get
        End Property

        Public ReadOnly Property MesesDiferido As String
            Get
                Return ValidaNothing(_MesesDiferido)
            End Get
        End Property

        Public ReadOnly Property NumPagos As String
            Get
                Return ValidaNothing(_NumPagos)
            End Get
        End Property

        Public ReadOnly Property TipoPlan As String
            Get
                Return ValidaNothing(_TipoPlan)
            End Get
        End Property

        Public ReadOnly Property NomTipoPlan As String
            Get
                Dim sPlan As String = ""
                Select Case ValidaNothing(_TipoPlan)
                    Case "03"
                        sPlan = " Meses Sin Intereses"
                    Case "07"
                        sPlan = ""
                End Select
                Return sPlan
            End Get
        End Property

        Public Property UrlBanorte As String
            Get
                Return ValidaNothing(_UrlBanorte)
            End Get
            Set(value As String)
                _UrlBanorte = value
            End Set
        End Property

        Public Property Puerto As String
            Get
                Return ValidaNothing(_Puerto)
            End Get
            Set(value As String)
                _Puerto = value
            End Set
        End Property

        Public Property User As String
            Get
                Return ValidaNothing(_User)
            End Get
            Set(value As String)
                _User = value
            End Set
        End Property

        Public Property Pswd As String
            Get
                Return ValidaNothing(_Pswd)
            End Get
            Set(value As String)
                _Pswd = value
            End Set
        End Property

        Public Property IdMerchant As String
            Get
                Return ValidaNothing(_IdMerchant)
            End Get
            Set(value As String)
                _IdMerchant = value
            End Set
        End Property

        Public Property Modo As String
            Get
                Return ValidaNothing(_Modo)
            End Get
            Set(value As String)
                _Modo = value
            End Set
        End Property

        Public Property Caja As String
            Get
                Return ValidaNothing(_Caja)
            End Get
            Set(value As String)
                _Caja = value
            End Set
        End Property

        Public Property Entry_Mode As String
            Get
                Return _Entry_Mode
            End Get
            Set(value As String)
                _Entry_Mode = value
            End Set
        End Property
    End Class
End Namespace