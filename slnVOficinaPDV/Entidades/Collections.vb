﻿' ============================================================================
'    Author: Kenneth Perkins
'    Date:   Nov 13, 2020
'    Taken From: http://programmingnotes.org/
'    File: Utils.vb
'    Description: Handles general utility functions
' ============================================================================
Option Strict On
Option Explicit On

Namespace Global.Utils
    Namespace Collections
        Public Module modCollections
            ''' <summary>
            ''' Returns a List(Of T) of the items contained in the DataTable
            ''' </summary>
            ''' <param name="source">The DataTable to convert to a List(Of T)</param> 
            ''' <returns>A List(Of T) with the contents of the DataTable</returns>
            <Runtime.CompilerServices.Extension()>
            Public Function ToList(Of T)(source As DataTable) As List(Of T)
                Dim type = GetType(T)
                Dim list = New List(Of T)
                Dim isPrimitive = IsPrimitiveType(type)
                Dim members = New List(Of System.Reflection.MemberInfo)

                If Not isPrimitive Then
                    members.AddRange(type.GetProperties)
                    members.AddRange(type.GetFields)
                End If

                ' Convert data rows to the object
                For Each row As DataRow In source.Rows
                    Dim obj = CreateObject(Of T)(isPrimitive, row, members)
                    list.Add(obj)
                Next
                Return list
            End Function

            ''' <summary>
            ''' Returns a DataTable of the items contained in the IEnumerable(Of T)
            ''' </summary>
            ''' <param name="source">The IEnumerable list to convert to a DataTable</param> 
            ''' <returns>A DataTable with the contents of the IEnumerable(Of T)</returns>
            <Runtime.CompilerServices.Extension()>
            Public Function ToDataTable(Of T)(source As IEnumerable(Of T)) As DataTable
                Dim type = GetType(T)
                Dim table = New DataTable(type.Name)
                Dim isPrimitive = IsPrimitiveType(type)
                Dim primitiveColumnName = "value"
                Dim members = New List(Of System.Reflection.MemberInfo)

                If Not isPrimitive Then
                    Dim flags = System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.Public
                    members.AddRange(type.GetProperties(flags))
                    members.AddRange(type.GetFields(flags))
                End If

                ' Build the header rows
                BuildColumns(Of T)(isPrimitive, primitiveColumnName, table, members)

                ' Add the data rows
                For Each item In source
                    Dim row = CreateRow(isPrimitive, primitiveColumnName, item, table, members)
                    table.Rows.Add(row)
                Next
                Return table
            End Function

            Private Sub BuildColumns(Of T)(isPrimitive As Boolean,
                                         primitiveColumnName As String,
                                         table As DataTable,
                                         members As IEnumerable(Of System.Reflection.MemberInfo))
                If isPrimitive Then
                    AddColumn(table, primitiveColumnName, GetType(T))
                Else
                    For Each member In members
                        AddColumn(table, member.Name, GetMemberDataType(member))
                    Next
                End If
            End Sub

            Private Sub AddColumn(table As DataTable, columnName As String, type As System.Type)
                table.Columns.Add(columnName, If(IsNullable(type), System.Nullable.GetUnderlyingType(type), type))
            End Sub

            Private Function CreateRow(Of T)(isPrimitive As Boolean,
                                   primitiveColumnName As String,
                                   obj As T,
                                   table As DataTable,
                                   members As IEnumerable(Of System.Reflection.MemberInfo)) As DataRow
                Dim row = table.NewRow
                If isPrimitive Then
                    SetRowValue(row, primitiveColumnName, obj)
                Else
                    For Each member In members
                        Dim value = If(CanRead(member), GetMemberValue(obj, member), Nothing)
                        SetRowValue(row, member.Name, value)
                    Next
                End If
                Return row
            End Function

            Private Sub SetRowValue(row As DataRow, columnName As String, value As Object)
                row(columnName) = If(value Is Nothing, System.DBNull.Value, value)
            End Sub

            Private Function CreateObject(Of T)(isPrimitive As Boolean, row As DataRow, members As IEnumerable(Of System.Reflection.MemberInfo)) As T
                Dim obj As T
                If isPrimitive Then
                    SetObjectValue(isPrimitive, obj, row(0), Nothing, "")
                Else
                    obj = System.Activator.CreateInstance(Of T)()
                    For Each column As DataColumn In row.Table.Columns
                        Dim member = members.FirstOrDefault(Function(x) x.Name.ToLower = column.ColumnName.ToLower)
                        If member Is Nothing _
                            OrElse Not CanWrite(member) Then
                            Continue For
                        End If
                        Dim value = row(column.ColumnName)
                        Dim tipo_dato As String = GetMemberDataType(member).Name.ToUpper
                        SetObjectValue(isPrimitive, obj, value, member, tipo_dato)
                    Next
                End If
                Return obj
            End Function

            Private Sub SetObjectValue(Of T)(isPrimitive As Boolean, ByRef obj As T, value As Object, Optional member As System.Reflection.MemberInfo = Nothing, Optional tipo_dato As String = "")
                If IsDBNull(value) Then
                    Dim type = If(isPrimitive, GetType(T), GetMemberDataType(member))
                    Dim nullValue = If(IsNullable(type), Nothing, GetDefaultValue(type))
                    If isPrimitive Then
                        obj = CType(nullValue, T)
                    Else
                        SetObjectValue(obj, member, nullValue, tipo_dato)
                    End If
                Else
                    If isPrimitive Then
                        obj = CType(value, T)
                    Else
                        SetObjectValue(obj, member, value, tipo_dato)
                    End If
                End If
            End Sub

            Private Sub SetObjectValue(Of T)(ByRef obj As T, member As System.Reflection.MemberInfo, value As Object, Optional tipo_dato As String = "")
                ' Boxing method used for modifying structures
                Dim boxed = If(obj.GetType.IsValueType, CType(obj, Object), obj)
                SetMemberValue(boxed, member, value, tipo_dato)
                obj = CType(boxed, T)
            End Sub

            Private Sub SetMemberValue(Of T)(ByRef obj As T, member As System.Reflection.MemberInfo, value As Object, Optional tipo_dato As String = "")
                If IsProperty(member) Then
                    Dim prop = CType(member, System.Reflection.PropertyInfo)
                    If prop.SetMethod IsNot Nothing Then
                        If tipo_dato = "STRING" Then
                            prop.SetValue(obj, value.ToString)
                        ElseIf tipo_dato = "DOUBLE" Then
                            prop.SetValue(obj, Valor(value))
                        Else
                            prop.SetValue(obj, value)
                        End If
                    End If
                ElseIf IsField(member) Then
                    Dim field = CType(member, System.Reflection.FieldInfo)
                    field.SetValue(obj, value)
                End If
            End Sub

            Private Function GetMemberValue(obj As Object, member As System.Reflection.MemberInfo) As Object
                Dim result As Object = Nothing
                If IsProperty(member) Then
                    Dim prop = CType(member, System.Reflection.PropertyInfo)
                    result = prop.GetValue(obj, If(prop.GetIndexParameters.Count = 1, New Object() {Nothing}, Nothing))
                ElseIf IsField(member) Then
                    Dim field = CType(member, System.Reflection.FieldInfo)
                    result = field.GetValue(obj)
                End If
                Return result
            End Function

            Private Function GetDefaultValue(type As System.Type) As Object
                Return Conversion.CTypeDynamic(Nothing, type)
            End Function

            Private Function IsPrimitiveType(type As System.Type) As Boolean
                Return type.IsPrimitive _
                    OrElse type.IsEnum _
                    OrElse (type.IsValueType AndAlso Not IsCustomStructure(type)) _
                    OrElse IsType(type, GetType(String))
            End Function

            Private Function IsCustomStructure(type As System.Type) As Boolean
                Dim result = type.IsValueType AndAlso Not type.IsPrimitive _
                    AndAlso (String.IsNullOrWhiteSpace(type.Namespace) _
                        OrElse Not type.Namespace.StartsWith("System") _
                        OrElse Not type.FullName.StartsWith("System.")) _
                    AndAlso Not System.Reflection.Assembly.GetAssembly(type).Location.ToLower.Contains("microsoft")
                Return result
            End Function

            Private Function IsNullable(type As System.Type) As Boolean
                Return System.Nullable.GetUnderlyingType(type) IsNot Nothing
            End Function

            Private Function GetMemberDataType(member As System.Reflection.MemberInfo) As System.Type
                Dim result As System.Type = Nothing
                If IsProperty(member) Then
                    result = CType(member, System.Reflection.PropertyInfo).PropertyType
                ElseIf IsField(member) Then
                    result = CType(member, System.Reflection.FieldInfo).FieldType
                End If
                Return result
            End Function

            Private Function CanWrite(member As System.Reflection.MemberInfo) As Boolean
                Return If(IsProperty(member), CType(member, System.Reflection.PropertyInfo).CanWrite, IsField(member))
            End Function

            Private Function CanRead(member As System.Reflection.MemberInfo) As Boolean
                Return If(IsProperty(member), CType(member, System.Reflection.PropertyInfo).CanRead, IsField(member))
            End Function

            Private Function IsProperty(member As System.Reflection.MemberInfo) As Boolean
                Return IsType(member.GetType, GetType(System.Reflection.PropertyInfo))
            End Function

            Private Function IsField(member As System.Reflection.MemberInfo) As Boolean
                Return IsType(member.GetType, GetType(System.Reflection.FieldInfo))
            End Function

            Private Function IsType(type As System.Type, targetType As System.Type) As Boolean
                Return type.Equals(targetType) OrElse type.IsSubclassOf(targetType)
            End Function
            Private Function Valor(ByVal Cantidad As Object) As Double
                If IsDBNull(Cantidad) Then
                    Valor = 0.0
                Else
                    If Cantidad.ToString.Contains(",") Then
                        Cantidad = Cantidad.ToString().Replace(",", "")
                    End If
                    If Cantidad.ToString.Contains("%") Then
                        Cantidad = Cantidad.ToString().Replace("%", "")
                    End If
                    If Cantidad.ToString.Contains("$") Then
                        Cantidad = Cantidad.ToString().Replace("$", "")
                    End If
                    If IsNumeric(Cantidad) Then
                        Valor = Val(Cantidad)
                    Else
                        Valor = 0.0
                    End If
                End If
            End Function
        End Module
    End Namespace
End Namespace ' http://programmingnotes.org/