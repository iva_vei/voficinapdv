﻿Imports DevExpress.XtraBars
Public Class SelArticulos
    Private SW_refrescar As Boolean = False
    Private Modo As Globales.TipoABC
    Private _Sw_Seleccionar As Boolean = False
    Private _Sw_Impresion As Boolean = False
    Private _Version As String = ""
    Public Property Sw_Seleccionar As Boolean
        Get
            Return _Sw_Seleccionar
        End Get
        Set(value As Boolean)
            _Sw_Seleccionar = value
        End Set
    End Property

    Public Property Sw_Impresion As Boolean
        Get
            Return _Sw_Impresion
        End Get
        Set(value As Boolean)
            _Sw_Impresion = value
        End Set
    End Property

    Public Property Version As String
        Get
            Return _Version
        End Get
        Set(value As String)
            _Version = value
        End Set
    End Property

    Public Sub New()
        InitializeComponent()
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControl.OptionsFocus.EnableAutoTabOrder = False
        Me.GridV_1.Appearance.Row.ForeColor = System.Drawing.Color.Black
    End Sub
    Private Sub ABCPantallas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim msj As String = ""
        Try
            Modo = Globales.TipoABC.Indice

            txt_Codigoref.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtCodigoRef", "")
            txt_CodigoBarra.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtCodigoBarra", "")
            txt_Marca.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtMarca", "")
            txt_Clase.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtClase", "")
            txt_Nombre.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtNombre", "")
            txt_NomCorto.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtNomCorto", "")

            If txt_Codigoref.Text <> "" _
            Or txt_CodigoBarra.Text <> "" _
            Or txt_Marca.Text <> "" _
            Or txt_Clase.Text <> "" _
            Or txt_Nombre.Text <> "" _
            Or txt_NomCorto.Text <> "" Then
                Call Refrescar()
            End If
            If Sw_Seleccionar Then
                GridV_1.OptionsSelection.MultiSelect = True
                GridV_1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
            Else
                GridV_1.OptionsSelection.MultiSelect = False
                GridV_1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
            End If
            ''Call ControlBotones("Ok")
            ''Call Botones("Ok")
            ''Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Public Sub Refrescar()
        Call Mostrar_Indice()

    End Sub
    Public Sub Imprimir()
        Dim Formularios As Globales.clsFormularios
        Try
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "IMPRIMIR") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Formularios = New Globales.clsFormularios
            Formularios.Imprimir(Me.Name, "Formulario Plantilla", GridC_1)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Excel()
        Dim Formularios As Globales.clsFormularios
        Try
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "EXCEL") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Formularios = New Globales.clsFormularios
            Formularios.ExportarXLS(Me.Name, "Formulario Plantilla", GridC_1, Globales.oAmbientes.Sistema, Application.ProductName, "ICC")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Regresar()
        Try
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Titulo_BtnPanel(ByVal sw_visible)
        PanelFiltros_Acciones.Visible = sw_visible
        If PanelFiltros_Acciones.Visible = True Then
            btn_OcultarMostrar_Acciones.Text = "Ocultar Filtros y Acciones"
        Else
            btn_OcultarMostrar_Acciones.Text = "Mostrar Filtros y Acciones"
        End If
    End Sub
    Private Sub btn_OcultarMostrar_Acciones_Click(sender As Object, e As EventArgs) Handles btn_OcultarMostrar_Acciones.Click
        Call Titulo_BtnPanel(Not PanelFiltros_Acciones.Visible)
    End Sub

    Private Sub Botones(ByVal tag As String)
        Dim btnOk As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Ok")
        Dim btnCancelar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Cancelar")

        ''Select Case tag
        ''    Case "Ok", "Cancelar", "Consulta"
        ''        btnOk.Visible = False
        ''        btnCancelar.Visible = False
        ''End Select
    End Sub
    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0
        Try
            Select Case tag
                Case "Cancelar"
                    Call Botones(tag)
                    Call Regresar()
                Case "Ok"
                    If GridV_1.RowCount > 0 Then
                        txt_Articulo.Text = GridV_1.GetFocusedRowCellValue(col1_Articulo)
                    Else
                        txt_Articulo.Text = ""
                    End If
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtCodigoRef", txt_Codigoref.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtCodigoBarra", txt_CodigoBarra.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtMarca", txt_Marca.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtClase", txt_Clase.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtNombre", txt_Nombre.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelArtNomCorto", txt_NomCorto.Text)
                    Call Regresar()
                Case "Imprime Preciador"
                    Select Case Version.ToUpper
                        Case "AA-6X6"
                            Call Impresion_PreciadorAA6X6()
                        Case "NA-3X4"
                            Call Impresion_PreciadorNA3X4()
                        Case "NB-3X4"
                            Call Impresion_PreciadorNA3X4()
                        Case "NA-4X9"
                            Call Impresion_PreciadorNA4X9()
                    End Select
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub Mostrar_Indice()
        SW_refrescar = False
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim IdUsuario As Integer = 0
        Try
            oDatos = New Datos_Viscoi
            band_existencia.Visible = chk_Existencias.Checked
            If oDatos.PVTA_Recupera_Articulos("ACTIVO", txt_Codigoref.Text, txt_CodigoBarra.Text, txt_Marca.Text, txt_Clase.Text, txt_Nombre.Text, txt_NomCorto.Text, txt_Estructura1.Text, chk_Existencias.Checked, dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    GridC_1.DataSource = dtDatos
                    GridV_1.Focus()
                Else
                    SW_refrescar = True
                    GridC_1.DataSource = Nothing
                    MessageBox.Show("No se puede encontrar registros con los filtos espeficiados.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                SW_refrescar = True
                GridC_1.DataSource = Nothing
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            GridV_1.BestFitColumns()
        Catch ex As Exception
            SW_refrescar = True
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
        Call ControlBotones("Ok")
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim oFrm As SelEstructura
        Try
            oFrm = New SelEstructura

            oFrm.ShowDialog()
            txt_Estructura1.Text = oFrm.Estructura1
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_Nombre_LostFocus(sender As Object, e As EventArgs) Handles txt_Codigoref.LostFocus _
                                                                             , txt_CodigoBarra.LostFocus _
                                                                             , txt_Marca.LostFocus _
                                                                             , txt_Clase.LostFocus _
                                                                             , txt_Nombre.LostFocus _
                                                                             , txt_NomCorto.LostFocus
        If txt_Codigoref.Text <> "" _
        Or txt_CodigoBarra.Text <> "" _
        Or txt_Marca.Text <> "" _
        Or txt_Clase.Text <> "" _
        Or txt_Nombre.Text <> "" _
        Or txt_NomCorto.Text <> "" Then
            Call Refrescar()
        End If
    End Sub
    Private Sub SelArticulos_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        If SW_refrescar Then Exit Sub
        Try
            Select Case e.KeyCode
                Case Keys.Enter
                    ControlBotones("Ok")
                Case Keys.Escape
                    txt_Articulo.Text = ""
                    Me.Close()
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SelArticulos_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Try
            txt_Nombre.Focus()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub chk_Existencias_CheckedChanged(sender As Object, e As EventArgs) Handles chk_Existencias.CheckedChanged
        Call Mostrar_Indice()
    End Sub
    Private Sub GridC_1_KeyDown(sender As Object, e As KeyEventArgs) Handles GridC_1.KeyDown
        Call Globales.clsFormularios.Grid_Prevenir_Tab(sender, e, txt_Codigoref)
    End Sub

    Private Sub Impresion_PreciadorAA6X6()
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool

        Dim oPreciadores As New entPreciador
        Dim oRenPrecio As entPreciadorRen
        Dim iColumna As Integer
        Dim iRenglon As Integer
        Dim iI As Integer
        Dim Ultimo As entPreciadorRen
        Dim oReporte As XtraRep_Preciador3
        Try
            iColumna = 0
            iRenglon = 0
            oRenPrecio = New entPreciadorRen
            For iI = 0 To GridV_1.RowCount - 1
                If GridV_1.IsRowSelected(iI) Then
                    Select Case iColumna
                        Case 0
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaA.R1Titulo1 = "PRECIO"
                                    oRenPrecio.ColumnaA.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Estilo)
                                    oRenPrecio.ColumnaA.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                Case 1

                                    oRenPrecio.ColumnaA.R2Titulo1 = "PRECIO"
                                    oRenPrecio.ColumnaA.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Estilo)
                                    oRenPrecio.ColumnaA.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                Case 2

                                    oRenPrecio.ColumnaA.R3Titulo1 = "PRECIO"
                                    oRenPrecio.ColumnaA.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Estilo)
                                    oRenPrecio.ColumnaA.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                            End Select
                            iRenglon += 1

                        Case 1
                            Select Case iRenglon - 1
                                Case 0
                                    oRenPrecio.ColumnaB.R1Titulo1 = "PRECIO"
                                    oRenPrecio.ColumnaB.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Estilo)
                                    oRenPrecio.ColumnaB.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                Case 1
                                    oRenPrecio.ColumnaB.R2Titulo1 = "PRECIO"
                                    oRenPrecio.ColumnaB.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Estilo)
                                    oRenPrecio.ColumnaB.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                Case 2

                                    oRenPrecio.ColumnaB.R3Titulo1 = "PRECIO"
                                    oRenPrecio.ColumnaB.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Estilo)
                                    oRenPrecio.ColumnaB.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                            End Select
                    End Select
                    If iRenglon > 3 Then
                        iRenglon = 0
                        oPreciadores.Renlones.Add(oRenPrecio)
                        oRenPrecio = New entPreciadorRen
                    End If
                    'Ultimo = oRenPrecio
                    iColumna += 1
                    If iColumna > 1 Then
                        iColumna = 0
                        '
                        'Else
                        '    If iI = GridV_1.RowCount - 1 Then
                        '        If iColumna < 2 Then
                        '            iColumna = 0
                        '            oPreciadores.Renlones.Add(Ultimo)
                        '        End If
                        '    End If
                    End If
                End If
            Next
            Debug.Print(oPreciadores.Renlones.Count().ToString)
            If oPreciadores.Renlones.Count() > 0 Then
                oReporte = New XtraRep_Preciador3
                oReporte.ods.DataSource = oPreciadores

                printTool = New DevExpress.XtraReports.UI.ReportPrintTool(oReporte)

                printTool.ShowPreviewDialog()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub Impresion_PreciadorNA3X4()
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool

        Dim sw_solo1vuelta As Boolean = True
        Dim oPreciadores As New entPreciador3
        Dim oRenPrecio As entPreciadorRen3
        Dim iColumna As Integer
        Dim iRenglon As Integer
        Dim TotColumnaMax As Integer = 2 ''se cuenta el cero
        Dim TotRenglonMax As Integer = 3
        Dim iI As Integer
        Dim Ultimo As entPreciadorRen3
        Dim oReporte As Object
        Try
            iColumna = 0
            iRenglon = 0
            oRenPrecio = New entPreciadorRen3
            For iI = 0 To GridV_1.RowCount - 1
                If iI = 207 Then
                    Debug.Print("aki")
                End If
                If GridV_1.IsRowSelected(iI) Then
                    Select Case iColumna
                        Case 0
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaA.R1Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R1Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R1Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R1Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 1

                                    oRenPrecio.ColumnaA.R2Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R2Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R2Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R2Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 2

                                    oRenPrecio.ColumnaA.R3Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R3Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R3Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R3Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 3

                                    oRenPrecio.ColumnaA.R4Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R4Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R4Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R4Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R4Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R4Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R4Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R4Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R4Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R4Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R4Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R4Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R4Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R4Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R4Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 4

                                    oRenPrecio.ColumnaA.R5Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R5Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R5Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R5Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R5Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R5Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R5Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R5Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R5Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R5Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R5Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R5Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R5Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R5Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R5Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")

                                Case 5

                                    oRenPrecio.ColumnaA.R6Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R6Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R6Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R6Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R6Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R6Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R6Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R6Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R6Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R6Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R6Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R6Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R6Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R6Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R6Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 6

                                    oRenPrecio.ColumnaA.R7Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R7Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R7Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R7Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R7Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R7Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R7Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R7Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R7Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R7Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R7Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R7Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R7Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R7Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R7Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 7

                                    oRenPrecio.ColumnaA.R8Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R8Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R8Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R8Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R8Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R8Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R8Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R8Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R8Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R8Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R8Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R8Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R8Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R8Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R8Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 8

                                    oRenPrecio.ColumnaA.R9Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R9Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R9Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R9Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R9Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R9Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R9Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R9Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R9Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaA.R9Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R9Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R9Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R9Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R9Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R9Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                            End Select

                        Case 1
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaB.R1Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R1Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R1Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R1Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 1

                                    oRenPrecio.ColumnaB.R2Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R2Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R2Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R2Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 2

                                    oRenPrecio.ColumnaB.R3Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R3Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R3Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R3Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 3

                                    oRenPrecio.ColumnaB.R4Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R4Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R4Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R4Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R4Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R4Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R4Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R4Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R4Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R4Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R4Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R4Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R4Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R4Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R4Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 4

                                    oRenPrecio.ColumnaB.R5Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R5Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R5Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R5Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R5Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R5Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R5Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R5Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R5Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R5Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R5Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R5Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R5Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R5Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R5Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 5

                                    oRenPrecio.ColumnaB.R6Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R6Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R6Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R6Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R6Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R6Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R6Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R6Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R6Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R6Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R6Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R6Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R6Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R6Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R6Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 6

                                    oRenPrecio.ColumnaB.R7Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R7Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R7Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R7Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R7Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R7Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R7Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R7Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R7Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R7Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R7Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R7Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R7Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R7Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R7Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 7

                                    oRenPrecio.ColumnaB.R8Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R8Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R8Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R8Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R8Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R8Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R8Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R8Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R8Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R8Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R8Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R8Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R8Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R8Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R8Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 8

                                    oRenPrecio.ColumnaB.R9Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R9Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R9Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R9Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R9Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R9Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R9Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R9Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R9Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaB.R9Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R9Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R9Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R9Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R9Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R9Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                            End Select


                        Case 2
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaC.R1Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R1Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R1Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R1Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 1
                                    oRenPrecio.ColumnaC.R2Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R2Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R2Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R2Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 2

                                    oRenPrecio.ColumnaC.R3Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R3Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R3Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R3Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 3

                                    oRenPrecio.ColumnaC.R4Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R4Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R4Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R4Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R4Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R4Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R4Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R4Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R4Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R4Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R4Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R4Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R4Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R4Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R4Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 4

                                    oRenPrecio.ColumnaC.R5Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R5Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R5Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R5Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R5Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R5Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R5Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R5Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R5Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R5Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R5Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R5Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R5Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R5Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R5Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 5

                                    oRenPrecio.ColumnaC.R6Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R6Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R6Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R6Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R6Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R6Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R6Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R6Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R6Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R6Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R6Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R6Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R6Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R6Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R6Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 6

                                    oRenPrecio.ColumnaC.R7Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R7Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R7Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R7Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R7Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R7Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R7Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R7Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R7Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R7Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R7Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R7Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R7Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R7Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R7Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 7

                                    oRenPrecio.ColumnaC.R8Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R8Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R8Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R8Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R8Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R8Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R8Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R8Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R8Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R8Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R8Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R8Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R8Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R8Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R8Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 8

                                    oRenPrecio.ColumnaC.R9Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R9Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R9Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R9Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R9Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R9Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R9Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R9Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R9Caracteristicas = "" & GridV_1.GetRowCellValue(iI, col1_Caracteristicas)
                                    oRenPrecio.ColumnaC.R9Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R9Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R9Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R9Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R9Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R9Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                            End Select
                    End Select
                    If iRenglon > TotRenglonMax Then
                        iRenglon = 0
                        oPreciadores.Renlones.Add(oRenPrecio)
                        oRenPrecio = New entPreciadorRen3
                        sw_solo1vuelta = False
                    End If
                    'Ultimo = oRenPrecio
                    iColumna += 1
                    If iColumna > TotColumnaMax Then
                        iColumna = 0
                        '
                        'Else
                        '    If iI = GridV_1.RowCount - 1 Then
                        '        If iColumna < 2 Then
                        '            iColumna = 0
                        '            oPreciadores.Renlones.Add(Ultimo)
                        '        End If
                        '    End If
                    End If
                End If
            Next
            If iColumna + iRenglon > 0 Then
                oPreciadores.Renlones.Add(oRenPrecio)
            End If
            Debug.Print(oPreciadores.Renlones.Count().ToString)
            If oPreciadores.Renlones.Count() > 0 Then
                Select Case Version.ToUpper
                    Case "NA-3X4"
                        oReporte = New XtraRep_PreciadorNA3X4
                        TryCast(oReporte, XtraRep_PreciadorNA3X4).ods.DataSource = oPreciadores
                    Case "NB-3X4"
                        oReporte = New XtraRep_PreciadorNA3X4B
                        TryCast(oReporte, XtraRep_PreciadorNA3X4B).ods.DataSource = oPreciadores
                End Select

                printTool = New DevExpress.XtraReports.UI.ReportPrintTool(oReporte)

                printTool.ShowPreviewDialog()
            End If

        Catch ex As Exception
            Debug.Print("aki")
        End Try
    End Sub

    Private Sub Impresion_PreciadorNA4X9()
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool

        Dim sw_solo1vuelta As Boolean = True
        Dim oPreciadores As New entPreciador4
        Dim oRenPrecio As entPreciadorRen4
        Dim iColumna As Integer
        Dim iRenglon As Integer
        Dim TotColumnaMax As Integer = 3 ''se cuenta el cero
        Dim TotRenglonMax As Integer = 8
        Dim iI As Integer
        Dim Ultimo As entPreciadorRen4
        Dim oReporte As XtraRep_PreciadorNA4X9
        Try
            iColumna = 0
            iRenglon = 0
            oRenPrecio = New entPreciadorRen4
            For iI = 0 To GridV_1.RowCount - 1
                If GridV_1.IsRowSelected(iI) Then
                    Select Case iColumna
                        Case 0
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaA.R1Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R1Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R1Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 1

                                    oRenPrecio.ColumnaA.R2Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R2Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R2Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 2

                                    oRenPrecio.ColumnaA.R3Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R3Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R3Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 3

                                    oRenPrecio.ColumnaA.R4Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R4Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R4Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R4Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R4Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R4Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R4Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R4Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R4Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R4Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R4Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R4Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R4Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R4Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 4

                                    oRenPrecio.ColumnaA.R5Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R5Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R5Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R5Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R5Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R5Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R5Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R5Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R5Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R5Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R5Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R5Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R5Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R5Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")

                                Case 5

                                    oRenPrecio.ColumnaA.R6Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R6Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R6Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R6Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R6Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R6Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R6Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R6Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R6Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R6Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R6Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R6Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R6Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R6Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 6

                                    oRenPrecio.ColumnaA.R7Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R7Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R7Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R7Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R7Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R7Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R7Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R7Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R7Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R7Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R7Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R7Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R7Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R7Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 7

                                    oRenPrecio.ColumnaA.R8Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R8Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R8Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R8Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R8Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R8Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R8Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R8Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R8Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R8Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R8Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R8Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R8Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R8Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 8

                                    oRenPrecio.ColumnaA.R9Titulo1 = "Contado"
                                    oRenPrecio.ColumnaA.R9Titulo2 = "Credito"
                                    oRenPrecio.ColumnaA.R9Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaA.R9Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaA.R9Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaA.R9Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaA.R9Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaA.R9Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaA.R9Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaA.R9Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaA.R9Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaA.R9Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaA.R9Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaA.R9Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                            End Select

                        Case 1
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaB.R1Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R1Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R1Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 1

                                    oRenPrecio.ColumnaB.R2Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R2Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R2Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 2

                                    oRenPrecio.ColumnaB.R3Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R3Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R3Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 3

                                    oRenPrecio.ColumnaB.R4Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R4Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R4Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R4Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R4Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R4Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R4Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R4Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R4Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R4Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R4Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R4Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R4Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R4Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 4

                                    oRenPrecio.ColumnaB.R5Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R5Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R5Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R5Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R5Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R5Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R5Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R5Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R5Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R5Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R5Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R5Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R5Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R5Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 5

                                    oRenPrecio.ColumnaB.R6Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R6Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R6Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R6Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R6Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R6Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R6Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R6Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R6Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R6Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R6Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R6Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R6Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R6Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 6

                                    oRenPrecio.ColumnaB.R7Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R7Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R7Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R7Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R7Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R7Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R7Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R7Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R7Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R7Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R7Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R7Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R7Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R7Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 7

                                    oRenPrecio.ColumnaB.R8Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R8Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R8Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R8Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R8Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R8Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R8Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R8Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R8Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R8Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R8Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R8Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R8Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R8Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 8

                                    oRenPrecio.ColumnaB.R9Titulo1 = "Contado"
                                    oRenPrecio.ColumnaB.R9Titulo2 = "Credito"
                                    oRenPrecio.ColumnaB.R9Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaB.R9Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaB.R9Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaB.R9Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaB.R9Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaB.R9Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaB.R9Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaB.R9Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaB.R9Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaB.R9Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaB.R9Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaB.R9Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                            End Select


                        Case 2
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaC.R1Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R1Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R1Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 1
                                    oRenPrecio.ColumnaC.R2Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R2Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R2Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 2

                                    oRenPrecio.ColumnaC.R3Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R3Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R3Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 3

                                    oRenPrecio.ColumnaC.R4Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R4Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R4Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R4Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R4Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R4Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R4Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R4Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R4Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R4Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R4Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R4Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R4Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R4Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 4

                                    oRenPrecio.ColumnaC.R5Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R5Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R5Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R5Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R5Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R5Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R5Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R5Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R5Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R5Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R5Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R5Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R5Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R5Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 5

                                    oRenPrecio.ColumnaC.R6Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R6Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R6Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R6Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R6Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R6Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R6Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R6Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R6Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R6Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R6Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R6Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R6Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R6Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 6

                                    oRenPrecio.ColumnaC.R7Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R7Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R7Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R7Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R7Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R7Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R7Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R7Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R7Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R7Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R7Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R7Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R7Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R7Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 7

                                    oRenPrecio.ColumnaC.R8Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R8Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R8Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R8Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R8Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R8Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R8Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R8Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R8Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R8Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R8Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R8Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R8Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R8Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                Case 8

                                    oRenPrecio.ColumnaC.R9Titulo1 = "Contado"
                                    oRenPrecio.ColumnaC.R9Titulo2 = "Credito"
                                    oRenPrecio.ColumnaC.R9Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaC.R9Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaC.R9Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaC.R9Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaC.R9Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaC.R9Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaC.R9Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaC.R9Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaC.R9Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaC.R9Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaC.R9Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaC.R9Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                            End Select


                        Case 3
                            Select Case iRenglon
                                Case 0
                                    oRenPrecio.ColumnaD.R1Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R1Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R1Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R1Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R1Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R1Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R1Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R1Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R1Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R1Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R1Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R1Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R1Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R1Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 1
                                    oRenPrecio.ColumnaD.R2Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R2Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R2Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R2Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R2Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R2Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R2Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R2Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R2Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R2Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R2Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R2Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R2Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R2Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 2

                                    oRenPrecio.ColumnaD.R3Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R3Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R3Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R3Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R3Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R3Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R3Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R3Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R3Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R3Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R3Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R3Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R3Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R3Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 3

                                    oRenPrecio.ColumnaD.R4Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R4Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R4Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R4Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R4Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R4Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R4Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R4Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R4Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R4Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R4Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R4Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R4Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R4Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 4

                                    oRenPrecio.ColumnaD.R5Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R5Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R5Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R5Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R5Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R5Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R5Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R5Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R5Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R5Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R5Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R5Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R5Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R5Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 5

                                    oRenPrecio.ColumnaD.R6Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R6Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R6Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R6Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R6Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R6Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R6Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R6Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R6Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R6Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R6Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R6Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R6Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R6Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 6

                                    oRenPrecio.ColumnaD.R7Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R7Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R7Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R7Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R7Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R7Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R7Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R7Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R7Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R7Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R7Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R7Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R7Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R7Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 7

                                    oRenPrecio.ColumnaD.R8Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R8Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R8Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R8Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R8Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R8Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R8Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R8Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R8Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R8Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R8Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R8Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R8Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R8Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                                Case 8

                                    oRenPrecio.ColumnaD.R9Titulo1 = "Contado"
                                    oRenPrecio.ColumnaD.R9Titulo2 = "Credito"
                                    oRenPrecio.ColumnaD.R9Titulo3 = "" ''"PAGOS"
                                    oRenPrecio.ColumnaD.R9Titulo4 = "" ''"A"
                                    oRenPrecio.ColumnaD.R9Titulo5 = "Plazos"
                                    oRenPrecio.ColumnaD.R9Articulo = GridV_1.GetRowCellValue(iI, col1_Articulo)
                                    oRenPrecio.ColumnaD.R9Marca = "" & GridV_1.GetRowCellValue(iI, col1_Marca)
                                    oRenPrecio.ColumnaD.R9Estilo = "" & GridV_1.GetRowCellValue(iI, col1_Nombre)
                                    oRenPrecio.ColumnaD.R9Estructura = GridV_1.GetRowCellValue(iI, col1_NomDivision) & " " & GridV_1.GetRowCellValue(iI, col1_NomDepto)
                                    oRenPrecio.ColumnaD.R9Precio1 = Format(GridV_1.GetRowCellValue(iI, col1_Precio1), "C0")
                                    oRenPrecio.ColumnaD.R9Precio2 = Format(GridV_1.GetRowCellValue(iI, col1_Precio2), "C0")
                                    oRenPrecio.ColumnaD.R9Precio3 = Format(GridV_1.GetRowCellValue(iI, col1_Precio3), "C0")
                                    oRenPrecio.ColumnaD.R9Precio4 = Format(GridV_1.GetRowCellValue(iI, col1_Precio4), "C0")
                                    oRenPrecio.ColumnaD.R9Precio5 = Format(GridV_1.GetRowCellValue(iI, col1_Precio5), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta1 = Format(GridV_1.GetRowCellValue(iI, col1_Oferta1), "N0")
                                    ''oRenPrecio.ColumnaA.Oferta2 = "" '' Format(GridV_1.GetRowCellValue(iI, col1_Oferta2), "N0")
                                    iRenglon += 1
                            End Select
                    End Select
                    If iRenglon > TotRenglonMax Then
                        iRenglon = 0
                        oPreciadores.Renlones.Add(oRenPrecio)
                        oRenPrecio = New entPreciadorRen4
                        sw_solo1vuelta = False
                    End If
                    'Ultimo = oRenPrecio
                    iColumna += 1
                    If iColumna > TotColumnaMax Then
                        iColumna = 0
                        '
                        'Else
                        '    If iI = GridV_1.RowCount - 1 Then
                        '        If iColumna < 2 Then
                        '            iColumna = 0
                        '            oPreciadores.Renlones.Add(Ultimo)
                        '        End If
                        '    End If
                    End If
                End If
            Next
            If iColumna + iRenglon > 0 Then
                oPreciadores.Renlones.Add(oRenPrecio)
            End If
            Debug.Print(oPreciadores.Renlones.Count().ToString)
            If oPreciadores.Renlones.Count() > 0 Then
                oReporte = New XtraRep_PreciadorNA4X9
                oReporte.ods.DataSource = oPreciadores

                printTool = New DevExpress.XtraReports.UI.ReportPrintTool(oReporte)

                printTool.ShowPreviewDialog()
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class