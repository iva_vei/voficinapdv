﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class XtraRep_PreciadorNA4X9
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.DetailReport = New DevExpress.XtraReports.UI.DetailReportBand()
        Me.Detail1 = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel193 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel194 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel195 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel196 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel197 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel198 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel199 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel200 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel201 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel202 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel203 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel204 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel205 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel206 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel207 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel208 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel209 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel210 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel211 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel212 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel213 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel214 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel215 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel216 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel169 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel170 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel171 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel172 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel173 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel174 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel175 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel176 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel177 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel178 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel179 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel180 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel181 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel182 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel183 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel184 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel185 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel186 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel187 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel188 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel189 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel190 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel191 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel192 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel145 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel146 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel147 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel148 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel149 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel150 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel151 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel152 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel153 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel154 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel155 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel156 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel157 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel158 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel159 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel160 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel161 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel162 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel163 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel164 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel165 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel166 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel167 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel168 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel121 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel122 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel123 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel124 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel125 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel126 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel127 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel128 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel129 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel130 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel131 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel132 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel133 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel134 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel135 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel136 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel137 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel138 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel139 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel140 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel141 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel142 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel143 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel144 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel97 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel98 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel99 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel100 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel101 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel102 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel103 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel104 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel105 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel106 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel107 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel108 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel109 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel110 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel111 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel112 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel113 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel114 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel115 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel116 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel117 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel118 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel119 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel120 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel73 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel74 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel75 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel76 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel77 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel78 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel79 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel80 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel81 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel82 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel83 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel84 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel85 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel86 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel87 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel88 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel89 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel90 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel91 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel92 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel93 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel94 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel95 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel96 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel49 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel50 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel51 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel52 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel53 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel54 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel55 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel56 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel57 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel58 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel59 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel60 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel61 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel62 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel63 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel64 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel65 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel66 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel67 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel68 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel69 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel70 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel71 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel72 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel42 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel43 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel44 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel45 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel46 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel47 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel48 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Pic1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox6 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox7 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox10 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox11 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox8 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox9 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox13 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox14 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox12 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox15 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox19 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox18 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox16 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox17 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox23 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox22 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox21 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox20 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox27 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox26 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox25 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox24 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox31 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox30 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox29 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox28 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox35 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox34 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox32 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox33 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.ods = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource(Me.components)
        CType(Me.ods, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 12.5!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 12.99998!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DetailReport
        '
        Me.DetailReport.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail1})
        Me.DetailReport.DataMember = "Renlones"
        Me.DetailReport.DataSource = Me.ods
        Me.DetailReport.Level = 0
        Me.DetailReport.Name = "DetailReport"
        '
        'Detail1
        '
        Me.Detail1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel193, Me.XrLabel194, Me.XrLabel195, Me.XrLabel196, Me.XrLabel197, Me.XrLabel198, Me.XrLabel199, Me.XrLabel200, Me.XrLabel201, Me.XrLabel202, Me.XrLabel203, Me.XrLabel204, Me.XrLabel205, Me.XrLabel206, Me.XrLabel207, Me.XrLabel208, Me.XrLabel209, Me.XrLabel210, Me.XrLabel211, Me.XrLabel212, Me.XrLabel213, Me.XrLabel214, Me.XrLabel215, Me.XrLabel216, Me.XrLabel169, Me.XrLabel170, Me.XrLabel171, Me.XrLabel172, Me.XrLabel173, Me.XrLabel174, Me.XrLabel175, Me.XrLabel176, Me.XrLabel177, Me.XrLabel178, Me.XrLabel179, Me.XrLabel180, Me.XrLabel181, Me.XrLabel182, Me.XrLabel183, Me.XrLabel184, Me.XrLabel185, Me.XrLabel186, Me.XrLabel187, Me.XrLabel188, Me.XrLabel189, Me.XrLabel190, Me.XrLabel191, Me.XrLabel192, Me.XrLabel145, Me.XrLabel146, Me.XrLabel147, Me.XrLabel148, Me.XrLabel149, Me.XrLabel150, Me.XrLabel151, Me.XrLabel152, Me.XrLabel153, Me.XrLabel154, Me.XrLabel155, Me.XrLabel156, Me.XrLabel157, Me.XrLabel158, Me.XrLabel159, Me.XrLabel160, Me.XrLabel161, Me.XrLabel162, Me.XrLabel163, Me.XrLabel164, Me.XrLabel165, Me.XrLabel166, Me.XrLabel167, Me.XrLabel168, Me.XrLabel121, Me.XrLabel122, Me.XrLabel123, Me.XrLabel124, Me.XrLabel125, Me.XrLabel126, Me.XrLabel127, Me.XrLabel128, Me.XrLabel129, Me.XrLabel130, Me.XrLabel131, Me.XrLabel132, Me.XrLabel133, Me.XrLabel134, Me.XrLabel135, Me.XrLabel136, Me.XrLabel137, Me.XrLabel138, Me.XrLabel139, Me.XrLabel140, Me.XrLabel141, Me.XrLabel142, Me.XrLabel143, Me.XrLabel144, Me.XrLabel97, Me.XrLabel98, Me.XrLabel99, Me.XrLabel100, Me.XrLabel101, Me.XrLabel102, Me.XrLabel103, Me.XrLabel104, Me.XrLabel105, Me.XrLabel106, Me.XrLabel107, Me.XrLabel108, Me.XrLabel109, Me.XrLabel110, Me.XrLabel111, Me.XrLabel112, Me.XrLabel113, Me.XrLabel114, Me.XrLabel115, Me.XrLabel116, Me.XrLabel117, Me.XrLabel118, Me.XrLabel119, Me.XrLabel120, Me.XrLabel73, Me.XrLabel74, Me.XrLabel75, Me.XrLabel76, Me.XrLabel77, Me.XrLabel78, Me.XrLabel79, Me.XrLabel80, Me.XrLabel81, Me.XrLabel82, Me.XrLabel83, Me.XrLabel84, Me.XrLabel85, Me.XrLabel86, Me.XrLabel87, Me.XrLabel88, Me.XrLabel89, Me.XrLabel90, Me.XrLabel91, Me.XrLabel92, Me.XrLabel93, Me.XrLabel94, Me.XrLabel95, Me.XrLabel96, Me.XrLabel49, Me.XrLabel50, Me.XrLabel51, Me.XrLabel52, Me.XrLabel53, Me.XrLabel54, Me.XrLabel55, Me.XrLabel56, Me.XrLabel57, Me.XrLabel58, Me.XrLabel59, Me.XrLabel60, Me.XrLabel61, Me.XrLabel62, Me.XrLabel63, Me.XrLabel64, Me.XrLabel65, Me.XrLabel66, Me.XrLabel67, Me.XrLabel68, Me.XrLabel69, Me.XrLabel70, Me.XrLabel71, Me.XrLabel72, Me.XrLabel25, Me.XrLabel26, Me.XrLabel27, Me.XrLabel28, Me.XrLabel29, Me.XrLabel30, Me.XrLabel31, Me.XrLabel32, Me.XrLabel33, Me.XrLabel34, Me.XrLabel35, Me.XrLabel36, Me.XrLabel37, Me.XrLabel38, Me.XrLabel39, Me.XrLabel40, Me.XrLabel41, Me.XrLabel42, Me.XrLabel43, Me.XrLabel44, Me.XrLabel45, Me.XrLabel46, Me.XrLabel47, Me.XrLabel48, Me.XrLabel19, Me.XrLabel20, Me.XrLabel21, Me.XrLabel22, Me.XrLabel23, Me.XrLabel24, Me.XrLabel12, Me.XrLabel13, Me.XrLabel14, Me.XrLabel15, Me.XrLabel17, Me.XrLabel18, Me.XrLabel6, Me.XrLabel7, Me.XrLabel8, Me.XrLabel9, Me.XrLabel10, Me.XrLabel11, Me.XrLabel16, Me.XrLabel2, Me.XrLabel3, Me.XrLabel5, Me.XrLabel1, Me.XrLabel4, Me.Pic1, Me.XrPictureBox1, Me.XrPictureBox3, Me.XrPictureBox2, Me.XrPictureBox5, Me.XrPictureBox6, Me.XrPictureBox4, Me.XrPictureBox7, Me.XrPictureBox10, Me.XrPictureBox11, Me.XrPictureBox8, Me.XrPictureBox9, Me.XrPictureBox13, Me.XrPictureBox14, Me.XrPictureBox12, Me.XrPictureBox15, Me.XrPictureBox19, Me.XrPictureBox18, Me.XrPictureBox16, Me.XrPictureBox17, Me.XrPictureBox23, Me.XrPictureBox22, Me.XrPictureBox21, Me.XrPictureBox20, Me.XrPictureBox27, Me.XrPictureBox26, Me.XrPictureBox25, Me.XrPictureBox24, Me.XrPictureBox31, Me.XrPictureBox30, Me.XrPictureBox29, Me.XrPictureBox28, Me.XrPictureBox35, Me.XrPictureBox34, Me.XrPictureBox32, Me.XrPictureBox33})
        Me.Detail1.HeightF = 1148.437!
        Me.Detail1.Name = "Detail1"
        '
        'XrLabel193
        '
        Me.XrLabel193.CanGrow = False
        Me.XrLabel193.CanShrink = True
        Me.XrLabel193.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R9Precio1]")})
        Me.XrLabel193.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel193.LocationFloat = New DevExpress.Utils.PointFloat(7.181614!, 1092.507!)
        Me.XrLabel193.Multiline = True
        Me.XrLabel193.Name = "XrLabel193"
        Me.XrLabel193.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel193.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel193.StylePriority.UseFont = False
        Me.XrLabel193.StylePriority.UseTextAlignment = False
        Me.XrLabel193.Text = "$99,999.00"
        Me.XrLabel193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel193.TextFormatString = "{0:$0}"
        '
        'XrLabel194
        '
        Me.XrLabel194.CanGrow = False
        Me.XrLabel194.CanShrink = True
        Me.XrLabel194.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R9Precio5]")})
        Me.XrLabel194.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel194.LocationFloat = New DevExpress.Utils.PointFloat(33.55618!, 1059.231!)
        Me.XrLabel194.Multiline = True
        Me.XrLabel194.Name = "XrLabel194"
        Me.XrLabel194.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel194.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel194.StylePriority.UseFont = False
        Me.XrLabel194.StylePriority.UseTextAlignment = False
        Me.XrLabel194.Text = "$99,999.00"
        Me.XrLabel194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel194.TextFormatString = "{0:N0}"
        '
        'XrLabel195
        '
        Me.XrLabel195.CanGrow = False
        Me.XrLabel195.CanShrink = True
        Me.XrLabel195.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R9Precio2]")})
        Me.XrLabel195.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel195.LocationFloat = New DevExpress.Utils.PointFloat(7.181614!, 1102.885!)
        Me.XrLabel195.Multiline = True
        Me.XrLabel195.Name = "XrLabel195"
        Me.XrLabel195.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel195.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel195.StylePriority.UseFont = False
        Me.XrLabel195.StylePriority.UseTextAlignment = False
        Me.XrLabel195.Text = "$99,999.00"
        Me.XrLabel195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel195.TextFormatString = "{0:$0}"
        '
        'XrLabel196
        '
        Me.XrLabel196.CanGrow = False
        Me.XrLabel196.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R9Marca]")})
        Me.XrLabel196.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel196.LocationFloat = New DevExpress.Utils.PointFloat(68.6402!, 1134.437!)
        Me.XrLabel196.Name = "XrLabel196"
        Me.XrLabel196.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel196.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel196.StylePriority.UseFont = False
        Me.XrLabel196.Text = "Marca:"
        '
        'XrLabel197
        '
        Me.XrLabel197.CanGrow = False
        Me.XrLabel197.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R9Estilo]")})
        Me.XrLabel197.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel197.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 1134.437!)
        Me.XrLabel197.Name = "XrLabel197"
        Me.XrLabel197.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel197.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel197.StylePriority.UseFont = False
        Me.XrLabel197.Text = "Estilo:"
        '
        'XrLabel198
        '
        Me.XrLabel198.CanGrow = False
        Me.XrLabel198.CanShrink = True
        Me.XrLabel198.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R9Precio3]")})
        Me.XrLabel198.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel198.LocationFloat = New DevExpress.Utils.PointFloat(99.5725!, 1059.231!)
        Me.XrLabel198.Multiline = True
        Me.XrLabel198.Name = "XrLabel198"
        Me.XrLabel198.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel198.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel198.StylePriority.UseFont = False
        Me.XrLabel198.StylePriority.UseTextAlignment = False
        Me.XrLabel198.Text = "$99,999.00"
        Me.XrLabel198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel198.TextFormatString = "{0:N0}"
        '
        'XrLabel199
        '
        Me.XrLabel199.CanGrow = False
        Me.XrLabel199.CanShrink = True
        Me.XrLabel199.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R9Precio3]")})
        Me.XrLabel199.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel199.LocationFloat = New DevExpress.Utils.PointFloat(296.088!, 1059.231!)
        Me.XrLabel199.Multiline = True
        Me.XrLabel199.Name = "XrLabel199"
        Me.XrLabel199.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel199.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel199.StylePriority.UseFont = False
        Me.XrLabel199.StylePriority.UseTextAlignment = False
        Me.XrLabel199.Text = "$99,999.00"
        Me.XrLabel199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel199.TextFormatString = "{0:N0}"
        '
        'XrLabel200
        '
        Me.XrLabel200.CanGrow = False
        Me.XrLabel200.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R9Estilo]")})
        Me.XrLabel200.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel200.LocationFloat = New DevExpress.Utils.PointFloat(206.5156!, 1134.437!)
        Me.XrLabel200.Name = "XrLabel200"
        Me.XrLabel200.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel200.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel200.StylePriority.UseFont = False
        Me.XrLabel200.Text = "Estilo:"
        '
        'XrLabel201
        '
        Me.XrLabel201.CanGrow = False
        Me.XrLabel201.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R9Marca]")})
        Me.XrLabel201.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel201.LocationFloat = New DevExpress.Utils.PointFloat(265.1558!, 1134.437!)
        Me.XrLabel201.Name = "XrLabel201"
        Me.XrLabel201.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel201.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel201.StylePriority.UseFont = False
        Me.XrLabel201.Text = "Marca:"
        '
        'XrLabel202
        '
        Me.XrLabel202.CanGrow = False
        Me.XrLabel202.CanShrink = True
        Me.XrLabel202.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R9Precio2]")})
        Me.XrLabel202.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel202.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 1102.885!)
        Me.XrLabel202.Multiline = True
        Me.XrLabel202.Name = "XrLabel202"
        Me.XrLabel202.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel202.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel202.StylePriority.UseFont = False
        Me.XrLabel202.StylePriority.UseTextAlignment = False
        Me.XrLabel202.Text = "$99,999.00"
        Me.XrLabel202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel202.TextFormatString = "{0:$0}"
        '
        'XrLabel203
        '
        Me.XrLabel203.CanGrow = False
        Me.XrLabel203.CanShrink = True
        Me.XrLabel203.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R9Precio5]")})
        Me.XrLabel203.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel203.LocationFloat = New DevExpress.Utils.PointFloat(230.0718!, 1059.231!)
        Me.XrLabel203.Multiline = True
        Me.XrLabel203.Name = "XrLabel203"
        Me.XrLabel203.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel203.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel203.StylePriority.UseFont = False
        Me.XrLabel203.StylePriority.UseTextAlignment = False
        Me.XrLabel203.Text = "$99,999.00"
        Me.XrLabel203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel203.TextFormatString = "{0:N0}"
        '
        'XrLabel204
        '
        Me.XrLabel204.CanGrow = False
        Me.XrLabel204.CanShrink = True
        Me.XrLabel204.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R9Precio1]")})
        Me.XrLabel204.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel204.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 1092.507!)
        Me.XrLabel204.Multiline = True
        Me.XrLabel204.Name = "XrLabel204"
        Me.XrLabel204.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel204.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel204.StylePriority.UseFont = False
        Me.XrLabel204.StylePriority.UseTextAlignment = False
        Me.XrLabel204.Text = "$99,999.00"
        Me.XrLabel204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel204.TextFormatString = "{0:$0}"
        '
        'XrLabel205
        '
        Me.XrLabel205.CanGrow = False
        Me.XrLabel205.CanShrink = True
        Me.XrLabel205.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R9Precio1]")})
        Me.XrLabel205.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel205.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 1092.507!)
        Me.XrLabel205.Multiline = True
        Me.XrLabel205.Name = "XrLabel205"
        Me.XrLabel205.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel205.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel205.StylePriority.UseFont = False
        Me.XrLabel205.StylePriority.UseTextAlignment = False
        Me.XrLabel205.Text = "$99,999.00"
        Me.XrLabel205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel205.TextFormatString = "{0:$0}"
        '
        'XrLabel206
        '
        Me.XrLabel206.CanGrow = False
        Me.XrLabel206.CanShrink = True
        Me.XrLabel206.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R9Precio5]")})
        Me.XrLabel206.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel206.LocationFloat = New DevExpress.Utils.PointFloat(426.5872!, 1059.231!)
        Me.XrLabel206.Multiline = True
        Me.XrLabel206.Name = "XrLabel206"
        Me.XrLabel206.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel206.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel206.StylePriority.UseFont = False
        Me.XrLabel206.StylePriority.UseTextAlignment = False
        Me.XrLabel206.Text = "$99,999.00"
        Me.XrLabel206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel206.TextFormatString = "{0:N0}"
        '
        'XrLabel207
        '
        Me.XrLabel207.CanGrow = False
        Me.XrLabel207.CanShrink = True
        Me.XrLabel207.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R9Precio2]")})
        Me.XrLabel207.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel207.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 1102.885!)
        Me.XrLabel207.Multiline = True
        Me.XrLabel207.Name = "XrLabel207"
        Me.XrLabel207.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel207.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel207.StylePriority.UseFont = False
        Me.XrLabel207.StylePriority.UseTextAlignment = False
        Me.XrLabel207.Text = "$99,999.00"
        Me.XrLabel207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel207.TextFormatString = "{0:$0}"
        '
        'XrLabel208
        '
        Me.XrLabel208.CanGrow = False
        Me.XrLabel208.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R9Marca]")})
        Me.XrLabel208.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel208.LocationFloat = New DevExpress.Utils.PointFloat(461.6713!, 1134.437!)
        Me.XrLabel208.Name = "XrLabel208"
        Me.XrLabel208.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel208.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel208.StylePriority.UseFont = False
        Me.XrLabel208.Text = "Marca:"
        '
        'XrLabel209
        '
        Me.XrLabel209.CanGrow = False
        Me.XrLabel209.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R9Estilo]")})
        Me.XrLabel209.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel209.LocationFloat = New DevExpress.Utils.PointFloat(403.0311!, 1134.437!)
        Me.XrLabel209.Name = "XrLabel209"
        Me.XrLabel209.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel209.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel209.StylePriority.UseFont = False
        Me.XrLabel209.Text = "Estilo:"
        '
        'XrLabel210
        '
        Me.XrLabel210.CanGrow = False
        Me.XrLabel210.CanShrink = True
        Me.XrLabel210.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R9Precio3]")})
        Me.XrLabel210.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel210.LocationFloat = New DevExpress.Utils.PointFloat(492.6035!, 1059.231!)
        Me.XrLabel210.Multiline = True
        Me.XrLabel210.Name = "XrLabel210"
        Me.XrLabel210.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel210.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel210.StylePriority.UseFont = False
        Me.XrLabel210.StylePriority.UseTextAlignment = False
        Me.XrLabel210.Text = "$99,999.00"
        Me.XrLabel210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel210.TextFormatString = "{0:N0}"
        '
        'XrLabel211
        '
        Me.XrLabel211.CanGrow = False
        Me.XrLabel211.CanShrink = True
        Me.XrLabel211.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R9Precio1]")})
        Me.XrLabel211.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel211.LocationFloat = New DevExpress.Utils.PointFloat(596.7282!, 1092.507!)
        Me.XrLabel211.Multiline = True
        Me.XrLabel211.Name = "XrLabel211"
        Me.XrLabel211.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel211.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel211.StylePriority.UseFont = False
        Me.XrLabel211.StylePriority.UseTextAlignment = False
        Me.XrLabel211.Text = "$99,999.00"
        Me.XrLabel211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel211.TextFormatString = "{0:$0}"
        '
        'XrLabel212
        '
        Me.XrLabel212.CanGrow = False
        Me.XrLabel212.CanShrink = True
        Me.XrLabel212.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R9Precio5]")})
        Me.XrLabel212.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel212.LocationFloat = New DevExpress.Utils.PointFloat(623.1028!, 1059.231!)
        Me.XrLabel212.Multiline = True
        Me.XrLabel212.Name = "XrLabel212"
        Me.XrLabel212.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel212.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel212.StylePriority.UseFont = False
        Me.XrLabel212.StylePriority.UseTextAlignment = False
        Me.XrLabel212.Text = "$99,999.00"
        Me.XrLabel212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel212.TextFormatString = "{0:N0}"
        '
        'XrLabel213
        '
        Me.XrLabel213.CanGrow = False
        Me.XrLabel213.CanShrink = True
        Me.XrLabel213.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R9Precio2]")})
        Me.XrLabel213.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel213.LocationFloat = New DevExpress.Utils.PointFloat(596.7282!, 1102.885!)
        Me.XrLabel213.Multiline = True
        Me.XrLabel213.Name = "XrLabel213"
        Me.XrLabel213.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel213.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel213.StylePriority.UseFont = False
        Me.XrLabel213.StylePriority.UseTextAlignment = False
        Me.XrLabel213.Text = "$99,999.00"
        Me.XrLabel213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel213.TextFormatString = "{0:$0}"
        '
        'XrLabel214
        '
        Me.XrLabel214.CanGrow = False
        Me.XrLabel214.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R9Marca]")})
        Me.XrLabel214.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel214.LocationFloat = New DevExpress.Utils.PointFloat(658.1866!, 1134.437!)
        Me.XrLabel214.Name = "XrLabel214"
        Me.XrLabel214.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel214.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel214.StylePriority.UseFont = False
        Me.XrLabel214.Text = "Marca:"
        '
        'XrLabel215
        '
        Me.XrLabel215.CanGrow = False
        Me.XrLabel215.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R9Estilo]")})
        Me.XrLabel215.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel215.LocationFloat = New DevExpress.Utils.PointFloat(599.5465!, 1134.437!)
        Me.XrLabel215.Name = "XrLabel215"
        Me.XrLabel215.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel215.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel215.StylePriority.UseFont = False
        Me.XrLabel215.Text = "Estilo:"
        '
        'XrLabel216
        '
        Me.XrLabel216.CanGrow = False
        Me.XrLabel216.CanShrink = True
        Me.XrLabel216.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R9Precio3]")})
        Me.XrLabel216.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel216.LocationFloat = New DevExpress.Utils.PointFloat(689.1188!, 1059.231!)
        Me.XrLabel216.Multiline = True
        Me.XrLabel216.Name = "XrLabel216"
        Me.XrLabel216.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel216.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel216.StylePriority.UseFont = False
        Me.XrLabel216.StylePriority.UseTextAlignment = False
        Me.XrLabel216.Text = "$99,999.00"
        Me.XrLabel216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel216.TextFormatString = "{0:N0}"
        '
        'XrLabel169
        '
        Me.XrLabel169.CanGrow = False
        Me.XrLabel169.CanShrink = True
        Me.XrLabel169.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R8Precio1]")})
        Me.XrLabel169.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel169.LocationFloat = New DevExpress.Utils.PointFloat(7.181263!, 964.9028!)
        Me.XrLabel169.Multiline = True
        Me.XrLabel169.Name = "XrLabel169"
        Me.XrLabel169.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel169.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel169.StylePriority.UseFont = False
        Me.XrLabel169.StylePriority.UseTextAlignment = False
        Me.XrLabel169.Text = "$99,999.00"
        Me.XrLabel169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel169.TextFormatString = "{0:$0}"
        '
        'XrLabel170
        '
        Me.XrLabel170.CanGrow = False
        Me.XrLabel170.CanShrink = True
        Me.XrLabel170.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R8Precio5]")})
        Me.XrLabel170.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel170.LocationFloat = New DevExpress.Utils.PointFloat(33.55579!, 931.6268!)
        Me.XrLabel170.Multiline = True
        Me.XrLabel170.Name = "XrLabel170"
        Me.XrLabel170.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel170.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel170.StylePriority.UseFont = False
        Me.XrLabel170.StylePriority.UseTextAlignment = False
        Me.XrLabel170.Text = "$99,999.00"
        Me.XrLabel170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel170.TextFormatString = "{0:N0}"
        '
        'XrLabel171
        '
        Me.XrLabel171.CanGrow = False
        Me.XrLabel171.CanShrink = True
        Me.XrLabel171.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R8Precio2]")})
        Me.XrLabel171.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel171.LocationFloat = New DevExpress.Utils.PointFloat(7.181263!, 975.2808!)
        Me.XrLabel171.Multiline = True
        Me.XrLabel171.Name = "XrLabel171"
        Me.XrLabel171.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel171.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel171.StylePriority.UseFont = False
        Me.XrLabel171.StylePriority.UseTextAlignment = False
        Me.XrLabel171.Text = "$99,999.00"
        Me.XrLabel171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel171.TextFormatString = "{0:$0}"
        '
        'XrLabel172
        '
        Me.XrLabel172.CanGrow = False
        Me.XrLabel172.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R8Marca]")})
        Me.XrLabel172.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel172.LocationFloat = New DevExpress.Utils.PointFloat(68.63985!, 1006.833!)
        Me.XrLabel172.Name = "XrLabel172"
        Me.XrLabel172.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel172.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel172.StylePriority.UseFont = False
        Me.XrLabel172.Text = "Marca:"
        '
        'XrLabel173
        '
        Me.XrLabel173.CanGrow = False
        Me.XrLabel173.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R8Estilo]")})
        Me.XrLabel173.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel173.LocationFloat = New DevExpress.Utils.PointFloat(9.999625!, 1006.833!)
        Me.XrLabel173.Name = "XrLabel173"
        Me.XrLabel173.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel173.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel173.StylePriority.UseFont = False
        Me.XrLabel173.Text = "Estilo:"
        '
        'XrLabel174
        '
        Me.XrLabel174.CanGrow = False
        Me.XrLabel174.CanShrink = True
        Me.XrLabel174.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R8Precio3]")})
        Me.XrLabel174.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel174.LocationFloat = New DevExpress.Utils.PointFloat(99.57215!, 931.6268!)
        Me.XrLabel174.Multiline = True
        Me.XrLabel174.Name = "XrLabel174"
        Me.XrLabel174.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel174.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel174.StylePriority.UseFont = False
        Me.XrLabel174.StylePriority.UseTextAlignment = False
        Me.XrLabel174.Text = "$99,999.00"
        Me.XrLabel174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel174.TextFormatString = "{0:N0}"
        '
        'XrLabel175
        '
        Me.XrLabel175.CanGrow = False
        Me.XrLabel175.CanShrink = True
        Me.XrLabel175.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R8Precio3]")})
        Me.XrLabel175.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel175.LocationFloat = New DevExpress.Utils.PointFloat(296.0877!, 931.6268!)
        Me.XrLabel175.Multiline = True
        Me.XrLabel175.Name = "XrLabel175"
        Me.XrLabel175.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel175.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel175.StylePriority.UseFont = False
        Me.XrLabel175.StylePriority.UseTextAlignment = False
        Me.XrLabel175.Text = "$99,999.00"
        Me.XrLabel175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel175.TextFormatString = "{0:N0}"
        '
        'XrLabel176
        '
        Me.XrLabel176.CanGrow = False
        Me.XrLabel176.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R8Estilo]")})
        Me.XrLabel176.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel176.LocationFloat = New DevExpress.Utils.PointFloat(206.5152!, 1006.833!)
        Me.XrLabel176.Name = "XrLabel176"
        Me.XrLabel176.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel176.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel176.StylePriority.UseFont = False
        Me.XrLabel176.Text = "Estilo:"
        '
        'XrLabel177
        '
        Me.XrLabel177.CanGrow = False
        Me.XrLabel177.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R8Marca]")})
        Me.XrLabel177.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel177.LocationFloat = New DevExpress.Utils.PointFloat(265.1555!, 1006.833!)
        Me.XrLabel177.Name = "XrLabel177"
        Me.XrLabel177.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel177.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel177.StylePriority.UseFont = False
        Me.XrLabel177.Text = "Marca:"
        '
        'XrLabel178
        '
        Me.XrLabel178.CanGrow = False
        Me.XrLabel178.CanShrink = True
        Me.XrLabel178.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R8Precio2]")})
        Me.XrLabel178.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel178.LocationFloat = New DevExpress.Utils.PointFloat(203.6969!, 975.2808!)
        Me.XrLabel178.Multiline = True
        Me.XrLabel178.Name = "XrLabel178"
        Me.XrLabel178.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel178.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel178.StylePriority.UseFont = False
        Me.XrLabel178.StylePriority.UseTextAlignment = False
        Me.XrLabel178.Text = "$99,999.00"
        Me.XrLabel178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel178.TextFormatString = "{0:$0}"
        '
        'XrLabel179
        '
        Me.XrLabel179.CanGrow = False
        Me.XrLabel179.CanShrink = True
        Me.XrLabel179.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R8Precio5]")})
        Me.XrLabel179.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel179.LocationFloat = New DevExpress.Utils.PointFloat(230.0714!, 931.6268!)
        Me.XrLabel179.Multiline = True
        Me.XrLabel179.Name = "XrLabel179"
        Me.XrLabel179.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel179.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel179.StylePriority.UseFont = False
        Me.XrLabel179.StylePriority.UseTextAlignment = False
        Me.XrLabel179.Text = "$99,999.00"
        Me.XrLabel179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel179.TextFormatString = "{0:N0}"
        '
        'XrLabel180
        '
        Me.XrLabel180.CanGrow = False
        Me.XrLabel180.CanShrink = True
        Me.XrLabel180.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R8Precio1]")})
        Me.XrLabel180.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel180.LocationFloat = New DevExpress.Utils.PointFloat(203.6969!, 964.9028!)
        Me.XrLabel180.Multiline = True
        Me.XrLabel180.Name = "XrLabel180"
        Me.XrLabel180.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel180.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel180.StylePriority.UseFont = False
        Me.XrLabel180.StylePriority.UseTextAlignment = False
        Me.XrLabel180.Text = "$99,999.00"
        Me.XrLabel180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel180.TextFormatString = "{0:$0}"
        '
        'XrLabel181
        '
        Me.XrLabel181.CanGrow = False
        Me.XrLabel181.CanShrink = True
        Me.XrLabel181.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R8Precio1]")})
        Me.XrLabel181.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel181.LocationFloat = New DevExpress.Utils.PointFloat(400.2124!, 964.9028!)
        Me.XrLabel181.Multiline = True
        Me.XrLabel181.Name = "XrLabel181"
        Me.XrLabel181.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel181.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel181.StylePriority.UseFont = False
        Me.XrLabel181.StylePriority.UseTextAlignment = False
        Me.XrLabel181.Text = "$99,999.00"
        Me.XrLabel181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel181.TextFormatString = "{0:$0}"
        '
        'XrLabel182
        '
        Me.XrLabel182.CanGrow = False
        Me.XrLabel182.CanShrink = True
        Me.XrLabel182.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R8Precio5]")})
        Me.XrLabel182.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel182.LocationFloat = New DevExpress.Utils.PointFloat(426.5868!, 931.6268!)
        Me.XrLabel182.Multiline = True
        Me.XrLabel182.Name = "XrLabel182"
        Me.XrLabel182.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel182.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel182.StylePriority.UseFont = False
        Me.XrLabel182.StylePriority.UseTextAlignment = False
        Me.XrLabel182.Text = "$99,999.00"
        Me.XrLabel182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel182.TextFormatString = "{0:N0}"
        '
        'XrLabel183
        '
        Me.XrLabel183.CanGrow = False
        Me.XrLabel183.CanShrink = True
        Me.XrLabel183.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R8Precio2]")})
        Me.XrLabel183.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel183.LocationFloat = New DevExpress.Utils.PointFloat(400.2124!, 975.2808!)
        Me.XrLabel183.Multiline = True
        Me.XrLabel183.Name = "XrLabel183"
        Me.XrLabel183.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel183.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel183.StylePriority.UseFont = False
        Me.XrLabel183.StylePriority.UseTextAlignment = False
        Me.XrLabel183.Text = "$99,999.00"
        Me.XrLabel183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel183.TextFormatString = "{0:$0}"
        '
        'XrLabel184
        '
        Me.XrLabel184.CanGrow = False
        Me.XrLabel184.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R8Marca]")})
        Me.XrLabel184.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel184.LocationFloat = New DevExpress.Utils.PointFloat(461.671!, 1006.833!)
        Me.XrLabel184.Name = "XrLabel184"
        Me.XrLabel184.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel184.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel184.StylePriority.UseFont = False
        Me.XrLabel184.Text = "Marca:"
        '
        'XrLabel185
        '
        Me.XrLabel185.CanGrow = False
        Me.XrLabel185.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R8Estilo]")})
        Me.XrLabel185.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel185.LocationFloat = New DevExpress.Utils.PointFloat(403.0307!, 1006.833!)
        Me.XrLabel185.Name = "XrLabel185"
        Me.XrLabel185.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel185.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel185.StylePriority.UseFont = False
        Me.XrLabel185.Text = "Estilo:"
        '
        'XrLabel186
        '
        Me.XrLabel186.CanGrow = False
        Me.XrLabel186.CanShrink = True
        Me.XrLabel186.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R8Precio3]")})
        Me.XrLabel186.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel186.LocationFloat = New DevExpress.Utils.PointFloat(492.6031!, 931.6268!)
        Me.XrLabel186.Multiline = True
        Me.XrLabel186.Name = "XrLabel186"
        Me.XrLabel186.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel186.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel186.StylePriority.UseFont = False
        Me.XrLabel186.StylePriority.UseTextAlignment = False
        Me.XrLabel186.Text = "$99,999.00"
        Me.XrLabel186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel186.TextFormatString = "{0:N0}"
        '
        'XrLabel187
        '
        Me.XrLabel187.CanGrow = False
        Me.XrLabel187.CanShrink = True
        Me.XrLabel187.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R8Precio1]")})
        Me.XrLabel187.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel187.LocationFloat = New DevExpress.Utils.PointFloat(596.7279!, 964.9028!)
        Me.XrLabel187.Multiline = True
        Me.XrLabel187.Name = "XrLabel187"
        Me.XrLabel187.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel187.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel187.StylePriority.UseFont = False
        Me.XrLabel187.StylePriority.UseTextAlignment = False
        Me.XrLabel187.Text = "$99,999.00"
        Me.XrLabel187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel187.TextFormatString = "{0:$0}"
        '
        'XrLabel188
        '
        Me.XrLabel188.CanGrow = False
        Me.XrLabel188.CanShrink = True
        Me.XrLabel188.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R8Precio5]")})
        Me.XrLabel188.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel188.LocationFloat = New DevExpress.Utils.PointFloat(623.1024!, 931.6268!)
        Me.XrLabel188.Multiline = True
        Me.XrLabel188.Name = "XrLabel188"
        Me.XrLabel188.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel188.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel188.StylePriority.UseFont = False
        Me.XrLabel188.StylePriority.UseTextAlignment = False
        Me.XrLabel188.Text = "$99,999.00"
        Me.XrLabel188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel188.TextFormatString = "{0:N0}"
        '
        'XrLabel189
        '
        Me.XrLabel189.CanGrow = False
        Me.XrLabel189.CanShrink = True
        Me.XrLabel189.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R8Precio2]")})
        Me.XrLabel189.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel189.LocationFloat = New DevExpress.Utils.PointFloat(596.7279!, 975.2808!)
        Me.XrLabel189.Multiline = True
        Me.XrLabel189.Name = "XrLabel189"
        Me.XrLabel189.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel189.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel189.StylePriority.UseFont = False
        Me.XrLabel189.StylePriority.UseTextAlignment = False
        Me.XrLabel189.Text = "$99,999.00"
        Me.XrLabel189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel189.TextFormatString = "{0:$0}"
        '
        'XrLabel190
        '
        Me.XrLabel190.CanGrow = False
        Me.XrLabel190.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R8Marca]")})
        Me.XrLabel190.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel190.LocationFloat = New DevExpress.Utils.PointFloat(658.1863!, 1006.833!)
        Me.XrLabel190.Name = "XrLabel190"
        Me.XrLabel190.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel190.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel190.StylePriority.UseFont = False
        Me.XrLabel190.Text = "Marca:"
        '
        'XrLabel191
        '
        Me.XrLabel191.CanGrow = False
        Me.XrLabel191.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R8Estilo]")})
        Me.XrLabel191.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel191.LocationFloat = New DevExpress.Utils.PointFloat(599.5462!, 1006.833!)
        Me.XrLabel191.Name = "XrLabel191"
        Me.XrLabel191.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel191.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel191.StylePriority.UseFont = False
        Me.XrLabel191.Text = "Estilo:"
        '
        'XrLabel192
        '
        Me.XrLabel192.CanGrow = False
        Me.XrLabel192.CanShrink = True
        Me.XrLabel192.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R8Precio3]")})
        Me.XrLabel192.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel192.LocationFloat = New DevExpress.Utils.PointFloat(689.1185!, 931.6268!)
        Me.XrLabel192.Multiline = True
        Me.XrLabel192.Name = "XrLabel192"
        Me.XrLabel192.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel192.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel192.StylePriority.UseFont = False
        Me.XrLabel192.StylePriority.UseTextAlignment = False
        Me.XrLabel192.Text = "$99,999.00"
        Me.XrLabel192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel192.TextFormatString = "{0:N0}"
        '
        'XrLabel145
        '
        Me.XrLabel145.CanGrow = False
        Me.XrLabel145.CanShrink = True
        Me.XrLabel145.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R7Precio1]")})
        Me.XrLabel145.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel145.LocationFloat = New DevExpress.Utils.PointFloat(7.181358!, 837.2989!)
        Me.XrLabel145.Multiline = True
        Me.XrLabel145.Name = "XrLabel145"
        Me.XrLabel145.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel145.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel145.StylePriority.UseFont = False
        Me.XrLabel145.StylePriority.UseTextAlignment = False
        Me.XrLabel145.Text = "$99,999.00"
        Me.XrLabel145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel145.TextFormatString = "{0:$0}"
        '
        'XrLabel146
        '
        Me.XrLabel146.CanGrow = False
        Me.XrLabel146.CanShrink = True
        Me.XrLabel146.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R7Precio5]")})
        Me.XrLabel146.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel146.LocationFloat = New DevExpress.Utils.PointFloat(33.55589!, 804.0229!)
        Me.XrLabel146.Multiline = True
        Me.XrLabel146.Name = "XrLabel146"
        Me.XrLabel146.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel146.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel146.StylePriority.UseFont = False
        Me.XrLabel146.StylePriority.UseTextAlignment = False
        Me.XrLabel146.Text = "$99,999.00"
        Me.XrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel146.TextFormatString = "{0:N0}"
        '
        'XrLabel147
        '
        Me.XrLabel147.CanGrow = False
        Me.XrLabel147.CanShrink = True
        Me.XrLabel147.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R7Precio2]")})
        Me.XrLabel147.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel147.LocationFloat = New DevExpress.Utils.PointFloat(7.181358!, 847.6769!)
        Me.XrLabel147.Multiline = True
        Me.XrLabel147.Name = "XrLabel147"
        Me.XrLabel147.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel147.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel147.StylePriority.UseFont = False
        Me.XrLabel147.StylePriority.UseTextAlignment = False
        Me.XrLabel147.Text = "$99,999.00"
        Me.XrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel147.TextFormatString = "{0:$0}"
        '
        'XrLabel148
        '
        Me.XrLabel148.CanGrow = False
        Me.XrLabel148.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R7Marca]")})
        Me.XrLabel148.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel148.LocationFloat = New DevExpress.Utils.PointFloat(68.63995!, 880.2286!)
        Me.XrLabel148.Name = "XrLabel148"
        Me.XrLabel148.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel148.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel148.StylePriority.UseFont = False
        Me.XrLabel148.Text = "Marca:"
        '
        'XrLabel149
        '
        Me.XrLabel149.CanGrow = False
        Me.XrLabel149.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R7Estilo]")})
        Me.XrLabel149.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel149.LocationFloat = New DevExpress.Utils.PointFloat(9.999721!, 880.2286!)
        Me.XrLabel149.Name = "XrLabel149"
        Me.XrLabel149.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel149.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel149.StylePriority.UseFont = False
        Me.XrLabel149.Text = "Estilo:"
        '
        'XrLabel150
        '
        Me.XrLabel150.CanGrow = False
        Me.XrLabel150.CanShrink = True
        Me.XrLabel150.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R7Precio3]")})
        Me.XrLabel150.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel150.LocationFloat = New DevExpress.Utils.PointFloat(99.57224!, 804.0229!)
        Me.XrLabel150.Multiline = True
        Me.XrLabel150.Name = "XrLabel150"
        Me.XrLabel150.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel150.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel150.StylePriority.UseFont = False
        Me.XrLabel150.StylePriority.UseTextAlignment = False
        Me.XrLabel150.Text = "$99,999.00"
        Me.XrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel150.TextFormatString = "{0:N0}"
        '
        'XrLabel151
        '
        Me.XrLabel151.CanGrow = False
        Me.XrLabel151.CanShrink = True
        Me.XrLabel151.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R7Precio3]")})
        Me.XrLabel151.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel151.LocationFloat = New DevExpress.Utils.PointFloat(296.0878!, 804.0229!)
        Me.XrLabel151.Multiline = True
        Me.XrLabel151.Name = "XrLabel151"
        Me.XrLabel151.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel151.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel151.StylePriority.UseFont = False
        Me.XrLabel151.StylePriority.UseTextAlignment = False
        Me.XrLabel151.Text = "$99,999.00"
        Me.XrLabel151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel151.TextFormatString = "{0:N0}"
        '
        'XrLabel152
        '
        Me.XrLabel152.CanGrow = False
        Me.XrLabel152.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R7Estilo]")})
        Me.XrLabel152.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel152.LocationFloat = New DevExpress.Utils.PointFloat(206.5153!, 880.2286!)
        Me.XrLabel152.Name = "XrLabel152"
        Me.XrLabel152.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel152.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel152.StylePriority.UseFont = False
        Me.XrLabel152.Text = "Estilo:"
        '
        'XrLabel153
        '
        Me.XrLabel153.CanGrow = False
        Me.XrLabel153.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R7Marca]")})
        Me.XrLabel153.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel153.LocationFloat = New DevExpress.Utils.PointFloat(265.1556!, 880.2286!)
        Me.XrLabel153.Name = "XrLabel153"
        Me.XrLabel153.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel153.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel153.StylePriority.UseFont = False
        Me.XrLabel153.Text = "Marca:"
        '
        'XrLabel154
        '
        Me.XrLabel154.CanGrow = False
        Me.XrLabel154.CanShrink = True
        Me.XrLabel154.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R7Precio2]")})
        Me.XrLabel154.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel154.LocationFloat = New DevExpress.Utils.PointFloat(203.697!, 847.6769!)
        Me.XrLabel154.Multiline = True
        Me.XrLabel154.Name = "XrLabel154"
        Me.XrLabel154.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel154.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel154.StylePriority.UseFont = False
        Me.XrLabel154.StylePriority.UseTextAlignment = False
        Me.XrLabel154.Text = "$99,999.00"
        Me.XrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel154.TextFormatString = "{0:$0}"
        '
        'XrLabel155
        '
        Me.XrLabel155.CanGrow = False
        Me.XrLabel155.CanShrink = True
        Me.XrLabel155.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R7Precio5]")})
        Me.XrLabel155.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel155.LocationFloat = New DevExpress.Utils.PointFloat(230.0715!, 804.0229!)
        Me.XrLabel155.Multiline = True
        Me.XrLabel155.Name = "XrLabel155"
        Me.XrLabel155.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel155.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel155.StylePriority.UseFont = False
        Me.XrLabel155.StylePriority.UseTextAlignment = False
        Me.XrLabel155.Text = "$99,999.00"
        Me.XrLabel155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel155.TextFormatString = "{0:N0}"
        '
        'XrLabel156
        '
        Me.XrLabel156.CanGrow = False
        Me.XrLabel156.CanShrink = True
        Me.XrLabel156.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R7Precio1]")})
        Me.XrLabel156.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel156.LocationFloat = New DevExpress.Utils.PointFloat(203.697!, 837.2989!)
        Me.XrLabel156.Multiline = True
        Me.XrLabel156.Name = "XrLabel156"
        Me.XrLabel156.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel156.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel156.StylePriority.UseFont = False
        Me.XrLabel156.StylePriority.UseTextAlignment = False
        Me.XrLabel156.Text = "$99,999.00"
        Me.XrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel156.TextFormatString = "{0:$0}"
        '
        'XrLabel157
        '
        Me.XrLabel157.CanGrow = False
        Me.XrLabel157.CanShrink = True
        Me.XrLabel157.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R7Precio1]")})
        Me.XrLabel157.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel157.LocationFloat = New DevExpress.Utils.PointFloat(400.2125!, 837.299!)
        Me.XrLabel157.Multiline = True
        Me.XrLabel157.Name = "XrLabel157"
        Me.XrLabel157.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel157.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel157.StylePriority.UseFont = False
        Me.XrLabel157.StylePriority.UseTextAlignment = False
        Me.XrLabel157.Text = "$99,999.00"
        Me.XrLabel157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel157.TextFormatString = "{0:$0}"
        '
        'XrLabel158
        '
        Me.XrLabel158.CanGrow = False
        Me.XrLabel158.CanShrink = True
        Me.XrLabel158.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R7Precio5]")})
        Me.XrLabel158.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel158.LocationFloat = New DevExpress.Utils.PointFloat(426.5869!, 804.0229!)
        Me.XrLabel158.Multiline = True
        Me.XrLabel158.Name = "XrLabel158"
        Me.XrLabel158.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel158.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel158.StylePriority.UseFont = False
        Me.XrLabel158.StylePriority.UseTextAlignment = False
        Me.XrLabel158.Text = "$99,999.00"
        Me.XrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel158.TextFormatString = "{0:N0}"
        '
        'XrLabel159
        '
        Me.XrLabel159.CanGrow = False
        Me.XrLabel159.CanShrink = True
        Me.XrLabel159.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R7Precio2]")})
        Me.XrLabel159.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel159.LocationFloat = New DevExpress.Utils.PointFloat(400.2125!, 847.6769!)
        Me.XrLabel159.Multiline = True
        Me.XrLabel159.Name = "XrLabel159"
        Me.XrLabel159.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel159.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel159.StylePriority.UseFont = False
        Me.XrLabel159.StylePriority.UseTextAlignment = False
        Me.XrLabel159.Text = "$99,999.00"
        Me.XrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel159.TextFormatString = "{0:$0}"
        '
        'XrLabel160
        '
        Me.XrLabel160.CanGrow = False
        Me.XrLabel160.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R7Marca]")})
        Me.XrLabel160.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel160.LocationFloat = New DevExpress.Utils.PointFloat(461.6711!, 880.2286!)
        Me.XrLabel160.Name = "XrLabel160"
        Me.XrLabel160.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel160.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel160.StylePriority.UseFont = False
        Me.XrLabel160.Text = "Marca:"
        '
        'XrLabel161
        '
        Me.XrLabel161.CanGrow = False
        Me.XrLabel161.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R7Estilo]")})
        Me.XrLabel161.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel161.LocationFloat = New DevExpress.Utils.PointFloat(403.0308!, 880.2286!)
        Me.XrLabel161.Name = "XrLabel161"
        Me.XrLabel161.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel161.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel161.StylePriority.UseFont = False
        Me.XrLabel161.Text = "Estilo:"
        '
        'XrLabel162
        '
        Me.XrLabel162.CanGrow = False
        Me.XrLabel162.CanShrink = True
        Me.XrLabel162.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R7Precio3]")})
        Me.XrLabel162.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel162.LocationFloat = New DevExpress.Utils.PointFloat(492.6032!, 804.0229!)
        Me.XrLabel162.Multiline = True
        Me.XrLabel162.Name = "XrLabel162"
        Me.XrLabel162.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel162.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel162.StylePriority.UseFont = False
        Me.XrLabel162.StylePriority.UseTextAlignment = False
        Me.XrLabel162.Text = "$99,999.00"
        Me.XrLabel162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel162.TextFormatString = "{0:N0}"
        '
        'XrLabel163
        '
        Me.XrLabel163.CanGrow = False
        Me.XrLabel163.CanShrink = True
        Me.XrLabel163.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R7Precio1]")})
        Me.XrLabel163.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel163.LocationFloat = New DevExpress.Utils.PointFloat(596.7279!, 837.299!)
        Me.XrLabel163.Multiline = True
        Me.XrLabel163.Name = "XrLabel163"
        Me.XrLabel163.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel163.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel163.StylePriority.UseFont = False
        Me.XrLabel163.StylePriority.UseTextAlignment = False
        Me.XrLabel163.Text = "$99,999.00"
        Me.XrLabel163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel163.TextFormatString = "{0:$0}"
        '
        'XrLabel164
        '
        Me.XrLabel164.CanGrow = False
        Me.XrLabel164.CanShrink = True
        Me.XrLabel164.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R7Precio5]")})
        Me.XrLabel164.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel164.LocationFloat = New DevExpress.Utils.PointFloat(623.1024!, 804.0229!)
        Me.XrLabel164.Multiline = True
        Me.XrLabel164.Name = "XrLabel164"
        Me.XrLabel164.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel164.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel164.StylePriority.UseFont = False
        Me.XrLabel164.StylePriority.UseTextAlignment = False
        Me.XrLabel164.Text = "$99,999.00"
        Me.XrLabel164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel164.TextFormatString = "{0:N0}"
        '
        'XrLabel165
        '
        Me.XrLabel165.CanGrow = False
        Me.XrLabel165.CanShrink = True
        Me.XrLabel165.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R7Precio2]")})
        Me.XrLabel165.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel165.LocationFloat = New DevExpress.Utils.PointFloat(596.7279!, 847.6769!)
        Me.XrLabel165.Multiline = True
        Me.XrLabel165.Name = "XrLabel165"
        Me.XrLabel165.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel165.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel165.StylePriority.UseFont = False
        Me.XrLabel165.StylePriority.UseTextAlignment = False
        Me.XrLabel165.Text = "$99,999.00"
        Me.XrLabel165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel165.TextFormatString = "{0:$0}"
        '
        'XrLabel166
        '
        Me.XrLabel166.CanGrow = False
        Me.XrLabel166.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R7Marca]")})
        Me.XrLabel166.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel166.LocationFloat = New DevExpress.Utils.PointFloat(658.1865!, 880.2286!)
        Me.XrLabel166.Name = "XrLabel166"
        Me.XrLabel166.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel166.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel166.StylePriority.UseFont = False
        Me.XrLabel166.Text = "Marca:"
        '
        'XrLabel167
        '
        Me.XrLabel167.CanGrow = False
        Me.XrLabel167.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R7Estilo]")})
        Me.XrLabel167.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel167.LocationFloat = New DevExpress.Utils.PointFloat(599.5462!, 880.2286!)
        Me.XrLabel167.Name = "XrLabel167"
        Me.XrLabel167.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel167.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel167.StylePriority.UseFont = False
        Me.XrLabel167.Text = "Estilo:"
        '
        'XrLabel168
        '
        Me.XrLabel168.CanGrow = False
        Me.XrLabel168.CanShrink = True
        Me.XrLabel168.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R7Precio3]")})
        Me.XrLabel168.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel168.LocationFloat = New DevExpress.Utils.PointFloat(689.1186!, 804.0229!)
        Me.XrLabel168.Multiline = True
        Me.XrLabel168.Name = "XrLabel168"
        Me.XrLabel168.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel168.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel168.StylePriority.UseFont = False
        Me.XrLabel168.StylePriority.UseTextAlignment = False
        Me.XrLabel168.Text = "$99,999.00"
        Me.XrLabel168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel168.TextFormatString = "{0:N0}"
        '
        'XrLabel121
        '
        Me.XrLabel121.CanGrow = False
        Me.XrLabel121.CanShrink = True
        Me.XrLabel121.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R6Precio1]")})
        Me.XrLabel121.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel121.LocationFloat = New DevExpress.Utils.PointFloat(7.181454!, 709.6948!)
        Me.XrLabel121.Multiline = True
        Me.XrLabel121.Name = "XrLabel121"
        Me.XrLabel121.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel121.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel121.StylePriority.UseFont = False
        Me.XrLabel121.StylePriority.UseTextAlignment = False
        Me.XrLabel121.Text = "$99,999.00"
        Me.XrLabel121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel121.TextFormatString = "{0:$0}"
        '
        'XrLabel122
        '
        Me.XrLabel122.CanGrow = False
        Me.XrLabel122.CanShrink = True
        Me.XrLabel122.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R6Precio5]")})
        Me.XrLabel122.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel122.LocationFloat = New DevExpress.Utils.PointFloat(33.55598!, 676.4188!)
        Me.XrLabel122.Multiline = True
        Me.XrLabel122.Name = "XrLabel122"
        Me.XrLabel122.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel122.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel122.StylePriority.UseFont = False
        Me.XrLabel122.StylePriority.UseTextAlignment = False
        Me.XrLabel122.Text = "$99,999.00"
        Me.XrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel122.TextFormatString = "{0:N0}"
        '
        'XrLabel123
        '
        Me.XrLabel123.CanGrow = False
        Me.XrLabel123.CanShrink = True
        Me.XrLabel123.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R6Precio2]")})
        Me.XrLabel123.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel123.LocationFloat = New DevExpress.Utils.PointFloat(7.181454!, 720.0728!)
        Me.XrLabel123.Multiline = True
        Me.XrLabel123.Name = "XrLabel123"
        Me.XrLabel123.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel123.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel123.StylePriority.UseFont = False
        Me.XrLabel123.StylePriority.UseTextAlignment = False
        Me.XrLabel123.Text = "$99,999.00"
        Me.XrLabel123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel123.TextFormatString = "{0:$0}"
        '
        'XrLabel124
        '
        Me.XrLabel124.CanGrow = False
        Me.XrLabel124.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R6Marca]")})
        Me.XrLabel124.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel124.LocationFloat = New DevExpress.Utils.PointFloat(68.64005!, 751.6246!)
        Me.XrLabel124.Name = "XrLabel124"
        Me.XrLabel124.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel124.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel124.StylePriority.UseFont = False
        Me.XrLabel124.Text = "Marca:"
        '
        'XrLabel125
        '
        Me.XrLabel125.CanGrow = False
        Me.XrLabel125.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R6Estilo]")})
        Me.XrLabel125.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel125.LocationFloat = New DevExpress.Utils.PointFloat(9.999816!, 751.6246!)
        Me.XrLabel125.Name = "XrLabel125"
        Me.XrLabel125.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel125.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel125.StylePriority.UseFont = False
        Me.XrLabel125.Text = "Estilo:"
        '
        'XrLabel126
        '
        Me.XrLabel126.CanGrow = False
        Me.XrLabel126.CanShrink = True
        Me.XrLabel126.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R6Precio3]")})
        Me.XrLabel126.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel126.LocationFloat = New DevExpress.Utils.PointFloat(99.57234!, 676.4188!)
        Me.XrLabel126.Multiline = True
        Me.XrLabel126.Name = "XrLabel126"
        Me.XrLabel126.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel126.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel126.StylePriority.UseFont = False
        Me.XrLabel126.StylePriority.UseTextAlignment = False
        Me.XrLabel126.Text = "$99,999.00"
        Me.XrLabel126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel126.TextFormatString = "{0:N0}"
        '
        'XrLabel127
        '
        Me.XrLabel127.CanGrow = False
        Me.XrLabel127.CanShrink = True
        Me.XrLabel127.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R6Precio3]")})
        Me.XrLabel127.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel127.LocationFloat = New DevExpress.Utils.PointFloat(296.0879!, 676.4188!)
        Me.XrLabel127.Multiline = True
        Me.XrLabel127.Name = "XrLabel127"
        Me.XrLabel127.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel127.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel127.StylePriority.UseFont = False
        Me.XrLabel127.StylePriority.UseTextAlignment = False
        Me.XrLabel127.Text = "$99,999.00"
        Me.XrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel127.TextFormatString = "{0:N0}"
        '
        'XrLabel128
        '
        Me.XrLabel128.CanGrow = False
        Me.XrLabel128.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R6Estilo]")})
        Me.XrLabel128.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel128.LocationFloat = New DevExpress.Utils.PointFloat(206.5154!, 751.6246!)
        Me.XrLabel128.Name = "XrLabel128"
        Me.XrLabel128.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel128.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel128.StylePriority.UseFont = False
        Me.XrLabel128.Text = "Estilo:"
        '
        'XrLabel129
        '
        Me.XrLabel129.CanGrow = False
        Me.XrLabel129.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R6Marca]")})
        Me.XrLabel129.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel129.LocationFloat = New DevExpress.Utils.PointFloat(265.1556!, 751.6246!)
        Me.XrLabel129.Name = "XrLabel129"
        Me.XrLabel129.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel129.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel129.StylePriority.UseFont = False
        Me.XrLabel129.Text = "Marca:"
        '
        'XrLabel130
        '
        Me.XrLabel130.CanGrow = False
        Me.XrLabel130.CanShrink = True
        Me.XrLabel130.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R6Precio2]")})
        Me.XrLabel130.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel130.LocationFloat = New DevExpress.Utils.PointFloat(203.6971!, 720.0729!)
        Me.XrLabel130.Multiline = True
        Me.XrLabel130.Name = "XrLabel130"
        Me.XrLabel130.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel130.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel130.StylePriority.UseFont = False
        Me.XrLabel130.StylePriority.UseTextAlignment = False
        Me.XrLabel130.Text = "$99,999.00"
        Me.XrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel130.TextFormatString = "{0:$0}"
        '
        'XrLabel131
        '
        Me.XrLabel131.CanGrow = False
        Me.XrLabel131.CanShrink = True
        Me.XrLabel131.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R6Precio5]")})
        Me.XrLabel131.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel131.LocationFloat = New DevExpress.Utils.PointFloat(230.0716!, 676.4188!)
        Me.XrLabel131.Multiline = True
        Me.XrLabel131.Name = "XrLabel131"
        Me.XrLabel131.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel131.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel131.StylePriority.UseFont = False
        Me.XrLabel131.StylePriority.UseTextAlignment = False
        Me.XrLabel131.Text = "$99,999.00"
        Me.XrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel131.TextFormatString = "{0:N0}"
        '
        'XrLabel132
        '
        Me.XrLabel132.CanGrow = False
        Me.XrLabel132.CanShrink = True
        Me.XrLabel132.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R6Precio1]")})
        Me.XrLabel132.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel132.LocationFloat = New DevExpress.Utils.PointFloat(203.6971!, 709.6948!)
        Me.XrLabel132.Multiline = True
        Me.XrLabel132.Name = "XrLabel132"
        Me.XrLabel132.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel132.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel132.StylePriority.UseFont = False
        Me.XrLabel132.StylePriority.UseTextAlignment = False
        Me.XrLabel132.Text = "$99,999.00"
        Me.XrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel132.TextFormatString = "{0:$0}"
        '
        'XrLabel133
        '
        Me.XrLabel133.CanGrow = False
        Me.XrLabel133.CanShrink = True
        Me.XrLabel133.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R6Precio1]")})
        Me.XrLabel133.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel133.LocationFloat = New DevExpress.Utils.PointFloat(400.2126!, 709.6949!)
        Me.XrLabel133.Multiline = True
        Me.XrLabel133.Name = "XrLabel133"
        Me.XrLabel133.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel133.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel133.StylePriority.UseFont = False
        Me.XrLabel133.StylePriority.UseTextAlignment = False
        Me.XrLabel133.Text = "$99,999.00"
        Me.XrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel133.TextFormatString = "{0:$0}"
        '
        'XrLabel134
        '
        Me.XrLabel134.CanGrow = False
        Me.XrLabel134.CanShrink = True
        Me.XrLabel134.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R6Precio5]")})
        Me.XrLabel134.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel134.LocationFloat = New DevExpress.Utils.PointFloat(426.587!, 676.4189!)
        Me.XrLabel134.Multiline = True
        Me.XrLabel134.Name = "XrLabel134"
        Me.XrLabel134.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel134.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel134.StylePriority.UseFont = False
        Me.XrLabel134.StylePriority.UseTextAlignment = False
        Me.XrLabel134.Text = "$99,999.00"
        Me.XrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel134.TextFormatString = "{0:N0}"
        '
        'XrLabel135
        '
        Me.XrLabel135.CanGrow = False
        Me.XrLabel135.CanShrink = True
        Me.XrLabel135.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R6Precio2]")})
        Me.XrLabel135.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel135.LocationFloat = New DevExpress.Utils.PointFloat(400.2126!, 720.0729!)
        Me.XrLabel135.Multiline = True
        Me.XrLabel135.Name = "XrLabel135"
        Me.XrLabel135.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel135.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel135.StylePriority.UseFont = False
        Me.XrLabel135.StylePriority.UseTextAlignment = False
        Me.XrLabel135.Text = "$99,999.00"
        Me.XrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel135.TextFormatString = "{0:$0}"
        '
        'XrLabel136
        '
        Me.XrLabel136.CanGrow = False
        Me.XrLabel136.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R6Marca]")})
        Me.XrLabel136.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel136.LocationFloat = New DevExpress.Utils.PointFloat(461.6712!, 751.6246!)
        Me.XrLabel136.Name = "XrLabel136"
        Me.XrLabel136.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel136.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel136.StylePriority.UseFont = False
        Me.XrLabel136.Text = "Marca:"
        '
        'XrLabel137
        '
        Me.XrLabel137.CanGrow = False
        Me.XrLabel137.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R6Estilo]")})
        Me.XrLabel137.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel137.LocationFloat = New DevExpress.Utils.PointFloat(403.0309!, 751.6246!)
        Me.XrLabel137.Name = "XrLabel137"
        Me.XrLabel137.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel137.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel137.StylePriority.UseFont = False
        Me.XrLabel137.Text = "Estilo:"
        '
        'XrLabel138
        '
        Me.XrLabel138.CanGrow = False
        Me.XrLabel138.CanShrink = True
        Me.XrLabel138.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R6Precio3]")})
        Me.XrLabel138.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel138.LocationFloat = New DevExpress.Utils.PointFloat(492.6033!, 676.4189!)
        Me.XrLabel138.Multiline = True
        Me.XrLabel138.Name = "XrLabel138"
        Me.XrLabel138.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel138.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel138.StylePriority.UseFont = False
        Me.XrLabel138.StylePriority.UseTextAlignment = False
        Me.XrLabel138.Text = "$99,999.00"
        Me.XrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel138.TextFormatString = "{0:N0}"
        '
        'XrLabel139
        '
        Me.XrLabel139.CanGrow = False
        Me.XrLabel139.CanShrink = True
        Me.XrLabel139.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R6Precio1]")})
        Me.XrLabel139.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel139.LocationFloat = New DevExpress.Utils.PointFloat(596.7281!, 709.6949!)
        Me.XrLabel139.Multiline = True
        Me.XrLabel139.Name = "XrLabel139"
        Me.XrLabel139.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel139.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel139.StylePriority.UseFont = False
        Me.XrLabel139.StylePriority.UseTextAlignment = False
        Me.XrLabel139.Text = "$99,999.00"
        Me.XrLabel139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel139.TextFormatString = "{0:$0}"
        '
        'XrLabel140
        '
        Me.XrLabel140.CanGrow = False
        Me.XrLabel140.CanShrink = True
        Me.XrLabel140.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R6Precio5]")})
        Me.XrLabel140.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel140.LocationFloat = New DevExpress.Utils.PointFloat(623.1025!, 676.4189!)
        Me.XrLabel140.Multiline = True
        Me.XrLabel140.Name = "XrLabel140"
        Me.XrLabel140.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel140.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel140.StylePriority.UseFont = False
        Me.XrLabel140.StylePriority.UseTextAlignment = False
        Me.XrLabel140.Text = "$99,999.00"
        Me.XrLabel140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel140.TextFormatString = "{0:N0}"
        '
        'XrLabel141
        '
        Me.XrLabel141.CanGrow = False
        Me.XrLabel141.CanShrink = True
        Me.XrLabel141.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R6Precio2]")})
        Me.XrLabel141.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel141.LocationFloat = New DevExpress.Utils.PointFloat(596.7281!, 720.0729!)
        Me.XrLabel141.Multiline = True
        Me.XrLabel141.Name = "XrLabel141"
        Me.XrLabel141.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel141.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel141.StylePriority.UseFont = False
        Me.XrLabel141.StylePriority.UseTextAlignment = False
        Me.XrLabel141.Text = "$99,999.00"
        Me.XrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel141.TextFormatString = "{0:$0}"
        '
        'XrLabel142
        '
        Me.XrLabel142.CanGrow = False
        Me.XrLabel142.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R6Marca]")})
        Me.XrLabel142.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel142.LocationFloat = New DevExpress.Utils.PointFloat(658.1866!, 751.6246!)
        Me.XrLabel142.Name = "XrLabel142"
        Me.XrLabel142.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel142.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel142.StylePriority.UseFont = False
        Me.XrLabel142.Text = "Marca:"
        '
        'XrLabel143
        '
        Me.XrLabel143.CanGrow = False
        Me.XrLabel143.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R6Estilo]")})
        Me.XrLabel143.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel143.LocationFloat = New DevExpress.Utils.PointFloat(599.5464!, 751.6246!)
        Me.XrLabel143.Name = "XrLabel143"
        Me.XrLabel143.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel143.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel143.StylePriority.UseFont = False
        Me.XrLabel143.Text = "Estilo:"
        '
        'XrLabel144
        '
        Me.XrLabel144.CanGrow = False
        Me.XrLabel144.CanShrink = True
        Me.XrLabel144.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R6Precio3]")})
        Me.XrLabel144.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel144.LocationFloat = New DevExpress.Utils.PointFloat(689.1187!, 676.4189!)
        Me.XrLabel144.Multiline = True
        Me.XrLabel144.Name = "XrLabel144"
        Me.XrLabel144.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel144.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel144.StylePriority.UseFont = False
        Me.XrLabel144.StylePriority.UseTextAlignment = False
        Me.XrLabel144.Text = "$99,999.00"
        Me.XrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel144.TextFormatString = "{0:N0}"
        '
        'XrLabel97
        '
        Me.XrLabel97.CanGrow = False
        Me.XrLabel97.CanShrink = True
        Me.XrLabel97.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R5Precio1]")})
        Me.XrLabel97.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel97.LocationFloat = New DevExpress.Utils.PointFloat(7.181613!, 582.0907!)
        Me.XrLabel97.Multiline = True
        Me.XrLabel97.Name = "XrLabel97"
        Me.XrLabel97.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel97.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel97.StylePriority.UseFont = False
        Me.XrLabel97.StylePriority.UseTextAlignment = False
        Me.XrLabel97.Text = "$99,999.00"
        Me.XrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel97.TextFormatString = "{0:$0}"
        '
        'XrLabel98
        '
        Me.XrLabel98.CanGrow = False
        Me.XrLabel98.CanShrink = True
        Me.XrLabel98.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R5Precio5]")})
        Me.XrLabel98.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel98.LocationFloat = New DevExpress.Utils.PointFloat(33.55614!, 548.8147!)
        Me.XrLabel98.Multiline = True
        Me.XrLabel98.Name = "XrLabel98"
        Me.XrLabel98.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel98.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel98.StylePriority.UseFont = False
        Me.XrLabel98.StylePriority.UseTextAlignment = False
        Me.XrLabel98.Text = "$99,999.00"
        Me.XrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel98.TextFormatString = "{0:N0}"
        '
        'XrLabel99
        '
        Me.XrLabel99.CanGrow = False
        Me.XrLabel99.CanShrink = True
        Me.XrLabel99.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R5Precio2]")})
        Me.XrLabel99.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel99.LocationFloat = New DevExpress.Utils.PointFloat(7.181613!, 592.4687!)
        Me.XrLabel99.Multiline = True
        Me.XrLabel99.Name = "XrLabel99"
        Me.XrLabel99.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel99.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel99.StylePriority.UseFont = False
        Me.XrLabel99.StylePriority.UseTextAlignment = False
        Me.XrLabel99.Text = "$99,999.00"
        Me.XrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel99.TextFormatString = "{0:$0}"
        '
        'XrLabel100
        '
        Me.XrLabel100.CanGrow = False
        Me.XrLabel100.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R5Marca]")})
        Me.XrLabel100.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel100.LocationFloat = New DevExpress.Utils.PointFloat(68.6402!, 624.0204!)
        Me.XrLabel100.Name = "XrLabel100"
        Me.XrLabel100.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel100.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel100.StylePriority.UseFont = False
        Me.XrLabel100.Text = "Marca:"
        '
        'XrLabel101
        '
        Me.XrLabel101.CanGrow = False
        Me.XrLabel101.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R5Estilo]")})
        Me.XrLabel101.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel101.LocationFloat = New DevExpress.Utils.PointFloat(9.999975!, 624.0204!)
        Me.XrLabel101.Name = "XrLabel101"
        Me.XrLabel101.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel101.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel101.StylePriority.UseFont = False
        Me.XrLabel101.Text = "Estilo:"
        '
        'XrLabel102
        '
        Me.XrLabel102.CanGrow = False
        Me.XrLabel102.CanShrink = True
        Me.XrLabel102.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R5Precio3]")})
        Me.XrLabel102.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel102.LocationFloat = New DevExpress.Utils.PointFloat(99.5725!, 548.8147!)
        Me.XrLabel102.Multiline = True
        Me.XrLabel102.Name = "XrLabel102"
        Me.XrLabel102.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel102.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel102.StylePriority.UseFont = False
        Me.XrLabel102.StylePriority.UseTextAlignment = False
        Me.XrLabel102.Text = "$99,999.00"
        Me.XrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel102.TextFormatString = "{0:N0}"
        '
        'XrLabel103
        '
        Me.XrLabel103.CanGrow = False
        Me.XrLabel103.CanShrink = True
        Me.XrLabel103.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R5Precio3]")})
        Me.XrLabel103.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel103.LocationFloat = New DevExpress.Utils.PointFloat(296.088!, 548.8147!)
        Me.XrLabel103.Multiline = True
        Me.XrLabel103.Name = "XrLabel103"
        Me.XrLabel103.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel103.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel103.StylePriority.UseFont = False
        Me.XrLabel103.StylePriority.UseTextAlignment = False
        Me.XrLabel103.Text = "$99,999.00"
        Me.XrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel103.TextFormatString = "{0:N0}"
        '
        'XrLabel104
        '
        Me.XrLabel104.CanGrow = False
        Me.XrLabel104.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R5Estilo]")})
        Me.XrLabel104.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel104.LocationFloat = New DevExpress.Utils.PointFloat(206.5156!, 624.0204!)
        Me.XrLabel104.Name = "XrLabel104"
        Me.XrLabel104.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel104.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel104.StylePriority.UseFont = False
        Me.XrLabel104.Text = "Estilo:"
        '
        'XrLabel105
        '
        Me.XrLabel105.CanGrow = False
        Me.XrLabel105.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R5Marca]")})
        Me.XrLabel105.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel105.LocationFloat = New DevExpress.Utils.PointFloat(265.1558!, 624.0204!)
        Me.XrLabel105.Name = "XrLabel105"
        Me.XrLabel105.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel105.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel105.StylePriority.UseFont = False
        Me.XrLabel105.Text = "Marca:"
        '
        'XrLabel106
        '
        Me.XrLabel106.CanGrow = False
        Me.XrLabel106.CanShrink = True
        Me.XrLabel106.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R5Precio2]")})
        Me.XrLabel106.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel106.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 592.4687!)
        Me.XrLabel106.Multiline = True
        Me.XrLabel106.Name = "XrLabel106"
        Me.XrLabel106.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel106.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel106.StylePriority.UseFont = False
        Me.XrLabel106.StylePriority.UseTextAlignment = False
        Me.XrLabel106.Text = "$99,999.00"
        Me.XrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel106.TextFormatString = "{0:$0}"
        '
        'XrLabel107
        '
        Me.XrLabel107.CanGrow = False
        Me.XrLabel107.CanShrink = True
        Me.XrLabel107.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R5Precio5]")})
        Me.XrLabel107.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel107.LocationFloat = New DevExpress.Utils.PointFloat(230.0718!, 548.8147!)
        Me.XrLabel107.Multiline = True
        Me.XrLabel107.Name = "XrLabel107"
        Me.XrLabel107.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel107.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel107.StylePriority.UseFont = False
        Me.XrLabel107.StylePriority.UseTextAlignment = False
        Me.XrLabel107.Text = "$99,999.00"
        Me.XrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel107.TextFormatString = "{0:N0}"
        '
        'XrLabel108
        '
        Me.XrLabel108.CanGrow = False
        Me.XrLabel108.CanShrink = True
        Me.XrLabel108.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R5Precio1]")})
        Me.XrLabel108.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel108.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 582.0907!)
        Me.XrLabel108.Multiline = True
        Me.XrLabel108.Name = "XrLabel108"
        Me.XrLabel108.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel108.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel108.StylePriority.UseFont = False
        Me.XrLabel108.StylePriority.UseTextAlignment = False
        Me.XrLabel108.Text = "$99,999.00"
        Me.XrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel108.TextFormatString = "{0:$0}"
        '
        'XrLabel109
        '
        Me.XrLabel109.CanGrow = False
        Me.XrLabel109.CanShrink = True
        Me.XrLabel109.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R5Precio1]")})
        Me.XrLabel109.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel109.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 582.0907!)
        Me.XrLabel109.Multiline = True
        Me.XrLabel109.Name = "XrLabel109"
        Me.XrLabel109.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel109.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel109.StylePriority.UseFont = False
        Me.XrLabel109.StylePriority.UseTextAlignment = False
        Me.XrLabel109.Text = "$99,999.00"
        Me.XrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel109.TextFormatString = "{0:$0}"
        '
        'XrLabel110
        '
        Me.XrLabel110.CanGrow = False
        Me.XrLabel110.CanShrink = True
        Me.XrLabel110.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R5Precio5]")})
        Me.XrLabel110.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel110.LocationFloat = New DevExpress.Utils.PointFloat(426.5872!, 548.8147!)
        Me.XrLabel110.Multiline = True
        Me.XrLabel110.Name = "XrLabel110"
        Me.XrLabel110.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel110.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel110.StylePriority.UseFont = False
        Me.XrLabel110.StylePriority.UseTextAlignment = False
        Me.XrLabel110.Text = "$99,999.00"
        Me.XrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel110.TextFormatString = "{0:N0}"
        '
        'XrLabel111
        '
        Me.XrLabel111.CanGrow = False
        Me.XrLabel111.CanShrink = True
        Me.XrLabel111.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R5Precio2]")})
        Me.XrLabel111.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel111.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 592.4687!)
        Me.XrLabel111.Multiline = True
        Me.XrLabel111.Name = "XrLabel111"
        Me.XrLabel111.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel111.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel111.StylePriority.UseFont = False
        Me.XrLabel111.StylePriority.UseTextAlignment = False
        Me.XrLabel111.Text = "$99,999.00"
        Me.XrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel111.TextFormatString = "{0:$0}"
        '
        'XrLabel112
        '
        Me.XrLabel112.CanGrow = False
        Me.XrLabel112.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R5Marca]")})
        Me.XrLabel112.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel112.LocationFloat = New DevExpress.Utils.PointFloat(461.6713!, 624.0204!)
        Me.XrLabel112.Name = "XrLabel112"
        Me.XrLabel112.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel112.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel112.StylePriority.UseFont = False
        Me.XrLabel112.Text = "Marca:"
        '
        'XrLabel113
        '
        Me.XrLabel113.CanGrow = False
        Me.XrLabel113.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R5Estilo]")})
        Me.XrLabel113.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel113.LocationFloat = New DevExpress.Utils.PointFloat(403.0311!, 624.0204!)
        Me.XrLabel113.Name = "XrLabel113"
        Me.XrLabel113.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel113.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel113.StylePriority.UseFont = False
        Me.XrLabel113.Text = "Estilo:"
        '
        'XrLabel114
        '
        Me.XrLabel114.CanGrow = False
        Me.XrLabel114.CanShrink = True
        Me.XrLabel114.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R5Precio3]")})
        Me.XrLabel114.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel114.LocationFloat = New DevExpress.Utils.PointFloat(492.6035!, 548.8147!)
        Me.XrLabel114.Multiline = True
        Me.XrLabel114.Name = "XrLabel114"
        Me.XrLabel114.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel114.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel114.StylePriority.UseFont = False
        Me.XrLabel114.StylePriority.UseTextAlignment = False
        Me.XrLabel114.Text = "$99,999.00"
        Me.XrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel114.TextFormatString = "{0:N0}"
        '
        'XrLabel115
        '
        Me.XrLabel115.CanGrow = False
        Me.XrLabel115.CanShrink = True
        Me.XrLabel115.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R5Precio1]")})
        Me.XrLabel115.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel115.LocationFloat = New DevExpress.Utils.PointFloat(596.7281!, 582.0907!)
        Me.XrLabel115.Multiline = True
        Me.XrLabel115.Name = "XrLabel115"
        Me.XrLabel115.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel115.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel115.StylePriority.UseFont = False
        Me.XrLabel115.StylePriority.UseTextAlignment = False
        Me.XrLabel115.Text = "$99,999.00"
        Me.XrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel115.TextFormatString = "{0:$0}"
        '
        'XrLabel116
        '
        Me.XrLabel116.CanGrow = False
        Me.XrLabel116.CanShrink = True
        Me.XrLabel116.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R5Precio5]")})
        Me.XrLabel116.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel116.LocationFloat = New DevExpress.Utils.PointFloat(623.1027!, 548.8147!)
        Me.XrLabel116.Multiline = True
        Me.XrLabel116.Name = "XrLabel116"
        Me.XrLabel116.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel116.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel116.StylePriority.UseFont = False
        Me.XrLabel116.StylePriority.UseTextAlignment = False
        Me.XrLabel116.Text = "$99,999.00"
        Me.XrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel116.TextFormatString = "{0:N0}"
        '
        'XrLabel117
        '
        Me.XrLabel117.CanGrow = False
        Me.XrLabel117.CanShrink = True
        Me.XrLabel117.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R5Precio2]")})
        Me.XrLabel117.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel117.LocationFloat = New DevExpress.Utils.PointFloat(596.7281!, 592.4687!)
        Me.XrLabel117.Multiline = True
        Me.XrLabel117.Name = "XrLabel117"
        Me.XrLabel117.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel117.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel117.StylePriority.UseFont = False
        Me.XrLabel117.StylePriority.UseTextAlignment = False
        Me.XrLabel117.Text = "$99,999.00"
        Me.XrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel117.TextFormatString = "{0:$0}"
        '
        'XrLabel118
        '
        Me.XrLabel118.CanGrow = False
        Me.XrLabel118.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R5Marca]")})
        Me.XrLabel118.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel118.LocationFloat = New DevExpress.Utils.PointFloat(658.1868!, 624.0204!)
        Me.XrLabel118.Name = "XrLabel118"
        Me.XrLabel118.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel118.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel118.StylePriority.UseFont = False
        Me.XrLabel118.Text = "Marca:"
        '
        'XrLabel119
        '
        Me.XrLabel119.CanGrow = False
        Me.XrLabel119.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R5Estilo]")})
        Me.XrLabel119.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel119.LocationFloat = New DevExpress.Utils.PointFloat(599.5464!, 624.0204!)
        Me.XrLabel119.Name = "XrLabel119"
        Me.XrLabel119.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel119.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel119.StylePriority.UseFont = False
        Me.XrLabel119.Text = "Estilo:"
        '
        'XrLabel120
        '
        Me.XrLabel120.CanGrow = False
        Me.XrLabel120.CanShrink = True
        Me.XrLabel120.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R5Precio3]")})
        Me.XrLabel120.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel120.LocationFloat = New DevExpress.Utils.PointFloat(689.1189!, 548.8147!)
        Me.XrLabel120.Multiline = True
        Me.XrLabel120.Name = "XrLabel120"
        Me.XrLabel120.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel120.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel120.StylePriority.UseFont = False
        Me.XrLabel120.StylePriority.UseTextAlignment = False
        Me.XrLabel120.Text = "$99,999.00"
        Me.XrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel120.TextFormatString = "{0:N0}"
        '
        'XrLabel73
        '
        Me.XrLabel73.CanGrow = False
        Me.XrLabel73.CanShrink = True
        Me.XrLabel73.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio1]")})
        Me.XrLabel73.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel73.LocationFloat = New DevExpress.Utils.PointFloat(7.181612!, 454.4866!)
        Me.XrLabel73.Multiline = True
        Me.XrLabel73.Name = "XrLabel73"
        Me.XrLabel73.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel73.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel73.StylePriority.UseFont = False
        Me.XrLabel73.StylePriority.UseTextAlignment = False
        Me.XrLabel73.Text = "$99,999.00"
        Me.XrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel73.TextFormatString = "{0:$0}"
        '
        'XrLabel74
        '
        Me.XrLabel74.CanGrow = False
        Me.XrLabel74.CanShrink = True
        Me.XrLabel74.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio5]")})
        Me.XrLabel74.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel74.LocationFloat = New DevExpress.Utils.PointFloat(33.55615!, 421.2106!)
        Me.XrLabel74.Multiline = True
        Me.XrLabel74.Name = "XrLabel74"
        Me.XrLabel74.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel74.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel74.StylePriority.UseFont = False
        Me.XrLabel74.StylePriority.UseTextAlignment = False
        Me.XrLabel74.Text = "$99,999.00"
        Me.XrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel74.TextFormatString = "{0:N0}"
        '
        'XrLabel75
        '
        Me.XrLabel75.CanGrow = False
        Me.XrLabel75.CanShrink = True
        Me.XrLabel75.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio2]")})
        Me.XrLabel75.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel75.LocationFloat = New DevExpress.Utils.PointFloat(7.181612!, 464.8646!)
        Me.XrLabel75.Multiline = True
        Me.XrLabel75.Name = "XrLabel75"
        Me.XrLabel75.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel75.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel75.StylePriority.UseFont = False
        Me.XrLabel75.StylePriority.UseTextAlignment = False
        Me.XrLabel75.Text = "$99,999.00"
        Me.XrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel75.TextFormatString = "{0:$0}"
        '
        'XrLabel76
        '
        Me.XrLabel76.CanGrow = False
        Me.XrLabel76.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Marca]")})
        Me.XrLabel76.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel76.LocationFloat = New DevExpress.Utils.PointFloat(68.6402!, 496.4164!)
        Me.XrLabel76.Name = "XrLabel76"
        Me.XrLabel76.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel76.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel76.StylePriority.UseFont = False
        Me.XrLabel76.Text = "Marca:"
        '
        'XrLabel77
        '
        Me.XrLabel77.CanGrow = False
        Me.XrLabel77.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Estilo]")})
        Me.XrLabel77.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel77.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 496.4164!)
        Me.XrLabel77.Name = "XrLabel77"
        Me.XrLabel77.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel77.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel77.StylePriority.UseFont = False
        Me.XrLabel77.Text = "Estilo:"
        '
        'XrLabel78
        '
        Me.XrLabel78.CanGrow = False
        Me.XrLabel78.CanShrink = True
        Me.XrLabel78.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio3]")})
        Me.XrLabel78.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel78.LocationFloat = New DevExpress.Utils.PointFloat(99.5725!, 421.2106!)
        Me.XrLabel78.Multiline = True
        Me.XrLabel78.Name = "XrLabel78"
        Me.XrLabel78.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel78.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel78.StylePriority.UseFont = False
        Me.XrLabel78.StylePriority.UseTextAlignment = False
        Me.XrLabel78.Text = "$99,999.00"
        Me.XrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel78.TextFormatString = "{0:N0}"
        '
        'XrLabel79
        '
        Me.XrLabel79.CanGrow = False
        Me.XrLabel79.CanShrink = True
        Me.XrLabel79.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio3]")})
        Me.XrLabel79.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel79.LocationFloat = New DevExpress.Utils.PointFloat(296.088!, 421.2106!)
        Me.XrLabel79.Multiline = True
        Me.XrLabel79.Name = "XrLabel79"
        Me.XrLabel79.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel79.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel79.StylePriority.UseFont = False
        Me.XrLabel79.StylePriority.UseTextAlignment = False
        Me.XrLabel79.Text = "$99,999.00"
        Me.XrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel79.TextFormatString = "{0:N0}"
        '
        'XrLabel80
        '
        Me.XrLabel80.CanGrow = False
        Me.XrLabel80.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Estilo]")})
        Me.XrLabel80.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel80.LocationFloat = New DevExpress.Utils.PointFloat(206.5156!, 496.4164!)
        Me.XrLabel80.Name = "XrLabel80"
        Me.XrLabel80.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel80.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel80.StylePriority.UseFont = False
        Me.XrLabel80.Text = "Estilo:"
        '
        'XrLabel81
        '
        Me.XrLabel81.CanGrow = False
        Me.XrLabel81.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Marca]")})
        Me.XrLabel81.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel81.LocationFloat = New DevExpress.Utils.PointFloat(265.1558!, 496.4164!)
        Me.XrLabel81.Name = "XrLabel81"
        Me.XrLabel81.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel81.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel81.StylePriority.UseFont = False
        Me.XrLabel81.Text = "Marca:"
        '
        'XrLabel82
        '
        Me.XrLabel82.CanGrow = False
        Me.XrLabel82.CanShrink = True
        Me.XrLabel82.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio2]")})
        Me.XrLabel82.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel82.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 464.8647!)
        Me.XrLabel82.Multiline = True
        Me.XrLabel82.Name = "XrLabel82"
        Me.XrLabel82.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel82.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel82.StylePriority.UseFont = False
        Me.XrLabel82.StylePriority.UseTextAlignment = False
        Me.XrLabel82.Text = "$99,999.00"
        Me.XrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel82.TextFormatString = "{0:$0}"
        '
        'XrLabel83
        '
        Me.XrLabel83.CanGrow = False
        Me.XrLabel83.CanShrink = True
        Me.XrLabel83.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio5]")})
        Me.XrLabel83.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel83.LocationFloat = New DevExpress.Utils.PointFloat(230.0718!, 421.2106!)
        Me.XrLabel83.Multiline = True
        Me.XrLabel83.Name = "XrLabel83"
        Me.XrLabel83.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel83.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel83.StylePriority.UseFont = False
        Me.XrLabel83.StylePriority.UseTextAlignment = False
        Me.XrLabel83.Text = "$99,999.00"
        Me.XrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel83.TextFormatString = "{0:N0}"
        '
        'XrLabel84
        '
        Me.XrLabel84.CanGrow = False
        Me.XrLabel84.CanShrink = True
        Me.XrLabel84.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio1]")})
        Me.XrLabel84.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel84.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 454.4866!)
        Me.XrLabel84.Multiline = True
        Me.XrLabel84.Name = "XrLabel84"
        Me.XrLabel84.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel84.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel84.StylePriority.UseFont = False
        Me.XrLabel84.StylePriority.UseTextAlignment = False
        Me.XrLabel84.Text = "$99,999.00"
        Me.XrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel84.TextFormatString = "{0:$0}"
        '
        'XrLabel85
        '
        Me.XrLabel85.CanGrow = False
        Me.XrLabel85.CanShrink = True
        Me.XrLabel85.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio1]")})
        Me.XrLabel85.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel85.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 454.4866!)
        Me.XrLabel85.Multiline = True
        Me.XrLabel85.Name = "XrLabel85"
        Me.XrLabel85.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel85.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel85.StylePriority.UseFont = False
        Me.XrLabel85.StylePriority.UseTextAlignment = False
        Me.XrLabel85.Text = "$99,999.00"
        Me.XrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel85.TextFormatString = "{0:$0}"
        '
        'XrLabel86
        '
        Me.XrLabel86.CanGrow = False
        Me.XrLabel86.CanShrink = True
        Me.XrLabel86.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio5]")})
        Me.XrLabel86.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel86.LocationFloat = New DevExpress.Utils.PointFloat(426.5872!, 421.2106!)
        Me.XrLabel86.Multiline = True
        Me.XrLabel86.Name = "XrLabel86"
        Me.XrLabel86.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel86.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel86.StylePriority.UseFont = False
        Me.XrLabel86.StylePriority.UseTextAlignment = False
        Me.XrLabel86.Text = "$99,999.00"
        Me.XrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel86.TextFormatString = "{0:N0}"
        '
        'XrLabel87
        '
        Me.XrLabel87.CanGrow = False
        Me.XrLabel87.CanShrink = True
        Me.XrLabel87.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio2]")})
        Me.XrLabel87.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel87.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 464.8647!)
        Me.XrLabel87.Multiline = True
        Me.XrLabel87.Name = "XrLabel87"
        Me.XrLabel87.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel87.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel87.StylePriority.UseFont = False
        Me.XrLabel87.StylePriority.UseTextAlignment = False
        Me.XrLabel87.Text = "$99,999.00"
        Me.XrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel87.TextFormatString = "{0:$0}"
        '
        'XrLabel88
        '
        Me.XrLabel88.CanGrow = False
        Me.XrLabel88.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Marca]")})
        Me.XrLabel88.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel88.LocationFloat = New DevExpress.Utils.PointFloat(461.6713!, 496.4164!)
        Me.XrLabel88.Name = "XrLabel88"
        Me.XrLabel88.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel88.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel88.StylePriority.UseFont = False
        Me.XrLabel88.Text = "Marca:"
        '
        'XrLabel89
        '
        Me.XrLabel89.CanGrow = False
        Me.XrLabel89.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Estilo]")})
        Me.XrLabel89.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel89.LocationFloat = New DevExpress.Utils.PointFloat(403.0311!, 496.4164!)
        Me.XrLabel89.Name = "XrLabel89"
        Me.XrLabel89.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel89.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel89.StylePriority.UseFont = False
        Me.XrLabel89.Text = "Estilo:"
        '
        'XrLabel90
        '
        Me.XrLabel90.CanGrow = False
        Me.XrLabel90.CanShrink = True
        Me.XrLabel90.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio3]")})
        Me.XrLabel90.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel90.LocationFloat = New DevExpress.Utils.PointFloat(492.6035!, 421.2106!)
        Me.XrLabel90.Multiline = True
        Me.XrLabel90.Name = "XrLabel90"
        Me.XrLabel90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel90.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel90.StylePriority.UseFont = False
        Me.XrLabel90.StylePriority.UseTextAlignment = False
        Me.XrLabel90.Text = "$99,999.00"
        Me.XrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel90.TextFormatString = "{0:N0}"
        '
        'XrLabel91
        '
        Me.XrLabel91.CanGrow = False
        Me.XrLabel91.CanShrink = True
        Me.XrLabel91.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R4Precio1]")})
        Me.XrLabel91.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel91.LocationFloat = New DevExpress.Utils.PointFloat(596.7283!, 454.4866!)
        Me.XrLabel91.Multiline = True
        Me.XrLabel91.Name = "XrLabel91"
        Me.XrLabel91.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel91.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel91.StylePriority.UseFont = False
        Me.XrLabel91.StylePriority.UseTextAlignment = False
        Me.XrLabel91.Text = "$99,999.00"
        Me.XrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel91.TextFormatString = "{0:$0}"
        '
        'XrLabel92
        '
        Me.XrLabel92.CanGrow = False
        Me.XrLabel92.CanShrink = True
        Me.XrLabel92.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R4Precio5]")})
        Me.XrLabel92.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel92.LocationFloat = New DevExpress.Utils.PointFloat(623.1028!, 421.2106!)
        Me.XrLabel92.Multiline = True
        Me.XrLabel92.Name = "XrLabel92"
        Me.XrLabel92.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel92.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel92.StylePriority.UseFont = False
        Me.XrLabel92.StylePriority.UseTextAlignment = False
        Me.XrLabel92.Text = "$99,999.00"
        Me.XrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel92.TextFormatString = "{0:N0}"
        '
        'XrLabel93
        '
        Me.XrLabel93.CanGrow = False
        Me.XrLabel93.CanShrink = True
        Me.XrLabel93.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R4Precio2]")})
        Me.XrLabel93.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel93.LocationFloat = New DevExpress.Utils.PointFloat(596.7283!, 464.8647!)
        Me.XrLabel93.Multiline = True
        Me.XrLabel93.Name = "XrLabel93"
        Me.XrLabel93.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel93.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel93.StylePriority.UseFont = False
        Me.XrLabel93.StylePriority.UseTextAlignment = False
        Me.XrLabel93.Text = "$99,999.00"
        Me.XrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel93.TextFormatString = "{0:$0}"
        '
        'XrLabel94
        '
        Me.XrLabel94.CanGrow = False
        Me.XrLabel94.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R4Marca]")})
        Me.XrLabel94.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel94.LocationFloat = New DevExpress.Utils.PointFloat(658.1869!, 496.4164!)
        Me.XrLabel94.Name = "XrLabel94"
        Me.XrLabel94.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel94.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel94.StylePriority.UseFont = False
        Me.XrLabel94.Text = "Marca:"
        '
        'XrLabel95
        '
        Me.XrLabel95.CanGrow = False
        Me.XrLabel95.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R4Estilo]")})
        Me.XrLabel95.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel95.LocationFloat = New DevExpress.Utils.PointFloat(599.5466!, 496.4164!)
        Me.XrLabel95.Name = "XrLabel95"
        Me.XrLabel95.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel95.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel95.StylePriority.UseFont = False
        Me.XrLabel95.Text = "Estilo:"
        '
        'XrLabel96
        '
        Me.XrLabel96.CanGrow = False
        Me.XrLabel96.CanShrink = True
        Me.XrLabel96.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R4Precio3]")})
        Me.XrLabel96.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel96.LocationFloat = New DevExpress.Utils.PointFloat(689.119!, 421.2106!)
        Me.XrLabel96.Multiline = True
        Me.XrLabel96.Name = "XrLabel96"
        Me.XrLabel96.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel96.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel96.StylePriority.UseFont = False
        Me.XrLabel96.StylePriority.UseTextAlignment = False
        Me.XrLabel96.Text = "$99,999.00"
        Me.XrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel96.TextFormatString = "{0:N0}"
        '
        'XrLabel49
        '
        Me.XrLabel49.CanGrow = False
        Me.XrLabel49.CanShrink = True
        Me.XrLabel49.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio1]")})
        Me.XrLabel49.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel49.LocationFloat = New DevExpress.Utils.PointFloat(7.181554!, 326.8824!)
        Me.XrLabel49.Multiline = True
        Me.XrLabel49.Name = "XrLabel49"
        Me.XrLabel49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel49.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel49.StylePriority.UseFont = False
        Me.XrLabel49.StylePriority.UseTextAlignment = False
        Me.XrLabel49.Text = "$99,999.00"
        Me.XrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel49.TextFormatString = "{0:$0}"
        '
        'XrLabel50
        '
        Me.XrLabel50.CanGrow = False
        Me.XrLabel50.CanShrink = True
        Me.XrLabel50.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio5]")})
        Me.XrLabel50.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel50.LocationFloat = New DevExpress.Utils.PointFloat(33.55609!, 293.6065!)
        Me.XrLabel50.Multiline = True
        Me.XrLabel50.Name = "XrLabel50"
        Me.XrLabel50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel50.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel50.StylePriority.UseFont = False
        Me.XrLabel50.StylePriority.UseTextAlignment = False
        Me.XrLabel50.Text = "$99,999.00"
        Me.XrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel50.TextFormatString = "{0:N0}"
        '
        'XrLabel51
        '
        Me.XrLabel51.CanGrow = False
        Me.XrLabel51.CanShrink = True
        Me.XrLabel51.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio2]")})
        Me.XrLabel51.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel51.LocationFloat = New DevExpress.Utils.PointFloat(7.181554!, 337.2605!)
        Me.XrLabel51.Multiline = True
        Me.XrLabel51.Name = "XrLabel51"
        Me.XrLabel51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel51.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel51.StylePriority.UseFont = False
        Me.XrLabel51.StylePriority.UseTextAlignment = False
        Me.XrLabel51.Text = "$99,999.00"
        Me.XrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel51.TextFormatString = "{0:$0}"
        '
        'XrLabel52
        '
        Me.XrLabel52.CanGrow = False
        Me.XrLabel52.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Marca]")})
        Me.XrLabel52.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel52.LocationFloat = New DevExpress.Utils.PointFloat(68.64014!, 367.8123!)
        Me.XrLabel52.Name = "XrLabel52"
        Me.XrLabel52.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel52.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel52.StylePriority.UseFont = False
        Me.XrLabel52.Text = "Marca:"
        '
        'XrLabel53
        '
        Me.XrLabel53.CanGrow = False
        Me.XrLabel53.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Estilo]")})
        Me.XrLabel53.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel53.LocationFloat = New DevExpress.Utils.PointFloat(9.999946!, 367.8123!)
        Me.XrLabel53.Name = "XrLabel53"
        Me.XrLabel53.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel53.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel53.StylePriority.UseFont = False
        Me.XrLabel53.Text = "Estilo:"
        '
        'XrLabel54
        '
        Me.XrLabel54.CanGrow = False
        Me.XrLabel54.CanShrink = True
        Me.XrLabel54.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio3]")})
        Me.XrLabel54.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel54.LocationFloat = New DevExpress.Utils.PointFloat(99.57246!, 293.6065!)
        Me.XrLabel54.Multiline = True
        Me.XrLabel54.Name = "XrLabel54"
        Me.XrLabel54.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel54.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel54.StylePriority.UseFont = False
        Me.XrLabel54.StylePriority.UseTextAlignment = False
        Me.XrLabel54.Text = "$99,999.00"
        Me.XrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel54.TextFormatString = "{0:N0}"
        '
        'XrLabel55
        '
        Me.XrLabel55.CanGrow = False
        Me.XrLabel55.CanShrink = True
        Me.XrLabel55.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio3]")})
        Me.XrLabel55.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel55.LocationFloat = New DevExpress.Utils.PointFloat(296.0879!, 293.6065!)
        Me.XrLabel55.Multiline = True
        Me.XrLabel55.Name = "XrLabel55"
        Me.XrLabel55.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel55.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel55.StylePriority.UseFont = False
        Me.XrLabel55.StylePriority.UseTextAlignment = False
        Me.XrLabel55.Text = "$99,999.00"
        Me.XrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel55.TextFormatString = "{0:N0}"
        '
        'XrLabel56
        '
        Me.XrLabel56.CanGrow = False
        Me.XrLabel56.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Estilo]")})
        Me.XrLabel56.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel56.LocationFloat = New DevExpress.Utils.PointFloat(206.5155!, 367.8123!)
        Me.XrLabel56.Name = "XrLabel56"
        Me.XrLabel56.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel56.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel56.StylePriority.UseFont = False
        Me.XrLabel56.Text = "Estilo:"
        '
        'XrLabel57
        '
        Me.XrLabel57.CanGrow = False
        Me.XrLabel57.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Marca]")})
        Me.XrLabel57.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel57.LocationFloat = New DevExpress.Utils.PointFloat(265.1557!, 367.8123!)
        Me.XrLabel57.Name = "XrLabel57"
        Me.XrLabel57.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel57.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel57.StylePriority.UseFont = False
        Me.XrLabel57.Text = "Marca:"
        '
        'XrLabel58
        '
        Me.XrLabel58.CanGrow = False
        Me.XrLabel58.CanShrink = True
        Me.XrLabel58.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio2]")})
        Me.XrLabel58.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel58.LocationFloat = New DevExpress.Utils.PointFloat(203.6971!, 337.2605!)
        Me.XrLabel58.Multiline = True
        Me.XrLabel58.Name = "XrLabel58"
        Me.XrLabel58.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel58.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel58.StylePriority.UseFont = False
        Me.XrLabel58.StylePriority.UseTextAlignment = False
        Me.XrLabel58.Text = "$99,999.00"
        Me.XrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel58.TextFormatString = "{0:$0}"
        '
        'XrLabel59
        '
        Me.XrLabel59.CanGrow = False
        Me.XrLabel59.CanShrink = True
        Me.XrLabel59.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio5]")})
        Me.XrLabel59.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel59.LocationFloat = New DevExpress.Utils.PointFloat(230.0717!, 293.6065!)
        Me.XrLabel59.Multiline = True
        Me.XrLabel59.Name = "XrLabel59"
        Me.XrLabel59.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel59.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel59.StylePriority.UseFont = False
        Me.XrLabel59.StylePriority.UseTextAlignment = False
        Me.XrLabel59.Text = "$99,999.00"
        Me.XrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel59.TextFormatString = "{0:N0}"
        '
        'XrLabel60
        '
        Me.XrLabel60.CanGrow = False
        Me.XrLabel60.CanShrink = True
        Me.XrLabel60.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio1]")})
        Me.XrLabel60.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel60.LocationFloat = New DevExpress.Utils.PointFloat(203.6971!, 326.8825!)
        Me.XrLabel60.Multiline = True
        Me.XrLabel60.Name = "XrLabel60"
        Me.XrLabel60.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel60.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel60.StylePriority.UseFont = False
        Me.XrLabel60.StylePriority.UseTextAlignment = False
        Me.XrLabel60.Text = "$99,999.00"
        Me.XrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel60.TextFormatString = "{0:$0}"
        '
        'XrLabel61
        '
        Me.XrLabel61.CanGrow = False
        Me.XrLabel61.CanShrink = True
        Me.XrLabel61.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio1]")})
        Me.XrLabel61.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel61.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 326.8825!)
        Me.XrLabel61.Multiline = True
        Me.XrLabel61.Name = "XrLabel61"
        Me.XrLabel61.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel61.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel61.StylePriority.UseFont = False
        Me.XrLabel61.StylePriority.UseTextAlignment = False
        Me.XrLabel61.Text = "$99,999.00"
        Me.XrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel61.TextFormatString = "{0:$0}"
        '
        'XrLabel62
        '
        Me.XrLabel62.CanGrow = False
        Me.XrLabel62.CanShrink = True
        Me.XrLabel62.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio5]")})
        Me.XrLabel62.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel62.LocationFloat = New DevExpress.Utils.PointFloat(426.5872!, 293.6065!)
        Me.XrLabel62.Multiline = True
        Me.XrLabel62.Name = "XrLabel62"
        Me.XrLabel62.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel62.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel62.StylePriority.UseFont = False
        Me.XrLabel62.StylePriority.UseTextAlignment = False
        Me.XrLabel62.Text = "$99,999.00"
        Me.XrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel62.TextFormatString = "{0:N0}"
        '
        'XrLabel63
        '
        Me.XrLabel63.CanGrow = False
        Me.XrLabel63.CanShrink = True
        Me.XrLabel63.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio2]")})
        Me.XrLabel63.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel63.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 337.2605!)
        Me.XrLabel63.Multiline = True
        Me.XrLabel63.Name = "XrLabel63"
        Me.XrLabel63.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel63.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel63.StylePriority.UseFont = False
        Me.XrLabel63.StylePriority.UseTextAlignment = False
        Me.XrLabel63.Text = "$99,999.00"
        Me.XrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel63.TextFormatString = "{0:$0}"
        '
        'XrLabel64
        '
        Me.XrLabel64.CanGrow = False
        Me.XrLabel64.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Marca]")})
        Me.XrLabel64.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel64.LocationFloat = New DevExpress.Utils.PointFloat(461.6712!, 367.8123!)
        Me.XrLabel64.Name = "XrLabel64"
        Me.XrLabel64.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel64.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel64.StylePriority.UseFont = False
        Me.XrLabel64.Text = "Marca:"
        '
        'XrLabel65
        '
        Me.XrLabel65.CanGrow = False
        Me.XrLabel65.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Estilo]")})
        Me.XrLabel65.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel65.LocationFloat = New DevExpress.Utils.PointFloat(403.031!, 367.8123!)
        Me.XrLabel65.Name = "XrLabel65"
        Me.XrLabel65.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel65.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel65.StylePriority.UseFont = False
        Me.XrLabel65.Text = "Estilo:"
        '
        'XrLabel66
        '
        Me.XrLabel66.CanGrow = False
        Me.XrLabel66.CanShrink = True
        Me.XrLabel66.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio3]")})
        Me.XrLabel66.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel66.LocationFloat = New DevExpress.Utils.PointFloat(492.6035!, 293.6065!)
        Me.XrLabel66.Multiline = True
        Me.XrLabel66.Name = "XrLabel66"
        Me.XrLabel66.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel66.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel66.StylePriority.UseFont = False
        Me.XrLabel66.StylePriority.UseTextAlignment = False
        Me.XrLabel66.Text = "$99,999.00"
        Me.XrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel66.TextFormatString = "{0:N0}"
        '
        'XrLabel67
        '
        Me.XrLabel67.CanGrow = False
        Me.XrLabel67.CanShrink = True
        Me.XrLabel67.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R3Precio1]")})
        Me.XrLabel67.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel67.LocationFloat = New DevExpress.Utils.PointFloat(596.7281!, 326.8825!)
        Me.XrLabel67.Multiline = True
        Me.XrLabel67.Name = "XrLabel67"
        Me.XrLabel67.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel67.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel67.StylePriority.UseFont = False
        Me.XrLabel67.StylePriority.UseTextAlignment = False
        Me.XrLabel67.Text = "$99,999.00"
        Me.XrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel67.TextFormatString = "{0:$0}"
        '
        'XrLabel68
        '
        Me.XrLabel68.CanGrow = False
        Me.XrLabel68.CanShrink = True
        Me.XrLabel68.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R3Precio5]")})
        Me.XrLabel68.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel68.LocationFloat = New DevExpress.Utils.PointFloat(623.1027!, 293.6065!)
        Me.XrLabel68.Multiline = True
        Me.XrLabel68.Name = "XrLabel68"
        Me.XrLabel68.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel68.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel68.StylePriority.UseFont = False
        Me.XrLabel68.StylePriority.UseTextAlignment = False
        Me.XrLabel68.Text = "$99,999.00"
        Me.XrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel68.TextFormatString = "{0:N0}"
        '
        'XrLabel69
        '
        Me.XrLabel69.CanGrow = False
        Me.XrLabel69.CanShrink = True
        Me.XrLabel69.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R3Precio2]")})
        Me.XrLabel69.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel69.LocationFloat = New DevExpress.Utils.PointFloat(596.7281!, 337.2605!)
        Me.XrLabel69.Multiline = True
        Me.XrLabel69.Name = "XrLabel69"
        Me.XrLabel69.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel69.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel69.StylePriority.UseFont = False
        Me.XrLabel69.StylePriority.UseTextAlignment = False
        Me.XrLabel69.Text = "$99,999.00"
        Me.XrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel69.TextFormatString = "{0:$0}"
        '
        'XrLabel70
        '
        Me.XrLabel70.CanGrow = False
        Me.XrLabel70.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R3Marca]")})
        Me.XrLabel70.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel70.LocationFloat = New DevExpress.Utils.PointFloat(658.1868!, 367.8123!)
        Me.XrLabel70.Name = "XrLabel70"
        Me.XrLabel70.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel70.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel70.StylePriority.UseFont = False
        Me.XrLabel70.Text = "Marca:"
        '
        'XrLabel71
        '
        Me.XrLabel71.CanGrow = False
        Me.XrLabel71.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R3Estilo]")})
        Me.XrLabel71.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel71.LocationFloat = New DevExpress.Utils.PointFloat(599.5467!, 367.8123!)
        Me.XrLabel71.Name = "XrLabel71"
        Me.XrLabel71.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel71.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel71.StylePriority.UseFont = False
        Me.XrLabel71.Text = "Estilo:"
        '
        'XrLabel72
        '
        Me.XrLabel72.CanGrow = False
        Me.XrLabel72.CanShrink = True
        Me.XrLabel72.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R3Precio3]")})
        Me.XrLabel72.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel72.LocationFloat = New DevExpress.Utils.PointFloat(689.1191!, 293.6065!)
        Me.XrLabel72.Multiline = True
        Me.XrLabel72.Name = "XrLabel72"
        Me.XrLabel72.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel72.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel72.StylePriority.UseFont = False
        Me.XrLabel72.StylePriority.UseTextAlignment = False
        Me.XrLabel72.Text = "$99,999.00"
        Me.XrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel72.TextFormatString = "{0:N0}"
        '
        'XrLabel25
        '
        Me.XrLabel25.CanGrow = False
        Me.XrLabel25.CanShrink = True
        Me.XrLabel25.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio1]")})
        Me.XrLabel25.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(7.181612!, 199.2784!)
        Me.XrLabel25.Multiline = True
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = "$99,999.00"
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel25.TextFormatString = "{0:$0}"
        '
        'XrLabel26
        '
        Me.XrLabel26.CanGrow = False
        Me.XrLabel26.CanShrink = True
        Me.XrLabel26.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio5]")})
        Me.XrLabel26.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(33.55615!, 166.0025!)
        Me.XrLabel26.Multiline = True
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "$99,999.00"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel26.TextFormatString = "{0:N0}"
        '
        'XrLabel27
        '
        Me.XrLabel27.CanGrow = False
        Me.XrLabel27.CanShrink = True
        Me.XrLabel27.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio2]")})
        Me.XrLabel27.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(7.181612!, 209.6565!)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.StylePriority.UseTextAlignment = False
        Me.XrLabel27.Text = "$99,999.00"
        Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel27.TextFormatString = "{0:$0}"
        '
        'XrLabel28
        '
        Me.XrLabel28.CanGrow = False
        Me.XrLabel28.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Marca]")})
        Me.XrLabel28.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(68.6402!, 240.2083!)
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel28.StylePriority.UseFont = False
        Me.XrLabel28.Text = "Marca:"
        '
        'XrLabel29
        '
        Me.XrLabel29.CanGrow = False
        Me.XrLabel29.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Estilo]")})
        Me.XrLabel29.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 240.2083!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.Text = "Estilo:"
        '
        'XrLabel30
        '
        Me.XrLabel30.CanGrow = False
        Me.XrLabel30.CanShrink = True
        Me.XrLabel30.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio3]")})
        Me.XrLabel30.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(99.5725!, 166.0025!)
        Me.XrLabel30.Multiline = True
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.StylePriority.UseTextAlignment = False
        Me.XrLabel30.Text = "$99,999.00"
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel30.TextFormatString = "{0:N0}"
        '
        'XrLabel31
        '
        Me.XrLabel31.CanGrow = False
        Me.XrLabel31.CanShrink = True
        Me.XrLabel31.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio3]")})
        Me.XrLabel31.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(296.088!, 166.0025!)
        Me.XrLabel31.Multiline = True
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.StylePriority.UseTextAlignment = False
        Me.XrLabel31.Text = "$99,999.00"
        Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel31.TextFormatString = "{0:N0}"
        '
        'XrLabel32
        '
        Me.XrLabel32.CanGrow = False
        Me.XrLabel32.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Estilo]")})
        Me.XrLabel32.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(206.5156!, 240.2083!)
        Me.XrLabel32.Name = "XrLabel32"
        Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel32.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel32.StylePriority.UseFont = False
        Me.XrLabel32.Text = "Estilo:"
        '
        'XrLabel33
        '
        Me.XrLabel33.CanGrow = False
        Me.XrLabel33.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Marca]")})
        Me.XrLabel33.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(265.1558!, 240.2083!)
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel33.StylePriority.UseFont = False
        Me.XrLabel33.Text = "Marca:"
        '
        'XrLabel34
        '
        Me.XrLabel34.CanGrow = False
        Me.XrLabel34.CanShrink = True
        Me.XrLabel34.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio2]")})
        Me.XrLabel34.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 209.6565!)
        Me.XrLabel34.Multiline = True
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel34.StylePriority.UseFont = False
        Me.XrLabel34.StylePriority.UseTextAlignment = False
        Me.XrLabel34.Text = "$99,999.00"
        Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel34.TextFormatString = "{0:$0}"
        '
        'XrLabel35
        '
        Me.XrLabel35.CanGrow = False
        Me.XrLabel35.CanShrink = True
        Me.XrLabel35.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio5]")})
        Me.XrLabel35.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel35.LocationFloat = New DevExpress.Utils.PointFloat(230.0718!, 166.0025!)
        Me.XrLabel35.Multiline = True
        Me.XrLabel35.Name = "XrLabel35"
        Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel35.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel35.StylePriority.UseFont = False
        Me.XrLabel35.StylePriority.UseTextAlignment = False
        Me.XrLabel35.Text = "$99,999.00"
        Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel35.TextFormatString = "{0:N0}"
        '
        'XrLabel36
        '
        Me.XrLabel36.CanGrow = False
        Me.XrLabel36.CanShrink = True
        Me.XrLabel36.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio1]")})
        Me.XrLabel36.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel36.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 199.2785!)
        Me.XrLabel36.Multiline = True
        Me.XrLabel36.Name = "XrLabel36"
        Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel36.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel36.StylePriority.UseFont = False
        Me.XrLabel36.StylePriority.UseTextAlignment = False
        Me.XrLabel36.Text = "$99,999.00"
        Me.XrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel36.TextFormatString = "{0:$0}"
        '
        'XrLabel37
        '
        Me.XrLabel37.CanGrow = False
        Me.XrLabel37.CanShrink = True
        Me.XrLabel37.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio1]")})
        Me.XrLabel37.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel37.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 199.2785!)
        Me.XrLabel37.Multiline = True
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel37.StylePriority.UseFont = False
        Me.XrLabel37.StylePriority.UseTextAlignment = False
        Me.XrLabel37.Text = "$99,999.00"
        Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel37.TextFormatString = "{0:$0}"
        '
        'XrLabel38
        '
        Me.XrLabel38.CanGrow = False
        Me.XrLabel38.CanShrink = True
        Me.XrLabel38.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio5]")})
        Me.XrLabel38.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel38.LocationFloat = New DevExpress.Utils.PointFloat(426.5872!, 166.0025!)
        Me.XrLabel38.Multiline = True
        Me.XrLabel38.Name = "XrLabel38"
        Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel38.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel38.StylePriority.UseFont = False
        Me.XrLabel38.StylePriority.UseTextAlignment = False
        Me.XrLabel38.Text = "$99,999.00"
        Me.XrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel38.TextFormatString = "{0:N0}"
        '
        'XrLabel39
        '
        Me.XrLabel39.CanGrow = False
        Me.XrLabel39.CanShrink = True
        Me.XrLabel39.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio2]")})
        Me.XrLabel39.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel39.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 209.6565!)
        Me.XrLabel39.Multiline = True
        Me.XrLabel39.Name = "XrLabel39"
        Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel39.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel39.StylePriority.UseFont = False
        Me.XrLabel39.StylePriority.UseTextAlignment = False
        Me.XrLabel39.Text = "$99,999.00"
        Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel39.TextFormatString = "{0:$0}"
        '
        'XrLabel40
        '
        Me.XrLabel40.CanGrow = False
        Me.XrLabel40.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Marca]")})
        Me.XrLabel40.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(461.6713!, 240.2083!)
        Me.XrLabel40.Name = "XrLabel40"
        Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel40.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel40.StylePriority.UseFont = False
        Me.XrLabel40.Text = "Marca:"
        '
        'XrLabel41
        '
        Me.XrLabel41.CanGrow = False
        Me.XrLabel41.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Estilo]")})
        Me.XrLabel41.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(403.0311!, 240.2083!)
        Me.XrLabel41.Name = "XrLabel41"
        Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel41.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel41.StylePriority.UseFont = False
        Me.XrLabel41.Text = "Estilo:"
        '
        'XrLabel42
        '
        Me.XrLabel42.CanGrow = False
        Me.XrLabel42.CanShrink = True
        Me.XrLabel42.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio3]")})
        Me.XrLabel42.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel42.LocationFloat = New DevExpress.Utils.PointFloat(492.6035!, 166.0025!)
        Me.XrLabel42.Multiline = True
        Me.XrLabel42.Name = "XrLabel42"
        Me.XrLabel42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel42.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel42.StylePriority.UseFont = False
        Me.XrLabel42.StylePriority.UseTextAlignment = False
        Me.XrLabel42.Text = "$99,999.00"
        Me.XrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel42.TextFormatString = "{0:N0}"
        '
        'XrLabel43
        '
        Me.XrLabel43.CanGrow = False
        Me.XrLabel43.CanShrink = True
        Me.XrLabel43.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R2Precio1]")})
        Me.XrLabel43.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel43.LocationFloat = New DevExpress.Utils.PointFloat(596.7283!, 199.2785!)
        Me.XrLabel43.Multiline = True
        Me.XrLabel43.Name = "XrLabel43"
        Me.XrLabel43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel43.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel43.StylePriority.UseFont = False
        Me.XrLabel43.StylePriority.UseTextAlignment = False
        Me.XrLabel43.Text = "$99,999.00"
        Me.XrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel43.TextFormatString = "{0:$0}"
        '
        'XrLabel44
        '
        Me.XrLabel44.CanGrow = False
        Me.XrLabel44.CanShrink = True
        Me.XrLabel44.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R2Precio5]")})
        Me.XrLabel44.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel44.LocationFloat = New DevExpress.Utils.PointFloat(623.1028!, 166.0025!)
        Me.XrLabel44.Multiline = True
        Me.XrLabel44.Name = "XrLabel44"
        Me.XrLabel44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel44.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel44.StylePriority.UseFont = False
        Me.XrLabel44.StylePriority.UseTextAlignment = False
        Me.XrLabel44.Text = "$99,999.00"
        Me.XrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel44.TextFormatString = "{0:N0}"
        '
        'XrLabel45
        '
        Me.XrLabel45.CanGrow = False
        Me.XrLabel45.CanShrink = True
        Me.XrLabel45.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R2Precio2]")})
        Me.XrLabel45.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel45.LocationFloat = New DevExpress.Utils.PointFloat(596.7283!, 209.6565!)
        Me.XrLabel45.Multiline = True
        Me.XrLabel45.Name = "XrLabel45"
        Me.XrLabel45.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel45.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel45.StylePriority.UseFont = False
        Me.XrLabel45.StylePriority.UseTextAlignment = False
        Me.XrLabel45.Text = "$99,999.00"
        Me.XrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel45.TextFormatString = "{0:$0}"
        '
        'XrLabel46
        '
        Me.XrLabel46.CanGrow = False
        Me.XrLabel46.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R2Marca]")})
        Me.XrLabel46.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel46.LocationFloat = New DevExpress.Utils.PointFloat(658.1869!, 240.2083!)
        Me.XrLabel46.Name = "XrLabel46"
        Me.XrLabel46.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel46.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel46.StylePriority.UseFont = False
        Me.XrLabel46.Text = "Marca:"
        '
        'XrLabel47
        '
        Me.XrLabel47.CanGrow = False
        Me.XrLabel47.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R2Estilo]")})
        Me.XrLabel47.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel47.LocationFloat = New DevExpress.Utils.PointFloat(599.5466!, 240.2083!)
        Me.XrLabel47.Name = "XrLabel47"
        Me.XrLabel47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel47.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel47.StylePriority.UseFont = False
        Me.XrLabel47.Text = "Estilo:"
        '
        'XrLabel48
        '
        Me.XrLabel48.CanGrow = False
        Me.XrLabel48.CanShrink = True
        Me.XrLabel48.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R2Precio3]")})
        Me.XrLabel48.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel48.LocationFloat = New DevExpress.Utils.PointFloat(689.119!, 166.0025!)
        Me.XrLabel48.Multiline = True
        Me.XrLabel48.Name = "XrLabel48"
        Me.XrLabel48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel48.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel48.StylePriority.UseFont = False
        Me.XrLabel48.StylePriority.UseTextAlignment = False
        Me.XrLabel48.Text = "$99,999.00"
        Me.XrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel48.TextFormatString = "{0:N0}"
        '
        'XrLabel19
        '
        Me.XrLabel19.CanGrow = False
        Me.XrLabel19.CanShrink = True
        Me.XrLabel19.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R1Precio3]")})
        Me.XrLabel19.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(689.119!, 32.39834!)
        Me.XrLabel19.Multiline = True
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "$99,999.00"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel19.TextFormatString = "{0:N0}"
        '
        'XrLabel20
        '
        Me.XrLabel20.CanGrow = False
        Me.XrLabel20.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R1Estilo]")})
        Me.XrLabel20.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(599.5466!, 111.6041!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.Text = "Estilo:"
        '
        'XrLabel21
        '
        Me.XrLabel21.CanGrow = False
        Me.XrLabel21.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R1Marca]")})
        Me.XrLabel21.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(658.1869!, 111.6041!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.Text = "Marca:"
        '
        'XrLabel22
        '
        Me.XrLabel22.CanGrow = False
        Me.XrLabel22.CanShrink = True
        Me.XrLabel22.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R1Precio2]")})
        Me.XrLabel22.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(596.7283!, 76.05235!)
        Me.XrLabel22.Multiline = True
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.StylePriority.UseTextAlignment = False
        Me.XrLabel22.Text = "$99,999.00"
        Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel22.TextFormatString = "{0:$0}"
        '
        'XrLabel23
        '
        Me.XrLabel23.CanGrow = False
        Me.XrLabel23.CanShrink = True
        Me.XrLabel23.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R1Precio5]")})
        Me.XrLabel23.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(623.1028!, 32.39834!)
        Me.XrLabel23.Multiline = True
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel23.StylePriority.UseFont = False
        Me.XrLabel23.StylePriority.UseTextAlignment = False
        Me.XrLabel23.Text = "$99,999.00"
        Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel23.TextFormatString = "{0:N0}"
        '
        'XrLabel24
        '
        Me.XrLabel24.CanGrow = False
        Me.XrLabel24.CanShrink = True
        Me.XrLabel24.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaD].[R1Precio1]")})
        Me.XrLabel24.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(596.7283!, 65.67433!)
        Me.XrLabel24.Multiline = True
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.Text = "$99,999.00"
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel24.TextFormatString = "{0:$0}"
        '
        'XrLabel12
        '
        Me.XrLabel12.CanGrow = False
        Me.XrLabel12.CanShrink = True
        Me.XrLabel12.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio3]")})
        Me.XrLabel12.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(492.6035!, 32.39834!)
        Me.XrLabel12.Multiline = True
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "$99,999.00"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel12.TextFormatString = "{0:N0}"
        '
        'XrLabel13
        '
        Me.XrLabel13.CanGrow = False
        Me.XrLabel13.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Estilo]")})
        Me.XrLabel13.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(403.0311!, 111.6041!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.Text = "Estilo:"
        '
        'XrLabel14
        '
        Me.XrLabel14.CanGrow = False
        Me.XrLabel14.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Marca]")})
        Me.XrLabel14.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(461.6713!, 111.6041!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.Text = "Marca:"
        '
        'XrLabel15
        '
        Me.XrLabel15.CanGrow = False
        Me.XrLabel15.CanShrink = True
        Me.XrLabel15.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio2]")})
        Me.XrLabel15.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 76.05235!)
        Me.XrLabel15.Multiline = True
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "$99,999.00"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel15.TextFormatString = "{0:$0}"
        '
        'XrLabel17
        '
        Me.XrLabel17.CanGrow = False
        Me.XrLabel17.CanShrink = True
        Me.XrLabel17.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio5]")})
        Me.XrLabel17.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(426.5872!, 32.39834!)
        Me.XrLabel17.Multiline = True
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.Text = "$99,999.00"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel17.TextFormatString = "{0:N0}"
        '
        'XrLabel18
        '
        Me.XrLabel18.CanGrow = False
        Me.XrLabel18.CanShrink = True
        Me.XrLabel18.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio1]")})
        Me.XrLabel18.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(400.2127!, 65.67433!)
        Me.XrLabel18.Multiline = True
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "$99,999.00"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel18.TextFormatString = "{0:$0}"
        '
        'XrLabel6
        '
        Me.XrLabel6.CanGrow = False
        Me.XrLabel6.CanShrink = True
        Me.XrLabel6.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio1]")})
        Me.XrLabel6.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 65.6743!)
        Me.XrLabel6.Multiline = True
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "$99,999.00"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel6.TextFormatString = "{0:$0}"
        '
        'XrLabel7
        '
        Me.XrLabel7.CanGrow = False
        Me.XrLabel7.CanShrink = True
        Me.XrLabel7.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio5]")})
        Me.XrLabel7.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(230.0718!, 32.39831!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "$99,999.00"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel7.TextFormatString = "{0:N0}"
        '
        'XrLabel8
        '
        Me.XrLabel8.CanGrow = False
        Me.XrLabel8.CanShrink = True
        Me.XrLabel8.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio2]")})
        Me.XrLabel8.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(203.6972!, 76.05234!)
        Me.XrLabel8.Multiline = True
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "$99,999.00"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel8.TextFormatString = "{0:$0}"
        '
        'XrLabel9
        '
        Me.XrLabel9.CanGrow = False
        Me.XrLabel9.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Marca]")})
        Me.XrLabel9.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(265.1558!, 111.6041!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.Text = "Marca:"
        '
        'XrLabel10
        '
        Me.XrLabel10.CanGrow = False
        Me.XrLabel10.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Estilo]")})
        Me.XrLabel10.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(206.5156!, 111.6041!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.Text = "Estilo:"
        '
        'XrLabel11
        '
        Me.XrLabel11.CanGrow = False
        Me.XrLabel11.CanShrink = True
        Me.XrLabel11.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio3]")})
        Me.XrLabel11.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(296.088!, 32.39831!)
        Me.XrLabel11.Multiline = True
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "$99,999.00"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel11.TextFormatString = "{0:N0}"
        '
        'XrLabel16
        '
        Me.XrLabel16.CanGrow = False
        Me.XrLabel16.CanShrink = True
        Me.XrLabel16.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio3]")})
        Me.XrLabel16.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 18.0!)
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(99.5725!, 32.3983!)
        Me.XrLabel16.Multiline = True
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(96.94309!, 43.27596!)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "$99,999.00"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel16.TextFormatString = "{0:N0}"
        '
        'XrLabel2
        '
        Me.XrLabel2.CanGrow = False
        Me.XrLabel2.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Estilo]")})
        Me.XrLabel2.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 111.6041!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "Estilo:"
        '
        'XrLabel3
        '
        Me.XrLabel3.CanGrow = False
        Me.XrLabel3.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Marca]")})
        Me.XrLabel3.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(68.6402!, 111.6041!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(57.91666!, 10.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "Marca:"
        '
        'XrLabel5
        '
        Me.XrLabel5.CanGrow = False
        Me.XrLabel5.CanShrink = True
        Me.XrLabel5.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio2]")})
        Me.XrLabel5.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(7.181612!, 76.05231!)
        Me.XrLabel5.Multiline = True
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "$99,999.00"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel5.TextFormatString = "{0:$0}"
        '
        'XrLabel1
        '
        Me.XrLabel1.CanGrow = False
        Me.XrLabel1.CanShrink = True
        Me.XrLabel1.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio5]")})
        Me.XrLabel1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 14.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(33.55615!, 32.3983!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(44.54238!, 33.27592!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "$99,999.00"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel1.TextFormatString = "{0:N0}"
        '
        'XrLabel4
        '
        Me.XrLabel4.CanGrow = False
        Me.XrLabel4.CanShrink = True
        Me.XrLabel4.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio1]")})
        Me.XrLabel4.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(7.181612!, 65.67426!)
        Me.XrLabel4.Multiline = True
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(50.0!, 10.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "$99,999.00"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel4.TextFormatString = "{0:$0}"
        '
        'Pic1
        '
        Me.Pic1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.Pic1.Name = "Pic1"
        Me.Pic1.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(196.5155!, 0.0000112197!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox3
        '
        Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(589.5466!, 0.00003365909!)
        Me.XrPictureBox3.Name = "XrPictureBox3"
        Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox2
        '
        Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(393.0311!, 0.00003365909!)
        Me.XrPictureBox2.Name = "XrPictureBox2"
        Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox5
        '
        Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(589.5466!, 127.6042!)
        Me.XrPictureBox5.Name = "XrPictureBox5"
        Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox6
        '
        Me.XrPictureBox6.LocationFloat = New DevExpress.Utils.PointFloat(196.5155!, 127.6042!)
        Me.XrPictureBox6.Name = "XrPictureBox6"
        Me.XrPictureBox6.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox4
        '
        Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(393.0311!, 127.6042!)
        Me.XrPictureBox4.Name = "XrPictureBox4"
        Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox7
        '
        Me.XrPictureBox7.LocationFloat = New DevExpress.Utils.PointFloat(0!, 127.6041!)
        Me.XrPictureBox7.Name = "XrPictureBox7"
        Me.XrPictureBox7.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox10
        '
        Me.XrPictureBox10.LocationFloat = New DevExpress.Utils.PointFloat(196.5155!, 255.2083!)
        Me.XrPictureBox10.Name = "XrPictureBox10"
        Me.XrPictureBox10.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox11
        '
        Me.XrPictureBox11.LocationFloat = New DevExpress.Utils.PointFloat(0!, 255.2083!)
        Me.XrPictureBox11.Name = "XrPictureBox11"
        Me.XrPictureBox11.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox8
        '
        Me.XrPictureBox8.LocationFloat = New DevExpress.Utils.PointFloat(393.0309!, 255.2083!)
        Me.XrPictureBox8.Name = "XrPictureBox8"
        Me.XrPictureBox8.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox9
        '
        Me.XrPictureBox9.LocationFloat = New DevExpress.Utils.PointFloat(589.5466!, 255.2083!)
        Me.XrPictureBox9.Name = "XrPictureBox9"
        Me.XrPictureBox9.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox13
        '
        Me.XrPictureBox13.LocationFloat = New DevExpress.Utils.PointFloat(589.5466!, 382.8124!)
        Me.XrPictureBox13.Name = "XrPictureBox13"
        Me.XrPictureBox13.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox14
        '
        Me.XrPictureBox14.LocationFloat = New DevExpress.Utils.PointFloat(196.5155!, 382.8123!)
        Me.XrPictureBox14.Name = "XrPictureBox14"
        Me.XrPictureBox14.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox12
        '
        Me.XrPictureBox12.LocationFloat = New DevExpress.Utils.PointFloat(393.0311!, 382.8124!)
        Me.XrPictureBox12.Name = "XrPictureBox12"
        Me.XrPictureBox12.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox15
        '
        Me.XrPictureBox15.LocationFloat = New DevExpress.Utils.PointFloat(0!, 382.8123!)
        Me.XrPictureBox15.Name = "XrPictureBox15"
        Me.XrPictureBox15.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox19
        '
        Me.XrPictureBox19.LocationFloat = New DevExpress.Utils.PointFloat(0!, 510.4164!)
        Me.XrPictureBox19.Name = "XrPictureBox19"
        Me.XrPictureBox19.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox18
        '
        Me.XrPictureBox18.LocationFloat = New DevExpress.Utils.PointFloat(196.5155!, 510.4164!)
        Me.XrPictureBox18.Name = "XrPictureBox18"
        Me.XrPictureBox18.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox16
        '
        Me.XrPictureBox16.LocationFloat = New DevExpress.Utils.PointFloat(393.0311!, 510.4164!)
        Me.XrPictureBox16.Name = "XrPictureBox16"
        Me.XrPictureBox16.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox17
        '
        Me.XrPictureBox17.LocationFloat = New DevExpress.Utils.PointFloat(589.5466!, 510.4164!)
        Me.XrPictureBox17.Name = "XrPictureBox17"
        Me.XrPictureBox17.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox23
        '
        Me.XrPictureBox23.LocationFloat = New DevExpress.Utils.PointFloat(0!, 638.0205!)
        Me.XrPictureBox23.Name = "XrPictureBox23"
        Me.XrPictureBox23.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox22
        '
        Me.XrPictureBox22.LocationFloat = New DevExpress.Utils.PointFloat(196.5153!, 638.0205!)
        Me.XrPictureBox22.Name = "XrPictureBox22"
        Me.XrPictureBox22.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox21
        '
        Me.XrPictureBox21.LocationFloat = New DevExpress.Utils.PointFloat(589.5464!, 638.0205!)
        Me.XrPictureBox21.Name = "XrPictureBox21"
        Me.XrPictureBox21.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox20
        '
        Me.XrPictureBox20.LocationFloat = New DevExpress.Utils.PointFloat(393.0309!, 638.0205!)
        Me.XrPictureBox20.Name = "XrPictureBox20"
        Me.XrPictureBox20.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox27
        '
        Me.XrPictureBox27.LocationFloat = New DevExpress.Utils.PointFloat(0!, 765.6246!)
        Me.XrPictureBox27.Name = "XrPictureBox27"
        Me.XrPictureBox27.SizeF = New System.Drawing.SizeF(196.5157!, 127.6042!)
        '
        'XrPictureBox26
        '
        Me.XrPictureBox26.LocationFloat = New DevExpress.Utils.PointFloat(196.5153!, 765.6246!)
        Me.XrPictureBox26.Name = "XrPictureBox26"
        Me.XrPictureBox26.SizeF = New System.Drawing.SizeF(196.5157!, 127.6042!)
        '
        'XrPictureBox25
        '
        Me.XrPictureBox25.LocationFloat = New DevExpress.Utils.PointFloat(589.5462!, 765.6246!)
        Me.XrPictureBox25.Name = "XrPictureBox25"
        Me.XrPictureBox25.SizeF = New System.Drawing.SizeF(196.5157!, 127.6042!)
        '
        'XrPictureBox24
        '
        Me.XrPictureBox24.LocationFloat = New DevExpress.Utils.PointFloat(393.0308!, 765.6246!)
        Me.XrPictureBox24.Name = "XrPictureBox24"
        Me.XrPictureBox24.SizeF = New System.Drawing.SizeF(196.5157!, 127.6042!)
        '
        'XrPictureBox31
        '
        Me.XrPictureBox31.LocationFloat = New DevExpress.Utils.PointFloat(0!, 893.2288!)
        Me.XrPictureBox31.Name = "XrPictureBox31"
        Me.XrPictureBox31.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox30
        '
        Me.XrPictureBox30.LocationFloat = New DevExpress.Utils.PointFloat(196.5152!, 893.2288!)
        Me.XrPictureBox30.Name = "XrPictureBox30"
        Me.XrPictureBox30.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox29
        '
        Me.XrPictureBox29.LocationFloat = New DevExpress.Utils.PointFloat(589.5462!, 893.2288!)
        Me.XrPictureBox29.Name = "XrPictureBox29"
        Me.XrPictureBox29.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox28
        '
        Me.XrPictureBox28.LocationFloat = New DevExpress.Utils.PointFloat(393.0307!, 893.2288!)
        Me.XrPictureBox28.Name = "XrPictureBox28"
        Me.XrPictureBox28.SizeF = New System.Drawing.SizeF(196.5157!, 127.6041!)
        '
        'XrPictureBox35
        '
        Me.XrPictureBox35.LocationFloat = New DevExpress.Utils.PointFloat(0!, 1020.833!)
        Me.XrPictureBox35.Name = "XrPictureBox35"
        Me.XrPictureBox35.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox34
        '
        Me.XrPictureBox34.LocationFloat = New DevExpress.Utils.PointFloat(196.5155!, 1020.833!)
        Me.XrPictureBox34.Name = "XrPictureBox34"
        Me.XrPictureBox34.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox32
        '
        Me.XrPictureBox32.LocationFloat = New DevExpress.Utils.PointFloat(393.0311!, 1020.833!)
        Me.XrPictureBox32.Name = "XrPictureBox32"
        Me.XrPictureBox32.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'XrPictureBox33
        '
        Me.XrPictureBox33.LocationFloat = New DevExpress.Utils.PointFloat(589.5466!, 1020.833!)
        Me.XrPictureBox33.Name = "XrPictureBox33"
        Me.XrPictureBox33.SizeF = New System.Drawing.SizeF(196.5157!, 127.604!)
        '
        'ods
        '
        Me.ods.DataSource = GetType(proyVOficina_PDV.entPreciador4)
        Me.ods.Name = "ods"
        '
        'XtraRep_PreciadorNA4X9
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.DetailReport})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.ods})
        Me.DataSource = Me.ods
        Me.Margins = New DevExpress.Drawing.DXMargins(24.0!, 29.0!, 12.5!, 12.99998!)
        Me.Version = "22.2"
        CType(Me.ods, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents ods As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
    Friend WithEvents DetailReport As DevExpress.XtraReports.UI.DetailReportBand
    Friend WithEvents Detail1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents Pic1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel38 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel39 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel40 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel41 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel42 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel43 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel44 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel45 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel46 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel47 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel48 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox6 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox7 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox8 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox9 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox10 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox11 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel49 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel50 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel51 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel52 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel53 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel54 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel55 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel56 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel57 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel58 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel59 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel60 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel61 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel62 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel63 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel64 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel65 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel66 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel67 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel68 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel69 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel70 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel71 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel72 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox12 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox13 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox14 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox15 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel73 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel74 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel75 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel76 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel77 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel78 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel79 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel80 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel81 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel82 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel83 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel84 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel85 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel86 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel87 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel88 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel89 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel90 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel91 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel92 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel93 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel94 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel95 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel96 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox16 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox17 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox18 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox19 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel97 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel98 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel99 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel100 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel101 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel102 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel103 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel104 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel105 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel106 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel107 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel108 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel109 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel110 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel111 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel112 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel113 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel114 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel115 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel116 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel117 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel118 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel119 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel120 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel121 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel122 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel123 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel124 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel125 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel126 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel127 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel128 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel129 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel130 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel131 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel132 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel133 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel134 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel135 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel136 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel137 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel138 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel139 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel140 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel141 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel142 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel143 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel144 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox23 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox22 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox21 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox20 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox24 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox25 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox26 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox27 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel145 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel146 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel147 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel148 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel149 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel150 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel151 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel152 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel153 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel154 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel155 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel156 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel157 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel158 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel159 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel160 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel161 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel162 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel163 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel164 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel165 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel166 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel167 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel168 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel169 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel170 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel171 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel172 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel173 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel174 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel175 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel176 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel177 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel178 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel179 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel180 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel181 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel182 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel183 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel184 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel185 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel186 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel187 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel188 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel189 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel190 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel191 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel192 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox31 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox30 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox29 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox28 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel193 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel194 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel195 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel196 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel197 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel198 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel199 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel200 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel201 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel202 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel203 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel204 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel205 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel206 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel207 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel208 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel209 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel210 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel211 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel212 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel213 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel214 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel215 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel216 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox35 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox34 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox32 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox33 As DevExpress.XtraReports.UI.XRPictureBox
End Class
