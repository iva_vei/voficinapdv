﻿Public Class uCtrl_CheckBoxSize

    Private _Factor As Double = 1.0

    Public Property Factor As Double
        Get
            Return _Factor
        End Get
        Set(value As Double)
            _Factor = value
        End Set
    End Property

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)

        'Make the box you check 3/4 the height
        Dim boxsize As Integer = Me.Height * _Factor
        Dim rect As New Rectangle(
                New Point(0, Me.Height / 2 - boxsize / 2),
                New Size(boxsize, boxsize)
            )

        ControlPaint.DrawCheckBox(e.Graphics, rect, If(Me.Checked, ButtonState.Checked, ButtonState.Normal))
    End Sub
End Class
