﻿Imports System.Net.Http
Imports System.Text
Imports System.Threading.Tasks
Imports DevExpress.DataAccess.Native.Web

Public Class ApiClient
    Public Url As String
    Public AutMetodo As String
    Public AutToken As String
    Public JsonContent As String

    Public RepuestaData As String
    Public RepuestaCode As String

    Private Shared ReadOnly HttpClient As New HttpClient()

    ' Función para llamar a la API
    Public Function CallApi() As Boolean
        Dim Resultado As Boolean = False
        Try
            ' Crear el contenido JSON
            Dim content As New StringContent(JsonContent, Encoding.UTF8, "application/json")

            ' Configurar el encabezado de autorización
            If AutMetodo.ToLower = "bearer" Then
                HttpClient.DefaultRequestHeaders.Authorization = New Net.Http.Headers.AuthenticationHeaderValue("Bearer", AutToken)
            End If
            If AutMetodo = "x-api-key" Then
                HttpClient.DefaultRequestHeaders.Add("X-API-Key", AutToken)
            End If
            ' Enviar la solicitud POST
            Dim response As HttpResponseMessage = HttpClient.PostAsync(Url, content).Result

            ' Verificar si la respuesta es exitosa (código 200)
            If response.IsSuccessStatusCode Then
                ' Leer y devolver la respuesta como cadena
                Dim responseData As String = response.Content.ReadAsStringAsync().Result
                RepuestaCode = response.StatusCode
                RepuestaData = responseData
                Resultado = True
            Else
                RepuestaCode = response.StatusCode
                RepuestaData = response.ReasonPhrase
                Resultado = False
                ' Manejar otros códigos de estado
                ''Throw New Exception($"Error en la solicitud: {response.StatusCode} - {response.ReasonPhrase}")
            End If
        Catch ex As Exception
            ' Manejar excepciones
            Resultado = False
            RepuestaCode = -1
            RepuestaData = "Error al ejecutar API: " & ex.Message
        End Try
        Return Resultado
    End Function
End Class