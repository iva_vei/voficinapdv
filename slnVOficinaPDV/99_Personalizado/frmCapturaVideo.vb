﻿Imports AForge.Video
Imports AForge.Video.DirectShow
Imports System.IO
Imports System
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports System.Diagnostics
Imports System.Data.SqlClient

Public Class frmCapturaVideo

    Private WithEvents cam As New DSCamCapture
    Dim MyPicturesFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
    Public PathImagen As String = ""

    Dim _Cliente As String

    Public Property Cliente As String
        Get
            Return _Cliente
        End Get
        Set(value As String)
            _Cliente = value
        End Set
    End Property

    Private videoDevices As FilterInfoCollection
    Private videoSource As VideoCaptureDevice
    Private ffmpegProcess As Process
    Private videoFilePath As String = Application.StartupPath & "\Videos\grabacion.mp4" ' Ruta donde se guardará el video
    Private ffmpegPath As String = Application.StartupPath & "\ffmpeg.exe" ' Ruta de FFmpeg
    Private Empleado As String

    Private Sub frmCapturaVideo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadVideoDevices()

        ComboBox_Devices.Items.AddRange(cam.GetCaptureDevices)
        If ComboBox_Devices.Items.Count > 0 Then ComboBox_Devices.SelectedIndex = 0

        ComboBox_Audio.Items.AddRange(cam.GetCaptureDevicesAudio)
        If ComboBox_Audio.Items.Count > 0 Then ComboBox_Audio.SelectedIndex = 0


    End Sub

    Private Sub ComboBox_Audio_DropDown(sender As Object, e As EventArgs) Handles ComboBox_Audio.DropDown

        ComboBox_Audio.Items.Clear()
        ComboBox_Audio.Items.AddRange(cam.GetCaptureDevicesAudio)
        If ComboBox_Audio.SelectedIndex = -1 And ComboBox_Audio.Items.Count > 0 Then ComboBox_Audio.SelectedIndex = 0

    End Sub

    Private Sub ComboBox_Devices_DropDown(sender As Object, e As EventArgs) Handles ComboBox_Devices.DropDown

        ComboBox_Devices.Items.Clear()
        ComboBox_Devices.Items.AddRange(cam.GetCaptureDevices)
        If ComboBox_Devices.SelectedIndex = -1 And ComboBox_Devices.Items.Count > 0 Then ComboBox_Devices.SelectedIndex = 0

    End Sub


    Private Sub LoadVideoDevices()
        videoDevices = New FilterInfoCollection(FilterCategory.VideoInputDevice)
        If videoDevices.Count > 0 Then
            videoSource = New VideoCaptureDevice(videoDevices(0).MonikerString)
            AddHandler videoSource.NewFrame, AddressOf VideoSource_NewFrame
        Else
            MessageBox.Show("No se encontraron cámaras.")
        End If
    End Sub

    Private Sub VideoSource_NewFrame(sender As Object, eventArgs As NewFrameEventArgs)
        pbVideo.Image = DirectCast(eventArgs.Frame.Clone(), Bitmap)
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        If Not File.Exists(ffmpegPath) Then
            MessageBox.Show("FFmpeg no encontrado en la ruta especificada.")
            Exit Sub
        End If

        StartRecording()
    End Sub

    Private Sub StartRecording()
        'Try
        '    Dim args As String = "-f dshow -i video=""Integrated Camera"" -c:v libx264 -preset ultrafast -t 10 -movflags +faststart """ & videoFilePath & """"
        '    ''ffmpeg -f dshow -i video="Nombre de tu cámara" -c:v libx264 -preset ultrafast -t 10 -movflags +faststart C:\Videos\grabacion.mp4
        '    ffmpegProcess = New Process()
        '    ffmpegProcess.StartInfo.FileName = ffmpegPath
        '    ffmpegProcess.StartInfo.Arguments = args
        '    ffmpegProcess.StartInfo.RedirectStandardError = True ' Capturar errores
        '    ffmpegProcess.StartInfo.UseShellExecute = False
        '    ffmpegProcess.StartInfo.CreateNoWindow = True
        '    ffmpegProcess.Start()

        '    MessageBox.Show("Grabación iniciada.")
        'Catch ex As Exception
        '    MessageBox.Show("Error al iniciar la grabación: " & ex.Message)
        'End Try

        Try

            Dim selectedText As String = ""

            If ComboBox_Devices.SelectedItem IsNot Nothing Then
                selectedText = ComboBox_Devices.SelectedItem.ToString()
            End If

            Dim selectedAudio As String = ""

            If ComboBox_Audio.SelectedItem IsNot Nothing Then
                selectedAudio = ComboBox_Audio.SelectedItem.ToString()
            End If



            Dim ffmpegPath As String = Application.StartupPath & "\ffmpeg.exe" ' Cambia esto con la ruta correcta
            Dim videoPath As String = Application.StartupPath & "\Videos\" + Cliente + ".mp4"
            Dim cameraName As String = selectedText
            Dim audioName As String = selectedAudio


            '"Integrated Camera" ' Verificar con ffmpeg -list_devices true
            ''                          -f dshow -i video="Nombre de tu cámara" -c:v libx264 -preset ultrafast -t 10 -movflags +faststart C:\Videos\grabacion.mp4
            ' Comando para grabar video
            ''        Dim arguments As String = $"-f dshow -i video=""{cameraName}"" -c:v mpeg4 -preset ultrafast -t 10 -movflags +faststart ""{videoPath}"""

            If Not Directory.Exists(Application.StartupPath & "\Videos") Then
                Directory.CreateDirectory(Application.StartupPath & "\Videos")
            End If

            If File.Exists(videoPath) Then
                Try
                    File.Delete(videoPath)
                    'MessageBox.Show("📌 Archivo anterior eliminado.")
                Catch ex As Exception
                    MessageBox.Show("❌ No se pudo eliminar el archivo: " & ex.Message)
                    Exit Sub ' Detiene la ejecución si no se pudo eliminar
                End Try
            End If




            'Dim arguments As String = $"-f dshow -rtbufsize 100M -i video=""{cameraName}"" -f dshow -i audio=""{audioName}"" -c:v libx264 -preset ultrafast -crf 23 -c:a aac -b:a 128k -t 10 -movflags +faststart ""{videoPath}"""
            Dim arguments As String = $"-f dshow -i video=""{cameraName}"" -f dshow -i audio=""{audioName}"" -c:v mpeg4 -q:v 5 -t 10 -movflags +faststart ""{videoPath}"""

            'Dim arguments As String = $"-f dshow -i video=""{cameraName}"" -f dshow -i audio=""{audioName}"" -c:v mpeg4 -q:v 5 -t 10 -movflags +faststart ""{videoPath}"""


            Dim process As New Process()
            process.StartInfo.FileName = ffmpegPath
            process.StartInfo.Arguments = arguments
            process.StartInfo.RedirectStandardOutput = True
            process.StartInfo.RedirectStandardError = True
            process.StartInfo.UseShellExecute = False
            process.StartInfo.CreateNoWindow = True


            process.Start()

            ' Leer errores de FFmpeg
            Dim output As String = process.StandardError.ReadToEnd()

            process.WaitForExit()

            If process.ExitCode = 0 Then
                SaveVideoToDatabase(videoPath)
                MessageBox.Show("✅ Video grabado correctamente en: " & videoPath)
                btnValidar.Enabled = True
            Else
                btnValidar.Enabled = False
                MessageBox.Show("❌ Error en FFmpeg: " & output)
            End If

        Catch ex As Exception
            btnValidar.Enabled = False
            MessageBox.Show("❌ Error al ejecutar FFmpeg: " & ex.Message)
        End Try

    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        StopRecording()
    End Sub

    Private Sub StopRecording()
        Try
            If ffmpegProcess IsNot Nothing AndAlso Not ffmpegProcess.HasExited Then
                ffmpegProcess.Kill()
                ffmpegProcess.Dispose()
                MessageBox.Show("Grabación detenida y guardada en: " & videoFilePath)
                SaveVideoToDatabase(videoFilePath)
            End If
        Catch ex As Exception
            MessageBox.Show("Error al detener la grabación: " & ex.Message)
        End Try
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If videoSource IsNot Nothing AndAlso videoSource.IsRunning Then
            videoSource.SignalToStop()
        End If
        StopRecording()
    End Sub

    Public Sub SaveVideoToDatabase(filePath As String)

        Dim oDatos As Datos_Viscoi
        Try
            oDatos = New Datos_Viscoi
            Dim videoData As Byte() = File.ReadAllBytes(filePath)

            If oDatos.Inserta_VideoApp(Globales.oAmbientes.Id_Empresa, "clientes-final-video-pvida", Cliente, 1, Today, Globales.oAmbientes.oUsuario.Id_usuario, videoData, "Msj") Then
                ''Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", txt_IdClienteFinal.Text, 0, PictureBox1)
            End If

            MessageBox.Show("Video guardado en la base de datos con éxito.")
        Catch ex As Exception
            MessageBox.Show("Error al guardar el video: " & ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnValidar.Click
        LoadVideoFromDatabase(1) ' Carga el video con ID = 1
    End Sub

    Public Sub LoadVideoFromDatabase(videoId As Integer)
        Dim oDatos As Datos_Viscoi
        Try
            oDatos = New Datos_Viscoi

            Dim videoData As Byte() = oDatos.Recupera_VideoApp(Globales.oAmbientes.Id_Empresa, "clientes-final-video-pvida", Cliente, "1")

            If videoData IsNot Nothing Then
                Dim tempPath As String = Path.Combine(Path.GetTempPath(), "video_recuperado.mp4")
                File.WriteAllBytes(tempPath, videoData)

                ' Reproducir el video con Windows Media Player
                Process.Start(tempPath)
            End If

        Catch ex As Exception
            MessageBox.Show("Error al recuperar el video: " & ex.Message)
        End Try
    End Sub

End Class