﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class uctrl_Estructura
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Estructura = New DevExpress.XtraEditors.SimpleButton()
#Disable Warning BC42025 ' Access of shared member, constant member, enum member or nested type through an instance
        Me.mem_Estructura = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.mem_Estructura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Estructura
        '
        Me.btn_Estructura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btn_Estructura.Location = New System.Drawing.Point(0, 0)
        Me.btn_Estructura.Name = "btn_Estructura"
        Me.btn_Estructura.Size = New System.Drawing.Size(64, 64)
        Me.btn_Estructura.TabIndex = 0
        Me.btn_Estructura.Text = "Familias"
        '
        'mem_Estructura
        '
        Me.mem_Estructura.Dock = System.Windows.Forms.DockStyle.Right
        Me.mem_Estructura.Location = New System.Drawing.Point(64, 0)
        Me.mem_Estructura.Name = "mem_Estructura"
        Me.mem_Estructura.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mem_Estructura.Properties.Appearance.Options.UseFont = True
        Me.mem_Estructura.Properties.ReadOnly = True
        Me.mem_Estructura.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.mem_Estructura.Properties.UseReadOnlyAppearance = False
        Me.mem_Estructura.Size = New System.Drawing.Size(192, 64)
        Me.mem_Estructura.TabIndex = 1
        '
        'uctrl_Estructura
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.Controls.Add(Me.btn_Estructura)
        Me.Controls.Add(Me.mem_Estructura)
        Me.Name = "uctrl_Estructura"
        Me.Size = New System.Drawing.Size(256, 64)
        CType(Me.mem_Estructura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
#Enable Warning BC42025 ' Access of shared member, constant member, enum member or nested type through an instance
    End Sub

    Friend WithEvents btn_Estructura As DevExpress.XtraEditors.SimpleButton
    Public Shared WithEvents mem_Estructura As DevExpress.XtraEditors.MemoEdit
End Class
