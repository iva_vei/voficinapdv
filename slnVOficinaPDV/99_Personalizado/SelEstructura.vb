﻿Public Class SelEstructura
    Public id_division As Integer
    Public id_depto As Integer
    Public id_familia As Integer
    Public division As String
    Public depto As String
    Public familia As String
    Public id_estructura As Integer
    Public Estructura1 As String = ""
    Private Sub SelEstructura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        division = ""
        depto = ""
        familia = ""
        Call Mostrar_Arbol(TreeList1)
    End Sub
    Private Sub Mostrar_Arbol(ByVal GridArbol As DevExpress.XtraTreeList.TreeList)
        Dim oDatos_Viscoi As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtEstructura As DataTable = Nothing
        Dim oNodo As oRenNodo
        Dim Mensaje As String = ""
        Try
            oDatos_Viscoi = New Datos_Viscoi
            GridArbol.ClearNodes()
            If oDatos_Viscoi.Estructura_SEL(dtEstructura, Mensaje) Then
                For Each oRen As DataRow In dtEstructura.Rows
                    oNodo = New oRenNodo
                    oNodo.IdDivision = oRen.Item("id_division")
                    oNodo.Division = oRen.Item("division")
                    oNodo.IdDepto = oRen.Item("id_depto")
                    oNodo.Depto = oRen.Item("depto")
                    oNodo.IdFamilia = oRen.Item("id_familia")
                    oNodo.Familia = oRen.Item("familia")
                    oNodo.Id_Estructura = oRen.Item("id_estructura")
                    oNodo.Estructura = oRen.Item("estructura")
                    oNodo.Estructura1 = oRen.Item("estructura1")
                    oNodo.iNivel = oRen.Item("Nivel")
                    GridArbol.AppendNode(New Object() {oNodo.IdDivision, oNodo.Division _
                                                     , oNodo.IdDepto, oNodo.Depto _
                                                     , oNodo.IdFamilia, oNodo.Familia _
                                                     , oNodo.iNivel _
                                                     , oNodo.Id_Estructura _
                                                     , oNodo.Estructura _
                                                     , oNodo.Estructura1
                                                     }, oRen.Item("id"))
                Next
                GridArbol.BestFitColumns()
            Else
                GridArbol.ClearNodes()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Class oRenNodo
        Public IdDivision As Integer = 0
        Public Division As String = ""
        Public IdDepto As Integer = 0
        Public Depto As String = ""
        Public IdFamilia As Integer = 0
        Public Familia As String = ""
        Public iNivel As Integer = 0
        Public Id_Estructura As Integer = 0
        Public Estructura As String = ""
        Public Estructura1 As String = ""
        'Public ReadOnly Property Estructura
        '    Get
        '        Dim nombre As String = ""
        '        If iNivel = 1 Then nombre = Format(IdJer01, "000") & " - " & NmJer01
        '        If iNivel = 2 Then nombre = Format(IdJer02, "000") & " - " & NmJer02
        '        If iNivel = 3 Then nombre = Format(IdJer03, "000") & " - " & NmJer03
        '        If iNivel = 4 Then nombre = Format(IdJer04, "000") & " - " & NmJer04
        '        If iNivel = 5 Then nombre = Format(IdJer05, "000") & " - " & NmJer05
        '        If iNivel = 6 Then nombre = Format(IdJer06, "000") & " - " & NmJer06
        '        If iNivel = 7 Then nombre = Format(IdJer07, "000") & " - " & NmJer07
        '        If iNivel = 8 Then nombre = Format(IdJer08, "000") & " - " & NmJer08
        '        If iNivel = 9 Then nombre = Format(IdJer09, "000") & " - " & NmJer09
        '        If iNivel = 10 Then nombre = Format(IdJer10, "000") & " - " & NmJer10
        '        Return nombre
        '    End Get
        'End Property
    End Class
    Private Sub ControlBotones(ByVal tag As String)
        Try
            Select Case tag
                Case "Ok"
                    id_division = TreeList1.Selection(0).GetValue(0)
                    id_depto = TreeList1.Selection(0).GetValue(2)
                    id_familia = TreeList1.Selection(0).GetValue(4)

                    division = TreeList1.Selection(0).GetValue(1)
                    depto = TreeList1.Selection(0).GetValue(3)
                    familia = TreeList1.Selection(0).GetValue(5)
                    id_estructura = 0
                    Estructura1 = TreeList1.Selection(0).GetValue(9)
                Case "Cancelar"
                    id_division = 0
                    id_depto = 0
                    id_familia = 0
                    id_estructura = 0
            End Select
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, DevExpress.XtraBars.Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption
            ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SelEstructura_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        Dim iNivel As Integer
        Try
            Select Case e.KeyCode
                Case Keys.Enter
                    ControlBotones("Ok")
                Case Keys.Space
                    iNivel = TreeList1.Selection(0).GetValue(6)

                    TreeList1.ExpandToLevel(iNivel)
                Case Keys.Escape
                    Estructura1 = ""
                    Me.Close()
            End Select
        Catch ex As Exception

        End Try
    End Sub
End Class