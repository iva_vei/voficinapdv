﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvClienteFinal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_Colonia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_CP = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Calle = New System.Windows.Forms.TextBox()
        Me.txt_Cliente = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_NumExt = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_NumInt = New System.Windows.Forms.TextBox()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txt_AMaterno = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txt_APaterno = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_Adicional = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Txt_CURP = New System.Windows.Forms.TextBox()
        Me.btn_Cliente = New DevExpress.XtraEditors.SimpleButton()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_EMail = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_Telefono2 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_Telefono = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.Cbo_Municipio = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Cbo_Estado = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btn_BuscarColonia = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Pais = New System.Windows.Forms.TextBox()
        Me.GC_Bancarios = New DevExpress.XtraEditors.GroupControl()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cbo_Bancos = New System.Windows.Forms.ComboBox()
        Me.Cbo_Banco_Tipo = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Txt_Banco_Cuenta = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Txt_Parentesco = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Txt_Beneficiario = New System.Windows.Forms.TextBox()
        Me.OFD1 = New System.Windows.Forms.OpenFileDialog()
        Me.PB_Bancarios = New System.Windows.Forms.PictureBox()
        CType(Me.txt_CP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GC_Bancarios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Bancarios.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.PB_Bancarios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(286, 655)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 2
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(509, 655)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 3
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(248, 212)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(230, 24)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Colonia"
        '
        'txt_Colonia
        '
        Me.txt_Colonia.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Colonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Colonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Colonia.Location = New System.Drawing.Point(246, 239)
        Me.txt_Colonia.Name = "txt_Colonia"
        Me.txt_Colonia.ReadOnly = True
        Me.txt_Colonia.Size = New System.Drawing.Size(232, 29)
        Me.txt_Colonia.TabIndex = 6
        Me.txt_Colonia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 210)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(241, 24)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "C. P."
        '
        'txt_CP
        '
        Me.txt_CP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_CP.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CP.Location = New System.Drawing.Point(5, 238)
        Me.txt_CP.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.txt_CP.Name = "txt_CP"
        Me.txt_CP.Size = New System.Drawing.Size(162, 29)
        Me.txt_CP.TabIndex = 5
        Me.txt_CP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(422, 24)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Calle"
        '
        'txt_Calle
        '
        Me.txt_Calle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Calle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Calle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Calle.Location = New System.Drawing.Point(5, 53)
        Me.txt_Calle.Name = "txt_Calle"
        Me.txt_Calle.Size = New System.Drawing.Size(473, 29)
        Me.txt_Calle.TabIndex = 0
        Me.txt_Calle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Cliente
        '
        Me.txt_Cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cliente.Location = New System.Drawing.Point(6, 50)
        Me.txt_Cliente.Name = "txt_Cliente"
        Me.txt_Cliente.ReadOnly = True
        Me.txt_Cliente.Size = New System.Drawing.Size(281, 29)
        Me.txt_Cliente.TabIndex = 0
        Me.txt_Cliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 86)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(204, 24)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Num Ext"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_NumExt
        '
        Me.txt_NumExt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_NumExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumExt.Location = New System.Drawing.Point(5, 117)
        Me.txt_NumExt.Name = "txt_NumExt"
        Me.txt_NumExt.Size = New System.Drawing.Size(232, 29)
        Me.txt_NumExt.TabIndex = 1
        Me.txt_NumExt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(223, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(204, 24)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Num Int"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_NumInt
        '
        Me.txt_NumInt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_NumInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumInt.Location = New System.Drawing.Point(246, 117)
        Me.txt_NumInt.Name = "txt_NumInt"
        Me.txt_NumInt.Size = New System.Drawing.Size(232, 29)
        Me.txt_NumInt.TabIndex = 2
        Me.txt_NumInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label22)
        Me.GroupControl1.Controls.Add(Me.txt_AMaterno)
        Me.GroupControl1.Controls.Add(Me.Label21)
        Me.GroupControl1.Controls.Add(Me.txt_APaterno)
        Me.GroupControl1.Controls.Add(Me.Label20)
        Me.GroupControl1.Controls.Add(Me.txt_Adicional)
        Me.GroupControl1.Controls.Add(Me.Label14)
        Me.GroupControl1.Controls.Add(Me.Txt_CURP)
        Me.GroupControl1.Controls.Add(Me.btn_Cliente)
        Me.GroupControl1.Controls.Add(Me.Label13)
        Me.GroupControl1.Controls.Add(Me.txt_EMail)
        Me.GroupControl1.Controls.Add(Me.txt_Cliente)
        Me.GroupControl1.Controls.Add(Me.Label12)
        Me.GroupControl1.Controls.Add(Me.txt_Telefono2)
        Me.GroupControl1.Controls.Add(Me.Label11)
        Me.GroupControl1.Controls.Add(Me.txt_Telefono)
        Me.GroupControl1.Controls.Add(Me.Label10)
        Me.GroupControl1.Controls.Add(Me.txt_RFC)
        Me.GroupControl1.Controls.Add(Me.Label5)
        Me.GroupControl1.Controls.Add(Me.txt_Nombre)
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Location = New System.Drawing.Point(7, 2)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(362, 641)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Datos"
        '
        'Label22
        '
        Me.Label22.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(84, 270)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(153, 24)
        Me.Label22.TabIndex = 44
        Me.Label22.Text = "Apellido Materno"
        '
        'txt_AMaterno
        '
        Me.txt_AMaterno.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_AMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_AMaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_AMaterno.Location = New System.Drawing.Point(6, 297)
        Me.txt_AMaterno.Name = "txt_AMaterno"
        Me.txt_AMaterno.Size = New System.Drawing.Size(351, 29)
        Me.txt_AMaterno.TabIndex = 43
        Me.txt_AMaterno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(86, 212)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(149, 24)
        Me.Label21.TabIndex = 42
        Me.Label21.Text = "Apellido Paterno"
        '
        'txt_APaterno
        '
        Me.txt_APaterno.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_APaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_APaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_APaterno.Location = New System.Drawing.Point(8, 239)
        Me.txt_APaterno.Name = "txt_APaterno"
        Me.txt_APaterno.Size = New System.Drawing.Size(351, 29)
        Me.txt_APaterno.TabIndex = 41
        Me.txt_APaterno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(79, 149)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(162, 24)
        Me.Label20.TabIndex = 40
        Me.Label20.Text = "Segundo Nombre"
        '
        'txt_Adicional
        '
        Me.txt_Adicional.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Adicional.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Adicional.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Adicional.Location = New System.Drawing.Point(8, 176)
        Me.txt_Adicional.Name = "txt_Adicional"
        Me.txt_Adicional.Size = New System.Drawing.Size(351, 29)
        Me.txt_Adicional.TabIndex = 39
        Me.txt_Adicional.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(130, 389)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(61, 24)
        Me.Label14.TabIndex = 38
        Me.Label14.Text = "CURP"
        '
        'Txt_CURP
        '
        Me.Txt_CURP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Txt_CURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_CURP.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_CURP.Location = New System.Drawing.Point(6, 417)
        Me.Txt_CURP.MaxLength = 18
        Me.Txt_CURP.Name = "Txt_CURP"
        Me.Txt_CURP.Size = New System.Drawing.Size(352, 29)
        Me.Txt_CURP.TabIndex = 2
        Me.Txt_CURP.Text = "XXXXXXXXXXXXXXXXXX"
        Me.Txt_CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btn_Cliente
        '
        Me.btn_Cliente.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.Location = New System.Drawing.Point(293, 50)
        Me.btn_Cliente.Name = "btn_Cliente"
        Me.btn_Cliente.Size = New System.Drawing.Size(64, 30)
        Me.btn_Cliente.TabIndex = 1
        Me.btn_Cliente.Text = "Cliente"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(130, 574)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(61, 24)
        Me.Label13.TabIndex = 36
        Me.Label13.Text = "e-mail"
        '
        'txt_EMail
        '
        Me.txt_EMail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_EMail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_EMail.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_EMail.Location = New System.Drawing.Point(5, 602)
        Me.txt_EMail.Name = "txt_EMail"
        Me.txt_EMail.Size = New System.Drawing.Size(352, 29)
        Me.txt_EMail.TabIndex = 5
        Me.txt_EMail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(126, 512)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 24)
        Me.Label12.TabIndex = 34
        Me.Label12.Text = "Celular"
        '
        'txt_Telefono2
        '
        Me.txt_Telefono2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Telefono2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Telefono2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Telefono2.Location = New System.Drawing.Point(6, 540)
        Me.txt_Telefono2.MaxLength = 10
        Me.txt_Telefono2.Name = "txt_Telefono2"
        Me.txt_Telefono2.Size = New System.Drawing.Size(352, 29)
        Me.txt_Telefono2.TabIndex = 4
        Me.txt_Telefono2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(118, 452)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(85, 24)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Teléfono"
        '
        'txt_Telefono
        '
        Me.txt_Telefono.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Telefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Telefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Telefono.Location = New System.Drawing.Point(5, 480)
        Me.txt_Telefono.MaxLength = 10
        Me.txt_Telefono.Name = "txt_Telefono"
        Me.txt_Telefono.Size = New System.Drawing.Size(352, 29)
        Me.txt_Telefono.TabIndex = 3
        Me.txt_Telefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(136, 329)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 24)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "RFC"
        '
        'txt_RFC
        '
        Me.txt_RFC.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_RFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(5, 357)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(352, 29)
        Me.txt_RFC.TabIndex = 1
        Me.txt_RFC.Text = "XXXX-000000-XXX"
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(121, 90)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 24)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Nombre"
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Nombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Nombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(6, 117)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(351, 29)
        Me.txt_Nombre.TabIndex = 0
        Me.txt_Nombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(126, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 24)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Cliente"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.Cbo_Municipio)
        Me.GroupControl2.Controls.Add(Me.Label8)
        Me.GroupControl2.Controls.Add(Me.Cbo_Estado)
        Me.GroupControl2.Controls.Add(Me.Label9)
        Me.GroupControl2.Controls.Add(Me.btn_BuscarColonia)
        Me.GroupControl2.Controls.Add(Me.Label4)
        Me.GroupControl2.Controls.Add(Me.txt_Calle)
        Me.GroupControl2.Controls.Add(Me.Label2)
        Me.GroupControl2.Controls.Add(Me.txt_CP)
        Me.GroupControl2.Controls.Add(Me.txt_NumInt)
        Me.GroupControl2.Controls.Add(Me.Label6)
        Me.GroupControl2.Controls.Add(Me.Label1)
        Me.GroupControl2.Controls.Add(Me.txt_Colonia)
        Me.GroupControl2.Controls.Add(Me.txt_NumExt)
        Me.GroupControl2.Controls.Add(Me.Label7)
        Me.GroupControl2.Location = New System.Drawing.Point(375, 2)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(483, 282)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Dirección"
        '
        'Cbo_Municipio
        '
        Me.Cbo_Municipio.DisplayMember = "municipio"
        Me.Cbo_Municipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cbo_Municipio.FormattingEnabled = True
        Me.Cbo_Municipio.Items.AddRange(New Object() {"CONTADO", "D. E."})
        Me.Cbo_Municipio.Location = New System.Drawing.Point(246, 177)
        Me.Cbo_Municipio.Name = "Cbo_Municipio"
        Me.Cbo_Municipio.Size = New System.Drawing.Size(232, 32)
        Me.Cbo_Municipio.TabIndex = 4
        Me.Cbo_Municipio.ValueMember = "id_municipio"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(246, 150)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(233, 24)
        Me.Label8.TabIndex = 73
        Me.Label8.Text = "Municipio"
        '
        'Cbo_Estado
        '
        Me.Cbo_Estado.DisplayMember = "estado"
        Me.Cbo_Estado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Cbo_Estado.FormattingEnabled = True
        Me.Cbo_Estado.Items.AddRange(New Object() {"TARJETA", "CLABE", "NO CUENTA"})
        Me.Cbo_Estado.Location = New System.Drawing.Point(5, 176)
        Me.Cbo_Estado.Name = "Cbo_Estado"
        Me.Cbo_Estado.Size = New System.Drawing.Size(232, 32)
        Me.Cbo_Estado.TabIndex = 3
        Me.Cbo_Estado.ValueMember = "id_estado"
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(5, 149)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(232, 24)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Estado"
        '
        'btn_BuscarColonia
        '
        Me.btn_BuscarColonia.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_BuscarColonia.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_BuscarColonia.Location = New System.Drawing.Point(173, 238)
        Me.btn_BuscarColonia.Name = "btn_BuscarColonia"
        Me.btn_BuscarColonia.Size = New System.Drawing.Size(64, 30)
        Me.btn_BuscarColonia.TabIndex = 66
        Me.btn_BuscarColonia.Text = "Buscar"
        '
        'txt_Pais
        '
        Me.txt_Pais.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Pais.Location = New System.Drawing.Point(749, 668)
        Me.txt_Pais.Name = "txt_Pais"
        Me.txt_Pais.Size = New System.Drawing.Size(100, 29)
        Me.txt_Pais.TabIndex = 68
        Me.txt_Pais.Text = "MEXICO"
        Me.txt_Pais.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Pais.Visible = False
        '
        'GC_Bancarios
        '
        Me.GC_Bancarios.Controls.Add(Me.Button2)
        Me.GC_Bancarios.Controls.Add(Me.Button1)
        Me.GC_Bancarios.Controls.Add(Me.cbo_Bancos)
        Me.GC_Bancarios.Controls.Add(Me.Cbo_Banco_Tipo)
        Me.GC_Bancarios.Controls.Add(Me.Label15)
        Me.GC_Bancarios.Controls.Add(Me.Txt_Banco_Cuenta)
        Me.GC_Bancarios.Controls.Add(Me.Label16)
        Me.GC_Bancarios.Controls.Add(Me.Label18)
        Me.GC_Bancarios.Location = New System.Drawing.Point(375, 316)
        Me.GC_Bancarios.Name = "GC_Bancarios"
        Me.GC_Bancarios.Size = New System.Drawing.Size(483, 149)
        Me.GC_Bancarios.TabIndex = 66
        Me.GC_Bancarios.Text = "Datos Bancarios"
        '
        'Button2
        '
        Me.Button2.Image = Global.proyVOficina_PDV.My.Resources.Resources.camera_360
        Me.Button2.Location = New System.Drawing.Point(384, 99)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(42, 42)
        Me.Button2.TabIndex = 67
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = Global.proyVOficina_PDV.My.Resources.Resources.adjunto
        Me.Button1.Location = New System.Drawing.Point(432, 98)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(42, 42)
        Me.Button1.TabIndex = 66
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cbo_Bancos
        '
        Me.cbo_Bancos.DisplayMember = "id_banco"
        Me.cbo_Bancos.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Bancos.FormattingEnabled = True
        Me.cbo_Bancos.Items.AddRange(New Object() {"CONTADO", "D. E."})
        Me.cbo_Bancos.Location = New System.Drawing.Point(5, 108)
        Me.cbo_Bancos.Name = "cbo_Bancos"
        Me.cbo_Bancos.Size = New System.Drawing.Size(184, 32)
        Me.cbo_Bancos.TabIndex = 1
        Me.cbo_Bancos.ValueMember = "id_banco"
        '
        'Cbo_Banco_Tipo
        '
        Me.Cbo_Banco_Tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Cbo_Banco_Tipo.FormattingEnabled = True
        Me.Cbo_Banco_Tipo.Items.AddRange(New Object() {"TARJETA", "CLABE", "NO CUENTA"})
        Me.Cbo_Banco_Tipo.Location = New System.Drawing.Point(195, 108)
        Me.Cbo_Banco_Tipo.Name = "Cbo_Banco_Tipo"
        Me.Cbo_Banco_Tipo.Size = New System.Drawing.Size(179, 32)
        Me.Cbo_Banco_Tipo.TabIndex = 2
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(3, 21)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(471, 22)
        Me.Label15.TabIndex = 52
        Me.Label15.Text = "Cuenta"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Txt_Banco_Cuenta
        '
        Me.Txt_Banco_Cuenta.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Txt_Banco_Cuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Banco_Cuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Banco_Cuenta.Location = New System.Drawing.Point(5, 46)
        Me.Txt_Banco_Cuenta.Name = "Txt_Banco_Cuenta"
        Me.Txt_Banco_Cuenta.Size = New System.Drawing.Size(471, 29)
        Me.Txt_Banco_Cuenta.TabIndex = 0
        Me.Txt_Banco_Cuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(191, 81)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(194, 24)
        Me.Label16.TabIndex = 65
        Me.Label16.Text = "Tipo Cuenta"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(4, 81)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(205, 24)
        Me.Label18.TabIndex = 63
        Me.Label18.Text = "Banco"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.Label19)
        Me.GroupControl4.Controls.Add(Me.Txt_Parentesco)
        Me.GroupControl4.Controls.Add(Me.Label17)
        Me.GroupControl4.Controls.Add(Me.Txt_Beneficiario)
        Me.GroupControl4.Location = New System.Drawing.Point(375, 491)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(483, 152)
        Me.GroupControl4.TabIndex = 69
        Me.GroupControl4.Text = "Datos del Beneficiario"
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(5, 76)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(429, 29)
        Me.Label19.TabIndex = 54
        Me.Label19.Text = "Parentesco"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Txt_Parentesco
        '
        Me.Txt_Parentesco.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Txt_Parentesco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Parentesco.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Parentesco.Location = New System.Drawing.Point(5, 101)
        Me.Txt_Parentesco.Name = "Txt_Parentesco"
        Me.Txt_Parentesco.Size = New System.Drawing.Size(473, 29)
        Me.Txt_Parentesco.TabIndex = 53
        Me.Txt_Parentesco.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(5, 21)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(429, 29)
        Me.Label17.TabIndex = 52
        Me.Label17.Text = "Beneficiario"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Txt_Beneficiario
        '
        Me.Txt_Beneficiario.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Txt_Beneficiario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Beneficiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Beneficiario.Location = New System.Drawing.Point(5, 46)
        Me.Txt_Beneficiario.Name = "Txt_Beneficiario"
        Me.Txt_Beneficiario.Size = New System.Drawing.Size(473, 29)
        Me.Txt_Beneficiario.TabIndex = 0
        Me.Txt_Beneficiario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'OFD1
        '
        Me.OFD1.Title = "Selecciona Foto del Documento"
        '
        'PB_Bancarios
        '
        Me.PB_Bancarios.Location = New System.Drawing.Point(584, 655)
        Me.PB_Bancarios.Name = "PB_Bancarios"
        Me.PB_Bancarios.Size = New System.Drawing.Size(82, 41)
        Me.PB_Bancarios.TabIndex = 70
        Me.PB_Bancarios.TabStop = False
        '
        'pdvClienteFinal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 705)
        Me.ControlBox = False
        Me.Controls.Add(Me.PB_Bancarios)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GC_Bancarios)
        Me.Controls.Add(Me.txt_Pais)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btn_Aceptar)
        Me.Controls.Add(Me.btn_Cancelar)
        Me.KeyPreview = True
        Me.Name = "pdvClienteFinal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cliente Final"
        CType(Me.txt_CP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.GC_Bancarios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Bancarios.ResumeLayout(False)
        Me.GC_Bancarios.PerformLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.PB_Bancarios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents btn_Aceptar As Button
    Private WithEvents btn_Cancelar As Button
    Private WithEvents Label7 As Label
    Private WithEvents Label6 As Label
    Private WithEvents Label4 As Label
    Private WithEvents Label1 As Label
    Private WithEvents Label2 As Label
    Friend WithEvents txt_Cliente As TextBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label10 As Label
    Private WithEvents txt_Pais As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txt_Colonia As TextBox
    Friend WithEvents txt_CP As NumericUpDown
    Friend WithEvents txt_Calle As TextBox
    Friend WithEvents txt_NumExt As TextBox
    Friend WithEvents txt_NumInt As TextBox
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents txt_EMail As TextBox
    Friend WithEvents txt_Telefono2 As TextBox
    Friend WithEvents txt_Telefono As TextBox
    Friend WithEvents btn_Cliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label14 As Label
    Friend WithEvents Txt_CURP As TextBox
    Friend WithEvents GC_Bancarios As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Cbo_Banco_Tipo As ComboBox
    Private WithEvents Label15 As Label
    Friend WithEvents Txt_Banco_Cuenta As TextBox
    Private WithEvents Label16 As Label
    Private WithEvents Label18 As Label
    Friend WithEvents cbo_Bancos As ComboBox
    Friend WithEvents Cbo_Municipio As ComboBox
    Private WithEvents Label8 As Label
    Friend WithEvents Cbo_Estado As ComboBox
    Private WithEvents Label9 As Label
    Friend WithEvents btn_BuscarColonia As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Private WithEvents Label19 As Label
    Friend WithEvents Txt_Parentesco As TextBox
    Private WithEvents Label17 As Label
    Friend WithEvents Txt_Beneficiario As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents txt_AMaterno As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txt_APaterno As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txt_Adicional As TextBox
    Private WithEvents Button1 As Button
    Friend WithEvents OFD1 As OpenFileDialog
    Friend WithEvents PB_Bancarios As PictureBox
    Private WithEvents Button2 As Button
End Class
