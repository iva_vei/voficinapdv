﻿Public Class uCtrl_Proveedores
    Enum typTipo
        PROVEEDORES
        TRANSPORTISTAS
    End Enum
    Dim _IdProveedor As Integer
    Dim _Proveedor As String
    Public _TipoProv As typTipo
    Public Event AfterShow()
    Public Property IdProveedor As Integer
        Get
            Return _IdProveedor
        End Get
        Set(value As Integer)
            _IdProveedor = value
        End Set
    End Property
    Public Property Proveedor As String
        Get
            Return _Proveedor
        End Get
        Set(value As String)
            _Proveedor = value
        End Set
    End Property
    Public Property TipoProv As typTipo
        Get
            Return _TipoProv
        End Get
        Set(value As typTipo)
            _TipoProv = value
        End Set
    End Property

    Private Sub uCtrl_Proveedores_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            btn_Proveedor.Top = 0
            btn_Proveedor.Left = 0
            btn_Proveedor.Width = 48
            btn_Proveedor.Height = Me.Height - 3
            'btn_Proveedor.ImageOptions.Image = Globales.oAmbientes.ImagenResize(My.Resources.Proveedores, 24, 24, True)
            Call Clear
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Proveedor_Click(sender As Object, e As EventArgs) Handles btn_Proveedor.Click
        Dim frm As SelCombobox
        Try
            frm = New SelCombobox
            frm.Tipo = Globales.TipoCombo.Proveedores
            frm.ShowDialog()
            Select Case _TipoProv
                Case typTipo.PROVEEDORES
                    lbl_IdProveedor.Text = IIf(frm.IdSelecionado > 0, Format(frm.IdSelecionado, "000000") & " - " & frm.ValorSelecionado, "Selecciona un Proveedor ...")
                Case typTipo.TRANSPORTISTAS
                    lbl_IdProveedor.Text = IIf(frm.IdSelecionado > 0, Format(frm.IdSelecionado, "000000") & " - " & frm.ValorSelecionado, "Selecciona un Transportista ...")
            End Select
            _IdProveedor = frm.IdSelecionado
            _Proveedor = frm.ValorSelecionado

            RaiseEvent AfterShow()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub uCtrl_Proveedores_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Try
            btn_Proveedor.Top = 0
            btn_Proveedor.Left = 0
            btn_Proveedor.Width = 48
            btn_Proveedor.Height = Me.Height - 3
            Me.lbl_IdProveedor.Width = Me.Width - btn_Proveedor.Width
        Catch ex As Exception
            btn_Proveedor.Top = 0
            btn_Proveedor.Left = 0
            Me.Width = 232
            Me.Height = 48
        End Try
    End Sub
    Public Sub Clear()
        Try
            Select Case _TipoProv
                Case typTipo.PROVEEDORES
                    btn_Proveedor.Text = "Provs"
                    lbl_IdProveedor.Text = "000000 - Selecciona un Proveedor ..."

                Case typTipo.TRANSPORTISTAS
                    btn_Proveedor.Text = "Transp"
                    lbl_IdProveedor.Text = "000000 - Selecciona un Transportista ..."

            End Select
        Catch ex As Exception

        End Try
    End Sub
End Class
