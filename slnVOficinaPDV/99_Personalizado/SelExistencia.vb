﻿Imports DevExpress.XtraBars
Public Class SelExistencia
    Private SW_refrescar As Boolean = False
    Private Modo As Globales.TipoABC
    Public Sub New()
        InitializeComponent()
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControl.OptionsFocus.EnableAutoTabOrder = False
        Me.GridV_1.Appearance.Row.ForeColor = System.Drawing.Color.Black
    End Sub
    Private Sub ABCPantallas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim msj As String = ""
        Try
            Modo = Globales.TipoABC.Indice

            txt_Codigoref.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiCodigoRef", "")
            txt_CodigoBarra.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiCodigoBarra", "")
            txt_Marca.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiMarca", "")
            txt_Clase.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiClase", "")
            txt_Nombre.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiNombre", "")
            txt_NomCorto.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiNomCorto", "")

            If txt_Codigoref.Text <> "" _
            Or txt_CodigoBarra.Text <> "" _
            Or txt_Marca.Text <> "" _
            Or txt_Clase.Text <> "" _
            Or txt_Nombre.Text <> "" _
            Or txt_NomCorto.Text <> "" Then
                Call Refrescar()
            End If
            ''Call ControlBotones("Ok")
            ''Call Botones("Ok")
            ''Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Public Sub Refrescar()
        Call Mostrar_Indice()

    End Sub
    Public Sub Imprimir()
        Dim Formularios As Globales.clsFormularios
        Try
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "IMPRIMIR") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Formularios = New Globales.clsFormularios
            Formularios.Imprimir(Me.Name, "Formulario Plantilla", GridC_1)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Excel()
        Dim Formularios As Globales.clsFormularios
        Try
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "EXCEL") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Formularios = New Globales.clsFormularios
            Formularios.ExportarXLS(Me.Name, "Formulario Plantilla", GridC_1, Globales.oAmbientes.Sistema, Application.ProductName, "ICC")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Regresar()
        Try
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Titulo_BtnPanel(ByVal sw_visible)
        PanelFiltros_Acciones.Visible = sw_visible
        If PanelFiltros_Acciones.Visible = True Then
            btn_OcultarMostrar_Acciones.Text = "Ocultar Filtros y Acciones"
        Else
            btn_OcultarMostrar_Acciones.Text = "Mostrar Filtros y Acciones"
        End If
    End Sub
    Private Sub btn_OcultarMostrar_Acciones_Click(sender As Object, e As EventArgs) Handles btn_OcultarMostrar_Acciones.Click
        Call Titulo_BtnPanel(Not PanelFiltros_Acciones.Visible)
    End Sub

    Private Sub Botones(ByVal tag As String)
        Dim btnOk As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Ok")
        Dim btnCancelar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Cancelar")

        ''Select Case tag
        ''    Case "Ok", "Cancelar", "Consulta"
        ''        btnOk.Visible = False
        ''        btnCancelar.Visible = False
        ''End Select
    End Sub
    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0
        Try
            Select Case tag
                Case "Cancelar"
                    Call Botones(tag)
                    Call Regresar()
                Case "Ok"
                    If GridV_1.RowCount > 0 Then
                        txt_Articulo.Text = GridV_1.GetFocusedRowCellValue(col1_Articulo)
                    Else
                        txt_Articulo.Text = ""
                    End If
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiCodigoRef", txt_Codigoref.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiCodigoBarra", txt_CodigoBarra.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiMarca", txt_Marca.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiClase", txt_Clase.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiNombre", txt_Nombre.Text)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "SelExiNomCorto", txt_NomCorto.Text)
                    Call Regresar()
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub Mostrar_Indice()
        SW_refrescar = False
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim IdUsuario As Integer = 0
        Try
            oDatos = New Datos_Viscoi
            band_existencia.Visible = chk_Existencias.Checked
            If oDatos.PVTA_Recupera_Articulos("ACTIVO", txt_Codigoref.Text, txt_CodigoBarra.Text, txt_Marca.Text, txt_Clase.Text, txt_Nombre.Text, txt_NomCorto.Text, txt_Estructura1.Text, chk_Existencias.Checked, dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    GridC_1.DataSource = dtDatos
                    GridV_1.Focus()
                Else
                    SW_refrescar = True
                    GridC_1.DataSource = Nothing
                    MessageBox.Show("No se puede encontrar registros con los filtos espeficiados.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                SW_refrescar = True
                GridC_1.DataSource = Nothing
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            GridV_1.BestFitColumns()
        Catch ex As Exception
            SW_refrescar = True
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
        Call ControlBotones("Ok")
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim oFrm As SelEstructura
        Try
            oFrm = New SelEstructura

            oFrm.ShowDialog()
            txt_Estructura1.Text = oFrm.Estructura1
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_Nombre_LostFocus(sender As Object, e As EventArgs) Handles txt_Nombre.LostFocus, txt_NomCorto.LostFocus
        If txt_Codigoref.Text <> "" _
        Or txt_CodigoBarra.Text <> "" _
        Or txt_Marca.Text <> "" _
        Or txt_Clase.Text <> "" _
        Or txt_Nombre.Text <> "" _
        Or txt_NomCorto.Text <> "" Then
            Call Refrescar()
        End If
    End Sub
    Private Sub SelArticulos_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        If SW_refrescar Then Exit Sub
        Try
            Select Case e.KeyCode
                Case Keys.Enter
                    ControlBotones("Ok")
                Case Keys.Escape
                    txt_Articulo.Text = ""
                    Me.Close()
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SelArticulos_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Try
            txt_Codigoref.Focus()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub chk_Existencias_CheckedChanged(sender As Object, e As EventArgs) Handles chk_Existencias.CheckedChanged
        Call Mostrar_Indice()
    End Sub
    Private Sub GridC_1_KeyDown(sender As Object, e As KeyEventArgs) Handles GridC_1.KeyDown
        Call Globales.clsFormularios.Grid_Prevenir_Tab(sender, e, txt_Codigoref)
    End Sub
End Class