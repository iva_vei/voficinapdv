﻿Public Class frmcameraValida
    Private WithEvents cam As New DSCamCapture
    Dim MyPicturesFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
    Public PathImagen As String = ""
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        cam.Dispose()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PictureBox3.Visible = False
        PictureBox4.Visible = False
        PictureBox5.Visible = False
        PictureBox6.Visible = False

        ComboBox_Devices.Items.AddRange(cam.GetCaptureDevices)
        If ComboBox_Devices.Items.Count > 0 Then ComboBox_Devices.SelectedIndex = 0

        For Each sz As String In [Enum].GetNames(GetType(DSCamCapture.FrameSizes))
            ComboBox_FrameSize.Items.Add(sz.Replace("s", ""))
        Next
        If ComboBox_FrameSize.Items.Count > 2 Then ComboBox_FrameSize.SelectedIndex = 2

        Button_Connect.Enabled = (ComboBox_Devices.Items.Count > 0)
        Button_Save.Enabled = False
    End Sub

    Private Sub ComboBox_Devices_DropDown(sender As Object, e As EventArgs) Handles ComboBox_Devices.DropDown
        ComboBox_Devices.Items.Clear()
        ComboBox_Devices.Items.AddRange(cam.GetCaptureDevices)
        Button_Connect.Enabled = (ComboBox_Devices.Items.Count > 0)
        If ComboBox_Devices.SelectedIndex = -1 And ComboBox_Devices.Items.Count > 0 Then ComboBox_Devices.SelectedIndex = 0
    End Sub

    Private Sub Button_Connect_Click(sender As Object, e As EventArgs) Handles Button_Connect.Click
        If Not cam.IsConnected Then
            Dim si As Integer = ComboBox_FrameSize.SelectedIndex
            Dim SelectedSize As DSCamCapture.FrameSizes = CType(si, DSCamCapture.FrameSizes)
            If cam.ConnectToDevice(ComboBox_Devices.SelectedIndex, 15, PictureBox1.ClientSize, SelectedSize, PictureBox1.Handle) Then
                cam.Start()
                Button_Connect.Text = "Disconnect"
            End If
        Else
            cam.Dispose()
            Button_Connect.Text = "Connect"
        End If
        Button_Save.Enabled = cam.IsConnected
        ComboBox_Devices.Enabled = Not cam.IsConnected
        ComboBox_FrameSize.Enabled = Not cam.IsConnected
    End Sub

    Private Sub Button_Save_Click(sender As Object, e As EventArgs) Handles Button_Save.Click
        If Not IO.Directory.Exists(MyPicturesFolder) Then IO.Directory.CreateDirectory(MyPicturesFolder)
        Dim fName As String = Now.ToString.Replace("/", "-").Replace(":", "-").Replace(" ", "_") & ".jpg"
        Dim SaveAs As String = IO.Path.Combine(MyPicturesFolder, fName)
        cam.SaveCurrentFrame(SaveAs, Imaging.ImageFormat.Jpeg)
    End Sub

    Dim appPath As String = My.Application.Info.DirectoryPath
    Private Sub PictureBox1_SizeChanged(sender As Object, e As EventArgs) Handles PictureBox1.SizeChanged
        cam.ResizeWindow(0, 0, PictureBox1.ClientSize.Width, PictureBox1.ClientSize.Height)
    End Sub

    Private Sub cam_FrameSaved(capImage As Bitmap, imgPath As String) Handles cam.FrameSaved

        If (PictureBox2.Visible) Then
            PictureBox2.Image = New Bitmap(capImage)
        End If
        If (PictureBox3.Visible) Then
            PictureBox3.Image = New Bitmap(capImage)
        End If
        If (PictureBox4.Visible) Then
            PictureBox4.Image = New Bitmap(capImage)
        End If
        If (PictureBox5.Visible) Then
            PictureBox5.Image = New Bitmap(capImage)
        End If

        Label2.Text = "Saved As - " & IO.Path.GetFileName(imgPath)
        Label3.Text = "Image Size - " & PictureBox2.Image.Size.ToString
        PathImagen = imgPath
    End Sub

    Private Sub Button_Next_Click(sender As Object, e As EventArgs) Handles Button_Next.Click
        If (PictureBox2.Visible) Then
            PictureBox2.Visible = False
            PictureBox3.Visible = True
            lbl_Imagen.Text = "FOTO INE"
        ElseIf (PictureBox3.Visible) Then
            PictureBox3.Visible = False
            PictureBox4.Visible = True
            lbl_Imagen.Text = "FOTO REVERSO INE"
        ElseIf (PictureBox4.Visible) Then
            PictureBox4.Visible = False
            PictureBox5.Visible = True
            lbl_Imagen.Text = "FOTO COMPROBANTE DE DOMICILIO"
            Button_Next.Enabled = False
        ElseIf (PictureBox5.Visible) Then
            PictureBox5.Visible = False

        Else

        End If
    End Sub
End Class
