﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SelSKU
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col_IdSku = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Marca = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Estilo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Color = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Talla = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Jerarquia01 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Jerarquia02 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Jerarquia03 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Jerarquia04 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.lbl_Etiqueta = New DevExpress.XtraEditors.LabelControl()
        Me.PanelFiltros_Acciones.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 4.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 4.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 4.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(63, Byte), Integer))
        WindowsUIButtonImageOptions1.ImageUri.Uri = "Apply;GrayScaled"
        WindowsUIButtonImageOptions2.ImageUri.Uri = "Cancel;GrayScaled"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Ok", True, WindowsUIButtonImageOptions1), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", True, WindowsUIButtonImageOptions2)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.BottomRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.GridC_1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lbl_Etiqueta)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(0, -1)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 500)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(70, 74)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(917, 500)
        Me.PanelFiltros_Acciones.TabIndex = 3
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'GridC_1
        '
        Me.GridC_1.Location = New System.Drawing.Point(12, 45)
        Me.GridC_1.LookAndFeel.SkinName = "Office 2010 Black"
        Me.GridC_1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(893, 395)
        Me.GridC_1.TabIndex = 2
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col_IdSku, Me.col_Marca, Me.col_Estilo, Me.col_Color, Me.col_Talla, Me.col_Jerarquia01, Me.col_Jerarquia02, Me.col_Jerarquia03, Me.col_Jerarquia04})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col_IdSku
        '
        Me.col_IdSku.AppearanceCell.Options.UseTextOptions = True
        Me.col_IdSku.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.col_IdSku.Caption = "IdSku"
        Me.col_IdSku.FieldName = "id_sku"
        Me.col_IdSku.MaxWidth = 75
        Me.col_IdSku.Name = "col_IdSku"
        Me.col_IdSku.OptionsColumn.AllowEdit = False
        Me.col_IdSku.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_IdSku.Visible = True
        Me.col_IdSku.VisibleIndex = 0
        '
        'col_Marca
        '
        Me.col_Marca.Caption = "Marca"
        Me.col_Marca.FieldName = "marca"
        Me.col_Marca.Name = "col_Marca"
        Me.col_Marca.OptionsColumn.AllowEdit = False
        Me.col_Marca.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Marca.Visible = True
        Me.col_Marca.VisibleIndex = 1
        '
        'col_Estilo
        '
        Me.col_Estilo.Caption = "Estilo"
        Me.col_Estilo.FieldName = "estilo"
        Me.col_Estilo.Name = "col_Estilo"
        Me.col_Estilo.OptionsColumn.AllowEdit = False
        Me.col_Estilo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Estilo.Visible = True
        Me.col_Estilo.VisibleIndex = 2
        '
        'col_Color
        '
        Me.col_Color.Caption = "Color"
        Me.col_Color.FieldName = "color"
        Me.col_Color.Name = "col_Color"
        Me.col_Color.OptionsColumn.AllowEdit = False
        Me.col_Color.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Color.Visible = True
        Me.col_Color.VisibleIndex = 3
        '
        'col_Talla
        '
        Me.col_Talla.Caption = "Talla"
        Me.col_Talla.FieldName = "talla"
        Me.col_Talla.Name = "col_Talla"
        Me.col_Talla.OptionsColumn.AllowEdit = False
        Me.col_Talla.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Talla.Visible = True
        Me.col_Talla.VisibleIndex = 8
        '
        'col_Jerarquia01
        '
        Me.col_Jerarquia01.Caption = "Jerarquia01"
        Me.col_Jerarquia01.FieldName = "jerarquia01"
        Me.col_Jerarquia01.Name = "col_Jerarquia01"
        Me.col_Jerarquia01.OptionsColumn.AllowEdit = False
        Me.col_Jerarquia01.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Jerarquia01.Visible = True
        Me.col_Jerarquia01.VisibleIndex = 4
        '
        'col_Jerarquia02
        '
        Me.col_Jerarquia02.Caption = "Jerarquia02"
        Me.col_Jerarquia02.FieldName = "jerarquia02"
        Me.col_Jerarquia02.Name = "col_Jerarquia02"
        Me.col_Jerarquia02.OptionsColumn.AllowEdit = False
        Me.col_Jerarquia02.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Jerarquia02.Visible = True
        Me.col_Jerarquia02.VisibleIndex = 5
        '
        'col_Jerarquia03
        '
        Me.col_Jerarquia03.Caption = "Jerarquia03"
        Me.col_Jerarquia03.FieldName = "jerarquia03"
        Me.col_Jerarquia03.Name = "col_Jerarquia03"
        Me.col_Jerarquia03.OptionsColumn.AllowEdit = False
        Me.col_Jerarquia03.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Jerarquia03.Visible = True
        Me.col_Jerarquia03.VisibleIndex = 6
        '
        'col_Jerarquia04
        '
        Me.col_Jerarquia04.Caption = "Jerarquia04"
        Me.col_Jerarquia04.FieldName = "jerarquia04"
        Me.col_Jerarquia04.Name = "col_Jerarquia04"
        Me.col_Jerarquia04.OptionsColumn.AllowEdit = False
        Me.col_Jerarquia04.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Jerarquia04.Visible = True
        Me.col_Jerarquia04.VisibleIndex = 7
        '
        'lbl_Etiqueta
        '
        Me.lbl_Etiqueta.Appearance.Font = New System.Drawing.Font("Tahoma", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Etiqueta.Appearance.Options.UseFont = True
        Me.lbl_Etiqueta.Location = New System.Drawing.Point(12, 11)
        Me.lbl_Etiqueta.Name = "lbl_Etiqueta"
        Me.lbl_Etiqueta.Size = New System.Drawing.Size(123, 28)
        Me.lbl_Etiqueta.TabIndex = 1
        Me.lbl_Etiqueta.Text = "Elije un SKU"
        '
        'SelSKU
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(917, 499)
        Me.Controls.Add(Me.PanelFiltros_Acciones)
        Me.Name = "SelSKU"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona un SKU ..."
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents lbl_Etiqueta As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col_IdSku As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Marca As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Estilo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Color As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Talla As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Jerarquia01 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Jerarquia02 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Jerarquia03 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Jerarquia04 As DevExpress.XtraGrid.Columns.GridColumn
End Class
