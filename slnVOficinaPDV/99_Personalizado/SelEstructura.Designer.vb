﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelEstructura
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.TreeList1 = New DevExpress.XtraTreeList.TreeList()
        Me.col_IdDivision = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_IdDepto = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_IdFamilia = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_NmJer01 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_NmJer02 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_NmJer03 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_Nivel = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_IdEstructura = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_Estructura = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.col_Estructura1 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 4.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 4.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 4.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(63, Byte), Integer))
        WindowsUIButtonImageOptions1.ImageUri.Uri = "Apply;GrayScaled"
        WindowsUIButtonImageOptions2.ImageUri.Uri = "Cancel;GrayScaled"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Ok", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(0, 306)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 60)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(52, 60)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(600, 60)
        Me.PanelFiltros_Acciones.TabIndex = 2
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'TreeList1
        '
        Me.TreeList1.AllowDrop = True
        Me.TreeList1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TreeList1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.TreeList1.Appearance.EvenRow.Options.UseBackColor = True
        Me.TreeList1.Columns.AddRange(New DevExpress.XtraTreeList.Columns.TreeListColumn() {Me.col_IdDivision, Me.col_IdDepto, Me.col_IdFamilia, Me.col_NmJer01, Me.col_NmJer02, Me.col_NmJer03, Me.col_Nivel, Me.col_IdEstructura, Me.col_Estructura, Me.col_Estructura1})
        Me.TreeList1.Cursor = System.Windows.Forms.Cursors.Default
        Me.TreeList1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeList1.FixedLineWidth = 1
        Me.TreeList1.HorzScrollStep = 2
        Me.TreeList1.Location = New System.Drawing.Point(0, 0)
        Me.TreeList1.Margin = New System.Windows.Forms.Padding(2)
        Me.TreeList1.MinWidth = 16
        Me.TreeList1.Name = "TreeList1"
        Me.TreeList1.OptionsDragAndDrop.DragNodesMode = DevExpress.XtraTreeList.DragNodesMode.[Single]
        Me.TreeList1.OptionsDragAndDrop.DropNodesMode = DevExpress.XtraTreeList.DropNodesMode.Standard
        Me.TreeList1.OptionsSelection.MultiSelect = True
        Me.TreeList1.OptionsView.FocusRectStyle = DevExpress.XtraTreeList.DrawFocusRectStyle.RowFocus
        Me.TreeList1.Size = New System.Drawing.Size(600, 306)
        Me.TreeList1.TabIndex = 1
        Me.TreeList1.TreeLevelWidth = 13
        '
        'col_IdDivision
        '
        Me.col_IdDivision.Caption = "Id Division"
        Me.col_IdDivision.FieldName = "IdDivision"
        Me.col_IdDivision.MinWidth = 16
        Me.col_IdDivision.Name = "col_IdDivision"
        Me.col_IdDivision.OptionsColumn.AllowEdit = False
        Me.col_IdDivision.Width = 56
        '
        'col_IdDepto
        '
        Me.col_IdDepto.Caption = "Id Depto"
        Me.col_IdDepto.FieldName = "IdDepto"
        Me.col_IdDepto.MinWidth = 16
        Me.col_IdDepto.Name = "col_IdDepto"
        Me.col_IdDepto.OptionsColumn.AllowEdit = False
        Me.col_IdDepto.Width = 56
        '
        'col_IdFamilia
        '
        Me.col_IdFamilia.Caption = "Id Familia"
        Me.col_IdFamilia.FieldName = "IdFamilia"
        Me.col_IdFamilia.MinWidth = 16
        Me.col_IdFamilia.Name = "col_IdFamilia"
        Me.col_IdFamilia.OptionsColumn.AllowEdit = False
        Me.col_IdFamilia.Width = 56
        '
        'col_NmJer01
        '
        Me.col_NmJer01.Caption = "Division"
        Me.col_NmJer01.FieldName = "Division"
        Me.col_NmJer01.MinWidth = 16
        Me.col_NmJer01.Name = "col_NmJer01"
        Me.col_NmJer01.OptionsColumn.AllowEdit = False
        Me.col_NmJer01.Width = 56
        '
        'col_NmJer02
        '
        Me.col_NmJer02.Caption = "Depto"
        Me.col_NmJer02.FieldName = "Depto"
        Me.col_NmJer02.MinWidth = 16
        Me.col_NmJer02.Name = "col_NmJer02"
        Me.col_NmJer02.OptionsColumn.AllowEdit = False
        Me.col_NmJer02.Width = 56
        '
        'col_NmJer03
        '
        Me.col_NmJer03.Caption = "Familia"
        Me.col_NmJer03.FieldName = "Familia"
        Me.col_NmJer03.MinWidth = 16
        Me.col_NmJer03.Name = "col_NmJer03"
        Me.col_NmJer03.OptionsColumn.AllowEdit = False
        Me.col_NmJer03.Width = 56
        '
        'col_Nivel
        '
        Me.col_Nivel.Caption = "Nivel"
        Me.col_Nivel.FieldName = "nivel"
        Me.col_Nivel.MinWidth = 16
        Me.col_Nivel.Name = "col_Nivel"
        Me.col_Nivel.OptionsColumn.AllowEdit = False
        Me.col_Nivel.Width = 56
        '
        'col_IdEstructura
        '
        Me.col_IdEstructura.Caption = "Id Estructura"
        Me.col_IdEstructura.FieldName = "Id_Estructura"
        Me.col_IdEstructura.MinWidth = 16
        Me.col_IdEstructura.Name = "col_IdEstructura"
        Me.col_IdEstructura.OptionsColumn.AllowEdit = False
        Me.col_IdEstructura.Width = 56
        '
        'col_Estructura
        '
        Me.col_Estructura.Caption = "Estructura"
        Me.col_Estructura.FieldName = "Estructura"
        Me.col_Estructura.MinWidth = 16
        Me.col_Estructura.Name = "col_Estructura"
        Me.col_Estructura.OptionsColumn.AllowEdit = False
        Me.col_Estructura.Visible = True
        Me.col_Estructura.VisibleIndex = 0
        Me.col_Estructura.Width = 56
        '
        'col_Estructura1
        '
        Me.col_Estructura1.Caption = "Estructura1"
        Me.col_Estructura1.FieldName = "estructura1"
        Me.col_Estructura1.Name = "col_Estructura1"
        Me.col_Estructura1.OptionsColumn.AllowEdit = False
        '
        'SelEstructura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 366)
        Me.Controls.Add(Me.TreeList1)
        Me.Controls.Add(Me.PanelFiltros_Acciones)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "SelEstructura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona la Estructura"
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents TreeList1 As DevExpress.XtraTreeList.TreeList
    Friend WithEvents col_IdDivision As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_IdDepto As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_IdFamilia As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_NmJer01 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_NmJer02 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_NmJer03 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_Nivel As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_IdEstructura As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_Estructura As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents col_Estructura1 As DevExpress.XtraTreeList.Columns.TreeListColumn
End Class
