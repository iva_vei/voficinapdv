﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelCombobox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.GridC_3 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col3_IdEstado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col3_IdMunicipio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col3_IdAsentamiento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col3_Asentamiento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col3_CP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridC_2 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col2_Pedido = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Proveedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Nombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Fecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Entrega = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Cancela = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Pendiente = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Bultos = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Cajas = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Pzas = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Calendario1 = New DevExpress.XtraEditors.Controls.CalendarControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col_Id = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col_Valor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.lbl_Etiqueta = New DevExpress.XtraEditors.LabelControl()
        Me.PanelFiltros_Acciones.SuspendLayout()
        CType(Me.GridC_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Calendario1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 1.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 1.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 1.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(63, Byte), Integer))
        WindowsUIButtonImageOptions1.ImageUri.Uri = "Apply;GrayScaled"
        WindowsUIButtonImageOptions2.ImageUri.Uri = "Cancel;GrayScaled"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Ok", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.BottomRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.GridC_3)
        Me.PanelFiltros_Acciones.Controls.Add(Me.GridC_2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Calendario1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.GridC_1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lbl_Etiqueta)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(0, 1)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 406)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(52, 60)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(586, 286)
        Me.PanelFiltros_Acciones.TabIndex = 3
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'GridC_3
        '
        Me.GridC_3.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_3.Location = New System.Drawing.Point(23, 35)
        Me.GridC_3.LookAndFeel.SkinName = "Office 2010 Black"
        Me.GridC_3.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GridC_3.MainView = Me.GridV_3
        Me.GridC_3.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_3.Name = "GridC_3"
        Me.GridC_3.Size = New System.Drawing.Size(357, 199)
        Me.GridC_3.TabIndex = 5
        Me.GridC_3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_3})
        '
        'GridV_3
        '
        Me.GridV_3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col3_IdEstado, Me.col3_IdMunicipio, Me.col3_IdAsentamiento, Me.col3_Asentamiento, Me.col3_CP})
        Me.GridV_3.DetailHeight = 284
        Me.GridV_3.GridControl = Me.GridC_3
        Me.GridV_3.Name = "GridV_3"
        Me.GridV_3.OptionsView.ShowGroupPanel = False
        '
        'col3_IdEstado
        '
        Me.col3_IdEstado.Caption = "IdEstado"
        Me.col3_IdEstado.FieldName = "id_estado"
        Me.col3_IdEstado.Name = "col3_IdEstado"
        Me.col3_IdEstado.OptionsColumn.AllowEdit = False
        '
        'col3_IdMunicipio
        '
        Me.col3_IdMunicipio.Caption = "IdMunicipio"
        Me.col3_IdMunicipio.FieldName = "id_municipio"
        Me.col3_IdMunicipio.Name = "col3_IdMunicipio"
        Me.col3_IdMunicipio.OptionsColumn.AllowEdit = False
        '
        'col3_IdAsentamiento
        '
        Me.col3_IdAsentamiento.Caption = "IdAsentamiento"
        Me.col3_IdAsentamiento.FieldName = "id_asentamiento"
        Me.col3_IdAsentamiento.Name = "col3_IdAsentamiento"
        Me.col3_IdAsentamiento.OptionsColumn.AllowEdit = False
        '
        'col3_Asentamiento
        '
        Me.col3_Asentamiento.Caption = "Asentamiento"
        Me.col3_Asentamiento.FieldName = "asentamiento"
        Me.col3_Asentamiento.Name = "col3_Asentamiento"
        Me.col3_Asentamiento.OptionsColumn.AllowEdit = False
        Me.col3_Asentamiento.Visible = True
        Me.col3_Asentamiento.VisibleIndex = 0
        '
        'col3_CP
        '
        Me.col3_CP.Caption = "C.P."
        Me.col3_CP.FieldName = "cp"
        Me.col3_CP.Name = "col3_CP"
        Me.col3_CP.OptionsColumn.AllowEdit = False
        Me.col3_CP.Visible = True
        Me.col3_CP.VisibleIndex = 1
        '
        'GridC_2
        '
        Me.GridC_2.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_2.Location = New System.Drawing.Point(9, 37)
        Me.GridC_2.LookAndFeel.SkinName = "Office 2010 Black"
        Me.GridC_2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GridC_2.MainView = Me.GridV_2
        Me.GridC_2.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_2.Name = "GridC_2"
        Me.GridC_2.Size = New System.Drawing.Size(570, 199)
        Me.GridC_2.TabIndex = 4
        Me.GridC_2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_2})
        '
        'GridV_2
        '
        Me.GridV_2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col2_Pedido, Me.col2_Proveedor, Me.col2_Nombre, Me.col2_Fecha, Me.col2_Entrega, Me.col2_Cancela, Me.col2_Pendiente, Me.col2_Bultos, Me.col2_Cajas, Me.col2_Pzas})
        Me.GridV_2.DetailHeight = 284
        Me.GridV_2.GridControl = Me.GridC_2
        Me.GridV_2.Name = "GridV_2"
        Me.GridV_2.OptionsView.ColumnAutoWidth = False
        Me.GridV_2.OptionsView.ShowGroupPanel = False
        '
        'col2_Pedido
        '
        Me.col2_Pedido.Caption = "Pedido"
        Me.col2_Pedido.FieldName = "pedido"
        Me.col2_Pedido.MinWidth = 19
        Me.col2_Pedido.Name = "col2_Pedido"
        Me.col2_Pedido.OptionsColumn.AllowEdit = False
        Me.col2_Pedido.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Pedido.Visible = True
        Me.col2_Pedido.VisibleIndex = 0
        Me.col2_Pedido.Width = 70
        '
        'col2_Proveedor
        '
        Me.col2_Proveedor.Caption = "Proveedor"
        Me.col2_Proveedor.FieldName = "proveedor"
        Me.col2_Proveedor.MinWidth = 19
        Me.col2_Proveedor.Name = "col2_Proveedor"
        Me.col2_Proveedor.OptionsColumn.AllowEdit = False
        Me.col2_Proveedor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Proveedor.Visible = True
        Me.col2_Proveedor.VisibleIndex = 1
        Me.col2_Proveedor.Width = 70
        '
        'col2_Nombre
        '
        Me.col2_Nombre.Caption = "Nombre"
        Me.col2_Nombre.FieldName = "nombre"
        Me.col2_Nombre.MinWidth = 19
        Me.col2_Nombre.Name = "col2_Nombre"
        Me.col2_Nombre.OptionsColumn.AllowEdit = False
        Me.col2_Nombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Nombre.Visible = True
        Me.col2_Nombre.VisibleIndex = 2
        Me.col2_Nombre.Width = 70
        '
        'col2_Fecha
        '
        Me.col2_Fecha.Caption = "Fecha"
        Me.col2_Fecha.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.col2_Fecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col2_Fecha.FieldName = "fecha"
        Me.col2_Fecha.MinWidth = 19
        Me.col2_Fecha.Name = "col2_Fecha"
        Me.col2_Fecha.OptionsColumn.AllowEdit = False
        Me.col2_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Fecha.Visible = True
        Me.col2_Fecha.VisibleIndex = 3
        Me.col2_Fecha.Width = 70
        '
        'col2_Entrega
        '
        Me.col2_Entrega.Caption = "Fec. Entrega"
        Me.col2_Entrega.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.col2_Entrega.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col2_Entrega.FieldName = "fecentrega"
        Me.col2_Entrega.MinWidth = 19
        Me.col2_Entrega.Name = "col2_Entrega"
        Me.col2_Entrega.OptionsColumn.AllowEdit = False
        Me.col2_Entrega.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Entrega.Visible = True
        Me.col2_Entrega.VisibleIndex = 4
        Me.col2_Entrega.Width = 70
        '
        'col2_Cancela
        '
        Me.col2_Cancela.Caption = "Fec. Cancela"
        Me.col2_Cancela.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.col2_Cancela.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col2_Cancela.FieldName = "feccancela"
        Me.col2_Cancela.MinWidth = 19
        Me.col2_Cancela.Name = "col2_Cancela"
        Me.col2_Cancela.OptionsColumn.AllowEdit = False
        Me.col2_Cancela.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Cancela.Visible = True
        Me.col2_Cancela.VisibleIndex = 5
        Me.col2_Cancela.Width = 70
        '
        'col2_Pendiente
        '
        Me.col2_Pendiente.Caption = "Cant. Pendiente"
        Me.col2_Pendiente.DisplayFormat.FormatString = "N2"
        Me.col2_Pendiente.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col2_Pendiente.FieldName = "cantpendiente"
        Me.col2_Pendiente.MinWidth = 19
        Me.col2_Pendiente.Name = "col2_Pendiente"
        Me.col2_Pendiente.OptionsColumn.AllowEdit = False
        Me.col2_Pendiente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Pendiente.Visible = True
        Me.col2_Pendiente.VisibleIndex = 6
        Me.col2_Pendiente.Width = 70
        '
        'col2_Bultos
        '
        Me.col2_Bultos.Caption = "Bultos"
        Me.col2_Bultos.DisplayFormat.FormatString = "N0"
        Me.col2_Bultos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col2_Bultos.FieldName = "bultos"
        Me.col2_Bultos.MinWidth = 19
        Me.col2_Bultos.Name = "col2_Bultos"
        Me.col2_Bultos.OptionsColumn.AllowEdit = False
        Me.col2_Bultos.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Bultos.Visible = True
        Me.col2_Bultos.VisibleIndex = 7
        '
        'col2_Cajas
        '
        Me.col2_Cajas.Caption = "Cajas"
        Me.col2_Cajas.DisplayFormat.FormatString = "N0"
        Me.col2_Cajas.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col2_Cajas.FieldName = "cajas"
        Me.col2_Cajas.MinWidth = 19
        Me.col2_Cajas.Name = "col2_Cajas"
        Me.col2_Cajas.OptionsColumn.AllowEdit = False
        Me.col2_Cajas.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Cajas.Visible = True
        Me.col2_Cajas.VisibleIndex = 8
        '
        'col2_Pzas
        '
        Me.col2_Pzas.Caption = "Pzas"
        Me.col2_Pzas.DisplayFormat.FormatString = "N2"
        Me.col2_Pzas.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col2_Pzas.FieldName = "pzas"
        Me.col2_Pzas.MinWidth = 19
        Me.col2_Pzas.Name = "col2_Pzas"
        Me.col2_Pzas.OptionsColumn.AllowEdit = False
        Me.col2_Pzas.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Pzas.Visible = True
        Me.col2_Pzas.VisibleIndex = 9
        '
        'Calendario1
        '
        Me.Calendario1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Calendario1.Location = New System.Drawing.Point(23, 37)
        Me.Calendario1.Margin = New System.Windows.Forms.Padding(2)
        Me.Calendario1.MinValue = New Date(2019, 11, 1, 0, 0, 0, 0)
        Me.Calendario1.Name = "Calendario1"
        Me.Calendario1.Size = New System.Drawing.Size(262, 235)
        Me.Calendario1.TabIndex = 3
        '
        'GridC_1
        '
        Me.GridC_1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_1.Location = New System.Drawing.Point(9, 37)
        Me.GridC_1.LookAndFeel.SkinName = "Office 2010 Black"
        Me.GridC_1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(357, 199)
        Me.GridC_1.TabIndex = 2
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col_Id, Me.col_Valor})
        Me.GridV_1.DetailHeight = 284
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col_Id
        '
        Me.col_Id.AppearanceCell.Options.UseTextOptions = True
        Me.col_Id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.col_Id.Caption = "Id"
        Me.col_Id.FieldName = "id"
        Me.col_Id.MaxWidth = 56
        Me.col_Id.MinWidth = 15
        Me.col_Id.Name = "col_Id"
        Me.col_Id.OptionsColumn.AllowEdit = False
        Me.col_Id.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Id.Visible = True
        Me.col_Id.VisibleIndex = 0
        Me.col_Id.Width = 56
        '
        'col_Valor
        '
        Me.col_Valor.Caption = "Valor"
        Me.col_Valor.FieldName = "valor"
        Me.col_Valor.MinWidth = 15
        Me.col_Valor.Name = "col_Valor"
        Me.col_Valor.OptionsColumn.AllowEdit = False
        Me.col_Valor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col_Valor.Visible = True
        Me.col_Valor.VisibleIndex = 1
        Me.col_Valor.Width = 56
        '
        'lbl_Etiqueta
        '
        Me.lbl_Etiqueta.Appearance.Font = New System.Drawing.Font("Tahoma", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Etiqueta.Appearance.Options.UseFont = True
        Me.lbl_Etiqueta.Location = New System.Drawing.Point(9, 9)
        Me.lbl_Etiqueta.Margin = New System.Windows.Forms.Padding(2)
        Me.lbl_Etiqueta.Name = "lbl_Etiqueta"
        Me.lbl_Etiqueta.Size = New System.Drawing.Size(79, 22)
        Me.lbl_Etiqueta.TabIndex = 1
        Me.lbl_Etiqueta.Text = "Elije un ..."
        '
        'SelCombobox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 287)
        Me.Controls.Add(Me.PanelFiltros_Acciones)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "SelCombobox"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SelCombobox"
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        CType(Me.GridC_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Calendario1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents lbl_Etiqueta As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col_Id As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col_Valor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Calendario1 As DevExpress.XtraEditors.Controls.CalendarControl
    Friend WithEvents GridC_2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col2_Pedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Proveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Nombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Entrega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Cancela As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Pendiente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Bultos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Cajas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Pzas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridC_3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col3_IdEstado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col3_IdMunicipio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col3_IdAsentamiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col3_Asentamiento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col3_CP As DevExpress.XtraGrid.Columns.GridColumn
End Class
