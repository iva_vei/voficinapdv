﻿Public Class LogMasivo
    Private _log1 As List(Of String)

    Public Property Log As List(Of String)
        Get
            Return _log1
        End Get
        Set(value As List(Of String))
            _log1 = value
        End Set
    End Property

    Private Sub NewsFeed_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.Lines = _log1.ToArray
    End Sub

End Class