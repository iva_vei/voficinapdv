﻿Public Class pdvCuentas
    Enum eTipoForma
        Cliente
        ClienteFinal
        Empleados
    End Enum
    Dim _Accion As Boolean
    Dim _Cuenta As String
    Dim _Nombre As String
    Dim _Tarjeta As String
    Dim _Empresa As String
    Dim _Rfc As String
    Dim _Estatus As String
    Dim _Direccion As String
    Dim _Colonia As String
    Dim _Ciudad As String
    Dim _Estado As String
    Dim _CodigoPostal As String
    Dim _Vendedor As String
    Dim _email As String
    Dim _descuento As Decimal
    Dim _Tipo As String
    Dim _TipoForma As eTipoForma
    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public ReadOnly Property Cuenta As String
        Get
            Return _Cuenta
        End Get
    End Property

    Public Property Nombre As String
        Get
            Return _Nombre
        End Get
        Set(value As String)
            _Nombre = value
        End Set
    End Property

    Public Property Tarjeta As String
        Get
            Return _Tarjeta
        End Get
        Set(value As String)
            _Tarjeta = value
        End Set
    End Property

    Public Property Empresa As String
        Get
            Return _Empresa
        End Get
        Set(value As String)
            _Empresa = value
        End Set
    End Property

    Public Property Rfc As String
        Get
            Return _Rfc
        End Get
        Set(value As String)
            _Rfc = value
        End Set
    End Property

    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(value As String)
            _Estatus = value
        End Set
    End Property

    Public Property Direccion As String
        Get
            Return _Direccion
        End Get
        Set(value As String)
            _Direccion = value
        End Set
    End Property

    Public Property Colonia As String
        Get
            Return _Colonia
        End Get
        Set(value As String)
            _Colonia = value
        End Set
    End Property

    Public Property Ciudad As String
        Get
            Return _Ciudad
        End Get
        Set(value As String)
            _Ciudad = value
        End Set
    End Property

    Public Property Estado As String
        Get
            Return _Estado
        End Get
        Set(value As String)
            _Estado = value
        End Set
    End Property

    Public Property CodigoPostal As String
        Get
            Return _CodigoPostal
        End Get
        Set(value As String)
            _CodigoPostal = value
        End Set
    End Property

    Public Property Vendedor As String
        Get
            Return _Vendedor
        End Get
        Set(value As String)
            _Vendedor = value
        End Set
    End Property

    Public Property EMail As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property TipoForma As eTipoForma
        Get
            Return _TipoForma
        End Get
        Set(value As eTipoForma)
            _TipoForma = value
        End Set
    End Property

    Public Property Descuento As Decimal
        Get
            Return _descuento
        End Get
        Set(value As Decimal)
            _descuento = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _Tipo
        End Get
        Set(value As String)
            _Tipo = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If _TipoForma = eTipoForma.Empleados Then
                Me.Text = "Empleados"
                txt_Estatus.Text = "ALTA"
                GridC_1.MainView = GridV_2

                lbl_Cuenta.Text = "No Empleado"
                lbl_Tarjeta.Visible = False
                lbl_RFC.Visible = False
                lbl_Empresa.Visible = False
                txt_Tarjeta.Visible = False
                txt_RFC.Visible = False
                txt_Empresa.Visible = False
            Else
                Me.Text = "Clientes"
                txt_Estatus.Text = "NORMAL"
                GridC_1.MainView = GridV_1
            End If

            Call Refrescar()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi

            If txt_Cuenta.Text <> "" Or txt_Tarjeta.Text <> "" Or txt_Empresa.Text <> "" Or txt_Nombre.Text <> "" Or txt_RFC.Text <> "" Then
                If _TipoForma = eTipoForma.Cliente Then
                    If oDatos.PVTA_Recupera_Cuentas(txt_Cuenta.Text, txt_Tarjeta.Text, txt_Empresa.Text, txt_Nombre.Text, txt_RFC.Text, txt_Estatus.Text, _Tipo, dtDatos, Mensaje) Then
                        GridC_1.DataSource = dtDatos
                        GridC_1.RefreshDataSource()
                        GridV_1.BestFitColumns()
                    Else
                        GridC_1.DataSource = Nothing
                        GridC_1.RefreshDataSource()
                    End If
                ElseIf _TipoForma = eTipoForma.ClienteFinal Then
                    If oDatos.PVTA_Recupera_CuentasFinal(txt_Cuenta.Text, txt_Tarjeta.Text, txt_Empresa.Text, txt_Nombre.Text, txt_RFC.Text, txt_Estatus.Text, _Tipo, dtDatos, Mensaje) Then
                        GridC_1.DataSource = dtDatos
                        GridC_1.RefreshDataSource()
                        GridV_1.BestFitColumns()
                    Else
                        GridC_1.DataSource = Nothing
                        GridC_1.RefreshDataSource()
                    End If
                Else
                    If oDatos.PVTA_Empleados("", "", txt_Cuenta.Text, txt_Nombre.Text, txt_Estatus.Text, dtDatos, Mensaje) Then
                        GridC_1.DataSource = dtDatos
                        GridC_1.RefreshDataSource()
                        GridV_1.BestFitColumns()
                    Else
                        GridC_1.DataSource = Nothing
                        GridC_1.RefreshDataSource()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim sw_continuar As Boolean = True
        Dim Requeridos As Double = 0
        Dim Disponible As Double = 0

        Try
            _Accion = True
            If _TipoForma = eTipoForma.Cliente Then
                If GridV_1.RowCount > 0 Then
                    _Cuenta = GridV_1.GetFocusedRowCellValue(col1_Cuenta)
                    _Nombre = GridV_1.GetFocusedRowCellValue(col1_Nombre)
                    _Estatus = GridV_1.GetFocusedRowCellValue(col1_Estatus)
                    _Tarjeta = GridV_1.GetFocusedRowCellValue(col1_Tarjeta)
                    _Rfc = GridV_1.GetFocusedRowCellValue(col1_Rfc)
                    _Empresa = GridV_1.GetFocusedRowCellValue(col1_Empresa)
                    _Direccion = GridV_1.GetFocusedRowCellValue(col1_Dircasa1)
                    _Colonia = GridV_1.GetFocusedRowCellValue(col1_Colcasa)
                    _Ciudad = GridV_1.GetFocusedRowCellValue(col1_Cdcasa)
                    _Estado = GridV_1.GetFocusedRowCellValue(col1_Estado)
                    _CodigoPostal = GridV_1.GetFocusedRowCellValue(col1_Cpcasa)
                    _email = GridV_1.GetFocusedRowCellValue(col1_EMail)
                    Vendedor = GridV_1.GetFocusedRowCellValue(col1_Agente)
                    _descuento = GridV_1.GetFocusedRowCellValue(col1_Descuento)
                    Me.Close()
                End If
            ElseIf _TipoForma = eTipoForma.ClienteFinal Then
                If GridV_1.RowCount > 0 Then
                    _Cuenta = GridV_1.GetFocusedRowCellValue(col1_Cuenta)
                    _Nombre = GridV_1.GetFocusedRowCellValue(col1_Nombre)
                    _Estatus = GridV_1.GetFocusedRowCellValue(col1_Estatus)
                    _Tarjeta = GridV_1.GetFocusedRowCellValue(col1_Tarjeta)
                    _Rfc = GridV_1.GetFocusedRowCellValue(col1_Rfc)
                    _Empresa = GridV_1.GetFocusedRowCellValue(col1_Empresa)
                    _Direccion = GridV_1.GetFocusedRowCellValue(col1_Dircasa1)
                    _Colonia = GridV_1.GetFocusedRowCellValue(col1_Colcasa)
                    _Ciudad = GridV_1.GetFocusedRowCellValue(col1_Cdcasa)
                    _Estado = GridV_1.GetFocusedRowCellValue(col1_Estado)
                    _CodigoPostal = GridV_1.GetFocusedRowCellValue(col1_Cpcasa)
                    _email = GridV_1.GetFocusedRowCellValue(col1_EMail)
                    Vendedor = GridV_1.GetFocusedRowCellValue(col1_Agente)
                    _descuento = GridV_1.GetFocusedRowCellValue(col1_Descuento)
                    Me.Close()
                End If
            Else
                If GridV_2.RowCount > 0 Then
                    _Cuenta = "C" & GridV_2.GetFocusedRowCellValue(col2_Empleado)
                    _Nombre = GridV_2.GetFocusedRowCellValue(col2_Nombre)
                    _Estatus = GridV_2.GetFocusedRowCellValue(col2_Estatus)
                    _Tarjeta = ""
                    _Rfc = ""
                    _Empresa = ""
                    _Direccion = ""
                    _Colonia = ""
                    _Ciudad = ""
                    _Estado = ""
                    _CodigoPostal = ""
                    _email = ""
                    Vendedor = ""
                    _descuento = GridV_1.GetFocusedRowCellValue(col2_Descuento)
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()
    End Sub

    Private Sub pdvCuentas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Try
            txt_Cuenta.Focus()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub PierdeFoco(sender As Object, e As EventArgs) Handles txt_Cuenta.LostFocus _
                                                                    , txt_Tarjeta.LostFocus _
                                                                    , txt_RFC.LostFocus _
                                                                    , txt_Nombre.LostFocus _
                                                                    , txt_Empresa.LostFocus
        Try
            If txt_Cuenta.Text <> "" _
            Or txt_Tarjeta.Text <> "" _
            Or txt_Empresa.Text <> "" _
            Or txt_Nombre.Text <> "" _
            Or txt_RFC.Text <> "" Then
                Call Refrescar()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
        Call btn_Aceptar_Click(Nothing, Nothing)
    End Sub
End Class