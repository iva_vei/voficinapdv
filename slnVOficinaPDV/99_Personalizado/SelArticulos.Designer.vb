﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SelArticulos
    Inherits DevExpress.XtraEditors.XtraForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    '''
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelArticulos))
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions3 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.layoutControl = New DevExpress.XtraLayout.LayoutControl()
        Me.btn_OcultarMostrar_Acciones = New DevExpress.XtraEditors.SimpleButton()
        Me.TabPane1 = New DevExpress.XtraBars.Navigation.TabPane()
        Me.tnpPage1 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Articulo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Nombre = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_NomCorto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Precio1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Precio2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Precio3 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_UnidadVta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TipoArt = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Origen = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.band_existencia = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Sucursal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Existencias = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand7 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_NomDivision = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_NomDepto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Familia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_ClaveDivision = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ClaveDepto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ClaveFamilia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand3 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Codigo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_CodigoCaja = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_CodbarraA1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_CodbarraA2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cuentactb8 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Precio4 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Precio5 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Marca = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Estilo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.layoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem1 = New DevExpress.XtraLayout.SplitterItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.SplitterItem2 = New DevExpress.XtraLayout.SplitterItem()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_Clase = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_Marca = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_CodigoBarra = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Codigoref = New System.Windows.Forms.TextBox()
        Me.chk_Existencias = New System.Windows.Forms.CheckBox()
        Me.txt_Articulo = New System.Windows.Forms.TextBox()
        Me.Txt_Familia = New System.Windows.Forms.MaskedTextBox()
        Me.txt_Estructura1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_NomCorto = New System.Windows.Forms.MaskedTextBox()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.pop_Menu1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsm_AbrirCaja = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_CerrarCaja = New System.Windows.Forms.ToolStripMenuItem()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1_Caracteristicas = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.layoutControl.SuspendLayout()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPane1.SuspendLayout()
        Me.tnpPage1.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelFiltros_Acciones.SuspendLayout()
        Me.pop_Menu1.SuspendLayout()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'layoutControl
        '
        Me.layoutControl.AllowCustomization = False
        Me.layoutControl.Controls.Add(Me.btn_OcultarMostrar_Acciones)
        Me.layoutControl.Controls.Add(Me.TabPane1)
        Me.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.layoutControl.Location = New System.Drawing.Point(0, 0)
        Me.layoutControl.Name = "layoutControl"
        Me.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(461, 277, 450, 400)
        Me.layoutControl.Root = Me.layoutControlGroup1
        Me.layoutControl.Size = New System.Drawing.Size(1078, 571)
        Me.layoutControl.TabIndex = 0
        '
        'btn_OcultarMostrar_Acciones
        '
        Me.btn_OcultarMostrar_Acciones.Location = New System.Drawing.Point(36, 547)
        Me.btn_OcultarMostrar_Acciones.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_OcultarMostrar_Acciones.Name = "btn_OcultarMostrar_Acciones"
        Me.btn_OcultarMostrar_Acciones.Size = New System.Drawing.Size(270, 22)
        Me.btn_OcultarMostrar_Acciones.StyleController = Me.layoutControl
        Me.btn_OcultarMostrar_Acciones.TabIndex = 0
        Me.btn_OcultarMostrar_Acciones.Text = "Ocultar / Mostrar Filtros y Acciones"
        '
        'TabPane1
        '
        Me.TabPane1.Controls.Add(Me.tnpPage1)
        Me.TabPane1.Location = New System.Drawing.Point(36, 2)
        Me.TabPane1.Name = "TabPane1"
        Me.TabPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.tnpPage1})
        Me.TabPane1.RegularSize = New System.Drawing.Size(996, 541)
        Me.TabPane1.SelectedPage = Me.tnpPage1
        Me.TabPane1.Size = New System.Drawing.Size(996, 541)
        Me.TabPane1.TabIndex = 0
        Me.TabPane1.Text = "TabPane1"
        '
        'tnpPage1
        '
        Me.tnpPage1.Caption = "Indice"
        Me.tnpPage1.Controls.Add(Me.GridC_1)
        Me.tnpPage1.Name = "tnpPage1"
        Me.tnpPage1.Size = New System.Drawing.Size(996, 508)
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GridC_1.Location = New System.Drawing.Point(0, 0)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(996, 508)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.band_existencia, Me.gridBand7, Me.gridBand2, Me.gridBand3})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_Articulo, Me.col1_Nombre, Me.col1_Precio1, Me.col1_UnidadVta, Me.col1_NomCorto, Me.col1_Origen, Me.col1_TipoArt, Me.col1_NomDivision, Me.col1_NomDepto, Me.col1_Familia, Me.col1_ClaveDivision, Me.col1_ClaveDepto, Me.col1_ClaveFamilia, Me.col1_Sucursal, Me.col1_Existencias, Me.col1_Precio2, Me.col1_Precio4, Me.col1_Precio5, Me.col1_Codigo, Me.col1_CodigoCaja, Me.col1_CodbarraA1, Me.col1_CodbarraA2, Me.col1_Cuentactb8, Me.col1_Marca, Me.col1_Precio3, Me.col1_Estilo, Me.col1_Caracteristicas})
        Me.GridV_1.DetailHeight = 284
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.AppearanceHeader.Options.UseTextOptions = True
        Me.GridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridBand1.Caption = "GENERALES"
        Me.GridBand1.Columns.Add(Me.col1_Articulo)
        Me.GridBand1.Columns.Add(Me.col1_Nombre)
        Me.GridBand1.Columns.Add(Me.col1_NomCorto)
        Me.GridBand1.Columns.Add(Me.col1_Precio1)
        Me.GridBand1.Columns.Add(Me.col1_Precio2)
        Me.GridBand1.Columns.Add(Me.col1_Precio3)
        Me.GridBand1.Columns.Add(Me.col1_UnidadVta)
        Me.GridBand1.Columns.Add(Me.col1_TipoArt)
        Me.GridBand1.Columns.Add(Me.col1_Origen)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 675
        '
        'col1_Articulo
        '
        Me.col1_Articulo.Caption = "Articulo"
        Me.col1_Articulo.FieldName = "articulo"
        Me.col1_Articulo.Name = "col1_Articulo"
        Me.col1_Articulo.OptionsColumn.AllowEdit = False
        Me.col1_Articulo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Articulo.Visible = True
        '
        'col1_Nombre
        '
        Me.col1_Nombre.Caption = "Nombre"
        Me.col1_Nombre.FieldName = "nombre"
        Me.col1_Nombre.Name = "col1_Nombre"
        Me.col1_Nombre.OptionsColumn.AllowEdit = False
        Me.col1_Nombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Nombre.Visible = True
        '
        'col1_NomCorto
        '
        Me.col1_NomCorto.Caption = "NomCorto"
        Me.col1_NomCorto.FieldName = "nomcorto"
        Me.col1_NomCorto.Name = "col1_NomCorto"
        Me.col1_NomCorto.OptionsColumn.AllowEdit = False
        Me.col1_NomCorto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_NomCorto.Visible = True
        '
        'col1_Precio1
        '
        Me.col1_Precio1.Caption = "Precio"
        Me.col1_Precio1.DisplayFormat.FormatString = "C2"
        Me.col1_Precio1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Precio1.FieldName = "precio1"
        Me.col1_Precio1.Name = "col1_Precio1"
        Me.col1_Precio1.OptionsColumn.AllowEdit = False
        Me.col1_Precio1.Visible = True
        '
        'col1_Precio2
        '
        Me.col1_Precio2.Caption = "Mayoreo"
        Me.col1_Precio2.DisplayFormat.FormatString = "C2"
        Me.col1_Precio2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Precio2.FieldName = "precio2"
        Me.col1_Precio2.Name = "col1_Precio2"
        Me.col1_Precio2.OptionsColumn.AllowEdit = False
        Me.col1_Precio2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[True]
        Me.col1_Precio2.Visible = True
        '
        'col1_Precio3
        '
        Me.col1_Precio3.Caption = "Precio3"
        Me.col1_Precio3.DisplayFormat.FormatString = "C2"
        Me.col1_Precio3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Precio3.FieldName = "precio3"
        Me.col1_Precio3.Name = "col1_Precio3"
        Me.col1_Precio3.OptionsColumn.AllowEdit = False
        Me.col1_Precio3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[True]
        Me.col1_Precio3.Visible = True
        '
        'col1_UnidadVta
        '
        Me.col1_UnidadVta.Caption = "UnidadVta"
        Me.col1_UnidadVta.FieldName = "unidadvta"
        Me.col1_UnidadVta.Name = "col1_UnidadVta"
        Me.col1_UnidadVta.OptionsColumn.AllowEdit = False
        Me.col1_UnidadVta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_UnidadVta.Visible = True
        '
        'col1_TipoArt
        '
        Me.col1_TipoArt.Caption = "TipoArt"
        Me.col1_TipoArt.FieldName = "tipoart"
        Me.col1_TipoArt.Name = "col1_TipoArt"
        Me.col1_TipoArt.OptionsColumn.AllowEdit = False
        Me.col1_TipoArt.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TipoArt.Visible = True
        '
        'col1_Origen
        '
        Me.col1_Origen.Caption = "Origen"
        Me.col1_Origen.FieldName = "origen"
        Me.col1_Origen.Name = "col1_Origen"
        Me.col1_Origen.OptionsColumn.AllowEdit = False
        Me.col1_Origen.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Origen.Visible = True
        '
        'band_existencia
        '
        Me.band_existencia.Caption = "Otras Tiendas"
        Me.band_existencia.Columns.Add(Me.col1_Sucursal)
        Me.band_existencia.Columns.Add(Me.col1_Existencias)
        Me.band_existencia.Name = "band_existencia"
        Me.band_existencia.VisibleIndex = 1
        Me.band_existencia.Width = 150
        '
        'col1_Sucursal
        '
        Me.col1_Sucursal.Caption = "Sucursal"
        Me.col1_Sucursal.FieldName = "id_sucursal"
        Me.col1_Sucursal.Name = "col1_Sucursal"
        Me.col1_Sucursal.OptionsColumn.AllowEdit = False
        Me.col1_Sucursal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Sucursal.Visible = True
        '
        'col1_Existencias
        '
        Me.col1_Existencias.Caption = "Exist"
        Me.col1_Existencias.FieldName = "existencia"
        Me.col1_Existencias.Name = "col1_Existencias"
        Me.col1_Existencias.OptionsColumn.AllowEdit = False
        Me.col1_Existencias.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Existencias.Visible = True
        '
        'gridBand7
        '
        Me.gridBand7.AppearanceHeader.Options.UseTextOptions = True
        Me.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridBand7.Caption = "ESTRUCTURA"
        Me.gridBand7.Columns.Add(Me.col1_NomDivision)
        Me.gridBand7.Columns.Add(Me.col1_NomDepto)
        Me.gridBand7.Columns.Add(Me.col1_Familia)
        Me.gridBand7.Name = "gridBand7"
        Me.gridBand7.VisibleIndex = 2
        Me.gridBand7.Width = 75
        '
        'col1_NomDivision
        '
        Me.col1_NomDivision.Caption = "Division"
        Me.col1_NomDivision.FieldName = "nomdivision"
        Me.col1_NomDivision.Name = "col1_NomDivision"
        Me.col1_NomDivision.OptionsColumn.AllowEdit = False
        Me.col1_NomDivision.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_NomDepto
        '
        Me.col1_NomDepto.Caption = "Depto"
        Me.col1_NomDepto.FieldName = "nomdepto"
        Me.col1_NomDepto.Name = "col1_NomDepto"
        Me.col1_NomDepto.OptionsColumn.AllowEdit = False
        Me.col1_NomDepto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Familia
        '
        Me.col1_Familia.Caption = "Familia"
        Me.col1_Familia.FieldName = "nomfamilia"
        Me.col1_Familia.Name = "col1_Familia"
        Me.col1_Familia.OptionsColumn.AllowEdit = False
        Me.col1_Familia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Familia.Visible = True
        '
        'gridBand2
        '
        Me.gridBand2.AppearanceHeader.Options.UseTextOptions = True
        Me.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridBand2.Caption = "CLAVES"
        Me.gridBand2.Columns.Add(Me.col1_ClaveDivision)
        Me.gridBand2.Columns.Add(Me.col1_ClaveDepto)
        Me.gridBand2.Columns.Add(Me.col1_ClaveFamilia)
        Me.gridBand2.Name = "gridBand2"
        Me.gridBand2.Visible = False
        Me.gridBand2.VisibleIndex = -1
        Me.gridBand2.Width = 225
        '
        'col1_ClaveDivision
        '
        Me.col1_ClaveDivision.Caption = "Div"
        Me.col1_ClaveDivision.FieldName = "clave_division"
        Me.col1_ClaveDivision.Name = "col1_ClaveDivision"
        Me.col1_ClaveDivision.OptionsColumn.AllowEdit = False
        Me.col1_ClaveDivision.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ClaveDivision.Visible = True
        '
        'col1_ClaveDepto
        '
        Me.col1_ClaveDepto.Caption = "Dep"
        Me.col1_ClaveDepto.FieldName = "clave_depto"
        Me.col1_ClaveDepto.Name = "col1_ClaveDepto"
        Me.col1_ClaveDepto.OptionsColumn.AllowEdit = False
        Me.col1_ClaveDepto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ClaveDepto.Visible = True
        '
        'col1_ClaveFamilia
        '
        Me.col1_ClaveFamilia.Caption = "Fam"
        Me.col1_ClaveFamilia.FieldName = "clave_familia"
        Me.col1_ClaveFamilia.Name = "col1_ClaveFamilia"
        Me.col1_ClaveFamilia.OptionsColumn.AllowEdit = False
        Me.col1_ClaveFamilia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ClaveFamilia.Visible = True
        '
        'gridBand3
        '
        Me.gridBand3.AppearanceHeader.Options.UseTextOptions = True
        Me.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridBand3.Caption = "UPC"
        Me.gridBand3.Columns.Add(Me.col1_Codigo)
        Me.gridBand3.Columns.Add(Me.col1_CodigoCaja)
        Me.gridBand3.Columns.Add(Me.col1_CodbarraA1)
        Me.gridBand3.Columns.Add(Me.col1_CodbarraA2)
        Me.gridBand3.Columns.Add(Me.col1_Cuentactb8)
        Me.gridBand3.Name = "gridBand3"
        Me.gridBand3.VisibleIndex = 3
        Me.gridBand3.Width = 375
        '
        'col1_Codigo
        '
        Me.col1_Codigo.Caption = "UPC1"
        Me.col1_Codigo.FieldName = "codigo"
        Me.col1_Codigo.Name = "col1_Codigo"
        Me.col1_Codigo.OptionsColumn.AllowEdit = False
        Me.col1_Codigo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Codigo.Visible = True
        '
        'col1_CodigoCaja
        '
        Me.col1_CodigoCaja.Caption = "UPC2"
        Me.col1_CodigoCaja.FieldName = "codigocaja"
        Me.col1_CodigoCaja.Name = "col1_CodigoCaja"
        Me.col1_CodigoCaja.OptionsColumn.AllowEdit = False
        Me.col1_CodigoCaja.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CodigoCaja.Visible = True
        '
        'col1_CodbarraA1
        '
        Me.col1_CodbarraA1.Caption = "UPC3"
        Me.col1_CodbarraA1.FieldName = "codbarra_a1"
        Me.col1_CodbarraA1.Name = "col1_CodbarraA1"
        Me.col1_CodbarraA1.OptionsColumn.AllowEdit = False
        Me.col1_CodbarraA1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CodbarraA1.Visible = True
        '
        'col1_CodbarraA2
        '
        Me.col1_CodbarraA2.Caption = "UPC4"
        Me.col1_CodbarraA2.FieldName = "codbarra_a2"
        Me.col1_CodbarraA2.Name = "col1_CodbarraA2"
        Me.col1_CodbarraA2.OptionsColumn.AllowEdit = False
        Me.col1_CodbarraA2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CodbarraA2.Visible = True
        '
        'col1_Cuentactb8
        '
        Me.col1_Cuentactb8.Caption = "UPC5"
        Me.col1_Cuentactb8.FieldName = "cuentactb8"
        Me.col1_Cuentactb8.Name = "col1_Cuentactb8"
        Me.col1_Cuentactb8.OptionsColumn.AllowEdit = False
        Me.col1_Cuentactb8.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Cuentactb8.Visible = True
        '
        'col1_Precio4
        '
        Me.col1_Precio4.Caption = "Precio4"
        Me.col1_Precio4.DisplayFormat.FormatString = "C2"
        Me.col1_Precio4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Precio4.FieldName = "precio4"
        Me.col1_Precio4.Name = "col1_Precio4"
        Me.col1_Precio4.OptionsColumn.AllowEdit = False
        Me.col1_Precio4.Visible = True
        '
        'col1_Precio5
        '
        Me.col1_Precio5.Caption = "Precio5"
        Me.col1_Precio5.DisplayFormat.FormatString = "N0"
        Me.col1_Precio5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Precio5.FieldName = "precio5"
        Me.col1_Precio5.Name = "col1_Precio5"
        Me.col1_Precio5.OptionsColumn.AllowEdit = False
        Me.col1_Precio5.Visible = True
        '
        'col1_Marca
        '
        Me.col1_Marca.Caption = "Marca"
        Me.col1_Marca.FieldName = "marca"
        Me.col1_Marca.Name = "col1_Marca"
        Me.col1_Marca.OptionsColumn.AllowEdit = False
        Me.col1_Marca.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[True]
        Me.col1_Marca.Visible = True
        Me.col1_Marca.Width = 64
        '
        'col1_Estilo
        '
        Me.col1_Estilo.Caption = "Estilo"
        Me.col1_Estilo.FieldName = "estilo"
        Me.col1_Estilo.Name = "col1_Estilo"
        Me.col1_Estilo.Visible = True
        '
        'layoutControlGroup1
        '
        Me.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.layoutControlGroup1.GroupBordersVisible = False
        Me.layoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.SplitterItem1, Me.EmptySpaceItem2, Me.SplitterItem2})
        Me.layoutControlGroup1.Name = "layoutControlGroup1"
        Me.layoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(34, 34, 0, 0)
        Me.layoutControlGroup1.Size = New System.Drawing.Size(1078, 571)
        Me.layoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.TabPane1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1000, 545)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.btn_OcultarMostrar_Acciones
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 545)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(274, 26)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'SplitterItem1
        '
        Me.SplitterItem1.AllowHotTrack = True
        Me.SplitterItem1.Location = New System.Drawing.Point(1000, 0)
        Me.SplitterItem1.Name = "SplitterItem1"
        Me.SplitterItem1.Size = New System.Drawing.Size(10, 571)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(284, 545)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(716, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'SplitterItem2
        '
        Me.SplitterItem2.AllowHotTrack = True
        Me.SplitterItem2.Location = New System.Drawing.Point(274, 545)
        Me.SplitterItem2.Name = "SplitterItem2"
        Me.SplitterItem2.Size = New System.Drawing.Size(10, 26)
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.Image = CType(resources.GetObject("WindowsUIButtonImageOptions1.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions2.ImageUri.Uri = "Apply;GrayScaled"
        WindowsUIButtonImageOptions3.ImageUri.Uri = "Cancel;GrayScaled"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Imprime Preciador", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "PRECIADOR", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUISeparator(), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Ok", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", True, WindowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label6)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Clase)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label5)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Marca)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label4)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_CodigoBarra)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label3)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Codigoref)
        Me.PanelFiltros_Acciones.Controls.Add(Me.chk_Existencias)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Articulo)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Txt_Familia)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Estructura1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Button1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_NomCorto)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Nombre)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(0, 571)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 120)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(60, 60)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(1078, 120)
        Me.PanelFiltros_Acciones.TabIndex = 1
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 91)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 19)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Clase"
        '
        'txt_Clase
        '
        Me.txt_Clase.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Clase.Location = New System.Drawing.Point(122, 87)
        Me.txt_Clase.Name = "txt_Clase"
        Me.txt_Clase.Size = New System.Drawing.Size(178, 27)
        Me.txt_Clase.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 63)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 19)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Marca"
        '
        'txt_Marca
        '
        Me.txt_Marca.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Marca.Location = New System.Drawing.Point(122, 59)
        Me.txt_Marca.Name = "txt_Marca"
        Me.txt_Marca.Size = New System.Drawing.Size(178, 27)
        Me.txt_Marca.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 19)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Codigo Barras"
        '
        'txt_CodigoBarra
        '
        Me.txt_CodigoBarra.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CodigoBarra.Location = New System.Drawing.Point(122, 31)
        Me.txt_CodigoBarra.Name = "txt_CodigoBarra"
        Me.txt_CodigoBarra.Size = New System.Drawing.Size(178, 27)
        Me.txt_CodigoBarra.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 19)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "CodigoRef"
        '
        'txt_Codigoref
        '
        Me.txt_Codigoref.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Codigoref.Location = New System.Drawing.Point(122, 3)
        Me.txt_Codigoref.Name = "txt_Codigoref"
        Me.txt_Codigoref.Size = New System.Drawing.Size(178, 27)
        Me.txt_Codigoref.TabIndex = 0
        '
        'chk_Existencias
        '
        Me.chk_Existencias.AutoSize = True
        Me.chk_Existencias.Location = New System.Drawing.Point(569, 94)
        Me.chk_Existencias.Name = "chk_Existencias"
        Me.chk_Existencias.Size = New System.Drawing.Size(138, 17)
        Me.chk_Existencias.TabIndex = 10
        Me.chk_Existencias.Text = "Ver Existencias Tiendas"
        Me.chk_Existencias.UseVisualStyleBackColor = True
        '
        'txt_Articulo
        '
        Me.txt_Articulo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Articulo.Location = New System.Drawing.Point(569, 58)
        Me.txt_Articulo.Name = "txt_Articulo"
        Me.txt_Articulo.Size = New System.Drawing.Size(140, 27)
        Me.txt_Articulo.TabIndex = 9
        Me.txt_Articulo.Visible = False
        '
        'Txt_Familia
        '
        Me.Txt_Familia.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Familia.Location = New System.Drawing.Point(385, 88)
        Me.Txt_Familia.Name = "Txt_Familia"
        Me.Txt_Familia.Size = New System.Drawing.Size(178, 27)
        Me.Txt_Familia.TabIndex = 8
        '
        'txt_Estructura1
        '
        Me.txt_Estructura1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Estructura1.Location = New System.Drawing.Point(385, 58)
        Me.txt_Estructura1.Name = "txt_Estructura1"
        Me.txt_Estructura1.Size = New System.Drawing.Size(178, 27)
        Me.txt_Estructura1.TabIndex = 7
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(329, 61)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(50, 50)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&Familia"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(325, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 19)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "NomCorto"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(325, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 19)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Nombre"
        '
        'txt_NomCorto
        '
        Me.txt_NomCorto.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NomCorto.Location = New System.Drawing.Point(423, 30)
        Me.txt_NomCorto.Name = "txt_NomCorto"
        Me.txt_NomCorto.Size = New System.Drawing.Size(178, 27)
        Me.txt_NomCorto.TabIndex = 5
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(423, 2)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(178, 27)
        Me.txt_Nombre.TabIndex = 4
        '
        'pop_Menu1
        '
        Me.pop_Menu1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.pop_Menu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_AbrirCaja, Me.tsm_CerrarCaja})
        Me.pop_Menu1.Name = "pop_Menu1"
        Me.pop_Menu1.Size = New System.Drawing.Size(133, 48)
        '
        'tsm_AbrirCaja
        '
        Me.tsm_AbrirCaja.Name = "tsm_AbrirCaja"
        Me.tsm_AbrirCaja.Size = New System.Drawing.Size(132, 22)
        Me.tsm_AbrirCaja.Text = "Cerrar Caja"
        '
        'tsm_CerrarCaja
        '
        Me.tsm_CerrarCaja.Name = "tsm_CerrarCaja"
        Me.tsm_CerrarCaja.Size = New System.Drawing.Size(132, 22)
        Me.tsm_CerrarCaja.Text = "Cerrar Caja"
        '
        'GridView1
        '
        Me.GridView1.Name = "GridView1"
        '
        'col1_Caracteristicas
        '
        Me.col1_Caracteristicas.Caption = "Caracteristicas"
        Me.col1_Caracteristicas.FieldName = "caracteristicas"
        Me.col1_Caracteristicas.Name = "col1_Caracteristicas"
        Me.col1_Caracteristicas.Visible = True
        '
        'SelArticulos
        '
        Me.Appearance.BackColor = System.Drawing.Color.White
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1078, 691)
        Me.Controls.Add(Me.layoutControl)
        Me.Controls.Add(Me.PanelFiltros_Acciones)
        Me.KeyPreview = True
        Me.Name = "SelArticulos"
        Me.Text = "Articulos"
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.layoutControl.ResumeLayout(False)
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPane1.ResumeLayout(False)
        Me.tnpPage1.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        Me.pop_Menu1.ResumeLayout(False)
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents layoutControl As DevExpress.XtraLayout.LayoutControl
    Private WithEvents layoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents SplitterItem1 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents btn_OcultarMostrar_Acciones As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SplitterItem2 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents pop_Menu1 As ContextMenuStrip
    Friend WithEvents tsm_AbrirCaja As ToolStripMenuItem
    Friend WithEvents tsm_CerrarCaja As ToolStripMenuItem
    Friend WithEvents TabPane1 As DevExpress.XtraBars.Navigation.TabPane
    Friend WithEvents tnpPage1 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1_Articulo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Nombre As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_NomCorto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_UnidadVta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TipoArt As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Origen As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_NomDivision As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_NomDepto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Familia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ClaveDivision As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ClaveDepto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ClaveFamilia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents Txt_Familia As MaskedTextBox
    Friend WithEvents txt_Estructura1 As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_NomCorto As MaskedTextBox
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents txt_Articulo As TextBox
    Friend WithEvents col1_Precio1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Sucursal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Existencias As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents chk_Existencias As CheckBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_Marca As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_CodigoBarra As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Codigoref As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_Clase As TextBox
    Friend WithEvents col1_Precio2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Codigo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_CodigoCaja As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_CodbarraA1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_CodbarraA2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cuentactb8 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Marca As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Precio3 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents band_existencia As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand7 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand3 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents col1_Estilo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Precio4 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Precio5 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Caracteristicas As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
End Class
