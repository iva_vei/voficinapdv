﻿
Public Class SelSKU
    Public IdSelecionado As Integer
    Public ValorSelecionado As String
    Private Sub SelSKU_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '''Dim oDatos As Datos_Supply
        '''Dim dtDatos As DataTable = Nothing
        '''Dim MsjError As String = ""
        '''Dim sw_error As Boolean = False
        '''Try
        '''    Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '''    Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '''    Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '''
        '''    GridC_1.Visible = False
        '''    oDatos = New Datos_Supply
        '''    GridC_1.Visible = True
        '''    Me.Text = "Catalogo de SKUS"
        '''    lbl_Etiqueta.Text = "Elije una de los siguientes SKUS"
        '''    col_Marca.Caption = "Corrida"
        '''    If oDatos.SKUS_Sel(Globales.oAmbientes.Id_Empresa, 0, 0, "", "", "", 0, 1, "", 0, 0, 0, 0, 0, dtDatos, MsjError) Then
        '''        GridC_1.DataSource = dtDatos
        '''        GridC_1.RefreshDataSource()
        '''        GridV_1.BestFitColumns()
        '''    Else
        '''        sw_error = True
        '''    End If
        '''    If sw_error Then
        '''        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & MsjError, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '''        Me.Close()
        '''    End If
        '''Catch ex As Exception
        '''    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '''End Try
        '''oDatos = Nothing
    End Sub
    Private Sub Aceptar_Cancelar(ByVal tag As String)
        Try
            Select Case tag
                Case "Ok"
                    IdSelecionado = GridV_1.GetFocusedRowCellValue(col_IdSku)
                    ValorSelecionado = GridV_1.GetFocusedRowCellValue(col_Marca)
                Case "Cancelar"
                    IdSelecionado = 0
                    ValorSelecionado = ""
            End Select
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Call Aceptar_Cancelar(DirectCast(CType(e, DevExpress.XtraBars.Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption)
    End Sub
    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
        Call Aceptar_Cancelar("Ok")
    End Sub
End Class