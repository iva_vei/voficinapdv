﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvClienteFinal
    Dim _Accion As Boolean
    Dim sw_datosbancarios As Boolean
    Public IdDir As Integer
    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property
    Private Sub pdvFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Msj As String = ""

        _Accion = False

        oDatos = New Datos_Viscoi
        If oDatos.PVTA_Recupera_Bancos(dtDatos, Msj) Then
            cbo_Bancos.DataSource = dtDatos
            cbo_Bancos.Refresh()
        Else
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End If

        If oDatos.Geo_Estados_Sel(Globales.oAmbientes.Id_Empresa, dtDatos, Msj) Then
            Cbo_Estado.DataSource = dtDatos
            Cbo_Estado.Refresh()
        Else
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End If
        If txt_Cliente.Text <> "" Then
            Call BuscarCuenta("")
        End If

        If Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "ADMINISTRARDATOS") Then
            GC_Bancarios.Enabled = True
        Else
            GC_Bancarios.Enabled = False
        End If

    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim Cliente As String = ""
        Dim Observacion As String = ""
        Try
            'If GC_Bancarios.Enabled And OFD1.FileName = "" And sw_datosbancarios Then
            '    MessageBox.Show("Debes subir imagen que avale el cambio de datos bancarios.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'Else
            oDatos = New Datos_Viscoi
            Cliente = txt_Cliente.Text
            sw_datosbancarios = False
            If sw_datosbancarios Then
                Observacion = InputBox("Introduce una observación por la cual se realizo el cambio de datos bancarios.", Me.Text)
                If Observacion.Length = 0 Then
                    MessageBox.Show("Debes introducir una observación por la cual se realizo el cambio de datos bancarios.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return
                End If

                If Cliente.Length > 0 Then
                    If oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-tbanco", Cliente, Txt_Banco_Cuenta.Text.Substring(Txt_Banco_Cuenta.Text.Length - 4), dtDatos, Msj) Then
                        If dtDatos.Rows.Count = 0 Then
                            MessageBox.Show("Debes tomar la fotografia por la cual se realizo el cambio de datos bancarios.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return
                        End If
                    End If
                End If
            End If


                If oDatos.Inserta_ClienteFinal(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , Cliente _
                                        , Cliente _
                                        , Cliente _
                                        , "" _
                                        , "ACTIVO" _
                                        , txt_Nombre.Text _
                                        , txt_Nombre.Text _
                                        , txt_Adicional.Text _
                                        , txt_APaterno.Text _
                                        , txt_AMaterno.Text _
                                        , "" _
                                        , txt_Calle.Text & " E:" & txt_NumExt.Text & IIf(txt_NumInt.Text.Length > 0, " I:" & txt_NumInt.Text, "") _
                                        , "" _
                                        , txt_Colonia.Text _
                                        , Cbo_Municipio.Text _
                                        , Cbo_Municipio.Text _
                                        , Cbo_Estado.Text _
                                        , txt_Pais.Text _
                                        , txt_CP.Value _
                                        , txt_RFC.Text _
                                        , txt_Telefono.Text _
                                        , txt_Telefono2.Text _
                                        , txt_EMail.Text _
                                        , "" _
                                        , "" _
                                        , "" _
                                        , "" _
                                        , Globales.oAmbientes.oUsuario.Id_usuario _
                                        , cbo_Bancos.Text _
                                        , Cbo_Banco_Tipo.Text _
                                        , Txt_Banco_Cuenta.Text _
                                        , Txt_CURP.Text _
                                        , Txt_Beneficiario.Text _
                                        , Txt_Parentesco.Text _
                                        , Msj) Then

                    If sw_datosbancarios Then

                        If txt_Cliente.Text.Length = 0 And Cliente.Length > 0 Then
                        If oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-tbanco", Cliente, Txt_Banco_Cuenta.Text.Substring(Txt_Banco_Cuenta.Text.Length - 4), dtDatos, Msj) Then
                            If dtDatos.Rows.Count = 0 Then
                                txt_Cliente.Text = Cliente
                                MessageBox.Show("Debes tomar la fotografia por la cual se realizo el cambio de datos bancarios.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Return
                            End If
                        End If
                    End If
                    Dim Ext1 As String
                    If OFD1.FileName.Length > 0 Then
                        Ext1 = OFD1.FileName.Substring(OFD1.FileName.Length - 3)
                    Else
                        Ext1 = ""
                    End If
                    If Not oDatos.Clientes_Finales_Datos_Bancarios_Ins(Globales.oAmbientes.Id_Empresa _
                                        , Cliente _
                                        , PB_Bancarios.Image _
                                        , Ext1 _
                                        , Observacion _
                                        , Globales.oAmbientes.oUsuario.Id_usuario _
                                        , Msj) Then
                            MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    Else
                        Refresh()
                    End If
                    txt_Cliente.Text = Cliente
                    _Accion = True
                    Me.Close()
                Else
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            'End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Cliente_Click(sender As Object, e As EventArgs) Handles btn_Cliente.Click
        Dim oFrm As pdvCuentasFinal
        Try
            oFrm = New pdvCuentasFinal

            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Cliente.Text = oFrm.Cuenta
                Call BuscarCuenta(txt_Cliente.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub BuscarCuenta(ByRef Tarjeta As String)
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim posExt As Integer
        Dim posInt As Integer
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_ClienteFinal(txt_Cliente.Text, "", "", "", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_Cliente.Text = dtDatos.Rows(0).Item("id_cliente")
                    txt_Nombre.Text = dtDatos.Rows(0).Item("nombre")
                    txt_Adicional.Text = dtDatos.Rows(0).Item("nombre_adicional")
                    txt_APaterno.Text = dtDatos.Rows(0).Item("apaterno")
                    txt_AMaterno.Text = dtDatos.Rows(0).Item("amaterno")
                    txt_RFC.Text = dtDatos.Rows(0).Item("rfc")
                    Txt_CURP.Text = dtDatos.Rows(0).Item("curp")
                    txt_Telefono.Text = dtDatos.Rows(0).Item("telefono")
                    txt_Telefono2.Text = dtDatos.Rows(0).Item("telefono2")
                    txt_EMail.Text = dtDatos.Rows(0).Item("email")
                    If Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "|") Then
                        posExt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "|")
                        posInt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "/")
                        If posExt > 0 Then
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(0, posExt - 1)
                        Else
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion")
                        End If
                        If posInt > 0 Then
                            txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt, posInt - posExt - 1).Replace("E:", "").Trim()
                            txt_NumInt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posInt).Replace("I:", "")
                        Else
                            If posExt > 0 Then
                                txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt).Replace("E:", "").Trim()
                            End If
                            txt_NumInt.Text = ""
                        End If

                    Else
                        posExt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "E:")
                        posInt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "I:")
                        If posExt > 0 Then
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(0, posExt - 1)
                        Else
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion")
                        End If
                        If posInt > 0 Then
                            txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt - 1, posInt - posExt - 1).Replace("E:", "").Trim()
                            txt_NumInt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posInt - 1).Replace("I:", "")
                        Else
                            If posExt > 0 Then
                                txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt - 1).Replace("E:", "").Trim()
                            End If
                            txt_NumInt.Text = ""
                        End If
                    End If
                    txt_CP.Value = dtDatos.Rows(0).Item("cpostal")
                    txt_Colonia.Text = dtDatos.Rows(0).Item("colonia")
                    Cbo_Estado.Text = dtDatos.Rows(0).Item("edo")
                    Cbo_Municipio.Text = dtDatos.Rows(0).Item("ciudad")
                    cbo_Bancos.Text = dtDatos.Rows(0).Item("id_banco")
                    Cbo_Banco_Tipo.Text = dtDatos.Rows(0).Item("tipo_banco")
                    Txt_Banco_Cuenta.Text = dtDatos.Rows(0).Item("cuenta_banco")
                    Txt_Beneficiario.Text = dtDatos.Rows(0).Item("beneficiario")
                    Txt_Parentesco.Text = dtDatos.Rows(0).Item("parentesco")
                Else
                    txt_Cliente.Text = ""
                    txt_Nombre.Text = ""
                    txt_RFC.Text = ""
                    txt_Telefono.Text = ""
                    txt_Telefono2.Text = ""
                    txt_EMail.Text = ""
                    txt_Calle.Text = ""
                    txt_NumExt.Text = ""
                    txt_NumInt.Text = ""
                    txt_CP.Value = 0
                    txt_Colonia.Text = ""
                    Cbo_Municipio.SelectedIndex = 0
                    Cbo_Estado.SelectedIndex = 0
                    cbo_Bancos.SelectedIndex = 0
                    Cbo_Banco_Tipo.SelectedIndex = 0
                    Txt_Banco_Cuenta.Text = ""
                    Txt_Beneficiario.Text = ""
                    Txt_Parentesco.Text = ""
                End If
            Else
                txt_Cliente.Text = ""
                txt_Nombre.Text = ""
                txt_RFC.Text = ""
                txt_Telefono.Text = ""
                txt_Telefono2.Text = ""
                txt_EMail.Text = ""
                txt_Calle.Text = ""
                txt_NumExt.Text = ""
                txt_NumInt.Text = ""
                txt_CP.Value = 0
                txt_Colonia.Text = ""
                Cbo_Municipio.SelectedIndex = 0
                Cbo_Estado.SelectedIndex = 0
                cbo_Bancos.SelectedIndex = 0
                Cbo_Banco_Tipo.SelectedIndex = 0
                Txt_Banco_Cuenta.Text = ""
                Txt_Beneficiario.Text = ""
                Txt_Parentesco.Text = ""

            End If

            sw_datosbancarios = False
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub Cbo_Estado_SelectedValueChanged(sender As Object, e As EventArgs) Handles Cbo_Estado.SelectedValueChanged
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If oDatos.Geo_Municipios_Sel(Globales.oAmbientes.Id_Empresa, Cbo_Estado.Text, dtDatos, Msj) Then
                Cbo_Municipio.DataSource = dtDatos
                Cbo_Municipio.Refresh()
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_BuscarColonia_Click(sender As Object, e As EventArgs) Handles btn_BuscarColonia.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim iCP As Integer = 0
        Dim oBusAsentamiento As SelCombobox
        Try
            oDatos = New Datos_Viscoi
            iCP = txt_CP.Value
            If oDatos.Geo_Asentamientos_Sel(Globales.oAmbientes.Id_Empresa, Cbo_Estado.Text, Cbo_Municipio.Text, iCP, dtDatos, Msj) Then
                Select Case dtDatos.Rows.Count
                    Case 0
                        dtDatos = Nothing
                        If oDatos.Geo_Asentamientos_Sel(Globales.oAmbientes.Id_Empresa, Cbo_Estado.Text, "", iCP, dtDatos, Msj) Then
                            Select Case dtDatos.Rows.Count
                                Case 0
                                    dtDatos = Nothing
                                    If oDatos.Geo_Asentamientos_Sel(Globales.oAmbientes.Id_Empresa, "", "", iCP, dtDatos, Msj) Then
                                        Select Case dtDatos.Rows.Count
                                            Case 0
                                                txt_Colonia.Text = ""
                                                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                                ''txt_CP.SelectAll()
                                                txt_CP.Focus()
                                            Case 1
                                                txt_Colonia.Text = dtDatos(0).Item("asentamiento")
                                            Case Else
                                                oBusAsentamiento = New SelCombobox
                                                oBusAsentamiento.Tipo = Globales.TipoCombo.Asentamientos
                                                oBusAsentamiento.IdFiltro1 = iCP
                                                oBusAsentamiento.IdFiltro2 = ""
                                                oBusAsentamiento.IdFiltro3 = ""
                                                oBusAsentamiento.ShowDialog()
                                                If oBusAsentamiento.IdSelecionado > 0 Then
                                                    txt_Colonia.Text = oBusAsentamiento.ValorSelecionado
                                                Else
                                                    txt_Colonia.Text = ""
                                                    ''MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                                    ''txt_CP.SelectAll()
                                                    txt_CP.Focus()
                                                End If
                                        End Select
                                    Else
                                        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End If
                                Case 1
                                    txt_Colonia.Text = dtDatos(0).Item("asentamiento")
                                Case Else
                                    oBusAsentamiento = New SelCombobox
                                    oBusAsentamiento.Tipo = Globales.TipoCombo.Asentamientos
                                    oBusAsentamiento.IdFiltro1 = iCP
                                    oBusAsentamiento.IdFiltro2 = ""
                                    oBusAsentamiento.IdFiltro3 = Cbo_Estado.Text
                                    oBusAsentamiento.ShowDialog()
                                    If oBusAsentamiento.IdSelecionado > 0 Then
                                        txt_Colonia.Text = oBusAsentamiento.ValorSelecionado
                                    Else
                                        txt_Colonia.Text = ""
                                        ''MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        ''txt_CP.SelectAll()
                                        txt_CP.Focus()
                                    End If
                            End Select
                        Else
                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    Case 1
                        txt_Colonia.Text = dtDatos(0).Item("asentamiento")
                    Case Else
                        oBusAsentamiento = New SelCombobox
                        oBusAsentamiento.Tipo = Globales.TipoCombo.Asentamientos
                        oBusAsentamiento.IdFiltro1 = iCP
                        oBusAsentamiento.IdFiltro2 = Cbo_Municipio.Text
                        oBusAsentamiento.IdFiltro3 = Cbo_Estado.Text
                        oBusAsentamiento.ShowDialog()
                        If oBusAsentamiento.IdSelecionado > 0 Then
                            txt_Colonia.Text = oBusAsentamiento.ValorSelecionado
                        Else
                            txt_Colonia.Text = ""
                            ''MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            ''txt_CP.SelectAll()
                            txt_CP.Focus()
                        End If
                End Select
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim oCliFinal As New entClienteFinal
        Dim lstClifinal As New List(Of entClienteFinal)
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Try

            oCliFinal.Cliente = txt_Cliente.Text
            oCliFinal.Nombre = txt_Nombre.Text & " " & txt_Adicional.Text & " " & txt_APaterno.Text & " " & txt_AMaterno.Text
            oCliFinal.RFC = txt_RFC.Text
            oCliFinal.Telefono = txt_Telefono.Text
            oCliFinal.Telefono2 = txt_Telefono2.Text
            oCliFinal.EMail = txt_EMail.Text
            oCliFinal.Calle = txt_Calle.Text
            oCliFinal.NumExt = txt_NumExt.Text
            oCliFinal.NumInt = txt_NumInt.Text
            oCliFinal.CP = txt_CP.Value
            oCliFinal.Colonia = txt_Colonia.Text
            oCliFinal.Municipio = Cbo_Municipio.Text
            oCliFinal.Estado = Cbo_Estado.Text
            oCliFinal.Bancos = cbo_Bancos.Text
            oCliFinal.Banco_Tipo = Cbo_Banco_Tipo.Text
            oCliFinal.Banco_Cuenta = Txt_Banco_Cuenta.Text
            oCliFinal.Beneficiario = Txt_Beneficiario.Text
            oCliFinal.Parentesco = Txt_Parentesco.Text
            oCliFinal.Fum = Now()

            lstClifinal.Add(oCliFinal)

            Dim oRep As New XtraRepCambioBancario
            oRep.ODS1.DataSource = lstClifinal

            printTool = New DevExpress.XtraReports.UI.ReportPrintTool(oRep)
            printTool.ShowPreviewDialog()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        'OFD1.ShowDialog(Me)
        'If OFD1.FileName.Length > 0 Then
        'PB_Bancarios.Load(OFD1.FileName)
        'End If
    End Sub

    Private Sub Bancarios_TextChanged(sender As Object, e As EventArgs) Handles Txt_Banco_Cuenta.TextChanged, cbo_Bancos.SelectedValueChanged, Cbo_Banco_Tipo.SelectedValueChanged
        Try
            sw_datosbancarios = True
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim msj As String = ""
        Dim oCam As New frmcamera
        oCam.StartPosition = FormStartPosition.CenterScreen
        oCam.ShowDialog()
        If oCam.PathImagen <> "" Then
            oDatos = New Datos_Viscoi
            If oDatos.Inserta_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-tbanco", txt_Cliente.Text, Txt_Banco_Cuenta.Text.Substring(Txt_Banco_Cuenta.Text.Length - 4), Today, Globales.oAmbientes.oUsuario.Id_usuario, oCam.PictureBox2.Image, msj) Then
                ''Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", txt_IdClienteFinal.Text, 0, PictureBox1)
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Else
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & "No se capturo Fotografia.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub
End Class