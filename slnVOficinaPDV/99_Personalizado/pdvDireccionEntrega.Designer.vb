﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvDireccionEntrega
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_Estado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_Municipio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_Colonia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_CP = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Calle = New System.Windows.Forms.TextBox()
        Me.txt_Cliente = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_NumExt = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_NumInt = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(117, 413)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 7
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(251, 413)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 8
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(7, 344)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(422, 24)
        Me.Label9.TabIndex = 60
        Me.Label9.Text = "Estado"
        '
        'txt_Estado
        '
        Me.txt_Estado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Estado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Estado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Estado.Location = New System.Drawing.Point(7, 375)
        Me.txt_Estado.Name = "txt_Estado"
        Me.txt_Estado.Size = New System.Drawing.Size(422, 29)
        Me.txt_Estado.TabIndex = 6
        Me.txt_Estado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(7, 277)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(422, 24)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Municipio"
        '
        'txt_Municipio
        '
        Me.txt_Municipio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Municipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Municipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Municipio.Location = New System.Drawing.Point(7, 308)
        Me.txt_Municipio.Name = "txt_Municipio"
        Me.txt_Municipio.Size = New System.Drawing.Size(422, 29)
        Me.txt_Municipio.TabIndex = 5
        Me.txt_Municipio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(7, 210)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(422, 24)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Colonia"
        '
        'txt_Colonia
        '
        Me.txt_Colonia.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Colonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Colonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Colonia.Location = New System.Drawing.Point(7, 241)
        Me.txt_Colonia.Name = "txt_Colonia"
        Me.txt_Colonia.Size = New System.Drawing.Size(422, 29)
        Me.txt_Colonia.TabIndex = 4
        Me.txt_Colonia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(7, 143)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(422, 24)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "C. P."
        '
        'txt_CP
        '
        Me.txt_CP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_CP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CP.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CP.Location = New System.Drawing.Point(7, 174)
        Me.txt_CP.Name = "txt_CP"
        Me.txt_CP.Size = New System.Drawing.Size(422, 29)
        Me.txt_CP.TabIndex = 3
        Me.txt_CP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(422, 24)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Calle"
        '
        'txt_Calle
        '
        Me.txt_Calle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Calle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Calle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Calle.Location = New System.Drawing.Point(7, 40)
        Me.txt_Calle.Name = "txt_Calle"
        Me.txt_Calle.Size = New System.Drawing.Size(422, 29)
        Me.txt_Calle.TabIndex = 0
        Me.txt_Calle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Cliente
        '
        Me.txt_Cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cliente.Location = New System.Drawing.Point(7, 413)
        Me.txt_Cliente.Name = "txt_Cliente"
        Me.txt_Cliente.Size = New System.Drawing.Size(100, 29)
        Me.txt_Cliente.TabIndex = 61
        Me.txt_Cliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txt_Cliente.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(204, 24)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Num Ext"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_NumExt
        '
        Me.txt_NumExt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_NumExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumExt.Location = New System.Drawing.Point(7, 107)
        Me.txt_NumExt.Name = "txt_NumExt"
        Me.txt_NumExt.Size = New System.Drawing.Size(212, 29)
        Me.txt_NumExt.TabIndex = 1
        Me.txt_NumExt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(225, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(204, 24)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Num Int"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_NumInt
        '
        Me.txt_NumInt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_NumInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumInt.Location = New System.Drawing.Point(225, 107)
        Me.txt_NumInt.Name = "txt_NumInt"
        Me.txt_NumInt.Size = New System.Drawing.Size(204, 29)
        Me.txt_NumInt.TabIndex = 2
        Me.txt_NumInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pdvDireccionEntrega
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 461)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_NumInt)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_NumExt)
        Me.Controls.Add(Me.txt_Cliente)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txt_Estado)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txt_Municipio)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_Colonia)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_CP)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_Calle)
        Me.Controls.Add(Me.btn_Aceptar)
        Me.Controls.Add(Me.btn_Cancelar)
        Me.KeyPreview = True
        Me.Name = "pdvDireccionEntrega"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nueva Dirección de Entrega"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents btn_Aceptar As Button
    Private WithEvents btn_Cancelar As Button
    Private WithEvents Label9 As Label
    Private WithEvents txt_Estado As TextBox
    Private WithEvents Label8 As Label
    Private WithEvents txt_Municipio As TextBox
    Private WithEvents Label7 As Label
    Private WithEvents txt_Colonia As TextBox
    Private WithEvents Label6 As Label
    Private WithEvents txt_CP As TextBox
    Private WithEvents Label4 As Label
    Private WithEvents txt_Calle As TextBox
    Private WithEvents Label1 As Label
    Private WithEvents txt_NumExt As TextBox
    Private WithEvents Label2 As Label
    Private WithEvents txt_NumInt As TextBox
    Friend WithEvents txt_Cliente As TextBox
End Class
