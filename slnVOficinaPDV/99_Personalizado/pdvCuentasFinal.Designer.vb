﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCuentasFinal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvCuentasFinal))
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.col1_IdCliente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Nombre = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Curp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Rfc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Telefono = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Telefono2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cpostal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Edo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Ciudad = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ClienteFinal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Referencia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Estatus = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_NombreCli = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Nomcorto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Direccion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Entrecalles = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Colonia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Municipio = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Pais = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Email = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Internet = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fax = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Contacto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Observaciones = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdUsuario = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fum = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdBanco = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TipoBanco = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_CuentaBanco = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Refrescar = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Estatus = New System.Windows.Forms.TextBox()
        Me.lbl_Estatus = New System.Windows.Forms.Label()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.lbl_RFC = New System.Windows.Forms.Label()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.lbl_Nombre = New System.Windows.Forms.Label()
        Me.txt_CURP = New System.Windows.Forms.TextBox()
        Me.txt_Cuenta = New System.Windows.Forms.TextBox()
        Me.lbl_Curp = New System.Windows.Forms.Label()
        Me.lbl_Cuenta = New System.Windows.Forms.Label()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.gridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(242, 26)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(481, 26)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 533)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(784, 80)
        Me.GroupControl3.TabIndex = 2
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(784, 389)
        Me.GroupControl2.TabIndex = 1
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(780, 364)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.gridBand2})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_IdCliente, Me.col1_ClienteFinal, Me.col1_Referencia, Me.col1_Tipo, Me.col1_Estatus, Me.col1_NombreCli, Me.col1_Nombre, Me.col1_Nomcorto, Me.col1_Direccion, Me.col1_Entrecalles, Me.col1_Colonia, Me.col1_Municipio, Me.col1_Ciudad, Me.col1_Edo, Me.col1_Pais, Me.col1_Cpostal, Me.col1_Rfc, Me.col1_Telefono, Me.col1_Telefono2, Me.col1_Email, Me.col1_Internet, Me.col1_Fax, Me.col1_Contacto, Me.col1_Observaciones, Me.col1_IdUsuario, Me.col1_Fum, Me.col1_IdBanco, Me.col1_TipoBanco, Me.col1_CuentaBanco, Me.col1_Curp})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col1_IdCliente
        '
        Me.col1_IdCliente.Caption = "IdCliente"
        Me.col1_IdCliente.FieldName = "id_cliente"
        Me.col1_IdCliente.Name = "col1_IdCliente"
        Me.col1_IdCliente.OptionsColumn.AllowEdit = False
        Me.col1_IdCliente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdCliente.Visible = True
        '
        'col1_Nombre
        '
        Me.col1_Nombre.Caption = "Nombre"
        Me.col1_Nombre.FieldName = "nombre"
        Me.col1_Nombre.Name = "col1_Nombre"
        Me.col1_Nombre.OptionsColumn.AllowEdit = False
        Me.col1_Nombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Curp
        '
        Me.col1_Curp.Caption = "Curp"
        Me.col1_Curp.FieldName = "curp"
        Me.col1_Curp.Name = "col1_Curp"
        Me.col1_Curp.OptionsColumn.AllowEdit = False
        Me.col1_Curp.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Curp.Visible = True
        '
        'col1_Rfc
        '
        Me.col1_Rfc.Caption = "Rfc"
        Me.col1_Rfc.FieldName = "rfc"
        Me.col1_Rfc.Name = "col1_Rfc"
        Me.col1_Rfc.OptionsColumn.AllowEdit = False
        Me.col1_Rfc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Rfc.Visible = True
        '
        'col1_Telefono
        '
        Me.col1_Telefono.Caption = "Telefono"
        Me.col1_Telefono.FieldName = "telefono"
        Me.col1_Telefono.Name = "col1_Telefono"
        Me.col1_Telefono.OptionsColumn.AllowEdit = False
        Me.col1_Telefono.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Telefono.Visible = True
        '
        'col1_Telefono2
        '
        Me.col1_Telefono2.Caption = "Telefono2"
        Me.col1_Telefono2.FieldName = "telefono2"
        Me.col1_Telefono2.Name = "col1_Telefono2"
        Me.col1_Telefono2.OptionsColumn.AllowEdit = False
        Me.col1_Telefono2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Telefono2.Visible = True
        '
        'col1_Cpostal
        '
        Me.col1_Cpostal.Caption = "Cpostal"
        Me.col1_Cpostal.FieldName = "cpostal"
        Me.col1_Cpostal.Name = "col1_Cpostal"
        Me.col1_Cpostal.OptionsColumn.AllowEdit = False
        Me.col1_Cpostal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Cpostal.Visible = True
        '
        'col1_Edo
        '
        Me.col1_Edo.Caption = "Edo"
        Me.col1_Edo.FieldName = "edo"
        Me.col1_Edo.Name = "col1_Edo"
        Me.col1_Edo.OptionsColumn.AllowEdit = False
        Me.col1_Edo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Edo.Visible = True
        '
        'col1_Ciudad
        '
        Me.col1_Ciudad.Caption = "Ciudad"
        Me.col1_Ciudad.FieldName = "ciudad"
        Me.col1_Ciudad.Name = "col1_Ciudad"
        Me.col1_Ciudad.OptionsColumn.AllowEdit = False
        Me.col1_Ciudad.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Ciudad.Visible = True
        '
        'col1_ClienteFinal
        '
        Me.col1_ClienteFinal.Caption = "ClienteFinal"
        Me.col1_ClienteFinal.FieldName = "cliente_final"
        Me.col1_ClienteFinal.Name = "col1_ClienteFinal"
        Me.col1_ClienteFinal.OptionsColumn.AllowEdit = False
        Me.col1_ClienteFinal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ClienteFinal.Visible = True
        '
        'col1_Referencia
        '
        Me.col1_Referencia.Caption = "Referencia"
        Me.col1_Referencia.FieldName = "referencia"
        Me.col1_Referencia.Name = "col1_Referencia"
        Me.col1_Referencia.OptionsColumn.AllowEdit = False
        Me.col1_Referencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Referencia.Visible = True
        '
        'col1_Tipo
        '
        Me.col1_Tipo.Caption = "Tipo"
        Me.col1_Tipo.FieldName = "tipo"
        Me.col1_Tipo.Name = "col1_Tipo"
        Me.col1_Tipo.OptionsColumn.AllowEdit = False
        Me.col1_Tipo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipo.Visible = True
        '
        'col1_Estatus
        '
        Me.col1_Estatus.Caption = "Estatus"
        Me.col1_Estatus.FieldName = "estatus"
        Me.col1_Estatus.Name = "col1_Estatus"
        Me.col1_Estatus.OptionsColumn.AllowEdit = False
        Me.col1_Estatus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Estatus.Visible = True
        '
        'col1_NombreCli
        '
        Me.col1_NombreCli.Caption = "NombreCli"
        Me.col1_NombreCli.FieldName = "nombre_cli"
        Me.col1_NombreCli.Name = "col1_NombreCli"
        Me.col1_NombreCli.OptionsColumn.AllowEdit = False
        Me.col1_NombreCli.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_NombreCli.Visible = True
        '
        'col1_Nomcorto
        '
        Me.col1_Nomcorto.Caption = "Nomcorto"
        Me.col1_Nomcorto.FieldName = "nomcorto"
        Me.col1_Nomcorto.Name = "col1_Nomcorto"
        Me.col1_Nomcorto.OptionsColumn.AllowEdit = False
        Me.col1_Nomcorto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Nomcorto.Visible = True
        '
        'col1_Direccion
        '
        Me.col1_Direccion.Caption = "Direccion"
        Me.col1_Direccion.FieldName = "direccion"
        Me.col1_Direccion.Name = "col1_Direccion"
        Me.col1_Direccion.OptionsColumn.AllowEdit = False
        Me.col1_Direccion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Direccion.Visible = True
        '
        'col1_Entrecalles
        '
        Me.col1_Entrecalles.Caption = "Entrecalles"
        Me.col1_Entrecalles.FieldName = "entrecalles"
        Me.col1_Entrecalles.Name = "col1_Entrecalles"
        Me.col1_Entrecalles.OptionsColumn.AllowEdit = False
        Me.col1_Entrecalles.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Entrecalles.Visible = True
        '
        'col1_Colonia
        '
        Me.col1_Colonia.Caption = "Colonia"
        Me.col1_Colonia.FieldName = "colonia"
        Me.col1_Colonia.Name = "col1_Colonia"
        Me.col1_Colonia.OptionsColumn.AllowEdit = False
        Me.col1_Colonia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Colonia.Visible = True
        '
        'col1_Municipio
        '
        Me.col1_Municipio.Caption = "Municipio"
        Me.col1_Municipio.FieldName = "municipio"
        Me.col1_Municipio.Name = "col1_Municipio"
        Me.col1_Municipio.OptionsColumn.AllowEdit = False
        Me.col1_Municipio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Municipio.Visible = True
        '
        'col1_Pais
        '
        Me.col1_Pais.Caption = "Pais"
        Me.col1_Pais.FieldName = "pais"
        Me.col1_Pais.Name = "col1_Pais"
        Me.col1_Pais.OptionsColumn.AllowEdit = False
        Me.col1_Pais.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Pais.Visible = True
        '
        'col1_Email
        '
        Me.col1_Email.Caption = "Email"
        Me.col1_Email.FieldName = "email"
        Me.col1_Email.Name = "col1_Email"
        Me.col1_Email.OptionsColumn.AllowEdit = False
        Me.col1_Email.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Email.Visible = True
        '
        'col1_Internet
        '
        Me.col1_Internet.Caption = "Internet"
        Me.col1_Internet.FieldName = "internet"
        Me.col1_Internet.Name = "col1_Internet"
        Me.col1_Internet.OptionsColumn.AllowEdit = False
        Me.col1_Internet.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Internet.Visible = True
        '
        'col1_Fax
        '
        Me.col1_Fax.Caption = "Fax"
        Me.col1_Fax.FieldName = "fax"
        Me.col1_Fax.Name = "col1_Fax"
        Me.col1_Fax.OptionsColumn.AllowEdit = False
        Me.col1_Fax.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fax.Visible = True
        '
        'col1_Contacto
        '
        Me.col1_Contacto.Caption = "Contacto"
        Me.col1_Contacto.FieldName = "contacto"
        Me.col1_Contacto.Name = "col1_Contacto"
        Me.col1_Contacto.OptionsColumn.AllowEdit = False
        Me.col1_Contacto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Contacto.Visible = True
        '
        'col1_Observaciones
        '
        Me.col1_Observaciones.Caption = "Observaciones"
        Me.col1_Observaciones.FieldName = "observaciones"
        Me.col1_Observaciones.Name = "col1_Observaciones"
        Me.col1_Observaciones.OptionsColumn.AllowEdit = False
        Me.col1_Observaciones.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Observaciones.Visible = True
        '
        'col1_IdUsuario
        '
        Me.col1_IdUsuario.Caption = "IdUsuario"
        Me.col1_IdUsuario.FieldName = "id_usuario"
        Me.col1_IdUsuario.Name = "col1_IdUsuario"
        Me.col1_IdUsuario.OptionsColumn.AllowEdit = False
        Me.col1_IdUsuario.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdUsuario.Visible = True
        '
        'col1_Fum
        '
        Me.col1_Fum.Caption = "Fum"
        Me.col1_Fum.FieldName = "fum"
        Me.col1_Fum.Name = "col1_Fum"
        Me.col1_Fum.OptionsColumn.AllowEdit = False
        Me.col1_Fum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fum.Visible = True
        '
        'col1_IdBanco
        '
        Me.col1_IdBanco.Caption = "IdBanco"
        Me.col1_IdBanco.FieldName = "id_banco"
        Me.col1_IdBanco.Name = "col1_IdBanco"
        Me.col1_IdBanco.OptionsColumn.AllowEdit = False
        Me.col1_IdBanco.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdBanco.Visible = True
        '
        'col1_TipoBanco
        '
        Me.col1_TipoBanco.Caption = "TipoBanco"
        Me.col1_TipoBanco.FieldName = "tipo_banco"
        Me.col1_TipoBanco.Name = "col1_TipoBanco"
        Me.col1_TipoBanco.OptionsColumn.AllowEdit = False
        Me.col1_TipoBanco.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TipoBanco.Visible = True
        '
        'col1_CuentaBanco
        '
        Me.col1_CuentaBanco.Caption = "CuentaBanco"
        Me.col1_CuentaBanco.FieldName = "cuenta_banco"
        Me.col1_CuentaBanco.Name = "col1_CuentaBanco"
        Me.col1_CuentaBanco.OptionsColumn.AllowEdit = False
        Me.col1_CuentaBanco.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CuentaBanco.Visible = True
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.txt_Estatus)
        Me.GroupControl1.Controls.Add(Me.lbl_Estatus)
        Me.GroupControl1.Controls.Add(Me.txt_RFC)
        Me.GroupControl1.Controls.Add(Me.lbl_RFC)
        Me.GroupControl1.Controls.Add(Me.txt_Nombre)
        Me.GroupControl1.Controls.Add(Me.lbl_Nombre)
        Me.GroupControl1.Controls.Add(Me.txt_CURP)
        Me.GroupControl1.Controls.Add(Me.txt_Cuenta)
        Me.GroupControl1.Controls.Add(Me.lbl_Curp)
        Me.GroupControl1.Controls.Add(Me.lbl_Cuenta)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 389)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(784, 144)
        Me.GroupControl1.TabIndex = 0
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.ImageOptions.Image = CType(resources.GetObject("btn_Refrescar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_Refrescar.Location = New System.Drawing.Point(730, 29)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Refrescar.TabIndex = 0
        '
        'txt_Estatus
        '
        Me.txt_Estatus.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Estatus.Location = New System.Drawing.Point(398, 26)
        Me.txt_Estatus.Name = "txt_Estatus"
        Me.txt_Estatus.ReadOnly = True
        Me.txt_Estatus.Size = New System.Drawing.Size(200, 30)
        Me.txt_Estatus.TabIndex = 16
        Me.txt_Estatus.Text = "NORM"
        Me.txt_Estatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_Estatus
        '
        Me.lbl_Estatus.AutoSize = True
        Me.lbl_Estatus.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Estatus.Location = New System.Drawing.Point(306, 29)
        Me.lbl_Estatus.Name = "lbl_Estatus"
        Me.lbl_Estatus.Size = New System.Drawing.Size(70, 23)
        Me.lbl_Estatus.TabIndex = 15
        Me.lbl_Estatus.Text = "Estatus"
        '
        'txt_RFC
        '
        Me.txt_RFC.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(398, 62)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(200, 30)
        Me.txt_RFC.TabIndex = 2
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_RFC
        '
        Me.lbl_RFC.AutoSize = True
        Me.lbl_RFC.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_RFC.Location = New System.Drawing.Point(306, 62)
        Me.lbl_RFC.Name = "lbl_RFC"
        Me.lbl_RFC.Size = New System.Drawing.Size(43, 23)
        Me.lbl_RFC.TabIndex = 11
        Me.lbl_RFC.Text = "RFC"
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(97, 98)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(675, 30)
        Me.txt_Nombre.TabIndex = 5
        '
        'lbl_Nombre
        '
        Me.lbl_Nombre.AutoSize = True
        Me.lbl_Nombre.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Nombre.Location = New System.Drawing.Point(5, 101)
        Me.lbl_Nombre.Name = "lbl_Nombre"
        Me.lbl_Nombre.Size = New System.Drawing.Size(77, 23)
        Me.lbl_Nombre.TabIndex = 8
        Me.lbl_Nombre.Text = "Nombre"
        '
        'txt_CURP
        '
        Me.txt_CURP.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CURP.Location = New System.Drawing.Point(97, 62)
        Me.txt_CURP.Name = "txt_CURP"
        Me.txt_CURP.Size = New System.Drawing.Size(200, 30)
        Me.txt_CURP.TabIndex = 3
        Me.txt_CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Cuenta
        '
        Me.txt_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cuenta.Location = New System.Drawing.Point(97, 26)
        Me.txt_Cuenta.Name = "txt_Cuenta"
        Me.txt_Cuenta.Size = New System.Drawing.Size(200, 30)
        Me.txt_Cuenta.TabIndex = 1
        Me.txt_Cuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_Curp
        '
        Me.lbl_Curp.AutoSize = True
        Me.lbl_Curp.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Curp.Location = New System.Drawing.Point(5, 62)
        Me.lbl_Curp.Name = "lbl_Curp"
        Me.lbl_Curp.Size = New System.Drawing.Size(55, 23)
        Me.lbl_Curp.TabIndex = 3
        Me.lbl_Curp.Text = "CURP"
        '
        'lbl_Cuenta
        '
        Me.lbl_Cuenta.AutoSize = True
        Me.lbl_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Cuenta.Location = New System.Drawing.Point(5, 29)
        Me.lbl_Cuenta.Name = "lbl_Cuenta"
        Me.lbl_Cuenta.Size = New System.Drawing.Size(84, 23)
        Me.lbl_Cuenta.TabIndex = 1
        Me.lbl_Cuenta.Text = "IdCliente"
        '
        'GridBand1
        '
        Me.GridBand1.Columns.Add(Me.col1_IdCliente)
        Me.GridBand1.Columns.Add(Me.col1_NombreCli)
        Me.GridBand1.Columns.Add(Me.col1_Nombre)
        Me.GridBand1.Columns.Add(Me.col1_Curp)
        Me.GridBand1.Columns.Add(Me.col1_Rfc)
        Me.GridBand1.Columns.Add(Me.col1_Telefono)
        Me.GridBand1.Columns.Add(Me.col1_Telefono2)
        Me.GridBand1.Columns.Add(Me.col1_Cpostal)
        Me.GridBand1.Columns.Add(Me.col1_Edo)
        Me.GridBand1.Columns.Add(Me.col1_Ciudad)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 675
        '
        'gridBand2
        '
        Me.gridBand2.Caption = "ENVIAR"
        Me.gridBand2.Name = "gridBand2"
        Me.gridBand2.Visible = False
        Me.gridBand2.VisibleIndex = -1
        Me.gridBand2.Width = 75
        '
        'pdvCuentasFinal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvCuentasFinal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cliente Final"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lbl_Curp As Label
    Friend WithEvents lbl_Cuenta As Label
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents lbl_RFC As Label
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents lbl_Nombre As Label
    Friend WithEvents txt_CURP As TextBox
    Friend WithEvents txt_Cuenta As TextBox
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents col1_IdCliente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ClienteFinal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Referencia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Estatus As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_NombreCli As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Nombre As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Nomcorto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Direccion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Entrecalles As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Colonia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Municipio As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Ciudad As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Edo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Pais As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cpostal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Rfc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Telefono As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Telefono2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Email As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Internet As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fax As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Contacto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Observaciones As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdUsuario As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fum As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdBanco As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TipoBanco As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_CuentaBanco As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Curp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn


    Friend WithEvents txt_Estatus As TextBox
    Friend WithEvents lbl_Estatus As Label
    Friend WithEvents btn_Refrescar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
End Class
