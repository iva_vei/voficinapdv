﻿Public Class uctrl_Estructura
    Private Shared _IdDivision As Integer
    Private Shared _IdDepto As Integer
    Private Shared _IdFamilia As Integer
    Private Shared _Id_Estructura As Integer
    Private Shared _Sw_SeleccionarSoloFamilia As Boolean
    Private Shared _Division As String
    Private Shared _Depto As String
    Private Shared _Familia As String
    Private Jerarquia04 As String
    Private Shared _ReadOnly As Boolean
    Shared Event AfterShow()

    Private Shared Sub btn_Estructura_Click(sender As Object, e As EventArgs) Handles btn_Estructura.Click
        Dim frmEstructura As SelEstructura
        Try
            If Not _ReadOnly Then
                frmEstructura = New SelEstructura
                frmEstructura.ShowDialog()

                _IdDivision = frmEstructura.id_division
                _IdDepto = frmEstructura.id_depto
                _IdFamilia = frmEstructura.id_familia
                _Division = frmEstructura.division
                _Depto = frmEstructura.depto
                _Familia = frmEstructura.familia
                _Id_Estructura = frmEstructura.id_estructura

                If _Sw_SeleccionarSoloFamilia Then
                    mem_Estructura.Text = frmEstructura.division _
                     & vbNewLine & ">>" & frmEstructura.depto _
                   & vbNewLine & ">>>>" & frmEstructura.familia
                Else
                    mem_Estructura.Text = frmEstructura.division _
     & IIf(_IdDepto > 0, vbNewLine & ">>" & frmEstructura.depto, "") _
     & IIf(_IdFamilia > 0, vbNewLine & ">>>>" & frmEstructura.familia, "")
                End If

                RaiseEvent AfterShow()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Shared Sub ShowEstructura()
        Call btn_Estructura_Click(Nothing, Nothing)
    End Sub
    Public Property IdDivision As Integer
        Get
            Return _IdDivision
        End Get
        Set(value As Integer)
            _IdDivision = value
        End Set
    End Property
    Public Property IdDepto As Integer
        Get
            Return _IdDepto
        End Get
        Set(value As Integer)
            _IdDepto = value
        End Set
    End Property
    Public Property IdFamilia As Integer
        Get
            Return _IdFamilia
        End Get
        Set(value As Integer)
            _IdFamilia = value
        End Set
    End Property
    Public Property Division As String
        Get
            Return _Division
        End Get
        Set(value As String)
            _Division = value
        End Set
    End Property
    Public Property Depto As String
        Get
            Return _Depto
        End Get
        Set(value As String)
            _Depto = value
        End Set
    End Property
    Public Property Familia As String
        Get
            Return _Familia
        End Get
        Set(value As String)
            _Familia = value
        End Set
    End Property
    Public Property IdEstructura As Integer
        Get
            Return _Id_Estructura
        End Get
        Set(value As Integer)
            _Id_Estructura = value
        End Set
    End Property
    Public ReadOnly Property Estructura As String
        Get
            Return mem_Estructura.Text
        End Get
    End Property

    Public Property [ReadOnly] As Boolean
        Get
            Return _ReadOnly
        End Get
        Set(value As Boolean)
            _ReadOnly = value
            mem_Estructura.ReadOnly = _ReadOnly
        End Set
    End Property
    Public Property Sw_SeleccionarSoloFamilia As Boolean
        Get
            Return _Sw_SeleccionarSoloFamilia
        End Get
        Set(value As Boolean)
            _Sw_SeleccionarSoloFamilia = value
        End Set
    End Property
    Private Sub uctrl_Estructura_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Me.Width = 256
            Me.Height = 64
            mem_Estructura.Height = 64
            mem_Estructura.Width = 192
            btn_Estructura.Height = 64
            btn_Estructura.Width = 64
        Catch ex As Exception

        End Try
    End Sub
End Class
