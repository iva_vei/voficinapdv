﻿Public Class pdvCuentasFinal
    Enum eTipoForma
        Cliente
        ClienteFinal
        Empleados
    End Enum
    Dim _Accion As Boolean
    Dim _Cuenta As String
    Dim _Nombre As String
    Dim _Direccion As String
    Dim _NumExt As String
    Dim _NumInt As String
    Dim _Municipio As String
    Dim _Estado As String
    Dim _IdBanco As String
    Dim _BancoTipo As String
    Dim _BancoCuenta As String
    Dim _TipoForma As eTipoForma
    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public ReadOnly Property Cuenta As String
        Get
            Return _Cuenta
        End Get
    End Property
    Public ReadOnly Property Nombre As String
        Get
            Return _Nombre
        End Get
    End Property

    Public Property Direccion As String
        Get
            Return _Direccion
        End Get
        Set(value As String)
            _Direccion = value
        End Set
    End Property

    Public Property NumExt As String
        Get
            Return _NumExt
        End Get
        Set(value As String)
            _NumExt = value
        End Set
    End Property

    Public Property NumInt As String
        Get
            Return _NumInt
        End Get
        Set(value As String)
            _NumInt = value
        End Set
    End Property

    Public Property Municipio As String
        Get
            Return _Municipio
        End Get
        Set(value As String)
            _Municipio = value
        End Set
    End Property

    Public Property Estado As String
        Get
            Return _Estado
        End Get
        Set(value As String)
            _Estado = value
        End Set
    End Property

    Public Property IdBanco As String
        Get
            Return _IdBanco
        End Get
        Set(value As String)
            _IdBanco = value
        End Set
    End Property

    Public Property BancoTipo As String
        Get
            Return _BancoTipo
        End Get
        Set(value As String)
            _BancoTipo = value
        End Set
    End Property

    Public Property BancoCuenta As String
        Get
            Return _BancoCuenta
        End Get
        Set(value As String)
            _BancoCuenta = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Text = "Clientes"
            txt_Estatus.Text = "NORMAL"
            GridC_1.MainView = GridV_1

            Call Refrescar()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_ClienteFinal(txt_Cuenta.Text, txt_Nombre.Text, txt_RFC.Text, txt_CURP.Text, dtDatos, Mensaje) Then
                GridC_1.DataSource = dtDatos
                GridC_1.RefreshDataSource()
                GridV_1.BestFitColumns()
            Else
                GridC_1.DataSource = Nothing
                GridC_1.RefreshDataSource()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim sw_continuar As Boolean = True
        Dim Requeridos As Double = 0
        Dim Disponible As Double = 0

        Try
            _Accion = True
            If GridV_1.RowCount > 0 Then
                _Cuenta = GridV_1.GetFocusedRowCellValue(col1_IdCliente)
                _Nombre = GridV_1.GetFocusedRowCellValue(col1_NombreCli)
                _Direccion = GridV_1.GetFocusedRowCellValue(col1_Direccion)

                _Municipio = GridV_1.GetFocusedRowCellValue(col1_Municipio)
                _Estado = GridV_1.GetFocusedRowCellValue(col1_Edo)
                _IdBanco = GridV_1.GetFocusedRowCellValue(col1_IdBanco)
                _BancoTipo = GridV_1.GetFocusedRowCellValue(col1_TipoBanco)
                _BancoCuenta = GridV_1.GetFocusedRowCellValue(col1_CuentaBanco)
                Me.Close()
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()
    End Sub

    Private Sub pdvCuentas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Try
            txt_Cuenta.Focus()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub PierdeFoco(sender As Object, e As EventArgs) Handles txt_Cuenta.LostFocus _
                                                                    , txt_CURP.LostFocus _
                                                                    , txt_RFC.LostFocus _
                                                                    , txt_Nombre.LostFocus

        Try
            If txt_Cuenta.Text <> "" _
            Or txt_CURP.Text <> "" _
            Or txt_Nombre.Text <> "" _
            Or txt_RFC.Text <> "" Then
                Call Refrescar()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
        Call btn_Aceptar_Click(Nothing, Nothing)
    End Sub
End Class