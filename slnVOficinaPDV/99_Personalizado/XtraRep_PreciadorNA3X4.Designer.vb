﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class XtraRep_PreciadorNA3X4
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.DetailReport = New DevExpress.XtraReports.UI.DetailReportBand()
        Me.Detail1 = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel82 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel83 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel84 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel79 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel80 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel81 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel76 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel77 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel78 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel75 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel74 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel73 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel55 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel56 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel57 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel58 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel59 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel60 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel61 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel62 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel63 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel64 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel65 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel66 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel67 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel68 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel69 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel70 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel71 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel72 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel42 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel43 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel44 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel45 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel46 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel47 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel48 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel49 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel50 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel51 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel52 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel53 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel54 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Pic1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox6 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox7 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox8 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox11 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox10 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox9 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.ods = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource(Me.components)
        CType(Me.ods, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 25.00001!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2})
        Me.BottomMargin.HeightF = 18.99998!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 6.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(100.0!, 12.99998!)
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrPageInfo2.TextFormatString = "Page {0} of {1}"
        '
        'DetailReport
        '
        Me.DetailReport.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail1})
        Me.DetailReport.DataMember = "Renlones"
        Me.DetailReport.DataSource = Me.ods
        Me.DetailReport.Level = 0
        Me.DetailReport.Name = "DetailReport"
        '
        'Detail1
        '
        Me.Detail1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel82, Me.XrLabel83, Me.XrLabel84, Me.XrLabel79, Me.XrLabel80, Me.XrLabel81, Me.XrLabel76, Me.XrLabel77, Me.XrLabel78, Me.XrLabel75, Me.XrLabel74, Me.XrLabel73, Me.XrLabel55, Me.XrLabel56, Me.XrLabel57, Me.XrLabel58, Me.XrLabel59, Me.XrLabel60, Me.XrLabel61, Me.XrLabel62, Me.XrLabel63, Me.XrLabel64, Me.XrLabel65, Me.XrLabel66, Me.XrLabel67, Me.XrLabel68, Me.XrLabel69, Me.XrLabel70, Me.XrLabel71, Me.XrLabel72, Me.XrLabel37, Me.XrLabel38, Me.XrLabel39, Me.XrLabel40, Me.XrLabel41, Me.XrLabel42, Me.XrLabel43, Me.XrLabel44, Me.XrLabel45, Me.XrLabel46, Me.XrLabel47, Me.XrLabel48, Me.XrLabel49, Me.XrLabel50, Me.XrLabel51, Me.XrLabel52, Me.XrLabel53, Me.XrLabel54, Me.XrLabel19, Me.XrLabel20, Me.XrLabel21, Me.XrLabel22, Me.XrLabel23, Me.XrLabel24, Me.XrLabel25, Me.XrLabel26, Me.XrLabel27, Me.XrLabel28, Me.XrLabel29, Me.XrLabel30, Me.XrLabel31, Me.XrLabel32, Me.XrLabel33, Me.XrLabel34, Me.XrLabel35, Me.XrLabel36, Me.XrLabel16, Me.XrLabel17, Me.XrLabel18, Me.XrLabel11, Me.XrLabel12, Me.XrLabel13, Me.XrLabel14, Me.XrLabel15, Me.XrLabel6, Me.XrLabel7, Me.XrLabel8, Me.XrLabel9, Me.XrLabel10, Me.XrLabel2, Me.XrLabel3, Me.XrLabel5, Me.XrLabel1, Me.XrLabel4, Me.Pic1, Me.XrPictureBox2, Me.XrPictureBox1, Me.XrPictureBox4, Me.XrPictureBox3, Me.XrPictureBox5, Me.XrPictureBox6, Me.XrPictureBox7, Me.XrPictureBox8, Me.XrPictureBox11, Me.XrPictureBox10, Me.XrPictureBox9})
        Me.Detail1.HeightF = 798.925!
        Me.Detail1.Name = "Detail1"
        '
        'XrLabel82
        '
        Me.XrLabel82.CanGrow = False
        Me.XrLabel82.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Caracteristicas]")})
        Me.XrLabel82.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel82.LocationFloat = New DevExpress.Utils.PointFloat(233.599!, 667.488!)
        Me.XrLabel82.Multiline = True
        Me.XrLabel82.Name = "XrLabel82"
        Me.XrLabel82.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel82.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel82.StylePriority.UseFont = False
        Me.XrLabel82.Text = "XrLabel73"
        '
        'XrLabel83
        '
        Me.XrLabel83.CanGrow = False
        Me.XrLabel83.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Caracteristicas]")})
        Me.XrLabel83.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel83.LocationFloat = New DevExpress.Utils.PointFloat(535.1563!, 667.488!)
        Me.XrLabel83.Multiline = True
        Me.XrLabel83.Name = "XrLabel83"
        Me.XrLabel83.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel83.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel83.StylePriority.UseFont = False
        Me.XrLabel83.Text = "XrLabel73"
        '
        'XrLabel84
        '
        Me.XrLabel84.CanGrow = False
        Me.XrLabel84.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Caracteristicas]")})
        Me.XrLabel84.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel84.LocationFloat = New DevExpress.Utils.PointFloat(832.9742!, 667.488!)
        Me.XrLabel84.Multiline = True
        Me.XrLabel84.Name = "XrLabel84"
        Me.XrLabel84.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel84.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel84.StylePriority.UseFont = False
        Me.XrLabel84.Text = "XrLabel73"
        '
        'XrLabel79
        '
        Me.XrLabel79.CanGrow = False
        Me.XrLabel79.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Caracteristicas]")})
        Me.XrLabel79.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel79.LocationFloat = New DevExpress.Utils.PointFloat(233.5991!, 471.7566!)
        Me.XrLabel79.Multiline = True
        Me.XrLabel79.Name = "XrLabel79"
        Me.XrLabel79.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel79.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel79.StylePriority.UseFont = False
        Me.XrLabel79.Text = "XrLabel73"
        '
        'XrLabel80
        '
        Me.XrLabel80.CanGrow = False
        Me.XrLabel80.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Caracteristicas]")})
        Me.XrLabel80.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel80.LocationFloat = New DevExpress.Utils.PointFloat(535.1564!, 471.7567!)
        Me.XrLabel80.Multiline = True
        Me.XrLabel80.Name = "XrLabel80"
        Me.XrLabel80.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel80.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel80.StylePriority.UseFont = False
        Me.XrLabel80.Text = "XrLabel73"
        '
        'XrLabel81
        '
        Me.XrLabel81.CanGrow = False
        Me.XrLabel81.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Caracteristicas]")})
        Me.XrLabel81.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel81.LocationFloat = New DevExpress.Utils.PointFloat(832.9743!, 471.7567!)
        Me.XrLabel81.Multiline = True
        Me.XrLabel81.Name = "XrLabel81"
        Me.XrLabel81.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel81.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel81.StylePriority.UseFont = False
        Me.XrLabel81.Text = "XrLabel73"
        '
        'XrLabel76
        '
        Me.XrLabel76.CanGrow = False
        Me.XrLabel76.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Caracteristicas]")})
        Me.XrLabel76.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel76.LocationFloat = New DevExpress.Utils.PointFloat(233.5991!, 276.0254!)
        Me.XrLabel76.Multiline = True
        Me.XrLabel76.Name = "XrLabel76"
        Me.XrLabel76.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel76.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel76.StylePriority.UseFont = False
        Me.XrLabel76.Text = "XrLabel73"
        '
        'XrLabel77
        '
        Me.XrLabel77.CanGrow = False
        Me.XrLabel77.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Caracteristicas]")})
        Me.XrLabel77.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel77.LocationFloat = New DevExpress.Utils.PointFloat(535.1565!, 276.0255!)
        Me.XrLabel77.Multiline = True
        Me.XrLabel77.Name = "XrLabel77"
        Me.XrLabel77.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel77.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel77.StylePriority.UseFont = False
        Me.XrLabel77.Text = "XrLabel73"
        '
        'XrLabel78
        '
        Me.XrLabel78.CanGrow = False
        Me.XrLabel78.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Caracteristicas]")})
        Me.XrLabel78.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel78.LocationFloat = New DevExpress.Utils.PointFloat(832.9744!, 276.0255!)
        Me.XrLabel78.Multiline = True
        Me.XrLabel78.Name = "XrLabel78"
        Me.XrLabel78.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel78.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel78.StylePriority.UseFont = False
        Me.XrLabel78.Text = "XrLabel73"
        '
        'XrLabel75
        '
        Me.XrLabel75.CanGrow = False
        Me.XrLabel75.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Caracteristicas]")})
        Me.XrLabel75.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel75.LocationFloat = New DevExpress.Utils.PointFloat(832.9742!, 80.2943!)
        Me.XrLabel75.Multiline = True
        Me.XrLabel75.Name = "XrLabel75"
        Me.XrLabel75.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel75.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel75.StylePriority.UseFont = False
        Me.XrLabel75.Text = "XrLabel73"
        '
        'XrLabel74
        '
        Me.XrLabel74.CanGrow = False
        Me.XrLabel74.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Caracteristicas]")})
        Me.XrLabel74.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel74.LocationFloat = New DevExpress.Utils.PointFloat(535.1563!, 80.2943!)
        Me.XrLabel74.Multiline = True
        Me.XrLabel74.Name = "XrLabel74"
        Me.XrLabel74.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel74.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel74.StylePriority.UseFont = False
        Me.XrLabel74.Text = "XrLabel73"
        '
        'XrLabel73
        '
        Me.XrLabel73.CanGrow = False
        Me.XrLabel73.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Caracteristicas]")})
        Me.XrLabel73.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel73.LocationFloat = New DevExpress.Utils.PointFloat(233.599!, 80.29427!)
        Me.XrLabel73.Multiline = True
        Me.XrLabel73.Name = "XrLabel73"
        Me.XrLabel73.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel73.SizeF = New System.Drawing.SizeF(100.0!, 121.4369!)
        Me.XrLabel73.StylePriority.UseFont = False
        Me.XrLabel73.Text = "XrLabel73"
        '
        'XrLabel55
        '
        Me.XrLabel55.CanGrow = False
        Me.XrLabel55.CanShrink = True
        Me.XrLabel55.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio3]")})
        Me.XrLabel55.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel55.LocationFloat = New DevExpress.Utils.PointFloat(70.64013!, 669.0138!)
        Me.XrLabel55.Multiline = True
        Me.XrLabel55.Name = "XrLabel55"
        Me.XrLabel55.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel55.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel55.StylePriority.UseFont = False
        Me.XrLabel55.StylePriority.UseTextAlignment = False
        Me.XrLabel55.Text = "$99,999.00"
        Me.XrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel55.TextFormatString = "{0:N0}"
        '
        'XrLabel56
        '
        Me.XrLabel56.CanGrow = False
        Me.XrLabel56.CanShrink = True
        Me.XrLabel56.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio3]")})
        Me.XrLabel56.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel56.LocationFloat = New DevExpress.Utils.PointFloat(360.8485!, 669.0138!)
        Me.XrLabel56.Multiline = True
        Me.XrLabel56.Name = "XrLabel56"
        Me.XrLabel56.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel56.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel56.StylePriority.UseFont = False
        Me.XrLabel56.StylePriority.UseTextAlignment = False
        Me.XrLabel56.Text = "$99,999.00"
        Me.XrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel56.TextFormatString = "{0:N0}"
        '
        'XrLabel57
        '
        Me.XrLabel57.CanGrow = False
        Me.XrLabel57.CanShrink = True
        Me.XrLabel57.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio3]")})
        Me.XrLabel57.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel57.LocationFloat = New DevExpress.Utils.PointFloat(665.015!, 669.0138!)
        Me.XrLabel57.Multiline = True
        Me.XrLabel57.Name = "XrLabel57"
        Me.XrLabel57.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel57.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel57.StylePriority.UseFont = False
        Me.XrLabel57.StylePriority.UseTextAlignment = False
        Me.XrLabel57.Text = "$99,999.00"
        Me.XrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel57.TextFormatString = "{0:N0}"
        '
        'XrLabel58
        '
        Me.XrLabel58.CanGrow = False
        Me.XrLabel58.CanShrink = True
        Me.XrLabel58.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio2]")})
        Me.XrLabel58.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel58.LocationFloat = New DevExpress.Utils.PointFloat(648.0156!, 784.399!)
        Me.XrLabel58.Multiline = True
        Me.XrLabel58.Name = "XrLabel58"
        Me.XrLabel58.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel58.SizeF = New System.Drawing.SizeF(103.5423!, 14.52594!)
        Me.XrLabel58.StylePriority.UseFont = False
        Me.XrLabel58.StylePriority.UseTextAlignment = False
        Me.XrLabel58.Text = "$99,999.00"
        Me.XrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel58.TextFormatString = "{0:$0}"
        '
        'XrLabel59
        '
        Me.XrLabel59.CanGrow = False
        Me.XrLabel59.CanShrink = True
        Me.XrLabel59.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio5]")})
        Me.XrLabel59.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel59.LocationFloat = New DevExpress.Utils.PointFloat(665.0153!, 713.7064!)
        Me.XrLabel59.Multiline = True
        Me.XrLabel59.Name = "XrLabel59"
        Me.XrLabel59.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel59.SizeF = New System.Drawing.SizeF(85.16736!, 43.69258!)
        Me.XrLabel59.StylePriority.UseFont = False
        Me.XrLabel59.StylePriority.UseTextAlignment = False
        Me.XrLabel59.Text = "$99,999.00"
        Me.XrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel59.TextFormatString = "{0:N0}"
        '
        'XrLabel60
        '
        Me.XrLabel60.CanGrow = False
        Me.XrLabel60.CanShrink = True
        Me.XrLabel60.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Precio1]")})
        Me.XrLabel60.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel60.LocationFloat = New DevExpress.Utils.PointFloat(648.0156!, 770.399!)
        Me.XrLabel60.Multiline = True
        Me.XrLabel60.Name = "XrLabel60"
        Me.XrLabel60.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel60.SizeF = New System.Drawing.SizeF(103.5423!, 14.52588!)
        Me.XrLabel60.StylePriority.UseFont = False
        Me.XrLabel60.StylePriority.UseTextAlignment = False
        Me.XrLabel60.Text = "$99,999.00"
        Me.XrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel60.TextFormatString = "{0:$0}"
        '
        'XrLabel61
        '
        Me.XrLabel61.CanGrow = False
        Me.XrLabel61.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Estilo]")})
        Me.XrLabel61.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel61.LocationFloat = New DevExpress.Utils.PointFloat(875.0576!, 635.1777!)
        Me.XrLabel61.Name = "XrLabel61"
        Me.XrLabel61.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel61.SizeF = New System.Drawing.SizeF(57.91681!, 16.66667!)
        Me.XrLabel61.StylePriority.UseFont = False
        Me.XrLabel61.Text = "Estilo:"
        '
        'XrLabel62
        '
        Me.XrLabel62.CanGrow = False
        Me.XrLabel62.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R4Marca]")})
        Me.XrLabel62.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel62.LocationFloat = New DevExpress.Utils.PointFloat(875.0576!, 652.037!)
        Me.XrLabel62.Name = "XrLabel62"
        Me.XrLabel62.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel62.SizeF = New System.Drawing.SizeF(57.91681!, 16.66672!)
        Me.XrLabel62.StylePriority.UseFont = False
        Me.XrLabel62.Text = "Marca:"
        '
        'XrLabel63
        '
        Me.XrLabel63.CanGrow = False
        Me.XrLabel63.CanShrink = True
        Me.XrLabel63.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio2]")})
        Me.XrLabel63.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel63.LocationFloat = New DevExpress.Utils.PointFloat(343.8487!, 784.399!)
        Me.XrLabel63.Multiline = True
        Me.XrLabel63.Name = "XrLabel63"
        Me.XrLabel63.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel63.SizeF = New System.Drawing.SizeF(103.5424!, 14.52594!)
        Me.XrLabel63.StylePriority.UseFont = False
        Me.XrLabel63.StylePriority.UseTextAlignment = False
        Me.XrLabel63.Text = "$99,999.00"
        Me.XrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel63.TextFormatString = "{0:$0}"
        '
        'XrLabel64
        '
        Me.XrLabel64.CanGrow = False
        Me.XrLabel64.CanShrink = True
        Me.XrLabel64.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio5]")})
        Me.XrLabel64.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel64.LocationFloat = New DevExpress.Utils.PointFloat(360.8485!, 713.7064!)
        Me.XrLabel64.Multiline = True
        Me.XrLabel64.Name = "XrLabel64"
        Me.XrLabel64.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel64.SizeF = New System.Drawing.SizeF(85.16742!, 43.69258!)
        Me.XrLabel64.StylePriority.UseFont = False
        Me.XrLabel64.StylePriority.UseTextAlignment = False
        Me.XrLabel64.Text = "$99,999.00"
        Me.XrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel64.TextFormatString = "{0:N0}"
        '
        'XrLabel65
        '
        Me.XrLabel65.CanGrow = False
        Me.XrLabel65.CanShrink = True
        Me.XrLabel65.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Precio1]")})
        Me.XrLabel65.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel65.LocationFloat = New DevExpress.Utils.PointFloat(343.8487!, 770.399!)
        Me.XrLabel65.Multiline = True
        Me.XrLabel65.Name = "XrLabel65"
        Me.XrLabel65.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel65.SizeF = New System.Drawing.SizeF(103.5424!, 14.52588!)
        Me.XrLabel65.StylePriority.UseFont = False
        Me.XrLabel65.StylePriority.UseTextAlignment = False
        Me.XrLabel65.Text = "$99,999.00"
        Me.XrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel65.TextFormatString = "{0:$0}"
        '
        'XrLabel66
        '
        Me.XrLabel66.CanGrow = False
        Me.XrLabel66.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Estilo]")})
        Me.XrLabel66.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel66.LocationFloat = New DevExpress.Utils.PointFloat(577.2398!, 635.1777!)
        Me.XrLabel66.Name = "XrLabel66"
        Me.XrLabel66.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel66.SizeF = New System.Drawing.SizeF(57.91669!, 16.66667!)
        Me.XrLabel66.StylePriority.UseFont = False
        Me.XrLabel66.Text = "Estilo:"
        '
        'XrLabel67
        '
        Me.XrLabel67.CanGrow = False
        Me.XrLabel67.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R4Marca]")})
        Me.XrLabel67.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel67.LocationFloat = New DevExpress.Utils.PointFloat(577.2398!, 652.037!)
        Me.XrLabel67.Name = "XrLabel67"
        Me.XrLabel67.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel67.SizeF = New System.Drawing.SizeF(57.91669!, 16.66672!)
        Me.XrLabel67.StylePriority.UseFont = False
        Me.XrLabel67.Text = "Marca:"
        '
        'XrLabel68
        '
        Me.XrLabel68.CanGrow = False
        Me.XrLabel68.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Estilo]")})
        Me.XrLabel68.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel68.LocationFloat = New DevExpress.Utils.PointFloat(275.6825!, 635.1777!)
        Me.XrLabel68.Name = "XrLabel68"
        Me.XrLabel68.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel68.SizeF = New System.Drawing.SizeF(57.91666!, 16.66667!)
        Me.XrLabel68.StylePriority.UseFont = False
        Me.XrLabel68.Text = "Estilo:"
        '
        'XrLabel69
        '
        Me.XrLabel69.CanGrow = False
        Me.XrLabel69.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Marca]")})
        Me.XrLabel69.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel69.LocationFloat = New DevExpress.Utils.PointFloat(275.6825!, 652.037!)
        Me.XrLabel69.Name = "XrLabel69"
        Me.XrLabel69.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel69.SizeF = New System.Drawing.SizeF(57.91666!, 16.66672!)
        Me.XrLabel69.StylePriority.UseFont = False
        Me.XrLabel69.Text = "Marca:"
        '
        'XrLabel70
        '
        Me.XrLabel70.CanGrow = False
        Me.XrLabel70.CanShrink = True
        Me.XrLabel70.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio2]")})
        Me.XrLabel70.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel70.LocationFloat = New DevExpress.Utils.PointFloat(53.64039!, 784.399!)
        Me.XrLabel70.Multiline = True
        Me.XrLabel70.Name = "XrLabel70"
        Me.XrLabel70.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel70.SizeF = New System.Drawing.SizeF(103.5424!, 14.52594!)
        Me.XrLabel70.StylePriority.UseFont = False
        Me.XrLabel70.StylePriority.UseTextAlignment = False
        Me.XrLabel70.Text = "$99,999.00"
        Me.XrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel70.TextFormatString = "{0:$0}"
        '
        'XrLabel71
        '
        Me.XrLabel71.CanGrow = False
        Me.XrLabel71.CanShrink = True
        Me.XrLabel71.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio5]")})
        Me.XrLabel71.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel71.LocationFloat = New DevExpress.Utils.PointFloat(70.64013!, 713.7064!)
        Me.XrLabel71.Multiline = True
        Me.XrLabel71.Name = "XrLabel71"
        Me.XrLabel71.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel71.SizeF = New System.Drawing.SizeF(85.16738!, 43.69258!)
        Me.XrLabel71.StylePriority.UseFont = False
        Me.XrLabel71.StylePriority.UseTextAlignment = False
        Me.XrLabel71.Text = "$99,999.00"
        Me.XrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel71.TextFormatString = "{0:N0}"
        '
        'XrLabel72
        '
        Me.XrLabel72.CanGrow = False
        Me.XrLabel72.CanShrink = True
        Me.XrLabel72.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R4Precio1]")})
        Me.XrLabel72.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel72.LocationFloat = New DevExpress.Utils.PointFloat(53.64039!, 770.399!)
        Me.XrLabel72.Multiline = True
        Me.XrLabel72.Name = "XrLabel72"
        Me.XrLabel72.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel72.SizeF = New System.Drawing.SizeF(103.5424!, 14.52588!)
        Me.XrLabel72.StylePriority.UseFont = False
        Me.XrLabel72.StylePriority.UseTextAlignment = False
        Me.XrLabel72.Text = "$99,999.00"
        Me.XrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel72.TextFormatString = "{0:$0}"
        '
        'XrLabel37
        '
        Me.XrLabel37.CanGrow = False
        Me.XrLabel37.CanShrink = True
        Me.XrLabel37.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio3]")})
        Me.XrLabel37.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel37.LocationFloat = New DevExpress.Utils.PointFloat(70.64005!, 473.2826!)
        Me.XrLabel37.Multiline = True
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel37.StylePriority.UseFont = False
        Me.XrLabel37.StylePriority.UseTextAlignment = False
        Me.XrLabel37.Text = "$99,999.00"
        Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel37.TextFormatString = "{0:N0}"
        '
        'XrLabel38
        '
        Me.XrLabel38.CanGrow = False
        Me.XrLabel38.CanShrink = True
        Me.XrLabel38.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio3]")})
        Me.XrLabel38.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel38.LocationFloat = New DevExpress.Utils.PointFloat(360.8484!, 473.2826!)
        Me.XrLabel38.Multiline = True
        Me.XrLabel38.Name = "XrLabel38"
        Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel38.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel38.StylePriority.UseFont = False
        Me.XrLabel38.StylePriority.UseTextAlignment = False
        Me.XrLabel38.Text = "$99,999.00"
        Me.XrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel38.TextFormatString = "{0:N0}"
        '
        'XrLabel39
        '
        Me.XrLabel39.CanGrow = False
        Me.XrLabel39.CanShrink = True
        Me.XrLabel39.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio3]")})
        Me.XrLabel39.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel39.LocationFloat = New DevExpress.Utils.PointFloat(665.0153!, 473.2826!)
        Me.XrLabel39.Multiline = True
        Me.XrLabel39.Name = "XrLabel39"
        Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel39.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel39.StylePriority.UseFont = False
        Me.XrLabel39.StylePriority.UseTextAlignment = False
        Me.XrLabel39.Text = "$99,999.00"
        Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel39.TextFormatString = "{0:N0}"
        '
        'XrLabel40
        '
        Me.XrLabel40.CanGrow = False
        Me.XrLabel40.CanShrink = True
        Me.XrLabel40.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio2]")})
        Me.XrLabel40.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(648.0156!, 590.1936!)
        Me.XrLabel40.Multiline = True
        Me.XrLabel40.Name = "XrLabel40"
        Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel40.SizeF = New System.Drawing.SizeF(103.5423!, 14.52588!)
        Me.XrLabel40.StylePriority.UseFont = False
        Me.XrLabel40.StylePriority.UseTextAlignment = False
        Me.XrLabel40.Text = "$99,999.00"
        Me.XrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel40.TextFormatString = "{0:$0}"
        '
        'XrLabel41
        '
        Me.XrLabel41.CanGrow = False
        Me.XrLabel41.CanShrink = True
        Me.XrLabel41.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio5]")})
        Me.XrLabel41.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(665.0153!, 517.9752!)
        Me.XrLabel41.Multiline = True
        Me.XrLabel41.Name = "XrLabel41"
        Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel41.SizeF = New System.Drawing.SizeF(85.16736!, 43.69258!)
        Me.XrLabel41.StylePriority.UseFont = False
        Me.XrLabel41.StylePriority.UseTextAlignment = False
        Me.XrLabel41.Text = "$99,999.00"
        Me.XrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel41.TextFormatString = "{0:N0}"
        '
        'XrLabel42
        '
        Me.XrLabel42.CanGrow = False
        Me.XrLabel42.CanShrink = True
        Me.XrLabel42.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Precio1]")})
        Me.XrLabel42.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel42.LocationFloat = New DevExpress.Utils.PointFloat(648.0156!, 574.6677!)
        Me.XrLabel42.Multiline = True
        Me.XrLabel42.Name = "XrLabel42"
        Me.XrLabel42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel42.SizeF = New System.Drawing.SizeF(103.5423!, 14.52588!)
        Me.XrLabel42.StylePriority.UseFont = False
        Me.XrLabel42.StylePriority.UseTextAlignment = False
        Me.XrLabel42.Text = "$99,999.00"
        Me.XrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel42.TextFormatString = "{0:$0}"
        '
        'XrLabel43
        '
        Me.XrLabel43.CanGrow = False
        Me.XrLabel43.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Estilo]")})
        Me.XrLabel43.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel43.LocationFloat = New DevExpress.Utils.PointFloat(875.0575!, 439.4466!)
        Me.XrLabel43.Name = "XrLabel43"
        Me.XrLabel43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel43.SizeF = New System.Drawing.SizeF(57.91681!, 16.66667!)
        Me.XrLabel43.StylePriority.UseFont = False
        Me.XrLabel43.Text = "Estilo:"
        '
        'XrLabel44
        '
        Me.XrLabel44.CanGrow = False
        Me.XrLabel44.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R3Marca]")})
        Me.XrLabel44.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel44.LocationFloat = New DevExpress.Utils.PointFloat(875.0575!, 456.3058!)
        Me.XrLabel44.Name = "XrLabel44"
        Me.XrLabel44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel44.SizeF = New System.Drawing.SizeF(57.91681!, 16.66672!)
        Me.XrLabel44.StylePriority.UseFont = False
        Me.XrLabel44.Text = "Marca:"
        '
        'XrLabel45
        '
        Me.XrLabel45.CanGrow = False
        Me.XrLabel45.CanShrink = True
        Me.XrLabel45.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio2]")})
        Me.XrLabel45.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel45.LocationFloat = New DevExpress.Utils.PointFloat(343.8486!, 590.1936!)
        Me.XrLabel45.Multiline = True
        Me.XrLabel45.Name = "XrLabel45"
        Me.XrLabel45.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel45.SizeF = New System.Drawing.SizeF(103.5425!, 14.52588!)
        Me.XrLabel45.StylePriority.UseFont = False
        Me.XrLabel45.StylePriority.UseTextAlignment = False
        Me.XrLabel45.Text = "$99,999.00"
        Me.XrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel45.TextFormatString = "{0:$0}"
        '
        'XrLabel46
        '
        Me.XrLabel46.CanGrow = False
        Me.XrLabel46.CanShrink = True
        Me.XrLabel46.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio5]")})
        Me.XrLabel46.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel46.LocationFloat = New DevExpress.Utils.PointFloat(360.8484!, 517.9752!)
        Me.XrLabel46.Multiline = True
        Me.XrLabel46.Name = "XrLabel46"
        Me.XrLabel46.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel46.SizeF = New System.Drawing.SizeF(85.16742!, 43.69258!)
        Me.XrLabel46.StylePriority.UseFont = False
        Me.XrLabel46.StylePriority.UseTextAlignment = False
        Me.XrLabel46.Text = "$99,999.00"
        Me.XrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel46.TextFormatString = "{0:N0}"
        '
        'XrLabel47
        '
        Me.XrLabel47.CanGrow = False
        Me.XrLabel47.CanShrink = True
        Me.XrLabel47.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Precio1]")})
        Me.XrLabel47.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel47.LocationFloat = New DevExpress.Utils.PointFloat(343.8486!, 574.6677!)
        Me.XrLabel47.Multiline = True
        Me.XrLabel47.Name = "XrLabel47"
        Me.XrLabel47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel47.SizeF = New System.Drawing.SizeF(103.5425!, 14.52588!)
        Me.XrLabel47.StylePriority.UseFont = False
        Me.XrLabel47.StylePriority.UseTextAlignment = False
        Me.XrLabel47.Text = "$99,999.00"
        Me.XrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel47.TextFormatString = "{0:$0}"
        '
        'XrLabel48
        '
        Me.XrLabel48.CanGrow = False
        Me.XrLabel48.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Estilo]")})
        Me.XrLabel48.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel48.LocationFloat = New DevExpress.Utils.PointFloat(577.2397!, 439.4466!)
        Me.XrLabel48.Name = "XrLabel48"
        Me.XrLabel48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel48.SizeF = New System.Drawing.SizeF(57.91669!, 16.66667!)
        Me.XrLabel48.StylePriority.UseFont = False
        Me.XrLabel48.Text = "Estilo:"
        '
        'XrLabel49
        '
        Me.XrLabel49.CanGrow = False
        Me.XrLabel49.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R3Marca]")})
        Me.XrLabel49.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel49.LocationFloat = New DevExpress.Utils.PointFloat(577.2397!, 456.3058!)
        Me.XrLabel49.Name = "XrLabel49"
        Me.XrLabel49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel49.SizeF = New System.Drawing.SizeF(57.91669!, 16.66672!)
        Me.XrLabel49.StylePriority.UseFont = False
        Me.XrLabel49.Text = "Marca:"
        '
        'XrLabel50
        '
        Me.XrLabel50.CanGrow = False
        Me.XrLabel50.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Estilo]")})
        Me.XrLabel50.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel50.LocationFloat = New DevExpress.Utils.PointFloat(275.6825!, 439.4466!)
        Me.XrLabel50.Name = "XrLabel50"
        Me.XrLabel50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel50.SizeF = New System.Drawing.SizeF(57.91666!, 16.66667!)
        Me.XrLabel50.StylePriority.UseFont = False
        Me.XrLabel50.Text = "Estilo:"
        '
        'XrLabel51
        '
        Me.XrLabel51.CanGrow = False
        Me.XrLabel51.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Marca]")})
        Me.XrLabel51.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel51.LocationFloat = New DevExpress.Utils.PointFloat(275.6825!, 456.3058!)
        Me.XrLabel51.Name = "XrLabel51"
        Me.XrLabel51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel51.SizeF = New System.Drawing.SizeF(57.91666!, 16.66672!)
        Me.XrLabel51.StylePriority.UseFont = False
        Me.XrLabel51.Text = "Marca:"
        '
        'XrLabel52
        '
        Me.XrLabel52.CanGrow = False
        Me.XrLabel52.CanShrink = True
        Me.XrLabel52.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio2]")})
        Me.XrLabel52.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel52.LocationFloat = New DevExpress.Utils.PointFloat(53.64033!, 590.1936!)
        Me.XrLabel52.Multiline = True
        Me.XrLabel52.Name = "XrLabel52"
        Me.XrLabel52.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel52.SizeF = New System.Drawing.SizeF(103.5424!, 14.52588!)
        Me.XrLabel52.StylePriority.UseFont = False
        Me.XrLabel52.StylePriority.UseTextAlignment = False
        Me.XrLabel52.Text = "$99,999.00"
        Me.XrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel52.TextFormatString = "{0:$0}"
        '
        'XrLabel53
        '
        Me.XrLabel53.CanGrow = False
        Me.XrLabel53.CanShrink = True
        Me.XrLabel53.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio5]")})
        Me.XrLabel53.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel53.LocationFloat = New DevExpress.Utils.PointFloat(70.64005!, 517.9752!)
        Me.XrLabel53.Multiline = True
        Me.XrLabel53.Name = "XrLabel53"
        Me.XrLabel53.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel53.SizeF = New System.Drawing.SizeF(85.16738!, 43.69258!)
        Me.XrLabel53.StylePriority.UseFont = False
        Me.XrLabel53.StylePriority.UseTextAlignment = False
        Me.XrLabel53.Text = "$99,999.00"
        Me.XrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel53.TextFormatString = "{0:N0}"
        '
        'XrLabel54
        '
        Me.XrLabel54.CanGrow = False
        Me.XrLabel54.CanShrink = True
        Me.XrLabel54.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R3Precio1]")})
        Me.XrLabel54.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel54.LocationFloat = New DevExpress.Utils.PointFloat(53.64033!, 574.6677!)
        Me.XrLabel54.Multiline = True
        Me.XrLabel54.Name = "XrLabel54"
        Me.XrLabel54.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel54.SizeF = New System.Drawing.SizeF(103.5424!, 14.52588!)
        Me.XrLabel54.StylePriority.UseFont = False
        Me.XrLabel54.StylePriority.UseTextAlignment = False
        Me.XrLabel54.Text = "$99,999.00"
        Me.XrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel54.TextFormatString = "{0:$0}"
        '
        'XrLabel19
        '
        Me.XrLabel19.CanGrow = False
        Me.XrLabel19.CanShrink = True
        Me.XrLabel19.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio1]")})
        Me.XrLabel19.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(53.6402!, 378.9365!)
        Me.XrLabel19.Multiline = True
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(103.5424!, 14.52591!)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "$99,999.00"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel19.TextFormatString = "{0:$0}"
        '
        'XrLabel20
        '
        Me.XrLabel20.CanGrow = False
        Me.XrLabel20.CanShrink = True
        Me.XrLabel20.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio5]")})
        Me.XrLabel20.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(70.63995!, 322.244!)
        Me.XrLabel20.Multiline = True
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(85.16738!, 43.69258!)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "$99,999.00"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel20.TextFormatString = "{0:N0}"
        '
        'XrLabel21
        '
        Me.XrLabel21.CanGrow = False
        Me.XrLabel21.CanShrink = True
        Me.XrLabel21.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio2]")})
        Me.XrLabel21.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(53.6402!, 394.4623!)
        Me.XrLabel21.Multiline = True
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(103.5424!, 14.52594!)
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "$99,999.00"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel21.TextFormatString = "{0:$0}"
        '
        'XrLabel22
        '
        Me.XrLabel22.CanGrow = False
        Me.XrLabel22.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Marca]")})
        Me.XrLabel22.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(275.6823!, 259.0488!)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(57.91666!, 16.66672!)
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.Text = "Marca:"
        '
        'XrLabel23
        '
        Me.XrLabel23.CanGrow = False
        Me.XrLabel23.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Estilo]")})
        Me.XrLabel23.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(275.6823!, 242.1895!)
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(57.91666!, 16.66667!)
        Me.XrLabel23.StylePriority.UseFont = False
        Me.XrLabel23.Text = "Estilo:"
        '
        'XrLabel24
        '
        Me.XrLabel24.CanGrow = False
        Me.XrLabel24.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Marca]")})
        Me.XrLabel24.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(577.2396!, 259.0488!)
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(57.91669!, 16.66672!)
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.Text = "Marca:"
        '
        'XrLabel25
        '
        Me.XrLabel25.CanGrow = False
        Me.XrLabel25.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Estilo]")})
        Me.XrLabel25.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(577.2396!, 242.1895!)
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(57.91669!, 16.66667!)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.Text = "Estilo:"
        '
        'XrLabel26
        '
        Me.XrLabel26.CanGrow = False
        Me.XrLabel26.CanShrink = True
        Me.XrLabel26.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio1]")})
        Me.XrLabel26.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(343.8485!, 378.9365!)
        Me.XrLabel26.Multiline = True
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(103.5424!, 14.52591!)
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "$99,999.00"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel26.TextFormatString = "{0:$0}"
        '
        'XrLabel27
        '
        Me.XrLabel27.CanGrow = False
        Me.XrLabel27.CanShrink = True
        Me.XrLabel27.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio5]")})
        Me.XrLabel27.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(360.8483!, 322.244!)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.SizeF = New System.Drawing.SizeF(85.16742!, 43.69258!)
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.StylePriority.UseTextAlignment = False
        Me.XrLabel27.Text = "$99,999.00"
        Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel27.TextFormatString = "{0:N0}"
        '
        'XrLabel28
        '
        Me.XrLabel28.CanGrow = False
        Me.XrLabel28.CanShrink = True
        Me.XrLabel28.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio2]")})
        Me.XrLabel28.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(343.8485!, 394.4623!)
        Me.XrLabel28.Multiline = True
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(103.5424!, 14.52594!)
        Me.XrLabel28.StylePriority.UseFont = False
        Me.XrLabel28.StylePriority.UseTextAlignment = False
        Me.XrLabel28.Text = "$99,999.00"
        Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel28.TextFormatString = "{0:$0}"
        '
        'XrLabel29
        '
        Me.XrLabel29.CanGrow = False
        Me.XrLabel29.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Marca]")})
        Me.XrLabel29.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(875.0574!, 259.0488!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(57.91681!, 16.66672!)
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.Text = "Marca:"
        '
        'XrLabel30
        '
        Me.XrLabel30.CanGrow = False
        Me.XrLabel30.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Estilo]")})
        Me.XrLabel30.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(875.0574!, 242.1895!)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(57.91681!, 16.66667!)
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.Text = "Estilo:"
        '
        'XrLabel31
        '
        Me.XrLabel31.CanGrow = False
        Me.XrLabel31.CanShrink = True
        Me.XrLabel31.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio1]")})
        Me.XrLabel31.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(648.0154!, 378.9365!)
        Me.XrLabel31.Multiline = True
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.SizeF = New System.Drawing.SizeF(103.5423!, 14.52591!)
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.StylePriority.UseTextAlignment = False
        Me.XrLabel31.Text = "$99,999.00"
        Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel31.TextFormatString = "{0:$0}"
        '
        'XrLabel32
        '
        Me.XrLabel32.CanGrow = False
        Me.XrLabel32.CanShrink = True
        Me.XrLabel32.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio5]")})
        Me.XrLabel32.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(665.0151!, 322.244!)
        Me.XrLabel32.Multiline = True
        Me.XrLabel32.Name = "XrLabel32"
        Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel32.SizeF = New System.Drawing.SizeF(85.16736!, 43.69258!)
        Me.XrLabel32.StylePriority.UseFont = False
        Me.XrLabel32.StylePriority.UseTextAlignment = False
        Me.XrLabel32.Text = "$99,999.00"
        Me.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel32.TextFormatString = "{0:N0}"
        '
        'XrLabel33
        '
        Me.XrLabel33.CanGrow = False
        Me.XrLabel33.CanShrink = True
        Me.XrLabel33.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio2]")})
        Me.XrLabel33.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(648.0154!, 394.4623!)
        Me.XrLabel33.Multiline = True
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.SizeF = New System.Drawing.SizeF(103.5423!, 14.52594!)
        Me.XrLabel33.StylePriority.UseFont = False
        Me.XrLabel33.StylePriority.UseTextAlignment = False
        Me.XrLabel33.Text = "$99,999.00"
        Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel33.TextFormatString = "{0:$0}"
        '
        'XrLabel34
        '
        Me.XrLabel34.CanGrow = False
        Me.XrLabel34.CanShrink = True
        Me.XrLabel34.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R2Precio3]")})
        Me.XrLabel34.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(665.0151!, 277.5513!)
        Me.XrLabel34.Multiline = True
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel34.StylePriority.UseFont = False
        Me.XrLabel34.StylePriority.UseTextAlignment = False
        Me.XrLabel34.Text = "$99,999.00"
        Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel34.TextFormatString = "{0:N0}"
        '
        'XrLabel35
        '
        Me.XrLabel35.CanGrow = False
        Me.XrLabel35.CanShrink = True
        Me.XrLabel35.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R2Precio3]")})
        Me.XrLabel35.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel35.LocationFloat = New DevExpress.Utils.PointFloat(360.8483!, 277.5513!)
        Me.XrLabel35.Multiline = True
        Me.XrLabel35.Name = "XrLabel35"
        Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel35.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel35.StylePriority.UseFont = False
        Me.XrLabel35.StylePriority.UseTextAlignment = False
        Me.XrLabel35.Text = "$99,999.00"
        Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel35.TextFormatString = "{0:N0}"
        '
        'XrLabel36
        '
        Me.XrLabel36.CanGrow = False
        Me.XrLabel36.CanShrink = True
        Me.XrLabel36.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R2Precio3]")})
        Me.XrLabel36.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel36.LocationFloat = New DevExpress.Utils.PointFloat(70.63995!, 277.5513!)
        Me.XrLabel36.Multiline = True
        Me.XrLabel36.Name = "XrLabel36"
        Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel36.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel36.StylePriority.UseFont = False
        Me.XrLabel36.StylePriority.UseTextAlignment = False
        Me.XrLabel36.Text = "$99,999.00"
        Me.XrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel36.TextFormatString = "{0:N0}"
        '
        'XrLabel16
        '
        Me.XrLabel16.CanGrow = False
        Me.XrLabel16.CanShrink = True
        Me.XrLabel16.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio3]")})
        Me.XrLabel16.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(70.63995!, 81.29427!)
        Me.XrLabel16.Multiline = True
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "$99,999.00"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel16.TextFormatString = "{0:N0}"
        '
        'XrLabel17
        '
        Me.XrLabel17.CanGrow = False
        Me.XrLabel17.CanShrink = True
        Me.XrLabel17.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio3]")})
        Me.XrLabel17.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(360.8483!, 81.29427!)
        Me.XrLabel17.Multiline = True
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.Text = "$99,999.00"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel17.TextFormatString = "{0:N0}"
        '
        'XrLabel18
        '
        Me.XrLabel18.CanGrow = False
        Me.XrLabel18.CanShrink = True
        Me.XrLabel18.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio3]")})
        Me.XrLabel18.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(665.015!, 81.29427!)
        Me.XrLabel18.Multiline = True
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(163.2924!, 43.69258!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "$99,999.00"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel18.TextFormatString = "{0:N0}"
        '
        'XrLabel11
        '
        Me.XrLabel11.CanGrow = False
        Me.XrLabel11.CanShrink = True
        Me.XrLabel11.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio2]")})
        Me.XrLabel11.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(648.0153!, 197.2053!)
        Me.XrLabel11.Multiline = True
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(103.5423!, 14.52592!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "$99,999.00"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel11.TextFormatString = "{0:$0}"
        '
        'XrLabel12
        '
        Me.XrLabel12.CanGrow = False
        Me.XrLabel12.CanShrink = True
        Me.XrLabel12.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio5]")})
        Me.XrLabel12.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(665.015!, 125.9868!)
        Me.XrLabel12.Multiline = True
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(85.16736!, 43.69258!)
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "$99,999.00"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel12.TextFormatString = "{0:N0}"
        '
        'XrLabel13
        '
        Me.XrLabel13.CanGrow = False
        Me.XrLabel13.CanShrink = True
        Me.XrLabel13.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Precio1]")})
        Me.XrLabel13.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(648.0153!, 182.6794!)
        Me.XrLabel13.Multiline = True
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(103.5423!, 14.52588!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "$99,999.00"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel13.TextFormatString = "{0:$0}"
        '
        'XrLabel14
        '
        Me.XrLabel14.CanGrow = False
        Me.XrLabel14.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Estilo]")})
        Me.XrLabel14.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(875.0574!, 46.45834!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(57.91681!, 16.66667!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.Text = "Estilo:"
        '
        'XrLabel15
        '
        Me.XrLabel15.CanGrow = False
        Me.XrLabel15.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaC].[R1Marca]")})
        Me.XrLabel15.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(875.0574!, 63.3176!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(57.91681!, 16.66672!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.Text = "Marca:"
        '
        'XrLabel6
        '
        Me.XrLabel6.CanGrow = False
        Me.XrLabel6.CanShrink = True
        Me.XrLabel6.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio2]")})
        Me.XrLabel6.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(343.8485!, 197.2053!)
        Me.XrLabel6.Multiline = True
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(103.5424!, 14.52592!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "$99,999.00"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel6.TextFormatString = "{0:$0}"
        '
        'XrLabel7
        '
        Me.XrLabel7.CanGrow = False
        Me.XrLabel7.CanShrink = True
        Me.XrLabel7.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio5]")})
        Me.XrLabel7.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(360.8483!, 125.9868!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(85.16742!, 43.69258!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "$99,999.00"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel7.TextFormatString = "{0:N0}"
        '
        'XrLabel8
        '
        Me.XrLabel8.CanGrow = False
        Me.XrLabel8.CanShrink = True
        Me.XrLabel8.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Precio1]")})
        Me.XrLabel8.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(343.8485!, 182.6794!)
        Me.XrLabel8.Multiline = True
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(103.5424!, 14.52588!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "$99,999.00"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel8.TextFormatString = "{0:$0}"
        '
        'XrLabel9
        '
        Me.XrLabel9.CanGrow = False
        Me.XrLabel9.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Estilo]")})
        Me.XrLabel9.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(577.2397!, 46.45834!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(57.91669!, 16.66667!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.Text = "Estilo:"
        '
        'XrLabel10
        '
        Me.XrLabel10.CanGrow = False
        Me.XrLabel10.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaB].[R1Marca]")})
        Me.XrLabel10.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(577.2397!, 63.3176!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(57.91669!, 16.66672!)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.Text = "Marca:"
        '
        'XrLabel2
        '
        Me.XrLabel2.CanGrow = False
        Me.XrLabel2.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Estilo]")})
        Me.XrLabel2.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(275.6823!, 46.45834!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(57.91666!, 16.66667!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "Estilo:"
        '
        'XrLabel3
        '
        Me.XrLabel3.CanGrow = False
        Me.XrLabel3.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Marca]")})
        Me.XrLabel3.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(275.6823!, 63.3176!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(57.91666!, 16.66672!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "Marca:"
        '
        'XrLabel5
        '
        Me.XrLabel5.CanGrow = False
        Me.XrLabel5.CanShrink = True
        Me.XrLabel5.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio2]")})
        Me.XrLabel5.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(53.64014!, 197.2053!)
        Me.XrLabel5.Multiline = True
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(103.5424!, 14.52592!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "$99,999.00"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel5.TextFormatString = "{0:$0}"
        '
        'XrLabel1
        '
        Me.XrLabel1.CanGrow = False
        Me.XrLabel1.CanShrink = True
        Me.XrLabel1.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio5]")})
        Me.XrLabel1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 24.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(70.63995!, 125.9868!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(85.16738!, 43.69258!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "$99,999.00"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrLabel1.TextFormatString = "{0:N0}"
        '
        'XrLabel4
        '
        Me.XrLabel4.CanGrow = False
        Me.XrLabel4.CanShrink = True
        Me.XrLabel4.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ColumnaA].[R1Precio1]")})
        Me.XrLabel4.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(53.64014!, 182.6794!)
        Me.XrLabel4.Multiline = True
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(103.5424!, 14.52588!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "$99,999.00"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel4.TextFormatString = "{0:$0}"
        '
        'Pic1
        '
        Me.Pic1.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ImageSource", "[ColumnaA].[R1Caracteristicas]")})
        Me.Pic1.LocationFloat = New DevExpress.Utils.PointFloat(41.87488!, 7.0!)
        Me.Pic1.Name = "Pic1"
        Me.Pic1.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox2
        '
        Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(646.1564!, 7.0!)
        Me.XrPictureBox2.Name = "XrPictureBox2"
        Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(344.0157!, 7.0!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox4
        '
        Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(646.1563!, 201.7312!)
        Me.XrPictureBox4.Name = "XrPictureBox4"
        Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox3
        '
        Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(344.0157!, 201.7312!)
        Me.XrPictureBox3.Name = "XrPictureBox3"
        Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox5
        '
        Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(41.87488!, 201.7312!)
        Me.XrPictureBox5.Name = "XrPictureBox5"
        Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox6
        '
        Me.XrPictureBox6.LocationFloat = New DevExpress.Utils.PointFloat(41.87497!, 397.4624!)
        Me.XrPictureBox6.Name = "XrPictureBox6"
        Me.XrPictureBox6.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox7
        '
        Me.XrPictureBox7.LocationFloat = New DevExpress.Utils.PointFloat(344.0157!, 397.4624!)
        Me.XrPictureBox7.Name = "XrPictureBox7"
        Me.XrPictureBox7.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox8
        '
        Me.XrPictureBox8.LocationFloat = New DevExpress.Utils.PointFloat(646.1564!, 397.4624!)
        Me.XrPictureBox8.Name = "XrPictureBox8"
        Me.XrPictureBox8.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox11
        '
        Me.XrPictureBox11.LocationFloat = New DevExpress.Utils.PointFloat(646.1565!, 593.1937!)
        Me.XrPictureBox11.Name = "XrPictureBox11"
        Me.XrPictureBox11.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox10
        '
        Me.XrPictureBox10.LocationFloat = New DevExpress.Utils.PointFloat(344.0158!, 593.1937!)
        Me.XrPictureBox10.Name = "XrPictureBox10"
        Me.XrPictureBox10.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'XrPictureBox9
        '
        Me.XrPictureBox9.LocationFloat = New DevExpress.Utils.PointFloat(41.87506!, 593.1937!)
        Me.XrPictureBox9.Name = "XrPictureBox9"
        Me.XrPictureBox9.SizeF = New System.Drawing.SizeF(302.1407!, 195.7312!)
        '
        'ods
        '
        Me.ods.DataSource = GetType(proyVOficina_PDV.entPreciador3)
        Me.ods.Name = "ods"
        '
        'XtraRep_PreciadorNA3X4
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.DetailReport})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.ods})
        Me.DataSource = Me.ods
        Me.Landscape = True
        Me.Margins = New DevExpress.Drawing.DXMargins(40.0!, 42.0!, 25.00001!, 18.99998!)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Version = "22.2"
        CType(Me.ods, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents ods As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
    Friend WithEvents DetailReport As DevExpress.XtraReports.UI.DetailReportBand
    Friend WithEvents Detail1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents Pic1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel55 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel56 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel57 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel58 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel59 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel60 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel61 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel62 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel63 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel64 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel65 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel66 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel67 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel68 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel69 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel70 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel71 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel72 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel38 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel39 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel40 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel41 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel42 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel43 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel44 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel45 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel46 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel47 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel48 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel49 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel50 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel51 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel52 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel53 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel54 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox6 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox7 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox8 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox11 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox10 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox9 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel82 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel83 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel84 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel79 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel80 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel81 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel76 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel77 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel78 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel75 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel74 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel73 As DevExpress.XtraReports.UI.XRLabel
End Class
