﻿
Public Class SelCombobox
    Public Tipo As Globales.TipoCombo
    Public IdSelecionado As Integer
    Public ValorSelecionado As String
    Public IdFiltro1 As Integer
    Public IdFiltro2 As String
    Public IdFiltro3 As String
    Public DlbSalida1 As Double = 0
    Public DlbSalida2 As Double = 0
    Public DlbSalida3 As Double = 0

    Private Sub SelCombobox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim MsjError As String = ""
        Dim sw_error As Boolean = False
        Try
            Me.Width = GridC_1.Width + 50
            GridC_1.Visible = False
            GridC_2.Visible = False
            GridC_3.Visible = False
            Calendario1.Visible = False
            oDatos = New Datos_Viscoi
            GridC_2.Top = GridC_1.Top
            GridC_2.Left = GridC_1.Left
            GridC_3.Top = GridC_1.Top
            GridC_3.Left = GridC_1.Left

            Select Case Tipo
                Case Globales.TipoCombo.Asentamientos
                    GridC_3.Visible = True
                    Me.Text = "Colonias " & IdFiltro3
                    lbl_Etiqueta.Text = "Elije una Colonia de " & IdFiltro2 & ":"
                    col_Valor.Caption = "Asentamiento"

                    If oDatos.Geo_Asentamientos_Sel(Globales.oAmbientes.Id_Empresa, IdFiltro3, IdFiltro2, IdFiltro1, dtDatos, MsjError) Then
                        GridC_3.DataSource = dtDatos
                    Else
                        sw_error = True
                    End If
                    GridV_3.BestFitColumns()
                Case Globales.TipoCombo.Fecha
                    Calendario1.Visible = True
                    Calendario1.DateTime = Now()
                    lbl_Etiqueta.Text = "Elije una fecha ..."
            End Select
            ''Select Case Tipo
            ''    Case Globales.TipoCombo.Pedidos
            ''        GridC_2.Visible = True
            ''        GridC_2.Width = 650
            ''        Me.Width = 700
            ''        Me.Text = "Pedidos pendientes"
            ''        lbl_Etiqueta.Text = "Elije un Pedido"
            ''        col_Valor.Caption = "Pedido"
            ''
            ''        If oDatos.WMS_Pedidos_Sel(Globales.oAmbientes.Id_Empresa, IdFiltro1, dtDatos, MsjError) Then
            ''            GridC_2.DataSource = dtDatos
            ''        Else
            ''            sw_error = True
            ''        End If
            ''        GridV_2.BestFitColumns()
            ''        col2_Nombre.Width = 150
            ''    Case Globales.TipoCombo.Proveedores
            ''        GridC_1.Visible = True
            ''        Me.Text = "Catalogo de Proveedores"
            ''        lbl_Etiqueta.Text = "Elije un Proveedor"
            ''        col_Valor.Caption = "Proveedor"
            ''
            ''        If oDatos.Proveedores_Cbo(False, "", dtDatos, MsjError) Then
            ''            GridC_1.DataSource = dtDatos
            ''        Else
            ''            sw_error = True
            ''        End If
            ''    Case Globales.TipoCombo.Areas
            ''        GridC_1.Visible = True
            ''        Me.Text = "Catalogo de Áreas"
            ''        lbl_Etiqueta.Text = "Elije una de las siguientes Áreas"
            ''        col_Valor.Caption = "Areas"
            ''
            ''        If oDatos.WMS_Cedis_Areas_Sel_SEL(Globales.oAmbientes.Id_Empresa, "ACTIVO", False, dtDatos, MsjError) Then
            ''            GridC_1.DataSource = dtDatos
            ''        Else
            ''            sw_error = True
            ''        End If
            ''    Case Globales.TipoCombo.Sucursales
            ''        GridC_1.Visible = True
            ''        Me.Text = "Catalogo de Sucursales"
            ''        lbl_Etiqueta.Text = "Elije una de las siguientes Sucursales"
            ''        col_Valor.Caption = "Sucursal"
            ''        col_Id.FieldName = "clave"
            ''        col_Valor.FieldName = "id_sucursal"
            ''        If oDatos.WMS_Cedis_Sucursales_Sel(Globales.oAmbientes.Id_Empresa, "", False, dtDatos, MsjError) Then
            ''            GridC_1.DataSource = dtDatos
            ''        Else
            ''            sw_error = True
            ''        End If
            ''    Case Globales.TipoCombo.Sucursales_CEDIS
            ''        GridC_1.Visible = True
            ''        Me.Text = "Catalogo de CEDIS"
            ''        lbl_Etiqueta.Text = "Elije una de los siguientes CEDIS"
            ''        col_Valor.Caption = "Sucursal"
            ''        If oDatos.WMS_Cedis_Sucursales_Sel(Globales.oAmbientes.Id_Empresa, "CEDIS", False, dtDatos, MsjError) Then
            ''            GridC_1.DataSource = dtDatos
            ''        Else
            ''            sw_error = True
            ''        End If
            ''    Case Globales.TipoCombo.Ruta
            ''        GridC_1.Visible = True
            ''        Me.Text = "Catalogo de Rutas"
            ''        lbl_Etiqueta.Text = "Elije una de las siguientes Rutas"
            ''        col_Valor.Caption = "Ruta"
            ''        col_Id.FieldName = "id"
            ''        col_Valor.FieldName = "ruta"
            ''        If oDatos.WMS_Cedis_Rutas_Sel(Globales.oAmbientes.Id_Empresa, False, dtDatos, MsjError) Then
            ''            GridC_1.DataSource = dtDatos
            ''        Else
            ''            sw_error = True
            ''        End If
            ''    Case Globales.TipoCombo.Ubicaciones_Mesa
            ''        GridC_1.Visible = True
            ''        Me.Text = "Catalogo de Mesas de Empaque"
            ''        lbl_Etiqueta.Text = "Elije una de las siguientes Mesas de Empaque"
            ''        col_Valor.Caption = "Mesa"
            ''        col_Id.FieldName = "id"
            ''        col_Valor.FieldName = "valor"
            ''        If oDatos.WMS_Cbo_Ubicaciones_Sel(Globales.oAmbientes.Id_Empresa, IdFiltro2, "EMPAQUE", False, dtDatos, MsjError) Then
            ''            GridC_1.DataSource = dtDatos
            ''        Else
            ''            sw_error = True
            ''        End If
            ''    Case Globales.TipoCombo.Fecha
            ''        Calendario1.Visible = True
            ''        Calendario1.DateTime = Now()
            ''        'Me.Text = "Catalogo de Tipos de Contactos"
            ''        lbl_Etiqueta.Text = "Elije una fecha ..."
            ''        'col_Valor.Caption = "Tipo de Contacto"
            ''End Select
            If sw_error Then
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & MsjError, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub Aceptar_Cancelar(ByVal tag As String)
        Try
            Select Case tag
                Case "Ok"
                    If Tipo = Globales.TipoCombo.Fecha Then
                        IdSelecionado = Calendario1.DateTime.Year * 10000 _
                                      + Calendario1.DateTime.Month * 100 _
                                      + Calendario1.DateTime.Day * 1
                        ValorSelecionado = Format(Calendario1.DateTime, "dd/MMM/yyyy")
                    ElseIf Tipo = Globales.TipoCombo.Pedidos Then
                        IdSelecionado = GridV_2.GetFocusedRowCellValue(col2_Pedido)
                        ValorSelecionado = GridV_2.GetFocusedRowCellValue(col2_Pedido)
                        DlbSalida1 = GridV_2.GetFocusedRowCellValue(col2_Bultos)
                        DlbSalida2 = GridV_2.GetFocusedRowCellValue(col2_Cajas)
                        DlbSalida3 = GridV_2.GetFocusedRowCellValue(col2_Pzas)
                    ElseIf Tipo = Globales.TipoCombo.Asentamientos Then
                        IdSelecionado = GridV_3.GetFocusedRowCellValue(col3_IdAsentamiento)
                        ValorSelecionado = GridV_3.GetFocusedRowCellValue(col3_Asentamiento)
                    Else
                        IdSelecionado = GridV_1.GetFocusedRowCellValue(col_Id)
                        ValorSelecionado = GridV_1.GetFocusedRowCellValue(col_Valor)
                    End If
                Case "Cancelar"
                    IdSelecionado = 0
                    ValorSelecionado = ""
            End Select
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Call Aceptar_Cancelar(DirectCast(CType(e, DevExpress.XtraBars.Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption)
    End Sub
    Private Sub GridV_DoubleClick(sender As Object, e As EventArgs) Handles GridV_1.DoubleClick, GridV_2.DoubleClick
        Try
            Call Aceptar_Cancelar("Ok")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class