﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCuentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvCuentas))
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Cuenta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numtar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tienda = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Empresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Folio = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Corte = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Nombre = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Estatus = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Sexo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Edocivil = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Rfc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_EMail = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Actividad = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Enviar = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Dircasa1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cdcasa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cpcasa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Estado = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Telcasa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Dirofna1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Colcasa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cdofna = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cpofna = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Estadoof = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Telofna1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Telofna2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fax = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Ruta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Contacto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Agente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Observ = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Plazo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipodescto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Descuento = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Limiteorig = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Limite = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Saldo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Distrito = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Ultpago = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Curp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Localidad = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Seccion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Disponible = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Direccion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Entrecalles = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Colonia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Ciudad = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Municipio = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Edo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Pais = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GridV_2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col2_Empresa = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Sucursal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Empleado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Nombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Estatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Puesto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_FechaIngreso = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Descuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txt_Estatus = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.lbl_Estatus = New System.Windows.Forms.Label()
        Me.lbl_RFC = New System.Windows.Forms.Label()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.lbl_Nombre = New System.Windows.Forms.Label()
        Me.lbl_Empresa = New System.Windows.Forms.Label()
        Me.txt_Tarjeta = New System.Windows.Forms.TextBox()
        Me.txt_Cuenta = New System.Windows.Forms.TextBox()
        Me.lbl_Tarjeta = New System.Windows.Forms.Label()
        Me.lbl_Cuenta = New System.Windows.Forms.Label()
        Me.btn_Refrescar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(242, 26)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(481, 26)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 557)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(784, 74)
        Me.GroupControl3.TabIndex = 2
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(784, 389)
        Me.GroupControl2.TabIndex = 1
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(780, 364)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1, Me.GridV_2})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.gridBand2})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_Cuenta, Me.col1_Tarjeta, Me.col1_Numtar, Me.col1_Tienda, Me.col1_Folio, Me.col1_Fecha, Me.col1_Corte, Me.col1_Empresa, Me.col1_Nombre, Me.col1_Tipo, Me.col1_Estatus, Me.col1_Sexo, Me.col1_Edocivil, Me.col1_Rfc, Me.col1_Actividad, Me.col1_Enviar, Me.col1_Dircasa1, Me.col1_Cdcasa, Me.col1_Cpcasa, Me.col1_Estado, Me.col1_Telcasa, Me.col1_Dirofna1, Me.col1_Colcasa, Me.col1_Cdofna, Me.col1_Cpofna, Me.col1_Estadoof, Me.col1_Telofna1, Me.col1_Telofna2, Me.col1_Fax, Me.col1_Ruta, Me.col1_Contacto, Me.col1_Agente, Me.col1_Observ, Me.col1_Plazo, Me.col1_Tipodescto, Me.col1_Descuento, Me.col1_Limiteorig, Me.col1_Limite, Me.col1_Saldo, Me.col1_Ultpago, Me.col1_Curp, Me.col1_Distrito, Me.col1_Localidad, Me.col1_Seccion, Me.col1_Disponible, Me.col1_Direccion, Me.col1_Entrecalles, Me.col1_Colonia, Me.col1_Municipio, Me.col1_Ciudad, Me.col1_Edo, Me.col1_Pais, Me.col1_EMail})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GENERALES"
        Me.GridBand1.Columns.Add(Me.col1_Cuenta)
        Me.GridBand1.Columns.Add(Me.col1_Tarjeta)
        Me.GridBand1.Columns.Add(Me.col1_Numtar)
        Me.GridBand1.Columns.Add(Me.col1_Tienda)
        Me.GridBand1.Columns.Add(Me.col1_Empresa)
        Me.GridBand1.Columns.Add(Me.col1_Folio)
        Me.GridBand1.Columns.Add(Me.col1_Fecha)
        Me.GridBand1.Columns.Add(Me.col1_Corte)
        Me.GridBand1.Columns.Add(Me.col1_Nombre)
        Me.GridBand1.Columns.Add(Me.col1_Tipo)
        Me.GridBand1.Columns.Add(Me.col1_Estatus)
        Me.GridBand1.Columns.Add(Me.col1_Sexo)
        Me.GridBand1.Columns.Add(Me.col1_Edocivil)
        Me.GridBand1.Columns.Add(Me.col1_Rfc)
        Me.GridBand1.Columns.Add(Me.col1_EMail)
        Me.GridBand1.Columns.Add(Me.col1_Actividad)
        Me.GridBand1.Columns.Add(Me.col1_Enviar)
        Me.GridBand1.Columns.Add(Me.col1_Dircasa1)
        Me.GridBand1.Columns.Add(Me.col1_Cdcasa)
        Me.GridBand1.Columns.Add(Me.col1_Cpcasa)
        Me.GridBand1.Columns.Add(Me.col1_Estado)
        Me.GridBand1.Columns.Add(Me.col1_Telcasa)
        Me.GridBand1.Columns.Add(Me.col1_Dirofna1)
        Me.GridBand1.Columns.Add(Me.col1_Colcasa)
        Me.GridBand1.Columns.Add(Me.col1_Cdofna)
        Me.GridBand1.Columns.Add(Me.col1_Cpofna)
        Me.GridBand1.Columns.Add(Me.col1_Estadoof)
        Me.GridBand1.Columns.Add(Me.col1_Telofna1)
        Me.GridBand1.Columns.Add(Me.col1_Telofna2)
        Me.GridBand1.Columns.Add(Me.col1_Fax)
        Me.GridBand1.Columns.Add(Me.col1_Ruta)
        Me.GridBand1.Columns.Add(Me.col1_Contacto)
        Me.GridBand1.Columns.Add(Me.col1_Agente)
        Me.GridBand1.Columns.Add(Me.col1_Observ)
        Me.GridBand1.Columns.Add(Me.col1_Plazo)
        Me.GridBand1.Columns.Add(Me.col1_Tipodescto)
        Me.GridBand1.Columns.Add(Me.col1_Descuento)
        Me.GridBand1.Columns.Add(Me.col1_Limiteorig)
        Me.GridBand1.Columns.Add(Me.col1_Limite)
        Me.GridBand1.Columns.Add(Me.col1_Saldo)
        Me.GridBand1.Columns.Add(Me.col1_Distrito)
        Me.GridBand1.Columns.Add(Me.col1_Ultpago)
        Me.GridBand1.Columns.Add(Me.col1_Curp)
        Me.GridBand1.Columns.Add(Me.col1_Localidad)
        Me.GridBand1.Columns.Add(Me.col1_Seccion)
        Me.GridBand1.Columns.Add(Me.col1_Disponible)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 1125
        '
        'col1_Cuenta
        '
        Me.col1_Cuenta.Caption = "Cuenta"
        Me.col1_Cuenta.FieldName = "cuenta"
        Me.col1_Cuenta.Name = "col1_Cuenta"
        Me.col1_Cuenta.OptionsColumn.AllowEdit = False
        Me.col1_Cuenta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Cuenta.Visible = True
        '
        'col1_Tarjeta
        '
        Me.col1_Tarjeta.Caption = "Tarjeta"
        Me.col1_Tarjeta.FieldName = "tarjeta"
        Me.col1_Tarjeta.Name = "col1_Tarjeta"
        Me.col1_Tarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Tarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Numtar
        '
        Me.col1_Numtar.Caption = "Numtar"
        Me.col1_Numtar.FieldName = "numtar"
        Me.col1_Numtar.Name = "col1_Numtar"
        Me.col1_Numtar.OptionsColumn.AllowEdit = False
        Me.col1_Numtar.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Tienda
        '
        Me.col1_Tienda.Caption = "Tienda"
        Me.col1_Tienda.FieldName = "tienda"
        Me.col1_Tienda.Name = "col1_Tienda"
        Me.col1_Tienda.OptionsColumn.AllowEdit = False
        Me.col1_Tienda.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Empresa
        '
        Me.col1_Empresa.Caption = "Empresa"
        Me.col1_Empresa.FieldName = "empresa"
        Me.col1_Empresa.Name = "col1_Empresa"
        Me.col1_Empresa.OptionsColumn.AllowEdit = False
        Me.col1_Empresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Empresa.Visible = True
        '
        'col1_Folio
        '
        Me.col1_Folio.Caption = "Folio"
        Me.col1_Folio.FieldName = "folio"
        Me.col1_Folio.Name = "col1_Folio"
        Me.col1_Folio.OptionsColumn.AllowEdit = False
        Me.col1_Folio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Fecha
        '
        Me.col1_Fecha.Caption = "Fecha"
        Me.col1_Fecha.FieldName = "fecha"
        Me.col1_Fecha.Name = "col1_Fecha"
        Me.col1_Fecha.OptionsColumn.AllowEdit = False
        Me.col1_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Corte
        '
        Me.col1_Corte.Caption = "Corte"
        Me.col1_Corte.FieldName = "corte"
        Me.col1_Corte.Name = "col1_Corte"
        Me.col1_Corte.OptionsColumn.AllowEdit = False
        Me.col1_Corte.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Nombre
        '
        Me.col1_Nombre.Caption = "Nombre"
        Me.col1_Nombre.FieldName = "nombre"
        Me.col1_Nombre.Name = "col1_Nombre"
        Me.col1_Nombre.OptionsColumn.AllowEdit = False
        Me.col1_Nombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Nombre.Visible = True
        '
        'col1_Tipo
        '
        Me.col1_Tipo.Caption = "Tipo"
        Me.col1_Tipo.FieldName = "tipo"
        Me.col1_Tipo.Name = "col1_Tipo"
        Me.col1_Tipo.OptionsColumn.AllowEdit = False
        Me.col1_Tipo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipo.Visible = True
        '
        'col1_Estatus
        '
        Me.col1_Estatus.Caption = "Estatus"
        Me.col1_Estatus.FieldName = "estatus"
        Me.col1_Estatus.Name = "col1_Estatus"
        Me.col1_Estatus.OptionsColumn.AllowEdit = False
        Me.col1_Estatus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Estatus.Visible = True
        '
        'col1_Sexo
        '
        Me.col1_Sexo.Caption = "Sexo"
        Me.col1_Sexo.FieldName = "sexo"
        Me.col1_Sexo.Name = "col1_Sexo"
        Me.col1_Sexo.OptionsColumn.AllowEdit = False
        Me.col1_Sexo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Edocivil
        '
        Me.col1_Edocivil.Caption = "Edocivil"
        Me.col1_Edocivil.FieldName = "edocivil"
        Me.col1_Edocivil.Name = "col1_Edocivil"
        Me.col1_Edocivil.OptionsColumn.AllowEdit = False
        Me.col1_Edocivil.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Rfc
        '
        Me.col1_Rfc.Caption = "Rfc"
        Me.col1_Rfc.FieldName = "rfc"
        Me.col1_Rfc.Name = "col1_Rfc"
        Me.col1_Rfc.OptionsColumn.AllowEdit = False
        Me.col1_Rfc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Rfc.Visible = True
        '
        'col1_EMail
        '
        Me.col1_EMail.Caption = "e-Mail"
        Me.col1_EMail.FieldName = "email"
        Me.col1_EMail.Name = "col1_EMail"
        Me.col1_EMail.OptionsColumn.AllowEdit = False
        Me.col1_EMail.OptionsColumn.AllowMove = False
        Me.col1_EMail.Visible = True
        '
        'col1_Actividad
        '
        Me.col1_Actividad.Caption = "Actividad"
        Me.col1_Actividad.FieldName = "actividad"
        Me.col1_Actividad.Name = "col1_Actividad"
        Me.col1_Actividad.OptionsColumn.AllowEdit = False
        Me.col1_Actividad.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Actividad.Visible = True
        '
        'col1_Enviar
        '
        Me.col1_Enviar.Caption = "Enviar"
        Me.col1_Enviar.FieldName = "enviar"
        Me.col1_Enviar.Name = "col1_Enviar"
        Me.col1_Enviar.OptionsColumn.AllowEdit = False
        Me.col1_Enviar.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Dircasa1
        '
        Me.col1_Dircasa1.Caption = "Dircasa1"
        Me.col1_Dircasa1.FieldName = "dircasa1"
        Me.col1_Dircasa1.Name = "col1_Dircasa1"
        Me.col1_Dircasa1.OptionsColumn.AllowEdit = False
        Me.col1_Dircasa1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Cdcasa
        '
        Me.col1_Cdcasa.Caption = "Cdcasa"
        Me.col1_Cdcasa.FieldName = "cdcasa"
        Me.col1_Cdcasa.Name = "col1_Cdcasa"
        Me.col1_Cdcasa.OptionsColumn.AllowEdit = False
        Me.col1_Cdcasa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Cpcasa
        '
        Me.col1_Cpcasa.Caption = "Cpcasa"
        Me.col1_Cpcasa.FieldName = "cpcasa"
        Me.col1_Cpcasa.Name = "col1_Cpcasa"
        Me.col1_Cpcasa.OptionsColumn.AllowEdit = False
        Me.col1_Cpcasa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Estado
        '
        Me.col1_Estado.Caption = "Estado"
        Me.col1_Estado.FieldName = "estado"
        Me.col1_Estado.Name = "col1_Estado"
        Me.col1_Estado.OptionsColumn.AllowEdit = False
        Me.col1_Estado.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Telcasa
        '
        Me.col1_Telcasa.Caption = "Telcasa"
        Me.col1_Telcasa.FieldName = "telcasa"
        Me.col1_Telcasa.Name = "col1_Telcasa"
        Me.col1_Telcasa.OptionsColumn.AllowEdit = False
        Me.col1_Telcasa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Dirofna1
        '
        Me.col1_Dirofna1.Caption = "Dirofna1"
        Me.col1_Dirofna1.FieldName = "dirofna1"
        Me.col1_Dirofna1.Name = "col1_Dirofna1"
        Me.col1_Dirofna1.OptionsColumn.AllowEdit = False
        Me.col1_Dirofna1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Colcasa
        '
        Me.col1_Colcasa.Caption = "Colonia"
        Me.col1_Colcasa.FieldName = "colcasa"
        Me.col1_Colcasa.Name = "col1_Colcasa"
        Me.col1_Colcasa.OptionsColumn.AllowEdit = False
        Me.col1_Colcasa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Cdofna
        '
        Me.col1_Cdofna.Caption = "Cdofna"
        Me.col1_Cdofna.FieldName = "cdofna"
        Me.col1_Cdofna.Name = "col1_Cdofna"
        Me.col1_Cdofna.OptionsColumn.AllowEdit = False
        Me.col1_Cdofna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Cpofna
        '
        Me.col1_Cpofna.Caption = "Cpofna"
        Me.col1_Cpofna.FieldName = "cpofna"
        Me.col1_Cpofna.Name = "col1_Cpofna"
        Me.col1_Cpofna.OptionsColumn.AllowEdit = False
        Me.col1_Cpofna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Estadoof
        '
        Me.col1_Estadoof.Caption = "Estadoof"
        Me.col1_Estadoof.FieldName = "estadoof"
        Me.col1_Estadoof.Name = "col1_Estadoof"
        Me.col1_Estadoof.OptionsColumn.AllowEdit = False
        Me.col1_Estadoof.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Telofna1
        '
        Me.col1_Telofna1.Caption = "Telofna1"
        Me.col1_Telofna1.FieldName = "telofna1"
        Me.col1_Telofna1.Name = "col1_Telofna1"
        Me.col1_Telofna1.OptionsColumn.AllowEdit = False
        Me.col1_Telofna1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Telofna2
        '
        Me.col1_Telofna2.Caption = "Telofna2"
        Me.col1_Telofna2.FieldName = "telofna2"
        Me.col1_Telofna2.Name = "col1_Telofna2"
        Me.col1_Telofna2.OptionsColumn.AllowEdit = False
        Me.col1_Telofna2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Fax
        '
        Me.col1_Fax.Caption = "Fax"
        Me.col1_Fax.FieldName = "fax"
        Me.col1_Fax.Name = "col1_Fax"
        Me.col1_Fax.OptionsColumn.AllowEdit = False
        Me.col1_Fax.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Ruta
        '
        Me.col1_Ruta.Caption = "Ruta"
        Me.col1_Ruta.FieldName = "ruta"
        Me.col1_Ruta.Name = "col1_Ruta"
        Me.col1_Ruta.OptionsColumn.AllowEdit = False
        Me.col1_Ruta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Ruta.Visible = True
        '
        'col1_Contacto
        '
        Me.col1_Contacto.Caption = "Contacto"
        Me.col1_Contacto.FieldName = "contacto"
        Me.col1_Contacto.Name = "col1_Contacto"
        Me.col1_Contacto.OptionsColumn.AllowEdit = False
        Me.col1_Contacto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Contacto.Visible = True
        '
        'col1_Agente
        '
        Me.col1_Agente.Caption = "Agente"
        Me.col1_Agente.FieldName = "agente"
        Me.col1_Agente.Name = "col1_Agente"
        Me.col1_Agente.OptionsColumn.AllowEdit = False
        Me.col1_Agente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Agente.Visible = True
        '
        'col1_Observ
        '
        Me.col1_Observ.Caption = "Observ"
        Me.col1_Observ.FieldName = "observ"
        Me.col1_Observ.Name = "col1_Observ"
        Me.col1_Observ.OptionsColumn.AllowEdit = False
        Me.col1_Observ.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Plazo
        '
        Me.col1_Plazo.Caption = "Plazo"
        Me.col1_Plazo.FieldName = "plazo"
        Me.col1_Plazo.Name = "col1_Plazo"
        Me.col1_Plazo.OptionsColumn.AllowEdit = False
        Me.col1_Plazo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Plazo.Visible = True
        '
        'col1_Tipodescto
        '
        Me.col1_Tipodescto.Caption = "Tipodescto"
        Me.col1_Tipodescto.FieldName = "tipodescto"
        Me.col1_Tipodescto.Name = "col1_Tipodescto"
        Me.col1_Tipodescto.OptionsColumn.AllowEdit = False
        Me.col1_Tipodescto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipodescto.Visible = True
        '
        'col1_Descuento
        '
        Me.col1_Descuento.Caption = "Descuento"
        Me.col1_Descuento.DisplayFormat.FormatString = "N2"
        Me.col1_Descuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Descuento.FieldName = "descuento"
        Me.col1_Descuento.Name = "col1_Descuento"
        Me.col1_Descuento.Visible = True
        '
        'col1_Limiteorig
        '
        Me.col1_Limiteorig.Caption = "Limiteorig"
        Me.col1_Limiteorig.FieldName = "limiteorig"
        Me.col1_Limiteorig.Name = "col1_Limiteorig"
        Me.col1_Limiteorig.OptionsColumn.AllowEdit = False
        Me.col1_Limiteorig.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Limite
        '
        Me.col1_Limite.Caption = "Limite"
        Me.col1_Limite.FieldName = "limite"
        Me.col1_Limite.Name = "col1_Limite"
        Me.col1_Limite.OptionsColumn.AllowEdit = False
        Me.col1_Limite.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Saldo
        '
        Me.col1_Saldo.Caption = "Saldo"
        Me.col1_Saldo.FieldName = "saldo"
        Me.col1_Saldo.Name = "col1_Saldo"
        Me.col1_Saldo.OptionsColumn.AllowEdit = False
        Me.col1_Saldo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Distrito
        '
        Me.col1_Distrito.Caption = "Distrito"
        Me.col1_Distrito.FieldName = "distrito"
        Me.col1_Distrito.Name = "col1_Distrito"
        Me.col1_Distrito.OptionsColumn.AllowEdit = False
        Me.col1_Distrito.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Ultpago
        '
        Me.col1_Ultpago.Caption = "Ultpago"
        Me.col1_Ultpago.FieldName = "ultpago"
        Me.col1_Ultpago.Name = "col1_Ultpago"
        Me.col1_Ultpago.OptionsColumn.AllowEdit = False
        Me.col1_Ultpago.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Curp
        '
        Me.col1_Curp.Caption = "Curp"
        Me.col1_Curp.FieldName = "curp"
        Me.col1_Curp.Name = "col1_Curp"
        Me.col1_Curp.OptionsColumn.AllowEdit = False
        Me.col1_Curp.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Curp.Visible = True
        '
        'col1_Localidad
        '
        Me.col1_Localidad.Caption = "Localidad"
        Me.col1_Localidad.FieldName = "localidad"
        Me.col1_Localidad.Name = "col1_Localidad"
        Me.col1_Localidad.OptionsColumn.AllowEdit = False
        Me.col1_Localidad.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Seccion
        '
        Me.col1_Seccion.Caption = "Seccion"
        Me.col1_Seccion.FieldName = "seccion"
        Me.col1_Seccion.Name = "col1_Seccion"
        Me.col1_Seccion.OptionsColumn.AllowEdit = False
        Me.col1_Seccion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Disponible
        '
        Me.col1_Disponible.Caption = "Disponible"
        Me.col1_Disponible.FieldName = "disponible"
        Me.col1_Disponible.Name = "col1_Disponible"
        Me.col1_Disponible.OptionsColumn.AllowEdit = False
        Me.col1_Disponible.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'gridBand2
        '
        Me.gridBand2.Caption = "ENVIAR"
        Me.gridBand2.Columns.Add(Me.col1_Direccion)
        Me.gridBand2.Columns.Add(Me.col1_Entrecalles)
        Me.gridBand2.Columns.Add(Me.col1_Colonia)
        Me.gridBand2.Columns.Add(Me.col1_Ciudad)
        Me.gridBand2.Columns.Add(Me.col1_Municipio)
        Me.gridBand2.Columns.Add(Me.col1_Edo)
        Me.gridBand2.Columns.Add(Me.col1_Pais)
        Me.gridBand2.Name = "gridBand2"
        Me.gridBand2.VisibleIndex = 1
        Me.gridBand2.Width = 375
        '
        'col1_Direccion
        '
        Me.col1_Direccion.Caption = "Direccion"
        Me.col1_Direccion.FieldName = "dircasa1"
        Me.col1_Direccion.Name = "col1_Direccion"
        Me.col1_Direccion.OptionsColumn.AllowEdit = False
        Me.col1_Direccion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Direccion.Visible = True
        '
        'col1_Entrecalles
        '
        Me.col1_Entrecalles.Caption = "Entrecalles"
        Me.col1_Entrecalles.FieldName = "entrecalles"
        Me.col1_Entrecalles.Name = "col1_Entrecalles"
        Me.col1_Entrecalles.OptionsColumn.AllowEdit = False
        Me.col1_Entrecalles.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Colonia
        '
        Me.col1_Colonia.Caption = "Colonia"
        Me.col1_Colonia.FieldName = "colcasa"
        Me.col1_Colonia.Name = "col1_Colonia"
        Me.col1_Colonia.OptionsColumn.AllowEdit = False
        Me.col1_Colonia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Colonia.Visible = True
        '
        'col1_Ciudad
        '
        Me.col1_Ciudad.Caption = "Ciudad"
        Me.col1_Ciudad.FieldName = "ciudad"
        Me.col1_Ciudad.Name = "col1_Ciudad"
        Me.col1_Ciudad.OptionsColumn.AllowEdit = False
        Me.col1_Ciudad.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Ciudad.Visible = True
        '
        'col1_Municipio
        '
        Me.col1_Municipio.Caption = "Municipio"
        Me.col1_Municipio.FieldName = "municipio"
        Me.col1_Municipio.Name = "col1_Municipio"
        Me.col1_Municipio.OptionsColumn.AllowEdit = False
        Me.col1_Municipio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Municipio.Visible = True
        '
        'col1_Edo
        '
        Me.col1_Edo.Caption = "Edo"
        Me.col1_Edo.FieldName = "estado"
        Me.col1_Edo.Name = "col1_Edo"
        Me.col1_Edo.OptionsColumn.AllowEdit = False
        Me.col1_Edo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Edo.Visible = True
        '
        'col1_Pais
        '
        Me.col1_Pais.Caption = "Pais"
        Me.col1_Pais.FieldName = "pais"
        Me.col1_Pais.Name = "col1_Pais"
        Me.col1_Pais.OptionsColumn.AllowEdit = False
        Me.col1_Pais.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridV_2
        '
        Me.GridV_2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col2_Empresa, Me.col2_Sucursal, Me.col2_Empleado, Me.col2_Nombre, Me.col2_Estatus, Me.col2_Puesto, Me.col2_FechaIngreso, Me.col2_Descuento})
        Me.GridV_2.GridControl = Me.GridC_1
        Me.GridV_2.Name = "GridV_2"
        '
        'col2_Empresa
        '
        Me.col2_Empresa.Caption = "Empresa"
        Me.col2_Empresa.FieldName = "id_empresa"
        Me.col2_Empresa.Name = "col2_Empresa"
        Me.col2_Empresa.OptionsColumn.AllowEdit = False
        Me.col2_Empresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Empresa.Visible = True
        Me.col2_Empresa.VisibleIndex = 0
        '
        'col2_Sucursal
        '
        Me.col2_Sucursal.Caption = "Sucursal"
        Me.col2_Sucursal.FieldName = "id_sucursal"
        Me.col2_Sucursal.Name = "col2_Sucursal"
        Me.col2_Sucursal.OptionsColumn.AllowEdit = False
        Me.col2_Sucursal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Sucursal.Visible = True
        Me.col2_Sucursal.VisibleIndex = 1
        '
        'col2_Empleado
        '
        Me.col2_Empleado.Caption = "Empleado"
        Me.col2_Empleado.FieldName = "id_empleado"
        Me.col2_Empleado.Name = "col2_Empleado"
        Me.col2_Empleado.OptionsColumn.AllowEdit = False
        Me.col2_Empleado.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Empleado.Visible = True
        Me.col2_Empleado.VisibleIndex = 2
        '
        'col2_Nombre
        '
        Me.col2_Nombre.Caption = "Nombre"
        Me.col2_Nombre.FieldName = "nombre"
        Me.col2_Nombre.Name = "col2_Nombre"
        Me.col2_Nombre.OptionsColumn.AllowEdit = False
        Me.col2_Nombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Nombre.Visible = True
        Me.col2_Nombre.VisibleIndex = 3
        '
        'col2_Estatus
        '
        Me.col2_Estatus.Caption = "Estatus"
        Me.col2_Estatus.FieldName = "estatus"
        Me.col2_Estatus.Name = "col2_Estatus"
        Me.col2_Estatus.OptionsColumn.AllowEdit = False
        Me.col2_Estatus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Estatus.Visible = True
        Me.col2_Estatus.VisibleIndex = 4
        '
        'col2_Puesto
        '
        Me.col2_Puesto.Caption = "Puesto"
        Me.col2_Puesto.FieldName = "puesto"
        Me.col2_Puesto.Name = "col2_Puesto"
        Me.col2_Puesto.OptionsColumn.AllowEdit = False
        Me.col2_Puesto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Puesto.Visible = True
        Me.col2_Puesto.VisibleIndex = 5
        '
        'col2_FechaIngreso
        '
        Me.col2_FechaIngreso.Caption = "Fecha Ingreso"
        Me.col2_FechaIngreso.FieldName = "fecha_ingreso"
        Me.col2_FechaIngreso.Name = "col2_FechaIngreso"
        Me.col2_FechaIngreso.OptionsColumn.AllowEdit = False
        Me.col2_FechaIngreso.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_FechaIngreso.Visible = True
        Me.col2_FechaIngreso.VisibleIndex = 6
        '
        'col2_Descuento
        '
        Me.col2_Descuento.Caption = "%Descto"
        Me.col2_Descuento.FieldName = "descuento"
        Me.col2_Descuento.Name = "col2_Descuento"
        Me.col2_Descuento.OptionsColumn.AllowEdit = False
        Me.col2_Descuento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Descuento.Visible = True
        Me.col2_Descuento.VisibleIndex = 7
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.txt_Estatus)
        Me.GroupControl1.Controls.Add(Me.txt_RFC)
        Me.GroupControl1.Controls.Add(Me.lbl_Estatus)
        Me.GroupControl1.Controls.Add(Me.lbl_RFC)
        Me.GroupControl1.Controls.Add(Me.txt_Nombre)
        Me.GroupControl1.Controls.Add(Me.txt_Empresa)
        Me.GroupControl1.Controls.Add(Me.lbl_Nombre)
        Me.GroupControl1.Controls.Add(Me.lbl_Empresa)
        Me.GroupControl1.Controls.Add(Me.txt_Tarjeta)
        Me.GroupControl1.Controls.Add(Me.txt_Cuenta)
        Me.GroupControl1.Controls.Add(Me.lbl_Tarjeta)
        Me.GroupControl1.Controls.Add(Me.lbl_Cuenta)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 389)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(784, 168)
        Me.GroupControl1.TabIndex = 0
        '
        'txt_Estatus
        '
        Me.txt_Estatus.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Estatus.Location = New System.Drawing.Point(398, 63)
        Me.txt_Estatus.Name = "txt_Estatus"
        Me.txt_Estatus.ReadOnly = True
        Me.txt_Estatus.Size = New System.Drawing.Size(200, 30)
        Me.txt_Estatus.TabIndex = 14
        Me.txt_Estatus.Text = "NORM"
        Me.txt_Estatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_RFC
        '
        Me.txt_RFC.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(398, 29)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(200, 30)
        Me.txt_RFC.TabIndex = 2
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_Estatus
        '
        Me.lbl_Estatus.AutoSize = True
        Me.lbl_Estatus.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Estatus.Location = New System.Drawing.Point(306, 66)
        Me.lbl_Estatus.Name = "lbl_Estatus"
        Me.lbl_Estatus.Size = New System.Drawing.Size(70, 23)
        Me.lbl_Estatus.TabIndex = 12
        Me.lbl_Estatus.Text = "Estatus"
        '
        'lbl_RFC
        '
        Me.lbl_RFC.AutoSize = True
        Me.lbl_RFC.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_RFC.Location = New System.Drawing.Point(306, 32)
        Me.lbl_RFC.Name = "lbl_RFC"
        Me.lbl_RFC.Size = New System.Drawing.Size(43, 23)
        Me.lbl_RFC.TabIndex = 11
        Me.lbl_RFC.Text = "RFC"
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(97, 130)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(675, 30)
        Me.txt_Nombre.TabIndex = 5
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa.Location = New System.Drawing.Point(97, 96)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(675, 30)
        Me.txt_Empresa.TabIndex = 4
        '
        'lbl_Nombre
        '
        Me.lbl_Nombre.AutoSize = True
        Me.lbl_Nombre.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Nombre.Location = New System.Drawing.Point(5, 133)
        Me.lbl_Nombre.Name = "lbl_Nombre"
        Me.lbl_Nombre.Size = New System.Drawing.Size(77, 23)
        Me.lbl_Nombre.TabIndex = 8
        Me.lbl_Nombre.Text = "Nombre"
        '
        'lbl_Empresa
        '
        Me.lbl_Empresa.AutoSize = True
        Me.lbl_Empresa.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Empresa.Location = New System.Drawing.Point(5, 99)
        Me.lbl_Empresa.Name = "lbl_Empresa"
        Me.lbl_Empresa.Size = New System.Drawing.Size(83, 23)
        Me.lbl_Empresa.TabIndex = 7
        Me.lbl_Empresa.Text = "Empresa"
        '
        'txt_Tarjeta
        '
        Me.txt_Tarjeta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Tarjeta.Location = New System.Drawing.Point(97, 60)
        Me.txt_Tarjeta.Name = "txt_Tarjeta"
        Me.txt_Tarjeta.Size = New System.Drawing.Size(200, 30)
        Me.txt_Tarjeta.TabIndex = 3
        Me.txt_Tarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Cuenta
        '
        Me.txt_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cuenta.Location = New System.Drawing.Point(97, 26)
        Me.txt_Cuenta.Name = "txt_Cuenta"
        Me.txt_Cuenta.Size = New System.Drawing.Size(200, 30)
        Me.txt_Cuenta.TabIndex = 1
        Me.txt_Cuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_Tarjeta
        '
        Me.lbl_Tarjeta.AutoSize = True
        Me.lbl_Tarjeta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Tarjeta.Location = New System.Drawing.Point(5, 63)
        Me.lbl_Tarjeta.Name = "lbl_Tarjeta"
        Me.lbl_Tarjeta.Size = New System.Drawing.Size(69, 23)
        Me.lbl_Tarjeta.TabIndex = 3
        Me.lbl_Tarjeta.Text = "Tarjeta"
        '
        'lbl_Cuenta
        '
        Me.lbl_Cuenta.AutoSize = True
        Me.lbl_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Cuenta.Location = New System.Drawing.Point(5, 29)
        Me.lbl_Cuenta.Name = "lbl_Cuenta"
        Me.lbl_Cuenta.Size = New System.Drawing.Size(69, 23)
        Me.lbl_Cuenta.TabIndex = 1
        Me.lbl_Cuenta.Text = "Cuenta"
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.ImageOptions.Image = CType(resources.GetObject("btn_Refrescar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_Refrescar.Location = New System.Drawing.Point(730, 26)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Refrescar.TabIndex = 15
        '
        'pdvCuentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 631)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvCuentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CUENTAS"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lbl_Tarjeta As Label
    Friend WithEvents lbl_Cuenta As Label
    Friend WithEvents txt_Estatus As TextBox
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents lbl_Estatus As Label
    Friend WithEvents lbl_RFC As Label
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents txt_Empresa As TextBox
    Friend WithEvents lbl_Nombre As Label
    Friend WithEvents lbl_Empresa As Label
    Friend WithEvents txt_Tarjeta As TextBox
    Friend WithEvents txt_Cuenta As TextBox
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents col1_Cuenta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numtar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tienda As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Empresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Folio As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Corte As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Nombre As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Estatus As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Sexo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Edocivil As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Rfc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Actividad As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Enviar As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Dircasa1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cdcasa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cpcasa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Estado As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Telcasa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Dirofna1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Colcasa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cdofna As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cpofna As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Estadoof As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Telofna1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Telofna2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fax As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Ruta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Contacto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Agente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Observ As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Plazo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipodescto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Descuento As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Limiteorig As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Limite As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Saldo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Distrito As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Ultpago As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Curp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Localidad As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Seccion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Disponible As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Direccion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Entrecalles As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Colonia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Ciudad As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Municipio As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Edo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Pais As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents col1_EMail As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents gridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents GridV_2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col2_Empresa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Sucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Empleado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Estatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Puesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_FechaIngreso As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Descuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Nombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btn_Refrescar As DevExpress.XtraEditors.SimpleButton
End Class
