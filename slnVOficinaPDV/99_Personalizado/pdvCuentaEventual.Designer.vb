﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCuentaEventual
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_Estado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_Municipio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_Colonia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_CP = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Calle = New System.Windows.Forms.TextBox()
        Me.txt_Cliente = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_NumExt = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_NumInt = New System.Windows.Forms.TextBox()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.cbo_UsoCFDI = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cbo_Regimen = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btn_Cliente = New DevExpress.XtraEditors.SimpleButton()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_EMail = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_Telefono2 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_Telefono = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.txt_Pais = New System.Windows.Forms.TextBox()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(466, 458)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(602, 458)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(5, 336)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(422, 24)
        Me.Label9.TabIndex = 60
        Me.Label9.Text = "Estado"
        '
        'txt_Estado
        '
        Me.txt_Estado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Estado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Estado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Estado.Location = New System.Drawing.Point(5, 367)
        Me.txt_Estado.Name = "txt_Estado"
        Me.txt_Estado.Size = New System.Drawing.Size(473, 29)
        Me.txt_Estado.TabIndex = 6
        Me.txt_Estado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(5, 269)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(422, 24)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Municipio"
        '
        'txt_Municipio
        '
        Me.txt_Municipio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Municipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Municipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Municipio.Location = New System.Drawing.Point(5, 300)
        Me.txt_Municipio.Name = "txt_Municipio"
        Me.txt_Municipio.Size = New System.Drawing.Size(473, 29)
        Me.txt_Municipio.TabIndex = 5
        Me.txt_Municipio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(5, 206)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(422, 24)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Colonia"
        '
        'txt_Colonia
        '
        Me.txt_Colonia.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Colonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Colonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Colonia.Location = New System.Drawing.Point(5, 237)
        Me.txt_Colonia.Name = "txt_Colonia"
        Me.txt_Colonia.Size = New System.Drawing.Size(473, 29)
        Me.txt_Colonia.TabIndex = 4
        Me.txt_Colonia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(422, 24)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "C. P."
        '
        'txt_CP
        '
        Me.txt_CP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_CP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CP.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CP.Location = New System.Drawing.Point(5, 179)
        Me.txt_CP.Name = "txt_CP"
        Me.txt_CP.Size = New System.Drawing.Size(473, 29)
        Me.txt_CP.TabIndex = 3
        Me.txt_CP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(422, 24)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Calle"
        '
        'txt_Calle
        '
        Me.txt_Calle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Calle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Calle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Calle.Location = New System.Drawing.Point(5, 54)
        Me.txt_Calle.Name = "txt_Calle"
        Me.txt_Calle.Size = New System.Drawing.Size(473, 29)
        Me.txt_Calle.TabIndex = 0
        Me.txt_Calle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Cliente
        '
        Me.txt_Cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cliente.Location = New System.Drawing.Point(6, 50)
        Me.txt_Cliente.Name = "txt_Cliente"
        Me.txt_Cliente.ReadOnly = True
        Me.txt_Cliente.Size = New System.Drawing.Size(281, 29)
        Me.txt_Cliente.TabIndex = 0
        Me.txt_Cliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(204, 24)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Num Ext"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_NumExt
        '
        Me.txt_NumExt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_NumExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumExt.Location = New System.Drawing.Point(5, 121)
        Me.txt_NumExt.Name = "txt_NumExt"
        Me.txt_NumExt.Size = New System.Drawing.Size(241, 29)
        Me.txt_NumExt.TabIndex = 1
        Me.txt_NumExt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(223, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(204, 24)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Num Int"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_NumInt
        '
        Me.txt_NumInt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_NumInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumInt.Location = New System.Drawing.Point(252, 121)
        Me.txt_NumInt.Name = "txt_NumInt"
        Me.txt_NumInt.Size = New System.Drawing.Size(226, 29)
        Me.txt_NumInt.TabIndex = 2
        Me.txt_NumInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.cbo_UsoCFDI)
        Me.GroupControl1.Controls.Add(Me.Label14)
        Me.GroupControl1.Controls.Add(Me.cbo_Regimen)
        Me.GroupControl1.Controls.Add(Me.Label17)
        Me.GroupControl1.Controls.Add(Me.btn_Cliente)
        Me.GroupControl1.Controls.Add(Me.Label13)
        Me.GroupControl1.Controls.Add(Me.txt_EMail)
        Me.GroupControl1.Controls.Add(Me.txt_Cliente)
        Me.GroupControl1.Controls.Add(Me.Label12)
        Me.GroupControl1.Controls.Add(Me.txt_Telefono2)
        Me.GroupControl1.Controls.Add(Me.Label11)
        Me.GroupControl1.Controls.Add(Me.txt_Telefono)
        Me.GroupControl1.Controls.Add(Me.Label10)
        Me.GroupControl1.Controls.Add(Me.txt_RFC)
        Me.GroupControl1.Controls.Add(Me.Label5)
        Me.GroupControl1.Controls.Add(Me.txt_Nombre)
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Location = New System.Drawing.Point(7, 2)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(362, 520)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Datos"
        '
        'cbo_UsoCFDI
        '
        Me.cbo_UsoCFDI.DisplayMember = "descripcion"
        Me.cbo_UsoCFDI.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_UsoCFDI.FormattingEnabled = True
        Me.cbo_UsoCFDI.Location = New System.Drawing.Point(6, 296)
        Me.cbo_UsoCFDI.Name = "cbo_UsoCFDI"
        Me.cbo_UsoCFDI.Size = New System.Drawing.Size(351, 31)
        Me.cbo_UsoCFDI.TabIndex = 5
        Me.cbo_UsoCFDI.ValueMember = "clave"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(126, 269)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(121, 24)
        Me.Label14.TabIndex = 65
        Me.Label14.Text = "Uso del CFDI"
        '
        'cbo_Regimen
        '
        Me.cbo_Regimen.DisplayMember = "descripcion"
        Me.cbo_Regimen.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Regimen.FormattingEnabled = True
        Me.cbo_Regimen.Location = New System.Drawing.Point(6, 235)
        Me.cbo_Regimen.Name = "cbo_Regimen"
        Me.cbo_Regimen.Size = New System.Drawing.Size(351, 31)
        Me.cbo_Regimen.TabIndex = 4
        Me.cbo_Regimen.ValueMember = "clave"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(98, 209)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(141, 24)
        Me.Label17.TabIndex = 63
        Me.Label17.Text = "Regimen Fiscal"
        '
        'btn_Cliente
        '
        Me.btn_Cliente.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.Location = New System.Drawing.Point(293, 50)
        Me.btn_Cliente.Name = "btn_Cliente"
        Me.btn_Cliente.Size = New System.Drawing.Size(64, 30)
        Me.btn_Cliente.TabIndex = 1
        Me.btn_Cliente.Text = "Cliente"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(137, 456)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(61, 24)
        Me.Label13.TabIndex = 36
        Me.Label13.Text = "e-mail"
        '
        'txt_EMail
        '
        Me.txt_EMail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_EMail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_EMail.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_EMail.Location = New System.Drawing.Point(5, 484)
        Me.txt_EMail.Name = "txt_EMail"
        Me.txt_EMail.Size = New System.Drawing.Size(352, 29)
        Me.txt_EMail.TabIndex = 8
        Me.txt_EMail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(138, 394)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 24)
        Me.Label12.TabIndex = 34
        Me.Label12.Text = "Celular"
        '
        'txt_Telefono2
        '
        Me.txt_Telefono2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Telefono2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Telefono2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Telefono2.Location = New System.Drawing.Point(6, 422)
        Me.txt_Telefono2.MaxLength = 10
        Me.txt_Telefono2.Name = "txt_Telefono2"
        Me.txt_Telefono2.Size = New System.Drawing.Size(352, 29)
        Me.txt_Telefono2.TabIndex = 7
        Me.txt_Telefono2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(137, 334)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(85, 24)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Teléfono"
        '
        'txt_Telefono
        '
        Me.txt_Telefono.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Telefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Telefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Telefono.Location = New System.Drawing.Point(5, 362)
        Me.txt_Telefono.MaxLength = 10
        Me.txt_Telefono.Name = "txt_Telefono"
        Me.txt_Telefono.Size = New System.Drawing.Size(352, 29)
        Me.txt_Telefono.TabIndex = 6
        Me.txt_Telefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(137, 149)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 24)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "RFC"
        '
        'txt_RFC
        '
        Me.txt_RFC.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_RFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(5, 177)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(352, 29)
        Me.txt_RFC.TabIndex = 3
        Me.txt_RFC.Text = "XXXX-000000-XXX"
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(126, 90)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 24)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Nombre"
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Nombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Nombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(6, 117)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(351, 29)
        Me.txt_Nombre.TabIndex = 2
        Me.txt_Nombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(126, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 24)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Cliente"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.Label4)
        Me.GroupControl2.Controls.Add(Me.txt_Calle)
        Me.GroupControl2.Controls.Add(Me.Label2)
        Me.GroupControl2.Controls.Add(Me.txt_CP)
        Me.GroupControl2.Controls.Add(Me.txt_NumInt)
        Me.GroupControl2.Controls.Add(Me.Label6)
        Me.GroupControl2.Controls.Add(Me.Label1)
        Me.GroupControl2.Controls.Add(Me.txt_Colonia)
        Me.GroupControl2.Controls.Add(Me.txt_NumExt)
        Me.GroupControl2.Controls.Add(Me.Label7)
        Me.GroupControl2.Controls.Add(Me.txt_Municipio)
        Me.GroupControl2.Controls.Add(Me.Label9)
        Me.GroupControl2.Controls.Add(Me.Label8)
        Me.GroupControl2.Controls.Add(Me.txt_Estado)
        Me.GroupControl2.Location = New System.Drawing.Point(375, 2)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(483, 407)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Dirección"
        '
        'txt_Pais
        '
        Me.txt_Pais.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Pais.Location = New System.Drawing.Point(758, 471)
        Me.txt_Pais.Name = "txt_Pais"
        Me.txt_Pais.Size = New System.Drawing.Size(100, 29)
        Me.txt_Pais.TabIndex = 68
        Me.txt_Pais.Text = "MEXICO"
        Me.txt_Pais.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Pais.Visible = False
        '
        'pdvCuentaEventual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 529)
        Me.ControlBox = False
        Me.Controls.Add(Me.txt_Pais)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btn_Aceptar)
        Me.Controls.Add(Me.btn_Cancelar)
        Me.KeyPreview = True
        Me.Name = "pdvCuentaEventual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nuevo Cliente Eventual"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents btn_Aceptar As Button
    Private WithEvents btn_Cancelar As Button
    Private WithEvents Label9 As Label
    Private WithEvents Label8 As Label
    Private WithEvents Label7 As Label
    Private WithEvents Label6 As Label
    Private WithEvents Label4 As Label
    Private WithEvents Label1 As Label
    Private WithEvents Label2 As Label
    Friend WithEvents txt_Cliente As TextBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label10 As Label
    Private WithEvents txt_Pais As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txt_Estado As TextBox
    Friend WithEvents txt_Municipio As TextBox
    Friend WithEvents txt_Colonia As TextBox
    Friend WithEvents txt_CP As TextBox
    Friend WithEvents txt_Calle As TextBox
    Friend WithEvents txt_NumExt As TextBox
    Friend WithEvents txt_NumInt As TextBox
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents txt_EMail As TextBox
    Friend WithEvents txt_Telefono2 As TextBox
    Friend WithEvents txt_Telefono As TextBox
    Friend WithEvents btn_Cliente As DevExpress.XtraEditors.SimpleButton
    Private WithEvents cbo_Regimen As ComboBox
    Private WithEvents Label17 As Label
    Private WithEvents cbo_UsoCFDI As ComboBox
    Private WithEvents Label14 As Label
End Class
