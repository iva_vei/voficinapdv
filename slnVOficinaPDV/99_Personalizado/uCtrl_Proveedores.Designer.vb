﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uCtrl_Proveedores
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_IdProveedor = New DevExpress.XtraEditors.LabelControl()
        Me.btn_Proveedor = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'lbl_IdProveedor
        '
        Me.lbl_IdProveedor.Appearance.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_IdProveedor.Appearance.Options.UseFont = True
        Me.lbl_IdProveedor.Appearance.Options.UseTextOptions = True
        Me.lbl_IdProveedor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lbl_IdProveedor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl_IdProveedor.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_IdProveedor.Location = New System.Drawing.Point(48, 0)
        Me.lbl_IdProveedor.Name = "lbl_IdProveedor"
        Me.lbl_IdProveedor.Size = New System.Drawing.Size(232, 48)
        Me.lbl_IdProveedor.TabIndex = 225
        Me.lbl_IdProveedor.Text = "Selecciona un Proveedor ..."
        '
        'btn_Proveedor
        '
        Me.btn_Proveedor.Location = New System.Drawing.Point(0, 0)
        Me.btn_Proveedor.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btn_Proveedor.MaximumSize = New System.Drawing.Size(48, 48)
        Me.btn_Proveedor.Text = "Prov"
        Me.btn_Proveedor.Name = "btn_Proveedor"
        Me.btn_Proveedor.Size = New System.Drawing.Size(48, 48)
        Me.btn_Proveedor.TabIndex = 226
        '
        'uCtrl_Proveedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lbl_IdProveedor)
        Me.Controls.Add(Me.btn_Proveedor)
        Me.Name = "uCtrl_Proveedores"
        Me.Size = New System.Drawing.Size(280, 48)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Proveedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbl_IdProveedor As DevExpress.XtraEditors.LabelControl
End Class
