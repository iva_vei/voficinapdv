﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvDireccionEntrega
    Dim _Accion As Boolean
    Public IdDir As Integer
    Private Sub pdvFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _Accion = False
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try

            oDatos = New Datos_Viscoi
            If oDatos.Inserta_CuentasLugarEntrega(Globales.oAmbientes.Id_Empresa, txt_Cliente.Text _
                                                  , 0 _
                                                  , txt_Calle.Text _
                                                  , txt_NumExt.Text _
                                                  , txt_NumInt.Text _
                                                  , txt_Colonia.Text _
                                                  , txt_CP.Text _
                                                  , txt_Municipio.Text _
                                                  , txt_Estado.Text _
                                                  , IdDir _
                                                  , Msj) Then
                _Accion = True
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
End Class