﻿Public Class uctrlCollapsePanel
    Private _AlturaFiltros As Integer
    Private _Sw_PanelOpen As Boolean

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _AlturaFiltros = 100
        _Sw_PanelOpen = True
    End Sub

    Public Property AlturaFiltros As Integer
        Get
            Return _AlturaFiltros
        End Get
        Set(value As Integer)
            _AlturaFiltros = value
        End Set
    End Property
    Public Property Sw_PanelOpen As Boolean
        Get
            Return _Sw_PanelOpen
        End Get
        Set(value As Boolean)
            _Sw_PanelOpen = value
            Call pnl_Titulo_Click(Nothing, Nothing)
        End Set
    End Property

    Private Sub pnl_Titulo_Click(sender As Object, e As EventArgs) Handles pnl_Titulo.Click
        Dim cColor As Color = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Try
            pnl_Titulo.BackColor = Color.Silver
            pnl_Titulo.BackColor = cColor
            If pnl_Titulo.Tag = "OPENED" Then
                pnl_Titulo.Tag = "CLOSED"
                pnl_Filtros.Visible = False
                pic_Panel_Open.Image = My.Resources.flecha_up
                _Sw_PanelOpen = False
                Me.Height = pnl_Titulo.Height
            Else
                pnl_Titulo.Tag = "OPENED"
                pnl_Filtros.Visible = True
                pic_Panel_Open.Image = My.Resources.flecha_down
                _Sw_PanelOpen = True
                Me.Height = pnl_Titulo.Height + _AlturaFiltros
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub pnl_Titulo_MouseHover(sender As Object, e As EventArgs) Handles pnl_Titulo.MouseHover
        Try
            pnl_Titulo.BackColor = Color.Black
        Catch ex As Exception

        End Try
    End Sub
    Private Sub pnl_Titulo_MouseLeave(sender As Object, e As EventArgs) Handles pnl_Titulo.MouseLeave
        Try
            Dim cColor As Color = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
            pnl_Titulo.BackColor = cColor
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub uctrlCollapsePanel_MouseHover(sender As Object, e As EventArgs) Handles Me.MouseHover
    '    Dim mouseArgs As MouseEventArgs
    '    'Dim hitInfo As  DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo
    '    Try
    '        mouseArgs = TryCast(e, MouseEventArgs)
    '        If Not mouseArgs Is Nothing Then
    '            If pnl_Titulo.Location = mouseArgs.Location Then
    '                Call pnl_Titulo_MouseHover(sender, e)
    '            End If
    '            ''hitInfo = GridV_1.CalcHitInfo(mouseArgs.Location)
    '            ''If hitInfo.HitTest = DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.Footer Then
    '            ''    SW_totales = True
    '            ''End If
    '        End If
    '    Catch ex As Exception
    '
    '    End Try
    'End Sub
    'Private Sub uctrlCollapsePanel_MouseLeave(sender As Object, e As EventArgs) Handles Me.MouseLeave
    '    Dim mouseArgs As MouseEventArgs
    '    'Dim hitInfo As  DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo
    '    Try
    '        mouseArgs = TryCast(e, MouseEventArgs)
    '        If Not mouseArgs Is Nothing Then
    '            If pnl_Titulo.Location = mouseArgs.Location Then
    '                Call pnl_Titulo_MouseLeave(sender, e)
    '            End If
    '            ''hitInfo = GridV_1.CalcHitInfo(mouseArgs.Location)
    '            ''If hitInfo.HitTest = DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.Footer Then
    '            ''    SW_totales = True
    '            ''End If
    '        End If
    '    Catch ex As Exception
    '
    '    End Try
    'End Sub
End Class
