﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCapturaVideo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.pbVideo = New System.Windows.Forms.PictureBox()
        Me.btnValidar = New System.Windows.Forms.Button()
        Me.ComboBox_Devices = New System.Windows.Forms.ComboBox()
        Me.ComboBox_Audio = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.pbVideo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(113, 73)
        Me.btnStart.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(70, 24)
        Me.btnStart.TabIndex = 0
        Me.btnStart.Text = "Empezar"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.Enabled = False
        Me.btnStop.Location = New System.Drawing.Point(187, 73)
        Me.btnStop.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(58, 24)
        Me.btnStop.TabIndex = 1
        Me.btnStop.Text = "Finalizar"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'pbVideo
        '
        Me.pbVideo.Location = New System.Drawing.Point(283, 107)
        Me.pbVideo.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pbVideo.Name = "pbVideo"
        Me.pbVideo.Size = New System.Drawing.Size(242, 207)
        Me.pbVideo.TabIndex = 2
        Me.pbVideo.TabStop = False
        '
        'btnValidar
        '
        Me.btnValidar.Enabled = False
        Me.btnValidar.Location = New System.Drawing.Point(256, 73)
        Me.btnValidar.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnValidar.Name = "btnValidar"
        Me.btnValidar.Size = New System.Drawing.Size(50, 24)
        Me.btnValidar.TabIndex = 3
        Me.btnValidar.Text = "Validar"
        Me.btnValidar.UseVisualStyleBackColor = True
        '
        'ComboBox_Devices
        '
        Me.ComboBox_Devices.FormattingEnabled = True
        Me.ComboBox_Devices.Location = New System.Drawing.Point(49, 9)
        Me.ComboBox_Devices.Name = "ComboBox_Devices"
        Me.ComboBox_Devices.Size = New System.Drawing.Size(343, 21)
        Me.ComboBox_Devices.TabIndex = 4
        '
        'ComboBox_Audio
        '
        Me.ComboBox_Audio.FormattingEnabled = True
        Me.ComboBox_Audio.Location = New System.Drawing.Point(49, 40)
        Me.ComboBox_Audio.Name = "ComboBox_Audio"
        Me.ComboBox_Audio.Size = New System.Drawing.Size(343, 21)
        Me.ComboBox_Audio.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 14)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Video:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 42)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 15)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Audio:"
        '
        'frmCapturaVideo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 121)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBox_Audio)
        Me.Controls.Add(Me.ComboBox_Devices)
        Me.Controls.Add(Me.btnValidar)
        Me.Controls.Add(Me.pbVideo)
        Me.Controls.Add(Me.btnStop)
        Me.Controls.Add(Me.btnStart)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MaximizeBox = False
        Me.Name = "frmCapturaVideo"
        Me.ShowIcon = False
        Me.Text = "Video de prueba de vida"
        CType(Me.pbVideo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnStart As Button
    Friend WithEvents btnStop As Button
    Friend WithEvents pbVideo As PictureBox
    Friend WithEvents btnValidar As Button
    Friend WithEvents ComboBox_Devices As ComboBox
    Friend WithEvents ComboBox_Audio As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
