﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvCuentaEventual
    Dim _Accion As Boolean
    Public IdDir As Integer
    Private Sub pdvFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Msj As String = ""

        _Accion = False

        oDatos = New Datos_Viscoi
        If oDatos.PVTA_Recupera_SatRegimenFiscal40(dtDatos, Msj) Then
            cbo_Regimen.DataSource = dtDatos
            cbo_Regimen.SelectedIndex = 0
        End If
        If oDatos.PVTA_Recupera_SatUsoCDFI("F", dtDatos, Msj) Then
            cbo_UsoCFDI.DataSource = dtDatos
            cbo_UsoCFDI.SelectedIndex = 0
        End If
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            Dim Cliente As String = txt_Cliente.Text
            oDatos = New Datos_Viscoi
            If oDatos.Inserta_Cuenta(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal _
                                    , Cliente _
                                    , Cliente _
                                    , "EVEN" _
                                    , "NORM" _
                                    , txt_Nombre.Text _
                                    , "" _
                                    , txt_Calle.Text & " E:" & txt_NumExt.Text & IIf(txt_NumInt.Text.Length > 0, " I:" & txt_NumInt.Text, "") _
                                    , "" _
                                    , txt_Colonia.Text _
                                    , txt_Municipio.Text _
                                    , txt_Municipio.Text _
                                    , txt_Estado.Text _
                                    , txt_Pais.Text _
                                    , txt_CP.Text _
                                    , txt_RFC.Text _
                                    , txt_Nombre.Text _
                                    , txt_Telefono.Text _
                                    , txt_Telefono2.Text _
                                    , txt_EMail.Text _
                                    , Globales.oAmbientes.oUsuario.Id_usuario _
                                    , 0.0 _
                                    , cbo_UsoCFDI.SelectedValue _
                                    , cbo_Regimen.SelectedValue _
                                    , Msj) Then
                txt_Cliente.Text = Cliente
                _Accion = True
                Me.Close()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Cliente_Click(sender As Object, e As EventArgs) Handles btn_Cliente.Click
        Dim oFrm As pdvCuentas
        Try
            oFrm = New pdvCuentas
            oFrm.Tipo = "EVEN"
            oFrm.TipoForma = pdvCuentas.eTipoForma.Cliente
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Cliente.Text = oFrm.Cuenta
                Call BuscarCuenta(txt_Cliente.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub BuscarCuenta(ByRef Tarjeta As String)
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim posExt As Integer
        Dim posInt As Integer
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_Cuentas("", Tarjeta, "", "", "", "", "EVEN", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_Cliente.Text = dtDatos.Rows(0).Item("cuenta")
                    txt_Nombre.Text = dtDatos.Rows(0).Item("nombre")
                    txt_RFC.Text = dtDatos.Rows(0).Item("rfc")
                    txt_Telefono.Text = dtDatos.Rows(0).Item("telcasa")
                    txt_Telefono2.Text = dtDatos.Rows(0).Item("telofna1")
                    txt_EMail.Text = dtDatos.Rows(0).Item("email")
                    Try
                        posExt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("dircasa1"), "E:")
                        posInt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("dircasa1"), "I:")
                        If posExt > 0 Then
                            txt_Calle.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(0, posExt - 1)
                        Else
                            txt_Calle.Text = dtDatos.Rows(0).Item("dircasa1")
                        End If
                        If posInt > 0 Then
                            txt_NumExt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posExt - 1, posInt - posExt - 1).Replace("E:", "").Trim()
                            txt_NumInt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posInt - 1).Replace("I:", "")
                        Else
                            txt_NumExt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posExt - 1).Replace("E:", "").Trim()
                            txt_NumInt.Text = ""
                        End If
                    Catch ex As Exception

                    End Try

                    txt_CP.Text = dtDatos.Rows(0).Item("cpcasa")
                    txt_Colonia.Text = dtDatos.Rows(0).Item("colcasa")
                    txt_Municipio.Text = dtDatos.Rows(0).Item("cdcasa")
                    txt_Estado.Text = dtDatos.Rows(0).Item("estado")
                    cbo_Regimen.SelectedValue = dtDatos.Rows(0).Item("regimen_fiscal")
                    cbo_UsoCFDI.SelectedValue = dtDatos.Rows(0).Item("uso_cfdi")
                Else
                    txt_Cliente.Text = ""
                    txt_Nombre.Text = ""
                    txt_RFC.Text = ""
                    txt_Telefono.Text = ""
                    txt_Telefono2.Text = ""
                    txt_EMail.Text = ""
                    txt_Calle.Text = ""
                    txt_NumExt.Text = ""
                    txt_NumInt.Text = ""
                    txt_CP.Text = ""
                    txt_Colonia.Text = ""
                    txt_Municipio.Text = ""
                    txt_Estado.Text = ""
                End If
            Else
                txt_Cliente.Text = ""
                txt_Nombre.Text = ""
                txt_RFC.Text = ""
                txt_Telefono.Text = ""
                txt_Telefono2.Text = ""
                txt_EMail.Text = ""
                txt_Calle.Text = ""
                txt_NumExt.Text = ""
                txt_NumInt.Text = ""
                txt_CP.Text = ""
                txt_Colonia.Text = ""
                txt_Municipio.Text = ""
                txt_Estado.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

End Class