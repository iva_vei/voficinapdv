﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uctrlCollapsePanel
    Inherits System.Windows.Forms.UserControl
    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnl_BaseFiltros = New System.Windows.Forms.Panel()
        Me.pnl_Filtros = New System.Windows.Forms.Panel()
        Me.pnl_Titulo = New System.Windows.Forms.Panel()
        Me.pic_Panel_Open = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnl_BaseFiltros.SuspendLayout()
        Me.pnl_Filtros.SuspendLayout()
        Me.pnl_Titulo.SuspendLayout()
        CType(Me.pic_Panel_Open, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnl_BaseFiltros
        '
        Me.pnl_BaseFiltros.Controls.Add(Me.pnl_Filtros)
        Me.pnl_BaseFiltros.Controls.Add(Me.pnl_Titulo)
        Me.pnl_BaseFiltros.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnl_BaseFiltros.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl_BaseFiltros.Location = New System.Drawing.Point(0, 0)
        Me.pnl_BaseFiltros.Name = "pnl_BaseFiltros"
        Me.pnl_BaseFiltros.Size = New System.Drawing.Size(537, 150)
        Me.pnl_BaseFiltros.TabIndex = 1
        '
        'pnl_Filtros
        '
        Me.pnl_Filtros.Controls.Add(Me.Label2)
        Me.pnl_Filtros.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnl_Filtros.Location = New System.Drawing.Point(0, 24)
        Me.pnl_Filtros.Name = "pnl_Filtros"
        Me.pnl_Filtros.Size = New System.Drawing.Size(537, 126)
        Me.pnl_Filtros.TabIndex = 2
        '
        'pnl_Titulo
        '
        Me.pnl_Titulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.pnl_Titulo.Controls.Add(Me.pic_Panel_Open)
        Me.pnl_Titulo.Controls.Add(Me.Label1)
        Me.pnl_Titulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnl_Titulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl_Titulo.Location = New System.Drawing.Point(0, 0)
        Me.pnl_Titulo.Name = "pnl_Titulo"
        Me.pnl_Titulo.Size = New System.Drawing.Size(537, 24)
        Me.pnl_Titulo.TabIndex = 1
        Me.pnl_Titulo.Tag = "OPENED"
        '
        'pic_Panel_Open
        '
        Me.pic_Panel_Open.Image = Global.proyVOficina_PDV.My.Resources.Resources.flecha_down
        Me.pic_Panel_Open.Location = New System.Drawing.Point(106, 5)
        Me.pic_Panel_Open.Name = "pic_Panel_Open"
        Me.pic_Panel_Open.Size = New System.Drawing.Size(16, 16)
        Me.pic_Panel_Open.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pic_Panel_Open.TabIndex = 1
        Me.pic_Panel_Open.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 2)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "FILTROS"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(291, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Label2"
        '
        'uctrlCollapsePanel
        '
        Me.Controls.Add(Me.pnl_BaseFiltros)
        Me.Name = "uctrlCollapsePanel"
        Me.Size = New System.Drawing.Size(537, 150)
        Me.pnl_BaseFiltros.ResumeLayout(False)
        Me.pnl_Filtros.ResumeLayout(False)
        Me.pnl_Filtros.PerformLayout()
        Me.pnl_Titulo.ResumeLayout(False)
        Me.pnl_Titulo.PerformLayout()
        CType(Me.pic_Panel_Open, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnl_BaseFiltros As Panel
    Friend WithEvents pnl_Filtros As Panel
    Friend WithEvents pic_Panel_Open As PictureBox
    Friend WithEvents Label1 As Label
    Public WithEvents pnl_Titulo As Panel
    Friend WithEvents Label2 As Label
End Class
