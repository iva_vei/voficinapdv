﻿Public Class NewsFeed
    Public _ArchivoNewsFeed As String
    Public _Path As String
    Private Sub NewsFeed_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WebBrowser1.Hide()
        WebBrowser1.Navigate(_Path & _ArchivoNewsFeed)
        'Panel1.AutoScrollMinSize = PDFPictureBox.Size()
        WebBrowser1.Show()
    End Sub
    Public Property Archivo()
        Get
            Return _ArchivoNewsFeed
        End Get
        Set(value)
            _ArchivoNewsFeed = value
        End Set
    End Property
    Public Property Path()
        Get
            Return _Path
        End Get
        Set(value)
            _Path = value
        End Set
    End Property

End Class