﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvFacturaGlobal40
    Dim _Accion As Boolean
    Private dtCabecero As DataTable
    Private dtDetalle As DataTable
    Private dtOtros As DataTable

    Private Sub pdvFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Msj As String = ""

        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""

        Dim FechaMesAntIni As Date
        Dim FechaMesAntFin As Date
        Try
            FechaMesAntIni = Today.AddMonths(-1)
            FechaMesAntIni = New Date(FechaMesAntIni.Year, FechaMesAntIni.Month, 1)

            FechaMesAntFin = Today
            FechaMesAntFin = New Date(FechaMesAntFin.Year, FechaMesAntFin.Month, 1)
            FechaMesAntFin = FechaMesAntFin.AddDays(-1)

            txt_Serie.Text = "FA"

            lblOC.Visible = True
            txt_OrdenCompra.Visible = True

            oDatos = New Datos_Viscoi
            datFEchaEntrega.Value = Today
            datFEchaEntrega.MinDate = Today

            txt_Empresa.Text = Globales.oAmbientes.Id_Empresa
            If oDatos.PVTA_Recupera_Sucursales(txt_Empresa.Text, "", dtDatosSuc, Msj) Then
                cbo_Sucursal.DataSource = dtDatosSuc
                cbo_Sucursal.SelectedIndex = 0
            End If
            If oDatos.PVTA_Recupera_SatRegimenFiscal(dtDatos, Msj) Then
                cbo_Regimen.DataSource = dtDatos
                cbo_Regimen.SelectedIndex = 0
            End If
            If oDatos.PVTA_Recupera_SatUsoCDFI("F", dtDatos, Msj) Then
                cbo_UsoCFDI.DataSource = dtDatos
                cbo_UsoCFDI.SelectedIndex = 0
            End If
            If oDatos.PVTA_Recupera_SatFormaPago("F", dtDatos, Msj) Then
                cbo_FormaPago.DataSource = dtDatos
                cbo_FormaPago.SelectedIndex = 0
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    Exit For
                End If
            Next
            cbo_Sucursal.Text = PDatSucursal

            If txt_Autonumsuc.Text <> "" Then
                Call btn_Buscar_Click(Nothing, Nothing)
            End If

            dat_FechaInicio.Value = FechaMesAntIni
            dat_FechaFin.Value = FechaMesAntFin
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub GridC_1_KeyDown(sender As Object, e As KeyEventArgs) Handles GridC_1.KeyDown
        Call Globales.clsFormularios.Grid_Prevenir_Tab(sender, e, cbo_Regimen)
    End Sub
    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtLugEntregas As DataTable = Nothing
        Dim Msj As String = ""
        Dim dtRenglon As DataRow = Nothing
        Dim id_dir As Integer
        Dim dtDatos As DataTable = Nothing
        Dim EmisorSMTPCorreo As String = ""
        Dim EmisorSMTPHost As String = ""
        Dim EmisorSMTPUsername As String = ""
        Dim EmisorSMTPPassword As String = ""
        Dim EmisorSMTPPort As Integer = 587

        Dim Sw_Continuar As Boolean = False

        Try

            dtCabecero = Nothing
            dtDetalle = Nothing
            dtOtros = Nothing
            oDatos = New Datos_Viscoi
            If txt_Autonumsuc.Text = "" Then

                If oDatos.Inserta_FacturaGlobal(txt_Empresa.Text, cbo_Sucursal.Text, dat_FechaInicio.Value, dat_FechaFin.Value, txt_Serie.Text, "MXN", 0.0, Globales.oAmbientes.oUsuario.Id_usuario, txt_Autonumsuc.Text, Msj) Then
                    Sw_Continuar = True
                Else
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                Sw_Continuar = True
            End If
            If Sw_Continuar Then
                If oDatos.Inserta_TicketFactura(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, txt_Serie.Text, "MXN", 0.0, Globales.oAmbientes.oUsuario.Id_usuario, Msj) Then
                    If oDatos.Recupera_TicketFactura(Nothing, Nothing, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtCabecero, dtDetalle, dtOtros, Msj) Then
                        If dtCabecero.Rows.Count > 0 Then
                            dtRenglon = dtCabecero.Rows(0)
                            gcDatosTicket.Enabled = False
                            PnlDatosCliente.Enabled = False
                            PnlDatosDirFactura.Enabled = False
                            txt_Cliente.Text = dtRenglon("id_cliente")
                            txt_Nombre.Text = dtRenglon("nomfac")
                            txt_RFC.Text = dtRenglon("rfc")
                            txt_Calle.Text = dtRenglon("direccionfac")
                            txt_Colonia.Text = dtRenglon("coloniafac")
                            txt_CP.Text = dtRenglon("cpostalfac")
                            txt_Municipio.Text = dtRenglon("ciudadfac")
                            txt_Estado.Text = dtRenglon("edofac")
                            txt_OrdenCompra.Text = dtRenglon("pedido_mov")

                            cbo_Regimen.SelectedValue = dtRenglon("regimen_fiscal")
                            cbo_FormaPago.SelectedValue = dtRenglon("forma_pago")
                            cbo_UsoCFDI.SelectedValue = dtRenglon("uso_cfdi")
                            txt_Serie.Text = dtRenglon("observ2")
                            txt_Folio.Text = dtRenglon("folio")
                            txt_SubTotal.Text = Format(dtRenglon("subtotal"), "N2")
                            txt_IVA.Text = Format(dtRenglon("impuesto_iva"), "N2")
                            txt_Total.Text = Format(dtRenglon("total"), "N2")

                            txt_Correos.Text = dtRenglon("observ1").ToString.Trim

                            If dtRenglon("lugarent1").ToString.ToString.Trim.Length >= 2 Then

                                id_dir = Integer.Parse(dtRenglon("lugarent1").ToString.Substring(0, 2))

                            End If
                            If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                                GridC_1.DataSource = dtLugEntregas
                                If dtLugEntregas.Rows.Count > 0 Then
                                    GridV_1.FocusedRowHandle = id_dir
                                End If
                            End If
                            If dtDetalle.Rows.Count = 0 Then
                                MessageBox.Show("Autonumsuc (" & txt_Autonumsuc.Text & ") No contiene detalle para facturacion", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If

                            txt_Cliente.Text = txt_Cliente.Text.Trim()
                            txt_Nombre.Text = txt_Nombre.Text.Trim()
                            txt_RFC.Text = txt_RFC.Text.Trim()
                            txt_Calle.Text = txt_Calle.Text.Trim()
                            txt_Colonia.Text = txt_Colonia.Text.Trim()
                            txt_CP.Text = txt_CP.Text.Trim()
                            txt_Municipio.Text = txt_Municipio.Text.Trim()
                            txt_Estado.Text = txt_Estado.Text.Trim()

                            txt_Serie.Text = txt_Serie.Text.Trim()
                            txt_Folio.Text = txt_Folio.Text.Trim()
                            txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                            txt_IVA.Text = txt_IVA.Text.Trim()
                            txt_Total.Text = txt_Total.Text.Trim()

                            txt_Correos.Text = txt_Correos.Text.Trim()

                            If dtDetalle IsNot Nothing Then
                                GridC_2.DataSource = dtDetalle
                            Else
                                GridC_2.DataSource = Nothing
                            End If
                            GridC_2.RefreshDataSource()

                        Else
                            MessageBox.Show("Autonumsuc (" & txt_Autonumsuc.Text & ") Factura no encontrada", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    Else
                        MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If Msj.Contains("Ya se encuentra facturado") Then
                        If MessageBox.Show("¿Deseas re-imprimir la factura?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then
                            If oDatos.Recupera_TicketFactura(Nothing, Nothing, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtCabecero, dtDetalle, dtOtros, Msj) Then
                                If dtCabecero.Rows.Count > 0 Then
                                    Call Imprimir()
                                Else
                                    MessageBox.Show("Autonumsuc (" & txt_Autonumsuc.Text & ") Factura no encontrada", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If

                                If MessageBox.Show("¿Deseas re-enviar la factura?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then

                                    If oDatos.PVTA_Recupera_SatInfo(txt_Empresa.Text _
                                                       , dtDatos _
                                                       , Msj) Then
                                        If dtDatos.Rows.Count > 0 Then
                                            EmisorSMTPCorreo = dtDatos.Rows(0).Item("EmisorSMTPCorreo")
                                            EmisorSMTPHost = dtDatos.Rows(0).Item("EmisorSMTPHost")
                                            EmisorSMTPUsername = dtDatos.Rows(0).Item("EmisorSMTPUsername")
                                            EmisorSMTPPassword = dtDatos.Rows(0).Item("EmisorSMTPPassword")
                                            EmisorSMTPPort = dtDatos.Rows(0).Item("EmisorSMTPPort")

                                            Dim correos_nuevos As String
                                            Dim xmlDoc As System.Xml.XmlDocument
                                            xmlDoc = New System.Xml.XmlDocument()
                                            xmlDoc.LoadXml(dtCabecero.Rows(0).Item("xml"))
                                            xmlDoc.Save("C:\Windows\Temp\F" & txt_Autonumsuc.Text & "\" & txt_Autonumsuc.Text & ".xml")
                                            correos_nuevos = InputBox("Confirmar Correos para enviar ... (Cada correo debe estar separado por ; )", "Enviar factura por correo.", dtCabecero.Rows(0).Item("observ1"))
                                            Call SendMail(EmisorSMTPUsername, EmisorSMTPPassword, EmisorSMTPHost, EmisorSMTPPort, EmisorSMTPCorreo, correos_nuevos, "C:\Windows\Temp\F" & txt_Autonumsuc.Text, txt_Autonumsuc.Text)
                                            Call LimpiarPantalla()
                                        End If
                                    End If
                                End If
                            Else
                                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim sw_continuar As Boolean = True
        Dim IdDir
        Dim RegimenFiscal As String = cbo_Regimen.SelectedValue
        Try
            oDatos = New Datos_Viscoi
            If Not oDatos.Actualiza_TicketFactura_Cuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, txt_Cliente.Text, txt_Nombre.Text, txt_RFC.Text, txt_Calle.Text, txt_CP.Text, txt_Colonia.Text, txt_Municipio.Text, txt_Estado.Text, txt_Correos.Text, txt_OrdenCompra.Text, "", Msj) Then
                sw_continuar = False
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            If RegimenFiscal Is Nothing Then
                RegimenFiscal = ""
            End If
            If Not oDatos.Actualiza_TicketFactura_DatosCFDI(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, RegimenFiscal, cbo_UsoCFDI.SelectedValue, cbo_FormaPago.SelectedValue, txt_Folio.Text, Msj) Then
                sw_continuar = False
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            IdDir = GridV_1.GetFocusedRowCellValue(col1_IdDir)
            If Not oDatos.Actualiza_TicketFactura_DirEntrega(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, IdDir, datFEchaEntrega.Value, Msj) Then
                sw_continuar = False
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            If sw_continuar Then
                Call btn_Buscar_Click(Nothing, Nothing)
                Call Facturar()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub Facturar()
        Dim oComprobante As VOficinaTimbrador40.Comprobante
        Dim oEmisor As VOficinaTimbrador40.ComprobanteEmisor
        Dim oReceptor As VOficinaTimbrador40.ComprobanteReceptor
        Dim oConcepto As VOficinaTimbrador40.ComprobanteConcepto
        Dim lstConceptos As List(Of VOficinaTimbrador40.ComprobanteConcepto)
        Dim oConceptoImpuestos As VOficinaTimbrador40.ComprobanteConceptoImpuestos
        Dim oConceptoImpuestosTraslado As VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado
        Dim oConceptoImpuestosTraslados As List(Of VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado)
        Dim oImpuestos As VOficinaTimbrador40.ComprobanteImpuestos
        Dim oImpuestoTraslado00 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestoTraslado08 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestoTraslado16 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestosTraslado As List(Of VOficinaTimbrador40.ComprobanteImpuestosTraslado)
        Dim ImpIVA As Double = 0

        Dim RutaCertificado As String = ""
        Dim RutaKey As String = ""
        Dim PasswordKey As String = ""

        Dim SubTotal As Decimal = 0.0
        Dim Iva As Decimal = 0.0
        Dim Total As Decimal = 0.0

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtFecha As DataTable = Nothing
        Dim Msj As String = ""
        Dim MsjFecha As String = ""
        Dim FechaActual As Date

        Dim EmisorNombre As String = ""
        Dim EmisorRfc As String = ""
        Dim EmisorRegimen As String = ""
        Dim EmisorFolio As String = ""
        Dim EmisorSerie As String = ""
        Dim EmisorVersion As String = ""
        Dim EmisorClaveProductoServ As String
        Dim EmisorPACAmbiente As String
        Dim EmisorPAC As String
        Dim EmisorPACUser As String
        Dim EmisorPACPass As String

        Dim EmisorSMTPCorreo As String
        Dim EmisorSMTPHost As String
        Dim EmisorSMTPUsername As String
        Dim EmisorSMTPPassword As String
        Dim EmisorSMTPPort As Integer = 587
        Dim LogTrimbrado As Boolean
        Dim EmisorLugarExpedicion As String
        Dim EmisorPeriocidad As String

        Dim email_enviado As Integer = 0

        Dim VOficinaTimbrador1 As VOficinaTimbrador40.VOficinaTimbrador40

        Dim oPara = New Dictionary(Of String, String)()

        Try
            If txt_Correos.Text.Length > 0 Then
                For Each sCorreo As String In txt_Correos.Text.Split(";")
                    oPara.Add(sCorreo.Trim, sCorreo.Trim)
                Next

                oDatos = New Datos_Viscoi
                Dim iRedondeo6 As Integer = 6
                Dim iRedondeo5 As Integer = 6
                Dim iRedondeo4 As Integer = 4
                Dim iRedondeo2 As Integer = 2
                If oDatos.PVTA_Sat_CFDI_FechaActual(dtFecha, MsjFecha) Then
                    FechaActual = dtFecha.Rows(0).Item("fum")
                    If oDatos.PVTA_Recupera_SatInfo(txt_Empresa.Text _
                                               , dtDatos _
                                               , Msj) Then
                        If dtDatos.Rows.Count > 0 Then
                            RutaCertificado = dtDatos.Rows(0).Item("RutaCertificado")
                            RutaKey = dtDatos.Rows(0).Item("RutaKey")
                            PasswordKey = dtDatos.Rows(0).Item("PasswordKey")
                            EmisorNombre = dtDatos.Rows(0).Item("EmisorNombre")
                            EmisorRfc = dtDatos.Rows(0).Item("EmisorRfc")
                            EmisorRegimen = dtDatos.Rows(0).Item("EmisorRegimen")
                            EmisorFolio = dtDatos.Rows(0).Item("EmisorFolio")
                            EmisorSerie = dtDatos.Rows(0).Item("EmisorSerie")
                            EmisorVersion = dtDatos.Rows(0).Item("EmisorVersion")
                            EmisorClaveProductoServ = dtDatos.Rows(0).Item("EmisorClaveProductoServ")
                            EmisorPACAmbiente = dtDatos.Rows(0).Item("EmisorPACAmbiente")
                            EmisorPAC = dtDatos.Rows(0).Item("EmisorPAC")
                            EmisorPACUser = dtDatos.Rows(0).Item("EmisorPACUser")
                            EmisorPACPass = dtDatos.Rows(0).Item("EmisorPACPass")
                            EmisorSMTPCorreo = dtDatos.Rows(0).Item("EmisorSMTPCorreo")
                            EmisorSMTPHost = dtDatos.Rows(0).Item("EmisorSMTPHost")
                            EmisorSMTPUsername = dtDatos.Rows(0).Item("EmisorSMTPUsername")
                            EmisorSMTPPassword = dtDatos.Rows(0).Item("EmisorSMTPPassword")
                            EmisorSMTPPort = dtDatos.Rows(0).Item("EmisorSMTPPort")
                            EmisorLugarExpedicion = dtDatos.Rows(0).Item("EmisorLugarExpedicion")
                            EmisorPeriocidad = dtDatos.Rows(0).Item("EmisorPeriocidad")
                            LogTrimbrado = dtDatos.Rows(0).Item("LogTrimbrado")

                            VOficinaTimbrador1 = New VOficinaTimbrador40.VOficinaTimbrador40(EmisorPAC, EmisorPACUser, EmisorPACPass, RutaCertificado, RutaKey, PasswordKey, EmisorPACAmbiente)

                            SubTotal = Double.Parse(txt_SubTotal.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                            Iva = Double.Parse(txt_IVA.Text.Replace("$", "").Replace(",", "").Replace("-", ""), Globalization.NumberStyles.AllowDecimalPoint)
                            Total = Double.Parse(txt_Total.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                            SubTotal = Math.Round(SubTotal, 2)
                            Total = Math.Round(Total, 2)
                            Iva = Math.Round(Iva, 2)

                            ''VOficinaTimbrador1.ValorSubtotal = SubTotal
                            ''VOficinaTimbrador1.ValorIVA = Iva
                            ''VOficinaTimbrador1.ValorTotal = Total

                            oComprobante = New VOficinaTimbrador40.Comprobante()
                            oComprobante.Version = EmisorVersion
                            oComprobante.Serie = txt_Serie.Text
                            oComprobante.Folio = txt_Folio.Text
                            ''oComprobante.Fecha = FechaActual.ToString("yyyy-MM-ddTHH:mm:ss")
                            oComprobante.Fecha = dtCabecero.Rows(0).Item("fum")
                            oComprobante.Fecha = oComprobante.Fecha.ToString("yyyy-MM-ddTHH:mm:ss")

                            oComprobante.FormaPago = cbo_FormaPago.SelectedValue
                            oComprobante.Total = 0 ''Total
                            oComprobante.SubTotal = 0 ''SubTotal
                            oComprobante.Moneda = VOficinaTimbrador40.c_Moneda.MXN
                            oComprobante.MetodoPago = dtCabecero.Rows(0).Item("metodo_pago")
                            oComprobante.LugarExpedicion = dtOtros.Rows(0).Item("FacCP")
                            oComprobante.TipoDeComprobante = VOficinaTimbrador40.c_TipoDeComprobante.I
                            oComprobante.Exportacion = dtCabecero.Rows(0).Item("exportacion")

                            Dim oInfoGlogal As New VOficinaTimbrador40.ComprobanteInformacionGlobal
                            oInfoGlogal.Año = dat_FechaFin.Value.Year
                            oInfoGlogal.Meses = Format(dat_FechaFin.Value.Month, "00")
                            oInfoGlogal.Periodicidad = EmisorPeriocidad
                            oComprobante.InformacionGlobal = oInfoGlogal

                            oEmisor = New VOficinaTimbrador40.ComprobanteEmisor

                            oEmisor.Nombre = EmisorNombre
                            oEmisor.RegimenFiscal = [Enum].Parse(GetType(VOficinaTimbrador40.c_RegimenFiscal), EmisorRegimen)
                            oEmisor.Rfc = EmisorRfc

                            oReceptor = New VOficinaTimbrador40.ComprobanteReceptor
                            oReceptor.Nombre = "PUBLICO EN GENERAL"
                            oReceptor.Rfc = txt_RFC.Text
                            oReceptor.UsoCFDI = [Enum].Parse(GetType(VOficinaTimbrador40.c_UsoCFDI), cbo_UsoCFDI.SelectedValue)
                            oReceptor.DomicilioFiscalReceptor = dtOtros.Rows(0).Item("FacCP")
                            oReceptor.RegimenFiscalReceptor = [Enum].Parse(GetType(VOficinaTimbrador40.c_RegimenFiscal), "616")

                            oComprobante.Emisor = oEmisor
                            oComprobante.Receptor = oReceptor
                            lstConceptos = New List(Of VOficinaTimbrador40.ComprobanteConcepto)

                            oImpuestos = New VOficinaTimbrador40.ComprobanteImpuestos
                            oImpuestosTraslado = New List(Of VOficinaTimbrador40.ComprobanteImpuestosTraslado)
                            oImpuestoTraslado00 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestoTraslado08 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestoTraslado16 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado

                            oImpuestos.TotalImpuestosTrasladados = 0.0
                            Dim importetras As Decimal = 0.0
                            Dim importebase00 As Decimal = 0.0
                            Dim importebase08 As Decimal = 0.0
                            Dim importebase16 As Decimal = 0.0
                            Dim importetras00 As Decimal = 0.0
                            Dim importetras08 As Decimal = 0.0
                            Dim importetras16 As Decimal = 0.0
                            Dim importeimpu00 As Decimal = 0.0
                            Dim importeimpu08 As Decimal = 0.0
                            Dim importeimpu16 As Decimal = 0.0
                            Dim importedesc As Decimal = 0.0
                            Dim conceptoimporte As Decimal = 0.0
                            Dim Sw_Traslado00 As Boolean = False
                            ''Dim importebase As Decimal = 0.0
                            Dim FechaFac As Date
                            For Each dtRenglon As DataRow In dtDetalle.Rows
                                FechaFac = dtCabecero.Rows(0).Item("fecha")
                                oConcepto = New VOficinaTimbrador40.ComprobanteConcepto
                                oConcepto.ClaveProdServ = dtRenglon("unidadvta")
                                oConcepto.Cantidad = dtRenglon("cantidad")
                                oConcepto.ClaveUnidad = [Enum].Parse(GetType(VOficinaTimbrador40.c_ClaveUnidad), dtRenglon("claveprodserv"))
                                'oConcepto.Descripcion = "FACTURA GLOBAL correspondiente al mes " & Format(FechaFac.Month, "00") & " del año " & FechaFac.Year.ToString() ''dtRenglon("concepto")
                                oConcepto.NoIdentificacion = dtRenglon("concepto")
                                oConcepto.Descripcion = "Venta"
                                oConcepto.ValorUnitario = Math.Round(dtRenglon("precio") / (1.0 + dtRenglon("tasa_iva")), 4)
                                oConcepto.Importe = Math.Round(oConcepto.Cantidad * oConcepto.ValorUnitario, iRedondeo4) ''Math.Round(dtRenglon("subtotal") / (1.0 + dtRenglon("tasa_iva")), 4)
                                oConcepto.Importe = Globales.oAmbientes.Redondear(oConcepto.Cantidad * oConcepto.ValorUnitario, iRedondeo2, True) ''Math.Round(dtRenglon("subtotal") / (1.0 + dtRenglon("tasa_iva")), 4)
                                oConcepto.ObjetoImp = dtRenglon("objetoimp")

                                oConceptoImpuestos = New VOficinaTimbrador40.ComprobanteConceptoImpuestos
                                oConceptoImpuestosTraslados = New List(Of VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado)
                                oConceptoImpuestosTraslado = New VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado
                                oConceptoImpuestosTraslado.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002 ''ESTE ES IVA
                                oConceptoImpuestosTraslado.Base = oConcepto.Importe ''Math.Round(dtRenglon("importe1") - dtRenglon("descuento"), iRedondeo6)

                                'oConceptoImpuestosTraslado.Importe = Globales.oAmbientes.Redondear4(oConceptoImpuestosTraslado.Base * dtRenglon("tasa_iva"), iRedondeo4, True)
                                oConceptoImpuestosTraslado.Importe = Globales.oAmbientes.Redondear(oConceptoImpuestosTraslado.Base * dtRenglon("tasa_iva"), iRedondeo2, True)
                                ''oConceptoImpuestosTraslado.Importe = Math.Round(oConceptoImpuestosTraslado.Base * dtRenglon("tasa_iva"), iRedondeo4)

                                conceptoimporte += oConceptoImpuestosTraslado.Importe
                                Select Case Math.Round(dtRenglon("tasa_iva"), iRedondeo6)
                                    Case 0.16
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase16 += oConceptoImpuestosTraslado.Base
                                        importeimpu16 += oConceptoImpuestosTraslado.Importe
                                    Case 0.08
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_08
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase08 += oConceptoImpuestosTraslado.Base
                                        importeimpu08 += oConceptoImpuestosTraslado.Importe
                                    Case 0.0
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_00
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase00 += oConceptoImpuestosTraslado.Base
                                        importeimpu00 += oConceptoImpuestosTraslado.Importe
                                        Sw_Traslado00 = True
                                    Case Else
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase16 += oConceptoImpuestosTraslado.Base
                                        importeimpu16 += oConceptoImpuestosTraslado.Importe
                                End Select
                                oComprobante.SubTotal += Math.Round(oConcepto.Importe, iRedondeo2)
                                oComprobante.Total += oConcepto.Importe + oConceptoImpuestosTraslado.Importe

                                oConceptoImpuestosTraslados.Add(oConceptoImpuestosTraslado)
                                oConceptoImpuestos.Traslados = oConceptoImpuestosTraslados.ToArray()
                                If Not (oConcepto.ObjetoImp = "01" Or oConcepto.ObjetoImp = "03") Then
                                    oConcepto.Impuestos = oConceptoImpuestos
                                End If

                                lstConceptos.Add(oConcepto)
                                oComprobante.Conceptos = lstConceptos.ToArray
                            Next

                            ''oComprobante.Descuento = importedesc

                            oImpuestoTraslado16.Base = Math.Round(importebase16, iRedondeo2)
                            oImpuestoTraslado16.Importe = Math.Round(importeimpu16, iRedondeo2)
                            oImpuestoTraslado16.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado16.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                            oImpuestoTraslado16.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            oImpuestoTraslado08.Base = Math.Round(importebase08, iRedondeo2)
                            oImpuestoTraslado08.Importe = Math.Round(importeimpu08, iRedondeo2)
                            oImpuestoTraslado08.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado08.TasaOCuota = VOficinaTimbrador1.ValorIVA_08
                            oImpuestoTraslado08.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            oImpuestoTraslado00.Base = Math.Round(importebase00, iRedondeo2)
                            oImpuestoTraslado00.Importe = Math.Round(importeimpu00, iRedondeo2)
                            oImpuestoTraslado00.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado00.TasaOCuota = VOficinaTimbrador1.ValorIVA_00
                            oImpuestoTraslado00.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            If importeimpu16 > 0 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado16)
                            End If
                            If importeimpu08 > 0 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado08)
                            End If
                            If Sw_Traslado00 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado00)
                            End If
                            oImpuestos.TotalImpuestosTrasladados = Math.Round(oImpuestoTraslado16.Importe + oImpuestoTraslado08.Importe + oImpuestoTraslado00.Importe, iRedondeo2)
                            If oImpuestosTraslado.Count > 0 And oImpuestos.TotalImpuestosTrasladados Then
                                oImpuestos.Traslados = oImpuestosTraslado.ToArray()
                                oComprobante.Impuestos = oImpuestos
                            End If

                            importetras += oImpuestos.TotalImpuestosTrasladados

                            ''oComprobante.Descuento = Math.Round(importedesc, iRedondeo2)

                            oComprobante.SubTotal = Math.Round(oComprobante.SubTotal, iRedondeo2)
                            oComprobante.Total = Math.Abs(oComprobante.Total)
                            oComprobante.SubTotal = Math.Abs(oComprobante.SubTotal)
                            ''oComprobante.Descuento = Math.Abs(oComprobante.Descuento)
                            oComprobante.Total = Math.Round(oComprobante.SubTotal - oComprobante.Descuento + oImpuestos.TotalImpuestosTrasladados, iRedondeo2)

                            VOficinaTimbrador1.Tipo = "Factura"
                            Dim oPagos As VOficinaTimbrador40.Pagos = Nothing
                            VOficinaTimbrador1.Sellar(oComprobante, LogTrimbrado, oPagos)
                            If VOficinaTimbrador1.ErrorMessage = "" Then
                                VOficinaTimbrador1.Timbrar(LogTrimbrado)

                                If (VOficinaTimbrador1.ErrorMessage = "") Then
                                    VOficinaTimbrador1.SMTPHost = EmisorSMTPHost
                                    VOficinaTimbrador1.SMTPUsername = EmisorSMTPUsername
                                    VOficinaTimbrador1.SMTPPassword = EmisorSMTPPassword
                                    VOficinaTimbrador1.SMTPPort = EmisorSMTPPort

                                    If oDatos.Actualiza_TicketFactura_CFDI_Gen(Nothing, Nothing, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, email_enviado, "FACTURADO", oComprobante.SubTotal, oComprobante.Total, VOficinaTimbrador1.TFD.UUID, VOficinaTimbrador1.XML, VOficinaTimbrador1.TFD.noCertificadoSAT, VOficinaTimbrador1.TFD.selloCFD, VOficinaTimbrador1.TFD.selloSAT, VOficinaTimbrador1.TFD.CadenaOriginalComplemento, VOficinaTimbrador1.TFD.version, VOficinaTimbrador1.TFD.FechaTimbrado, VOficinaTimbrador1.TFD.RfcProvCertif, VOficinaTimbrador1.NoCertificado, EmisorPeriocidad, "", "", Msj) Then
                                        MessageBox.Show("Factura Generada con exito.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                                        Dim xmlDoc As System.Xml.XmlDocument
                                        xmlDoc = New System.Xml.XmlDocument()
                                        xmlDoc.LoadXml(VOficinaTimbrador1.XML)
                                        If Not IO.Directory.Exists("C:\Windows\Temp\F" & txt_Autonumsuc.Text) Then
                                            IO.Directory.CreateDirectory("C:\Windows\Temp\F" & txt_Autonumsuc.Text)
                                        End If
                                        xmlDoc.Save("C:\Windows\Temp\F" & txt_Autonumsuc.Text & "\" & txt_Autonumsuc.Text & ".xml")
                                        If oDatos.Recupera_TicketFactura(Nothing, Nothing, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtCabecero, dtDetalle, dtOtros, Msj) Then
                                            If dtCabecero.Rows.Count > 0 Then
                                                Call Imprimir()
                                                Call SendMail(EmisorSMTPUsername, EmisorSMTPPassword, EmisorSMTPHost, EmisorSMTPPort, EmisorSMTPUsername, txt_Correos.Text, "C:\Windows\Temp\F" & txt_Autonumsuc.Text, txt_Autonumsuc.Text)
                                                Call LimpiarPantalla()
                                            Else
                                                MessageBox.Show("Autonumsuc (" & txt_Autonumsuc.Text & ") Factura no encontrada", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                            End If
                                        End If
                                    Else
                                        MessageBox.Show("Error al actualizar Factura " & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    End If

                                    Console.WriteLine(VOficinaTimbrador1.TFD.noCertificadoSAT)
                                    Console.WriteLine(VOficinaTimbrador1.TFD.selloCFD)
                                    Console.WriteLine(VOficinaTimbrador1.TFD.selloSAT)
                                    Console.WriteLine(VOficinaTimbrador1.TFD.UUID)
                                    Console.WriteLine(VOficinaTimbrador1.ErrorMail)

                                Else
                                    MessageBox.Show("El timbrador dijo: " & vbNewLine & VOficinaTimbrador1.ErrorMessage, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If
                            Else
                                MessageBox.Show("Sellar dijo: " & vbNewLine & VOficinaTimbrador1.ErrorMessage, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        Else
                            MessageBox.Show("No se encontraron datos del emisor para facturar para la empresa ('" & txt_Empresa.Text & "')", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    Else
                        MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    MessageBox.Show("No es posuble obtener la fecha (PVTA_Sat_CFDI_FechaActual)", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                    MessageBox.Show("Debe introducir un correo electronico.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    Private Sub LimpiarPantalla()
        txt_Autonumsuc.Text = ""
        txt_Cliente.Text = ""
        txt_Nombre.Text = ""
        txt_RFC.Text = ""
        txt_Calle.Text = ""
        txt_CP.Text = ""
        txt_Colonia.Text = ""
        txt_Estado.Text = ""
        txt_Municipio.Text = ""
        txt_Serie.Text = "FA"
        txt_Folio.Text = ""
        txt_SubTotal.Text = "0"
        txt_IVA.Text = "0"
        txt_Total.Text = "0"
        cbo_Regimen.SelectedIndex = 0
        cbo_FormaPago.SelectedIndex = 0
        cbo_UsoCFDI.SelectedIndex = 0
        txt_Correos.Text = ""
        GridC_1.DataSource = Nothing
        GridC_1.RefreshDataSource()
        GridC_2.DataSource = Nothing
        GridC_2.RefreshDataSource()
        gcDatosTicket.Enabled = True
    End Sub

    Private Sub btn_CambiarCliente_Click(sender As Object, e As EventArgs) Handles btn_CambiarCliente.Click
        Dim oForma As pdvCuentas
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtLugEntregas As DataTable = Nothing
        Dim Msj As String = ""
        Dim Id_dir As Integer = 0
        Try
            oForma = New pdvCuentas
            oForma.Tipo = ""
            oForma.TipoForma = pdvCuentas.eTipoForma.Cliente
            oForma.ShowDialog()

            oDatos = New Datos_Viscoi
            If oDatos.Actualiza_TicketFactura_Cuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, oForma.Cuenta, oForma.Nombre, oForma.Rfc, oForma.Direccion, oForma.CodigoPostal, oForma.Colonia, oForma.Ciudad, oForma.Estado, oForma.EMail, txt_OrdenCompra.Text, "", Msj) Then
                txt_Cliente.Text = oForma.Cuenta
                txt_Nombre.Text = oForma.Nombre
                txt_RFC.Text = oForma.Rfc

                txt_Calle.Text = oForma.Direccion
                txt_CP.Text = oForma.CodigoPostal
                txt_Colonia.Text = oForma.Colonia
                txt_Municipio.Text = oForma.Ciudad
                txt_Estado.Text = oForma.Estado
                txt_Correos.Text = oForma.EMail

                txt_Cliente.Text = txt_Cliente.Text.Trim()
                txt_Nombre.Text = txt_Nombre.Text.Trim()
                txt_RFC.Text = txt_RFC.Text.Trim()
                txt_Calle.Text = txt_Calle.Text.Trim()
                txt_Colonia.Text = txt_Colonia.Text.Trim()
                txt_CP.Text = txt_CP.Text.Trim()
                txt_Municipio.Text = txt_Municipio.Text.Trim()
                txt_Estado.Text = txt_Estado.Text.Trim()

                txt_Serie.Text = txt_Serie.Text.Trim()
                txt_Folio.Text = txt_Folio.Text.Trim()
                txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                txt_IVA.Text = txt_IVA.Text.Trim()
                txt_Total.Text = txt_Total.Text.Trim()

                txt_Correos.Text = txt_Correos.Text.Trim()

                cbo_Regimen.SelectedValue = IIf(txt_RFC.Text.Length > 12, "F", "M")

                If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                    GridC_1.DataSource = dtLugEntregas
                    If dtLugEntregas.Rows.Count > 0 Then
                        GridV_1.FocusedRowHandle = Id_dir
                    End If
                End If
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_CambiarDir_Click(sender As Object, e As EventArgs) Handles btn_CambiarDir.Click
        Try
            PnlDatosDirFactura.Enabled = True
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    Private Sub btn_GuardarDir_Click(sender As Object, e As EventArgs) Handles btn_GuardarDir.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If oDatos.Actualiza_TicketFactura_Cuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, txt_Cliente.Text, txt_Nombre.Text, txt_RFC.Text, txt_Calle.Text, txt_CP.Text, txt_Colonia.Text, txt_Municipio.Text, txt_Estado.Text, txt_Correos.Text, txt_OrdenCompra.Text, "", Msj) Then
                PnlDatosDirFactura.Enabled = False

                txt_Cliente.Text = txt_Cliente.Text.Trim()
                txt_Nombre.Text = txt_Nombre.Text.Trim()
                txt_RFC.Text = txt_RFC.Text.Trim()
                txt_Calle.Text = txt_Calle.Text.Trim()
                txt_Colonia.Text = txt_Colonia.Text.Trim()
                txt_CP.Text = txt_CP.Text.Trim()
                txt_Municipio.Text = txt_Municipio.Text.Trim()
                txt_Estado.Text = txt_Estado.Text.Trim()

                txt_Serie.Text = txt_Serie.Text.Trim()
                txt_Folio.Text = txt_Folio.Text.Trim()
                txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                txt_IVA.Text = txt_IVA.Text.Trim()
                txt_Total.Text = txt_Total.Text.Trim()

                txt_Correos.Text = txt_Correos.Text.Trim()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub btn_NuevoDireccion_Click(sender As Object, e As EventArgs) Handles btn_NuevoDireccion.Click
        Dim oForma As pdvDireccionEntrega
        Dim id_dir As Integer
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtLugEntregas As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oForma = New pdvDireccionEntrega
            oForma.txt_Cliente.Text = txt_Cliente.Text
            oForma.ShowDialog()
            id_dir = oForma.IdDir
            oDatos = New Datos_Viscoi
            If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                GridC_1.DataSource = dtLugEntregas
                If dtLugEntregas.Rows.Count > 0 Then
                    GridV_1.FocusedRowHandle = id_dir
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Me.Close()
    End Sub
    Private Sub pdvFactura_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Call btn_Cancelar_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_NuevoCliente_Click(sender As Object, e As EventArgs) Handles btn_NuevoCliente.Click
        Dim id_dir As Integer
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtLugEntregas As DataTable = Nothing
        Dim Msj As String = ""
        Try
            Dim oForma As pdvCuentaEventual
            oForma = New pdvCuentaEventual
            oForma.StartPosition = FormStartPosition.CenterScreen
            oForma.ShowDialog()
            oDatos = New Datos_Viscoi
            If oForma.txt_Cliente.Text <> "" Then
                If oDatos.Actualiza_TicketFactura_Cuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, oForma.txt_Cliente.Text, oForma.txt_Nombre.Text, oForma.txt_RFC.Text _
                                                    , oForma.txt_Calle.Text & " E:" & oForma.txt_NumExt.Text & IIf(oForma.txt_NumInt.Text.Length > 0, " I:" & oForma.txt_NumInt.Text, "") _
                                                    , oForma.txt_CP.Text, oForma.txt_Colonia.Text, oForma.txt_Municipio.Text, oForma.txt_Estado.Text, oForma.txt_EMail.Text, txt_OrdenCompra.Text _
                                                    , "", Msj) Then
                    txt_Cliente.Text = oForma.txt_Cliente.Text
                    txt_Nombre.Text = oForma.txt_Nombre.Text
                    txt_RFC.Text = oForma.txt_RFC.Text

                    txt_Calle.Text = oForma.txt_Calle.Text & " E:" & oForma.txt_NumExt.Text & IIf(oForma.txt_NumInt.Text.Length > 0, " I:" & oForma.txt_NumInt.Text, "")
                    txt_CP.Text = oForma.txt_CP.Text
                    txt_Colonia.Text = oForma.txt_Colonia.Text
                    txt_Municipio.Text = oForma.txt_Municipio.Text
                    txt_Estado.Text = oForma.txt_Estado.Text

                    txt_Cliente.Text = txt_Cliente.Text.Trim()
                    txt_Nombre.Text = txt_Nombre.Text.Trim()
                    txt_RFC.Text = txt_RFC.Text.Trim()
                    txt_Calle.Text = txt_Calle.Text.Trim()
                    txt_Colonia.Text = txt_Colonia.Text.Trim()
                    txt_CP.Text = txt_CP.Text.Trim()
                    txt_Municipio.Text = txt_Municipio.Text.Trim()
                    txt_Estado.Text = txt_Estado.Text.Trim()

                    txt_Serie.Text = txt_Serie.Text.Trim()
                    txt_Folio.Text = txt_Folio.Text.Trim()
                    txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                    txt_IVA.Text = txt_IVA.Text.Trim()
                    txt_Total.Text = txt_Total.Text.Trim()

                    txt_Correos.Text = oForma.txt_EMail.Text.Trim()

                    cbo_Regimen.SelectedValue = IIf(txt_RFC.Text.Length > 12, "F", "M")

                    If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                        GridC_1.DataSource = dtLugEntregas
                        If dtLugEntregas.Rows.Count > 0 Then
                            GridV_1.FocusedRowHandle = id_dir
                        End If
                    End If
                Else
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub Imprimir()
        Dim Formularios As Globales.clsFormularios
        Dim ForamtoOC As Object = Nothing
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oFacturas As New Facturas
        Dim Mensaje As String = ""
        Dim oDatos_Supply As New Datos_Viscoi

        Try
            oFacturas.Fill(dtCabecero, dtDetalle, dtOtros)

            Select Case txt_Empresa.Text
                Case "PAPELERA"
                    ForamtoOC = New XtraRep_FacturaCuadros
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Empresa = oFacturas.FacEmpresa
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Direccion = oFacturas.FacDireccion.Replace("|", vbNewLine)
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).LugExpedicion = oFacturas.LugExpedicion
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Fecha = Format(oFacturas.Fecha, "dd/MMM/yyyy")
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Importe = Format(oFacturas.Total, "C2")
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Letra = oFacturas.Letra
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).FacCP = oFacturas.FacCP
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).SW_ImprimirDirecion = chk_Imprimir_Direccion.Checked
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).odsDatosFacturas.DataSource = oFacturas
                Case Else
                    ForamtoOC = New XtraRep_FacturaBase
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Empresa = oFacturas.FacEmpresa
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Direccion = oFacturas.FacDireccion.Replace("|", vbNewLine)
                    TryCast(ForamtoOC, XtraRep_FacturaBase).LugExpedicion = oFacturas.LugExpedicion
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Fecha = Format(oFacturas.Fecha, "dd/MMM/yyyy")
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Importe = Format(oFacturas.Total, "C2")
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Letra = oFacturas.Letra
                    TryCast(ForamtoOC, XtraRep_FacturaBase).FacCP = oFacturas.FacCP
                    TryCast(ForamtoOC, XtraRep_FacturaBase).odsDatosFacturas.DataSource = oFacturas
            End Select
            If Not ForamtoOC Is Nothing Then
                printTool = New DevExpress.XtraReports.UI.ReportPrintTool(ForamtoOC)
                printTool.ShowPreviewDialog()
                If Not IO.Directory.Exists("C:\Windows\Temp\F" & txt_Autonumsuc.Text) Then
                    IO.Directory.CreateDirectory("C:\Windows\Temp\F" & txt_Autonumsuc.Text)
                End If
                ForamtoOC.ExportToPdf("C:\Windows\Temp\F" & txt_Autonumsuc.Text & "\" & txt_Autonumsuc.Text & ".pdf")
            Else
                MessageBox.Show("Formato no registrado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
        oDatos_Supply = Nothing
    End Sub
    Private Sub SendMail(ByVal var_MailUser As String _
                         , ByVal var_MailPass As String _
                         , ByVal var_MailHost As String _
                         , ByVal var_MailPort As String _
                         , ByVal var_MailFrom As String _
                         , ByVal var_MailDestinos As String _
                         , ByVal var_Archivo As String _
                         , ByVal Ticket As String)
        Dim Smtp_Server As New System.Net.Mail.SmtpClient
        Dim e_mail As New System.Net.Mail.MailMessage()
        Dim txt_destinos As New TextBox
        Try
            If var_MailUser <> "" And var_MailPass <> "" _
                And var_MailPort <> "" And var_MailHost <> "" _
                And var_MailFrom <> "" And var_MailDestinos <> "" Then

                Smtp_Server.UseDefaultCredentials = False
                Smtp_Server.Credentials = New Net.NetworkCredential(var_MailUser, var_MailPass)
                Smtp_Server.Port = var_MailPort
                Smtp_Server.EnableSsl = True
                Smtp_Server.Host = var_MailHost

                e_mail = New System.Net.Mail.MailMessage()
                e_mail.From = New System.Net.Mail.MailAddress(var_MailFrom)
                txt_destinos.Multiline = True

                For Each dest As String In var_MailDestinos.Split(";")
                    e_mail.To.Add(dest)
                Next
                'e_mail.CC.Add(txtTo.Text)
                e_mail.IsBodyHtml = True
                Select Case txt_Empresa.Text
                    Case "PAPELERA"
                        e_mail.Subject = "Papelera Del Norte: Envio de comprobante fiscal digital a traves de Internet" & Ticket & ""
                        e_mail.Body = "Estimado cliente"
                        e_mail.Body = e_mail.Body & "<BR>" & "Se adjunta a este e-mail archivo ZIP conteniendo los archivos XML y PDF del comprobante fiscal digital a traves de internet (CFDI) correspondiente al ComprobanteFolio" & Ticket & "."
                        e_mail.Body = e_mail.Body & "<BR>" & "Gracias."
                        e_mail.Body = e_mail.Body & "<BR>" & "Papelera Del Norte De La Laguna, S.A. de C.V."
                    Case Else
                        e_mail.Subject = "VOFICNA - Trimbrador"
                        e_mail.Body = "El sistema VOFICNA Envia factura"
                End Select

                If IO.File.Exists(var_Archivo & ".zip") Then
                    IO.File.Delete(var_Archivo & ".zip")
                End If
                System.IO.Compression.ZipFile.CreateFromDirectory(var_Archivo, var_Archivo & ".zip", System.IO.Compression.CompressionLevel.Optimal, False)

                Dim attachfile As New System.Net.Mail.Attachment(var_Archivo & ".zip")
                e_mail.Attachments.Add(attachfile)

                Smtp_Server.Send(e_mail)
                e_mail.Dispose()
                Smtp_Server.Dispose()

                MessageBox.Show("Se ha enviado a los correos proporcionados.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_Empresa_LostFocus(sender As Object, e As EventArgs) Handles txt_Empresa.LostFocus
        If txt_Empresa.Text = "ICC" Then
            lblOC.Visible = True
            txt_OrdenCompra.Visible = True
        Else
            lblOC.Visible = False
            txt_OrdenCompra.Visible = False
        End If
    End Sub

    Private Sub txt_Folio_TextChanged(sender As Object, e As EventArgs) Handles txt_Folio.TextChanged
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim sw_continuar As Boolean = True
        Dim RegimenFiscal As String = cbo_Regimen.SelectedValue
        Try
            oDatos = New Datos_Viscoi
            Dim Folio As Integer
            If txt_Folio.Text <> "" Then
                Folio = Integer.Parse(txt_Folio.Text)
                If cbo_UsoCFDI.SelectedValue IsNot Nothing Then
                    If RegimenFiscal Is Nothing Then
                        RegimenFiscal = ""
                    End If
                    If Not oDatos.Actualiza_TicketFactura_DatosCFDI(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, RegimenFiscal, cbo_UsoCFDI.SelectedValue, cbo_FormaPago.SelectedValue, Folio, Msj) Then
                        sw_continuar = False
                        MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub
End Class