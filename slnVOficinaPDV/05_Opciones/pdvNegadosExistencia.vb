﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvNegadosExistencia
    Dim _Accion As Boolean
    Dim _Fraccion As Double

    Public Property Fraccion As Double
        Get
            Return _Fraccion
        End Get
        Set(value As Double)
            _Fraccion = value
        End Set
    End Property

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Private Sub pdvFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Try
            _Accion = False
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_negados_motivos("ACTIVO", dtDatos, Msj) Then
                ComboBox1.DataSource = dtDatos
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
            _Fraccion = 1
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Dim articulo As Integer
        Dim precio As Double
        Dim cantidad As Double
        Try
            _Accion = True
            oDatos = New Datos_Viscoi
            If txt_Articulo.Text <> "" Then
                articulo = Integer.Parse(txt_Articulo.Text, Globalization.NumberStyles.Any)
            Else
                articulo = 0
            End If
            If txt_Cantidad.Text <> "" Then
                cantidad = Double.Parse(txt_Cantidad.Text, Globalization.NumberStyles.Any)
            Else
                cantidad = 0
            End If
            If txt_Precio.Text <> "" Then
                precio = Double.Parse(txt_Precio.Text, Globalization.NumberStyles.Any)
            Else
                precio = 0
            End If

            If oDatos.PVTA_Inserta_articulos_negados(Globales.oAmbientes.Id_Empresa _
                                                    , Globales.oAmbientes.Id_Sucursal _
                                                    , articulo _
                                                    , txt_Nombre.Text _
                                                    , precio _
                                                    , cantidad _
                                                    , ComboBox1.SelectedValue _
                                                    , Globales.oAmbientes.oUsuario.Id_usuario _
                                                    , Msj) Then

                MessageBox.Show("Se guardo el registro con exito.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub MostarArticulo()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Dim Precio As Double
        Dim Capacidad As Double

        Try
            oDatos = New Datos_Viscoi
            txt_Cantidad.Text = "1"
            If oDatos.PVTA_Recupera_Articulos("ACTIVO", "", txt_Articulo.Text, "", "", "", "", "", False, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    Precio = dtDatos.Rows(0).Item("precio1")
                    Capacidad = dtDatos.Rows(0).Item("capacidad_operacion")
                    txt_Nombre.Text = dtDatos.Rows(0).Item("nombre")
                    txt_Precio.Text = Format(Precio, "C2")
                    txt_Nombre.Focus()
                Else
                    MessageBox.Show("Articulo no encontrado", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub txt_Articulo_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_Articulo.KeyUp
        Try
            If e.KeyCode = Keys.Enter Then
                Call txt_Articulo_LostFocus(Nothing, Nothing)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub txt_Articulo_LostFocus(sender As Object, e As EventArgs) Handles txt_Articulo.LostFocus
        If txt_Articulo.Text <> "" Then
            Call MostarArticulo()
        End If
    End Sub

    Private Sub pdvFracciones_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Call btn_Cancelar_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btn_Articulos_Click(sender As Object, e As EventArgs) Handles btn_Articulos.Click
        Dim oFrm As SelArticulos
        Try
            oFrm = New SelArticulos
            oFrm.ShowDialog()
            txt_Articulo.Text = oFrm.txt_Articulo.Text
            If txt_Articulo.Text <> "" Then
                Call MostarArticulo()
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class