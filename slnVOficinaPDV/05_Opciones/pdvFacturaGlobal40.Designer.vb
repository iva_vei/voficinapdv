﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvFacturaGlobal40
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.gcDatosCliente = New DevExpress.XtraEditors.GroupControl()
        Me.chk_Imprimir_Direccion = New System.Windows.Forms.CheckBox()
        Me.PnlDatosCliente = New DevExpress.XtraEditors.PanelControl()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_Cliente = New System.Windows.Forms.TextBox()
        Me.btn_CambiarCliente = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_NuevoCliente = New DevExpress.XtraEditors.SimpleButton()
        Me.gcDatosDirFactura = New DevExpress.XtraEditors.GroupControl()
        Me.PnlDatosDirFactura = New DevExpress.XtraEditors.PanelControl()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_Estado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_Municipio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_Colonia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_CP = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Calle = New System.Windows.Forms.TextBox()
        Me.btn_GuardarDir = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_CambiarDir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcDatosFacturacion = New DevExpress.XtraEditors.GroupControl()
        Me.cbo_FormaPago = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.cbo_Regimen = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_Folio = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_Serie = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_Total = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_IVA = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_SubTotal = New System.Windows.Forms.TextBox()
        Me.cbo_UsoCFDI = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.gcDatosEnvio = New DevExpress.XtraEditors.GroupControl()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.datFEchaEntrega = New System.Windows.Forms.DateTimePicker()
        Me.btn_NuevoDireccion = New DevExpress.XtraEditors.SimpleButton()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1_IdDir = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Calle = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_NoExt = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_CP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Colonia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Municipio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Estado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.gcDatosCorreo = New DevExpress.XtraEditors.GroupControl()
        Me.lblOC = New System.Windows.Forms.Label()
        Me.txt_OrdenCompra = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_Correos = New System.Windows.Forms.TextBox()
        Me.gcDatosTicket = New DevExpress.XtraEditors.GroupControl()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.dat_FechaFin = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.dat_FechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.btn_Buscar = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Autonumsuc = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cbo_Sucursal = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_2 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col2_Autonumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Cantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Subtotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Total = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.gcDatosCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosCliente.SuspendLayout()
        CType(Me.PnlDatosCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlDatosCliente.SuspendLayout()
        CType(Me.gcDatosDirFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosDirFactura.SuspendLayout()
        CType(Me.PnlDatosDirFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlDatosDirFactura.SuspendLayout()
        CType(Me.gcDatosFacturacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosFacturacion.SuspendLayout()
        CType(Me.gcDatosEnvio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosEnvio.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcDatosCorreo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosCorreo.SuspendLayout()
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosTicket.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcDatosCliente
        '
        Me.gcDatosCliente.Controls.Add(Me.chk_Imprimir_Direccion)
        Me.gcDatosCliente.Controls.Add(Me.PnlDatosCliente)
        Me.gcDatosCliente.Controls.Add(Me.btn_CambiarCliente)
        Me.gcDatosCliente.Controls.Add(Me.btn_NuevoCliente)
        Me.gcDatosCliente.Location = New System.Drawing.Point(0, 92)
        Me.gcDatosCliente.Name = "gcDatosCliente"
        Me.gcDatosCliente.Size = New System.Drawing.Size(824, 65)
        Me.gcDatosCliente.TabIndex = 1
        Me.gcDatosCliente.Text = "DATOS DEL CLIENTE"
        Me.gcDatosCliente.Visible = False
        '
        'chk_Imprimir_Direccion
        '
        Me.chk_Imprimir_Direccion.AutoSize = True
        Me.chk_Imprimir_Direccion.BackColor = System.Drawing.Color.Transparent
        Me.chk_Imprimir_Direccion.Location = New System.Drawing.Point(368, 4)
        Me.chk_Imprimir_Direccion.Name = "chk_Imprimir_Direccion"
        Me.chk_Imprimir_Direccion.Size = New System.Drawing.Size(110, 17)
        Me.chk_Imprimir_Direccion.TabIndex = 24
        Me.chk_Imprimir_Direccion.Text = "Imprimir Dirección"
        Me.chk_Imprimir_Direccion.UseVisualStyleBackColor = False
        '
        'PnlDatosCliente
        '
        Me.PnlDatosCliente.Controls.Add(Me.Label3)
        Me.PnlDatosCliente.Controls.Add(Me.txt_RFC)
        Me.PnlDatosCliente.Controls.Add(Me.Label2)
        Me.PnlDatosCliente.Controls.Add(Me.txt_Nombre)
        Me.PnlDatosCliente.Controls.Add(Me.Label1)
        Me.PnlDatosCliente.Controls.Add(Me.txt_Cliente)
        Me.PnlDatosCliente.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlDatosCliente.Location = New System.Drawing.Point(2, 23)
        Me.PnlDatosCliente.Name = "PnlDatosCliente"
        Me.PnlDatosCliente.Size = New System.Drawing.Size(820, 40)
        Me.PnlDatosCliente.TabIndex = 33
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(692, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 24)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "RFC"
        '
        'txt_RFC
        '
        Me.txt_RFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(635, 30)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(180, 29)
        Me.txt_RFC.TabIndex = 2
        Me.txt_RFC.Text = "XXXX-000000-XXX"
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(273, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 24)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Nombre"
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Nombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(116, 30)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(1812, 29)
        Me.txt_Nombre.TabIndex = 1
        Me.txt_Nombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 24)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Cliente"
        '
        'txt_Cliente
        '
        Me.txt_Cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cliente.Location = New System.Drawing.Point(8, 30)
        Me.txt_Cliente.Name = "txt_Cliente"
        Me.txt_Cliente.Size = New System.Drawing.Size(100, 29)
        Me.txt_Cliente.TabIndex = 0
        Me.txt_Cliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btn_CambiarCliente
        '
        Me.btn_CambiarCliente.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_CambiarCliente.Location = New System.Drawing.Point(187, 0)
        Me.btn_CambiarCliente.Name = "btn_CambiarCliente"
        Me.btn_CambiarCliente.Size = New System.Drawing.Size(75, 23)
        Me.btn_CambiarCliente.TabIndex = 0
        Me.btn_CambiarCliente.Text = "Cambiar"
        '
        'btn_NuevoCliente
        '
        Me.btn_NuevoCliente.Location = New System.Drawing.Point(267, 0)
        Me.btn_NuevoCliente.Name = "btn_NuevoCliente"
        Me.btn_NuevoCliente.Size = New System.Drawing.Size(75, 23)
        Me.btn_NuevoCliente.TabIndex = 23
        Me.btn_NuevoCliente.Text = "Nuevo"
        '
        'gcDatosDirFactura
        '
        Me.gcDatosDirFactura.Controls.Add(Me.PnlDatosDirFactura)
        Me.gcDatosDirFactura.Controls.Add(Me.btn_GuardarDir)
        Me.gcDatosDirFactura.Controls.Add(Me.btn_CambiarDir)
        Me.gcDatosDirFactura.Location = New System.Drawing.Point(2, 184)
        Me.gcDatosDirFactura.Name = "gcDatosDirFactura"
        Me.gcDatosDirFactura.Size = New System.Drawing.Size(327, 280)
        Me.gcDatosDirFactura.TabIndex = 2
        Me.gcDatosDirFactura.Text = "DIRECCION DE FACTURACION"
        Me.gcDatosDirFactura.Visible = False
        '
        'PnlDatosDirFactura
        '
        Me.PnlDatosDirFactura.Controls.Add(Me.Label9)
        Me.PnlDatosDirFactura.Controls.Add(Me.txt_Estado)
        Me.PnlDatosDirFactura.Controls.Add(Me.Label8)
        Me.PnlDatosDirFactura.Controls.Add(Me.txt_Municipio)
        Me.PnlDatosDirFactura.Controls.Add(Me.Label7)
        Me.PnlDatosDirFactura.Controls.Add(Me.txt_Colonia)
        Me.PnlDatosDirFactura.Controls.Add(Me.Label6)
        Me.PnlDatosDirFactura.Controls.Add(Me.txt_CP)
        Me.PnlDatosDirFactura.Controls.Add(Me.Label4)
        Me.PnlDatosDirFactura.Controls.Add(Me.txt_Calle)
        Me.PnlDatosDirFactura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlDatosDirFactura.Location = New System.Drawing.Point(2, 23)
        Me.PnlDatosDirFactura.Name = "PnlDatosDirFactura"
        Me.PnlDatosDirFactura.Size = New System.Drawing.Size(323, 255)
        Me.PnlDatosDirFactura.TabIndex = 33
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(112, 186)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 24)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Estado"
        '
        'txt_Estado
        '
        Me.txt_Estado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Estado.Location = New System.Drawing.Point(8, 215)
        Me.txt_Estado.Name = "txt_Estado"
        Me.txt_Estado.Size = New System.Drawing.Size(300, 29)
        Me.txt_Estado.TabIndex = 4
        Me.txt_Estado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(100, 126)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 24)
        Me.Label8.TabIndex = 38
        Me.Label8.Text = "Municipio"
        '
        'txt_Municipio
        '
        Me.txt_Municipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Municipio.Location = New System.Drawing.Point(8, 153)
        Me.txt_Municipio.Name = "txt_Municipio"
        Me.txt_Municipio.Size = New System.Drawing.Size(300, 29)
        Me.txt_Municipio.TabIndex = 3
        Me.txt_Municipio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(182, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 24)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "Colonia"
        '
        'txt_Colonia
        '
        Me.txt_Colonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Colonia.Location = New System.Drawing.Point(118, 91)
        Me.txt_Colonia.Name = "txt_Colonia"
        Me.txt_Colonia.Size = New System.Drawing.Size(190, 29)
        Me.txt_Colonia.TabIndex = 2
        Me.txt_Colonia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(33, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 24)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "C. P."
        '
        'txt_CP
        '
        Me.txt_CP.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CP.Location = New System.Drawing.Point(8, 91)
        Me.txt_CP.Name = "txt_CP"
        Me.txt_CP.Size = New System.Drawing.Size(100, 29)
        Me.txt_CP.TabIndex = 1
        Me.txt_CP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(114, 4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 24)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Calle"
        '
        'txt_Calle
        '
        Me.txt_Calle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Calle.Location = New System.Drawing.Point(8, 31)
        Me.txt_Calle.Name = "txt_Calle"
        Me.txt_Calle.Size = New System.Drawing.Size(300, 29)
        Me.txt_Calle.TabIndex = 0
        Me.txt_Calle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btn_GuardarDir
        '
        Me.btn_GuardarDir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_GuardarDir.Location = New System.Drawing.Point(251, 0)
        Me.btn_GuardarDir.Name = "btn_GuardarDir"
        Me.btn_GuardarDir.Size = New System.Drawing.Size(75, 23)
        Me.btn_GuardarDir.TabIndex = 32
        Me.btn_GuardarDir.Text = "Guardar"
        '
        'btn_CambiarDir
        '
        Me.btn_CambiarDir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_CambiarDir.Location = New System.Drawing.Point(174, 0)
        Me.btn_CambiarDir.Name = "btn_CambiarDir"
        Me.btn_CambiarDir.Size = New System.Drawing.Size(75, 23)
        Me.btn_CambiarDir.TabIndex = 31
        Me.btn_CambiarDir.Text = "Cambiar"
        '
        'gcDatosFacturacion
        '
        Me.gcDatosFacturacion.Controls.Add(Me.cbo_FormaPago)
        Me.gcDatosFacturacion.Controls.Add(Me.Label18)
        Me.gcDatosFacturacion.Controls.Add(Me.cbo_Regimen)
        Me.gcDatosFacturacion.Controls.Add(Me.Label17)
        Me.gcDatosFacturacion.Controls.Add(Me.Label16)
        Me.gcDatosFacturacion.Controls.Add(Me.txt_Folio)
        Me.gcDatosFacturacion.Controls.Add(Me.Label15)
        Me.gcDatosFacturacion.Controls.Add(Me.txt_Serie)
        Me.gcDatosFacturacion.Controls.Add(Me.Label12)
        Me.gcDatosFacturacion.Controls.Add(Me.txt_Total)
        Me.gcDatosFacturacion.Controls.Add(Me.Label13)
        Me.gcDatosFacturacion.Controls.Add(Me.txt_IVA)
        Me.gcDatosFacturacion.Controls.Add(Me.Label14)
        Me.gcDatosFacturacion.Controls.Add(Me.txt_SubTotal)
        Me.gcDatosFacturacion.Controls.Add(Me.cbo_UsoCFDI)
        Me.gcDatosFacturacion.Controls.Add(Me.Label10)
        Me.gcDatosFacturacion.Location = New System.Drawing.Point(828, 0)
        Me.gcDatosFacturacion.Name = "gcDatosFacturacion"
        Me.gcDatosFacturacion.Size = New System.Drawing.Size(277, 464)
        Me.gcDatosFacturacion.TabIndex = 4
        Me.gcDatosFacturacion.Text = "FACTURACION"
        '
        'cbo_FormaPago
        '
        Me.cbo_FormaPago.DisplayMember = "descripcion"
        Me.cbo_FormaPago.Enabled = False
        Me.cbo_FormaPago.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_FormaPago.FormattingEnabled = True
        Me.cbo_FormaPago.Location = New System.Drawing.Point(12, 115)
        Me.cbo_FormaPago.Name = "cbo_FormaPago"
        Me.cbo_FormaPago.Size = New System.Drawing.Size(250, 31)
        Me.cbo_FormaPago.TabIndex = 1
        Me.cbo_FormaPago.ValueMember = "clave"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(87, 88)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(114, 24)
        Me.Label18.TabIndex = 43
        Me.Label18.Text = "Forma Pago"
        '
        'cbo_Regimen
        '
        Me.cbo_Regimen.DisplayMember = "descripcion"
        Me.cbo_Regimen.Enabled = False
        Me.cbo_Regimen.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Regimen.FormattingEnabled = True
        Me.cbo_Regimen.Location = New System.Drawing.Point(12, 54)
        Me.cbo_Regimen.Name = "cbo_Regimen"
        Me.cbo_Regimen.Size = New System.Drawing.Size(250, 31)
        Me.cbo_Regimen.TabIndex = 0
        Me.cbo_Regimen.ValueMember = "clave"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(76, 29)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(141, 24)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Regimen Fiscal"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(174, 154)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(52, 24)
        Me.Label16.TabIndex = 40
        Me.Label16.Text = "Folio"
        '
        'txt_Folio
        '
        Me.txt_Folio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Folio.Location = New System.Drawing.Point(142, 181)
        Me.txt_Folio.Name = "txt_Folio"
        Me.txt_Folio.Size = New System.Drawing.Size(120, 29)
        Me.txt_Folio.TabIndex = 3
        Me.txt_Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(39, 154)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(54, 24)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "Serie"
        '
        'txt_Serie
        '
        Me.txt_Serie.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Serie.Location = New System.Drawing.Point(12, 181)
        Me.txt_Serie.Name = "txt_Serie"
        Me.txt_Serie.Size = New System.Drawing.Size(120, 29)
        Me.txt_Serie.TabIndex = 2
        Me.txt_Serie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(109, 399)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(51, 24)
        Me.Label12.TabIndex = 36
        Me.Label12.Text = "Total"
        '
        'txt_Total
        '
        Me.txt_Total.Enabled = False
        Me.txt_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Location = New System.Drawing.Point(11, 426)
        Me.txt_Total.Name = "txt_Total"
        Me.txt_Total.Size = New System.Drawing.Size(251, 29)
        Me.txt_Total.TabIndex = 7
        Me.txt_Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(116, 340)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(40, 24)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "IVA"
        '
        'txt_IVA
        '
        Me.txt_IVA.Enabled = False
        Me.txt_IVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IVA.Location = New System.Drawing.Point(13, 367)
        Me.txt_IVA.Name = "txt_IVA"
        Me.txt_IVA.Size = New System.Drawing.Size(249, 29)
        Me.txt_IVA.TabIndex = 6
        Me.txt_IVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(101, 284)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(85, 24)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "SubTotal"
        '
        'txt_SubTotal
        '
        Me.txt_SubTotal.Enabled = False
        Me.txt_SubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SubTotal.Location = New System.Drawing.Point(13, 311)
        Me.txt_SubTotal.Name = "txt_SubTotal"
        Me.txt_SubTotal.Size = New System.Drawing.Size(249, 29)
        Me.txt_SubTotal.TabIndex = 5
        Me.txt_SubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cbo_UsoCFDI
        '
        Me.cbo_UsoCFDI.DisplayMember = "descripcion"
        Me.cbo_UsoCFDI.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_UsoCFDI.FormattingEnabled = True
        Me.cbo_UsoCFDI.Location = New System.Drawing.Point(12, 247)
        Me.cbo_UsoCFDI.Name = "cbo_UsoCFDI"
        Me.cbo_UsoCFDI.Size = New System.Drawing.Size(250, 31)
        Me.cbo_UsoCFDI.TabIndex = 4
        Me.cbo_UsoCFDI.ValueMember = "clave"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(80, 220)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(121, 24)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Uso del CFDI"
        '
        'gcDatosEnvio
        '
        Me.gcDatosEnvio.Controls.Add(Me.Label5)
        Me.gcDatosEnvio.Controls.Add(Me.datFEchaEntrega)
        Me.gcDatosEnvio.Controls.Add(Me.btn_NuevoDireccion)
        Me.gcDatosEnvio.Controls.Add(Me.GridC_1)
        Me.gcDatosEnvio.Location = New System.Drawing.Point(335, 184)
        Me.gcDatosEnvio.Name = "gcDatosEnvio"
        Me.gcDatosEnvio.Size = New System.Drawing.Size(489, 280)
        Me.gcDatosEnvio.TabIndex = 3
        Me.gcDatosEnvio.Text = "DIRECCION DE ENVIO DE MERCANCIA (SELECCIONE UNA)"
        Me.gcDatosEnvio.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(17, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 20)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "Fecha Entrega"
        Me.Label5.Visible = False
        '
        'datFEchaEntrega
        '
        Me.datFEchaEntrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.datFEchaEntrega.Location = New System.Drawing.Point(179, 30)
        Me.datFEchaEntrega.Name = "datFEchaEntrega"
        Me.datFEchaEntrega.Size = New System.Drawing.Size(298, 26)
        Me.datFEchaEntrega.TabIndex = 1
        '
        'btn_NuevoDireccion
        '
        Me.btn_NuevoDireccion.Location = New System.Drawing.Point(332, 0)
        Me.btn_NuevoDireccion.Name = "btn_NuevoDireccion"
        Me.btn_NuevoDireccion.Size = New System.Drawing.Size(75, 23)
        Me.btn_NuevoDireccion.TabIndex = 0
        Me.btn_NuevoDireccion.Text = "Nuevo"
        '
        'GridC_1
        '
        Me.GridC_1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridC_1.Location = New System.Drawing.Point(5, 68)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(465, 199)
        Me.GridC_1.TabIndex = 2
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col1_IdDir, Me.col1_Calle, Me.col1_NoExt, Me.col1_CP, Me.col1_Colonia, Me.col1_Municipio, Me.col1_Estado})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col1_IdDir
        '
        Me.col1_IdDir.Caption = "Id"
        Me.col1_IdDir.FieldName = "id_dir"
        Me.col1_IdDir.Name = "col1_IdDir"
        Me.col1_IdDir.OptionsColumn.AllowEdit = False
        Me.col1_IdDir.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdDir.Visible = True
        Me.col1_IdDir.VisibleIndex = 0
        '
        'col1_Calle
        '
        Me.col1_Calle.Caption = "Calle"
        Me.col1_Calle.FieldName = "direccion"
        Me.col1_Calle.Name = "col1_Calle"
        Me.col1_Calle.OptionsColumn.AllowEdit = False
        Me.col1_Calle.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Calle.Visible = True
        Me.col1_Calle.VisibleIndex = 1
        '
        'col1_NoExt
        '
        Me.col1_NoExt.Caption = "No Ext"
        Me.col1_NoExt.FieldName = "numext"
        Me.col1_NoExt.Name = "col1_NoExt"
        Me.col1_NoExt.OptionsColumn.AllowEdit = False
        Me.col1_NoExt.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_NoExt.Visible = True
        Me.col1_NoExt.VisibleIndex = 2
        '
        'col1_CP
        '
        Me.col1_CP.Caption = "CP"
        Me.col1_CP.FieldName = "codigo_postal"
        Me.col1_CP.Name = "col1_CP"
        Me.col1_CP.OptionsColumn.AllowEdit = False
        Me.col1_CP.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CP.Visible = True
        Me.col1_CP.VisibleIndex = 3
        '
        'col1_Colonia
        '
        Me.col1_Colonia.Caption = "Colonia"
        Me.col1_Colonia.FieldName = "colonia"
        Me.col1_Colonia.Name = "col1_Colonia"
        Me.col1_Colonia.OptionsColumn.AllowEdit = False
        Me.col1_Colonia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Colonia.Visible = True
        Me.col1_Colonia.VisibleIndex = 4
        '
        'col1_Municipio
        '
        Me.col1_Municipio.Caption = "Municipio"
        Me.col1_Municipio.FieldName = "municipio"
        Me.col1_Municipio.Name = "col1_Municipio"
        Me.col1_Municipio.OptionsColumn.AllowEdit = False
        Me.col1_Municipio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Municipio.Visible = True
        Me.col1_Municipio.VisibleIndex = 5
        '
        'col1_Estado
        '
        Me.col1_Estado.Caption = "Estado"
        Me.col1_Estado.FieldName = "estado"
        Me.col1_Estado.Name = "col1_Estado"
        Me.col1_Estado.OptionsColumn.AllowEdit = False
        Me.col1_Estado.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Estado.Visible = True
        Me.col1_Estado.VisibleIndex = 6
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(333, 81)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 1
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(761, 81)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 2
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'gcDatosCorreo
        '
        Me.gcDatosCorreo.Controls.Add(Me.lblOC)
        Me.gcDatosCorreo.Controls.Add(Me.txt_OrdenCompra)
        Me.gcDatosCorreo.Controls.Add(Me.Label11)
        Me.gcDatosCorreo.Controls.Add(Me.txt_Correos)
        Me.gcDatosCorreo.Controls.Add(Me.btn_Cancelar)
        Me.gcDatosCorreo.Controls.Add(Me.btn_Aceptar)
        Me.gcDatosCorreo.Location = New System.Drawing.Point(2, 470)
        Me.gcDatosCorreo.Name = "gcDatosCorreo"
        Me.gcDatosCorreo.Size = New System.Drawing.Size(1103, 128)
        Me.gcDatosCorreo.TabIndex = 5
        Me.gcDatosCorreo.Text = "CORREO PARA ENVIAR FACTURA (PDF y XML) "
        '
        'lblOC
        '
        Me.lblOC.AutoSize = True
        Me.lblOC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOC.Location = New System.Drawing.Point(4, 88)
        Me.lblOC.Name = "lblOC"
        Me.lblOC.Size = New System.Drawing.Size(53, 24)
        Me.lblOC.TabIndex = 27
        Me.lblOC.Text = "O. C."
        '
        'txt_OrdenCompra
        '
        Me.txt_OrdenCompra.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_OrdenCompra.Location = New System.Drawing.Point(110, 85)
        Me.txt_OrdenCompra.Name = "txt_OrdenCompra"
        Me.txt_OrdenCompra.Size = New System.Drawing.Size(200, 29)
        Me.txt_OrdenCompra.TabIndex = 26
        Me.txt_OrdenCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(12, 58)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(383, 16)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "* Puedes agregar mas correos mientras vayan separados por ;"
        '
        'txt_Correos
        '
        Me.txt_Correos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Correos.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Correos.Location = New System.Drawing.Point(12, 26)
        Me.txt_Correos.Name = "txt_Correos"
        Me.txt_Correos.Size = New System.Drawing.Size(1082, 29)
        Me.txt_Correos.TabIndex = 0
        Me.txt_Correos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gcDatosTicket
        '
        Me.gcDatosTicket.Controls.Add(Me.Label23)
        Me.gcDatosTicket.Controls.Add(Me.dat_FechaFin)
        Me.gcDatosTicket.Controls.Add(Me.Label22)
        Me.gcDatosTicket.Controls.Add(Me.dat_FechaInicio)
        Me.gcDatosTicket.Controls.Add(Me.btn_Buscar)
        Me.gcDatosTicket.Controls.Add(Me.txt_Autonumsuc)
        Me.gcDatosTicket.Controls.Add(Me.Label21)
        Me.gcDatosTicket.Controls.Add(Me.Label20)
        Me.gcDatosTicket.Controls.Add(Me.cbo_Sucursal)
        Me.gcDatosTicket.Controls.Add(Me.Label19)
        Me.gcDatosTicket.Controls.Add(Me.txt_Empresa)
        Me.gcDatosTicket.Location = New System.Drawing.Point(0, 0)
        Me.gcDatosTicket.Name = "gcDatosTicket"
        Me.gcDatosTicket.Size = New System.Drawing.Size(824, 92)
        Me.gcDatosTicket.TabIndex = 0
        Me.gcDatosTicket.Text = "Datos del Ticket"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(337, 63)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(37, 24)
        Me.Label23.TabIndex = 30
        Me.Label23.Text = "Fin"
        '
        'dat_FechaFin
        '
        Me.dat_FechaFin.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaFin.Location = New System.Drawing.Point(418, 58)
        Me.dat_FechaFin.Name = "dat_FechaFin"
        Me.dat_FechaFin.Size = New System.Drawing.Size(131, 30)
        Me.dat_FechaFin.TabIndex = 3
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(337, 31)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(54, 24)
        Me.Label22.TabIndex = 28
        Me.Label22.Text = "Inicio"
        '
        'dat_FechaInicio
        '
        Me.dat_FechaInicio.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaInicio.Location = New System.Drawing.Point(418, 26)
        Me.dat_FechaInicio.Name = "dat_FechaInicio"
        Me.dat_FechaInicio.Size = New System.Drawing.Size(131, 30)
        Me.dat_FechaInicio.TabIndex = 2
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Location = New System.Drawing.Point(557, 29)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(50, 50)
        Me.btn_Buscar.TabIndex = 4
        Me.btn_Buscar.Text = "Buscar"
        '
        'txt_Autonumsuc
        '
        Me.txt_Autonumsuc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Autonumsuc.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Autonumsuc.Location = New System.Drawing.Point(617, 54)
        Me.txt_Autonumsuc.Name = "txt_Autonumsuc"
        Me.txt_Autonumsuc.Size = New System.Drawing.Size(200, 29)
        Me.txt_Autonumsuc.TabIndex = 5
        Me.txt_Autonumsuc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(668, 23)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(117, 24)
        Me.Label21.TabIndex = 23
        Me.Label21.Text = "Autonumsuc"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(8, 59)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(83, 24)
        Me.Label20.TabIndex = 22
        Me.Label20.Text = "Sucursal"
        '
        'cbo_Sucursal
        '
        Me.cbo_Sucursal.DisplayMember = "id_sucursal"
        Me.cbo_Sucursal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Sucursal.FormattingEnabled = True
        Me.cbo_Sucursal.Location = New System.Drawing.Point(99, 57)
        Me.cbo_Sucursal.Name = "cbo_Sucursal"
        Me.cbo_Sucursal.Size = New System.Drawing.Size(200, 27)
        Me.cbo_Sucursal.TabIndex = 1
        Me.cbo_Sucursal.ValueMember = "id_sucursal"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(10, 26)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(86, 24)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "Empresa"
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa.Location = New System.Drawing.Point(99, 26)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(200, 29)
        Me.txt_Empresa.TabIndex = 0
        Me.txt_Empresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.GridC_2)
        Me.GroupControl1.Location = New System.Drawing.Point(0, 92)
        Me.GroupControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(823, 372)
        Me.GroupControl1.TabIndex = 6
        Me.GroupControl1.Text = "Detalle Factura Global"
        '
        'GridC_2
        '
        Me.GridC_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_2.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_2.Location = New System.Drawing.Point(2, 23)
        Me.GridC_2.MainView = Me.GridV_2
        Me.GridC_2.Margin = New System.Windows.Forms.Padding(2)
        Me.GridC_2.Name = "GridC_2"
        Me.GridC_2.Size = New System.Drawing.Size(819, 347)
        Me.GridC_2.TabIndex = 0
        Me.GridC_2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_2})
        '
        'GridV_2
        '
        Me.GridV_2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col2_Autonumero, Me.col2_Cantidad, Me.col2_Subtotal, Me.col2_Total})
        Me.GridV_2.DetailHeight = 284
        Me.GridV_2.GridControl = Me.GridC_2
        Me.GridV_2.Name = "GridV_2"
        '
        'col2_Autonumero
        '
        Me.col2_Autonumero.Caption = "AutoNumSuc"
        Me.col2_Autonumero.FieldName = "concepto"
        Me.col2_Autonumero.MinWidth = 19
        Me.col2_Autonumero.Name = "col2_Autonumero"
        Me.col2_Autonumero.OptionsColumn.AllowEdit = False
        Me.col2_Autonumero.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Autonumero.Visible = True
        Me.col2_Autonumero.VisibleIndex = 0
        Me.col2_Autonumero.Width = 70
        '
        'col2_Cantidad
        '
        Me.col2_Cantidad.Caption = "Cantidad"
        Me.col2_Cantidad.FieldName = "cantidad"
        Me.col2_Cantidad.MinWidth = 19
        Me.col2_Cantidad.Name = "col2_Cantidad"
        Me.col2_Cantidad.OptionsColumn.AllowEdit = False
        Me.col2_Cantidad.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Cantidad.Visible = True
        Me.col2_Cantidad.VisibleIndex = 1
        Me.col2_Cantidad.Width = 70
        '
        'col2_Subtotal
        '
        Me.col2_Subtotal.Caption = "SubTotal"
        Me.col2_Subtotal.DisplayFormat.FormatString = "C2"
        Me.col2_Subtotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col2_Subtotal.FieldName = "subtotal"
        Me.col2_Subtotal.MinWidth = 19
        Me.col2_Subtotal.Name = "col2_Subtotal"
        Me.col2_Subtotal.OptionsColumn.AllowEdit = False
        Me.col2_Subtotal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Subtotal.Visible = True
        Me.col2_Subtotal.VisibleIndex = 2
        Me.col2_Subtotal.Width = 70
        '
        'col2_Total
        '
        Me.col2_Total.Caption = "Total"
        Me.col2_Total.DisplayFormat.FormatString = "C2"
        Me.col2_Total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col2_Total.FieldName = "importe"
        Me.col2_Total.MinWidth = 19
        Me.col2_Total.Name = "col2_Total"
        Me.col2_Total.OptionsColumn.AllowEdit = False
        Me.col2_Total.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Total.Visible = True
        Me.col2_Total.VisibleIndex = 3
        Me.col2_Total.Width = 70
        '
        'pdvFacturaGlobal40
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1106, 601)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.gcDatosEnvio)
        Me.Controls.Add(Me.gcDatosCorreo)
        Me.Controls.Add(Me.gcDatosFacturacion)
        Me.Controls.Add(Me.gcDatosDirFactura)
        Me.Controls.Add(Me.gcDatosCliente)
        Me.Controls.Add(Me.gcDatosTicket)
        Me.KeyPreview = True
        Me.MaximumSize = New System.Drawing.Size(1122, 640)
        Me.MinimumSize = New System.Drawing.Size(1122, 640)
        Me.Name = "pdvFacturaGlobal40"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Facturar Ticket"
        CType(Me.gcDatosCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosCliente.ResumeLayout(False)
        Me.gcDatosCliente.PerformLayout()
        CType(Me.PnlDatosCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlDatosCliente.ResumeLayout(False)
        Me.PnlDatosCliente.PerformLayout()
        CType(Me.gcDatosDirFactura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosDirFactura.ResumeLayout(False)
        CType(Me.PnlDatosDirFactura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlDatosDirFactura.ResumeLayout(False)
        Me.PnlDatosDirFactura.PerformLayout()
        CType(Me.gcDatosFacturacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosFacturacion.ResumeLayout(False)
        Me.gcDatosFacturacion.PerformLayout()
        CType(Me.gcDatosEnvio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosEnvio.ResumeLayout(False)
        Me.gcDatosEnvio.PerformLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcDatosCorreo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosCorreo.ResumeLayout(False)
        Me.gcDatosCorreo.PerformLayout()
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosTicket.ResumeLayout(False)
        Me.gcDatosTicket.PerformLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1_Calle As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_NoExt As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_CP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Colonia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Municipio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Estado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDatosTicket As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label19 As Label
    Friend WithEvents txt_Empresa As TextBox
    Friend WithEvents txt_Autonumsuc As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents cbo_Sucursal As ComboBox
    Friend WithEvents btn_Buscar As DevExpress.XtraEditors.SimpleButton
    Private WithEvents gcDatosCliente As DevExpress.XtraEditors.GroupControl
    Private WithEvents gcDatosDirFactura As DevExpress.XtraEditors.GroupControl
    Private WithEvents gcDatosFacturacion As DevExpress.XtraEditors.GroupControl
    Private WithEvents gcDatosEnvio As DevExpress.XtraEditors.GroupControl
    Private WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Private WithEvents btn_Aceptar As Button
    Private WithEvents btn_Cancelar As Button
    Private WithEvents Label12 As Label
    Private WithEvents txt_Total As TextBox
    Private WithEvents Label13 As Label
    Private WithEvents txt_IVA As TextBox
    Private WithEvents Label14 As Label
    Private WithEvents txt_SubTotal As TextBox
    Private WithEvents cbo_UsoCFDI As ComboBox
    Private WithEvents Label10 As Label
    Private WithEvents gcDatosCorreo As DevExpress.XtraEditors.GroupControl
    Private WithEvents Label11 As Label
    Private WithEvents txt_Correos As TextBox
    Private WithEvents btn_NuevoCliente As DevExpress.XtraEditors.SimpleButton
    Private WithEvents btn_GuardarDir As DevExpress.XtraEditors.SimpleButton
    Private WithEvents btn_CambiarDir As DevExpress.XtraEditors.SimpleButton
    Private WithEvents Label16 As Label
    Private WithEvents txt_Folio As TextBox
    Private WithEvents Label15 As Label
    Private WithEvents txt_Serie As TextBox
    Private WithEvents cbo_Regimen As ComboBox
    Private WithEvents Label17 As Label
    Private WithEvents cbo_FormaPago As ComboBox
    Private WithEvents Label18 As Label
    Friend WithEvents PnlDatosDirFactura As DevExpress.XtraEditors.PanelControl
    Private WithEvents Label9 As Label
    Private WithEvents txt_Estado As TextBox
    Private WithEvents Label8 As Label
    Private WithEvents txt_Municipio As TextBox
    Private WithEvents Label7 As Label
    Private WithEvents txt_Colonia As TextBox
    Private WithEvents Label6 As Label
    Private WithEvents txt_CP As TextBox
    Private WithEvents Label4 As Label
    Private WithEvents txt_Calle As TextBox
    Private WithEvents btn_CambiarCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PnlDatosCliente As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label3 As Label
    Private WithEvents txt_RFC As TextBox
    Friend WithEvents Label2 As Label
    Private WithEvents txt_Nombre As TextBox
    Friend WithEvents Label1 As Label
    Private WithEvents txt_Cliente As TextBox
    Private WithEvents btn_NuevoDireccion As DevExpress.XtraEditors.SimpleButton
    Private WithEvents Label5 As Label
    Friend WithEvents datFEchaEntrega As DateTimePicker
    Friend WithEvents col1_IdDir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblOC As Label
    Friend WithEvents txt_OrdenCompra As TextBox
    Friend WithEvents chk_Imprimir_Direccion As CheckBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col2_Autonumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Cantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Subtotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Total As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label23 As Label
    Friend WithEvents dat_FechaFin As DateTimePicker
    Friend WithEvents Label22 As Label
    Friend WithEvents dat_FechaInicio As DateTimePicker
End Class
