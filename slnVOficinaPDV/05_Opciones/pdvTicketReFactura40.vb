﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvTicketReFactura40

    Private Sub pdvTicketReimprimir_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatosSuc As DataTable = Nothing
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Dim PDatCaja As String = ""

        Try
            oDatos = New Datos_Viscoi
            Dim pd As New Printing.PrintDocument
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer
            txt_Empresa.Text = Globales.oAmbientes.Id_Empresa
            If oDatos.PVTA_Recupera_Sucursales(txt_Empresa.Text, "", dtDatosSuc, Msj) Then
                cbo_Sucursal.DataSource = dtDatosSuc
                cbo_Sucursal.SelectedIndex = 0
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next
            cbo_Sucursal.Text = PDatSucursal
            If oDatos.PVTA_Recupera_Cajeros(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "", Globales.oAmbientes.oUsuario.Id_usuario, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_Cajero.Text = dtDatos.Rows(0).Item("cajero")


                    If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "TODAS", dtDatos, Msj) Then
                        If dtDatos.Rows.Count > 0 Then
                            txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                            txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper

                            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                                lstImpresoras.Enabled = True
                            Else
                                lstImpresoras.Enabled = False
                            End If
                            If dtDatos.Rows(0).Item("actividad").ToString.ToUpper = "ENUSO" Then
                                ''dtDatos.Rows(0).Item("corte").ToString()
                                Me.Text = "Refacturar Venta"

                            Else
                                MessageBox.Show("Caja(" & txt_Caja.Text & ") esta cerrada, realice apertura de caja primero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Me.Close()
                            End If
                        Else
                            MessageBox.Show("Caja(" & txt_Caja.Text & ") no esta registrada para Sucursal(" & Globales.oAmbientes.Id_Sucursal & ").", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Me.Close()
                        End If
                    Else
                        MessageBox.Show("Caja(" & txt_Caja.Text & ") no esta registrada.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Me.Close()
                    End If
                Else
                    MessageBox.Show("Usuario no es cajero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.Close()
                End If
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        Try
            If txt_Autonumsuc.Text <> "" Then
                MostrarTicket(txt_FormatoTicket.Text)
            Else
                MessageBox.Show("Debes teclar un Folio para reimprimir", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub MostrarTicket(ByVal Formato As String)
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            txt_Empresa.ReadOnly = False
            txt_Autonumsuc.ReadOnly = False
            cbo_Sucursal.Enabled = True

            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, DevExpress.XtraBars.Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Tag
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0

        Dim Total As Double
        ''Dim PagoContado As Double
        ''Dim PagoDE As Double
        ''Dim PagoTotal As Double
        Try
            Select Case tag.ToUpper
                Case "TERMINAR"
                    Call Terminar()

                Case "LIMPIAR"
                    Call Limpiar()
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub Limpiar()
        Try
            txt_Autonumsuc.Text = ""
            ''txt_Empresa.Text = ""
            ''cbo_Sucursal.Text = ""

            txt_Empresa.ReadOnly = True
            txt_Autonumsuc.ReadOnly = False
            cbo_Sucursal.Enabled = True

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Terminar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosFac As DataTable = Nothing
        Dim dtDatosDet As DataTable = Nothing
        Dim dtDatosOtros As DataTable = Nothing
        Dim Mensaje As String = ""

        Dim Renglon As Integer = 0
        Dim Cantidad As Double = 0.0

        Dim cn As System.Data.SqlClient.SqlConnection = Nothing
        Dim ControlTransaccion As System.Data.SqlClient.SqlTransaction = Nothing

        Dim RutaCertificado As String = ""
        Dim RutaKey As String = ""
        Dim PasswordKey As String = ""

        Dim EmisorNombre As String = ""
        Dim EmisorRfc As String = ""
        Dim EmisorRegimen As String = ""
        Dim EmisorFolio As String = ""
        Dim EmisorSerie As String = ""
        Dim EmisorVersion As String = ""
        Dim EmisorClaveProductoServ As String
        Dim EmisorPACAmbiente As String
        Dim EmisorPAC As String
        Dim EmisorPACUser As String
        Dim EmisorPACPass As String
        Dim EmisorPACpfxB64 As String
        Dim EmisorPACpfxpwd As String

        Dim EmisorSMTPCorreo As String = ""
        Dim EmisorSMTPHost As String = ""
        Dim EmisorSMTPUsername As String = ""
        Dim EmisorSMTPPassword As String = ""
        Dim EmisorSMTPPort As Integer = 587
        Dim LogTrimbrado As Boolean

        Dim sCP As String
        Dim sCorreos As String
        Dim correos_nuevos As String = ""
        Try
            oDatos = New Datos_Viscoi
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            cn.Open()
            ControlTransaccion = cn.BeginTransaction()

            Dim VOficinaTimbrador1 As VOficinaTimbrador40.VOficinaTimbrador40

            If oDatos.PVTA_Recupera_SatInfo(txt_Empresa.Text _
                                               , dtDatos _
                                               , Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    RutaCertificado = dtDatos.Rows(0).Item("RutaCertificado")
                    RutaKey = dtDatos.Rows(0).Item("RutaKey")
                    PasswordKey = dtDatos.Rows(0).Item("PasswordKey")
                    EmisorNombre = dtDatos.Rows(0).Item("EmisorNombre")
                    EmisorRfc = dtDatos.Rows(0).Item("EmisorRfc")
                    EmisorRegimen = dtDatos.Rows(0).Item("EmisorRegimen")
                    EmisorFolio = dtDatos.Rows(0).Item("EmisorFolio")
                    EmisorSerie = dtDatos.Rows(0).Item("EmisorSerie")
                    EmisorVersion = dtDatos.Rows(0).Item("EmisorVersion")
                    EmisorClaveProductoServ = dtDatos.Rows(0).Item("EmisorClaveProductoServ")
                    EmisorPACAmbiente = dtDatos.Rows(0).Item("EmisorPACAmbiente")
                    EmisorPAC = dtDatos.Rows(0).Item("EmisorPAC")
                    EmisorPACUser = dtDatos.Rows(0).Item("EmisorPACUser")
                    EmisorPACPass = dtDatos.Rows(0).Item("EmisorPACPass")
                    EmisorSMTPCorreo = dtDatos.Rows(0).Item("EmisorSMTPCorreo")
                    EmisorSMTPHost = dtDatos.Rows(0).Item("EmisorSMTPHost")
                    EmisorSMTPUsername = dtDatos.Rows(0).Item("EmisorSMTPUsername")
                    EmisorSMTPPassword = dtDatos.Rows(0).Item("EmisorSMTPPassword")
                    EmisorSMTPPort = dtDatos.Rows(0).Item("EmisorSMTPPort")
                    LogTrimbrado = dtDatos.Rows(0).Item("LogTrimbrado")
                    EmisorPACpfxB64 = dtDatos.Rows(0).Item("EmisorPACpfxB64")
                    EmisorPACpfxpwd = dtDatos.Rows(0).Item("EmisorPACpfxpwd")


                    VOficinaTimbrador1 = New VOficinaTimbrador40.VOficinaTimbrador40(EmisorPAC, EmisorPACUser, EmisorPACPass, RutaCertificado, EmisorPACpfxB64, EmisorPACpfxpwd, EmisorPACAmbiente)

                    If oDatos.Recupera_TicketFactura(cn, ControlTransaccion, Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Autonumsuc.Text, dtDatosFac, dtDatosDet, dtDatosOtros, Mensaje) Then
                        If dtDatosFac.Rows.Count > 0 Then
                            If dtDatosFac.Rows(0).Item("estatus").ToString.ToUpper = "FACTURADO" And dtDatosFac.Rows(0).Item("UUID") <> "" Then
                                Dim email_enviado As Integer = dtDatosFac.Rows(0).Item("email_enviado")
                                Dim SubTotal As Double = dtDatosFac.Rows(0).Item("SubTotal")
                                Dim Total As Double = dtDatosFac.Rows(0).Item("Total")
                                Dim UUID As String = dtDatosFac.Rows(0).Item("UUID")
                                Dim ReceptorRFC As String = dtDatosFac.Rows(0).Item("rfc")
                                Dim Xml As String = dtDatosFac.Rows(0).Item("Xml")
                                Dim noCertificadoSAT As String = dtDatosFac.Rows(0).Item("noCertificadoSAT")
                                Dim selloCFD As String = dtDatosFac.Rows(0).Item("selloCFD")
                                Dim selloSAT As String = dtDatosFac.Rows(0).Item("selloSAT")
                                Dim cadenaoriginal As String = dtDatosFac.Rows(0).Item("cadenaoriginal")
                                '' Solo es una cancelacion ya se habia facturado

                                If Chk_Refacturar.Checked Then
                                    sCorreos = dtDatosFac.Rows(0).Item("observ1")
                                    sCP = dtDatosOtros.Rows(0).Item("FacCP")
                                    If ReTimbrar(cn, ControlTransaccion, Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal _
                                                  , txt_Autonumsuc.Text, sCP, sCorreos, dtDatosFac, dtDatosDet, dtDatosOtros _
                                                  , EmisorSMTPUsername, EmisorSMTPPassword, EmisorSMTPHost, EmisorSMTPPort, EmisorSMTPCorreo, Mensaje) Then
                                        'ControlTransaccion.Commit()
                                        ''MessageBox.Show("Exito", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)

                                        Call Imprimir(txt_Autonumsuc.Text, dtDatosFac, dtDatosDet, dtDatosOtros)
                                        correos_nuevos = InputBox("Confirmar Correos para enviar ... (Cada correo debe estar separado por ; )", "Enviar factura por correo.", sCorreos)
                                        Call SendMail(EmisorSMTPUsername, EmisorSMTPPassword, EmisorSMTPHost, EmisorSMTPPort, EmisorSMTPCorreo, correos_nuevos, "C:\Windows\Temp\F" & txt_Autonumsuc.Text, txt_Autonumsuc.Text)

                                        VOficinaTimbrador1.Cancelar(UUID, EmisorRfc, ReceptorRFC, Total, "01", dtDatosFac.Rows(0).Item("UUID"), "I")
                                        If (VOficinaTimbrador1.ErrorMessage = "") Then
                                            ControlTransaccion.Commit()
                                            MessageBox.Show("Exito", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                            Call Limpiar()
                                        Else
                                            '' Error PAC
                                            ControlTransaccion.Rollback()
                                            MessageBox.Show(EmisorPAC & vbNewLine & VOficinaTimbrador1.ErrorMessage, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        End If
                                    Else
                                        ControlTransaccion.Rollback()
                                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    End If

                                Else
                                    VOficinaTimbrador1.Cancelar(dtDatosFac.Rows(0).Item("UUID"), EmisorRfc, dtDatosFac.Rows(0).Item("rfc"), dtDatosFac.Rows(0).Item("total"), "02", "", "I")
                                    If (VOficinaTimbrador1.ErrorMessage = "") Then
                                        UUID = ""
                                        Xml = ""
                                        noCertificadoSAT = ""
                                        selloCFD = ""
                                        selloSAT = ""
                                        cadenaoriginal = ""
                                        If oDatos.Actualiza_TicketFactura_CFDI_Gen(cn, ControlTransaccion, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, email_enviado, "Cancelado", SubTotal, Total, UUID, Xml, noCertificadoSAT, selloCFD, selloSAT, cadenaoriginal, "", "", "", "", "", "", "", Mensaje) Then
                                            ControlTransaccion.Commit()
                                            MessageBox.Show("Exito", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                        Else
                                            '' Error FACTURA (VISCOI)
                                            ControlTransaccion.Rollback()
                                            MessageBox.Show("Error al actualizar Factura " & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                        End If
                                    Else
                                        '' Error PAC
                                        ControlTransaccion.Rollback()
                                        MessageBox.Show(EmisorPAC & vbNewLine & VOficinaTimbrador1.ErrorMessage, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    End If
                                End If
                            Else
                                '' Error PAC
                                ControlTransaccion.Rollback()
                                MessageBox.Show("Estatus no FACTURADO o sin UUID", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else
                            ControlTransaccion.Rollback()
                            MessageBox.Show("No se encontro factura.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Call Limpiar()
                        End If
                    Else
                        ControlTransaccion.Rollback()
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Else
                    ControlTransaccion.Rollback()
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                ControlTransaccion.Rollback()
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception
            If ControlTransaccion IsNot Nothing Then
                ControlTransaccion.Rollback()
            End If
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            ControlTransaccion = Nothing
            cn = Nothing
        End Try
        oDatos = Nothing
    End Sub
    Private Sub ImprimirTicket(ByVal Formato As String)
        Dim Reporte As Object
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Transacciones(CDate("1900-01-01") _
                                               , CDate("1900-01-01") _
                                               , 0 _
                                               , txt_Autonumsuc.Text _
                                               , txt_Empresa.Text _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , "OPER" _
                                               , "" _
                                               , "" _
                                               , "" _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , dtDatos2 _
                                               , Mensaje) Then
                If dtDatos1.Rows.Count > 0 Then
                    txt_Empresa1.Text = dtDatos1.Rows(0).Item("empresa")
                    txt_RFC.Text = dtDatos1.Rows(0).Item("rfc")
                    txt_Direccion.Text = dtDatos1.Rows(0).Item("direccion")
                    txt_Direccion2.Text = dtDatos2.Rows(0).Item("direccion")

                    txt_Cajero.Text = dtDatos.Rows(0).Item("cajero")
                    txt_Caja.Text = dtDatos.Rows(0).Item("caja")
                    txt_TipoCliente.Text = dtDatos.Rows(0).Item("referencia")

                    If oDatos.PVTA_Recupera_DetVentas_Impresion(CDate("1900-01-01") _
                                              , txt_Autonumsuc.Text _
                                               , txt_Empresa.Text _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , Mensaje) Then


                        oTicket = New entTicketVenta
                        oTicket.Fill(dtDatos, dtDatos1, Nothing)

                        oTicket.Empresa = txt_Empresa1.Text
                        oTicket.Rfc = txt_RFC.Text
                        oTicket.Direccion = txt_Direccion.Text
                        oTicket.Direccion2 = txt_Direccion2.Text
                        oTicket.Cajero = txt_Cajero.Text
                        oTicket.Caja = txt_Caja.Text

                        Dim MargenAba As Integer = 0
                        Dim MargenIzq As Integer = 0
                        Dim MargenDer As Integer = 0
                        Dim MargenArr As Integer = 0

                        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
                        Select Case Formato
                            Case "SEAFON"
                                Reporte = New xtraRepTicketVenta_Seafon
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).ods.DataSource = oTicket
                                ''TryCast(Reporte, xtraRepTicketVenta_Seafon).SwReimpresion = True
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta_Seafon).PrintingSystem)
                            Case "EPSON"
                                Reporte = New xtraRepTicketVenta
                                TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                                ''TryCast(Reporte, xtraRepTicketVenta).SwReimpresion = True
                                TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                            Case Else
                                Reporte = New xtraRepTicketVenta
                                TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                        End Select
                        ''printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                        ''printTool.ShowPreviewDialog()
                        ''lstImpresoras.SelectedIndex = 5
                        printBase.Print(lstImpresoras.Text)

                    Else
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                End If
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Function ReTimbrar(ByVal cn As System.Data.SqlClient.SqlConnection _
                                , ByVal ControlTransaccion As System.Data.SqlClient.SqlTransaction _
                                , ByVal v_Empresa As String _
                                , ByVal v_Sucursal As String _
                                , ByVal AutoNumSuc_DEV As String _
                                , ByVal sCP As String _
                                , ByVal sCorreos As String _
                                , ByRef dtCabecero As DataTable _
                                , ByRef dtDetalle As DataTable _
                                , ByRef dtOtros As DataTable _
                                , ByRef EmisorSMTPUsername As String _
                                , ByRef EmisorSMTPPassword As String _
                                , ByRef EmisorSMTPHost As String _
                                , ByRef EmisorSMTPPort As Integer _
                                , ByRef EmisorSMTPCorreo As String _
                                , ByRef Mensaje As String) As Boolean
        Dim sw_continuar As Boolean = False
        Dim oComprobante As VOficinaTimbrador40.Comprobante
        Dim oEmisor As VOficinaTimbrador40.ComprobanteEmisor
        Dim oReceptor As VOficinaTimbrador40.ComprobanteReceptor
        Dim oConcepto As VOficinaTimbrador40.ComprobanteConcepto
        Dim lstConceptos As List(Of VOficinaTimbrador40.ComprobanteConcepto)
        Dim oConceptoImpuestos As VOficinaTimbrador40.ComprobanteConceptoImpuestos
        Dim oConceptoImpuestosTraslado As VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado
        Dim oConceptoImpuestosTraslados As List(Of VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado)
        Dim oImpuestos As VOficinaTimbrador40.ComprobanteImpuestos
        Dim oImpuestoTraslado00 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestoTraslado08 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestoTraslado16 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestosTraslado As List(Of VOficinaTimbrador40.ComprobanteImpuestosTraslado)

        Dim RutaCertificado As String = ""
        Dim RutaKey As String = ""
        Dim PasswordKey As String = ""

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtFecha As DataTable = Nothing
        Dim Msj As String = ""
        Dim MsjFecha As String = ""
        Dim FechaActual As Date

        Dim SubTotal As Double
        Dim IVA As Double
        Dim Total As Double

        Dim EmisorNombre As String = ""
        Dim EmisorRfc As String = ""
        Dim EmisorRegimen As String = ""
        Dim EmisorFolio As String = ""
        Dim EmisorSerie As String = ""
        Dim EmisorVersion As String = ""
        Dim EmisorClaveProductoServ As String
        Dim EmisorPACAmbiente As String
        Dim EmisorPAC As String
        Dim EmisorPACUser As String
        Dim EmisorPACPass As String

        Dim LogTrimbrado As Boolean

        Dim email_enviado As Integer = 0

        Dim VOficinaTimbrador1 As VOficinaTimbrador40.VOficinaTimbrador40

        Dim oPara = New Dictionary(Of String, String)()

        Try
            If sCorreos.Length > 0 Then
                EmisorSMTPPort = 587

                For Each sCorreo As String In sCorreos.Split(";")
                    oPara.Add(sCorreo.Trim, sCorreo.Trim)
                Next

                oDatos = New Datos_Viscoi
                Dim iRedondeo6 As Integer = 6
                Dim iRedondeo5 As Integer = 6
                Dim iRedondeo4 As Integer = 4
                Dim iRedondeo2 As Integer = 2
                If oDatos.PVTA_Sat_CFDI_FechaActual(dtFecha, MsjFecha) Then
                    FechaActual = dtFecha.Rows(0).Item("fum")
                    If oDatos.PVTA_Recupera_SatInfo(v_Empresa _
                                               , dtDatos _
                                               , Msj) Then
                        If dtDatos.Rows.Count > 0 Then
                            RutaCertificado = dtDatos.Rows(0).Item("RutaCertificado")
                            RutaKey = dtDatos.Rows(0).Item("RutaKey")
                            PasswordKey = dtDatos.Rows(0).Item("PasswordKey")
                            EmisorNombre = dtDatos.Rows(0).Item("EmisorNombre")
                            EmisorRfc = dtDatos.Rows(0).Item("EmisorRfc")
                            EmisorRegimen = dtDatos.Rows(0).Item("EmisorRegimen")
                            EmisorFolio = dtDatos.Rows(0).Item("EmisorFolio")
                            EmisorSerie = dtDatos.Rows(0).Item("EmisorSerie")
                            EmisorVersion = dtDatos.Rows(0).Item("EmisorVersion")
                            EmisorClaveProductoServ = dtDatos.Rows(0).Item("EmisorClaveProductoServ")
                            EmisorPACAmbiente = dtDatos.Rows(0).Item("EmisorPACAmbiente")
                            EmisorPAC = dtDatos.Rows(0).Item("EmisorPAC")
                            EmisorPACUser = dtDatos.Rows(0).Item("EmisorPACUser")
                            EmisorPACPass = dtDatos.Rows(0).Item("EmisorPACPass")
                            EmisorSMTPCorreo = dtDatos.Rows(0).Item("EmisorSMTPCorreo")
                            EmisorSMTPHost = dtDatos.Rows(0).Item("EmisorSMTPHost")
                            EmisorSMTPUsername = dtDatos.Rows(0).Item("EmisorSMTPUsername")
                            EmisorSMTPPassword = dtDatos.Rows(0).Item("EmisorSMTPPassword")
                            EmisorSMTPPort = dtDatos.Rows(0).Item("EmisorSMTPPort")
                            LogTrimbrado = dtDatos.Rows(0).Item("LogTrimbrado")

                            VOficinaTimbrador1 = New VOficinaTimbrador40.VOficinaTimbrador40(EmisorPAC, EmisorPACUser, EmisorPACPass, RutaCertificado, RutaKey, PasswordKey, EmisorPACAmbiente)

                            SubTotal = 0.0 ''dtCabecero.Rows(0).Item("subtotal")
                            Total = 0.0 ''dtDatosFac.Rows(0).Item("total")
                            IVA = 0.0 ''dtCabecero.Rows(0).Item("impuesto_iva")

                            SubTotal = Math.Round(SubTotal, 2)
                            Total = Math.Round(Total, 2)
                            IVA = Math.Round(IVA, 2)

                            oComprobante = New VOficinaTimbrador40.Comprobante()
                            Dim lstCfdisRel As New List(Of VOficinaTimbrador40.ComprobanteCfdiRelacionadosCfdiRelacionado)
                            Dim obj1 As New VOficinaTimbrador40.ComprobanteCfdiRelacionadosCfdiRelacionado
                            obj1.UUID = dtCabecero.Rows(0).Item("uuid")
                            lstCfdisRel.Add(obj1)

                            Dim oCFDIs As New List(Of VOficinaTimbrador40.ComprobanteCfdiRelacionados)
                            oCFDIs.Add(New VOficinaTimbrador40.ComprobanteCfdiRelacionados)
                            oComprobante.CfdiRelacionados = oCFDIs.ToArray
                            oComprobante.CfdiRelacionados(0).CfdiRelacionado = lstCfdisRel.ToArray
                            oComprobante.CfdiRelacionados(0).TipoRelacion = "04"
                            oComprobante.Version = EmisorVersion
                            oComprobante.Serie = dtCabecero.Rows(0).Item("observ2")
                            oComprobante.Folio = dtCabecero.Rows(0).Item("folio")
                            ''If CDate(dtCabecero.Rows(0).Item("fum")).ToString("yyyy-MM-dd") = FechaActual.ToString("yyyy-MM-dd") Then
                            ''    oComprobante.Fecha = dtCabecero.Rows(0).Item("fum")
                            ''    oComprobante.Fecha = oComprobante.Fecha.ToString("yyyy-MM-ddTHH:mm:ss")
                            ''Else
                            ''    oComprobante.Fecha = FechaActual.ToString("yyyy-MM-ddTHH:mm:ss")
                            ''End If
                            oComprobante.Fecha = dtCabecero.Rows(0).Item("fum")
                            oComprobante.Fecha = oComprobante.Fecha.ToString("yyyy-MM-ddTHH:mm:ss")

                            oComprobante.FormaPago = dtCabecero.Rows(0).Item("forma_pago").ToString.Trim
                            oComprobante.Total = 0 ''Total
                            oComprobante.SubTotal = 0 ''SubTotal
                            oComprobante.Moneda = VOficinaTimbrador40.c_Moneda.MXN
                            oComprobante.MetodoPago = dtCabecero.Rows(0).Item("metodo_pago")
                            oComprobante.LugarExpedicion = dtOtros.Rows(0).Item("FacCP")
                            oComprobante.TipoDeComprobante = VOficinaTimbrador40.c_TipoDeComprobante.I
                            oComprobante.Exportacion = dtCabecero.Rows(0).Item("exportacion")

                            oEmisor = New VOficinaTimbrador40.ComprobanteEmisor

                            oEmisor.Nombre = EmisorNombre
                            oEmisor.RegimenFiscal = EmisorRegimen
                            oEmisor.Rfc = EmisorRfc

                            oReceptor = New VOficinaTimbrador40.ComprobanteReceptor
                            oReceptor.Nombre = dtCabecero.Rows(0).Item("nomfac").Trim
                            oReceptor.Rfc = dtCabecero.Rows(0).Item("rfc")
                            oReceptor.UsoCFDI = [Enum].Parse(GetType(VOficinaTimbrador40.c_UsoCFDI), dtCabecero.Rows(0).Item("uso_cfdi"))
                            oReceptor.DomicilioFiscalReceptor = dtCabecero.Rows(0).Item("cpostalfac")
                            oReceptor.RegimenFiscalReceptor = dtCabecero.Rows(0).Item("regimen_fiscal1")

                            oComprobante.Emisor = oEmisor
                            oComprobante.Receptor = oReceptor
                            lstConceptos = New List(Of VOficinaTimbrador40.ComprobanteConcepto)

                            oImpuestos = New VOficinaTimbrador40.ComprobanteImpuestos
                            oImpuestosTraslado = New List(Of VOficinaTimbrador40.ComprobanteImpuestosTraslado)
                            oImpuestoTraslado00 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestoTraslado08 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestoTraslado16 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestos.TotalImpuestosTrasladados = 0.0
                            Dim importetras As Decimal = 0.0
                            Dim importebase00 As Decimal = 0.0
                            Dim importebase08 As Decimal = 0.0
                            Dim importebase16 As Decimal = 0.0
                            Dim importetras00 As Decimal = 0.0
                            Dim importetras08 As Decimal = 0.0
                            Dim importetras16 As Decimal = 0.0
                            Dim importeimpu00 As Decimal = 0.0
                            Dim importeimpu08 As Decimal = 0.0
                            Dim importeimpu16 As Decimal = 0.0
                            Dim importedesc As Decimal = 0.0
                            Dim conceptoimporte As Decimal = 0.0
                            Dim Sw_Traslado00 As Boolean = False
                            Dim SumaSubTotal As Decimal = 0.0
                            For Each dtRenglon As DataRow In dtDetalle.Rows

                                oConcepto = New VOficinaTimbrador40.ComprobanteConcepto
                                oConcepto.ClaveProdServ = dtRenglon("unidadvta")
                                oConcepto.Cantidad = dtRenglon("cantidad")
                                oConcepto.ClaveUnidad = [Enum].Parse(GetType(VOficinaTimbrador40.c_ClaveUnidad), dtRenglon("claveunidad")) ''VOficinaTimbrador40.c_ClaveUnidad.H87
                                oConcepto.Descripcion = dtRenglon("concepto")
                                oConcepto.ValorUnitario = IIf(dtRenglon("cantidad") <> 0, Math.Round(dtRenglon("importe1") / dtRenglon("cantidad"), iRedondeo6), 0.0)
                                oConcepto.ValorUnitario = Math.Round(oConcepto.ValorUnitario, iRedondeo6)
                                oConcepto.Importe = Globales.oAmbientes.Redondear(dtRenglon("importe1"), iRedondeo4)
                                oConcepto.Descuento = Math.Round(dtRenglon("descuento"), iRedondeo2)
                                oConcepto.ObjetoImp = dtRenglon("objetoimp")

                                importedesc += oConcepto.Descuento ''Math.Round(dtRenglon("descuento"), iRedondeo6)

                                'SumaSubTotal += Math.Round(dtRenglon("importe1"), iRedondeo6)
                                'oComprobante.SubTotal += Math.Round(dtRenglon("importe1"), iRedondeo6)
                                'oComprobante.Total += Math.Round(dtRenglon("importe1") - dtRenglon("descuento") + dtRenglon("impuesto_iva"), iRedondeo6)

                                oConceptoImpuestos = New VOficinaTimbrador40.ComprobanteConceptoImpuestos
                                oConceptoImpuestosTraslados = New List(Of VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado)
                                oConceptoImpuestosTraslado = New VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado
                                oConceptoImpuestosTraslado.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002 ''ESTE ES IVA
                                oConceptoImpuestosTraslado.Base = oConcepto.Importe ''Math.Round(dtRenglon("importe1") - dtRenglon("descuento"), iRedondeo6)

                                oConceptoImpuestosTraslado.Importe = Math.Round(dtRenglon("impuesto_iva"), iRedondeo2)
                                'oConceptoImpuestosTraslado.Importe = Globales.oAmbientes.Redondear(oConceptoImpuestosTraslado.Base * dtRenglon("tasa_iva"), iRedondeo2, True)


                                conceptoimporte += oConceptoImpuestosTraslado.Importe
                                Select Case Math.Round(dtRenglon("tasa_iva"), iRedondeo6)
                                    Case 0.16
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase16 += oConceptoImpuestosTraslado.Base
                                        importeimpu16 += oConceptoImpuestosTraslado.Importe
                                    Case 0.08
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_08
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase08 += oConceptoImpuestosTraslado.Base
                                        importeimpu08 += oConceptoImpuestosTraslado.Importe
                                    Case 0.0
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_00
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase00 += oConceptoImpuestosTraslado.Base
                                        importeimpu00 += oConceptoImpuestosTraslado.Importe
                                        Sw_Traslado00 = True
                                    Case Else
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase16 += oConceptoImpuestosTraslado.Base
                                        importeimpu16 += oConceptoImpuestosTraslado.Importe
                                End Select
                                oComprobante.SubTotal += Math.Round(oConcepto.Importe, iRedondeo2)
                                oComprobante.Total += oConcepto.Importe + oConceptoImpuestosTraslado.Importe

                                oConceptoImpuestosTraslados.Add(oConceptoImpuestosTraslado)
                                oConceptoImpuestos.Traslados = oConceptoImpuestosTraslados.ToArray()
                                If Not (oConcepto.ObjetoImp = "01" Or oConcepto.ObjetoImp = "03") Then
                                    oConcepto.Impuestos = oConceptoImpuestos
                                End If

                                lstConceptos.Add(oConcepto)
                                oComprobante.Conceptos = lstConceptos.ToArray
                            Next
                            oComprobante.Descuento = importedesc

                            oImpuestoTraslado16.Base = Math.Round(importebase16, iRedondeo2)
                            oImpuestoTraslado16.Importe = Math.Round(importeimpu16, iRedondeo2)
                            oImpuestoTraslado16.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado16.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                            oImpuestoTraslado16.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            oImpuestoTraslado08.Base = Math.Round(importebase08, iRedondeo2)
                            oImpuestoTraslado08.Importe = Math.Round(importeimpu08, iRedondeo2)
                            oImpuestoTraslado08.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado08.TasaOCuota = VOficinaTimbrador1.ValorIVA_08
                            oImpuestoTraslado08.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            oImpuestoTraslado00.Base = Math.Round(importebase00, iRedondeo2)
                            oImpuestoTraslado00.Importe = Math.Round(importeimpu00, iRedondeo2)
                            oImpuestoTraslado00.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado00.TasaOCuota = VOficinaTimbrador1.ValorIVA_00
                            oImpuestoTraslado00.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            If importeimpu16 > 0 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado16)
                            End If
                            If importeimpu08 > 0 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado08)
                            End If
                            If Sw_Traslado00 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado00)
                            End If
                            oImpuestos.TotalImpuestosTrasladados = Math.Round(oImpuestoTraslado16.Importe + oImpuestoTraslado08.Importe + oImpuestoTraslado00.Importe, iRedondeo4)
                            If oImpuestosTraslado.Count > 0 Then
                                oImpuestos.Traslados = oImpuestosTraslado.ToArray()
                                oComprobante.Impuestos = oImpuestos
                            End If

                            importetras += oImpuestos.TotalImpuestosTrasladados

                            oComprobante.Descuento = Math.Round(importedesc, iRedondeo2)

                            oComprobante.SubTotal = Math.Round(oComprobante.SubTotal, iRedondeo2)
                            oComprobante.Total = Math.Abs(oComprobante.Total)
                            oComprobante.SubTotal = Math.Abs(oComprobante.SubTotal)
                            oComprobante.Descuento = Math.Abs(oComprobante.Descuento)
                            oComprobante.Total = Math.Round(oComprobante.SubTotal - oComprobante.Descuento + oImpuestos.TotalImpuestosTrasladados, iRedondeo2)

                            Dim difTotal As Double = 0
                            Dim iFactor As Integer = 1
                            Dim difBase16 As Decimal = 0
                            Dim difBase08 As Decimal = 0
                            Dim difBase00 As Decimal = 0

                            difTotal = Math.Abs(Math.Round(dtCabecero.Rows(0).Item("totaltc"), 2)) - oComprobante.Total

                            VOficinaTimbrador1.Tipo = "Factura"
                            Dim oPagos As VOficinaTimbrador40.Pagos = Nothing
                            VOficinaTimbrador1.Sellar(oComprobante, LogTrimbrado, oPagos)
                            If VOficinaTimbrador1.ErrorMessage = "" Then
                                VOficinaTimbrador1.Timbrar(LogTrimbrado)

                                If (VOficinaTimbrador1.ErrorMessage = "") Then
                                    VOficinaTimbrador1.SMTPHost = EmisorSMTPHost
                                    VOficinaTimbrador1.SMTPUsername = EmisorSMTPUsername
                                    VOficinaTimbrador1.SMTPPassword = EmisorSMTPPassword
                                    VOficinaTimbrador1.SMTPPort = EmisorSMTPPort
                                    If oDatos.Actualiza_TicketFactura_CFDI_Gen(cn, ControlTransaccion, v_Empresa, v_Sucursal, AutoNumSuc_DEV, email_enviado, "FACTURADO", oComprobante.SubTotal, oComprobante.Total, VOficinaTimbrador1.TFD.UUID, VOficinaTimbrador1.XML, VOficinaTimbrador1.TFD.noCertificadoSAT, VOficinaTimbrador1.TFD.selloCFD, VOficinaTimbrador1.TFD.selloSAT, VOficinaTimbrador1.TFD.CadenaOriginalComplemento, VOficinaTimbrador1.TFD.version, VOficinaTimbrador1.TFD.FechaTimbrado, VOficinaTimbrador1.TFD.RfcProvCertif, VOficinaTimbrador1.NoCertificado, "", "04", dtCabecero.Rows(0).Item("uuid"), Msj) Then
                                        MessageBox.Show("Venta Refacturada con exito.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                                        Dim xmlDoc As System.Xml.XmlDocument
                                        xmlDoc = New System.Xml.XmlDocument()
                                        xmlDoc.LoadXml(VOficinaTimbrador1.XML)
                                        If Not IO.Directory.Exists("C: \Windows\Temp\F" & AutoNumSuc_DEV) Then
                                            IO.Directory.CreateDirectory("C:\Windows\Temp\F" & AutoNumSuc_DEV)
                                        End If
                                        xmlDoc.Save("C:\Windows\Temp\F" & AutoNumSuc_DEV & "\" & AutoNumSuc_DEV & ".xml")
                                        If oDatos.Recupera_TicketFactura(cn, ControlTransaccion, v_Empresa, v_Sucursal, AutoNumSuc_DEV, dtCabecero, dtDetalle, dtOtros, Msj) Then
                                            If dtCabecero.Rows.Count > 0 Then
                                                sw_continuar = True
                                                Mensaje = "Exito"
                                            Else
                                                sw_continuar = False
                                                Mensaje = "Autonumsuc (" & AutoNumSuc_DEV & ") Factura no encontrada"
                                            End If
                                        Else
                                            sw_continuar = False
                                            Mensaje = "Error Recupera_TicketFactura " & Msj
                                        End If
                                    Else
                                        sw_continuar = False
                                        Mensaje = "Error al actualizar Factura " & Msj
                                    End If
                                Else
                                    sw_continuar = False
                                    Mensaje = "El timbrador dijo: " & vbNewLine & VOficinaTimbrador1.ErrorMessage
                                End If
                            Else
                                sw_continuar = False
                                Mensaje = "Sellar dijo: " & vbNewLine & VOficinaTimbrador1.ErrorMessage
                            End If
                        Else
                            sw_continuar = False
                            Mensaje = "No se encontraron datos del emisor para facturar para la empresa ('" & v_Empresa & "')"
                        End If
                    Else
                        sw_continuar = False
                        Mensaje = Msj
                    End If
                Else
                    sw_continuar = False
                    Mensaje = "No es posuble obtener la fecha (PVTA_Sat_CFDI_FechaActual)"
                End If
            Else
                    sw_continuar = False
                Mensaje = "Debe introducir un correo electronico."
            End If
        Catch ex As Exception
            sw_continuar = False
            Mensaje = ex.Message
        End Try
        Return sw_continuar
    End Function
    Public Sub Imprimir(ByVal AS_Dev As String, ByVal dtCabecero As DataTable, ByVal dtDetalle As DataTable, ByVal dtOtros As DataTable)
        Dim Formularios As Globales.clsFormularios
        Dim ForamtoOC As Object = Nothing
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oFacturas As New Facturas
        Dim Mensaje As String = ""
        Dim oDatos_Supply As New Datos_Viscoi

        Try
            oFacturas.Fill(dtCabecero, dtDetalle, dtOtros)

            Select Case txt_Empresa.Text
                Case "PAPELERA"
                    ForamtoOC = New XtraRep_FacturaCuadros
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).lbl_OC_CFDIREL.Text = "CFDIS RELACIONADO"
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Empresa = oFacturas.FacEmpresa
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Direccion = oFacturas.FacDireccion.Replace("|", vbNewLine)
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).LugExpedicion = oFacturas.LugExpedicion
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Fecha = Format(oFacturas.Fecha, "dd/MMM/yyyy")
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Importe = Format(oFacturas.Total, "C2")
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Letra = oFacturas.Letra
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).FacCP = oFacturas.FacCP
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).odsDatosFacturas.DataSource = oFacturas
                Case Else
                    ForamtoOC = New XtraRep_FacturaBase
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Empresa = oFacturas.FacEmpresa
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Direccion = oFacturas.FacDireccion.Replace("|", vbNewLine)
                    TryCast(ForamtoOC, XtraRep_FacturaBase).LugExpedicion = oFacturas.LugExpedicion
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Fecha = Format(oFacturas.Fecha, "dd/MMM/yyyy")
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Importe = Format(oFacturas.Total, "C2")
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Letra = oFacturas.Letra
                    TryCast(ForamtoOC, XtraRep_FacturaBase).FacCP = oFacturas.FacCP
                    TryCast(ForamtoOC, XtraRep_FacturaBase).odsDatosFacturas.DataSource = oFacturas
            End Select
            If Not ForamtoOC Is Nothing Then
                printTool = New DevExpress.XtraReports.UI.ReportPrintTool(ForamtoOC)
                printTool.ShowPreviewDialog()
                If Not IO.Directory.Exists("C:\Windows\Temp\F" & AS_Dev) Then
                    IO.Directory.CreateDirectory("C:\Windows\Temp\F" & AS_Dev)
                End If
                ForamtoOC.ExportToPdf("C:\Windows\Temp\F" & AS_Dev & "\" & AS_Dev & ".pdf")
            Else
                MessageBox.Show("Formato no registrado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
        oDatos_Supply = Nothing
    End Sub
    Private Sub SendMail(ByVal var_MailUser As String _
                         , ByVal var_MailPass As String _
                         , ByVal var_MailHost As String _
                         , ByVal var_MailPort As String _
                         , ByVal var_MailFrom As String _
                         , ByVal var_MailDestinos As String _
                         , ByVal var_Archivo As String _
                         , ByVal Ticket As String)
        Dim Smtp_Server As New System.Net.Mail.SmtpClient
        Dim e_mail As New System.Net.Mail.MailMessage()
        Dim txt_destinos As New TextBox
        Try
            If var_MailUser <> "" And var_MailPass <> "" _
                And var_MailPort <> "" And var_MailHost <> "" _
                And var_MailFrom <> "" And var_MailDestinos <> "" Then

                Smtp_Server.UseDefaultCredentials = False
                Smtp_Server.Credentials = New Net.NetworkCredential(var_MailUser, var_MailPass)
                Smtp_Server.Port = var_MailPort
                Smtp_Server.EnableSsl = True
                Smtp_Server.Host = var_MailHost

                e_mail = New System.Net.Mail.MailMessage()
                e_mail.From = New System.Net.Mail.MailAddress(var_MailFrom)
                txt_destinos.Multiline = True

                For Each dest As String In var_MailDestinos.Split(";")
                    e_mail.To.Add(dest)
                Next
                'e_mail.CC.Add(txtTo.Text)
                e_mail.IsBodyHtml = True
                Select Case txt_Empresa.Text
                    Case "PAPELERA"
                        e_mail.Subject = "Papelera Del Norte: Envio de comprobante fiscal digital a traves de Internet" & Ticket & ""
                        e_mail.Body = "Estimado cliente"
                        e_mail.Body = e_mail.Body & "<BR>" & "Se adjunta a este e-mail archivo ZIP conteniendo los archivos XML y PDF del comprobante fiscal digital a traves de internet (CFDI) correspondiente al ComprobanteFolio" & Ticket & "."
                        e_mail.Body = e_mail.Body & "<BR>" & "Gracias."
                        e_mail.Body = e_mail.Body & "<BR>" & "Papelera Del Norte De La Laguna, S.A. de C.V."
                    Case Else
                        e_mail.Subject = "VOFICNA - Trimbrador"
                        e_mail.Body = "El sistema VOFICNA Envia factura"
                End Select

                If IO.File.Exists(var_Archivo & ".zip") Then
                    IO.File.Delete(var_Archivo & ".zip")
                End If
                System.IO.Compression.ZipFile.CreateFromDirectory(var_Archivo, var_Archivo & ".zip", System.IO.Compression.CompressionLevel.Optimal, False)

                Dim attachfile As New System.Net.Mail.Attachment(var_Archivo & ".zip")
                e_mail.Attachments.Add(attachfile)

                Smtp_Server.Send(e_mail)
                e_mail.Dispose()
                Smtp_Server.Dispose()

                MessageBox.Show("Se ha enviado a los correos proporcionados.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class