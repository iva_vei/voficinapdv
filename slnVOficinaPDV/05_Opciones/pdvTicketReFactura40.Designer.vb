﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvTicketReFactura40
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvTicketReFactura40))
        Me.gcDatosTicket = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Buscar = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Autonumsuc = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cbo_Sucursal = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.txt_FormatoTicket = New System.Windows.Forms.TextBox()
        Me.txt_MargenesTicket = New System.Windows.Forms.TextBox()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.Chk_Refacturar = New System.Windows.Forms.CheckBox()
        Me.txt_Empresa1 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txt_Corte = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosTicket.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.PanelFiltros_Acciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcDatosTicket
        '
        Me.gcDatosTicket.Controls.Add(Me.btn_Buscar)
        Me.gcDatosTicket.Controls.Add(Me.txt_Autonumsuc)
        Me.gcDatosTicket.Controls.Add(Me.Label21)
        Me.gcDatosTicket.Controls.Add(Me.Label20)
        Me.gcDatosTicket.Controls.Add(Me.cbo_Sucursal)
        Me.gcDatosTicket.Controls.Add(Me.Label19)
        Me.gcDatosTicket.Controls.Add(Me.txt_Empresa)
        Me.gcDatosTicket.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcDatosTicket.Location = New System.Drawing.Point(0, 0)
        Me.gcDatosTicket.Name = "gcDatosTicket"
        Me.gcDatosTicket.Size = New System.Drawing.Size(755, 103)
        Me.gcDatosTicket.TabIndex = 1
        Me.gcDatosTicket.Text = "Datos del Ticket"
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Location = New System.Drawing.Point(647, 34)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(50, 50)
        Me.btn_Buscar.TabIndex = 3
        Me.btn_Buscar.Text = "Buscar"
        '
        'txt_Autonumsuc
        '
        Me.txt_Autonumsuc.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Autonumsuc.Location = New System.Drawing.Point(430, 55)
        Me.txt_Autonumsuc.Name = "txt_Autonumsuc"
        Me.txt_Autonumsuc.Size = New System.Drawing.Size(200, 29)
        Me.txt_Autonumsuc.TabIndex = 2
        Me.txt_Autonumsuc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(492, 31)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(117, 24)
        Me.Label21.TabIndex = 23
        Me.Label21.Text = "Autonumsuc"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(273, 31)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(83, 24)
        Me.Label20.TabIndex = 22
        Me.Label20.Text = "Sucursal"
        '
        'cbo_Sucursal
        '
        Me.cbo_Sucursal.DisplayMember = "id_sucursal"
        Me.cbo_Sucursal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Sucursal.FormattingEnabled = True
        Me.cbo_Sucursal.Location = New System.Drawing.Point(216, 55)
        Me.cbo_Sucursal.Name = "cbo_Sucursal"
        Me.cbo_Sucursal.Size = New System.Drawing.Size(200, 27)
        Me.cbo_Sucursal.TabIndex = 1
        Me.cbo_Sucursal.ValueMember = "id_sucursal"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(70, 31)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(86, 24)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "Empresa"
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa.Location = New System.Drawing.Point(10, 55)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(200, 29)
        Me.txt_Empresa.TabIndex = 0
        Me.txt_Empresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_RFC
        '
        Me.txt_RFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(767, 12)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(14, 29)
        Me.txt_RFC.TabIndex = 5
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_RFC.Visible = False
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion.Location = New System.Drawing.Point(787, 12)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion.TabIndex = 6
        Me.txt_Direccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion2.Location = New System.Drawing.Point(807, 12)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion2.TabIndex = 7
        Me.txt_Direccion2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion2.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Location = New System.Drawing.Point(827, 12)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(14, 29)
        Me.txt_TipoCliente.TabIndex = 8
        Me.txt_TipoCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_TipoCliente.Visible = False
        '
        'txt_FormatoTicket
        '
        Me.txt_FormatoTicket.Enabled = False
        Me.txt_FormatoTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FormatoTicket.Location = New System.Drawing.Point(535, 1)
        Me.txt_FormatoTicket.Name = "txt_FormatoTicket"
        Me.txt_FormatoTicket.Size = New System.Drawing.Size(120, 29)
        Me.txt_FormatoTicket.TabIndex = 22
        Me.txt_FormatoTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_MargenesTicket
        '
        Me.txt_MargenesTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_MargenesTicket.Location = New System.Drawing.Point(847, 12)
        Me.txt_MargenesTicket.Name = "txt_MargenesTicket"
        Me.txt_MargenesTicket.Size = New System.Drawing.Size(14, 29)
        Me.txt_MargenesTicket.TabIndex = 23
        Me.txt_MargenesTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_MargenesTicket.Visible = False
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.PanelFiltros_Acciones)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl3.Location = New System.Drawing.Point(0, 106)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(755, 75)
        Me.GroupControl3.TabIndex = 25
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.ImageUri.Uri = "Apply"
        WindowsUIButtonImageOptions2.ImageUri.Uri = "Close"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Terminar", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Terminar", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Limpiar", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Limpiar", -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.Chk_Refacturar)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Empresa1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_RFC)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label34)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Corte)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_TipoCliente)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label33)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_MargenesTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label4)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lstImpresoras)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_FormatoTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label35)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Cajero)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Caja)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(2, 8)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 65)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(65, 65)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(751, 65)
        Me.PanelFiltros_Acciones.TabIndex = 24
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'Chk_Refacturar
        '
        Me.Chk_Refacturar.AutoSize = True
        Me.Chk_Refacturar.Checked = True
        Me.Chk_Refacturar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Chk_Refacturar.Location = New System.Drawing.Point(472, 39)
        Me.Chk_Refacturar.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Chk_Refacturar.Name = "Chk_Refacturar"
        Me.Chk_Refacturar.Size = New System.Drawing.Size(80, 17)
        Me.Chk_Refacturar.TabIndex = 26
        Me.Chk_Refacturar.Text = "ReFacturar"
        Me.Chk_Refacturar.UseVisualStyleBackColor = True
        '
        'txt_Empresa1
        '
        Me.txt_Empresa1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa1.Location = New System.Drawing.Point(747, 12)
        Me.txt_Empresa1.Name = "txt_Empresa1"
        Me.txt_Empresa1.Size = New System.Drawing.Size(14, 29)
        Me.txt_Empresa1.TabIndex = 25
        Me.txt_Empresa1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Empresa1.Visible = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(367, 2)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(54, 23)
        Me.Label34.TabIndex = 24
        Me.Label34.Text = "Corte"
        '
        'txt_Corte
        '
        Me.txt_Corte.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.txt_Corte.Location = New System.Drawing.Point(436, 2)
        Me.txt_Corte.Name = "txt_Corte"
        Me.txt_Corte.Size = New System.Drawing.Size(76, 30)
        Me.txt_Corte.TabIndex = 23
        Me.txt_Corte.Text = "0001"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(207, 1)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(63, 23)
        Me.Label33.TabIndex = 22
        Me.Label33.Text = "Cajero"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(103, 23)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(112, 37)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(350, 21)
        Me.lstImpresoras.TabIndex = 15
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(10, 1)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(53, 23)
        Me.Label35.TabIndex = 5
        Me.Label35.Text = "Caja:"
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.txt_Cajero.Location = New System.Drawing.Point(276, 1)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(76, 30)
        Me.txt_Cajero.TabIndex = 2
        Me.txt_Cajero.Text = "0001"
        '
        'txt_Caja
        '
        Me.txt_Caja.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Caja.Location = New System.Drawing.Point(69, 1)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(130, 30)
        Me.txt_Caja.TabIndex = 1
        Me.txt_Caja.Text = "CAJA 0001"
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 103)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(755, 3)
        Me.Panel1.TabIndex = 26
        '
        'pdvTicketReFactura40
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(755, 181)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.gcDatosTicket)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "pdvTicketReFactura40"
        Me.Text = "Ticket"
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosTicket.ResumeLayout(False)
        Me.gcDatosTicket.PerformLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gcDatosTicket As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Buscar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Autonumsuc As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents cbo_Sucursal As ComboBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txt_Empresa As TextBox
    Private WithEvents txt_RFC As TextBox
    Private WithEvents txt_Direccion As TextBox
    Private WithEvents txt_Direccion2 As TextBox
    Private WithEvents txt_TipoCliente As TextBox
    Private WithEvents txt_FormatoTicket As TextBox
    Private WithEvents txt_MargenesTicket As TextBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Panel1 As Panel
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents Label34 As Label
    Friend WithEvents txt_Corte As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Friend WithEvents Label35 As Label
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Private WithEvents txt_Empresa1 As TextBox
    Friend WithEvents Chk_Refacturar As CheckBox
End Class
