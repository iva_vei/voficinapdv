﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvReFactura40
    Dim _Accion As Boolean
    Dim sTipoDocto As String
    Dim sPath As String = "C:\Windows\Temp"
    Private dtCabecero As DataTable
    Private dtDetalle As DataTable
    Private dtOtros As DataTable
    Public SW_CerrarAuto As Boolean = False

    Private Sub pdvFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Msj As String = ""

        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Try
            btn_CambiarCliente.Visible = False
            btn_NuevoCliente.Visible = False
            cmd_RefrescaDatos.Visible = False
            btn_CambiarDir.Visible = False
            btn_GuardarDir.Visible = False
            btn_NuevoDireccion.Visible = False

            cbo_Regimen.Enabled = False
            cbo_FormaPago.Enabled = False
            txt_Serie.Enabled = False
            txt_Folio.Enabled = False
            cbo_PlanCredito.Enabled = False

            cbo_UsoCFDI.Enabled = False
            txt_Serie.Text = "FA"

            lblOC.Visible = True
            txt_OrdenCompra.Visible = True

            oDatos = New Datos_Viscoi
            datFEchaEntrega.Value = Today
            datFEchaEntrega.MinDate = Today

            txt_Empresa.Text = Globales.oAmbientes.Id_Empresa
            If oDatos.PVTA_Recupera_Sucursales(txt_Empresa.Text, "", dtDatosSuc, Msj) Then
                cbo_Sucursal.DataSource = dtDatosSuc
                cbo_Sucursal.SelectedIndex = 0
            End If
            If oDatos.PVTA_Recupera_SatRegimenFiscal(dtDatos, Msj) Then
                cbo_Regimen.DataSource = dtDatos
                cbo_Regimen.SelectedIndex = 0
            End If
            If oDatos.PVTA_Recupera_SatUsoCDFI("F", dtDatos, Msj) Then
                cbo_UsoCFDI.DataSource = dtDatos
                cbo_UsoCFDI.SelectedIndex = 0
            End If
            If oDatos.PVTA_Recupera_SatFormaPago("F", dtDatos, Msj) Then
                cbo_FormaPago.DataSource = dtDatos
                cbo_FormaPago.SelectedIndex = 0
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    Exit For
                End If
            Next
            cbo_Sucursal.Text = PDatSucursal

            oDatos = New Datos_Viscoi
            If oDatos.Recupera_tiposcredito(Globales.oAmbientes.Id_Empresa, "TODOS", "TODOS", dtDatos, Msj) Then
                cbo_PlanCredito.DataSource = dtDatos
            Else
                cbo_PlanCredito.DataSource = Nothing
            End If

            If txt_Autonumsuc.Text <> "" Then
                Call btn_Buscar_Click(Nothing, Nothing)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub GridC_1_KeyDown(sender As Object, e As KeyEventArgs) Handles GridC_1.KeyDown
        Call Globales.clsFormularios.Grid_Prevenir_Tab(sender, e, cbo_Regimen)
    End Sub
    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtLugEntregas As DataTable = Nothing
        Dim Msj As String = ""
        Dim dtRenglon As DataRow = Nothing
        Dim id_dir As Integer
        Dim dtDatos As DataTable = Nothing
        Dim EmisorSMTPCorreo As String = ""
        Dim EmisorSMTPHost As String = ""
        Dim EmisorSMTPUsername As String = ""
        Dim EmisorSMTPPassword As String = ""
        Dim EmisorSMTPPort As Integer = 587

        Try
            If txt_Autonumsuc.Text <> "" Then

                dtCabecero = Nothing
                dtDetalle = Nothing
                dtOtros = Nothing
                oDatos = New Datos_Viscoi
                If oDatos.Recupera_TicketFactura(Nothing, Nothing, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtCabecero, dtDetalle, dtOtros, Msj) Then
                    If dtCabecero.Rows.Count > 0 Then
                        dtRenglon = dtCabecero.Rows(0)
                        gcDatosTicket.Enabled = False
                        PnlDatosCliente.Enabled = False
                        PnlDatosDirFactura.Enabled = False
                        txt_Cliente.Text = dtRenglon("id_cliente")
                        txt_Nombre.Text = dtRenglon("nomfac")
                        txt_RFC.Text = dtRenglon("rfc")
                        txt_Calle.Text = dtRenglon("direccionfac")
                        txt_Colonia.Text = dtRenglon("coloniafac")
                        txt_CP.Text = dtRenglon("cpostalfac")
                        txt_Municipio.Text = dtRenglon("ciudadfac")
                        txt_Estado.Text = dtRenglon("edofac")
                        txt_OrdenCompra.Text = dtRenglon("pedido_mov")

                        cbo_Regimen.SelectedValue = dtRenglon("regimen_fiscal1")
                        cbo_FormaPago.SelectedValue = dtRenglon("forma_pago")
                        cbo_UsoCFDI.SelectedValue = dtRenglon("uso_cfdi")
                        txt_Serie.Text = dtRenglon("observ2")
                        txt_Folio.Text = dtRenglon("folio")
                        txt_SubTotal.Text = Format(dtRenglon("subtotal"), "N2")
                        txt_IVA.Text = Format(dtRenglon("impuesto_iva"), "N2")
                        txt_Descuento.Text = Format(dtRenglon("descuentoart"), "N2")
                        txt_Total.Text = Format(dtRenglon("total"), "N2")
                        sTipoDocto = dtRenglon("tipodocto")

                        txt_Correos.Text = dtRenglon("observ1").ToString.Trim
                        ''cbo_PlanCredito.SelectedValue = dtRenglon("tipocredito")
                        Dim TipoCredito As String = ""
                        TipoCredito = IIf(dtRenglon("tipocredito") <> "CONTADO", "CREDITO", dtRenglon("tipocredito"))

                        If oDatos.Recupera_tiposcredito(Globales.oAmbientes.Id_Empresa, "TODOS", TipoCredito, dtDatos, Msj) Then
                            cbo_PlanCredito.DataSource = dtDatos

                            If dtDatos.Rows.Count > 0 Then
                                cbo_PlanCredito.SelectedValue = dtRenglon("tipocredito")
                            Else
                                cbo_PlanCredito.SelectedIndex = -1
                                cbo_PlanCredito.Text = ""
                            End If
                        Else
                            cbo_PlanCredito.DataSource = Nothing
                            cbo_PlanCredito.SelectedIndex = -1
                            cbo_PlanCredito.Text = ""
                        End If


                        If dtRenglon("lugarent1").ToString.ToString.Trim.Length >= 2 Then

                            id_dir = Integer.Parse(dtRenglon("lugarent1").ToString.Substring(0, 2))

                        End If
                        If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                            GridC_1.DataSource = dtLugEntregas
                            If dtLugEntregas.Rows.Count > 0 Then
                                GridV_1.FocusedRowHandle = id_dir
                            End If
                        End If
                        If dtDetalle.Rows.Count = 0 Then
                            MessageBox.Show("Autonumsuc (" & txt_Autonumsuc.Text & ") No contiene detalle para facturacion", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                        txt_Cliente.Text = txt_Cliente.Text.Trim()
                        txt_Nombre.Text = txt_Nombre.Text.Trim()
                        txt_RFC.Text = txt_RFC.Text.Trim()
                        txt_Calle.Text = txt_Calle.Text.Trim()
                        txt_Colonia.Text = txt_Colonia.Text.Trim()
                        txt_CP.Text = txt_CP.Text.Trim()
                        txt_Municipio.Text = txt_Municipio.Text.Trim()
                        txt_Estado.Text = txt_Estado.Text.Trim()

                        txt_Serie.Text = txt_Serie.Text.Trim()
                        txt_Folio.Text = txt_Folio.Text.Trim()
                        txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                        txt_IVA.Text = txt_IVA.Text.Trim()
                        txt_Descuento.Text = txt_Descuento.Text.Trim()
                        txt_Total.Text = txt_Total.Text.Trim()

                        txt_Correos.Text = txt_Correos.Text.Trim()

                        If sTipoDocto = "PAGOS" Then
                            cbo_FormaPago.Enabled = True
                        Else
                            cbo_FormaPago.Enabled = False
                        End If

                        If txt_Cliente.Text <> "" Then
                            btn_CambiarCliente.Enabled = False
                            btn_NuevoCliente.Enabled = False
                        End If
                    Else
                        MessageBox.Show("Autonumsuc (" & txt_Autonumsuc.Text & ") Factura no encontrada", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                Else
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    MessageBox.Show("No se encontro movimiento para facturar.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        btn_Aceptar.Enabled = False

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim sw_continuar As Boolean = True
        Dim IdDir
        Try
            oDatos = New Datos_Viscoi
            Dim sTipoCredito As String = ""
            If Not cbo_PlanCredito.SelectedValue Is Nothing Then
                sTipoCredito = cbo_PlanCredito.SelectedValue
            End If
            If Not oDatos.Actualiza_TicketFactura_DatosCFDI(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, cbo_Regimen.SelectedValue, cbo_UsoCFDI.SelectedValue, cbo_FormaPago.SelectedValue, txt_Folio.Text, Msj) Then
                sw_continuar = False
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            If sw_continuar Then
                Call btn_Buscar_Click(Nothing, Nothing)
                Call ReFacturar()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
        btn_Aceptar.Enabled = True
    End Sub
    Private Sub ReFacturar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosFac As DataTable = Nothing
        Dim dtDatosDet As DataTable = Nothing
        Dim dtDatosOtros As DataTable = Nothing
        Dim Mensaje As String = ""

        Dim Renglon As Integer = 0
        Dim Cantidad As Double = 0.0

        Dim cn As System.Data.SqlClient.SqlConnection = Nothing
        Dim ControlTransaccion As System.Data.SqlClient.SqlTransaction = Nothing

        Dim RutaCertificado As String = ""
        Dim RutaKey As String = ""
        Dim PasswordKey As String = ""

        Dim EmisorNombre As String = ""
        Dim EmisorRfc As String = ""
        Dim EmisorRegimen As String = ""
        Dim EmisorFolio As String = ""
        Dim EmisorSerie As String = ""
        Dim EmisorVersion As String = ""
        Dim EmisorClaveProductoServ As String
        Dim EmisorPACAmbiente As String
        Dim EmisorPAC As String
        Dim EmisorPACUser As String
        Dim EmisorPACPass As String
        Dim EmisorPACpfxB64 As String
        Dim EmisorPACpfxpwd As String

        Dim EmisorSMTPCorreo As String = ""
        Dim EmisorSMTPHost As String = ""
        Dim EmisorSMTPUsername As String = ""
        Dim EmisorSMTPPassword As String = ""
        Dim EmisorSMTPPort As Integer = 587
        Dim LogTrimbrado As Boolean

        Dim sCP As String
        Dim sCorreos As String
        Dim correos_nuevos As String = ""
        Dim Tipo_relacion As String = "01"
        Try
            oDatos = New Datos_Viscoi
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            cn.Open()
            ControlTransaccion = cn.BeginTransaction()

            Dim VOficinaTimbrador1 As VOficinaTimbrador40.VOficinaTimbrador40

            If oDatos.PVTA_Recupera_SatInfo(txt_Empresa.Text _
                                                   , dtDatos _
                                                   , Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    RutaCertificado = dtDatos.Rows(0).Item("RutaCertificado")
                    RutaKey = dtDatos.Rows(0).Item("RutaKey")
                    PasswordKey = dtDatos.Rows(0).Item("PasswordKey")
                    EmisorNombre = dtDatos.Rows(0).Item("EmisorNombre")
                    EmisorRfc = dtDatos.Rows(0).Item("EmisorRfc")
                    EmisorRegimen = dtDatos.Rows(0).Item("EmisorRegimen")
                    EmisorFolio = dtDatos.Rows(0).Item("EmisorFolio")
                    EmisorSerie = dtDatos.Rows(0).Item("EmisorSerie")
                    EmisorVersion = dtDatos.Rows(0).Item("EmisorVersion")
                    EmisorClaveProductoServ = dtDatos.Rows(0).Item("EmisorClaveProductoServ")
                    EmisorPACAmbiente = dtDatos.Rows(0).Item("EmisorPACAmbiente")
                    EmisorPAC = dtDatos.Rows(0).Item("EmisorPAC")
                    EmisorPACUser = dtDatos.Rows(0).Item("EmisorPACUser")
                    EmisorPACPass = dtDatos.Rows(0).Item("EmisorPACPass")
                    EmisorSMTPCorreo = dtDatos.Rows(0).Item("EmisorSMTPCorreo")
                    EmisorSMTPHost = dtDatos.Rows(0).Item("EmisorSMTPHost")
                    EmisorSMTPUsername = dtDatos.Rows(0).Item("EmisorSMTPUsername")
                    EmisorSMTPPassword = dtDatos.Rows(0).Item("EmisorSMTPPassword")
                    EmisorSMTPPort = dtDatos.Rows(0).Item("EmisorSMTPPort")
                    LogTrimbrado = dtDatos.Rows(0).Item("LogTrimbrado")
                    EmisorPACpfxB64 = dtDatos.Rows(0).Item("EmisorPACpfxB64")
                    EmisorPACpfxpwd = dtDatos.Rows(0).Item("EmisorPACpfxpwd")


                    VOficinaTimbrador1 = New VOficinaTimbrador40.VOficinaTimbrador40(EmisorPAC, EmisorPACUser, EmisorPACPass, RutaCertificado, EmisorPACpfxB64, EmisorPACpfxpwd, EmisorPACAmbiente)
                    If oDatos.Recupera_TicketFactura(cn, ControlTransaccion, Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtDatosFac, dtDatosDet, dtDatosOtros, Mensaje) Then
                        If oDatos.Inserta_TicketFactura_Refactura(cn, ControlTransaccion, Globales.oAmbientes.Id_Empresa, dtDatosFac.Rows(0).Item("movimiento"), RG1.SelectedIndex, cbo_UsoCFDI.SelectedValue, cbo_Regimen.SelectedValue, Mensaje) Then
                            If dtDatosFac.Rows.Count > 0 Then
                                If (dtDatosFac.Rows(0).Item("estatus").ToString.ToUpper = "FACTURADO" Or dtDatosFac.Rows(0).Item("estatus").ToString.ToUpper = "Pendiente") And dtDatosFac.Rows(0).Item("UUID") <> "" Then
                                    Dim email_enviado As Integer = dtDatosFac.Rows(0).Item("email_enviado")
                                    Dim SubTotal As Double = dtDatosFac.Rows(0).Item("SubTotal")
                                    Dim Total As Double = dtDatosFac.Rows(0).Item("Total")
                                    Dim UUID As String = dtDatosFac.Rows(0).Item("UUID")
                                    Dim ReceptorRFC As String = dtDatosFac.Rows(0).Item("rfc")
                                    Dim Xml As String = dtDatosFac.Rows(0).Item("Xml")
                                    Dim noCertificadoSAT As String = dtDatosFac.Rows(0).Item("noCertificadoSAT")
                                    Dim selloCFD As String = dtDatosFac.Rows(0).Item("selloCFD")
                                    Dim selloSAT As String = dtDatosFac.Rows(0).Item("selloSAT")
                                    Dim cadenaoriginal As String = dtDatosFac.Rows(0).Item("cadenaoriginal")
                                    Dim version As String = dtDatosFac.Rows(0).Item("version")
                                    Dim fechatimbrado As String = dtDatosFac.Rows(0).Item("fechatimbrado")
                                    Dim rfcprovcertif As String = dtDatosFac.Rows(0).Item("rfcprovcertif")
                                    Dim nocertificado As String = dtDatosFac.Rows(0).Item("nocertificado")

                                    Dim MotivoCancelacion As String = "03 - No se llevó a cabo la operación."
                                    Dim FolioSustitucion As String = dtDatosFac.Rows(0).Item("Xml")

                                    'If UUID.Length > 0 Then
                                    '    MotivoCancelacion = "01 - Comprobante emitido con errores de relación"
                                    '    FolioSustitucion = UUID
                                    'End If
                                    FolioSustitucion = ""
                                    sCorreos = dtDatosFac.Rows(0).Item("observ1")
                                    sCP = dtDatosOtros.Rows(0).Item("FacCP")
                                    If ReTimbrar(cn, ControlTransaccion, Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text _
                                                      , txt_Autonumsuc.Text, sCP, sCorreos, dtDatosFac, dtDatosDet, dtDatosOtros _
                                                      , EmisorSMTPUsername, EmisorSMTPPassword, EmisorSMTPHost, EmisorSMTPPort, EmisorSMTPCorreo, Mensaje) Then
                                        If oDatos.Recupera_TicketFactura(cn, ControlTransaccion, Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtDatosFac, dtDatosDet, dtDatosOtros, Mensaje) Then
                                            If UUID.Length > 0 Then
                                                MotivoCancelacion = "01 - Comprobante emitido con errores de relación"
                                                FolioSustitucion = dtDatosFac.Rows(0).Item("UUID")
                                            End If
                                            VOficinaTimbrador1.Cancelar(UUID, EmisorRfc, ReceptorRFC, Total, MotivoCancelacion.Substring(0, 2), FolioSustitucion, "I")
                                            If (VOficinaTimbrador1.ErrorMessage = "") Then

                                                Dim RespEstatus As String
                                                Dim xmlDoc As New System.Xml.XmlDocument
                                                'xmlDoc.LoadXml(VOficinaTimbrador1.XML.Replace("&lt;", "<").Replace("&gt;", ">"))
                                                xmlDoc.LoadXml(VOficinaTimbrador1.XML)
                                                RespEstatus = xmlDoc.ChildNodes(0).ChildNodes(3).ChildNodes(0).ChildNodes(5).InnerText
                                                If RespEstatus.ToUpper = "CANCELADO SIN ACEPTACIÓN" Then
                                                    RespEstatus = "CANCELADO"
                                                Else
                                                    RespEstatus = RespEstatus
                                                End If

                                                If oDatos.Tim_Canc_Actualiza_TicketFactura_CFDI_Gen(cn, ControlTransaccion, Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text, txt_Autonumsuc.Text, email_enviado, RespEstatus, SubTotal, Total, UUID, VOficinaTimbrador1.XML, noCertificadoSAT, selloCFD, selloSAT, cadenaoriginal, version, fechatimbrado, rfcprovcertif, nocertificado, "", Tipo_relacion, UUID, Mensaje) Then
                                                    ControlTransaccion.Commit()

                                                    Call Imprimir(txt_Autonumsuc.Text, dtDatosFac, dtDatosDet, dtDatosOtros)
                                                    correos_nuevos = InputBox("Confirmar Correos para enviar ... (Cada correo debe estar separado por ; )", "Enviar factura por correo.", sCorreos)
                                                    Call SendMail(EmisorSMTPUsername, EmisorSMTPPassword, EmisorSMTPHost, EmisorSMTPPort, EmisorSMTPCorreo, correos_nuevos, "C:\Windows\Temp\F" & txt_Autonumsuc.Text, txt_Autonumsuc.Text)


                                                    MessageBox.Show("Exito", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                                    Call LimpiarPantalla()
                                                Else
                                                    ControlTransaccion.Rollback()
                                                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                                End If
                                            Else
                                                '' Error PAC
                                                ControlTransaccion.Rollback()
                                                MessageBox.Show(EmisorPAC & vbNewLine & VOficinaTimbrador1.ErrorMessage, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                            End If
                                        Else
                                            ControlTransaccion.Rollback()
                                            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        End If
                                    Else
                                        ControlTransaccion.Rollback()
                                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    End If
                                Else
                                    '' Error PAC
                                    ControlTransaccion.Rollback()
                                    MessageBox.Show("Estatus no FACTURADO o sin UUID", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                End If
                            Else
                                ControlTransaccion.Rollback()
                                MessageBox.Show("No se encontro factura.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Call LimpiarPantalla()
                            End If
                        Else
                            ControlTransaccion.Rollback()
                            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Else
                        ControlTransaccion.Rollback()
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Else
                    ControlTransaccion.Rollback()
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                ControlTransaccion.Rollback()
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception
            If ControlTransaccion IsNot Nothing Then
                ControlTransaccion.Rollback()
            End If
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            ControlTransaccion = Nothing
            cn = Nothing
        End Try
        oDatos = Nothing
    End Sub
    Private Function ReTimbrar(ByVal cn As System.Data.SqlClient.SqlConnection _
                                , ByVal ControlTransaccion As System.Data.SqlClient.SqlTransaction _
                                , ByVal v_Empresa As String _
                                , ByVal v_Sucursal As String _
                                , ByVal AutoNumSuc_DEV As String _
                                , ByVal sCP As String _
                                , ByVal sCorreos As String _
                                , ByRef dtCabecero As DataTable _
                                , ByRef dtDetalle As DataTable _
                                , ByRef dtOtros As DataTable _
                                , ByRef EmisorSMTPUsername As String _
                                , ByRef EmisorSMTPPassword As String _
                                , ByRef EmisorSMTPHost As String _
                                , ByRef EmisorSMTPPort As Integer _
                                , ByRef EmisorSMTPCorreo As String _
                                , ByRef Mensaje As String) As Boolean
        Dim sw_continuar As Boolean = False
        Dim oComprobante As VOficinaTimbrador40.Comprobante
        Dim oEmisor As VOficinaTimbrador40.ComprobanteEmisor
        Dim oReceptor As VOficinaTimbrador40.ComprobanteReceptor
        Dim oConcepto As VOficinaTimbrador40.ComprobanteConcepto
        Dim lstConceptos As List(Of VOficinaTimbrador40.ComprobanteConcepto)
        Dim oConceptoImpuestos As VOficinaTimbrador40.ComprobanteConceptoImpuestos
        Dim oConceptoImpuestosTraslado As VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado
        Dim oConceptoImpuestosTraslados As List(Of VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado)
        Dim oImpuestos As VOficinaTimbrador40.ComprobanteImpuestos
        Dim oImpuestoTraslado00 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestoTraslado08 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestoTraslado16 As VOficinaTimbrador40.ComprobanteImpuestosTraslado
        Dim oImpuestosTraslado As List(Of VOficinaTimbrador40.ComprobanteImpuestosTraslado)

        Dim RutaCertificado As String = ""
        Dim RutaKey As String = ""
        Dim PasswordKey As String = ""

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtFecha As DataTable = Nothing
        Dim Msj As String = ""
        Dim MsjFecha As String = ""
        Dim FechaActual As Date

        Dim SubTotal As Double
        Dim IVA As Double
        Dim Total As Double

        Dim EmisorNombre As String = ""
        Dim EmisorRfc As String = ""
        Dim EmisorRegimen As String = ""
        Dim EmisorFolio As String = ""
        Dim EmisorSerie As String = ""
        Dim EmisorVersion As String = ""
        Dim EmisorClaveProductoServ As String
        Dim EmisorPACAmbiente As String
        Dim EmisorPAC As String
        Dim EmisorPACUser As String
        Dim EmisorPACPass As String

        Dim LogTrimbrado As Boolean

        Dim email_enviado As Integer = 0

        Dim VOficinaTimbrador1 As VOficinaTimbrador40.VOficinaTimbrador40

        Dim oPara = New Dictionary(Of String, String)()

        Try
            If sCorreos.Length > 0 Then
                EmisorSMTPPort = 587

                For Each sCorreo As String In sCorreos.Split(";")
                    oPara.Add(sCorreo.Trim, sCorreo.Trim)
                Next

                oDatos = New Datos_Viscoi
                Dim iRedondeo6 As Integer = 6
                Dim iRedondeo5 As Integer = 6
                Dim iRedondeo4 As Integer = 4
                Dim iRedondeo2 As Integer = 2
                If oDatos.PVTA_Sat_CFDI_FechaActual(dtFecha, MsjFecha) Then
                    FechaActual = dtFecha.Rows(0).Item("fum")
                    If oDatos.PVTA_Recupera_SatInfo(v_Empresa _
                                               , dtDatos _
                                               , Msj) Then
                        If dtDatos.Rows.Count > 0 Then
                            RutaCertificado = dtDatos.Rows(0).Item("RutaCertificado")
                            RutaKey = dtDatos.Rows(0).Item("RutaKey")
                            PasswordKey = dtDatos.Rows(0).Item("PasswordKey")
                            EmisorNombre = dtDatos.Rows(0).Item("EmisorNombre")
                            EmisorRfc = dtDatos.Rows(0).Item("EmisorRfc")
                            EmisorRegimen = dtDatos.Rows(0).Item("EmisorRegimen")
                            EmisorFolio = dtDatos.Rows(0).Item("EmisorFolio")
                            EmisorSerie = dtDatos.Rows(0).Item("EmisorSerie")
                            EmisorVersion = dtDatos.Rows(0).Item("EmisorVersion")
                            EmisorClaveProductoServ = dtDatos.Rows(0).Item("EmisorClaveProductoServ")
                            EmisorPACAmbiente = dtDatos.Rows(0).Item("EmisorPACAmbiente")
                            EmisorPAC = dtDatos.Rows(0).Item("EmisorPAC")
                            EmisorPACUser = dtDatos.Rows(0).Item("EmisorPACUser")
                            EmisorPACPass = dtDatos.Rows(0).Item("EmisorPACPass")
                            EmisorSMTPCorreo = dtDatos.Rows(0).Item("EmisorSMTPCorreo")
                            EmisorSMTPHost = dtDatos.Rows(0).Item("EmisorSMTPHost")
                            EmisorSMTPUsername = dtDatos.Rows(0).Item("EmisorSMTPUsername")
                            EmisorSMTPPassword = dtDatos.Rows(0).Item("EmisorSMTPPassword")
                            EmisorSMTPPort = dtDatos.Rows(0).Item("EmisorSMTPPort")
                            LogTrimbrado = dtDatos.Rows(0).Item("LogTrimbrado")

                            VOficinaTimbrador1 = New VOficinaTimbrador40.VOficinaTimbrador40(EmisorPAC, EmisorPACUser, EmisorPACPass, RutaCertificado, RutaKey, PasswordKey, EmisorPACAmbiente)

                            SubTotal = 0.0 ''dtCabecero.Rows(0).Item("subtotal")
                            Total = 0.0 ''dtDatosFac.Rows(0).Item("total")
                            IVA = 0.0 ''dtCabecero.Rows(0).Item("impuesto_iva")

                            SubTotal = Math.Round(SubTotal, 2)
                            Total = Math.Round(Total, 2)
                            IVA = Math.Round(IVA, 2)

                            oComprobante = New VOficinaTimbrador40.Comprobante()
                            Dim lstCfdisRel As New List(Of VOficinaTimbrador40.ComprobanteCfdiRelacionadosCfdiRelacionado)
                            Dim obj1 As New VOficinaTimbrador40.ComprobanteCfdiRelacionadosCfdiRelacionado
                            obj1.UUID = dtCabecero.Rows(0).Item("uuid")
                            lstCfdisRel.Add(obj1)

                            Dim oCFDIs As New List(Of VOficinaTimbrador40.ComprobanteCfdiRelacionados)
                            oCFDIs.Add(New VOficinaTimbrador40.ComprobanteCfdiRelacionados)
                            oComprobante.CfdiRelacionados = oCFDIs.ToArray
                            oComprobante.CfdiRelacionados(0).CfdiRelacionado = lstCfdisRel.ToArray
                            oComprobante.CfdiRelacionados(0).TipoRelacion = "04"
                            oComprobante.Version = EmisorVersion
                            oComprobante.Serie = dtCabecero.Rows(0).Item("observ2")
                            oComprobante.Folio = dtCabecero.Rows(0).Item("folio")
                            FechaActual = dtCabecero.Rows(0).Item("fum")
                            oComprobante.Fecha = FechaActual.ToString("yyyy-MM-ddTHH:mm:ss")

                            oComprobante.FormaPago = dtCabecero.Rows(0).Item("forma_pago").ToString.Trim
                            oComprobante.Total = 0 ''Total
                            oComprobante.SubTotal = 0 ''SubTotal
                            oComprobante.Moneda = VOficinaTimbrador40.c_Moneda.MXN
                            oComprobante.MetodoPago = dtCabecero.Rows(0).Item("metodo_pago")
                            oComprobante.LugarExpedicion = dtOtros.Rows(0).Item("FacCP")
                            oComprobante.TipoDeComprobante = VOficinaTimbrador40.c_TipoDeComprobante.I
                            oComprobante.Exportacion = dtCabecero.Rows(0).Item("exportacion")

                            oEmisor = New VOficinaTimbrador40.ComprobanteEmisor

                            oEmisor.Nombre = EmisorNombre
                            oEmisor.RegimenFiscal = EmisorRegimen
                            oEmisor.Rfc = EmisorRfc
                            oReceptor = New VOficinaTimbrador40.ComprobanteReceptor
                            If txt_RFC.Text = “XAXX010101000” Then
                                oReceptor.Nombre = "PUBLICO EN GENERAL"

                                Dim oInfoGlogal As New VOficinaTimbrador40.ComprobanteInformacionGlobal
                                oInfoGlogal.Año = CDate(dtCabecero.Rows(0).Item("fecha")).Year
                                oInfoGlogal.Meses = Format(CDate(dtCabecero.Rows(0).Item("fecha")).Month, "00") ''[Enum].Parse(GetType(VOficinaTimbrador40.c_MetodoPago), Format(CDate(dtCabecero.Rows(0).Item("fecha")).Month, "00"))
                                oInfoGlogal.Periodicidad = "01" ''[Enum].Parse(GetType(VOficinaTimbrador40.c_Periodicidad), EmisorPeriocidad) '''' VOficinaTimbrador40.c_Periodicidad.Item04
                                oComprobante.InformacionGlobal = oInfoGlogal
                            Else
                                oReceptor.Nombre = txt_Nombre.Text.Replace("S DE RL DE CV", "").Replace("SA DE CV", "").Replace("S. DE R.L. DE C.V.", "").Replace("S.A. DE C.V.", "").Trim()
                            End If
                            oReceptor.Rfc = txt_RFC.Text
                            oReceptor.UsoCFDI = [Enum].Parse(GetType(VOficinaTimbrador40.c_UsoCFDI), cbo_UsoCFDI.SelectedValue)
                            oReceptor.DomicilioFiscalReceptor = txt_CP.Text
                            oReceptor.RegimenFiscalReceptor = dtCabecero.Rows(0).Item("regimen_fiscal1")

                            oComprobante.Emisor = oEmisor
                            oComprobante.Receptor = oReceptor
                            lstConceptos = New List(Of VOficinaTimbrador40.ComprobanteConcepto)

                            oImpuestos = New VOficinaTimbrador40.ComprobanteImpuestos
                            oImpuestosTraslado = New List(Of VOficinaTimbrador40.ComprobanteImpuestosTraslado)
                            oImpuestoTraslado00 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestoTraslado08 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestoTraslado16 = New VOficinaTimbrador40.ComprobanteImpuestosTraslado
                            oImpuestos.TotalImpuestosTrasladados = 0.0
                            Dim importetras As Decimal = 0.0
                            Dim importebase00 As Decimal = 0.0
                            Dim importebase08 As Decimal = 0.0
                            Dim importebase16 As Decimal = 0.0
                            Dim importetras00 As Decimal = 0.0
                            Dim importetras08 As Decimal = 0.0
                            Dim importetras16 As Decimal = 0.0
                            Dim importeimpu00 As Decimal = 0.0
                            Dim importeimpu08 As Decimal = 0.0
                            Dim importeimpu16 As Decimal = 0.0
                            Dim importedesc As Decimal = 0.0
                            Dim conceptoimporte As Decimal = 0.0
                            Dim Sw_Traslado00 As Boolean = False
                            Dim SumaSubTotal As Decimal = 0.0
                            For Each dtRenglon As DataRow In dtDetalle.Rows

                                oConcepto = New VOficinaTimbrador40.ComprobanteConcepto
                                oConcepto.ClaveProdServ = dtRenglon("unidadvta")
                                oConcepto.Cantidad = dtRenglon("cantidad")
                                oConcepto.ClaveUnidad = [Enum].Parse(GetType(VOficinaTimbrador40.c_ClaveUnidad), dtRenglon("claveunidad")) ''VOficinaTimbrador40.c_ClaveUnidad.H87
                                oConcepto.Descripcion = dtRenglon("concepto")
                                oConcepto.ValorUnitario = IIf(dtRenglon("cantidad") <> 0, Math.Round(dtRenglon("importe1") / dtRenglon("cantidad"), iRedondeo6), 0.0)
                                oConcepto.ValorUnitario = Math.Round(oConcepto.ValorUnitario, iRedondeo6)
                                oConcepto.Importe = Globales.oAmbientes.Redondear(dtRenglon("importe1"), iRedondeo4)
                                oConcepto.Descuento = Math.Round(dtRenglon("descuento"), iRedondeo2)
                                oConcepto.ObjetoImp = dtRenglon("objetoimp")

                                importedesc += oConcepto.Descuento ''Math.Round(dtRenglon("descuento"), iRedondeo6)

                                oConceptoImpuestos = New VOficinaTimbrador40.ComprobanteConceptoImpuestos
                                oConceptoImpuestosTraslados = New List(Of VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado)
                                oConceptoImpuestosTraslado = New VOficinaTimbrador40.ComprobanteConceptoImpuestosTraslado
                                oConceptoImpuestosTraslado.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002 ''ESTE ES IVA
                                oConceptoImpuestosTraslado.Base = oConcepto.Importe ''Math.Round(dtRenglon("importe1") - dtRenglon("descuento"), iRedondeo6)

                                oConceptoImpuestosTraslado.Importe = Math.Round(dtRenglon("impuesto_iva"), iRedondeo2)

                                conceptoimporte += oConceptoImpuestosTraslado.Importe
                                Select Case Math.Round(dtRenglon("tasa_iva"), iRedondeo6)
                                    Case 0.16
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase16 += oConceptoImpuestosTraslado.Base
                                        importeimpu16 += oConceptoImpuestosTraslado.Importe
                                    Case 0.08
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_08
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase08 += oConceptoImpuestosTraslado.Base
                                        importeimpu08 += oConceptoImpuestosTraslado.Importe
                                    Case 0.0
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_00
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase00 += oConceptoImpuestosTraslado.Base
                                        importeimpu00 += oConceptoImpuestosTraslado.Importe
                                        Sw_Traslado00 = True
                                    Case Else
                                        oConceptoImpuestosTraslado.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                                        oConceptoImpuestosTraslado.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa
                                        importebase16 += oConceptoImpuestosTraslado.Base
                                        importeimpu16 += oConceptoImpuestosTraslado.Importe
                                End Select
                                oComprobante.SubTotal += Math.Round(oConcepto.Importe, iRedondeo2)
                                oComprobante.Total += oConcepto.Importe + oConceptoImpuestosTraslado.Importe

                                oConceptoImpuestosTraslados.Add(oConceptoImpuestosTraslado)
                                oConceptoImpuestos.Traslados = oConceptoImpuestosTraslados.ToArray()
                                If Not (oConcepto.ObjetoImp = "01" Or oConcepto.ObjetoImp = "03") Then
                                    oConcepto.Impuestos = oConceptoImpuestos
                                End If

                                lstConceptos.Add(oConcepto)
                                oComprobante.Conceptos = lstConceptos.ToArray
                            Next
                            oComprobante.Descuento = importedesc

                            oImpuestoTraslado16.Base = Math.Round(importebase16, iRedondeo2)
                            oImpuestoTraslado16.Importe = Math.Round(importeimpu16, iRedondeo2)
                            oImpuestoTraslado16.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado16.TasaOCuota = VOficinaTimbrador1.ValorIVA_16
                            oImpuestoTraslado16.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            oImpuestoTraslado08.Base = Math.Round(importebase08, iRedondeo2)
                            oImpuestoTraslado08.Importe = Math.Round(importeimpu08, iRedondeo2)
                            oImpuestoTraslado08.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado08.TasaOCuota = VOficinaTimbrador1.ValorIVA_08
                            oImpuestoTraslado08.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            oImpuestoTraslado00.Base = Math.Round(importebase00, iRedondeo2)
                            oImpuestoTraslado00.Importe = Math.Round(importeimpu00, iRedondeo2)
                            oImpuestoTraslado00.Impuesto = VOficinaTimbrador40.c_Impuesto.Item002
                            oImpuestoTraslado00.TasaOCuota = VOficinaTimbrador1.ValorIVA_00
                            oImpuestoTraslado00.TipoFactor = VOficinaTimbrador40.c_TipoFactor.Tasa

                            If importeimpu16 > 0 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado16)
                            End If
                            If importeimpu08 > 0 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado08)
                            End If
                            If Sw_Traslado00 Then
                                oImpuestosTraslado.Add(oImpuestoTraslado00)
                            End If
                            oImpuestos.TotalImpuestosTrasladados = Math.Round(oImpuestoTraslado16.Importe + oImpuestoTraslado08.Importe + oImpuestoTraslado00.Importe, iRedondeo4)
                            If oImpuestosTraslado.Count > 0 Then
                                oImpuestos.Traslados = oImpuestosTraslado.ToArray()
                                oComprobante.Impuestos = oImpuestos
                            End If

                            importetras += oImpuestos.TotalImpuestosTrasladados

                            oComprobante.Descuento = Math.Round(importedesc, iRedondeo2)

                            oComprobante.SubTotal = Math.Round(oComprobante.SubTotal, iRedondeo2)
                            oComprobante.Total = Math.Abs(oComprobante.Total)
                            oComprobante.SubTotal = Math.Abs(oComprobante.SubTotal)
                            oComprobante.Descuento = Math.Abs(oComprobante.Descuento)
                            oComprobante.Total = Math.Round(oComprobante.SubTotal - oComprobante.Descuento + oImpuestos.TotalImpuestosTrasladados, iRedondeo2)

                            Dim difTotal As Double = 0
                            Dim iFactor As Integer = 1
                            Dim difBase16 As Decimal = 0
                            Dim difBase08 As Decimal = 0
                            Dim difBase00 As Decimal = 0

                            difTotal = Math.Abs(Math.Round(dtCabecero.Rows(0).Item("totaltc"), 2)) - oComprobante.Total

                            VOficinaTimbrador1.Tipo = "Factura"
                            Dim oPagos As VOficinaTimbrador40.Pagos = Nothing
                            VOficinaTimbrador1.Sellar(oComprobante, LogTrimbrado, oPagos)
                            If VOficinaTimbrador1.ErrorMessage = "" Then
                                VOficinaTimbrador1.Timbrar(LogTrimbrado)

                                If (VOficinaTimbrador1.ErrorMessage = "") Then
                                    VOficinaTimbrador1.SMTPHost = EmisorSMTPHost
                                    VOficinaTimbrador1.SMTPUsername = EmisorSMTPUsername
                                    VOficinaTimbrador1.SMTPPassword = EmisorSMTPPassword
                                    VOficinaTimbrador1.SMTPPort = EmisorSMTPPort
                                    If oDatos.Actualiza_TicketFactura_CFDI_Gen(cn, ControlTransaccion, v_Empresa, v_Sucursal, AutoNumSuc_DEV, email_enviado, "FACTURADO", oComprobante.SubTotal, oComprobante.Total, VOficinaTimbrador1.TFD.UUID, VOficinaTimbrador1.XML, VOficinaTimbrador1.TFD.noCertificadoSAT, VOficinaTimbrador1.TFD.selloCFD, VOficinaTimbrador1.TFD.selloSAT, VOficinaTimbrador1.TFD.CadenaOriginalComplemento, VOficinaTimbrador1.TFD.version, VOficinaTimbrador1.TFD.FechaTimbrado, VOficinaTimbrador1.TFD.RfcProvCertif, VOficinaTimbrador1.NoCertificado, "", "04", dtCabecero.Rows(0).Item("uuid"), Msj) Then
                                        MessageBox.Show("Venta Refacturada con exito.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                                        Dim xmlDoc As System.Xml.XmlDocument
                                        xmlDoc = New System.Xml.XmlDocument()
                                        xmlDoc.LoadXml(VOficinaTimbrador1.XML)
                                        If Not IO.Directory.Exists("C: \Windows\Temp\F" & AutoNumSuc_DEV) Then
                                            IO.Directory.CreateDirectory("C:\Windows\Temp\F" & AutoNumSuc_DEV)
                                        End If
                                        xmlDoc.Save("C:\Windows\Temp\F" & AutoNumSuc_DEV & "\" & AutoNumSuc_DEV & ".xml")
                                        If oDatos.Recupera_TicketFactura(cn, ControlTransaccion, v_Empresa, v_Sucursal, AutoNumSuc_DEV, dtCabecero, dtDetalle, dtOtros, Msj) Then
                                            If dtCabecero.Rows.Count > 0 Then
                                                sw_continuar = True
                                                Mensaje = "Exito"
                                            Else
                                                sw_continuar = False
                                                Mensaje = "Autonumsuc (" & AutoNumSuc_DEV & ") Factura no encontrada"
                                            End If
                                        Else
                                            sw_continuar = False
                                            Mensaje = "Error Recupera_TicketFactura " & Msj
                                        End If
                                    Else
                                        sw_continuar = False
                                        Mensaje = "Error al actualizar Factura " & Msj
                                    End If
                                Else
                                    sw_continuar = False
                                    Mensaje = "El timbrador dijo: " & vbNewLine & VOficinaTimbrador1.ErrorMessage
                                End If
                            Else
                                sw_continuar = False
                                Mensaje = "Sellar dijo: " & vbNewLine & VOficinaTimbrador1.ErrorMessage
                            End If
                        Else
                            sw_continuar = False
                            Mensaje = "No se encontraron datos del emisor para facturar para la empresa ('" & v_Empresa & "')"
                        End If
                    Else
                        sw_continuar = False
                        Mensaje = Msj
                    End If
                Else
                    sw_continuar = False
                    Mensaje = "No es posuble obtener la fecha (PVTA_Sat_CFDI_FechaActual)"
                End If
            Else
                    sw_continuar = False
                Mensaje = "Debe introducir un correo electronico."
            End If
        Catch ex As Exception
            sw_continuar = False
            Mensaje = ex.Message
        End Try
        Return sw_continuar
    End Function
    Private Sub LimpiarPantalla()
        txt_Autonumsuc.Text = ""
        txt_Cliente.Text = ""
        txt_Nombre.Text = ""
        txt_RFC.Text = ""
        txt_Calle.Text = ""
        txt_CP.Text = ""
        txt_Colonia.Text = ""
        txt_Estado.Text = ""
        txt_Municipio.Text = ""
        txt_Serie.Text = "FA"
        txt_Folio.Text = ""
        txt_SubTotal.Text = "0"
        txt_IVA.Text = "0"
        txt_Descuento.Text = "0"
        txt_Total.Text = "0"
        cbo_Regimen.SelectedIndex = 0
        cbo_FormaPago.SelectedIndex = 0
        cbo_UsoCFDI.SelectedIndex = 0
        txt_Correos.Text = ""
        GridC_1.DataSource = Nothing
        GridC_1.RefreshDataSource()
        gcDatosTicket.Enabled = True
    End Sub

    Private Sub btn_CambiarCliente_Click(sender As Object, e As EventArgs) Handles btn_CambiarCliente.Click
        Dim oForma As pdvCuentas
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtLugEntregas As DataTable = Nothing
        Dim Msj As String = ""
        Dim sTipoCredito As String = ""
        Dim Id_dir As Integer = 0
        Dim dtAuxCabecero As DataTable = Nothing
        Try
            oForma = New pdvCuentas
            oForma.Tipo = ""
            oForma.TipoForma = pdvCuentas.eTipoForma.Cliente
            oForma.ShowDialog()

            oDatos = New Datos_Viscoi
            If Not cbo_PlanCredito.SelectedValue Is Nothing Then
                sTipoCredito = cbo_PlanCredito.SelectedValue
            End If
            If oDatos.Actualiza_TicketFactura_Cuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, oForma.Cuenta, oForma.Nombre, oForma.Rfc, oForma.Direccion, oForma.CodigoPostal, oForma.Colonia, oForma.Ciudad, oForma.Estado, oForma.EMail, txt_OrdenCompra.Text, sTipoCredito, Msj) Then
                txt_Cliente.Text = oForma.Cuenta
                txt_Nombre.Text = oForma.Nombre
                txt_RFC.Text = oForma.Rfc

                txt_Calle.Text = oForma.Direccion
                txt_CP.Text = oForma.CodigoPostal
                txt_Colonia.Text = oForma.Colonia
                txt_Municipio.Text = oForma.Ciudad
                txt_Estado.Text = oForma.Estado
                txt_Correos.Text = oForma.EMail

                txt_Cliente.Text = txt_Cliente.Text.Trim()
                txt_Nombre.Text = txt_Nombre.Text.Trim()
                txt_RFC.Text = txt_RFC.Text.Trim()
                txt_Calle.Text = txt_Calle.Text.Trim()
                txt_Colonia.Text = txt_Colonia.Text.Trim()
                txt_CP.Text = txt_CP.Text.Trim()
                txt_Municipio.Text = txt_Municipio.Text.Trim()
                txt_Estado.Text = txt_Estado.Text.Trim()

                txt_Serie.Text = txt_Serie.Text.Trim()
                txt_Folio.Text = txt_Folio.Text.Trim()
                txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                txt_IVA.Text = txt_IVA.Text.Trim()
                txt_Descuento.Text = txt_Descuento.Text.Trim()
                txt_Total.Text = txt_Total.Text.Trim()

                txt_Correos.Text = txt_Correos.Text.Trim()

                If oDatos.Recupera_TicketFactura(Nothing, Nothing, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtAuxCabecero, Nothing, Nothing, Msj) Then
                    If dtAuxCabecero.Rows.Count > 0 Then
                        cbo_Regimen.SelectedValue = dtAuxCabecero.Rows(0).Item("regimen_fiscal1")
                    Else
                        cbo_Regimen.SelectedIndex = 0
                    End If
                End If

                If txt_Cliente.Text <> "" Then
                    btn_CambiarCliente.Enabled = False
                    btn_NuevoCliente.Enabled = False
                End If

                If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                    GridC_1.DataSource = dtLugEntregas
                    If dtLugEntregas.Rows.Count > 0 Then
                        GridV_1.FocusedRowHandle = Id_dir
                    End If
                End If
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_CambiarDir_Click(sender As Object, e As EventArgs) Handles btn_CambiarDir.Click
        Try
            PnlDatosDirFactura.Enabled = True
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    Private Sub btn_GuardarDir_Click(sender As Object, e As EventArgs) Handles btn_GuardarDir.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim sTipoCredito As String = ""
        Try
            oDatos = New Datos_Viscoi
            If Not cbo_PlanCredito.SelectedValue Is Nothing Then
                sTipoCredito = cbo_PlanCredito.SelectedValue
            End If
            If oDatos.Actualiza_TicketFactura_Cuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, txt_Cliente.Text, txt_Nombre.Text, txt_RFC.Text, txt_Calle.Text, txt_CP.Text, txt_Colonia.Text, txt_Municipio.Text, txt_Estado.Text, txt_Correos.Text, txt_OrdenCompra.Text, sTipoCredito, Msj) Then
                PnlDatosDirFactura.Enabled = False

                txt_Cliente.Text = txt_Cliente.Text.Trim()
                txt_Nombre.Text = txt_Nombre.Text.Trim()
                txt_RFC.Text = txt_RFC.Text.Trim()
                txt_Calle.Text = txt_Calle.Text.Trim()
                txt_Colonia.Text = txt_Colonia.Text.Trim()
                txt_CP.Text = txt_CP.Text.Trim()
                txt_Municipio.Text = txt_Municipio.Text.Trim()
                txt_Estado.Text = txt_Estado.Text.Trim()

                txt_Serie.Text = txt_Serie.Text.Trim()
                txt_Folio.Text = txt_Folio.Text.Trim()
                txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                txt_IVA.Text = txt_IVA.Text.Trim()
                txt_Descuento.Text = txt_Descuento.Text.Trim()
                txt_Total.Text = txt_Total.Text.Trim()

                txt_Correos.Text = txt_Correos.Text.Trim()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub btn_NuevoDireccion_Click(sender As Object, e As EventArgs) Handles btn_NuevoDireccion.Click
        Dim oForma As pdvDireccionEntrega
        Dim id_dir As Integer
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtLugEntregas As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oForma = New pdvDireccionEntrega
            oForma.txt_Cliente.Text = txt_Cliente.Text
            oForma.ShowDialog()
            id_dir = oForma.IdDir
            oDatos = New Datos_Viscoi
            If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                GridC_1.DataSource = dtLugEntregas
                If dtLugEntregas.Rows.Count > 0 Then
                    GridV_1.FocusedRowHandle = id_dir
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Me.Close()
    End Sub
    Private Sub pdvFactura_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Call btn_Cancelar_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_NuevoCliente_Click(sender As Object, e As EventArgs) Handles btn_NuevoCliente.Click
        Dim id_dir As Integer
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtLugEntregas As DataTable = Nothing
        Dim dtAuxCabecero As DataTable = Nothing
        Dim Msj As String = ""
        Dim sTipoCredito As String = ""
        Try
            Dim oForma As pdvCuentaEventual
            oForma = New pdvCuentaEventual
            oForma.StartPosition = FormStartPosition.CenterScreen
            oForma.ShowDialog()
            oDatos = New Datos_Viscoi
            If oForma.txt_Cliente.Text <> "" Then
                If Not cbo_PlanCredito.SelectedValue Is Nothing Then
                    sTipoCredito = cbo_PlanCredito.SelectedValue
                End If
                If oDatos.Actualiza_TicketFactura_Cuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, oForma.txt_Cliente.Text, oForma.txt_Nombre.Text, oForma.txt_RFC.Text _
                                                    , oForma.txt_Calle.Text & " E:" & oForma.txt_NumExt.Text & IIf(oForma.txt_NumInt.Text.Length > 0, " I:" & oForma.txt_NumInt.Text, "") _
                                                    , oForma.txt_CP.Text, oForma.txt_Colonia.Text, oForma.txt_Municipio.Text, oForma.txt_Estado.Text, oForma.txt_EMail.Text, txt_OrdenCompra.Text, sTipoCredito, Msj) Then
                    txt_Cliente.Text = oForma.txt_Cliente.Text
                    txt_Nombre.Text = oForma.txt_Nombre.Text
                    txt_RFC.Text = oForma.txt_RFC.Text

                    txt_Calle.Text = oForma.txt_Calle.Text & " E:" & oForma.txt_NumExt.Text & IIf(oForma.txt_NumInt.Text.Length > 0, " I:" & oForma.txt_NumInt.Text, "")
                    txt_CP.Text = oForma.txt_CP.Text
                    txt_Colonia.Text = oForma.txt_Colonia.Text
                    txt_Municipio.Text = oForma.txt_Municipio.Text
                    txt_Estado.Text = oForma.txt_Estado.Text

                    txt_Cliente.Text = txt_Cliente.Text.Trim()
                    txt_Nombre.Text = txt_Nombre.Text.Trim()
                    txt_RFC.Text = txt_RFC.Text.Trim()
                    txt_Calle.Text = txt_Calle.Text.Trim()
                    txt_Colonia.Text = txt_Colonia.Text.Trim()
                    txt_CP.Text = txt_CP.Text.Trim()
                    txt_Municipio.Text = txt_Municipio.Text.Trim()
                    txt_Estado.Text = txt_Estado.Text.Trim()

                    txt_Serie.Text = txt_Serie.Text.Trim()
                    txt_Folio.Text = txt_Folio.Text.Trim()
                    txt_SubTotal.Text = txt_SubTotal.Text.Trim()
                    txt_IVA.Text = txt_IVA.Text.Trim()
                    txt_Descuento.Text = txt_Descuento.Text.Trim()
                    txt_Total.Text = txt_Total.Text.Trim()

                    txt_Correos.Text = oForma.txt_EMail.Text.Trim()

                    If oDatos.Recupera_TicketFactura(Nothing, Nothing, txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, dtAuxCabecero, Nothing, Nothing, Msj) Then
                        If dtAuxCabecero.Rows.Count > 0 Then
                            cbo_Regimen.SelectedValue = dtAuxCabecero.Rows(0).Item("regimen_fiscal1")
                        Else
                            cbo_Regimen.SelectedIndex = 0
                        End If
                    End If

                    If oDatos.Recupera_CuentasLugarEntrega(txt_Empresa.Text, txt_Cliente.Text, dtLugEntregas, Msj) Then
                        GridC_1.DataSource = dtLugEntregas
                        If dtLugEntregas.Rows.Count > 0 Then
                            GridV_1.FocusedRowHandle = id_dir
                        End If
                    End If
                Else
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub Imprimir(ByVal AS_Dev As String, ByVal dtCabecero As DataTable, ByVal dtDetalle As DataTable, ByVal dtOtros As DataTable)
        Dim Formularios As Globales.clsFormularios
        Dim ForamtoOC As Object = Nothing
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oFacturas As New Facturas
        Dim Mensaje As String = ""
        Dim oDatos_Supply As New Datos_Viscoi

        Try
            oFacturas.Fill(dtCabecero, dtDetalle, dtOtros)

            Select Case txt_Empresa.Text
                Case "PAPELERA"
                    ForamtoOC = New XtraRep_FacturaCuadros
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).lbl_OC_CFDIREL.Text = "CFDIS RELACIONADO"
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Empresa = oFacturas.FacEmpresa
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Direccion = oFacturas.FacDireccion.Replace("|", vbNewLine)
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).LugExpedicion = oFacturas.LugExpedicion
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Fecha = Format(oFacturas.Fecha, "dd/MMM/yyyy")
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Importe = Format(oFacturas.Total, "C2")
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).Letra = oFacturas.Letra
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).FacCP = oFacturas.FacCP
                    TryCast(ForamtoOC, XtraRep_FacturaCuadros).odsDatosFacturas.DataSource = oFacturas
                Case Else
                    ForamtoOC = New XtraRep_FacturaBase
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Empresa = oFacturas.FacEmpresa
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Direccion = oFacturas.FacDireccion.Replace("|", vbNewLine)
                    TryCast(ForamtoOC, XtraRep_FacturaBase).LugExpedicion = oFacturas.LugExpedicion
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Fecha = Format(oFacturas.Fecha, "dd/MMM/yyyy")
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Importe = Format(oFacturas.Total, "C2")
                    TryCast(ForamtoOC, XtraRep_FacturaBase).Letra = oFacturas.Letra
                    TryCast(ForamtoOC, XtraRep_FacturaBase).FacCP = oFacturas.FacCP
                    TryCast(ForamtoOC, XtraRep_FacturaBase).odsDatosFacturas.DataSource = oFacturas
            End Select
            If Not ForamtoOC Is Nothing Then
                printTool = New DevExpress.XtraReports.UI.ReportPrintTool(ForamtoOC)
                printTool.ShowPreviewDialog()
                If Not IO.Directory.Exists("C:\Windows\Temp\F" & AS_Dev) Then
                    IO.Directory.CreateDirectory("C:\Windows\Temp\F" & AS_Dev)
                End If
                ForamtoOC.ExportToPdf("C:\Windows\Temp\F" & AS_Dev & "\" & AS_Dev & ".pdf")
            Else
                MessageBox.Show("Formato no registrado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
        oDatos_Supply = Nothing
    End Sub
    Private Sub SendMail(ByVal var_MailUser As String _
                         , ByVal var_MailPass As String _
                         , ByVal var_MailHost As String _
                         , ByVal var_MailPort As String _
                         , ByVal var_MailFrom As String _
                         , ByVal var_MailDestinos As String _
                         , ByVal var_Archivo As String _
                         , ByVal Ticket As String)
        Dim Smtp_Server As New System.Net.Mail.SmtpClient
        Dim e_mail As New System.Net.Mail.MailMessage()
        Dim txt_destinos As New TextBox
        Try
            If var_MailUser <> "" And var_MailPass <> "" _
                And var_MailPort <> "" And var_MailHost <> "" _
                And var_MailFrom <> "" And var_MailDestinos <> "" Then

                Smtp_Server.UseDefaultCredentials = False
                Smtp_Server.Credentials = New Net.NetworkCredential(var_MailUser, var_MailPass)
                Smtp_Server.Port = var_MailPort
                Smtp_Server.EnableSsl = True
                Smtp_Server.Host = var_MailHost

                e_mail = New System.Net.Mail.MailMessage()
                e_mail.From = New System.Net.Mail.MailAddress(var_MailFrom)
                txt_destinos.Multiline = True

                For Each dest As String In var_MailDestinos.Split(";")
                    e_mail.To.Add(dest)
                Next
                'e_mail.CC.Add(txtTo.Text)
                e_mail.IsBodyHtml = True
                Select Case txt_Empresa.Text
                    Case "PAPELERA"
                        e_mail.Subject = "Papelera Del Norte: Envio de comprobante fiscal digital a traves de Internet" & Ticket & ""
                        e_mail.Body = "Estimado cliente"
                        e_mail.Body = e_mail.Body & "<BR>" & "Se adjunta a este e-mail archivo ZIP conteniendo los archivos XML y PDF del comprobante fiscal digital a traves de internet (CFDI) correspondiente al ComprobanteFolio" & Ticket & "."
                        e_mail.Body = e_mail.Body & "<BR>" & "Gracias."
                        e_mail.Body = e_mail.Body & "<BR>" & "Papelera Del Norte De La Laguna, S.A. de C.V."
                    Case Else
                        e_mail.Subject = "VOFICNA - Trimbrador"
                        e_mail.Body = "El sistema VOFICNA Envia factura"
                End Select

                If IO.File.Exists(var_Archivo & ".zip") Then
                    IO.File.Delete(var_Archivo & ".zip")
                End If
                System.IO.Compression.ZipFile.CreateFromDirectory(var_Archivo, var_Archivo & ".zip", System.IO.Compression.CompressionLevel.Optimal, False)

                Dim attachfile As New System.Net.Mail.Attachment(var_Archivo & ".zip")
                e_mail.Attachments.Add(attachfile)

                Smtp_Server.Send(e_mail)
                e_mail.Dispose()
                Smtp_Server.Dispose()

                MessageBox.Show("Se ha enviado a los correos proporcionados.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_Empresa_LostFocus(sender As Object, e As EventArgs) Handles txt_Empresa.LostFocus
        If txt_Empresa.Text = "ICC" Then
            lblOC.Visible = True
            txt_OrdenCompra.Visible = True
        Else
            lblOC.Visible = False
            txt_OrdenCompra.Visible = False
        End If
    End Sub

    Private Sub txt_Folio_TextChanged(sender As Object, e As EventArgs) Handles txt_Folio.TextChanged
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim sw_continuar As Boolean = True
        Try
            oDatos = New Datos_Viscoi
            Dim Folio As Integer
            If txt_Folio.Text <> "" Then

                Folio = Integer.Parse(txt_Folio.Text)
                If cbo_UsoCFDI.SelectedValue IsNot Nothing Then
                    If Not oDatos.Actualiza_TicketFactura_DatosCFDI(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, cbo_Regimen.SelectedValue, cbo_UsoCFDI.SelectedValue, cbo_FormaPago.SelectedValue, Folio, Msj) Then
                        sw_continuar = False
                        MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub pdvFactura_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.WindowState = FormWindowState.Maximized
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    Private Sub cmd_RefrescaDatos_Click(sender As Object, e As EventArgs) Handles cmd_RefrescaDatos.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi

            If Not oDatos.ACtualiza_TicketFactura_DatosCuenta(txt_Empresa.Text, cbo_Sucursal.Text, txt_Autonumsuc.Text, Msj) Then
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            If txt_Autonumsuc.Text <> "" Then
                Call btn_Buscar_Click(Nothing, Nothing)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub RG1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RG1.SelectedIndexChanged
        cbo_Regimen.Enabled = False
        cbo_FormaPago.Enabled = False
        txt_Serie.Enabled = False
        txt_Folio.Enabled = False
        cbo_PlanCredito.Enabled = False
        cbo_UsoCFDI.Enabled = False
        If RG1.SelectedIndex = 0 Then
            
        End If
        If RG1.SelectedIndex = 1 Then
            cbo_Regimen.Enabled = True
            cbo_UsoCFDI.Enabled = True
        End If
        Call btn_Buscar_Click(Nothing, Nothing)
    End Sub
End Class