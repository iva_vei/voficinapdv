﻿Public Class pdvConfigActualizaVersion
    Dim _Accion As Boolean


    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim RutaDrobox As String = ""
        Dim RutaSincro As String = ""
        Try
            RutaDrobox = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "RutaDrobox", "")
            RutaSincro = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "RutaSincro", "")
            txt_VersionDropBox.Text = RutaDrobox
            txt_RutaSincronizador.Text = RutaSincro
        Catch ex As Exception
        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim sw_continuar As Boolean = True
        Dim Requeridos As Double = 0
        Dim Disponible As Double = 0

        Try
            _Accion = True
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "RutaDrobox", txt_VersionDropBox.Text)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VoficinaPDV\", "RutaSincro", txt_RutaSincronizador.Text)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

    End Sub
End Class