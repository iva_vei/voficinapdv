﻿Imports DevExpress.XtraBars
Public Class pdvCajeros
    Private Modo As Globales.TipoABC
    Public Sub New()
        InitializeComponent()
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControl.OptionsFocus.EnableAutoTabOrder = False
        Me.GridV_1.Appearance.Row.ForeColor = System.Drawing.Color.Black
    End Sub
    '   Private Sub ABCPantallas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    '       Dim oDatos As New Datos_Viscoi
    '       Dim dtDatos As DataTable = Nothing
    '       Dim msj As String = ""
    '       Try
    '           Modo = Globales.TipoABC.Indice
    '           tnpPage2.Visible = False
    '           tnpPage2.Enabled = False
    '
    '           If oDatos.PVTA_Recupera_Sucursales(Globales.oAmbientes.Id_Empresa, "", dtDatos, msj) Then
    '               cbo_Sucursal.DataSource = dtDatos
    '               If Globales.oAmbientes.Id_Sucursal <> "" Then
    '                   cbo_Sucursal.Text = Globales.oAmbientes.Id_Sucursal
    '                   cbo_Sucursal.Enabled = False
    '               Else
    '                   cbo_Sucursal.SelectedIndex = 0
    '                   cbo_Sucursal.Enabled = True
    '               End If
    '           End If
    '
    '           Call ControlBotones("Ok")
    '           Call Botones("Ok")
    '           Call Refrescar()
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       oDatos = Nothing
    '   End Sub
    '   Public Sub Refrescar()
    '       Select Case TabPane1.SelectedPage.Caption
    '           Case tnpPage1.Caption
    '               Call Mostrar_Indice()
    '           Case tnpPage2.Caption
    '               Call Mostrar_Detalle(Modo)
    '       End Select
    '   End Sub
    '   Public Sub Imprimir()
    '       Dim Formularios As Globales.clsFormularios
    '       Try
    '           If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "IMPRIMIR") Then
    '               MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '               Return
    '           End If
    '
    '           Formularios = New Globales.clsFormularios
    '           Formularios.Imprimir(Me.Name, "Formulario Plantilla", GridC_1)
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       Formularios = Nothing
    '   End Sub
    '   Public Sub Excel()
    '       Dim Formularios As Globales.clsFormularios
    '       Try
    '           If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "EXCEL") Then
    '               MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '               Return
    '           End If
    '
    '           Formularios = New Globales.clsFormularios
    '           Formularios.ExportarXLS(Me.Name, "Formulario Plantilla", GridC_1, Globales.oAmbientes.Sistema, Application.ProductName, "ICC")
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       Formularios = Nothing
    '   End Sub
    '   Public Sub Regresar()
    '       Try
    '           Select Case TabPane1.SelectedPage.Caption
    '               Case tnpPage1.Caption
    '                   Me.Close()
    '               Case tnpPage2.Caption
    '                   TabPane1.SelectedPage = tnpPage1
    '                   tnpPage1.Enabled = True
    '                   tnpPage2.Enabled = False
    '                   Modo = Globales.TipoABC.Indice
    '                   Call Titulo_BtnPanel(False)
    '                   Call Botones("Cancelar")
    '                   Call Refrescar()
    '           End Select
    '
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '   End Sub
    '   Private Sub Titulo_BtnPanel(ByVal sw_visible)
    '       PanelFiltros_Acciones.Visible = sw_visible
    '       If PanelFiltros_Acciones.Visible = True Then
    '           btn_OcultarMostrar_Acciones.Text = "Ocultar Filtros y Acciones"
    '       Else
    '           btn_OcultarMostrar_Acciones.Text = "Mostrar Filtros y Acciones"
    '       End If
    '   End Sub
    '   Private Sub btn_OcultarMostrar_Acciones_Click(sender As Object, e As EventArgs) Handles btn_OcultarMostrar_Acciones.Click
    '       Call Titulo_BtnPanel(Not PanelFiltros_Acciones.Visible)
    '   End Sub
    '
    '   Private Sub Botones(ByVal tag As String)
    '       Dim btnNuevo As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Nuevo")
    '       Dim btnEditar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Editar")
    '       Dim btnEliminar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Eliminar")
    '       Dim btnOk As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Ok")
    '       Dim btnCancelar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Cancelar")
    '
    '       Select Case tag
    '           Case "Nuevo", "Editar", "Eliminar", "Consulta"
    '               btnNuevo.Visible = False
    '               btnEditar.Visible = False
    '               btnEliminar.Visible = False
    '               btnOk.Visible = True
    '               btnCancelar.Visible = True
    '
    '           Case "Ok", "Cancelar", "Consulta"
    '               btnNuevo.Visible = True
    '               btnEditar.Visible = True
    '               btnEliminar.Visible = True
    '               btnOk.Visible = False
    '               btnCancelar.Visible = False
    '       End Select
    '   End Sub
    '   Private Sub ControlBotones(ByVal tag As String)
    '       Dim Msj As String = ""
    '       Dim oDatos As New Datos_Viscoi
    '       Dim pass As String = ""
    '       Dim IdUsuario As Integer = 0
    '       Try
    '           Select Case tag
    '               Case "Nuevo"
    '                   If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "INSERTARDATOS") Then
    '                       MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                       Return
    '                   End If
    '
    '                   Call Botones(tag)
    '                   Modo = Globales.TipoABC.Nuevo
    '                   TabPane1.SelectedPage = tnpPage2
    '                   Call Refrescar()
    '               Case "Editar"
    '                   If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "ACTUALIZARDATOS") Then
    '                       MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                       Return
    '                   End If
    '
    '                   Call Botones(tag)
    '                   Modo = Globales.TipoABC.Modificar
    '                   TabPane1.SelectedPage = tnpPage2
    '                   Call Refrescar()
    '               Case "Eliminar"
    '                   If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "CANCELARDATOS") Then
    '                       MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                       Return
    '                   End If
    '
    '                   Call Botones(tag)
    '                   Modo = Globales.TipoABC.Eliminar
    '                   TabPane1.SelectedPage = tnpPage1
    '                   Call Refrescar()
    '               Case "Consulta"
    '                   Call Botones(tag)
    '                   Modo = Globales.TipoABC.Consulta
    '                   TabPane1.SelectedPage = tnpPage2
    '                   Call Refrescar()
    '               Case "Cancelar"
    '                   Call Botones(tag)
    '                   Call Regresar()
    '               Case "Ok"
    '                   Select Case Modo
    '                       Case Globales.TipoABC.Nuevo
    '                           If MessageBox.Show("¿Desea guardar la información del nuevo Caja?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
    '                               If oDatos.PVTA_Inserta_Cajas(Globales.oAmbientes.Id_Empresa _
    '                                                           , cbo_Sucursal.Text _
    '                                                           , txt1_IdPVenta.Text _
    '                                                           , txt1_Cajero.Text _
    '                                                           , txt1_Nombre.Text _
    '                                                           , txt1_IdUsuario.Text _
    '                                                           , Globales.oAmbientes.oUsuario.Id_usuario _
    '                                                           , txt1_Nombre1.Text _
    '                                                           , txt1_Puerto1.Text _
    '                                                           , txt1_Driver1.Text _
    '                                                           , txt1_Nombre2.Text _
    '                                                           , txt1_Puerto2.Text _
    '                                                           , txt1_Driver2.Text _
    '                                                           , txt1_Nombre3.Text _
    '                                                           , txt1_Puerto3.Text _
    '                                                           , txt1_Driver3.Text _
    '                                                           , Msj) Then
    '                                   Call Botones(tag)
    '                                   Call Regresar()
    '                               Else
    '                                   MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                               End If
    '                           End If
    '                       Case Globales.TipoABC.Eliminar
    '                           'If MessageBox.Show("¿Desea Desactivar al usuario?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
    '                           '    IdUsuario = Globales.oAmbientes.Valor(txt_IdUsuario.Text)
    '                           '    If oDatos.Seguridad_Usuarios_ABC("B", IdUsuario, txt_Usuario.Text, txt_Nombre.Text, txt_pass.Text, cbo_Perfiles.SelectedValue, CDate(txt_fecha_alta.Text), dtx_fecha_vence.DateTime, 0, Msj) Then
    '                           '        Call Botones(tag)
    '                           '        Call Regresar()
    '                           '    Else
    '                           '        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                           '    End If
    '                           'End If
    '                       Case Globales.TipoABC.Modificar
    '                           If MessageBox.Show("¿Desea modificar la información del nuevo Caja?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
    '                               If oDatos.PVTA_Inserta_Cajas(Globales.oAmbientes.Id_Empresa _
    '                                                           , cbo_Sucursal.Text _
    '                                                           , txt1_IdPVenta.Text _
    '                                                           , txt1_Cajero.Text _
    '                                                           , txt1_Nombre.Text _
    '                                                           , txt1_IdUsuario.Text _
    '                                                           , Globales.oAmbientes.oUsuario.Id_usuario _
    '                                                           , txt1_Nombre1.Text _
    '                                                           , txt1_Puerto1.Text _
    '                                                           , txt1_Driver1.Text _
    '                                                           , txt1_Nombre2.Text _
    '                                                           , txt1_Puerto2.Text _
    '                                                           , txt1_Driver2.Text _
    '                                                           , txt1_Nombre3.Text _
    '                                                           , txt1_Puerto3.Text _
    '                                                           , txt1_Driver3.Text _
    '                                                           , Msj) Then
    '                                   Call Botones(tag)
    '                                   Call Regresar()
    '                               Else
    '                                   MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                               End If
    '                           End If
    '                       Case Globales.TipoABC.Consulta
    '                           Call Botones(tag)
    '                           Call Regresar()
    '                   End Select
    '           End Select
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       oDatos = Nothing
    '   End Sub
    '   Private Sub Mostrar_Indice()
    '       Dim oDatos As Datos_Viscoi
    '       Dim dtDatos As DataTable = Nothing
    '       Dim Mensaje As String = ""
    '       Dim IdUsuario As Integer = 0
    '       Try
    '           oDatos = New Datos_Viscoi
    '           If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "", dtDatos, Mensaje) Then
    '               GridC_1.DataSource = dtDatos
    '           Else
    '               GridC_1.DataSource = Nothing
    '           End If
    '           GridV_1.BestFitColumns()
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       oDatos = Nothing
    '   End Sub
    '   Private Sub Mostrar_Detalle(ByVal Modo As Globales.TipoABC)
    '       Dim iRen As Integer
    '       Dim oDatos As Datos_Viscoi
    '       Dim dtDatos As DataTable = Nothing
    '       Dim Mensaje As String = ""
    '       Dim IdUsuario As Integer = 0
    '       Try
    '           oDatos = New Datos_Viscoi
    '           iRen = GridV_1.FocusedRowHandle
    '
    '           If Modo = Globales.TipoABC.Nuevo Then
    '               txt1_IdPVenta.Text = ""
    '               txt1_Cajero.Text = ""
    '               txt1_Nombre.Text = ""
    '               cbo1_Estatus.Text = ""
    '               txt1_Actividad.Text = ""
    '               txt1_UltimoCorte.Text = "01/01/1900"
    '               txt1_NumCorte.Text = "0"
    '               txt1_Folio.Text = "0"
    '               txt1_Ventas.Text = "0"
    '               txt1_Devs.Text = "0"
    '               txt1_Cancs.Text = "0"
    '               txt1_Nombre1.Text = ""
    '               txt1_Nombre2.Text = ""
    '               txt1_Nombre3.Text = ""
    '               txt1_Driver1.Text = ""
    '               txt1_Driver2.Text = ""
    '               txt1_Driver3.Text = ""
    '               txt1_Puerto1.Text = ""
    '               txt1_Puerto2.Text = ""
    '               txt1_Puerto3.Text = ""
    '           Else
    '               If GridV_1.RowCount > 0 Then
    '                   TabPane1.SelectedPage = tnpPage2
    '                   tnpPage1.Enabled = False
    '                   tnpPage2.Enabled = True
    '
    '                   txt1_IdPVenta.Text = GridV_1.GetFocusedRowCellValue(col1_id_pventa)
    '                   txt1_Cajero.Text = GridV_1.GetFocusedRowCellValue(col1_caja)
    '                   txt1_Nombre.Text = GridV_1.GetFocusedRowCellValue(col1_nombre)
    '                   cbo1_Estatus.Text = GridV_1.GetFocusedRowCellValue(col1_estatus)
    '                   txt1_Actividad.Text = GridV_1.GetFocusedRowCellValue(col1_actividad)
    '                   txt1_UltimoCorte.Text = GridV_1.GetFocusedRowCellValue(col1_ultcorte)
    '                   txt1_NumCorte.Text = GridV_1.GetFocusedRowCellValue(col1_numcorte)
    '                   txt1_Folio.Text = GridV_1.GetFocusedRowCellValue(col1_folio)
    '                   txt1_Ventas.Text = GridV_1.GetFocusedRowCellValue(col1_folio)
    '                   txt1_Devs.Text = GridV_1.GetFocusedRowCellValue(col1_devoluciones)
    '                   txt1_Cancs.Text = GridV_1.GetFocusedRowCellValue(col1_cancelaciones)
    '                   txt1_Nombre1.Text = GridV_1.GetFocusedRowCellValue(col1_printername1)
    '                   txt1_Nombre2.Text = GridV_1.GetFocusedRowCellValue(col1_printername2)
    '                   txt1_Nombre3.Text = GridV_1.GetFocusedRowCellValue(col1_printername3)
    '                   txt1_Driver1.Text = GridV_1.GetFocusedRowCellValue(col1_printerdriver1)
    '                   txt1_Driver2.Text = GridV_1.GetFocusedRowCellValue(col1_printerdriver2)
    '                   txt1_Driver3.Text = GridV_1.GetFocusedRowCellValue(col1_printerdriver3)
    '                   txt1_Puerto1.Text = GridV_1.GetFocusedRowCellValue(col1_printerport1)
    '                   txt1_Puerto2.Text = GridV_1.GetFocusedRowCellValue(col1_printerport2)
    '                   txt1_Puerto3.Text = GridV_1.GetFocusedRowCellValue(col1_printerport3)
    '               End If
    '           End If
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       oDatos = Nothing
    '   End Sub
    '   Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
    '       Try
    '           Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption
    '           Call ControlBotones(tag)
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '   End Sub
    '   Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
    '       Call ControlBotones("Consulta")
    '   End Sub
    '
    '   Private Sub tsm_AbrirCaja_Click(sender As Object, e As EventArgs) Handles tsm_AbrirCaja.Click
    '       Dim iRen As Integer
    '       Dim oDatos As Datos_Viscoi
    '       Dim dtDatos As DataTable = Nothing
    '       Dim Mensaje As String = ""
    '
    '       Dim sIdPVenta As String
    '       Dim sCaja As String
    '       Dim sNombre As String
    '       Dim sEstatus As String
    '       Dim sActividad As String
    '       Dim sNombre1 As String
    '       Dim sNombre2 As String
    '       Dim sNombre3 As String
    '       Dim sDriver1 As String
    '       Dim sDriver2 As String
    '       Dim sDriver3 As String
    '       Dim sPuerto1 As String
    '       Dim sPuerto2 As String
    '       Dim sPuerto3 As String
    '       Try
    '           oDatos = New Datos_Viscoi
    '
    '
    '           sIdPVenta = GridV_1.GetFocusedRowCellValue(col1_id_pventa)
    '           sCaja = GridV_1.GetFocusedRowCellValue(col1_caja)
    '           sNombre = GridV_1.GetFocusedRowCellValue(col1_nombre)
    '           sEstatus = "EnUso"
    '           sActividad = GridV_1.GetFocusedRowCellValue(col1_actividad)
    '           sNombre1 = GridV_1.GetFocusedRowCellValue(col1_printername1)
    '           sNombre2 = GridV_1.GetFocusedRowCellValue(col1_printername2)
    '           sNombre3 = GridV_1.GetFocusedRowCellValue(col1_printername3)
    '           sDriver1 = GridV_1.GetFocusedRowCellValue(col1_printerdriver1)
    '           sDriver2 = GridV_1.GetFocusedRowCellValue(col1_printerdriver2)
    '           sDriver3 = GridV_1.GetFocusedRowCellValue(col1_printerdriver3)
    '           sPuerto1 = GridV_1.GetFocusedRowCellValue(col1_printerport1)
    '           sPuerto2 = GridV_1.GetFocusedRowCellValue(col1_printerport2)
    '           sPuerto3 = GridV_1.GetFocusedRowCellValue(col1_printerport3)
    '
    '           If oDatos.PVTA_Inserta_Cajas(Globales.oAmbientes.Id_Empresa _
    '                                       , cbo_Sucursal.Text _
    '                                       , sIdPVenta, sCaja, sNombre, sEstatus, sActividad _
    '                                       , sNombre1, sPuerto1, sDriver1 _
    '                                       , sNombre2, sPuerto2, sDriver2 _
    '                                       , sNombre3, sPuerto3, sDriver3 _
    '                                       , Mensaje) Then
    '               Call Refrescar()
    '           Else
    '               MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '           End If
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       oDatos = Nothing
    '   End Sub
    '
    '   Private Sub tsm_CerrarCaja_Click(sender As Object, e As EventArgs) Handles tsm_CerrarCaja.Click
    '       Dim iRen As Integer
    '       Dim oDatos As Datos_Viscoi
    '       Dim dtDatos As DataTable = Nothing
    '       Dim Mensaje As String = ""
    '
    '       Dim sIdPVenta As String
    '       Dim sCaja As String
    '       Dim sNombre As String
    '       Dim sEstatus As String
    '       Dim sActividad As String
    '       Dim sNombre1 As String
    '       Dim sNombre2 As String
    '       Dim sNombre3 As String
    '       Dim sDriver1 As String
    '       Dim sDriver2 As String
    '       Dim sDriver3 As String
    '       Dim sPuerto1 As String
    '       Dim sPuerto2 As String
    '       Dim sPuerto3 As String
    '       Try
    '           oDatos = New Datos_Viscoi
    '
    '
    '           sIdPVenta = GridV_1.GetFocusedRowCellValue(col1_id_pventa)
    '           sCaja = GridV_1.GetFocusedRowCellValue(col1_caja)
    '           sNombre = GridV_1.GetFocusedRowCellValue(col1_nombre)
    '           sEstatus = "Cerrada"
    '           sActividad = GridV_1.GetFocusedRowCellValue(col1_actividad)
    '           sNombre1 = GridV_1.GetFocusedRowCellValue(col1_printername1)
    '           sNombre2 = GridV_1.GetFocusedRowCellValue(col1_printername2)
    '           sNombre3 = GridV_1.GetFocusedRowCellValue(col1_printername3)
    '           sDriver1 = GridV_1.GetFocusedRowCellValue(col1_printerdriver1)
    '           sDriver2 = GridV_1.GetFocusedRowCellValue(col1_printerdriver2)
    '           sDriver3 = GridV_1.GetFocusedRowCellValue(col1_printerdriver3)
    '           sPuerto1 = GridV_1.GetFocusedRowCellValue(col1_printerport1)
    '           sPuerto2 = GridV_1.GetFocusedRowCellValue(col1_printerport2)
    '           sPuerto3 = GridV_1.GetFocusedRowCellValue(col1_printerport3)
    '
    '           If oDatos.PVTA_Inserta_Cajas(Globales.oAmbientes.Id_Empresa _
    '                                       , cbo_Sucursal.Text _
    '                                       , sIdPVenta, sCaja, sNombre, sEstatus, sActividad _
    '                                       , sNombre1, sPuerto1, sDriver1 _
    '                                       , sNombre2, sPuerto2, sDriver2 _
    '                                       , sNombre3, sPuerto3, sDriver3 _
    '                                       , Mensaje) Then
    '               Call Refrescar()
    '           Else
    '               MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '           End If
    '       Catch ex As Exception
    '           MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '       End Try
    '       oDatos = Nothing
    '   End Sub
End Class