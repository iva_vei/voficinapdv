﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCajeros
    Inherits DevExpress.XtraEditors.XtraForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    '''
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions3 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions4 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions5 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.layoutControl = New DevExpress.XtraLayout.LayoutControl()
        Me.btn_OcultarMostrar_Acciones = New DevExpress.XtraEditors.SimpleButton()
        Me.TabPane1 = New DevExpress.XtraBars.Navigation.TabPane()
        Me.tnpPage1 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_id_sucursal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_cajero = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_nombre = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_estatus = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand6 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_numcorte = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_feccorte = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_usuariofum = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_id_usuario = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand5 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_nomina = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_contable = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_id_empresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_id_pventa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_fum = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.tnpPage2 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txt1_IdUsuario = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt1_Actividad = New System.Windows.Forms.TextBox()
        Me.cbo1_Estatus = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt1_Nombre = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt1_Cajero = New System.Windows.Forms.TextBox()
        Me.txt1_IdPVenta = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt1_Folio = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt1_NumCorte = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt1_UltimoCorte = New System.Windows.Forms.TextBox()
        Me.layoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem1 = New DevExpress.XtraLayout.SplitterItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.SplitterItem2 = New DevExpress.XtraLayout.SplitterItem()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.cbo_Sucursal = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pop_Menu1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsm_AbrirCaja = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_CerrarCaja = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.layoutControl.SuspendLayout()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPane1.SuspendLayout()
        Me.tnpPage1.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tnpPage2.SuspendLayout()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.layoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelFiltros_Acciones.SuspendLayout()
        Me.pop_Menu1.SuspendLayout()
        Me.SuspendLayout()
        '
        'layoutControl
        '
        Me.layoutControl.AllowCustomization = False
        Me.layoutControl.Controls.Add(Me.btn_OcultarMostrar_Acciones)
        Me.layoutControl.Controls.Add(Me.TabPane1)
        Me.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.layoutControl.Location = New System.Drawing.Point(0, 0)
        Me.layoutControl.Name = "layoutControl"
        Me.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(461, 277, 450, 400)
        Me.layoutControl.Root = Me.layoutControlGroup1
        Me.layoutControl.Size = New System.Drawing.Size(800, 501)
        Me.layoutControl.TabIndex = 1
        '
        'btn_OcultarMostrar_Acciones
        '
        Me.btn_OcultarMostrar_Acciones.Location = New System.Drawing.Point(36, 477)
        Me.btn_OcultarMostrar_Acciones.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_OcultarMostrar_Acciones.Name = "btn_OcultarMostrar_Acciones"
        Me.btn_OcultarMostrar_Acciones.Size = New System.Drawing.Size(194, 22)
        Me.btn_OcultarMostrar_Acciones.StyleController = Me.layoutControl
        Me.btn_OcultarMostrar_Acciones.TabIndex = 0
        Me.btn_OcultarMostrar_Acciones.Text = "Ocultar / Mostrar Filtros y Acciones"
        '
        'TabPane1
        '
        Me.TabPane1.Controls.Add(Me.tnpPage1)
        Me.TabPane1.Controls.Add(Me.tnpPage2)
        Me.TabPane1.Location = New System.Drawing.Point(36, 2)
        Me.TabPane1.Name = "TabPane1"
        Me.TabPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.tnpPage1, Me.tnpPage2})
        Me.TabPane1.RegularSize = New System.Drawing.Size(718, 471)
        Me.TabPane1.SelectedPage = Me.tnpPage1
        Me.TabPane1.Size = New System.Drawing.Size(718, 471)
        Me.TabPane1.TabIndex = 4
        Me.TabPane1.Text = "TabPane1"
        '
        'tnpPage1
        '
        Me.tnpPage1.Caption = "Indice"
        Me.tnpPage1.Controls.Add(Me.GridC_1)
        Me.tnpPage1.Name = "tnpPage1"
        Me.tnpPage1.Size = New System.Drawing.Size(718, 442)
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GridC_1.Location = New System.Drawing.Point(0, 0)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(718, 442)
        Me.GridC_1.TabIndex = 1
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.gridBand6, Me.gridBand5})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_id_empresa, Me.col1_id_sucursal, Me.col1_id_pventa, Me.col1_cajero, Me.col1_id_usuario, Me.col1_nombre, Me.col1_nomina, Me.col1_contable, Me.col1_estatus, Me.col1_numcorte, Me.col1_feccorte, Me.col1_fum, Me.col1_usuariofum})
        Me.GridV_1.DetailHeight = 284
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        '
        'GridBand1
        '
        Me.GridBand1.AppearanceHeader.Options.UseTextOptions = True
        Me.GridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridBand1.Caption = "GENERALES"
        Me.GridBand1.Columns.Add(Me.col1_id_sucursal)
        Me.GridBand1.Columns.Add(Me.col1_cajero)
        Me.GridBand1.Columns.Add(Me.col1_nombre)
        Me.GridBand1.Columns.Add(Me.col1_estatus)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 300
        '
        'col1_id_sucursal
        '
        Me.col1_id_sucursal.Caption = "Id_Sucursal"
        Me.col1_id_sucursal.FieldName = "id_sucursal"
        Me.col1_id_sucursal.Name = "col1_id_sucursal"
        Me.col1_id_sucursal.OptionsColumn.AllowEdit = False
        Me.col1_id_sucursal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_id_sucursal.Visible = True
        '
        'col1_cajero
        '
        Me.col1_cajero.Caption = "Cajero"
        Me.col1_cajero.FieldName = "cajero"
        Me.col1_cajero.Name = "col1_cajero"
        Me.col1_cajero.OptionsColumn.AllowEdit = False
        Me.col1_cajero.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_cajero.Visible = True
        '
        'col1_nombre
        '
        Me.col1_nombre.Caption = "Nombre"
        Me.col1_nombre.FieldName = "nombre"
        Me.col1_nombre.Name = "col1_nombre"
        Me.col1_nombre.OptionsColumn.AllowEdit = False
        Me.col1_nombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_nombre.Visible = True
        '
        'col1_estatus
        '
        Me.col1_estatus.Caption = "Estatus"
        Me.col1_estatus.FieldName = "estatus"
        Me.col1_estatus.Name = "col1_estatus"
        Me.col1_estatus.OptionsColumn.AllowEdit = False
        Me.col1_estatus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_estatus.Visible = True
        '
        'gridBand6
        '
        Me.gridBand6.AppearanceHeader.Options.UseTextOptions = True
        Me.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridBand6.Caption = "MOVIMIENTOS"
        Me.gridBand6.Columns.Add(Me.col1_numcorte)
        Me.gridBand6.Columns.Add(Me.col1_feccorte)
        Me.gridBand6.Columns.Add(Me.col1_usuariofum)
        Me.gridBand6.Columns.Add(Me.col1_id_usuario)
        Me.gridBand6.Name = "gridBand6"
        Me.gridBand6.VisibleIndex = 1
        Me.gridBand6.Width = 300
        '
        'col1_numcorte
        '
        Me.col1_numcorte.Caption = "Numcorte"
        Me.col1_numcorte.FieldName = "numcorte"
        Me.col1_numcorte.Name = "col1_numcorte"
        Me.col1_numcorte.OptionsColumn.AllowEdit = False
        Me.col1_numcorte.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_numcorte.Visible = True
        '
        'col1_feccorte
        '
        Me.col1_feccorte.Caption = "Feccorte"
        Me.col1_feccorte.FieldName = "feccorte"
        Me.col1_feccorte.Name = "col1_feccorte"
        Me.col1_feccorte.OptionsColumn.AllowEdit = False
        Me.col1_feccorte.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_feccorte.Visible = True
        '
        'col1_usuariofum
        '
        Me.col1_usuariofum.Caption = "Usuariofum"
        Me.col1_usuariofum.FieldName = "usuariofum"
        Me.col1_usuariofum.Name = "col1_usuariofum"
        Me.col1_usuariofum.OptionsColumn.AllowEdit = False
        Me.col1_usuariofum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_usuariofum.Visible = True
        '
        'col1_id_usuario
        '
        Me.col1_id_usuario.Caption = "Id_Usuario"
        Me.col1_id_usuario.FieldName = "id_usuario"
        Me.col1_id_usuario.Name = "col1_id_usuario"
        Me.col1_id_usuario.OptionsColumn.AllowEdit = False
        Me.col1_id_usuario.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_id_usuario.Visible = True
        '
        'gridBand5
        '
        Me.gridBand5.AppearanceHeader.Options.UseTextOptions = True
        Me.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridBand5.Columns.Add(Me.col1_nomina)
        Me.gridBand5.Columns.Add(Me.col1_contable)
        Me.gridBand5.Name = "gridBand5"
        Me.gridBand5.VisibleIndex = 2
        Me.gridBand5.Width = 150
        '
        'col1_nomina
        '
        Me.col1_nomina.Caption = "Nomina"
        Me.col1_nomina.FieldName = "nomina"
        Me.col1_nomina.Name = "col1_nomina"
        Me.col1_nomina.OptionsColumn.AllowEdit = False
        Me.col1_nomina.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_nomina.Visible = True
        '
        'col1_contable
        '
        Me.col1_contable.Caption = "Contable"
        Me.col1_contable.FieldName = "contable"
        Me.col1_contable.Name = "col1_contable"
        Me.col1_contable.OptionsColumn.AllowEdit = False
        Me.col1_contable.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_contable.Visible = True
        '
        'col1_id_empresa
        '
        Me.col1_id_empresa.Caption = "Id_Empresa"
        Me.col1_id_empresa.FieldName = "id_empresa"
        Me.col1_id_empresa.Name = "col1_id_empresa"
        Me.col1_id_empresa.OptionsColumn.AllowEdit = False
        Me.col1_id_empresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_id_empresa.Visible = True
        '
        'col1_id_pventa
        '
        Me.col1_id_pventa.Caption = "Id_Pventa"
        Me.col1_id_pventa.FieldName = "id_pventa"
        Me.col1_id_pventa.Name = "col1_id_pventa"
        Me.col1_id_pventa.OptionsColumn.AllowEdit = False
        Me.col1_id_pventa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_id_pventa.Visible = True
        '
        'col1_fum
        '
        Me.col1_fum.Caption = "Fum"
        Me.col1_fum.FieldName = "fum"
        Me.col1_fum.Name = "col1_fum"
        Me.col1_fum.OptionsColumn.AllowEdit = False
        Me.col1_fum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_fum.Visible = True
        '
        'tnpPage2
        '
        Me.tnpPage2.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.tnpPage2.Appearance.Options.UseBackColor = True
        Me.tnpPage2.Caption = "Detalle"
        Me.tnpPage2.Controls.Add(Me.SplitContainerControl1)
        Me.tnpPage2.Name = "tnpPage2"
        Me.tnpPage2.Size = New System.Drawing.Size(718, 442)
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Horizontal = False
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label10)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txt1_IdUsuario)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txt1_Actividad)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cbo1_Estatus)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txt1_Nombre)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txt1_Cajero)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txt1_IdPVenta)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.Label11)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.txt1_Folio)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.Label9)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.txt1_NumCorte)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.Label8)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.txt1_UltimoCorte)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(718, 442)
        Me.SplitContainerControl1.SplitterPosition = 125
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(475, 68)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(150, 19)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Fecha Corte"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(325, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(150, 19)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Usuario"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(475, 90)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(150, 27)
        Me.TextBox1.TabIndex = 18
        '
        'txt1_IdUsuario
        '
        Me.txt1_IdUsuario.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_IdUsuario.Location = New System.Drawing.Point(325, 90)
        Me.txt1_IdUsuario.Name = "txt1_IdUsuario"
        Me.txt1_IdUsuario.Size = New System.Drawing.Size(150, 27)
        Me.txt1_IdUsuario.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(169, 68)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(150, 19)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Actividad"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_Actividad
        '
        Me.txt1_Actividad.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_Actividad.Location = New System.Drawing.Point(169, 90)
        Me.txt1_Actividad.Name = "txt1_Actividad"
        Me.txt1_Actividad.Size = New System.Drawing.Size(150, 27)
        Me.txt1_Actividad.TabIndex = 8
        '
        'cbo1_Estatus
        '
        Me.cbo1_Estatus.DisplayMember = "id_sucursal"
        Me.cbo1_Estatus.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo1_Estatus.FormattingEnabled = True
        Me.cbo1_Estatus.Items.AddRange(New Object() {"Operacio", "EnUso", "Cancelado"})
        Me.cbo1_Estatus.Location = New System.Drawing.Point(15, 90)
        Me.cbo1_Estatus.Name = "cbo1_Estatus"
        Me.cbo1_Estatus.Size = New System.Drawing.Size(150, 27)
        Me.cbo1_Estatus.TabIndex = 7
        Me.cbo1_Estatus.ValueMember = "id_sucursal"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(15, 68)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(150, 19)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Estatus"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(169, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(300, 19)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Nombre"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_Nombre
        '
        Me.txt1_Nombre.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_Nombre.Location = New System.Drawing.Point(169, 38)
        Me.txt1_Nombre.Name = "txt1_Nombre"
        Me.txt1_Nombre.Size = New System.Drawing.Size(300, 27)
        Me.txt1_Nombre.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(150, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Cajero"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(475, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "IdPVenta"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_Cajero
        '
        Me.txt1_Cajero.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_Cajero.Location = New System.Drawing.Point(15, 38)
        Me.txt1_Cajero.Name = "txt1_Cajero"
        Me.txt1_Cajero.Size = New System.Drawing.Size(150, 27)
        Me.txt1_Cajero.TabIndex = 0
        '
        'txt1_IdPVenta
        '
        Me.txt1_IdPVenta.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_IdPVenta.Location = New System.Drawing.Point(475, 38)
        Me.txt1_IdPVenta.Name = "txt1_IdPVenta"
        Me.txt1_IdPVenta.Size = New System.Drawing.Size(150, 27)
        Me.txt1_IdPVenta.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(250, 4)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(150, 19)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Fecha Corte"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_Folio
        '
        Me.txt1_Folio.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_Folio.Location = New System.Drawing.Point(250, 26)
        Me.txt1_Folio.Name = "txt1_Folio"
        Me.txt1_Folio.Size = New System.Drawing.Size(150, 27)
        Me.txt1_Folio.TabIndex = 16
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(169, 4)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 19)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "#Corte"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_NumCorte
        '
        Me.txt1_NumCorte.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_NumCorte.Location = New System.Drawing.Point(169, 26)
        Me.txt1_NumCorte.Name = "txt1_NumCorte"
        Me.txt1_NumCorte.Size = New System.Drawing.Size(75, 27)
        Me.txt1_NumCorte.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(15, 4)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(150, 19)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Ultimo Corte"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_UltimoCorte
        '
        Me.txt1_UltimoCorte.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_UltimoCorte.Location = New System.Drawing.Point(15, 26)
        Me.txt1_UltimoCorte.Name = "txt1_UltimoCorte"
        Me.txt1_UltimoCorte.Size = New System.Drawing.Size(150, 27)
        Me.txt1_UltimoCorte.TabIndex = 12
        '
        'layoutControlGroup1
        '
        Me.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.layoutControlGroup1.GroupBordersVisible = False
        Me.layoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.SplitterItem1, Me.EmptySpaceItem2, Me.SplitterItem2})
        Me.layoutControlGroup1.Name = "layoutControlGroup1"
        Me.layoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(34, 34, 0, 0)
        Me.layoutControlGroup1.Size = New System.Drawing.Size(800, 501)
        Me.layoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.TabPane1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(722, 475)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.btn_OcultarMostrar_Acciones
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 475)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(198, 26)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'SplitterItem1
        '
        Me.SplitterItem1.AllowHotTrack = True
        Me.SplitterItem1.Location = New System.Drawing.Point(722, 0)
        Me.SplitterItem1.Name = "SplitterItem1"
        Me.SplitterItem1.Size = New System.Drawing.Size(10, 501)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(208, 475)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(514, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'SplitterItem2
        '
        Me.SplitterItem2.AllowHotTrack = True
        Me.SplitterItem2.Location = New System.Drawing.Point(198, 475)
        Me.SplitterItem2.Name = "SplitterItem2"
        Me.SplitterItem2.Size = New System.Drawing.Size(10, 26)
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.ImageUri.Uri = "New;Size32x32;GrayScaled"
        WindowsUIButtonImageOptions2.ImageUri.Uri = "Edit;Size32x32;GrayScaled"
        WindowsUIButtonImageOptions3.ImageUri.Uri = "Edit/Delete;Size32x32;GrayScaled"
        WindowsUIButtonImageOptions4.ImageUri.Uri = "Apply;GrayScaled"
        WindowsUIButtonImageOptions5.ImageUri.Uri = "Cancel;GrayScaled"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Nuevo", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Editar", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Eliminar", True, WindowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUISeparator(), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Ok", True, WindowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", True, WindowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.cbo_Sucursal)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label1)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(0, 501)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 60)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(60, 60)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(800, 60)
        Me.PanelFiltros_Acciones.TabIndex = 0
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'cbo_Sucursal
        '
        Me.cbo_Sucursal.DisplayMember = "id_sucursal"
        Me.cbo_Sucursal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Sucursal.FormattingEnabled = True
        Me.cbo_Sucursal.Location = New System.Drawing.Point(109, 6)
        Me.cbo_Sucursal.Name = "cbo_Sucursal"
        Me.cbo_Sucursal.Size = New System.Drawing.Size(150, 27)
        Me.cbo_Sucursal.TabIndex = 2
        Me.cbo_Sucursal.ValueMember = "id_sucursal"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Sucursal"
        '
        'pop_Menu1
        '
        Me.pop_Menu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_AbrirCaja, Me.tsm_CerrarCaja})
        Me.pop_Menu1.Name = "pop_Menu1"
        Me.pop_Menu1.Size = New System.Drawing.Size(133, 48)
        '
        'tsm_AbrirCaja
        '
        Me.tsm_AbrirCaja.Name = "tsm_AbrirCaja"
        Me.tsm_AbrirCaja.Size = New System.Drawing.Size(132, 22)
        Me.tsm_AbrirCaja.Text = "Cerrar Caja"
        '
        'tsm_CerrarCaja
        '
        Me.tsm_CerrarCaja.Name = "tsm_CerrarCaja"
        Me.tsm_CerrarCaja.Size = New System.Drawing.Size(132, 22)
        Me.tsm_CerrarCaja.Text = "Cerrar Caja"
        '
        'pdvCajeros
        '
        Me.Appearance.BackColor = System.Drawing.Color.White
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 561)
        Me.Controls.Add(Me.layoutControl)
        Me.Controls.Add(Me.PanelFiltros_Acciones)
        Me.Name = "pdvCajeros"
        Me.Text = "Cajeros"
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.layoutControl.ResumeLayout(False)
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPane1.ResumeLayout(False)
        Me.tnpPage1.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tnpPage2.ResumeLayout(False)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.layoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        Me.pop_Menu1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents layoutControl As DevExpress.XtraLayout.LayoutControl
    Private WithEvents layoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents TabPane1 As DevExpress.XtraBars.Navigation.TabPane
    Friend WithEvents tnpPage1 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents tnpPage2 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem1 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents btn_OcultarMostrar_Acciones As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SplitterItem2 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents col1_id_empresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_id_sucursal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_id_pventa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_cajero As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_id_usuario As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_nombre As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_nomina As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_contable As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_estatus As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_numcorte As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_feccorte As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_fum As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_usuariofum As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txt1_IdPVenta As ComboBox
    Friend WithEvents cbo_Sucursal As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txt1_IdUsuario As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt1_Actividad As TextBox
    Friend WithEvents cbo1_Estatus As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txt1_Nombre As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txt1_Cajero As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txt1_Folio As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txt1_NumCorte As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txt1_UltimoCorte As TextBox
    Friend WithEvents pop_Menu1 As ContextMenuStrip
    Friend WithEvents tsm_AbrirCaja As ToolStripMenuItem
    Friend WithEvents tsm_CerrarCaja As ToolStripMenuItem
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand6 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand5 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents Label10 As Label
    Friend WithEvents TextBox1 As TextBox
End Class
