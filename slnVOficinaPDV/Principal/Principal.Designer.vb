﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Principal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal))
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lbl_Usuario1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl_Perfil = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl_Ambiente = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl_Version = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl_Servidor = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl_Empresa = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl_BD = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl_Teclas = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.tsm_Configurar = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_ParametrosControl = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Cajas = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Cajeros = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Vendedores = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpcionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArticulosNegadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarClienteEventualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefacturarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaGlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VouchersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PreciadoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Base2x3ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.N3x4ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.N3x4BToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.N4x9ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Administrar = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Cortes = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Devoluciones = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Cancelaciones = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Reportes = New System.Windows.Forms.ToolStripMenuItem()
        Me.CanjeValesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrestamoClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecibirPagosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecibirPagosClienteFinalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CorteDeCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TraspasoDeDineroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.EstadoDeCuentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobranzaDiariaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReimprimirToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReimprimirPagosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReestructurasRelaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReestructurasClienteFinalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConveniosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LiquidaciónesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_Acciones = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm_PuntoVenta = New System.Windows.Forms.ToolStripMenuItem()
        Me.MostradorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CotizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RevisarExistenciasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReimprimirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReimprimirCorteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevolucionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CorteDeCajaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.pic_Logo = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.gridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_IdCliente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdClienteFinal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ClienteFinal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImporteVale = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_PagoQuincenal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ContraVale = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Popup = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.pic_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Popup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Popup.SuspendLayout()
        Me.SuspendLayout()
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lbl_Usuario1, Me.lbl_Perfil, Me.lbl_Ambiente, Me.lbl_Version, Me.lbl_Servidor, Me.lbl_Empresa, Me.lbl_BD, Me.lbl_Teclas})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 660)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1283, 22)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lbl_Usuario1
        '
        Me.lbl_Usuario1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbl_Usuario1.Name = "lbl_Usuario1"
        Me.lbl_Usuario1.Size = New System.Drawing.Size(49, 17)
        Me.lbl_Usuario1.Text = "Usuario"
        '
        'lbl_Perfil
        '
        Me.lbl_Perfil.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbl_Perfil.Name = "lbl_Perfil"
        Me.lbl_Perfil.Size = New System.Drawing.Size(37, 17)
        Me.lbl_Perfil.Text = "Perfil"
        '
        'lbl_Ambiente
        '
        Me.lbl_Ambiente.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbl_Ambiente.Name = "lbl_Ambiente"
        Me.lbl_Ambiente.Size = New System.Drawing.Size(62, 17)
        Me.lbl_Ambiente.Text = "Ambiente"
        '
        'lbl_Version
        '
        Me.lbl_Version.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbl_Version.Name = "lbl_Version"
        Me.lbl_Version.Size = New System.Drawing.Size(54, 17)
        Me.lbl_Version.Text = "Version: "
        '
        'lbl_Servidor
        '
        Me.lbl_Servidor.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Servidor.Name = "lbl_Servidor"
        Me.lbl_Servidor.Size = New System.Drawing.Size(58, 17)
        Me.lbl_Servidor.Text = "Servidor:"
        '
        'lbl_Empresa
        '
        Me.lbl_Empresa.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Empresa.Name = "lbl_Empresa"
        Me.lbl_Empresa.Size = New System.Drawing.Size(57, 17)
        Me.lbl_Empresa.Text = "Empresa:"
        '
        'lbl_BD
        '
        Me.lbl_BD.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbl_BD.Name = "lbl_BD"
        Me.lbl_BD.Size = New System.Drawing.Size(27, 17)
        Me.lbl_BD.Text = "BD:"
        '
        'lbl_Teclas
        '
        Me.lbl_Teclas.Name = "lbl_Teclas"
        Me.lbl_Teclas.Size = New System.Drawing.Size(0, 17)
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_Configurar, Me.OpcionesToolStripMenuItem, Me.tsm_Administrar, Me.tsm_Reportes, Me.tsm_Acciones})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1283, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'tsm_Configurar
        '
        Me.tsm_Configurar.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_ParametrosControl, Me.tsm_Cajas, Me.tsm_Cajeros, Me.tsm_Vendedores})
        Me.tsm_Configurar.Name = "tsm_Configurar"
        Me.tsm_Configurar.Size = New System.Drawing.Size(94, 20)
        Me.tsm_Configurar.Text = "1.0 Configurar"
        '
        'tsm_ParametrosControl
        '
        Me.tsm_ParametrosControl.Name = "tsm_ParametrosControl"
        Me.tsm_ParametrosControl.Size = New System.Drawing.Size(223, 22)
        Me.tsm_ParametrosControl.Text = "1.1 Configurar Actualización"
        '
        'tsm_Cajas
        '
        Me.tsm_Cajas.Name = "tsm_Cajas"
        Me.tsm_Cajas.Size = New System.Drawing.Size(223, 22)
        Me.tsm_Cajas.Text = "1.2 Cajas"
        Me.tsm_Cajas.Visible = False
        '
        'tsm_Cajeros
        '
        Me.tsm_Cajeros.Name = "tsm_Cajeros"
        Me.tsm_Cajeros.Size = New System.Drawing.Size(223, 22)
        Me.tsm_Cajeros.Text = "1.3 Cajeros"
        Me.tsm_Cajeros.Visible = False
        '
        'tsm_Vendedores
        '
        Me.tsm_Vendedores.Name = "tsm_Vendedores"
        Me.tsm_Vendedores.Size = New System.Drawing.Size(223, 22)
        Me.tsm_Vendedores.Text = "1.4 Vendedores"
        Me.tsm_Vendedores.Visible = False
        '
        'OpcionesToolStripMenuItem
        '
        Me.OpcionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArticulosNegadosToolStripMenuItem, Me.ModificarClienteEventualToolStripMenuItem, Me.RefacturarToolStripMenuItem, Me.FacturaGlobalToolStripMenuItem, Me.VouchersToolStripMenuItem, Me.PreciadoresToolStripMenuItem, Me.ToolStripMenuItem6})
        Me.OpcionesToolStripMenuItem.Name = "OpcionesToolStripMenuItem"
        Me.OpcionesToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.OpcionesToolStripMenuItem.Text = "5.0 Opciones"
        '
        'ArticulosNegadosToolStripMenuItem
        '
        Me.ArticulosNegadosToolStripMenuItem.Checked = True
        Me.ArticulosNegadosToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ArticulosNegadosToolStripMenuItem.Name = "ArticulosNegadosToolStripMenuItem"
        Me.ArticulosNegadosToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.ArticulosNegadosToolStripMenuItem.Text = "5.1 Articulos Negados"
        '
        'ModificarClienteEventualToolStripMenuItem
        '
        Me.ModificarClienteEventualToolStripMenuItem.Checked = True
        Me.ModificarClienteEventualToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ModificarClienteEventualToolStripMenuItem.Name = "ModificarClienteEventualToolStripMenuItem"
        Me.ModificarClienteEventualToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.ModificarClienteEventualToolStripMenuItem.Text = "5.2 Clientes Eventuales"
        '
        'RefacturarToolStripMenuItem
        '
        Me.RefacturarToolStripMenuItem.Checked = True
        Me.RefacturarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.RefacturarToolStripMenuItem.Name = "RefacturarToolStripMenuItem"
        Me.RefacturarToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.RefacturarToolStripMenuItem.Text = "5.3 Refacturar"
        '
        'FacturaGlobalToolStripMenuItem
        '
        Me.FacturaGlobalToolStripMenuItem.Name = "FacturaGlobalToolStripMenuItem"
        Me.FacturaGlobalToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.FacturaGlobalToolStripMenuItem.Text = "5.4 Factura Global"
        '
        'VouchersToolStripMenuItem
        '
        Me.VouchersToolStripMenuItem.Name = "VouchersToolStripMenuItem"
        Me.VouchersToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.VouchersToolStripMenuItem.Text = "5.5 Vouchers"
        '
        'PreciadoresToolStripMenuItem
        '
        Me.PreciadoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Base2x3ToolStripMenuItem, Me.N3x4ToolStripMenuItem, Me.N3x4BToolStripMenuItem, Me.N4x9ToolStripMenuItem})
        Me.PreciadoresToolStripMenuItem.Name = "PreciadoresToolStripMenuItem"
        Me.PreciadoresToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.PreciadoresToolStripMenuItem.Text = "5.6 Preciadores"
        '
        'Base2x3ToolStripMenuItem
        '
        Me.Base2x3ToolStripMenuItem.Name = "Base2x3ToolStripMenuItem"
        Me.Base2x3ToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.Base2x3ToolStripMenuItem.Text = "5.6.1 Base 2x3"
        '
        'N3x4ToolStripMenuItem
        '
        Me.N3x4ToolStripMenuItem.Name = "N3x4ToolStripMenuItem"
        Me.N3x4ToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.N3x4ToolStripMenuItem.Text = "5.6.2 N3x4 A"
        '
        'N3x4BToolStripMenuItem
        '
        Me.N3x4BToolStripMenuItem.Name = "N3x4BToolStripMenuItem"
        Me.N3x4BToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.N3x4BToolStripMenuItem.Text = "5.6.3 N3x4 B"
        '
        'N4x9ToolStripMenuItem
        '
        Me.N4x9ToolStripMenuItem.Name = "N4x9ToolStripMenuItem"
        Me.N4x9ToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.N4x9ToolStripMenuItem.Text = "5.6.4 N4x9"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(193, 22)
        Me.ToolStripMenuItem6.Text = "5.7 Contra Vales"
        '
        'tsm_Administrar
        '
        Me.tsm_Administrar.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_Cortes, Me.tsm_Devoluciones, Me.tsm_Cancelaciones})
        Me.tsm_Administrar.Name = "tsm_Administrar"
        Me.tsm_Administrar.Size = New System.Drawing.Size(99, 20)
        Me.tsm_Administrar.Text = "2.0 Administrar"
        Me.tsm_Administrar.Visible = False
        '
        'tsm_Cortes
        '
        Me.tsm_Cortes.Name = "tsm_Cortes"
        Me.tsm_Cortes.Size = New System.Drawing.Size(168, 22)
        Me.tsm_Cortes.Text = "2.1 Cortes"
        '
        'tsm_Devoluciones
        '
        Me.tsm_Devoluciones.Name = "tsm_Devoluciones"
        Me.tsm_Devoluciones.Size = New System.Drawing.Size(168, 22)
        Me.tsm_Devoluciones.Text = "2.2 Devoluciones"
        '
        'tsm_Cancelaciones
        '
        Me.tsm_Cancelaciones.Name = "tsm_Cancelaciones"
        Me.tsm_Cancelaciones.Size = New System.Drawing.Size(168, 22)
        Me.tsm_Cancelaciones.Text = "2.3 Cancelaciones"
        '
        'tsm_Reportes
        '
        Me.tsm_Reportes.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CanjeValesToolStripMenuItem, Me.PrestamoClientesToolStripMenuItem, Me.RecibirPagosToolStripMenuItem, Me.RecibirPagosClienteFinalToolStripMenuItem, Me.CorteDeCajaToolStripMenuItem, Me.ToolStripMenuItem4, Me.TraspasoDeDineroToolStripMenuItem, Me.ToolStripMenuItem1, Me.EstadoDeCuentaToolStripMenuItem, Me.CobranzaDiariaToolStripMenuItem, Me.ToolStripMenuItem2, Me.ReimprimirToolStripMenuItem1, Me.ReimprimirPagosToolStripMenuItem, Me.ToolStripMenuItem3, Me.ReestructurasRelaciónToolStripMenuItem, Me.ReestructurasClienteFinalToolStripMenuItem, Me.ConveniosToolStripMenuItem, Me.LiquidaciónesToolStripMenuItem})
        Me.tsm_Reportes.Name = "tsm_Reportes"
        Me.tsm_Reportes.Size = New System.Drawing.Size(76, 20)
        Me.tsm_Reportes.Text = "3.0 Credito"
        '
        'CanjeValesToolStripMenuItem
        '
        Me.CanjeValesToolStripMenuItem.Name = "CanjeValesToolStripMenuItem"
        Me.CanjeValesToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.CanjeValesToolStripMenuItem.Text = "3.1 Canje Vales"
        '
        'PrestamoClientesToolStripMenuItem
        '
        Me.PrestamoClientesToolStripMenuItem.Name = "PrestamoClientesToolStripMenuItem"
        Me.PrestamoClientesToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.PrestamoClientesToolStripMenuItem.Text = "3.2 Prestamo Clientes"
        '
        'RecibirPagosToolStripMenuItem
        '
        Me.RecibirPagosToolStripMenuItem.Name = "RecibirPagosToolStripMenuItem"
        Me.RecibirPagosToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.RecibirPagosToolStripMenuItem.Text = "3.3.1 Recibir Pagos"
        '
        'RecibirPagosClienteFinalToolStripMenuItem
        '
        Me.RecibirPagosClienteFinalToolStripMenuItem.Name = "RecibirPagosClienteFinalToolStripMenuItem"
        Me.RecibirPagosClienteFinalToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.RecibirPagosClienteFinalToolStripMenuItem.Text = "3.3.2 Recibir Pagos Cliente Final"
        '
        'CorteDeCajaToolStripMenuItem
        '
        Me.CorteDeCajaToolStripMenuItem.Name = "CorteDeCajaToolStripMenuItem"
        Me.CorteDeCajaToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.CorteDeCajaToolStripMenuItem.Text = "3.4.1 Corte de Caja"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(240, 22)
        Me.ToolStripMenuItem4.Text = "3.4.2 Traspaso de Caja a Boveda"
        '
        'TraspasoDeDineroToolStripMenuItem
        '
        Me.TraspasoDeDineroToolStripMenuItem.Name = "TraspasoDeDineroToolStripMenuItem"
        Me.TraspasoDeDineroToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.TraspasoDeDineroToolStripMenuItem.Text = "3.4.3 Traspaso de Boveda a Caja"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(237, 6)
        '
        'EstadoDeCuentaToolStripMenuItem
        '
        Me.EstadoDeCuentaToolStripMenuItem.Name = "EstadoDeCuentaToolStripMenuItem"
        Me.EstadoDeCuentaToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.EstadoDeCuentaToolStripMenuItem.Text = "3.5 Estado de Cuenta"
        '
        'CobranzaDiariaToolStripMenuItem
        '
        Me.CobranzaDiariaToolStripMenuItem.Name = "CobranzaDiariaToolStripMenuItem"
        Me.CobranzaDiariaToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.CobranzaDiariaToolStripMenuItem.Text = "3.6 Cobranza Diaria"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(237, 6)
        '
        'ReimprimirToolStripMenuItem1
        '
        Me.ReimprimirToolStripMenuItem1.Name = "ReimprimirToolStripMenuItem1"
        Me.ReimprimirToolStripMenuItem1.Size = New System.Drawing.Size(240, 22)
        Me.ReimprimirToolStripMenuItem1.Text = "3.7.1 Reimprimir Canjes"
        '
        'ReimprimirPagosToolStripMenuItem
        '
        Me.ReimprimirPagosToolStripMenuItem.Name = "ReimprimirPagosToolStripMenuItem"
        Me.ReimprimirPagosToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.ReimprimirPagosToolStripMenuItem.Text = "3.7.2 Reimprimir Pagos"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(237, 6)
        '
        'ReestructurasRelaciónToolStripMenuItem
        '
        Me.ReestructurasRelaciónToolStripMenuItem.Name = "ReestructurasRelaciónToolStripMenuItem"
        Me.ReestructurasRelaciónToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.ReestructurasRelaciónToolStripMenuItem.Text = "3.8.1 Reestructuras Relación"
        '
        'ReestructurasClienteFinalToolStripMenuItem
        '
        Me.ReestructurasClienteFinalToolStripMenuItem.Name = "ReestructurasClienteFinalToolStripMenuItem"
        Me.ReestructurasClienteFinalToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.ReestructurasClienteFinalToolStripMenuItem.Text = "3.8.2 Reestructuras Cliente Final"
        '
        'ConveniosToolStripMenuItem
        '
        Me.ConveniosToolStripMenuItem.Name = "ConveniosToolStripMenuItem"
        Me.ConveniosToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.ConveniosToolStripMenuItem.Text = "3.8.3 Convenios"
        '
        'LiquidaciónesToolStripMenuItem
        '
        Me.LiquidaciónesToolStripMenuItem.Name = "LiquidaciónesToolStripMenuItem"
        Me.LiquidaciónesToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.LiquidaciónesToolStripMenuItem.Text = "3.8.4 Liquidaciónes"
        '
        'tsm_Acciones
        '
        Me.tsm_Acciones.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_PuntoVenta, Me.MostradorToolStripMenuItem, Me.CotizacionesToolStripMenuItem, Me.RevisarExistenciasToolStripMenuItem, Me.FacturarToolStripMenuItem, Me.ReimprimirToolStripMenuItem, Me.AbrirCajaToolStripMenuItem, Me.ReimprimirCorteToolStripMenuItem, Me.DevolucionToolStripMenuItem, Me.CancelaciónToolStripMenuItem, Me.CorteDeCajaToolStripMenuItem1})
        Me.tsm_Acciones.Name = "tsm_Acciones"
        Me.tsm_Acciones.Size = New System.Drawing.Size(85, 20)
        Me.tsm_Acciones.Text = "4.0 Acciones"
        '
        'tsm_PuntoVenta
        '
        Me.tsm_PuntoVenta.Checked = True
        Me.tsm_PuntoVenta.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tsm_PuntoVenta.Name = "tsm_PuntoVenta"
        Me.tsm_PuntoVenta.Size = New System.Drawing.Size(189, 22)
        Me.tsm_PuntoVenta.Text = "4.1 Punto de Venta"
        '
        'MostradorToolStripMenuItem
        '
        Me.MostradorToolStripMenuItem.Checked = True
        Me.MostradorToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.MostradorToolStripMenuItem.Name = "MostradorToolStripMenuItem"
        Me.MostradorToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.MostradorToolStripMenuItem.Text = "4.2 Mostrador"
        '
        'CotizacionesToolStripMenuItem
        '
        Me.CotizacionesToolStripMenuItem.Checked = True
        Me.CotizacionesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CotizacionesToolStripMenuItem.Name = "CotizacionesToolStripMenuItem"
        Me.CotizacionesToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.CotizacionesToolStripMenuItem.Text = "4.3 Cotizaciones"
        '
        'RevisarExistenciasToolStripMenuItem
        '
        Me.RevisarExistenciasToolStripMenuItem.Checked = True
        Me.RevisarExistenciasToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.RevisarExistenciasToolStripMenuItem.Name = "RevisarExistenciasToolStripMenuItem"
        Me.RevisarExistenciasToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.RevisarExistenciasToolStripMenuItem.Text = "4.4 Revisar Existencias"
        '
        'FacturarToolStripMenuItem
        '
        Me.FacturarToolStripMenuItem.Checked = True
        Me.FacturarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.FacturarToolStripMenuItem.Name = "FacturarToolStripMenuItem"
        Me.FacturarToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.FacturarToolStripMenuItem.Text = "4.5 Facturar"
        '
        'ReimprimirToolStripMenuItem
        '
        Me.ReimprimirToolStripMenuItem.Checked = True
        Me.ReimprimirToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ReimprimirToolStripMenuItem.Name = "ReimprimirToolStripMenuItem"
        Me.ReimprimirToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ReimprimirToolStripMenuItem.Text = "4.6 Reimprimir"
        '
        'AbrirCajaToolStripMenuItem
        '
        Me.AbrirCajaToolStripMenuItem.Checked = True
        Me.AbrirCajaToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.AbrirCajaToolStripMenuItem.Name = "AbrirCajaToolStripMenuItem"
        Me.AbrirCajaToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.AbrirCajaToolStripMenuItem.Text = "4.7 Abrir Caja"
        '
        'ReimprimirCorteToolStripMenuItem
        '
        Me.ReimprimirCorteToolStripMenuItem.Checked = True
        Me.ReimprimirCorteToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ReimprimirCorteToolStripMenuItem.Name = "ReimprimirCorteToolStripMenuItem"
        Me.ReimprimirCorteToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ReimprimirCorteToolStripMenuItem.Text = "4.8 Reimprimir Corte"
        '
        'DevolucionToolStripMenuItem
        '
        Me.DevolucionToolStripMenuItem.Checked = True
        Me.DevolucionToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.DevolucionToolStripMenuItem.Name = "DevolucionToolStripMenuItem"
        Me.DevolucionToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.DevolucionToolStripMenuItem.Text = "4.9 Devolución"
        '
        'CancelaciónToolStripMenuItem
        '
        Me.CancelaciónToolStripMenuItem.Checked = True
        Me.CancelaciónToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CancelaciónToolStripMenuItem.Name = "CancelaciónToolStripMenuItem"
        Me.CancelaciónToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.CancelaciónToolStripMenuItem.Text = "4.10 Cancelación"
        '
        'CorteDeCajaToolStripMenuItem1
        '
        Me.CorteDeCajaToolStripMenuItem1.Name = "CorteDeCajaToolStripMenuItem1"
        Me.CorteDeCajaToolStripMenuItem1.Size = New System.Drawing.Size(189, 22)
        Me.CorteDeCajaToolStripMenuItem1.Text = "4.11 Corte de Caja"
        '
        'pic_Logo
        '
        Me.pic_Logo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pic_Logo.Image = Global.proyVOficina_PDV.My.Resources.Resources.logo
        Me.pic_Logo.Location = New System.Drawing.Point(0, 24)
        Me.pic_Logo.Margin = New System.Windows.Forms.Padding(2)
        Me.pic_Logo.Name = "pic_Logo"
        Me.pic_Logo.Size = New System.Drawing.Size(1283, 636)
        Me.pic_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pic_Logo.TabIndex = 4
        Me.pic_Logo.TabStop = False
        '
        'Timer1
        '
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(718, 128)
        Me.GridC_1.TabIndex = 6
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1, Me.GridView1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.gridBand1})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_IdClienteFinal, Me.col1_ClienteFinal, Me.col1_IdCliente, Me.col1_ImporteVale, Me.col1_ContraVale, Me.col1_PagoQuincenal})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'gridBand1
        '
        Me.gridBand1.Columns.Add(Me.col1_IdCliente)
        Me.gridBand1.Columns.Add(Me.col1_IdClienteFinal)
        Me.gridBand1.Columns.Add(Me.col1_ClienteFinal)
        Me.gridBand1.Columns.Add(Me.col1_ImporteVale)
        Me.gridBand1.Columns.Add(Me.col1_PagoQuincenal)
        Me.gridBand1.Columns.Add(Me.col1_ContraVale)
        Me.gridBand1.Name = "gridBand1"
        Me.gridBand1.VisibleIndex = 0
        Me.gridBand1.Width = 498
        '
        'col1_IdCliente
        '
        Me.col1_IdCliente.Caption = "IdCliente"
        Me.col1_IdCliente.FieldName = "idcliente"
        Me.col1_IdCliente.Name = "col1_IdCliente"
        Me.col1_IdCliente.OptionsColumn.AllowEdit = False
        Me.col1_IdCliente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdCliente.Visible = True
        Me.col1_IdCliente.Width = 123
        '
        'col1_IdClienteFinal
        '
        Me.col1_IdClienteFinal.Caption = "IdClienteFinal"
        Me.col1_IdClienteFinal.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.col1_IdClienteFinal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col1_IdClienteFinal.FieldName = "IdClienteFinal"
        Me.col1_IdClienteFinal.Name = "col1_IdClienteFinal"
        Me.col1_IdClienteFinal.OptionsColumn.AllowEdit = False
        Me.col1_IdClienteFinal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdClienteFinal.Visible = True
        '
        'col1_ClienteFinal
        '
        Me.col1_ClienteFinal.Caption = "ClienteFinal"
        Me.col1_ClienteFinal.FieldName = "clientefinal"
        Me.col1_ClienteFinal.Name = "col1_ClienteFinal"
        Me.col1_ClienteFinal.OptionsColumn.AllowEdit = False
        Me.col1_ClienteFinal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ClienteFinal.Visible = True
        '
        'col1_ImporteVale
        '
        Me.col1_ImporteVale.Caption = "ImporteVale"
        Me.col1_ImporteVale.DisplayFormat.FormatString = "C0"
        Me.col1_ImporteVale.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_ImporteVale.FieldName = "importevale"
        Me.col1_ImporteVale.Name = "col1_ImporteVale"
        Me.col1_ImporteVale.OptionsColumn.AllowEdit = False
        Me.col1_ImporteVale.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImporteVale.Visible = True
        '
        'col1_PagoQuincenal
        '
        Me.col1_PagoQuincenal.Caption = "PagoQuincenal"
        Me.col1_PagoQuincenal.DisplayFormat.FormatString = "C0"
        Me.col1_PagoQuincenal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_PagoQuincenal.FieldName = "PagoQuincenal"
        Me.col1_PagoQuincenal.Name = "col1_PagoQuincenal"
        Me.col1_PagoQuincenal.OptionsColumn.AllowEdit = False
        Me.col1_PagoQuincenal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_PagoQuincenal.Visible = True
        '
        'col1_ContraVale
        '
        Me.col1_ContraVale.Caption = "ContraVale"
        Me.col1_ContraVale.DisplayFormat.FormatString = "C0"
        Me.col1_ContraVale.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_ContraVale.FieldName = "ContraVale"
        Me.col1_ContraVale.Name = "col1_ContraVale"
        Me.col1_ContraVale.OptionsColumn.AllowEdit = False
        Me.col1_ContraVale.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ContraVale.Visible = True
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridC_1
        Me.GridView1.Name = "GridView1"
        '
        'Popup
        '
        Me.Popup.AlwaysScrollActiveControlIntoView = False
        Me.Popup.AutoSize = True
        Me.Popup.Controls.Add(Me.SimpleButton2)
        Me.Popup.Controls.Add(Me.SimpleButton1)
        Me.Popup.Controls.Add(Me.GridC_1)
        Me.Popup.Location = New System.Drawing.Point(563, 507)
        Me.Popup.Name = "Popup"
        Me.Popup.Size = New System.Drawing.Size(722, 153)
        Me.Popup.TabIndex = 7
        Me.Popup.Text = "Popup"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(525, 0)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(93, 23)
        Me.SimpleButton2.TabIndex = 8
        Me.SimpleButton2.Text = "Crear Reporte"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(624, 0)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(91, 23)
        Me.SimpleButton1.TabIndex = 7
        Me.SimpleButton1.Text = "Cerrar"
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(1283, 682)
        Me.Controls.Add(Me.Popup)
        Me.Controls.Add(Me.pic_Logo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(1284, 668)
        Me.Name = "Principal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VOficina 5+ Punto de Venta"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.pic_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Popup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Popup.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lbl_Usuario1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lbl_Perfil As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lbl_Ambiente As ToolStripStatusLabel
    Friend WithEvents lbl_Version As ToolStripStatusLabel
    Friend WithEvents lbl_Servidor As ToolStripStatusLabel
    Friend WithEvents lbl_Empresa As ToolStripStatusLabel
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents pic_Logo As PictureBox
    Friend WithEvents tsm_Configurar As ToolStripMenuItem
    Friend WithEvents tsm_Cajas As ToolStripMenuItem
    Friend WithEvents tsm_Cajeros As ToolStripMenuItem
    Friend WithEvents tsm_Vendedores As ToolStripMenuItem
    Friend WithEvents tsm_Administrar As ToolStripMenuItem
    Friend WithEvents tsm_Reportes As ToolStripMenuItem
    Friend WithEvents tsm_ParametrosControl As ToolStripMenuItem
    Friend WithEvents tsm_Cortes As ToolStripMenuItem
    Friend WithEvents tsm_Devoluciones As ToolStripMenuItem
    Friend WithEvents tsm_Cancelaciones As ToolStripMenuItem
    Friend WithEvents tsm_Acciones As ToolStripMenuItem
    Friend WithEvents tsm_PuntoVenta As ToolStripMenuItem
    Friend WithEvents lbl_BD As ToolStripStatusLabel
    Friend WithEvents lbl_Teclas As ToolStripStatusLabel
    Friend WithEvents MostradorToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CotizacionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RevisarExistenciasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReimprimirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbrirCajaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReimprimirCorteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DevolucionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CancelaciónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OpcionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArticulosNegadosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarClienteEventualToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefacturarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturaGlobalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VouchersToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CanjeValesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrestamoClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RecibirPagosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CorteDeCajaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents EstadoDeCuentaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CobranzaDiariaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents ReimprimirToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents RecibirPagosClienteFinalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents ReestructurasRelaciónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReestructurasClienteFinalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConveniosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReimprimirPagosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CorteDeCajaToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents TraspasoDeDineroToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents LiquidaciónesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PreciadoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Base2x3ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents N3x4ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents N3x4BToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents N4x9ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As ToolStripMenuItem
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Popup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents gridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents col1_IdCliente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdClienteFinal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ClienteFinal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImporteVale As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_PagoQuincenal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ContraVale As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
End Class
