﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogin))
        Me.Pnl_Login = New DevExpress.XtraEditors.PanelControl()
        Me.lbl_Version = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.Cmd_Cancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.Cmd_Enviar = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Password = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Usuario = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Pnl_Config = New DevExpress.XtraEditors.PanelControl()
        Me.cmd2_Cancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmd_Ok_Config = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_TestViscoi = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_TestControl = New DevExpress.XtraEditors.SimpleButton()
        Me.txt1_ConfigPass_VisCoi = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigPass_VisCoi = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_ConfigUser_VisCoi = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigUser_VisCoi = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_ConfigBaseDatos_VisCoi = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigBaseDatos_VisCoi = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_ConfigServer_VisCoi = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigServer_VisCoi = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_ConfigPass_Control = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigPass_Control = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_ConfigUser_Control = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigUser_Control = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_ConfigBaseDatos_Control = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigBaseDatos_Control = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_ConfigServer_Control = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1_ConfigServer_Control = New DevExpress.XtraEditors.LabelControl()
        Me.txt1_Llave = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.SFD1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.Pnl_Login, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_Login.SuspendLayout()
        CType(Me.txt_Password.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Usuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pnl_Config, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_Config.SuspendLayout()
        CType(Me.txt1_ConfigPass_VisCoi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_ConfigUser_VisCoi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_ConfigBaseDatos_VisCoi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_ConfigServer_VisCoi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_ConfigPass_Control.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_ConfigUser_Control.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_ConfigBaseDatos_Control.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_ConfigServer_Control.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_Llave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Pnl_Login
        '
        Me.Pnl_Login.Controls.Add(Me.lbl_Version)
        Me.Pnl_Login.Controls.Add(Me.LabelControl3)
        Me.Pnl_Login.Controls.Add(Me.LabelControl2)
        Me.Pnl_Login.Controls.Add(Me.Cmd_Cancelar)
        Me.Pnl_Login.Controls.Add(Me.Cmd_Enviar)
        Me.Pnl_Login.Controls.Add(Me.txt_Password)
        Me.Pnl_Login.Controls.Add(Me.Txt_Usuario)
        Me.Pnl_Login.Controls.Add(Me.LabelControl1)
        Me.Pnl_Login.Controls.Add(Me.PictureBox1)
        Me.Pnl_Login.Location = New System.Drawing.Point(10, 10)
        Me.Pnl_Login.Margin = New System.Windows.Forms.Padding(4)
        Me.Pnl_Login.Name = "Pnl_Login"
        Me.Pnl_Login.Size = New System.Drawing.Size(400, 300)
        Me.Pnl_Login.TabIndex = 3
        '
        'lbl_Version
        '
        Me.lbl_Version.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Version.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl_Version.Appearance.Options.UseFont = True
        Me.lbl_Version.Appearance.Options.UseForeColor = True
        Me.lbl_Version.Appearance.Options.UseTextOptions = True
        Me.lbl_Version.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl_Version.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.lbl_Version.Location = New System.Drawing.Point(47, 282)
        Me.lbl_Version.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl_Version.Name = "lbl_Version"
        Me.lbl_Version.Size = New System.Drawing.Size(150, 19)
        Me.lbl_Version.TabIndex = 11
        Me.lbl_Version.Text = "VERSION: 000.000"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Appearance.Options.UseForeColor = True
        Me.LabelControl3.Appearance.Options.UseTextOptions = True
        Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(13, 34)
        Me.LabelControl3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(381, 42)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = "Caja Registradora"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseForeColor = True
        Me.LabelControl2.Appearance.Options.UseTextOptions = True
        Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(13, 3)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(381, 38)
        Me.LabelControl2.TabIndex = 9
        Me.LabelControl2.Text = "PDV"
        '
        'Cmd_Cancelar
        '
        Me.Cmd_Cancelar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Cmd_Cancelar.Appearance.Options.UseFont = True
        Me.Cmd_Cancelar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.Cmd_Cancelar.ImageOptions.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.Cmd_Cancelar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.Cmd_Cancelar.Location = New System.Drawing.Point(328, 226)
        Me.Cmd_Cancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.Cmd_Cancelar.Name = "Cmd_Cancelar"
        Me.Cmd_Cancelar.Size = New System.Drawing.Size(48, 48)
        Me.Cmd_Cancelar.TabIndex = 7
        '
        'Cmd_Enviar
        '
        Me.Cmd_Enviar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Cmd_Enviar.Appearance.Options.UseFont = True
        Me.Cmd_Enviar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.Cmd_Enviar.ImageOptions.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.Cmd_Enviar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.Cmd_Enviar.Location = New System.Drawing.Point(225, 226)
        Me.Cmd_Enviar.Margin = New System.Windows.Forms.Padding(4)
        Me.Cmd_Enviar.Name = "Cmd_Enviar"
        Me.Cmd_Enviar.Size = New System.Drawing.Size(48, 48)
        Me.Cmd_Enviar.TabIndex = 6
        '
        'txt_Password
        '
        Me.txt_Password.EditValue = ""
        Me.txt_Password.Location = New System.Drawing.Point(211, 182)
        Me.txt_Password.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Password.Name = "txt_Password"
        Me.txt_Password.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txt_Password.Properties.Appearance.Options.UseFont = True
        Me.txt_Password.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Password.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Password.Properties.NullText = "Contraseña"
        Me.txt_Password.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_Password.Size = New System.Drawing.Size(170, 30)
        Me.txt_Password.TabIndex = 5
        '
        'Txt_Usuario
        '
        Me.Txt_Usuario.Location = New System.Drawing.Point(211, 140)
        Me.Txt_Usuario.Margin = New System.Windows.Forms.Padding(4)
        Me.Txt_Usuario.Name = "Txt_Usuario"
        Me.Txt_Usuario.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.Txt_Usuario.Properties.Appearance.Options.UseFont = True
        Me.Txt_Usuario.Properties.Appearance.Options.UseTextOptions = True
        Me.Txt_Usuario.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Txt_Usuario.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Usuario.Properties.NullText = "Usuario"
        Me.Txt_Usuario.Size = New System.Drawing.Size(170, 30)
        Me.Txt_Usuario.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Appearance.Options.UseTextOptions = True
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(211, 113)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(170, 19)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "INICIAR SESIÓN"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.proyVOficina_PDV.My.Resources.Resources.logo_g
        Me.PictureBox1.Location = New System.Drawing.Point(13, 84)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(190, 190)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'Pnl_Config
        '
        Me.Pnl_Config.Controls.Add(Me.cmd2_Cancelar)
        Me.Pnl_Config.Controls.Add(Me.cmd_Ok_Config)
        Me.Pnl_Config.Controls.Add(Me.btn_TestViscoi)
        Me.Pnl_Config.Controls.Add(Me.btn_TestControl)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigPass_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigPass_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigUser_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigUser_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigBaseDatos_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigBaseDatos_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigServer_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigServer_VisCoi)
        Me.Pnl_Config.Controls.Add(Me.LabelControl15)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigPass_Control)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigPass_Control)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigUser_Control)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigUser_Control)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigBaseDatos_Control)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigBaseDatos_Control)
        Me.Pnl_Config.Controls.Add(Me.txt1_ConfigServer_Control)
        Me.Pnl_Config.Controls.Add(Me.lbl1_ConfigServer_Control)
        Me.Pnl_Config.Controls.Add(Me.txt1_Llave)
        Me.Pnl_Config.Controls.Add(Me.LabelControl6)
        Me.Pnl_Config.Controls.Add(Me.LabelControl5)
        Me.Pnl_Config.Controls.Add(Me.LabelControl4)
        Me.Pnl_Config.Location = New System.Drawing.Point(417, 10)
        Me.Pnl_Config.Name = "Pnl_Config"
        Me.Pnl_Config.Size = New System.Drawing.Size(400, 450)
        Me.Pnl_Config.TabIndex = 4
        '
        'cmd2_Cancelar
        '
        Me.cmd2_Cancelar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.cmd2_Cancelar.Appearance.Options.UseFont = True
        Me.cmd2_Cancelar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.cmd2_Cancelar.ImageOptions.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.cmd2_Cancelar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.cmd2_Cancelar.Location = New System.Drawing.Point(285, 399)
        Me.cmd2_Cancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmd2_Cancelar.Name = "cmd2_Cancelar"
        Me.cmd2_Cancelar.Size = New System.Drawing.Size(48, 48)
        Me.cmd2_Cancelar.TabIndex = 34
        '
        'cmd_Ok_Config
        '
        Me.cmd_Ok_Config.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.cmd_Ok_Config.Appearance.Options.UseFont = True
        Me.cmd_Ok_Config.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.cmd_Ok_Config.ImageOptions.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.cmd_Ok_Config.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.cmd_Ok_Config.Location = New System.Drawing.Point(71, 396)
        Me.cmd_Ok_Config.Margin = New System.Windows.Forms.Padding(4)
        Me.cmd_Ok_Config.Name = "cmd_Ok_Config"
        Me.cmd_Ok_Config.Size = New System.Drawing.Size(48, 48)
        Me.cmd_Ok_Config.TabIndex = 33
        '
        'btn_TestViscoi
        '
        Me.btn_TestViscoi.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btn_TestViscoi.Appearance.Options.UseFont = True
        Me.btn_TestViscoi.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.btn_TestViscoi.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btn_TestViscoi.Location = New System.Drawing.Point(240, 246)
        Me.btn_TestViscoi.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_TestViscoi.Name = "btn_TestViscoi"
        Me.btn_TestViscoi.Size = New System.Drawing.Size(156, 28)
        Me.btn_TestViscoi.TabIndex = 32
        Me.btn_TestViscoi.Text = "Probar Cnx Viscoi"
        '
        'btn_TestControl
        '
        Me.btn_TestControl.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btn_TestControl.Appearance.Options.UseFont = True
        Me.btn_TestControl.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.btn_TestControl.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btn_TestControl.Location = New System.Drawing.Point(240, 82)
        Me.btn_TestControl.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_TestControl.Name = "btn_TestControl"
        Me.btn_TestControl.Size = New System.Drawing.Size(156, 28)
        Me.btn_TestControl.TabIndex = 31
        Me.btn_TestControl.Text = "Probar Cnx Control"
        '
        'txt1_ConfigPass_VisCoi
        '
        Me.txt1_ConfigPass_VisCoi.EditValue = ""
        Me.txt1_ConfigPass_VisCoi.Location = New System.Drawing.Point(216, 365)
        Me.txt1_ConfigPass_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigPass_VisCoi.Name = "txt1_ConfigPass_VisCoi"
        Me.txt1_ConfigPass_VisCoi.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigPass_VisCoi.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigPass_VisCoi.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigPass_VisCoi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigPass_VisCoi.Properties.NullText = "Usuario"
        Me.txt1_ConfigPass_VisCoi.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt1_ConfigPass_VisCoi.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigPass_VisCoi.TabIndex = 30
        '
        'lbl1_ConfigPass_VisCoi
        '
        Me.lbl1_ConfigPass_VisCoi.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigPass_VisCoi.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigPass_VisCoi.Appearance.Options.UseFont = True
        Me.lbl1_ConfigPass_VisCoi.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigPass_VisCoi.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigPass_VisCoi.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigPass_VisCoi.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigPass_VisCoi.Location = New System.Drawing.Point(216, 336)
        Me.lbl1_ConfigPass_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigPass_VisCoi.Name = "lbl1_ConfigPass_VisCoi"
        Me.lbl1_ConfigPass_VisCoi.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigPass_VisCoi.TabIndex = 29
        Me.lbl1_ConfigPass_VisCoi.Text = "PASSWORD"
        Me.lbl1_ConfigPass_VisCoi.Visible = False
        '
        'txt1_ConfigUser_VisCoi
        '
        Me.txt1_ConfigUser_VisCoi.EditValue = ""
        Me.txt1_ConfigUser_VisCoi.Location = New System.Drawing.Point(216, 305)
        Me.txt1_ConfigUser_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigUser_VisCoi.Name = "txt1_ConfigUser_VisCoi"
        Me.txt1_ConfigUser_VisCoi.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigUser_VisCoi.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigUser_VisCoi.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigUser_VisCoi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigUser_VisCoi.Properties.NullText = "Usuario"
        Me.txt1_ConfigUser_VisCoi.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigUser_VisCoi.TabIndex = 28
        '
        'lbl1_ConfigUser_VisCoi
        '
        Me.lbl1_ConfigUser_VisCoi.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigUser_VisCoi.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigUser_VisCoi.Appearance.Options.UseFont = True
        Me.lbl1_ConfigUser_VisCoi.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigUser_VisCoi.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigUser_VisCoi.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigUser_VisCoi.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigUser_VisCoi.Location = New System.Drawing.Point(216, 277)
        Me.lbl1_ConfigUser_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigUser_VisCoi.Name = "lbl1_ConfigUser_VisCoi"
        Me.lbl1_ConfigUser_VisCoi.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigUser_VisCoi.TabIndex = 27
        Me.lbl1_ConfigUser_VisCoi.Text = "USUARIO"
        Me.lbl1_ConfigUser_VisCoi.Visible = False
        '
        'txt1_ConfigBaseDatos_VisCoi
        '
        Me.txt1_ConfigBaseDatos_VisCoi.EditValue = "VISCOI"
        Me.txt1_ConfigBaseDatos_VisCoi.Location = New System.Drawing.Point(6, 365)
        Me.txt1_ConfigBaseDatos_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigBaseDatos_VisCoi.Name = "txt1_ConfigBaseDatos_VisCoi"
        Me.txt1_ConfigBaseDatos_VisCoi.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigBaseDatos_VisCoi.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigBaseDatos_VisCoi.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigBaseDatos_VisCoi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigBaseDatos_VisCoi.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt1_ConfigBaseDatos_VisCoi.Properties.NullText = "Usuario"
        Me.txt1_ConfigBaseDatos_VisCoi.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigBaseDatos_VisCoi.TabIndex = 26
        '
        'lbl1_ConfigBaseDatos_VisCoi
        '
        Me.lbl1_ConfigBaseDatos_VisCoi.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigBaseDatos_VisCoi.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigBaseDatos_VisCoi.Appearance.Options.UseFont = True
        Me.lbl1_ConfigBaseDatos_VisCoi.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigBaseDatos_VisCoi.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigBaseDatos_VisCoi.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigBaseDatos_VisCoi.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigBaseDatos_VisCoi.Location = New System.Drawing.Point(6, 336)
        Me.lbl1_ConfigBaseDatos_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigBaseDatos_VisCoi.Name = "lbl1_ConfigBaseDatos_VisCoi"
        Me.lbl1_ConfigBaseDatos_VisCoi.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigBaseDatos_VisCoi.TabIndex = 25
        Me.lbl1_ConfigBaseDatos_VisCoi.Text = "BASE DATOS"
        Me.lbl1_ConfigBaseDatos_VisCoi.Visible = False
        '
        'txt1_ConfigServer_VisCoi
        '
        Me.txt1_ConfigServer_VisCoi.EditValue = "000.000.000.000"
        Me.txt1_ConfigServer_VisCoi.Location = New System.Drawing.Point(6, 305)
        Me.txt1_ConfigServer_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigServer_VisCoi.Name = "txt1_ConfigServer_VisCoi"
        Me.txt1_ConfigServer_VisCoi.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigServer_VisCoi.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigServer_VisCoi.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigServer_VisCoi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigServer_VisCoi.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt1_ConfigServer_VisCoi.Properties.NullText = "Usuario"
        Me.txt1_ConfigServer_VisCoi.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigServer_VisCoi.TabIndex = 24
        '
        'lbl1_ConfigServer_VisCoi
        '
        Me.lbl1_ConfigServer_VisCoi.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigServer_VisCoi.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigServer_VisCoi.Appearance.Options.UseFont = True
        Me.lbl1_ConfigServer_VisCoi.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigServer_VisCoi.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigServer_VisCoi.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigServer_VisCoi.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigServer_VisCoi.Location = New System.Drawing.Point(6, 277)
        Me.lbl1_ConfigServer_VisCoi.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigServer_VisCoi.Name = "lbl1_ConfigServer_VisCoi"
        Me.lbl1_ConfigServer_VisCoi.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigServer_VisCoi.TabIndex = 23
        Me.lbl1_ConfigServer_VisCoi.Text = "SERVIDOR"
        Me.lbl1_ConfigServer_VisCoi.Visible = False
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Appearance.Options.UseForeColor = True
        Me.LabelControl15.Appearance.Options.UseTextOptions = True
        Me.LabelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl15.Location = New System.Drawing.Point(6, 250)
        Me.LabelControl15.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(225, 28)
        Me.LabelControl15.TabIndex = 22
        Me.LabelControl15.Text = "SQL VISCOI"
        '
        'txt1_ConfigPass_Control
        '
        Me.txt1_ConfigPass_Control.EditValue = ""
        Me.txt1_ConfigPass_Control.Location = New System.Drawing.Point(216, 198)
        Me.txt1_ConfigPass_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigPass_Control.Name = "txt1_ConfigPass_Control"
        Me.txt1_ConfigPass_Control.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigPass_Control.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigPass_Control.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigPass_Control.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigPass_Control.Properties.NullText = "Usuario"
        Me.txt1_ConfigPass_Control.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt1_ConfigPass_Control.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigPass_Control.TabIndex = 21
        '
        'lbl1_ConfigPass_Control
        '
        Me.lbl1_ConfigPass_Control.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigPass_Control.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigPass_Control.Appearance.Options.UseFont = True
        Me.lbl1_ConfigPass_Control.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigPass_Control.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigPass_Control.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigPass_Control.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigPass_Control.Location = New System.Drawing.Point(216, 169)
        Me.lbl1_ConfigPass_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigPass_Control.Name = "lbl1_ConfigPass_Control"
        Me.lbl1_ConfigPass_Control.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigPass_Control.TabIndex = 20
        Me.lbl1_ConfigPass_Control.Text = "PASSWORD"
        Me.lbl1_ConfigPass_Control.Visible = False
        '
        'txt1_ConfigUser_Control
        '
        Me.txt1_ConfigUser_Control.EditValue = ""
        Me.txt1_ConfigUser_Control.Location = New System.Drawing.Point(216, 141)
        Me.txt1_ConfigUser_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigUser_Control.Name = "txt1_ConfigUser_Control"
        Me.txt1_ConfigUser_Control.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigUser_Control.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigUser_Control.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigUser_Control.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigUser_Control.Properties.NullText = "Usuario"
        Me.txt1_ConfigUser_Control.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigUser_Control.TabIndex = 19
        '
        'lbl1_ConfigUser_Control
        '
        Me.lbl1_ConfigUser_Control.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigUser_Control.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigUser_Control.Appearance.Options.UseFont = True
        Me.lbl1_ConfigUser_Control.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigUser_Control.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigUser_Control.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigUser_Control.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigUser_Control.Location = New System.Drawing.Point(216, 113)
        Me.lbl1_ConfigUser_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigUser_Control.Name = "lbl1_ConfigUser_Control"
        Me.lbl1_ConfigUser_Control.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigUser_Control.TabIndex = 18
        Me.lbl1_ConfigUser_Control.Text = "USUARIO"
        Me.lbl1_ConfigUser_Control.Visible = False
        '
        'txt1_ConfigBaseDatos_Control
        '
        Me.txt1_ConfigBaseDatos_Control.EditValue = "CONTROL"
        Me.txt1_ConfigBaseDatos_Control.Location = New System.Drawing.Point(6, 198)
        Me.txt1_ConfigBaseDatos_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigBaseDatos_Control.Name = "txt1_ConfigBaseDatos_Control"
        Me.txt1_ConfigBaseDatos_Control.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigBaseDatos_Control.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigBaseDatos_Control.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigBaseDatos_Control.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigBaseDatos_Control.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt1_ConfigBaseDatos_Control.Properties.NullText = "Usuario"
        Me.txt1_ConfigBaseDatos_Control.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigBaseDatos_Control.TabIndex = 17
        '
        'lbl1_ConfigBaseDatos_Control
        '
        Me.lbl1_ConfigBaseDatos_Control.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigBaseDatos_Control.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigBaseDatos_Control.Appearance.Options.UseFont = True
        Me.lbl1_ConfigBaseDatos_Control.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigBaseDatos_Control.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigBaseDatos_Control.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigBaseDatos_Control.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigBaseDatos_Control.Location = New System.Drawing.Point(6, 169)
        Me.lbl1_ConfigBaseDatos_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigBaseDatos_Control.Name = "lbl1_ConfigBaseDatos_Control"
        Me.lbl1_ConfigBaseDatos_Control.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigBaseDatos_Control.TabIndex = 16
        Me.lbl1_ConfigBaseDatos_Control.Text = "BASE DATOS"
        Me.lbl1_ConfigBaseDatos_Control.Visible = False
        '
        'txt1_ConfigServer_Control
        '
        Me.txt1_ConfigServer_Control.EditValue = "000.000.000.000"
        Me.txt1_ConfigServer_Control.Location = New System.Drawing.Point(6, 141)
        Me.txt1_ConfigServer_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_ConfigServer_Control.Name = "txt1_ConfigServer_Control"
        Me.txt1_ConfigServer_Control.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_ConfigServer_Control.Properties.Appearance.Options.UseFont = True
        Me.txt1_ConfigServer_Control.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1_ConfigServer_Control.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_ConfigServer_Control.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt1_ConfigServer_Control.Properties.NullText = "Usuario"
        Me.txt1_ConfigServer_Control.Size = New System.Drawing.Size(180, 26)
        Me.txt1_ConfigServer_Control.TabIndex = 15
        '
        'lbl1_ConfigServer_Control
        '
        Me.lbl1_ConfigServer_Control.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1_ConfigServer_Control.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbl1_ConfigServer_Control.Appearance.Options.UseFont = True
        Me.lbl1_ConfigServer_Control.Appearance.Options.UseForeColor = True
        Me.lbl1_ConfigServer_Control.Appearance.Options.UseTextOptions = True
        Me.lbl1_ConfigServer_Control.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl1_ConfigServer_Control.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl1_ConfigServer_Control.Location = New System.Drawing.Point(6, 113)
        Me.lbl1_ConfigServer_Control.Margin = New System.Windows.Forms.Padding(4)
        Me.lbl1_ConfigServer_Control.Name = "lbl1_ConfigServer_Control"
        Me.lbl1_ConfigServer_Control.Size = New System.Drawing.Size(180, 24)
        Me.lbl1_ConfigServer_Control.TabIndex = 14
        Me.lbl1_ConfigServer_Control.Text = "SERVIDOR"
        Me.lbl1_ConfigServer_Control.Visible = False
        '
        'txt1_Llave
        '
        Me.txt1_Llave.EditValue = "ICC"
        Me.txt1_Llave.Location = New System.Drawing.Point(216, 42)
        Me.txt1_Llave.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1_Llave.Name = "txt1_Llave"
        Me.txt1_Llave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt1_Llave.Properties.Appearance.Options.UseFont = True
        Me.txt1_Llave.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt1_Llave.Properties.NullText = "Usuario"
        Me.txt1_Llave.Size = New System.Drawing.Size(180, 26)
        Me.txt1_Llave.TabIndex = 13
        Me.txt1_Llave.Visible = False
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Appearance.Options.UseForeColor = True
        Me.LabelControl6.Appearance.Options.UseTextOptions = True
        Me.LabelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl6.Location = New System.Drawing.Point(6, 82)
        Me.LabelControl6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(225, 28)
        Me.LabelControl6.TabIndex = 12
        Me.LabelControl6.Text = "SQL CONTROL"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Appearance.Options.UseForeColor = True
        Me.LabelControl5.Appearance.Options.UseTextOptions = True
        Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Location = New System.Drawing.Point(6, 6)
        Me.LabelControl5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(408, 28)
        Me.LabelControl5.TabIndex = 11
        Me.LabelControl5.Text = "Configuración de Archivo Inicio"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Appearance.Options.UseForeColor = True
        Me.LabelControl4.Location = New System.Drawing.Point(6, 45)
        Me.LabelControl4.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(182, 19)
        Me.LabelControl4.TabIndex = 4
        Me.LabelControl4.Text = "LLAVE DE SEGURIDAD"
        Me.LabelControl4.Visible = False
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(96, 318)
        Me.RichTextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(297, 129)
        Me.RichTextBox1.TabIndex = 25
        Me.RichTextBox1.Text = ""
        Me.RichTextBox1.Visible = False
        '
        'SFD1
        '
        Me.SFD1.DefaultExt = "dat"
        '
        'frmLogin
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(820, 470)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Controls.Add(Me.Pnl_Config)
        Me.Controls.Add(Me.Pnl_Login)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmLogin"
        CType(Me.Pnl_Login, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_Login.ResumeLayout(False)
        Me.Pnl_Login.PerformLayout()
        CType(Me.txt_Password.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Usuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pnl_Config, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_Config.ResumeLayout(False)
        Me.Pnl_Config.PerformLayout()
        CType(Me.txt1_ConfigPass_VisCoi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_ConfigUser_VisCoi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_ConfigBaseDatos_VisCoi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_ConfigServer_VisCoi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_ConfigPass_Control.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_ConfigUser_Control.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_ConfigBaseDatos_Control.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_ConfigServer_Control.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_Llave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Pnl_Login As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Cmd_Enviar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Password As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_Usuario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Cmd_Cancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl_Version As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Pnl_Config As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmd2_Cancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmd_Ok_Config As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_TestViscoi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_TestControl As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt1_ConfigPass_VisCoi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigPass_VisCoi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_ConfigUser_VisCoi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigUser_VisCoi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_ConfigBaseDatos_VisCoi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigBaseDatos_VisCoi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_ConfigServer_VisCoi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigServer_VisCoi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_ConfigPass_Control As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigPass_Control As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_ConfigUser_Control As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigUser_Control As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_ConfigBaseDatos_Control As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigBaseDatos_Control As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_ConfigServer_Control As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1_ConfigServer_Control As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1_Llave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents SFD1 As SaveFileDialog
End Class
