﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPopUp
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GC_ProgramaLealtad = New DevExpress.XtraEditors.GroupControl()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_NombreCanje = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_CodigoCanje = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        CType(Me.GC_ProgramaLealtad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_ProgramaLealtad.SuspendLayout()
        CType(Me.txt_NombreCanje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GC_ProgramaLealtad
        '
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label1)
        Me.GC_ProgramaLealtad.Controls.Add(Me.TextEdit1)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label5)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_NombreCanje)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label4)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_CodigoCanje)
        Me.GC_ProgramaLealtad.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_ProgramaLealtad.Location = New System.Drawing.Point(0, 0)
        Me.GC_ProgramaLealtad.Name = "GC_ProgramaLealtad"
        Me.GC_ProgramaLealtad.Size = New System.Drawing.Size(571, 143)
        Me.GC_ProgramaLealtad.TabIndex = 2
        Me.GC_ProgramaLealtad.Text = "Crear Reporte"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(5, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 24)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Nombre"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt_NombreCanje
        '
        Me.txt_NombreCanje.Location = New System.Drawing.Point(169, 62)
        Me.txt_NombreCanje.Name = "txt_NombreCanje"
        Me.txt_NombreCanje.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreCanje.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreCanje.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreCanje.Size = New System.Drawing.Size(326, 30)
        Me.txt_NombreCanje.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 24)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Codigo Canje"
        '
        'txt_CodigoCanje
        '
        Me.txt_CodigoCanje.Location = New System.Drawing.Point(169, 26)
        Me.txt_CodigoCanje.Name = "txt_CodigoCanje"
        Me.txt_CodigoCanje.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CodigoCanje.Properties.Appearance.Options.UseFont = True
        Me.txt_CodigoCanje.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CodigoCanje.Size = New System.Drawing.Size(326, 30)
        Me.txt_CodigoCanje.TabIndex = 0
        '
        'GroupControl3
        '
        Me.GroupControl3.AllowHtmlText = True
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 143)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(571, 84)
        Me.GroupControl3.TabIndex = 3
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(258, 30)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 24)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Nombre"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(169, 98)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit1.Size = New System.Drawing.Size(326, 30)
        Me.TextEdit1.TabIndex = 10
        '
        'frmPopUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(571, 227)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GC_ProgramaLealtad)
        Me.Name = "frmPopUp"
        Me.Text = "popup"
        CType(Me.GC_ProgramaLealtad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_ProgramaLealtad.ResumeLayout(False)
        Me.GC_ProgramaLealtad.PerformLayout()
        CType(Me.txt_NombreCanje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GC_ProgramaLealtad As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_NombreCanje As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_CodigoCanje As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
End Class
