﻿Imports System
Imports System.Xml
''Imports System.Security.Cryptography

Public Class frmLogin
    Dim SW_ArchivoINI As Boolean = False
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        CargaIni()
    End Sub
    Private Sub CargaIni()
        Dim xmlDoc As System.Xml.XmlDocument
        Dim sXml As String
        Dim XmlRuta As String
        Dim RichTextBox1 As New RichTextBox

        Dim Control_Servidor As String = ""
        Dim Control_BaseDAtos As String = ""
        Dim Control_User As String = ""
        Dim Control_Pass As String = ""
        Dim Viscoi_Servidor As String = ""
        Dim Viscoi_BaseDAtos As String = ""
        Dim Viscoi_User As String = ""
        Dim Viscoi_Pass As String = ""

        Dim Minor As String
        Dim MinorRevision As String
        Dim Major As String
        Dim MajorRevision As String
        Try
            Globales.oAmbientes.Sistema = Application.ProductName.Substring(3)
            Globales.oAmbientes.oControl = New Globales.clsConexion
            Globales.oAmbientes.oViscoi = New Globales.clsConexion
            Globales.oAmbientes.oUsuario = New Globales.clsUsuarios
            Globales.oAmbientes.oFormulario = New Globales.clsFormularios

            xmlDoc = New System.Xml.XmlDocument()
            XmlRuta = "inicio.ini"
            Try
                RichTextBox1.LoadFile(XmlRuta)
                sXml = CryptoZ.Decrypt(RichTextBox1.Text, "ICC")
                xmlDoc.LoadXml(sXml)
                SW_ArchivoINI = True

            Catch ex As Exception
                MostrarMensaje("No se encontro el Archivo  para tener acceso al Sistema." & ex.Message)

                SW_ArchivoINI = False

            End Try

            If SW_ArchivoINI Then
                Globales.oAmbientes.SW_Produccion = False
                Globales.oAmbientes.FechaEjecucion = Today

                Call LlenaPaletaColores()

                Control_Servidor = xmlDoc.SelectNodes("archivo_inicio/sql_control/server").Item(0).InnerText
                Control_BaseDAtos = xmlDoc.SelectNodes("archivo_inicio/sql_control/basedatos").Item(0).InnerText
                Control_User = xmlDoc.SelectNodes("archivo_inicio/sql_control/usuario").Item(0).InnerText
                Control_Pass = xmlDoc.SelectNodes("archivo_inicio/sql_control/contrasena").Item(0).InnerText

                Viscoi_Servidor = xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/server").Item(0).InnerText
                Viscoi_BaseDAtos = xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/basedatos").Item(0).InnerText
                Viscoi_User = xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/usuario").Item(0).InnerText
                Viscoi_Pass = xmlDoc.SelectNodes("archivo_inicio/sql_viscoi/contrasena").Item(0).InnerText

                'Control_Servidor = "138.128.244.197\SQLEXPRESS"
                'Control_BaseDAtos = "control"
                'Control_User = "UsrAppICCPK"
                'Control_Pass = "Iqjq534qD43q58fq#22"

                'Viscoi_Servidor = "138.128.244.197\SQLEXPRESS"
                'Viscoi_BaseDAtos = "voficina_pruebas"
                'Viscoi_User = "UsrAppICCPK"
                'Viscoi_Pass = "Iqjq534qD43q58fq#22"

                'Control_Servidor = "192.168.55.1\MSSQLSERVERVO"
                'Control_BaseDAtos = "control"
                'Control_User = "sa"
                'Control_Pass = "ssdNOW200@60Gb2.5pvo"

                'Viscoi_Servidor = "192.168.55.1\MSSQLSERVERVO"
                'Viscoi_BaseDAtos = "viscoi_pdv_1005"
                'Viscoi_User = "sa"
                'Viscoi_Pass = "ssdNOW200@60Gb2.5pvo"

                Globales.oAmbientes.SW_Produccion = True


                Globales.oAmbientes.oControl.cnx_Seguridad(Control_Servidor, Control_BaseDAtos, Control_User, Control_Pass)
                Globales.oAmbientes.oViscoi.cnx_Seguridad(Viscoi_Servidor, Viscoi_BaseDAtos, Viscoi_User, Viscoi_Pass)

                ''If Globales.oAmbientes.Sistema = "Supply" Then
                Globales.oAmbientes.Servidor = Viscoi_Servidor
                'End If

                Minor = Format(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build, "000")
                MinorRevision = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.MinorRevision
                Major = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major
                MajorRevision = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString

                Globales.oAmbientes.BaseDatos = Viscoi_BaseDAtos

                Globales.oAmbientes.SW_Version = False
                Globales.oAmbientes.Version = Major & "." & MajorRevision & "." & Minor
                lbl_Version.Text = "Version: " & Globales.oAmbientes.Version
            Else

            End If
        Catch ex As Exception
            MostrarMensaje("No se encontro el Archivo  para tener acceso al Sistema." & ex.Message)
            Me.Close()
        End Try
    End Sub
    Public Sub MostrarMensaje(ByVal sMensaje As String)
        MessageBox.Show(sMensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub Cmd_Enviar_Click(sender As System.Object, e As System.EventArgs) Handles Cmd_Enviar.Click
        Dim cxSeguridad As System.Data.SqlClient.SqlConnection
        Dim Msg As String = ""
        Dim pass As String = ""
        Dim pass1 As String = ""
        Try
            cxSeguridad = Globales.oAmbientes.oControl.ObtenerConexion()
            pass1 = Txt_Usuario.Text.ToUpper().PadLeft(12)
            pass = Globales.oAmbientes.Crypt(pass1 & txt_Password.Text)

            'pass = txt_Password.Text.Trim()
            If Globales.oAmbientes.oUsuario.EsUsuarioValido(Txt_Usuario.Text.Trim(), pass, cxSeguridad, Msg) Then

                cxSeguridad = Globales.oAmbientes.oControl.ObtenerConexion()
                Globales.oAmbientes.oUsuario.Per_Pantallas_Permisos_SEL(cxSeguridad, Msg)

                'If Globales.oAmbientes.oUsuario.ValidaPermiso(Globales.oAmbientes.Sistema, "ADMINISTRARDATOS") Then
                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\ViscoiWMS\" & Globales.oAmbientes.Sistema, "user", Txt_Usuario.Text)
                    Me.Close()
                'Else
                '    MessageBox.Show("El usuario no cuenta con acceso al sistema " & Globales.oAmbientes.Sistema, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                'End If
            Else
                MostrarMensaje(Msg)
            End If
        Catch ex As Exception
            MostrarMensaje("Ocurrio un error: " & ex.Message.ToString())
        End Try
    End Sub
    Private Sub Txt_Usuario_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Txt_Usuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            txt_Password.Focus()
        End If
    End Sub
    Private Sub txt_Password_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_Password.KeyUp
        If e.KeyCode = Keys.Enter Then
            Cmd_Enviar.Focus()
            Call Cmd_Enviar_Click(Cmd_Enviar, e)
        End If
    End Sub
    Private Sub Cmd_Cancelar_Click(sender As System.Object, e As System.EventArgs) Handles Cmd_Cancelar.Click _
                                                                                         , cmd2_Cancelar.Click
        Globales.oAmbientes.oUsuario = Nothing
        Me.Close()
    End Sub
    Private Sub frmLogin_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Try
            If SW_ArchivoINI Then

                Me.Width = 10 + Pnl_Login.Width + 10
                Me.Height = 10 + Pnl_Login.Height + 10

                Pnl_Login.Visible = True
                Pnl_Config.Visible = False

                Call Valida_Version()

                Txt_Usuario.Focus()
                Txt_Usuario.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\ViscoiWMS\" & Globales.oAmbientes.Sistema, "user", "")

                If Txt_Usuario.Text <> "" Then
                    txt_Password.Focus()
                End If
            Else
                Me.Width = 10 + Pnl_Config.Width + 10
                Me.Height = 10 + Pnl_Config.Height + 10

                Pnl_Login.Visible = False
                Pnl_Config.Visible = True

                Pnl_Config.Top = Pnl_Login.Top
                Pnl_Config.Left = Pnl_Login.Left
            End If
            Me.CenterToScreen()
        Catch ex As Exception
            MostrarMensaje("Ocurrio un error: " & ex.Message.ToString())
        End Try
    End Sub
    Private Sub Valida_Version()
        Dim oDatos As Datos_Control
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Control
            oDatos.WMS_Versiones_Sel("VOFICINA", "PDV", dtDatos, Msj)

            Dim list1 = From p In dtDatos
                        Select version = p.Item("version")

            If list1.Count > 0 Then
                'If Globales.oAmbientes.Version = list1(0) Then
                If True Then
                    Globales.oAmbientes.SW_Version = True

                    'Globales.oAmbientes.Jerarquia01 = oDatos.Opciones_Proceso_Str_Sel(Globales.oAmbientes.Id_Empresa, "CATALOGOS", "JERARQUIA01 NOMBRE", Msj)
                    'Globales.oAmbientes.Jerarquia02 = oDatos.Opciones_Proceso_Str_Sel(Globales.oAmbientes.Id_Empresa, "CATALOGOS", "JERARQUIA02 NOMBRE", Msj)
                    'Globales.oAmbientes.Jerarquia03 = oDatos.Opciones_Proceso_Str_Sel(Globales.oAmbientes.Id_Empresa, "CATALOGOS", "JERARQUIA03 NOMBRE", Msj)
                    'Globales.oAmbientes.Jerarquia04 = oDatos.Opciones_Proceso_Str_Sel(Globales.oAmbientes.Id_Empresa, "CATALOGOS", "JERARQUIA04 NOMBRE", Msj)
                Else
                    Globales.oAmbientes.SW_Version = False
                    Me.Close()
                End If
            Else
                Globales.oAmbientes.SW_Version = False
                Me.Close()
            End If

        Catch ex As Exception
            MostrarMensaje("Ocurrio un error: " & ex.Message.ToString())
        End Try
        oDatos = Nothing
    End Sub

    Private Sub cmd_Ok_Config_Click(sender As Object, e As EventArgs) Handles cmd_Ok_Config.Click
        Try
            Dim xmlDoc As XmlDocument
            Dim sXml As String = "<archivo_inicio>" & vbNewLine _
                                & vbTab & "<id_empresa>#id_empresa#</id_empresa>" & vbNewLine _
                                & vbTab & "<sql_control>" & vbNewLine _
                                    & vbTab & "<server>#servidor_control#</server>" & vbNewLine _
                                    & vbTab & "<basedatos>#basedatos_control#</basedatos>" & vbNewLine _
                                    & vbTab & "<usuario>#user_control#</usuario>" & vbNewLine _
                                    & vbTab & "<contrasena>#pass_control#</contrasena>" & vbNewLine _
                                & vbTab & "</sql_control>" & vbNewLine _
                                & vbTab & "<sql_viscoi>" & vbNewLine _
                                    & vbTab & "<server>#servidor_viscoi#</server>" & vbNewLine _
                                    & vbTab & "<basedatos>#basedatos_viscoi#</basedatos>" & vbNewLine _
                                    & vbTab & "<usuario>#user_viscoi#</usuario>" & vbNewLine _
                                    & vbTab & "<contrasena>#pass_viscoi#</contrasena>" & vbNewLine _
                                & vbTab & "</sql_viscoi>" & vbNewLine _
                                & "</archivo_inicio>"

            sXml = sXml.Replace("#id_empresa#", "")

            sXml = sXml.Replace("#servidor_control#", txt1_ConfigServer_Control.Text)
            sXml = sXml.Replace("#basedatos_control#", txt1_ConfigBaseDatos_Control.Text)
            sXml = sXml.Replace("#user_control#", txt1_ConfigUser_Control.Text)
            sXml = sXml.Replace("#pass_control#", txt1_ConfigPass_Control.Text)

            sXml = sXml.Replace("#servidor_viscoi#", txt1_ConfigServer_VisCoi.Text)
            sXml = sXml.Replace("#basedatos_viscoi#", txt1_ConfigBaseDatos_VisCoi.Text)
            sXml = sXml.Replace("#user_viscoi#", txt1_ConfigUser_VisCoi.Text)
            sXml = sXml.Replace("#pass_viscoi#", txt1_ConfigPass_VisCoi.Text)
            RichTextBox1.Text = sXml

            xmlDoc = New XmlDocument()

            If txt1_Llave.Text <> "" Then
                If RichTextBox1.Text <> "" Then
                    xmlDoc.LoadXml(RichTextBox1.Text)
                    RichTextBox1.Text = CryptoZ.Encrypt(xmlDoc.OuterXml, txt1_Llave.Text)
                    If SFD1.ShowDialog = DialogResult.OK Then
                        If Not IO.File.Exists(SFD1.FileName) Then
                            RichTextBox1.SaveFile(SFD1.FileName)
                        End If
                        RichTextBox1.SaveFile(SFD1.FileName)
                    End If
                Else
                    MsgBox("No hay texto para convertir en XML")
                End If
            Else
                MsgBox("Debe Proporcionar la llave para encriptar o desencriptar.")
            End If
        Catch ex As Exception
            MostrarMensaje("Ocurrio un error: " & ex.Message.ToString())
        End Try
    End Sub
    Private Sub btn_TestControl_Click(sender As Object, e As EventArgs) Handles btn_TestControl.Click
        Dim oDatos As New Datos_Control
        Dim Msj As String = ""
        Try
            Globales.oAmbientes.oControl.cnx_Seguridad(txt1_ConfigServer_Control.Text, txt1_ConfigBaseDatos_Control.Text, txt1_ConfigUser_Control.Text, txt1_ConfigPass_Control.Text)

            If oDatos.Test(Msj) Then
                MostrarMensaje("Conexion CONTROL establecida con exito.")
            Else
                MostrarMensaje("Ocurrio un error: " & Msj)
            End If
        Catch ex As Exception
            MostrarMensaje("Ocurrio un error: " & ex.Message.ToString())
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_TestViscoi_Click(sender As Object, e As EventArgs) Handles btn_TestViscoi.Click
        Dim oDatos As New Datos_Viscoi
        Dim Msj As String = ""
        Try
            Globales.oAmbientes.oViscoi.cnx_Seguridad(txt1_ConfigServer_Control.Text, txt1_ConfigBaseDatos_Control.Text, txt1_ConfigUser_Control.Text, txt1_ConfigPass_Control.Text)

            If oDatos.Test(Msj) Then
                MostrarMensaje("Conexion VISCOI establecida con exito.")
            Else
                MostrarMensaje("Ocurrio un error: " & Msj)
            End If
        Catch ex As Exception
            MostrarMensaje("Ocurrio un error: " & ex.Message.ToString())
        End Try
        oDatos = Nothing
    End Sub
    Private Sub LlenaPaletaColores()
        Dim RutaPaletaColores As String = Application.StartupPath & "\Imagenes\PaletaColores.txt"
        If Not IO.File.Exists(RutaPaletaColores) Then Return

        Try
            Dim objStreamReader As System.IO.StreamReader
            Dim strLine As String
            Dim indice As Integer
            'Pass the file path and the file name to the StreamReader constructor.
            objStreamReader = New System.IO.StreamReader(RutaPaletaColores)
            'Read the first line of text.
            strLine = objStreamReader.ReadLine
            'Continue to read until you reach the end of the file.
            Do While Not strLine Is Nothing
                'Write the line to the Console window.
                If strLine.Substring(0, 1) <> "#" Then
                    Try
                        indice = CInt(strLine.Substring(0, 1))
                    Catch ex As Exception
                        indice = 0
                    End Try
                    Globales.oAmbientes.PaletaColores(indice) = Globales.oAmbientes.hexToRbgNew(strLine.Substring(2, 6))
                    'Read the next line.
                End If
                strLine = objStreamReader.ReadLine
            Loop
            'Close the file.
            objStreamReader.Close()

            Me.BackColor = Globales.oAmbientes.PaletaColores(0)
            LabelControl2.ForeColor = Globales.oAmbientes.PaletaColores(0)
            LabelControl3.ForeColor = Globales.oAmbientes.PaletaColores(0)

        Catch ex As Exception
            MostrarMensaje("Ocurrio un error: " & ex.Message.ToString())
        End Try
    End Sub
End Class