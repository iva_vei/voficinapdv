﻿Imports DevExpress.XtraBars
Public Class Formulario_Plantilla
    Private Modo As Globales.TipoABC
    Public Sub New()
        InitializeComponent()
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 12.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControl.OptionsFocus.EnableAutoTabOrder = False
        Me.GridV_1.Appearance.Row.ForeColor = System.Drawing.Color.Black
    End Sub
    Private Sub ABCPantallas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim msj As String = ""
        Try
            Modo = Globales.TipoABC.Indice
            tnpPage2.Visible = False
            tnpPage2.Enabled = False

            Call ControlBotones("Ok")
            Call Botones("Ok")
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Public Sub Refrescar()
        Dim oDatos_Seguridad As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos_Seguridad = New Datos_Viscoi

            'If oDatos_Seguridad.Seguridad_Usuarios_Sel(Globales.oAmbientes.Id_Empresa, dtDatos, Mensaje) Then
            '    GridC_1.DataSource = dtDatos
            'Else
            '    GridC_1.DataSource = Nothing
            'End If
            GridV_1.BestFitColumns()
            Call Titulo_BtnPanel(False)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos_Seguridad = Nothing
    End Sub
    Public Sub Imprimir()
        Dim Formularios As Globales.clsFormularios
        Try
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "IMPRIMIR") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Formularios = New Globales.clsFormularios
            Formularios.Imprimir(Me.Name, "Formulario Plantilla", GridC_1)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Excel()
        Dim Formularios As Globales.clsFormularios
        Try
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "EXCEL") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Formularios = New Globales.clsFormularios
            Formularios.ExportarXLS(Me.Name, "Formulario Plantilla", GridC_1, Globales.oAmbientes.Sistema, Application.ProductName, "ICC")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Regresar()
        Try
            Select Case TabPane1.SelectedPage.Caption
                Case tnpPage1.Caption
                    Me.Close()
                Case tnpPage2.Caption
                    TabPane1.SelectedPage = tnpPage1
                    tnpPage1.Enabled = True
                    tnpPage2.Enabled = False
                    Modo = Globales.TipoABC.Indice
                    Call Titulo_BtnPanel(False)
                    Call Botones("Cancelar")
                    Call Refrescar()
            End Select

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Titulo_BtnPanel(ByVal sw_visible)
        PanelFiltros_Acciones.Visible = sw_visible
        If PanelFiltros_Acciones.Visible = True Then
            btn_OcultarMostrar_Acciones.Text = "Ocultar Filtros y Acciones"
        Else
            btn_OcultarMostrar_Acciones.Text = "Mostrar Filtros y Acciones"
        End If
    End Sub
    Private Sub btn_OcultarMostrar_Acciones_Click(sender As Object, e As EventArgs) Handles btn_OcultarMostrar_Acciones.Click
        Call Titulo_BtnPanel(Not PanelFiltros_Acciones.Visible)
    End Sub

    Private Sub Botones(ByVal tag As String)
        Dim btnNuevo As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Nuevo")
        Dim btnEditar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Editar")
        Dim btnEliminar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Eliminar")
        Dim btnOk As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Ok")
        Dim btnCancelar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Cancelar")

        Select Case tag
            Case "Nuevo", "Editar", "Eliminar", "Consulta"
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnEliminar.Visible = False
                btnOk.Visible = True
                btnCancelar.Visible = True
            Case "Ok", "Cancelar", "Consulta"
                btnNuevo.Visible = True
                btnEditar.Visible = True
                btnEliminar.Visible = True
                btnOk.Visible = False
                btnCancelar.Visible = False
        End Select
    End Sub
    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0
        Try
            Select Case tag
                Case "Nuevo"
                    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "INSERTAR") Then
                        MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If

                    Call Botones(tag)
                    Modo = Globales.TipoABC.Nuevo
                    'Call Mostrar_Detalle(Modo)
                Case "Editar"
                    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "ACTUALIZAR") Then
                        MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If

                    Call Botones(tag)
                    Modo = Globales.TipoABC.Modificar
                    'Call Mostrar_Detalle(Modo)
                Case "Eliminar"
                    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "CANCELAR") Then
                        MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If

                    Call Botones(tag)
                    Modo = Globales.TipoABC.Eliminar
                    'Call Mostrar_Detalle(Modo)
                Case "Consulta"
                    Call Botones(tag)
                    Modo = Globales.TipoABC.Consulta
                    'Call Mostrar_Detalle(Modo)
                Case "Cancelar"
                    Call Botones(tag)
                    Call Regresar()
                Case "Ok"
                    Select Case Modo
                        Case Globales.TipoABC.Nuevo
                            'If txt_pass.Text = txt_pass_confirma.Text Then
                            '    pass = Globales.oAmbientes.Crypt(txt_Usuario.Text.Trim() & txt_pass.Text.Trim())
                            '    If MessageBox.Show("¿Desea guardar la información del nuevo usuario?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            '        IdUsuario = Globales.oAmbientes.Valor(txt_IdUsuario.Text)
                            '        If oDatos.Seguridad_Usuarios_ABC("A", IdUsuario, txt_Usuario.Text, txt_Nombre.Text, pass, cbo_Perfiles.SelectedValue, CDate(txt_fecha_alta.Text), dtx_fecha_vence.DateTime, 0, Msj) Then
                            '            If oDatos.Seguridad_Permisos_Estructura_ABC(IdUsuario, GridC_2.DataSource, Msj) Then
                            '                Call Botones(tag)
                            '                Call Regresar()
                            '            Else
                            '                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '            End If
                            '        Else
                            '            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '        End If
                            '    End If
                            'Else
                            '    MessageBox.Show("Verifique el password no coincide.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            'End If
                        Case Globales.TipoABC.Eliminar
                            'If MessageBox.Show("¿Desea Desactivar al usuario?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            '    IdUsuario = Globales.oAmbientes.Valor(txt_IdUsuario.Text)
                            '    If oDatos.Seguridad_Usuarios_ABC("B", IdUsuario, txt_Usuario.Text, txt_Nombre.Text, txt_pass.Text, cbo_Perfiles.SelectedValue, CDate(txt_fecha_alta.Text), dtx_fecha_vence.DateTime, 0, Msj) Then
                            '        Call Botones(tag)
                            '        Call Regresar()
                            '    Else
                            '        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '    End If
                            'End If
                        Case Globales.TipoABC.Modificar
                            'If MessageBox.Show("¿Desea modificar la informacion del usuario?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            '    IdUsuario = Globales.oAmbientes.Valor(txt_IdUsuario.Text)
                            '    If oDatos.Seguridad_Usuarios_ABC("C", IdUsuario, txt_Usuario.Text, txt_Nombre.Text, txt_pass.Text, cbo_Perfiles.SelectedValue, CDate(txt_fecha_alta.Text), dtx_fecha_vence.DateTime, 0, Msj) Then
                            '        If oDatos.Seguridad_Permisos_Estructura_ABC(IdUsuario, GridC_2.DataSource, Msj) Then
                            '            Call Botones(tag)
                            '            Call Regresar()
                            '        Else
                            '            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '        End If
                            '    Else
                            '        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '    End If
                            'End If
                        Case Globales.TipoABC.Consulta
                            Call Botones(tag)
                            Call Regresar()
                    End Select
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub Mostrar_Detalle(ByVal Modo As Globales.TipoABC)
        Dim iRen As Integer
        Dim oDatos_Seguridad As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim IdUsuario As Integer = 0
        Try
            oDatos_Seguridad = New Datos_Viscoi
            iRen = GridV_1.FocusedRowHandle

            If GridV_1.RowCount > 0 Then
                TabPane1.SelectedPage = tnpPage2
                tnpPage1.Enabled = False
                tnpPage2.Enabled = True

                'txt_IdUsuario.ReadOnly = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, False, True)
                'txt_Usuario.ReadOnly = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, False, True)
                'txt_Nombre.ReadOnly = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, False, True)
                'txt_fecha_alta.ReadOnly = True 'IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, False, True)
                'dtx_fecha_vence.ReadOnly = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, False, True)
                'cbo_Perfiles.Enabled = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, True, False)
                'chk_activo.Enabled = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, True, False)

                'btn_CambiaPass.Visible = IIf(Modo = Globales.TipoABC.Modificar, True, False)
                'txt_pass.Visible = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, True, False)
                'txt_pass_confirma.Visible = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, True, False)
                'lbl_04.Visible = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, True, False)
                'lbl_05.Visible = IIf(Modo = Globales.TipoABC.Nuevo Or Modo = Globales.TipoABC.Modificar, True, False)

                'txt_pass.Text = ""
                'txt_pass_confirma.Text = ""

                'If Modo = Globales.TipoABC.Nuevo Then
                '    txt_IdUsuario.Text = "0"
                '    txt_Usuario.Text = ""
                '    txt_Nombre.Text = ""
                '    cbo_Perfiles.SelectedIndex = -1
                '    txt_fecha_alta.Text = Format(Now(), "dd/MMM/yyyy")
                '    'dtx_fecha_vence.Text = GridV_1.GetRowCellValue(iRen, col_FechaVence)
                '    dtx_fecha_vence.DateTime = Now().AddYears(1)
                '    chk_activo.Checked = True
                'Else
                '    txt_IdUsuario.Text = GridV_1.GetRowCellValue(iRen, col_IdUsuario)
                '    txt_Usuario.Text = GridV_1.GetRowCellValue(iRen, col_Usuario)
                '    txt_Nombre.Text = GridV_1.GetRowCellValue(iRen, col_Nombre)
                '    cbo_Perfiles.SelectedValue = GridV_1.GetRowCellValue(iRen, col_IdPerfil)
                '    txt_fecha_alta.Text = Format(CDate(GridV_1.GetRowCellValue(iRen, col_FechaAlta)), "dd/MMM/yyyy")
                '    dtx_fecha_vence.DateTime = CDate(GridV_1.GetRowCellValue(iRen, col_FechaVence))
                '    chk_activo.Checked = IIf(GridV_1.GetRowCellValue(iRen, col_Sw_Activo) = 1, True, False)
                'End If
                'IdUsuario = Globales.oAmbientes.Valor(txt_IdUsuario.Text)
                'If oDatos_Seguridad.Seguridad_Permisos_Estructura_Sel(IdUsuario, dtDatos, Mensaje) Then
                '    GridC_2.DataSource = dtDatos
                'Else
                '    GridC_2.DataSource = Nothing
                'End If
                'GridV_2.BestFitColumns()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos_Seguridad = Nothing
    End Sub
    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
        Call ControlBotones("Consulta")
    End Sub
End Class