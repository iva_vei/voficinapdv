﻿Imports System.Threading
Imports System.Globalization
Imports DevExpress.XtraTabbedMdi

Public Class Principal
    Private trd1 As System.Threading.Thread
    Private trd2 As System.Threading.Thread
    Dim SW_Carga As Boolean
    Dim blnVersion As Boolean
    Dim EjecutoAct As Boolean
    Dim sTxt_Ocultar As String = "«" & vbNewLine & vbNewLine & "O" & vbNewLine & "c" & vbNewLine & "u" & vbNewLine & "l" & vbNewLine & "t" & vbNewLine & "a" & vbNewLine & "r" & vbNewLine & vbNewLine & "A" & vbNewLine & "c" & vbNewLine & "c" & vbNewLine & "i" & vbNewLine & "o" & vbNewLine & "n" & vbNewLine & "e" & vbNewLine & "s"
    Dim sTxt_Mostrar As String = "»" & vbNewLine & vbNewLine & "M" & vbNewLine & "o" & vbNewLine & "s" & vbNewLine & "t" & vbNewLine & "r" & vbNewLine & "a" & vbNewLine & "r" & vbNewLine & vbNewLine & "A" & vbNewLine & "c" & vbNewLine & "c" & vbNewLine & "i" & vbNewLine & "o" & vbNewLine & "n" & vbNewLine & "e" & vbNewLine & "s"
    Dim TecladoAtajos As New List(Of clsTecladoAtajos)
    Dim segundos As Integer
    Dim PopTiempoEspera As Integer
    Dim mostrar As Boolean

    Private Function IconoPestana(vImagen As Bitmap) As Bitmap
        Dim Imagen As System.Drawing.Bitmap
        Imagen = New Bitmap(vImagen, New Size(16, 16))
        Return Imagen.Clone()
    End Function
    Private Function IconoMenu(vImagen As Bitmap) As Bitmap
        Dim Imagen As System.Drawing.Bitmap
        Imagen = New Bitmap(vImagen, New Size(32, 32))
        Return Imagen.Clone()
    End Function
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""
        Me.Hide()
        Dim aplicacioncorriendo As Process() = Process.GetProcessesByName("proyVOficina_PDV")

        If aplicacioncorriendo.Length > 1 Then
            MessageBox.Show("La aplciacion VOficina_PDV ya se esta ejecutando.")
            End
        End If
        'btn_OcultarMostrarMenu.Text = sTxt_Ocultar

        Dim fNewsFeed As NewsFeed
        fNewsFeed = New NewsFeed

        Dim fLogin As frmLogin
        fLogin = New frmLogin
        fLogin.ShowDialog(Me)
        fLogin = Nothing
        ''Globales.oAmbientes.SW_Version = True
        If ActualizaVersion() Then
            Call ActualizaExe()
        End If

        If Globales.oAmbientes.SW_Version Then
            Dim startInfo As New ProcessStartInfo
            Dim Process1() As Process
            Dim RutaSincro = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "RutaSincro", "")
            Process1 = Process.GetProcessesByName("VOficinaSync")

            If RutaSincro <> "" And Process1.Length = 0 Then
                startInfo.FileName = RutaSincro & "\VOficinaSync.EXE"
                startInfo.Arguments = """" & RutaSincro & """"
                Process.Start(startInfo)
            End If

            If Not Globales.oAmbientes.oUsuario Is Nothing Then
                Dim DatosConexion() As String
                Dim sQuerys As String = ""
                If IO.File.Exists(Application.StartupPath & "\SQLQuery.sql") Then
                    DatosConexion = System.IO.File.ReadAllLines(Application.StartupPath & "\SQLQuery.sql")
                    sQuerys = ""
                    For Each sRen As String In DatosConexion
                        If sRen = "#EJECUTAR#" Or sRen = "#EJECUTAR_CONTROL#" Then
                            oDatos = New Datos_Viscoi
                            If Not oDatos.SQLQuery(sQuerys, sRen, Msj) Then
                                MessageBox.Show("Se encontro un problema al actualizar SQLQuery" & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                            sQuerys = ""
                        Else
                            sQuerys = sQuerys & vbNewLine & sRen
                        End If
                    Next
                    IO.File.Delete(Application.StartupPath & "\SQLQuery.sql")
                End If

                Me.Show()
                lbl_Usuario1.Text = Globales.oAmbientes.oUsuario.Id_usuario
                lbl_Perfil.Text = Globales.oAmbientes.oUsuario.Perfil
                lbl_Ambiente.Text = IIf(Globales.oAmbientes.SW_Produccion, "PRODUCCION", "PRUEBAS")
                lbl_Version.Text = "Version: " & Globales.oAmbientes.Version
                lbl_Servidor.Text = "Servidor: " & Globales.oAmbientes.Servidor
                lbl_Empresa.Text = "Empresa: " & Globales.oAmbientes.Empresa
                lbl_BD.Text = "BD: " & Globales.oAmbientes.BaseDatos
                'RibbonControl1.Minimized = True

                fNewsFeed.Path = Application.ExecutablePath.Replace("proyVOficina_PDV.exe", "")
                fNewsFeed.Archivo = "News_" & Globales.oAmbientes.Version.Replace(".", "_") & ".pdf"
                If My.Computer.FileSystem.FileExists(fNewsFeed.Path & fNewsFeed.Archivo) _
                And My.Computer.Registry.GetValue("HKEY_CURRENT_USER\ViscoiWMS\" & Globales.oAmbientes.Sistema, fNewsFeed.Archivo, "NO") = "NO" Then
                    fNewsFeed.ShowDialog(Me)
                    My.Computer.Registry.SetValue("HKEY_CURRENT_USER\ViscoiWMS\" & Globales.oAmbientes.Sistema, fNewsFeed.Archivo, "SI")
                End If

                'nbg1_Compras.Expanded = False
                'nbg1_Cedis.Expanded = False
                'nbg1_Mercancia.Expanded = False
                'nbg1_Catalogos.Expanded = False
            Else
                Me.Close()
            End If

            'Call Refrescar()

            Call LeerArchivoAtajos()

            GridC_1.Visible = False
            Popup.Visible = False
            ' Configura el temporizador para que se ejecute cada segundo (1000 milisegundos)
            Timer1.Interval = 1000
            ' Inicia el temporizador
            If Globales.oAmbientes.oUsuario.ValidaPermiso("pdvContaVales", "ADMINISTRARDATOS") Then
                Timer1.Enabled = True
            Else
                Timer1.Enabled = False
            End If
        Else
            MessageBox.Show("Porfavor, revisa la version " & Globales.oAmbientes.Version & " No es valida revisa que se descargue la version correcta.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End If
    End Sub
    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Dim Combinacion As String
        Dim Accion As String

        Try

            Select Case e.KeyCode
                ''''Case System.Windows.Forms.Keys.Escape
                ''''    Regresar(Me.ActiveMdiChild)
                ''Case System.Windows.Forms.Keys.F1
                ''    Funcion01(Me.ActiveMdiChild)
                ''Case System.Windows.Forms.Keys.F2
                ''    Funcion02(Me.ActiveMdiChild)
                ''Case System.Windows.Forms.Keys.F3
                ''    Funcion03(Me.ActiveMdiChild)
                ''Case System.Windows.Forms.Keys.F4
                ''    Funcion04(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.F5
                    Refrescar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.F6
                    Imprimir(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.F7
                    ExportaXLS(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.F8
                    Funcion08(Me.ActiveMdiChild)
                    ''Case System.Windows.Forms.Keys.F9
                    ''    Funcion09(Me.ActiveMdiChild)
                    ''Case System.Windows.Forms.Keys.F10
                    ''    Funcion10(Me.ActiveMdiChild)
                    ''Case System.Windows.Forms.Keys.F11
                    ''    Funcion11(Me.ActiveMdiChild)
                    ''Case System.Windows.Forms.Keys.F12
                    ''    Funcion12(Me.ActiveMdiChild)
                Case Else

            End Select

            Combinacion = IIf(e.Control, "CTRL+", "") _
                        & IIf(e.Alt, "ALT+", "") _
                        & IIf(e.Shift, "SHIFT+", "") _
                        & IIf(e.KeyCode, e.KeyCode.ToString, "")

            Accion = ""
            For Each oRen As clsTecladoAtajos In TecladoAtajos
                If oRen.Combinacion = Combinacion Then
                    Accion = oRen.Accion
                End If
            Next

            Select Case Accion
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case "AYUDA"
                    PVTA_Ayuda(Me.ActiveMdiChild)
                Case "NUEVA VENTA"
                    PVTA_NuevaVenta(Me.ActiveMdiChild)
                Case "LECTURA"
                    PVTA_Lectura(Me.ActiveMdiChild)
                Case "ELIMINAR PARTIDA"
                    PVTA_EliminarPartida(Me.ActiveMdiChild)
                Case "LISTA PRECIOS"
                    PVTA_LSTPRECIOS(Me.ActiveMdiChild)
                Case "DESCTO GLOBAL"
                    PVTA_Descto_Global(Me.ActiveMdiChild)
                Case "DESCTO PARTIDA"
                    PVTA_Descto_Partida(Me.ActiveMdiChild)
                Case "PRECIODIRECTO PARTIDA"
                    PVTA_PrecioDirecto_Partida(Me.ActiveMdiChild)
                Case "PAGAR VENTA"
                    PVTA_PagarVenta(Me.ActiveMdiChild)
                Case "BUSCAR ARTICULO"
                    PVTA_BuscarArticulo(Me.ActiveMdiChild)
                Case "TERMINAR VENTA"
                    PVTA_TerminarVenta(Me.ActiveMdiChild)
                Case "FACTURAR"
                    PVTA_Facturar(Me.ActiveMdiChild)
                Case "ENTREGAS"
                    PVTA_Entregas(Me.ActiveMdiChild)
                Case "CLIENTES"
                    PVTA_Clientes(Me.ActiveMdiChild)
                Case "VENTA MOSTRADOR"
                    PVTA_VTAMostrador(Me.ActiveMdiChild)
                Case "COTIZACION"
                    PVTA_VTACotizacion(Me.ActiveMdiChild)
                Case "DATOS GENERALES"
                    PVTA_DatosGenerales(Me.ActiveMdiChild)
                Case "FRACCIONAR"
                    PVTA_Fraccionar(Me.ActiveMdiChild)
                Case "NUEVO CLIENTE"
                    PVTA_NvoCliente(Me.ActiveMdiChild)
                Case "MODIFICA CLIENTE"
                    PVTA_ModCliente(Me.ActiveMdiChild)
                Case Else

            End Select
            ''lbl_Teclas.Text = IIf(e.Control, "CTRL ", "") _
            ''                & IIf(e.Alt, "ALT ", "") _
            ''                & IIf(e.Shift, "SHIFT ", "") _
            ''                & IIf(e.KeyCode, e.KeyCode.ToString, "")
        Catch ex As Exception
            'MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    'Private Sub BBtn_Regresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BBtn_Regresar.ItemClick
    '    Regresar(Me.ActiveMdiChild)
    'End Sub
    Private Sub Regresar(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Regresar()
        Catch e As Exception
            MessageBox.Show("Opción Regresar no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    'Private Sub BBtn_Refrescar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BBtn_Refrescar.ItemClick
    '    Refrescar(Me.ActiveMdiChild)
    'End Sub
    Private Sub Refrescar(ByRef oForma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oForma.Refrescar()
        Catch e As Exception
            MessageBox.Show("La opción Refrescar no está disponible para esta pantalla.", "Refrescar ... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Cursor = Cursors.Default
    End Sub
    'Private Sub BBtn_Imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BBtn_Imprimir.ItemClick
    '    Imprimir(Me.ActiveMdiChild)
    'End Sub
    Private Sub Imprimir(ByRef oForma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            'If Globales.oAmbientes.oUsuario.ValidaPermiso(oForma.Name, "LEERDATOS") Then
            oForma.Imprimir()
            'Else
            '    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_sinacceso(oForma.Name, "IMPRIMIR"), Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'End If
        Catch e As Exception
            MessageBox.Show("Reporte no disponible para esta pantalla.", "Imprimir... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    'Private Sub BBtn_Excel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BBtn_Excel.ItemClick
    '    ExportaXLS(Me.ActiveMdiChild)
    'End Sub
    Private Sub ExportaXLS(ByRef oForma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            'If Globales.oAmbientes.oUsuario.ValidaPermiso(oForma.Name, "LEERDATOS") Then
            Dim PathExcel As String
                PathExcel = ""

                oForma.Excel()

            'Else
            '    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_sinacceso(oForma.Name, "EXCEL"), Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'End If
        Catch e As Exception
            MessageBox.Show("Exportar a Excel no disponible para esta pantalla.", "Exportar a... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion01(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion01()
        Catch e As Exception
            'MessageBox.Show("Opción F1 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion02(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion02()
        Catch e As Exception
            'MessageBox.Show("Opción F2 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion03(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion03()
        Catch e As Exception
            'MessageBox.Show("Opción F3 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion04(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion04()
        Catch e As Exception
            'MessageBox.Show("Opción F4 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion08(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion08()
        Catch e As Exception
            'MessageBox.Show("Opción F8 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion09(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion09()
        Catch e As Exception
            'MessageBox.Show("Opción F9 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion10(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion10()
        Catch e As Exception
            'MessageBox.Show("Opción F10 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion11(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion11()
        Catch e As Exception
            'MessageBox.Show("Opción F11 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Funcion12(ByRef oforma As Object)
        Me.Cursor = Cursors.WaitCursor
        Try
            oforma.Funcion12()
        Catch e As Exception
            'MessageBox.Show("Opción F12 no está disponible para esta pantalla.", "Regresar... " & Globales.oAmbientes.Sistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Public Sub PVTA_Ayuda(ByRef oforma As Object)
        Try
            oforma.PVTA_Ayuda()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_NuevaVenta(ByRef oforma As Object)
        Try
            oforma.PVTA_NuevaVenta()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Lectura(ByRef oforma As Object)
        Try
            oforma.PVTA_Lectura()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_EliminarPartida(ByRef oforma As Object)
        Try
            oforma.PVTA_EliminarPartida()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_LSTPRECIOS(ByRef oforma As Object)
        Try
            oforma.PVTA_LST_PRECIO()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub
    Public Sub PVTA_Descto_Global(ByRef oforma As Object)
        Try
            oforma.PVTA_Descto_Global()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Descto_Partida(ByRef oforma As Object)
        Try
            oforma.PVTA_Descto_Partida()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_PrecioDirecto_Partida(ByRef oforma As Object)
        Try
            oforma.PVTA_PrecioDirecto_Partida()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_PagarVenta(ByRef oforma As Object)
        Try
            oforma.PVTA_PagarVenta()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_PagarDineroElec(ByRef oforma As Object)
        Try
            oforma.PVTA_PagarDineroElec()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_BuscarArticulo(ByRef oforma As Object)
        Try
            oforma.PVTA_BuscarArticulo()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_TerminarVenta(ByRef oforma As Object)
        Try
            oforma.PVTA_TerminarVenta()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Facturar(ByRef oforma As Object)
        Try
            oforma.PVTA_EliminarPartida()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Entregas(ByRef oforma As Object)
        Try
            oforma.PVTA_Entregas()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_VTAMostrador(ByRef oforma As Object)
        Try
            oforma.PVTA_VTAMostrador()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_VTACotizacion(ByRef oforma As Object)
        Try
            oforma.PVTA_VTACotizacion()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Clientes(ByRef oforma As Object)
        Try
            oforma.PVTA_Clientes()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_DatosGenerales(ByRef oforma As Object)
        Try
            oforma.PVTA_DatosGenerales()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Fraccionar(ByRef oforma As Object)
        Try
            oforma.PVTA_Fraccionar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_NvoCliente(ByRef oforma As Object)
        Try
            oforma.PVTA_NvoCliente()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_ModCliente(ByRef oforma As Object)
        Try
            oforma.PVTA_ModCliente()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_AgregaTransacciones(ByRef oforma As Object)
        Try
            oforma.PVTA_AgregaTransacciones()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Carga_Catalogos_Generales()
        'Dim Glb_cnnCONTROL As System.Data.SqlClient.SqlConnection
        'Glb_cnnCONTROL = New System.Data.SqlClient.SqlConnection(Entidades.CN_Generales.CnControlNet)
        Try
            'Entidades.CN_Globales.Usuario.Per_Rubros_SEL(Glb_cnnCONTROL)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub
    Private Sub GeneraCarpetas()
        'Dependecias 
        If Not My.Computer.FileSystem.DirectoryExists("C:\ViscoiWMS") Then
            My.Computer.FileSystem.CreateDirectory("C:\ViscoiWMS")
        End If
        If Not My.Computer.FileSystem.DirectoryExists("C:\ViscoiWMS\Temporal") Then
            My.Computer.FileSystem.CreateDirectory("C:\ViscoiWMS\Temporal")
        End If
    End Sub


    Private Sub Principal_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Try
            If Globales.oAmbientes.FechaEjecucion <> Today Then
                Me.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub XtraTabbedMdiManager1_PageAdded(sender As Object, e As MdiTabPageEventArgs) Handles XtraTabbedMdiManager1.PageAdded
        Try
            If XtraTabbedMdiManager1.Pages.Count > 0 Then
                pic_Logo.Visible = False
            Else
                pic_Logo.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub XtraTabbedMdiManager1_PageRemoved(sender As Object, e As MdiTabPageEventArgs) Handles XtraTabbedMdiManager1.PageRemoved
        Try
            If XtraTabbedMdiManager1.Pages.Count > 0 Then
                pic_Logo.Visible = False
            Else
                pic_Logo.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub MercanciaSurtidaSinEmpaqueToolStripMenuItem_Click(sender As Object, e As EventArgs)
        ''Dim frm As New wmsReporteMciaSurtidaSinEmpaque
        ''If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        ''    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        ''    Return
        ''End If
        ''
        ''frm.MdiParent = Me
        ''frm.Show()
    End Sub

    Private Sub tsm_Cajas_Click(sender As Object, e As EventArgs) Handles tsm_Cajas.Click
        Dim frm As New pdvCajas
        'If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Return
        'End If

        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub tsm_Cajeros_Click(sender As Object, e As EventArgs) Handles tsm_Cajeros.Click
        Dim frm As New pdvCajeros
        'If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Return
        'End If

        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub tsm_PuntoVenta_Click(sender As Object, e As EventArgs) Handles tsm_PuntoVenta.Click
        Dim frm As New pdvPuntoVenta(eTipoTran.VENTA)
        'If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Return
        'End If

        frm.MdiParent = Me
        frm.TecladoAtajos = TecladoAtajos
        frm.Show()
    End Sub
    Private Sub tsm_Mostrador_Click(sender As Object, e As EventArgs) Handles MostradorToolStripMenuItem.Click
        Dim frm As New pdvPuntoVenta(eTipoTran.MOSTRADOR)
        'If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Return
        'End If

        frm.MdiParent = Me
        frm.TecladoAtajos = TecladoAtajos
        frm.Show()
    End Sub
    Private Sub tsm_Cotizacion_Click(sender As Object, e As EventArgs) Handles CotizacionesToolStripMenuItem.Click
        Dim frm As New pdvPuntoVenta(eTipoTran.COTIZACION)
        'If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Return
        'End If

        frm.MdiParent = Me
        frm.TecladoAtajos = TecladoAtajos
        frm.Show()
    End Sub
    Private Sub LeerArchivoAtajos()
        Dim DatosConexion() As String
        Dim ta As clsTecladoAtajos
        Try
            DatosConexion = System.IO.File.ReadAllLines(Application.StartupPath & "\TeclasAsignadas.txt")
            TecladoAtajos.Clear()

            For Each Renglon As String In DatosConexion
                ta = New clsTecladoAtajos
                ta.Accion = Renglon.Split("|")(0)
                ta.Combinacion = Renglon.Split("|")(1)
                If ta.Accion.ToUpper = "POPTIEMPOESPERA" Then
                    Try
                        PopTiempoEspera = CInt(ta.Combinacion)
                    Catch ex As Exception
                        PopTiempoEspera = 180
                    End Try
                Else
                    TecladoAtajos.Add(ta)
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Function ActualizaVersion() As Boolean
        Dim Sw_regresa As Boolean = False
        Dim DatosConexion() As String
        Dim RutaDrobox As String = ""
        Try
            RutaDrobox = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "RutaDrobox", "")
            If RutaDrobox <> "" Then
                If IO.File.Exists(RutaDrobox & "\VersionPDV.txt") Then
                    DatosConexion = System.IO.File.ReadAllLines(RutaDrobox & "\VersionPDV.txt")
                    If Globales.oAmbientes.Version <> DatosConexion(0) Then
                        Sw_regresa = True
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Return Sw_regresa
    End Function
    Private Sub ActualizaExe()
        Dim RutaDrobox As String = ""
        Dim RutaDestino As String = ""

        Dim startInfo As New ProcessStartInfo
        ' Run calculator.
        Try
            RutaDrobox = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VoficinaPDV\", "RutaDrobox", "")
            startInfo.FileName = RutaDrobox & "\ActualizaVersion.EXE"
            startInfo.Arguments = """" & RutaDrobox & """ """ & Application.StartupPath & """ """ & Application.ExecutablePath & """"
            Process.Start(startInfo)

            Application.Exit()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Class clsTecladoAtajos
        Private _accion As String
        Private _combinacion As String

        Public Property Accion As String
            Get
                Return _accion
            End Get
            Set(value As String)
                _accion = value
            End Set
        End Property

        Public Property Combinacion As String
            Get
                Return _combinacion
            End Get
            Set(value As String)
                _combinacion = value
            End Set
        End Property
    End Class

    Private Sub tsm_ParametrosControl_Click(sender As Object, e As EventArgs) Handles tsm_ParametrosControl.Click
        Dim oFrm As pdvConfigActualizaVersion
        Try
            oFrm = pdvConfigActualizaVersion
            oFrm.ShowDialog()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub RevisarExistenciasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RevisarExistenciasToolStripMenuItem.Click
        Dim frm As New SelExistencia
        'If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Return
        'End If

        frm.MdiParent = Me
        frm.Show()
    End Sub
    Private Sub FacturarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturarToolStripMenuItem.Click
        Dim frm1 As pdvFactura
        Dim frm2 As pdvFactura40

        Dim oDatos = New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_SatInfo(Globales.oAmbientes.Id_Empresa _
                                               , dtDatos _
                                               , Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("EmisorVersion") = "3.3" Then
                        frm1 = New pdvFactura
                        frm1.ShowDialog()
                    Else
                        frm2 = New pdvFactura40
                        frm2.ShowDialog()
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "No se puede determinar version de CFDI", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub ReimprimirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReimprimirToolStripMenuItem.Click, ReimprimirPagosToolStripMenuItem.Click
        Dim frm As pdvTicketReimprimir
        Try
            frm = New pdvTicketReimprimir
            frm.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub AbrirCajaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirCajaToolStripMenuItem.Click
        Dim oDatos As New Datos_Viscoi
        Dim msj As String = ""
        Dim DatosConexion() As String

        Dim Caja As String = ""
        Try
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    Caja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            ''txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal
            If Caja <> "" Then
                If MessageBox.Show("¿Desea aperturar caja(" & Caja & ")?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = DialogResult.Yes Then

                    If oDatos.PVTA_Actualiza_Caja_Abrir(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, Caja, msj) Then
                        MessageBox.Show("Caja (" & Caja & ") Aperturada", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    Else
                        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ReimprimirCorteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReimprimirCorteToolStripMenuItem.Click
        Dim frm As pdvCorteReimprimir
        Try
            frm = New pdvCorteReimprimir
            frm.MdiParent = Me
            frm.Show()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub DevolucionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DevolucionToolStripMenuItem.Click
        'Dim frm As pdvTicketCancelacion
        'Try
        '    frm = New pdvTicketCancelacion
        '    frm.TipoTran = eTipoTranCanc.CANCELADO
        '    frm.MdiParent = Me
        '    frm.Show()

        'Catch ex As Exception
        '    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        'End Try


        Dim frm1 As pdvTicketCancelacion
        Dim frm2 As pdvTicketCancelacion40

        Dim oDatos = New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_SatInfo(Globales.oAmbientes.Id_Empresa _
                                               , dtDatos _
                                               , Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("EmisorVersion") = "3.3" Then
                        frm1 = New pdvTicketCancelacion
                        frm1.TipoTran = eTipoTranCanc.DEVUELTO
                        frm1.MdiParent = Me
                        frm1.Show()
                    Else
                        frm2 = New pdvTicketCancelacion40
                        frm2.TipoTran = eTipoTranCanc.DEVUELTO
                        frm2.MdiParent = Me
                        frm2.Show()
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "No se puede determinar version de CFDI", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub CancelaciónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CancelaciónToolStripMenuItem.Click
        'Dim frm As pdvTicketCancelacion
        'Try
        '    frm = New pdvTicketCancelacion
        '    frm.TipoTran = eTipoTranCanc.CANCELADO
        '    frm.MdiParent = Me
        '    frm.Show()

        'Catch ex As Exception
        '    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        'End Try


        Dim frm1 As pdvTicketCancelacion
        Dim frm2 As pdvTicketCancelacion40

        Dim oDatos = New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_SatInfo(Globales.oAmbientes.Id_Empresa _
                                               , dtDatos _
                                               , Msj) Then
                If dtDatos.Rows.Count > 0 Then


                    If dtDatos.Rows(0).Item("EmisorVersion") = "3.3" Then
                        frm1 = New pdvTicketCancelacion
                        If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm1.Name, "ADMINISTRARDATOS") Then
                                MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm1.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Return
                            End If
                        End If
                        frm1.TipoTran = eTipoTranCanc.CANCELADO
                        frm1.MdiParent = Me
                        frm1.Show()
                    Else
                        frm2 = New pdvTicketCancelacion40
                        If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm2.Name, "ADMINISTRARDATOS") Then
                                MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm2.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Return
                            End If
                        End If
                        frm2.TipoTran = eTipoTranCanc.CANCELADO
                        frm2.MdiParent = Me
                        frm2.Show()
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "No se puede determinar version de CFDI", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub ArticulosNegadosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ArticulosNegadosToolStripMenuItem.Click
        Dim frm As pdvNegadosExistencia
        Try
            frm = New pdvNegadosExistencia
            frm.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ModificarClienteEventualToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarClienteEventualToolStripMenuItem.Click
        Dim frm As pdvCuentaEventual
        Dim DatosConexion() As String

        Dim Caja As String = ""
        Try
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    Caja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            frm = New pdvCuentaEventual
            frm.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub RefacturarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefacturarToolStripMenuItem.Click

        Dim frm1 As pdvTicketReFactura
        Dim frm2 As pdvReFactura40

        Dim oDatos = New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_SatInfo(Globales.oAmbientes.Id_Empresa _
                                               , dtDatos _
                                               , Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("EmisorVersion") = "3.3" Then
                        frm1 = New pdvTicketReFactura
                        frm1.MdiParent = Me
                        frm1.Show()
                    Else
                        frm2 = New pdvReFactura40
                        frm2.MdiParent = Me
                        frm2.Show()
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "No se puede determinar version de CFDI", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub FacturaGlobalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturaGlobalToolStripMenuItem.Click

        Dim frm1 As pdvFacturaGlobal
        Dim frm2 As pdvFacturaGlobal40

        Dim oDatos = New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_SatInfo(Globales.oAmbientes.Id_Empresa _
                                               , dtDatos _
                                               , Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("EmisorVersion") = "3.3" Then
                        frm1 = New pdvFacturaGlobal
                        frm1.ShowDialog()
                    Else
                        frm2 = New pdvFacturaGlobal40
                        frm2.ShowDialog()
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "No se puede determinar version de CFDI", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub VouchersToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VouchersToolStripMenuItem.Click
        Try
            Dim frm As pdvVouchers
            Try
                frm = New pdvVouchers
                frm.Sw_pendientes = False
                frm.ShowDialog()
            Catch ex As Exception
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub CanjeValesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CanjeValesToolStripMenuItem.Click
        Dim frm As pdvCanjes
        Try
            frm = New pdvCanjes
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            frm.TipoTran = eTipoTran.VALES
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub PrestamoClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrestamoClientesToolStripMenuItem.Click
        Dim frm As pdvCanjes
        Try
            frm = New pdvCanjes
            If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            frm.TipoTran = eTipoTran.PRESTAMOS
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub RecibirPagosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RecibirPagosToolStripMenuItem.Click
        Dim frm As pdvRecibirPagos
        Try
            frm = New pdvRecibirPagos
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub EstadoDeCuentaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EstadoDeCuentaToolStripMenuItem.Click
        Dim frm As pdvCliEdoCuenta
        Try
            frm = New pdvCliEdoCuenta
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End Try

    End Sub

    Private Sub CobranzaDiariaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CobranzaDiariaToolStripMenuItem.Click
        Dim frm As pdvPagosAplicados
        Try
            frm = New pdvPagosAplicados
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End Try
    End Sub

    Private Sub ReimprimirToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ReimprimirToolStripMenuItem1.Click
        Dim frm As pdvTicketReimprimirVale
        Try
            frm = New pdvTicketReimprimirVale
            frm.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub RecibirPagosClienteFinalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RecibirPagosClienteFinalToolStripMenuItem.Click
        Dim frm As pdvRecibirPagos_ClienteFinal
        Try
            frm = New pdvRecibirPagos_ClienteFinal
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub ReestructurasRelaciónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReestructurasRelaciónToolStripMenuItem.Click
        Dim frm As pdvReestructuras_Relacion
        Try
            frm = New pdvReestructuras_Relacion
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ReestructurasClienteFinalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReestructurasClienteFinalToolStripMenuItem.Click
        Dim frm As pdvReestructuras_ClienteFinal
        Try
            frm = New pdvReestructuras_ClienteFinal
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ConveniosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConveniosToolStripMenuItem.Click
        Dim frm As pdvConvenios
        Try
            frm = New pdvConvenios
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub CorteDeCajaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CorteDeCajaToolStripMenuItem.Click
        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Dim PDatCaja As String = ""
        Dim PDatCajero As String = ""
        Dim msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim oDatos As Datos_Viscoi = Nothing
        Dim pd As New Printing.PrintDocument
        Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName

        Dim frm As pdvEntregas
        Try
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    PDatCaja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            If PDatSucursal <> "" Then
                oDatos = New Datos_Viscoi
                If oDatos.PVTA_Recupera_Cajeros(Globales.oAmbientes.Id_Empresa, PDatSucursal, PDatCaja, "", Globales.oAmbientes.oUsuario.Id_usuario, dtDatos, msj) Then
                    If dtDatos.Rows.Count > 0 Then
                        PDatCajero = dtDatos.Rows(0).Item("cajero")
                    Else
                        MessageBox.Show("Usuario no es cajero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Me.Close()
                    End If
                Else
                    MessageBox.Show("Archivo PuntoVenta no configurado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                frm = New pdvEntregas

                frm.IdEmpresa = Globales.oAmbientes.Id_Empresa
                frm.IdSucursal = PDatSucursal
                frm.txt_Caja.Text = PDatCaja
                frm.txt_Cajero.Text = PDatCajero
                frm.txt_Corte.Text = ""
                frm.Impresora = s_Default_Printer
                frm.TipoOperacion = "CORTE"

                If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                        MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If
                End If
                ''frm.MdiParent = Me
                frm.ShowDialog()
            Else
                MessageBox.Show("Archivo PuntoVenta no configurado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub TraspasoDeDineroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TraspasoDeDineroToolStripMenuItem.Click, ToolStripMenuItem4.Click
        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Dim PDatCaja As String = ""
        Dim PDatCajero As String = ""
        Dim msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim oDatos As Datos_Viscoi = Nothing
        Dim pd As New Printing.PrintDocument
        Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName

        Dim frm As pdvTraspasoDinero
        Try

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    PDatCaja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            If PDatSucursal <> "" Then
                oDatos = New Datos_Viscoi
                If oDatos.PVTA_Recupera_Cajeros(Globales.oAmbientes.Id_Empresa, PDatSucursal, PDatCaja, "", Globales.oAmbientes.oUsuario.Id_usuario, dtDatos, msj) Then
                    If dtDatos.Rows.Count > 0 Then
                        PDatCajero = dtDatos.Rows(0).Item("cajero")
                    Else
                        MessageBox.Show("Usuario no es cajero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Me.Close()
                    End If
                Else
                    MessageBox.Show("Archivo PuntoVenta no configurado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                frm = New pdvTraspasoDinero

                frm.IdEmpresa = Globales.oAmbientes.Id_Empresa
                frm.IdSucursal = PDatSucursal
                frm.txt_Caja.Text = PDatCaja
                frm.txt_Cajero.Text = PDatCajero
                frm.txt_Corte.Text = ""
                frm.Impresora = s_Default_Printer
                frm.RG_Cajas.Enabled = False
                If sender.text = ToolStripMenuItem4.Text Then
                    frm.TipoOperacion = "CAJA"
                Else
                    frm.TipoOperacion = "BOVEDA"
                End If

                If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                        MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If
                End If
                ''frm.MdiParent = Me
                frm.ShowDialog()
            Else
                MessageBox.Show("Archivo PuntoVenta no configurado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub LiquidaciónesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LiquidaciónesToolStripMenuItem.Click

        Dim frm As pdvLiquidacion
        Try
            frm = New pdvLiquidacion
            If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
                If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
                    MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If
            End If
            frm.TecladoAtajos = TecladoAtajos
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Base2x3ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Base2x3ToolStripMenuItem.Click
        Dim frm As New SelArticulos
        'If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
        '    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '        MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        Return
        '    End If
        'End If
        frm.Version = "AA-6X6"
        frm.Sw_Impresion = True
        frm.Sw_Seleccionar = True
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub N3x4ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles N3x4ToolStripMenuItem.Click
        Dim frm As New SelArticulos
        'If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
        '    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '        MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        Return
        '    End If
        'End If
        frm.Version = "NA-3X4"
        frm.Sw_Impresion = True
        frm.Sw_Seleccionar = True
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub N3x4BToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles N3x4BToolStripMenuItem.Click
        Dim frm As New SelArticulos
        'If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
        '    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '        MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        Return
        '    End If
        'End If
        frm.Version = "NB-3x4"
        frm.Sw_Impresion = True
        frm.Sw_Seleccionar = True
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub N4x9ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles N4x9ToolStripMenuItem.Click
        Dim frm As New SelArticulos
        'If (Globales.oAmbientes.Id_Empresa <> "PAPELERA") Then
        '    If Not Globales.oAmbientes.oUsuario.ValidaPermiso(frm.Name, "ADMINISTRARDATOS") Then
        '        MessageBox.Show("El usuario no cuenta con acceso a la opción " & frm.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        Return
        '    End If
        'End If
        frm.Version = "NA-4X9"
        frm.Sw_Impresion = True
        frm.Sw_Seleccionar = True
        frm.MdiParent = Me
        frm.Show()
    End Sub
    Private Sub ToolStripMenuItem6_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem6.Click
        Try
            Dim frm As pdvContaVales
            Try
                frm = New pdvContaVales
                'frm.Sw_pendientes = False
                frm.ShowDialog()
            Catch ex As Exception
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim dat_FechaIni As Date = Today
        Dim dat_FechaFin As Date = Today
        Dim DatosConexion() As String
        Dim cbo_Suc As String = ""

        Try
            ' Leer datos de conexión desde el archivo
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    cbo_Suc = Globales.oAmbientes.Id_Sucursal
                    Exit For
                End If
            Next

            ' Inicializar objeto de datos
            oDatos = New Datos_Viscoi

            ' Obtener datos desde la base de datos
            If oDatos.PVTA_ContravaleRpt(Globales.oAmbientes.Id_Empresa, cbo_Suc, dat_FechaIni, dat_FechaFin, dtDatos, Mensaje) Then
                If dtDatos IsNot Nothing AndAlso dtDatos.Rows.Count > 0 Then
                    ' Configurar columnas del grid si no están configuradas
                    If GridC_1.DataSource Is Nothing Then
                        GridC_1.DataSource = dtDatos
                        GridV_1.PopulateColumns()
                        ' Ocultar la columna autonumsuc
                        GridV_1.Columns("autonumsuc").Visible = False
                    Else
                        ' Actualizar los datos del grid
                        GridC_1.DataSource = dtDatos
                    End If
                    GridC_1.RefreshDataSource()
                    GridV_1.BestFitColumns()

                Else
                    GridC_1.DataSource = Nothing
                    GridC_1.RefreshDataSource()
                    'MessageBox.Show("No se encontraron datos en la Tabla")
                End If
                'Else
                '    MessageBox.Show("Error al obtener datos: " & Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            oDatos = Nothing
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Static timeCount As Integer
        ' Incrementa el contador de tiempo
        timeCount = timeCount + 1


        If timeCount = PopTiempoEspera Then
            ' Muestra el grid después de 10 segundos
            Call Refrescar()

            GridC_1.Visible = True
            Popup.Visible = True
            timeCount = 0
        ElseIf timeCount = 5 And GridC_1.Visible = True Then
            ' Oculta el grid después de 5 segundos más (en total, 15 segundos)
            'GridC_1.Visible = False
            'Popup.Visible = False
            ' Reinicia el contador de tiempo
            timeCount = 0
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Popup.Visible = False
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        Dim popupForm As New frmPopUp()
        popupForm.ShowDialog()
    End Sub

End Class
