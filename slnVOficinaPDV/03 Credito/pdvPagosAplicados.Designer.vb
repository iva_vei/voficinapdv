﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvPagosAplicados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvPagosAplicados))
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.Chk_Detalle = New System.Windows.Forms.CheckBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txt_Sucursal = New System.Windows.Forms.TextBox()
        Me.txt_MargenesTicket = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.txt_FormatoTicket = New System.Windows.Forms.TextBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.txt_Empresa1 = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.pop_Menu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.pop_Opc1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.pop_Opc2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.pop_Opc3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.col1_FechaAplico = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_CveDocumento = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Serie = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numero = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Total = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Saldodocto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Uuid = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Flujo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdCliente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Nombre = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_EImporteAplicad = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Usuario = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ESerie = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ECveDocumento = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ENumero = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_EReferencia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_EAutonumsuc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.FechaFin = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.FechaIni = New System.Windows.Forms.DateTimePicker()
        Me.btn_Refrescar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_BuscarCliente = New System.Windows.Forms.Button()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Cuenta = New System.Windows.Forms.TextBox()
        Me.lbl_FechaIni = New System.Windows.Forms.Label()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.hideContainerRight = New DevExpress.XtraBars.Docking.AutoHideContainer()
        Me.CG_Ayuda = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.gridBand4 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.PanelFiltros_Acciones.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pop_Menu.SuspendLayout()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hideContainerRight.SuspendLayout()
        Me.CG_Ayuda.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.PanelFiltros_Acciones)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl3.Location = New System.Drawing.Point(0, 612)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(809, 87)
        Me.GroupControl3.TabIndex = 2
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.Image = CType(resources.GetObject("WindowsUIButtonImageOptions1.Image"), System.Drawing.Image)
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Imprimir", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Imprimir", -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.Chk_Detalle)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label36)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Sucursal)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_MargenesTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label31)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lstImpresoras)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_FormatoTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Cajero)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Caja)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Empresa1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_TipoCliente)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(2, 20)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 65)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(65, 65)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(805, 65)
        Me.PanelFiltros_Acciones.TabIndex = 40
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'Chk_Detalle
        '
        Me.Chk_Detalle.AutoSize = True
        Me.Chk_Detalle.Location = New System.Drawing.Point(581, 39)
        Me.Chk_Detalle.Name = "Chk_Detalle"
        Me.Chk_Detalle.Size = New System.Drawing.Size(173, 17)
        Me.Chk_Detalle.TabIndex = 42
        Me.Chk_Detalle.Text = "Mostrar Documentos Aplicados"
        Me.Chk_Detalle.UseVisualStyleBackColor = True
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(135, 5)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(88, 23)
        Me.Label36.TabIndex = 41
        Me.Label36.Text = "Sucursal:"
        '
        'txt_Sucursal
        '
        Me.txt_Sucursal.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Sucursal.Location = New System.Drawing.Point(135, 32)
        Me.txt_Sucursal.Name = "txt_Sucursal"
        Me.txt_Sucursal.ReadOnly = True
        Me.txt_Sucursal.Size = New System.Drawing.Size(130, 30)
        Me.txt_Sucursal.TabIndex = 40
        '
        'txt_MargenesTicket
        '
        Me.txt_MargenesTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_MargenesTicket.Location = New System.Drawing.Point(406, 7)
        Me.txt_MargenesTicket.Name = "txt_MargenesTicket"
        Me.txt_MargenesTicket.Size = New System.Drawing.Size(14, 29)
        Me.txt_MargenesTicket.TabIndex = 37
        Me.txt_MargenesTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_MargenesTicket.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(270, 5)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(103, 23)
        Me.Label31.TabIndex = 23
        Me.Label31.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(270, 37)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(246, 21)
        Me.lstImpresoras.TabIndex = 22
        '
        'txt_FormatoTicket
        '
        Me.txt_FormatoTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FormatoTicket.Location = New System.Drawing.Point(341, 7)
        Me.txt_FormatoTicket.Name = "txt_FormatoTicket"
        Me.txt_FormatoTicket.Size = New System.Drawing.Size(14, 29)
        Me.txt_FormatoTicket.TabIndex = 39
        Me.txt_FormatoTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_FormatoTicket.Visible = False
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion.Location = New System.Drawing.Point(264, 7)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion.TabIndex = 34
        Me.txt_Direccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Location = New System.Drawing.Point(471, 10)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(30, 21)
        Me.txt_Cajero.TabIndex = 21
        Me.txt_Cajero.Text = "0001"
        Me.txt_Cajero.Visible = False
        '
        'txt_Caja
        '
        Me.txt_Caja.Location = New System.Drawing.Point(435, 10)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(30, 21)
        Me.txt_Caja.TabIndex = 20
        Me.txt_Caja.Text = "0001"
        Me.txt_Caja.Visible = False
        '
        'txt_Empresa1
        '
        Me.txt_Empresa1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa1.Location = New System.Drawing.Point(322, 7)
        Me.txt_Empresa1.Name = "txt_Empresa1"
        Me.txt_Empresa1.Size = New System.Drawing.Size(14, 29)
        Me.txt_Empresa1.TabIndex = 38
        Me.txt_Empresa1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Empresa1.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion2.Location = New System.Drawing.Point(283, 7)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion2.TabIndex = 35
        Me.txt_Direccion2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion2.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Location = New System.Drawing.Point(304, 7)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(14, 29)
        Me.txt_TipoCliente.TabIndex = 36
        Me.txt_TipoCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_TipoCliente.Visible = False
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(788, 489)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "DOCUMENTOS"
        '
        'GridC_1
        '
        Me.GridC_1.ContextMenuStrip = Me.pop_Menu
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(784, 464)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'pop_Menu
        '
        Me.pop_Menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.pop_Opc1, Me.pop_Opc2, Me.pop_Opc3})
        Me.pop_Menu.Name = "pop_Menu"
        Me.pop_Menu.Size = New System.Drawing.Size(186, 70)
        '
        'pop_Opc1
        '
        Me.pop_Opc1.Name = "pop_Opc1"
        Me.pop_Opc1.Size = New System.Drawing.Size(185, 22)
        Me.pop_Opc1.Text = "Selecionar hasta aquí"
        '
        'pop_Opc2
        '
        Me.pop_Opc2.Name = "pop_Opc2"
        Me.pop_Opc2.Size = New System.Drawing.Size(185, 22)
        Me.pop_Opc2.Text = "Seleccionar Todo"
        '
        'pop_Opc3
        '
        Me.pop_Opc3.Name = "pop_Opc3"
        Me.pop_Opc3.Size = New System.Drawing.Size(185, 22)
        Me.pop_Opc3.Text = "Invertir Selección"
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.gridBand4})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_FechaAplico, Me.col1_Fecha, Me.col1_CveDocumento, Me.col1_Serie, Me.col1_Numero, Me.col1_Total, Me.col1_Saldodocto, Me.col1_Uuid, Me.col1_IdCliente, Me.col1_Nombre, Me.col1_Flujo, Me.col1_ECveDocumento, Me.col1_ESerie, Me.col1_ENumero, Me.col1_EReferencia, Me.col1_EAutonumsuc, Me.col1_EImporteAplicad, Me.col1_Usuario})
        Me.GridV_1.CustomizationFormBounds = New System.Drawing.Rectangle(577, 602, 266, 236)
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col1_FechaAplico
        '
        Me.col1_FechaAplico.Caption = "FechaAplico"
        Me.col1_FechaAplico.FieldName = "fecha_aplico"
        Me.col1_FechaAplico.Name = "col1_FechaAplico"
        Me.col1_FechaAplico.OptionsColumn.AllowEdit = False
        Me.col1_FechaAplico.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_FechaAplico.Visible = True
        '
        'col1_Fecha
        '
        Me.col1_Fecha.Caption = "Fecha"
        Me.col1_Fecha.FieldName = "fecha"
        Me.col1_Fecha.Name = "col1_Fecha"
        Me.col1_Fecha.OptionsColumn.AllowEdit = False
        Me.col1_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fecha.Visible = True
        '
        'col1_CveDocumento
        '
        Me.col1_CveDocumento.Caption = "CveDocumento"
        Me.col1_CveDocumento.FieldName = "cve_documento"
        Me.col1_CveDocumento.Name = "col1_CveDocumento"
        Me.col1_CveDocumento.OptionsColumn.AllowEdit = False
        Me.col1_CveDocumento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CveDocumento.Visible = True
        '
        'col1_Serie
        '
        Me.col1_Serie.Caption = "Serie"
        Me.col1_Serie.FieldName = "serie"
        Me.col1_Serie.Name = "col1_Serie"
        Me.col1_Serie.OptionsColumn.AllowEdit = False
        Me.col1_Serie.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Serie.Visible = True
        '
        'col1_Numero
        '
        Me.col1_Numero.Caption = "Numero"
        Me.col1_Numero.FieldName = "numero"
        Me.col1_Numero.Name = "col1_Numero"
        Me.col1_Numero.OptionsColumn.AllowEdit = False
        Me.col1_Numero.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numero.Visible = True
        '
        'col1_Total
        '
        Me.col1_Total.Caption = "Total"
        Me.col1_Total.DisplayFormat.FormatString = "N2"
        Me.col1_Total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Total.FieldName = "total"
        Me.col1_Total.Name = "col1_Total"
        Me.col1_Total.OptionsColumn.AllowEdit = False
        Me.col1_Total.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Total.Visible = True
        '
        'col1_Saldodocto
        '
        Me.col1_Saldodocto.Caption = "Saldodocto"
        Me.col1_Saldodocto.DisplayFormat.FormatString = "N2"
        Me.col1_Saldodocto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Saldodocto.FieldName = "saldodocto"
        Me.col1_Saldodocto.Name = "col1_Saldodocto"
        Me.col1_Saldodocto.OptionsColumn.AllowEdit = False
        Me.col1_Saldodocto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Saldodocto.Visible = True
        '
        'col1_Uuid
        '
        Me.col1_Uuid.Caption = "Uuid"
        Me.col1_Uuid.FieldName = "uuid"
        Me.col1_Uuid.Name = "col1_Uuid"
        Me.col1_Uuid.OptionsColumn.AllowEdit = False
        Me.col1_Uuid.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Uuid.Visible = True
        '
        'col1_Flujo
        '
        Me.col1_Flujo.Caption = "Flujo"
        Me.col1_Flujo.FieldName = "flujo"
        Me.col1_Flujo.Name = "col1_Flujo"
        Me.col1_Flujo.OptionsColumn.AllowEdit = False
        Me.col1_Flujo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Flujo.Visible = True
        '
        'col1_IdCliente
        '
        Me.col1_IdCliente.Caption = "IdCliente"
        Me.col1_IdCliente.FieldName = "id_cliente"
        Me.col1_IdCliente.Name = "col1_IdCliente"
        Me.col1_IdCliente.OptionsColumn.AllowEdit = False
        Me.col1_IdCliente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdCliente.Visible = True
        '
        'col1_Nombre
        '
        Me.col1_Nombre.Caption = "Nombre"
        Me.col1_Nombre.FieldName = "nombre"
        Me.col1_Nombre.Name = "col1_Nombre"
        Me.col1_Nombre.OptionsColumn.AllowEdit = False
        Me.col1_Nombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Nombre.Visible = True
        '
        'col1_EImporteAplicad
        '
        Me.col1_EImporteAplicad.Caption = "ImporteAplicad"
        Me.col1_EImporteAplicad.FieldName = "eimporte_aplicad"
        Me.col1_EImporteAplicad.Name = "col1_EImporteAplicad"
        Me.col1_EImporteAplicad.OptionsColumn.AllowEdit = False
        Me.col1_EImporteAplicad.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Usuario
        '
        Me.col1_Usuario.Caption = "Usuario"
        Me.col1_Usuario.FieldName = "usuario"
        Me.col1_Usuario.Name = "col1_Usuario"
        Me.col1_Usuario.OptionsColumn.AllowEdit = False
        Me.col1_Usuario.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Usuario.Visible = True
        '
        'col1_ESerie
        '
        Me.col1_ESerie.Caption = "ESerie"
        Me.col1_ESerie.FieldName = "eserie"
        Me.col1_ESerie.Name = "col1_ESerie"
        Me.col1_ESerie.OptionsColumn.AllowEdit = False
        Me.col1_ESerie.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ESerie.Visible = True
        '
        'col1_ECveDocumento
        '
        Me.col1_ECveDocumento.Caption = "ECveDocumento"
        Me.col1_ECveDocumento.FieldName = "ecve_documento"
        Me.col1_ECveDocumento.Name = "col1_ECveDocumento"
        Me.col1_ECveDocumento.OptionsColumn.AllowEdit = False
        Me.col1_ECveDocumento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ECveDocumento.Visible = True
        '
        'col1_ENumero
        '
        Me.col1_ENumero.Caption = "ENumero"
        Me.col1_ENumero.FieldName = "enumero"
        Me.col1_ENumero.Name = "col1_ENumero"
        Me.col1_ENumero.OptionsColumn.AllowEdit = False
        Me.col1_ENumero.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ENumero.Visible = True
        '
        'col1_EReferencia
        '
        Me.col1_EReferencia.Caption = "Referencia"
        Me.col1_EReferencia.FieldName = "ereferencia"
        Me.col1_EReferencia.Name = "col1_EReferencia"
        Me.col1_EReferencia.OptionsColumn.AllowEdit = False
        Me.col1_EReferencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_EReferencia.Visible = True
        '
        'col1_EAutonumsuc
        '
        Me.col1_EAutonumsuc.Caption = "Autonumsuc"
        Me.col1_EAutonumsuc.FieldName = "eautonumsuc"
        Me.col1_EAutonumsuc.Name = "col1_EAutonumsuc"
        Me.col1_EAutonumsuc.OptionsColumn.AllowEdit = False
        Me.col1_EAutonumsuc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_EAutonumsuc.Visible = True
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.FechaFin)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.Label7)
        Me.GroupControl1.Controls.Add(Me.FechaIni)
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.btn_BuscarCliente)
        Me.GroupControl1.Controls.Add(Me.txt_Nombre)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txt_Cuenta)
        Me.GroupControl1.Controls.Add(Me.lbl_FechaIni)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl1.Location = New System.Drawing.Point(0, 489)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(809, 123)
        Me.GroupControl1.TabIndex = 0
        '
        'FechaFin
        '
        Me.FechaFin.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaFin.Location = New System.Drawing.Point(667, 30)
        Me.FechaFin.Name = "FechaFin"
        Me.FechaFin.Size = New System.Drawing.Size(142, 30)
        Me.FechaFin.TabIndex = 23
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(579, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 23)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Fecha Fin"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(346, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 23)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Fecha Ini"
        '
        'FechaIni
        '
        Me.FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaIni.Location = New System.Drawing.Point(437, 30)
        Me.FechaIni.Name = "FechaIni"
        Me.FechaIni.Size = New System.Drawing.Size(142, 30)
        Me.FechaIni.TabIndex = 21
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.ImageOptions.Image = CType(resources.GetObject("btn_Refrescar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_Refrescar.Location = New System.Drawing.Point(762, 72)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Refrescar.TabIndex = 2
        '
        'btn_BuscarCliente
        '
        Me.btn_BuscarCliente.Location = New System.Drawing.Point(303, 32)
        Me.btn_BuscarCliente.Name = "btn_BuscarCliente"
        Me.btn_BuscarCliente.Size = New System.Drawing.Size(42, 31)
        Me.btn_BuscarCliente.TabIndex = 1
        Me.btn_BuscarCliente.Text = "..."
        Me.btn_BuscarCliente.UseVisualStyleBackColor = True
        '
        'txt_Nombre
        '
        Me.txt_Nombre.BackColor = System.Drawing.Color.White
        Me.txt_Nombre.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(97, 69)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.ReadOnly = True
        Me.txt_Nombre.Size = New System.Drawing.Size(573, 30)
        Me.txt_Nombre.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(5, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 23)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Nombre"
        '
        'txt_Cuenta
        '
        Me.txt_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cuenta.Location = New System.Drawing.Point(97, 33)
        Me.txt_Cuenta.Name = "txt_Cuenta"
        Me.txt_Cuenta.Size = New System.Drawing.Size(200, 30)
        Me.txt_Cuenta.TabIndex = 0
        Me.txt_Cuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_FechaIni
        '
        Me.lbl_FechaIni.AutoSize = True
        Me.lbl_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_FechaIni.Location = New System.Drawing.Point(5, 36)
        Me.lbl_FechaIni.Name = "lbl_FechaIni"
        Me.lbl_FechaIni.Size = New System.Drawing.Size(69, 23)
        Me.lbl_FechaIni.TabIndex = 1
        Me.lbl_FechaIni.Text = "Cuenta"
        '
        'DockManager1
        '
        Me.DockManager1.AutoHideContainers.AddRange(New DevExpress.XtraBars.Docking.AutoHideContainer() {Me.hideContainerRight})
        Me.DockManager1.Form = Me
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'hideContainerRight
        '
        Me.hideContainerRight.BackColor = System.Drawing.SystemColors.Control
        Me.hideContainerRight.Controls.Add(Me.CG_Ayuda)
        Me.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.hideContainerRight.Location = New System.Drawing.Point(788, 0)
        Me.hideContainerRight.Name = "hideContainerRight"
        Me.hideContainerRight.Size = New System.Drawing.Size(21, 489)
        '
        'CG_Ayuda
        '
        Me.CG_Ayuda.Controls.Add(Me.DockPanel1_Container)
        Me.CG_Ayuda.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.CG_Ayuda.ID = New System.Guid("234d4c82-6833-474c-8637-7252e94fa3b6")
        Me.CG_Ayuda.Location = New System.Drawing.Point(0, 0)
        Me.CG_Ayuda.Name = "CG_Ayuda"
        Me.CG_Ayuda.OriginalSize = New System.Drawing.Size(200, 200)
        Me.CG_Ayuda.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.CG_Ayuda.SavedIndex = 0
        Me.CG_Ayuda.Size = New System.Drawing.Size(200, 699)
        Me.CG_Ayuda.Text = "AYUDA - TECLAS RAPIDAS"
        Me.CG_Ayuda.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 26)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(193, 670)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'GridBand1
        '
        Me.GridBand1.Columns.Add(Me.col1_FechaAplico)
        Me.GridBand1.Columns.Add(Me.col1_Fecha)
        Me.GridBand1.Columns.Add(Me.col1_CveDocumento)
        Me.GridBand1.Columns.Add(Me.col1_Serie)
        Me.GridBand1.Columns.Add(Me.col1_Numero)
        Me.GridBand1.Columns.Add(Me.col1_Total)
        Me.GridBand1.Columns.Add(Me.col1_Saldodocto)
        Me.GridBand1.Columns.Add(Me.col1_Uuid)
        Me.GridBand1.Columns.Add(Me.col1_Flujo)
        Me.GridBand1.Columns.Add(Me.col1_IdCliente)
        Me.GridBand1.Columns.Add(Me.col1_Nombre)
        Me.GridBand1.Columns.Add(Me.col1_EImporteAplicad)
        Me.GridBand1.Columns.Add(Me.col1_Usuario)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 900
        '
        'gridBand4
        '
        Me.gridBand4.Columns.Add(Me.col1_ESerie)
        Me.gridBand4.Columns.Add(Me.col1_ECveDocumento)
        Me.gridBand4.Columns.Add(Me.col1_ENumero)
        Me.gridBand4.Columns.Add(Me.col1_EReferencia)
        Me.gridBand4.Columns.Add(Me.col1_EAutonumsuc)
        Me.gridBand4.Name = "gridBand4"
        Me.gridBand4.Visible = False
        Me.gridBand4.VisibleIndex = -1
        Me.gridBand4.Width = 375
        '
        'pdvPagosAplicados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(809, 699)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.hideContainerRight)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl3)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "pdvPagosAplicados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pagos Aplicados"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pop_Menu.ResumeLayout(False)
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hideContainerRight.ResumeLayout(False)
        Me.CG_Ayuda.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lbl_FechaIni As Label
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_Cuenta As TextBox
    Friend WithEvents btn_BuscarCliente As Button
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Private WithEvents txt_FormatoTicket As TextBox
    Private WithEvents txt_Empresa1 As TextBox
    Private WithEvents txt_MargenesTicket As TextBox
    Private WithEvents txt_TipoCliente As TextBox
    Private WithEvents txt_Direccion2 As TextBox
    Private WithEvents txt_Direccion As TextBox
    Friend WithEvents btn_Refrescar As DevExpress.XtraEditors.SimpleButton
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents CG_Ayuda As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents hideContainerRight As DevExpress.XtraBars.Docking.AutoHideContainer
    Friend WithEvents pop_Menu As ContextMenuStrip
    Friend WithEvents pop_Opc1 As ToolStripMenuItem
    Friend WithEvents pop_Opc2 As ToolStripMenuItem
    Friend WithEvents pop_Opc3 As ToolStripMenuItem
    Friend WithEvents Label7 As Label
    Friend WithEvents FechaIni As DateTimePicker
    Friend WithEvents Label36 As Label
    Friend WithEvents txt_Sucursal As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents FechaFin As DateTimePicker
    Friend WithEvents Chk_Detalle As CheckBox
    Friend WithEvents col1_FechaAplico As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_CveDocumento As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Serie As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numero As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Total As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Saldodocto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Uuid As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdCliente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Nombre As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Flujo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ECveDocumento As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ESerie As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ENumero As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_EReferencia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_EAutonumsuc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_EImporteAplicad As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Usuario As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand4 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
End Class
