﻿Imports DevExpress.XtraBars
Imports DevExpress.XtraGrid.Views.Base

Public Class pdvPagosAplicados
    Dim _Accion As Boolean
    Dim Usuario_Permitir_CambiarPago As String
    Dim Timbrar_pago As String
    Public TecladoAtajos As List(Of Principal.clsTecladoAtajos)

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property


    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim DatosConexion() As String
        Try
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            Usuario_Permitir_CambiarPago = "NO"
            Timbrar_pago = "NO"
            txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal
            FechaIni.Enabled = False

            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", Globales.oAmbientes.oUsuario.Id_usuario, "DESBLOQUEA_FECHA", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    Usuario_Permitir_CambiarPago = dtDatos.Rows(0).Item("valor")
                End If
            End If
            If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", "", "TIMBRAR_PAGO", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    Timbrar_pago = dtDatos.Rows(0).Item("valor")
                End If
            End If

            FechaIni.Enabled = IIf(Usuario_Permitir_CambiarPago = "SI", True, False)

            Call Refrescar()

            Dim pd As New Printing.PrintDocument
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer

            If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", "TODAS", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                    txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim APagar As Double = 0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.CLI_Aplicaciones_Sel(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_Cuenta.Text, FechaIni.Value, FechaIni.Value, Globales.oAmbientes.oUsuario.Id_usuario, False, dtDatos, Mensaje) Then
                GridC_1.DataSource = dtDatos
                GridC_1.RefreshDataSource()
                GridV_1.BestFitColumns()
            Else
                GridC_1.DataSource = Nothing
                GridC_1.RefreshDataSource()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub bnt_Nuevo()
        txt_Cuenta.Text = ""
        txt_Empresa1.Text = ""
        txt_Nombre.Text = ""
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()
    End Sub

    Private Sub pdvCuentas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim oDatos As Datos_Viscoi
        Dim msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, txt_Cajero.Text, 0, msj) Then
                MessageBox.Show(msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Application.Exit()
            End If
            txt_Cuenta.Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_BuscarCliente_Click(sender As Object, e As EventArgs) Handles btn_BuscarCliente.Click
        Dim oFrm As pdvCuentas
        Try
            oFrm = New pdvCuentas
            oFrm.Tipo = ""
            oFrm.TipoForma = pdvCuentas.eTipoForma.Cliente
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Cuenta.Text = oFrm.Cuenta
                txt_Nombre.Text = oFrm.Nombre

                Call Refrescar()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub txt_Cuenta_LostFocus(sender As Object, e As EventArgs) Handles txt_Cuenta.LostFocus
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi
            If txt_Cuenta.Text <> "" Then
                If oDatos.PVTA_Recupera_Cuentas(txt_Cuenta.Text, "", "", "", "", "", "", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        txt_Nombre.Text = dtDatos.Rows(0).Item("nombre").ToString.Trim

                        Call Refrescar()
                    Else
                        txt_Nombre.Text = ""

                    End If
                Else
                    txt_Nombre.Text = ""

                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Ayuda()
        Try
            Call ControlBotones("Ayuda")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_NuevaVenta()
        Try
            Call ControlBotones("Nuevo")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_TerminarVenta()
        Try
            Call ControlBotones("Terminar")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0

        ''Dim Total As Double
        ''Dim PagoContado As Double
        ''Dim PagoDE As Double
        ''Dim PagoTotal As Double
        Try
            Select Case tag.ToUpper
                Case "AYUDA"
                    CG_Ayuda.Visible = If(CG_Ayuda.Visible, False, True)
                Case "IMPRIMIR"
                    Call Imprimir(txt_FormatoTicket.Text)
                Case "CANCELAR"
                    Call Btn_Cancel_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Tag
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Btn_Cancel_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub
    Private Sub Imprimir(ByVal Formato As String)
        Dim Reporte As Object
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entAplicaciones
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool


        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.CLI_Aplicaciones_Sel(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_Cuenta.Text, FechaIni.Value, FechaIni.Value, Globales.oAmbientes.oUsuario.Id_usuario, False, dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then

                    oTicket = New entAplicaciones

                    oTicket.Fill(dtDatos)



                    Dim MargenAba As Integer = 0
                    Dim MargenIzq As Integer = 0
                    Dim MargenDer As Integer = 0
                    Dim MargenArr As Integer = 0

                    Dim printBase As DevExpress.XtraPrinting.PrintToolBase
                    Select Case Formato
                        Case "SEAFON"
                            Reporte = New XtraRep_Aplicacion
                            TryCast(Reporte, XtraRep_Aplicacion).Empresa = Globales.oAmbientes.Id_Empresa
                            TryCast(Reporte, XtraRep_Aplicacion).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_Aplicacion).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_Aplicacion).PrintingSystem)
                        Case "EPSON"
                            Reporte = New XtraRep_Aplicacion
                            TryCast(Reporte, XtraRep_Aplicacion).Empresa = Globales.oAmbientes.Id_Empresa
                            TryCast(Reporte, XtraRep_Aplicacion).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_Aplicacion).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_Aplicacion).PrintingSystem)
                        Case Else
                            Reporte = New XtraRep_Aplicacion
                            TryCast(Reporte, XtraRep_Aplicacion).Empresa = Globales.oAmbientes.Id_Empresa
                            TryCast(Reporte, XtraRep_Aplicacion).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_Aplicacion).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_Aplicacion).PrintingSystem)
                    End Select
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()


                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If


            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

End Class