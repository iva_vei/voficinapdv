﻿Imports DevExpress.XtraBars
Imports DevExpress.XtraGrid.Views.Base

Public Class pdvLiquidacion
    Dim _Accion As Boolean
    Dim Usuario_Permitir_CambiarPago As String
    Dim Timbrar_pago As String
    Public TecladoAtajos As List(Of Principal.clsTecladoAtajos)
    Public Tipo As eTipoTran1
    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property


    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim dtTipos As DataTable = Nothing
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim DatosConexion() As String
        Try
            'If Tipo = eTipoTran.VALES Then
            '    Label8.Visible = True
            '    cbo_FormaPago.Visible = True
            'Else
            '    cbo_FormaPago.Visible = False
            '    Label8.Visible = False
            'End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            Usuario_Permitir_CambiarPago = "NO"
            Timbrar_pago = "NO"
            txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal
            DateTimePicker1.Enabled = False

            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", Globales.oAmbientes.oUsuario.Id_usuario, "DESBLOQUEA_FECHA", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    Usuario_Permitir_CambiarPago = dtDatos.Rows(0).Item("valor")
                End If
            End If
            If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", "", "TIMBRAR_PAGO", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    Timbrar_pago = dtDatos.Rows(0).Item("valor")
                End If
            End If
            If oDatos.PVTA_Sat_CFDI_FechaActual(dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    DateTimePicker1.Value = dtDatos.Rows(0).Item("fum")
                End If
            End If

            DateTimePicker1.Enabled = IIf(Usuario_Permitir_CambiarPago = "SI", True, False)

            txt_Total.Text = "0"
            Call Refrescar()

            GridC_2.DataSource = TecladoAtajos
            GridC_2.RefreshDataSource()

            Dim pd As New Printing.PrintDocument
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer

            If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", "TODAS", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                    txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper
                End If
            End If
            Try
                oDatos = New Datos_Viscoi
                If oDatos.Crm_Tipos_Sel(Globales.oAmbientes.Id_Empresa, "PAGOS", False, "LIQUIDACION", dtTipos, Mensaje) Then
                    If dtTipos.Rows.Count > 0 Then
                        cbo_FormaPago.Visible = True
                        lblFormaPago.Visible = True
                    Else
                        cbo_FormaPago.Visible = False
                        lblFormaPago.Visible = False
                    End If
                    cbo_FormaPago.DataSource = dtTipos
                    Me.cbo_FormaPago.SelectedIndex = 0
                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim APagar As Double = 0
        Try
            oDatos = New Datos_Viscoi
            If txt_Cuenta.Text <> "" Then
                If oDatos.PVTA_Recupera_cliedocta_Final(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_Cuenta.Text, txt_IdClienteFinal.Text, DateTimePicker1.Value, Txt_CodigoBonifciacion.Text, cbo_FormaPago.SelectedValue, dtDatos, Mensaje) Then
                    GridC_1.DataSource = dtDatos
                    GridC_1.RefreshDataSource()
                    GridV_1.BestFitColumns()

                    If dtDatos.Rows.Count > 0 Then
                        Txt_CodigoBonifciacion.Enabled = False
                        Txt_CodigoBonifciacion.Text = dtDatos.Rows(0).Item("flujo")
                    End If
                Else
                    GridC_1.DataSource = Nothing
                    GridC_1.RefreshDataSource()
                End If
                APagar = CalculaTotal()
            End If
            txt_TotalAPagar.Text = Format(APagar, "C2")
            txt_Total.Text = Format(APagar, "C2")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub bnt_Nuevo()
        txt_Cuenta.Text = ""
        txt_Empresa1.Text = ""
        txt_Nombre.Text = ""
        txt_TotalAPagar.Text = Format(0, "C2")
        txt_Total.Text = Format(0, "C2")
    End Sub
    Private Sub btn_Aceptar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim sw_continuar As Boolean = True
        Dim SaldoDocto As Double = 0
        Dim Pago As Double = 0

        Dim Cuenta As String
        Dim Autonumsuc As String = ""
        Dim MonedaNacional As String = ""
        Dim TCambio As Double = 1.0

        Dim dtDatosFact As DataTable
        Dim drRenglon As DataRow

        Dim dtCabecero As DataTable = Nothing
        Dim dtDetalle As DataTable = Nothing
        Dim dtOtros As DataTable = Nothing

        Dim dt1Cabecero As DataTable = Nothing
        Dim dt1Detalle As DataTable = Nothing
        Dim dt1Otros As DataTable = Nothing

        Dim sTipoDocto As String = ""
        Dim sObserv2 As String = ""
        Dim sIdEmpresa As String = ""
        Dim sIdSucursal As String = ""
        Dim sAutonumsuc As String = ""
        Dim sEstatus As String = ""
        Dim sFolio As String = ""
        Dim sSerie As String = ""
        Dim sNombre As String = ""
        Dim sRFC As String = ""
        Dim sFormaPago As String = ""
        Dim sUsoCFDI As String = ""
        Dim sCorreos As String = ""
        Dim sSubTotal As String = ""
        Dim sIVA As String = ""
        Dim sTotal As String = ""
        Dim gUUID As String = ""
        Dim gnoCertificadoSAT = ""
        Dim gselloCFD = ""
        Dim gselloSAT = ""
        Dim gCadenaOriginalComplemento = ""
        Dim gVersion = ""
        Dim gFechaTimbrado = ""
        Dim gRfcProvCertif = ""
        Dim gNoCertificado = ""
        Try
            _Accion = True

            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Monedas(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", "ENUSO", "NO", "SI", dtDatos, MonedaNacional, Mensaje) Then

            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If

            If GridV_1.RowCount > 0 Then
                For iI = 0 To GridV_1.RowCount - 1
                    GridV_1.FocusedRowHandle = iI
                Next

                Cuenta = GridV_1.GetFocusedRowCellValue(col1_IdCliente)
                ''Autonumsuc = GridV_1.GetFocusedRowCellValue(col1_Autonumsuc)
                SaldoDocto = GridV_1.GetFocusedRowCellValue(col1_Saldodocto)
                Pago = Double.Parse(txt_Total.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                If Pago > 0 Then
                    dtDatosFact = GridC_1.DataSource
                    Autonumsuc = ""
                    For Each drRenglon In dtDatosFact.Rows
                        If drRenglon.Item(col1_SwSel.FieldName) Then
                            If Autonumsuc.Length > 0 Then
                                Autonumsuc = Autonumsuc & "|" & drRenglon.Item(col1_Autonumsuc.FieldName)
                            Else
                                Autonumsuc = drRenglon.Item(col1_Autonumsuc.FieldName)
                            End If
                        End If
                    Next
                    If Autonumsuc <> "" Then
                        Dim AS_Pago As String = ""
                        Dim FormaPago As String = ""
                        FormaPago = cbo_FormaPago.Text
                        If oDatos.PVTA_Inserta_Pago_cliedocta_Final(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, Cuenta, txt_IdClienteFinal.Text, Autonumsuc, MonedaNacional, Pago, TCambio, Globales.oAmbientes.oUsuario.Id_usuario, txt_Caja.Text, txt_Cajero.Text, DateTimePicker1.Value, FormaPago, AS_Pago, "", Txt_CodigoBonifciacion.Text, "", "", Mensaje) Then
                            Call ImprimirTicket(AS_Pago, txt_FormatoTicket.Text)

                            Call Refrescar()
                            If Timbrar_pago = "SI" Then
                                Dim frm2 As pdvFactura40
                                frm2 = New pdvFactura40
                                frm2.SW_CerrarAuto = True
                                frm2.txt_Autonumsuc.Text = AS_Pago
                                frm2.ShowDialog()

                            Else
                                Me.Close()
                            End If
                        Else
                            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    Else
                        MessageBox.Show("Debes introducir un importe mayor a cero", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()
    End Sub

    Private Sub pdvCuentas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim oDatos As Datos_Viscoi
        Dim msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, txt_Cajero.Text, 0, msj) Then
                MessageBox.Show(msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Application.Exit()
            End If
            txt_Cuenta.Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_BuscarCliente_Click(sender As Object, e As EventArgs) Handles btn_BuscarCliente.Click
        Dim oFrm As pdvCuentas
        Try
            oFrm = New pdvCuentas
            oFrm.Tipo = ""
            oFrm.TipoForma = pdvCuentas.eTipoForma.Cliente
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Cuenta.Text = oFrm.Cuenta
                txt_Nombre.Text = oFrm.Nombre

                ''Call Refrescar()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Function CalculaTotal() As Double
        Dim dtDatos As DataTable
        Dim drRenglon As DataRow
        Dim APagar As Double = 0
        Try
            dtDatos = GridC_1.DataSource
            If dtDatos IsNot Nothing Then
                If dtDatos.Rows.Count > 0 Then
                    For Each drRenglon In dtDatos.Rows
                        If drRenglon.Item(col1_SwSel.FieldName) Then
                            APagar += drRenglon.Item(col1_Saldodocto.FieldName)
                        End If
                    Next
                End If
            End If

            ''GridV_1.FocusedColumn = col1_Saldodocto
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Return APagar
    End Function
    Private Sub GridV_1_CellValueChanged(sender As Object, e As CellValueChangedEventArgs) Handles GridV_1.CellValueChanged
        Dim APagar As Double = 0
        APagar = CalculaTotal()
        txt_TotalAPagar.Text = Format(APagar, "C2")
        txt_Total.Text = Format(APagar, "C2")
    End Sub

    Private Sub GridV_1_CellValueChanging(sender As Object, e As CellValueChangedEventArgs) Handles GridV_1.CellValueChanging
        Dim APagar As Double = 0
        Try
            If e.Column.FieldName = col1_SwSel.FieldName Then

                APagar = CalculaTotal()
                txt_TotalAPagar.Text = Format(APagar, "C2")
                txt_Total.Text = Format(APagar, "C2")

                APagar = APagar + GridV_1.GetRowCellValue(e.RowHandle, col1_Saldodocto) * IIf(e.Value, 1, -1)

                txt_TotalAPagar.Text = Format(APagar, "C2")
                txt_Total.Text = Format(APagar, "C2")
                ''GridV_1.FocusedColumn = col1_Saldodocto
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub ImprimirTicket(ByVal P_Autonumsuc As String, ByVal Formato As String)
        Dim Reporte As Object
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Transacciones(CDate("1900-01-01") _
                                               , CDate("1900-01-01") _
                                               , 0 _
                                               , P_Autonumsuc _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , txt_Sucursal.Text _
                                               , "OPER" _
                                               , "" _
                                               , "" _
                                               , "" _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , dtDatos2 _
                                               , Mensaje) Then
                If dtDatos1.Rows.Count > 0 Then
                    txt_Empresa1.Text = dtDatos1.Rows(0).Item("empresa")
                    txt_Direccion.Text = dtDatos1.Rows(0).Item("direccion")
                    txt_Direccion2.Text = dtDatos2.Rows(0).Item("direccion")

                    txt_Cajero.Text = dtDatos.Rows(0).Item("cajero")
                    txt_Caja.Text = dtDatos.Rows(0).Item("caja")
                    txt_TipoCliente.Text = dtDatos.Rows(0).Item("referencia")

                    If oDatos.PVTA_Recupera_DetVentas_Impresion(CDate("1900-01-01") _
                                              , P_Autonumsuc _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , txt_Sucursal.Text _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , Mensaje) Then


                        oTicket = New entTicketVenta
                        oTicket.Fill(dtDatos, dtDatos1, Nothing)

                        oTicket.Empresa = txt_Empresa1.Text
                        oTicket.Direccion = txt_Direccion.Text
                        oTicket.Direccion2 = txt_Direccion2.Text
                        oTicket.Cajero = txt_Cajero.Text
                        oTicket.Caja = txt_Caja.Text
                        ''If txt_NombreCanje.Text.Length > 0 Then
                        ''    oTicket.Lealtad_nombre = txt_NombreCanje.Text
                        ''    oTicket.Puntos_saldo_ant = PuntosActual
                        ''    oTicket.Puntos_acumulados = PuntosAcumulados
                        ''    oTicket.Puntos_utilizados = PuntosUtilizados
                        ''    oTicket.Puntos_saldo_act = PuntosFinal
                        ''
                        ''    oTicket.De_saldo_ant = DEActual
                        ''    oTicket.De_acumulados = DEAcumulados
                        ''    oTicket.De_utilizados = DEUtilizados
                        ''    oTicket.De_saldo_act = DEFinal
                        ''Else
                        ''    oTicket.Lealtad_nombre = 0
                        ''    oTicket.Puntos_saldo_ant = 0
                        ''    oTicket.Puntos_acumulados = 0
                        ''    oTicket.Puntos_utilizados = 0
                        ''    oTicket.Puntos_saldo_act = 0
                        ''
                        ''    oTicket.De_saldo_ant = 0
                        ''    oTicket.De_acumulados = 0
                        ''    oTicket.De_utilizados = 0
                        ''    oTicket.De_saldo_act = 0
                        ''End If

                        Dim MargenAba As Integer = 0
                        Dim MargenIzq As Integer = 0
                        Dim MargenDer As Integer = 0
                        Dim MargenArr As Integer = 0

                        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
                        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
                        Select Case Formato
                            Case "CARTA"
                                Reporte = New xtraRepTicketVentaCarta
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVentaCarta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVentaCarta).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVentaCarta).SwReimpresion = False
                                TryCast(Reporte, xtraRepTicketVentaCarta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVentaCarta).PrintingSystem)
                            Case "SEAFON"
                                Reporte = New xtraRepTicketVenta_Seafon
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).SwReimpresion = False
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta_Seafon).PrintingSystem)
                            Case "EPSON"
                                Reporte = New xtraRepTicketVenta
                                TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVenta).SwReimpresion = False
                                TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                            Case Else
                                Reporte = New xtraRepTicketVenta
                                TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                        End Select
                        If Formato = "CARTA" Then
                            printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                            printTool.ShowPreviewDialog()
                        Else
                            ''lstImpresoras.SelectedIndex = 5
                            printBase.Print(lstImpresoras.Text)
                        End If

                        If oDatos.PVTA_Recupera_Desglose1(P_Autonumsuc _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , txt_Sucursal.Text _
                                                , CDate("01/01/1900") _
                                                , "" _
                                                , "" _
                                                , "CREDITO" _
                                                , dtDatos _
                                                , Mensaje) Then
                            If dtDatos.Rows.Count > 0 Then
                                If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                                        , P_Autonumsuc _
                                                        , Globales.oAmbientes.Id_Empresa _
                                                        , txt_Sucursal.Text _
                                                        , dtDatos _
                                                        , Mensaje) Then
                                    If dtDatos.Rows.Count > 0 Then
                                        Dim oPagare As New entPagare
                                        oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                        oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                        oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                        oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                        oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                        oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                        oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                        oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                        oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                        oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                        Select Case Formato
                                            Case "SEAFON"
                                                Reporte = New xtraRepTicketPagare
                                                TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                            Case "EPSON"
                                                Reporte = New xtraRepTicketPagare
                                                TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                            Case Else
                                                Reporte = New xtraRepTicketPagare
                                                TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                        End Select
                                        printBase.Print(lstImpresoras.Text)

                                    End If
                                End If
                            End If
                        End If

                    Else
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                End If
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub txt_Cuenta_LostFocus(sender As Object, e As EventArgs) Handles txt_Cuenta.LostFocus
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi
            If txt_Cuenta.Text <> "" Then
                If oDatos.PVTA_Recupera_Cuentas(txt_Cuenta.Text, "", "", "", "", "", "", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        txt_Nombre.Text = dtDatos.Rows(0).Item("nombre").ToString.Trim

                        Call Refrescar()
                    Else
                        txt_Nombre.Text = ""

                    End If
                Else
                    txt_Nombre.Text = ""

                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Ayuda()
        Try
            Call ControlBotones("Ayuda")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_NuevaVenta()
        Try
            Call ControlBotones("Nuevo")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_TerminarVenta()
        Try
            Call ControlBotones("Terminar")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0

        ''Dim Total As Double
        ''Dim PagoContado As Double
        ''Dim PagoDE As Double
        ''Dim PagoTotal As Double
        Try
            Select Case tag.ToUpper
                Case "AYUDA"
                    CG_Ayuda.Visible = If(CG_Ayuda.Visible, False, True)
                Case "TERMINAR"
                    Call btn_Aceptar()
                Case "NUEVO"
                    Call bnt_Nuevo()
                Case "CANCELAR"
                    Call Btn_Cancel_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Tag
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Btn_Cancel_Click(sender As Object, e As EventArgs) Handles Btn_Cancel.Click
        Me.Close()
    End Sub

    Private Sub pop_Opc3_Click(sender As Object, e As EventArgs) Handles pop_Opc3.Click
        Dim Valor As Boolean
        Try
            For iI = 0 To GridV_1.RowCount - 1
                GridV_1.FocusedRowHandle = iI
                Valor = GridV_1.GetFocusedRowCellValue(col1_SwSel.FieldName)
                GridV_1.SetFocusedRowCellValue(col1_SwSel.FieldName, Not Valor)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub pop_Opc2_Click(sender As Object, e As EventArgs) Handles pop_Opc2.Click
        Try
            For iI = 0 To GridV_1.RowCount - 1
                GridV_1.FocusedRowHandle = iI
                GridV_1.SetFocusedRowCellValue(col1_SwSel.FieldName, True)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub pop_Opc1_Click(sender As Object, e As EventArgs) Handles pop_Opc1.Click
        Dim Valor As Boolean
        Dim iSel As Integer
        Try
            iSel = GridV_1.FocusedRowHandle
            For iI = 0 To GridV_1.RowCount - 1
                GridV_1.FocusedRowHandle = iI
                Valor = IIf(iI <= iSel, True, False)
                GridV_1.SetFocusedRowCellValue(col1_SwSel.FieldName, Valor)
            Next
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btn_FinalCliente_Click(sender As Object, e As EventArgs) Handles btn_FinalCliente.Click
        Dim oFrm As pdvCuentasFinal
        Try
            oFrm = New pdvCuentasFinal
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_IdClienteFinal.Text = oFrm.Cuenta
                txt_NombreClienteFinal.Text = oFrm.Nombre
                BuscarFinalCuenta(txt_IdClienteFinal.Text)
                Call Refrescar()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub BuscarFinalCuenta(ByRef Tarjeta As String)
        Dim Msj As String = ""

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim posExt As Integer
        Dim posInt As Integer
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_ClienteFinal(txt_IdClienteFinal.Text, "", "", "", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_IdClienteFinal.Text = dtDatos.Rows(0).Item("id_cliente").ToString.Trim
                    txt_NombreClienteFinal.Text = dtDatos.Rows(0).Item("nombre_cli").ToString.Trim

                    txt_NombreClienteFinal.Focus()

                Else
                    txt_NombreClienteFinal.Text = ""
                End If
            Else
                txt_NombreClienteFinal.Text = ""
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub txt_IdClienteFinal_LostFocus(sender As Object, e As EventArgs) Handles txt_IdClienteFinal.LostFocus
        If txt_IdClienteFinal.Text <> "" Then
            Call BuscarFinalCuenta(txt_IdClienteFinal.Text)
            Call Refrescar()
        Else
            txt_NombreClienteFinal.Text = ""
        End If
    End Sub
End Class