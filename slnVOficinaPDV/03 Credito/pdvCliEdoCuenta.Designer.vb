﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCliEdoCuenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim col1_saldo_acumulado As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvCliEdoCuenta))
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions3 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions4 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions5 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdSucursal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_CveDocumento = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdCliente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdEmpresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Ejercicio = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipocartera = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipodoc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Serie = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numero = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cargo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Abono = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Saldodocto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Referencia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fechavence = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_vencido = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdVendedor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Concepto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Moneda = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Total = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Importe = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Iva = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tcambio = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cvedocto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_FolioAclaracion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fum = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdUsuario = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Feccap = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Autonumsuc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Poliza = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fechapoliza = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_AplContable = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_FecUltmovto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_SaldoF = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_VencidoF = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_DiasvencF = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestosTrasladados = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestosRetenidos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TotalImpuestos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIva = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIva = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIeps = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIeps = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIvaret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIvaret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIsrret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIsrret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdCcosto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Flujo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand3 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_TasaOtroimp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoOtroimp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_SwSel = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.Col1_Cargoo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.lbl_FechaIni = New System.Windows.Forms.Label()
        Me.txt_Cuenta = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.btn_BuscarCliente = New System.Windows.Forms.Button()
        Me.btn_Refrescar = New DevExpress.XtraEditors.SimpleButton()
        Me.Btn_Cancel = New System.Windows.Forms.Button()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_Empresa1 = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_FormatoTicket = New System.Windows.Forms.TextBox()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txt_MargenesTicket = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txt_pvence6 = New System.Windows.Forms.TextBox()
        Me.txt_pvence5 = New System.Windows.Forms.TextBox()
        Me.txt_pvence4 = New System.Windows.Forms.TextBox()
        Me.txt_pvence3 = New System.Windows.Forms.TextBox()
        Me.txt_pvence2 = New System.Windows.Forms.TextBox()
        Me.txt_pvence1 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_vence6 = New System.Windows.Forms.TextBox()
        Me.txt_vence5 = New System.Windows.Forms.TextBox()
        Me.txt_vence4 = New System.Windows.Forms.TextBox()
        Me.txt_vence3 = New System.Windows.Forms.TextBox()
        Me.txt_vence2 = New System.Windows.Forms.TextBox()
        Me.txt_vence1 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Txt_anticipo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_vencido = New System.Windows.Forms.TextBox()
        Me.txt_sfinal = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.WindowsUIButtonPanel1 = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_sucursal = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.Cg_ayuda = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.GridC_2 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col2_Accion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Combinacion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        col1_saldo_acumulado = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.WindowsUIButtonPanel1.SuspendLayout()
        Me.PanelFiltros_Acciones.SuspendLayout()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Cg_ayuda.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'col1_saldo_acumulado
        '
        col1_saldo_acumulado.Caption = "saldo_acumulado"
        col1_saldo_acumulado.DisplayFormat.FormatString = "C2"
        col1_saldo_acumulado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        col1_saldo_acumulado.FieldName = "saldo_acumulado"
        col1_saldo_acumulado.MinWidth = 30
        col1_saldo_acumulado.Name = "col1_saldo_acumulado"
        col1_saldo_acumulado.Visible = True
        col1_saldo_acumulado.Width = 112
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        GridLevelNode1.RelationName = "Level1"
        Me.GridC_1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridC_1.Location = New System.Drawing.Point(0, 0)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(1389, 269)
        Me.GridC_1.TabIndex = 1
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.gridBand3, Me.gridBand2})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_CveDocumento, Me.col1_IdCliente, Me.col1_IdEmpresa, Me.col1_IdSucursal, Me.col1_Ejercicio, Me.col1_Fecha, Me.col1_Tipocartera, Me.col1_Tipodoc, Me.col1_Serie, Me.col1_Numero, Me.col1_Referencia, Me.col1_Fechavence, Me.col1_Cargo, Me.col1_vencido, Me.col1_IdVendedor, Me.col1_Concepto, Me.col1_Moneda, Me.col1_Importe, Me.col1_Iva, Me.col1_Total, Me.col1_Tcambio, Me.col1_Abono, Me.col1_Cvedocto, Me.col1_DiasvencF, Me.col1_Saldodocto, Me.col1_FolioAclaracion, Me.col1_SwSel, Me.col1_Fum, Me.col1_IdUsuario, Me.col1_Feccap, Me.col1_Autonumsuc, Me.col1_Poliza, Me.col1_Fechapoliza, Me.col1_AplContable, Me.col1_FecUltmovto, Me.col1_SaldoF, Me.col1_VencidoF, col1_saldo_acumulado, Me.col1_ImpuestosTrasladados, Me.col1_ImpuestosRetenidos, Me.col1_TotalImpuestos, Me.col1_TasaIva, Me.col1_ImpuestoIva, Me.col1_TasaIeps, Me.col1_ImpuestoIeps, Me.col1_TasaIvaret, Me.col1_ImpuestoIvaret, Me.col1_TasaIsrret, Me.col1_ImpuestoIsrret, Me.col1_TasaOtroimp, Me.col1_ImpuestoOtroimp, Me.col1_IdCcosto, Me.col1_Flujo, Me.Col1_Cargoo})
        Me.GridV_1.CustomizationFormBounds = New System.Drawing.Rectangle(577, 602, 399, 363)
        Me.GridV_1.DetailHeight = 538
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.GroupSummary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "", Nothing, "")})
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Columns.Add(Me.col1_Fecha)
        Me.GridBand1.Columns.Add(Me.col1_IdSucursal)
        Me.GridBand1.Columns.Add(Me.col1_CveDocumento)
        Me.GridBand1.Columns.Add(Me.col1_IdCliente)
        Me.GridBand1.Columns.Add(Me.col1_IdEmpresa)
        Me.GridBand1.Columns.Add(Me.col1_Ejercicio)
        Me.GridBand1.Columns.Add(Me.col1_Tipocartera)
        Me.GridBand1.Columns.Add(Me.col1_Tipodoc)
        Me.GridBand1.Columns.Add(Me.col1_Serie)
        Me.GridBand1.Columns.Add(Me.col1_Numero)
        Me.GridBand1.Columns.Add(Me.col1_Cargo)
        Me.GridBand1.Columns.Add(Me.col1_Abono)
        Me.GridBand1.Columns.Add(Me.col1_Saldodocto)
        Me.GridBand1.Columns.Add(Me.col1_Referencia)
        Me.GridBand1.Columns.Add(Me.col1_Fechavence)
        Me.GridBand1.Columns.Add(Me.col1_vencido)
        Me.GridBand1.Columns.Add(col1_saldo_acumulado)
        Me.GridBand1.Columns.Add(Me.col1_IdVendedor)
        Me.GridBand1.Columns.Add(Me.col1_Concepto)
        Me.GridBand1.Columns.Add(Me.col1_Moneda)
        Me.GridBand1.Columns.Add(Me.col1_Total)
        Me.GridBand1.Columns.Add(Me.col1_Importe)
        Me.GridBand1.Columns.Add(Me.col1_Iva)
        Me.GridBand1.Columns.Add(Me.col1_Tcambio)
        Me.GridBand1.Columns.Add(Me.col1_Cvedocto)
        Me.GridBand1.Columns.Add(Me.col1_FolioAclaracion)
        Me.GridBand1.Columns.Add(Me.col1_Fum)
        Me.GridBand1.Columns.Add(Me.col1_IdUsuario)
        Me.GridBand1.Columns.Add(Me.col1_Feccap)
        Me.GridBand1.Columns.Add(Me.col1_Autonumsuc)
        Me.GridBand1.Columns.Add(Me.col1_Poliza)
        Me.GridBand1.Columns.Add(Me.col1_Fechapoliza)
        Me.GridBand1.Columns.Add(Me.col1_AplContable)
        Me.GridBand1.Columns.Add(Me.col1_FecUltmovto)
        Me.GridBand1.Columns.Add(Me.col1_SaldoF)
        Me.GridBand1.Columns.Add(Me.col1_VencidoF)
        Me.GridBand1.Columns.Add(Me.col1_DiasvencF)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestosTrasladados)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestosRetenidos)
        Me.GridBand1.Columns.Add(Me.col1_TotalImpuestos)
        Me.GridBand1.Columns.Add(Me.col1_TasaIva)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIva)
        Me.GridBand1.Columns.Add(Me.col1_TasaIeps)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIeps)
        Me.GridBand1.Columns.Add(Me.col1_TasaIvaret)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIvaret)
        Me.GridBand1.Columns.Add(Me.col1_TasaIsrret)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIsrret)
        Me.GridBand1.Columns.Add(Me.col1_IdCcosto)
        Me.GridBand1.Columns.Add(Me.col1_Flujo)
        Me.GridBand1.MinWidth = 15
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 1120
        '
        'col1_Fecha
        '
        Me.col1_Fecha.Caption = "fecha"
        Me.col1_Fecha.FieldName = "fecha"
        Me.col1_Fecha.MinWidth = 30
        Me.col1_Fecha.Name = "col1_Fecha"
        Me.col1_Fecha.OptionsColumn.AllowEdit = False
        Me.col1_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fecha.Visible = True
        Me.col1_Fecha.Width = 112
        '
        'col1_IdSucursal
        '
        Me.col1_IdSucursal.Caption = "Sucursal"
        Me.col1_IdSucursal.FieldName = "id_sucursal"
        Me.col1_IdSucursal.MinWidth = 30
        Me.col1_IdSucursal.Name = "col1_IdSucursal"
        Me.col1_IdSucursal.OptionsColumn.AllowEdit = False
        Me.col1_IdSucursal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdSucursal.Visible = True
        Me.col1_IdSucursal.Width = 112
        '
        'col1_CveDocumento
        '
        Me.col1_CveDocumento.Caption = "cve_documento"
        Me.col1_CveDocumento.FieldName = "cve_documento"
        Me.col1_CveDocumento.MinWidth = 30
        Me.col1_CveDocumento.Name = "col1_CveDocumento"
        Me.col1_CveDocumento.OptionsColumn.AllowEdit = False
        Me.col1_CveDocumento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CveDocumento.Visible = True
        Me.col1_CveDocumento.Width = 112
        '
        'col1_IdCliente
        '
        Me.col1_IdCliente.Caption = "IdCliente"
        Me.col1_IdCliente.FieldName = "id_cliente"
        Me.col1_IdCliente.MinWidth = 30
        Me.col1_IdCliente.Name = "col1_IdCliente"
        Me.col1_IdCliente.OptionsColumn.AllowEdit = False
        Me.col1_IdCliente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdCliente.Width = 112
        '
        'col1_IdEmpresa
        '
        Me.col1_IdEmpresa.Caption = "IdEmpresa"
        Me.col1_IdEmpresa.FieldName = "id_empresa"
        Me.col1_IdEmpresa.MinWidth = 30
        Me.col1_IdEmpresa.Name = "col1_IdEmpresa"
        Me.col1_IdEmpresa.OptionsColumn.AllowEdit = False
        Me.col1_IdEmpresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdEmpresa.Width = 112
        '
        'col1_Ejercicio
        '
        Me.col1_Ejercicio.FieldName = "ejercicio"
        Me.col1_Ejercicio.MinWidth = 30
        Me.col1_Ejercicio.Name = "col1_Ejercicio"
        Me.col1_Ejercicio.OptionsColumn.AllowEdit = False
        Me.col1_Ejercicio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Ejercicio.Width = 112
        '
        'col1_Tipocartera
        '
        Me.col1_Tipocartera.Caption = "Tipocartera"
        Me.col1_Tipocartera.FieldName = "tipocartera"
        Me.col1_Tipocartera.MinWidth = 30
        Me.col1_Tipocartera.Name = "col1_Tipocartera"
        Me.col1_Tipocartera.OptionsColumn.AllowEdit = False
        Me.col1_Tipocartera.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipocartera.Width = 112
        '
        'col1_Tipodoc
        '
        Me.col1_Tipodoc.Caption = "Tipodoc"
        Me.col1_Tipodoc.FieldName = "tipodoc"
        Me.col1_Tipodoc.MinWidth = 30
        Me.col1_Tipodoc.Name = "col1_Tipodoc"
        Me.col1_Tipodoc.OptionsColumn.AllowEdit = False
        Me.col1_Tipodoc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipodoc.Width = 112
        '
        'col1_Serie
        '
        Me.col1_Serie.Caption = "Serie"
        Me.col1_Serie.FieldName = "serie"
        Me.col1_Serie.MinWidth = 30
        Me.col1_Serie.Name = "col1_Serie"
        Me.col1_Serie.OptionsColumn.AllowEdit = False
        Me.col1_Serie.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Serie.Width = 112
        '
        'col1_Numero
        '
        Me.col1_Numero.Caption = "Numero"
        Me.col1_Numero.FieldName = "numero"
        Me.col1_Numero.MinWidth = 30
        Me.col1_Numero.Name = "col1_Numero"
        Me.col1_Numero.OptionsColumn.AllowEdit = False
        Me.col1_Numero.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numero.Width = 112
        '
        'col1_Cargo
        '
        Me.col1_Cargo.Caption = "Cargo"
        Me.col1_Cargo.DisplayFormat.FormatString = "C2"
        Me.col1_Cargo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Cargo.FieldName = "cargo"
        Me.col1_Cargo.MinWidth = 30
        Me.col1_Cargo.Name = "col1_Cargo"
        Me.col1_Cargo.OptionsColumn.AllowEdit = False
        Me.col1_Cargo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Cargo.Visible = True
        Me.col1_Cargo.Width = 112
        '
        'col1_Abono
        '
        Me.col1_Abono.Caption = "abono"
        Me.col1_Abono.DisplayFormat.FormatString = "C2"
        Me.col1_Abono.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Abono.FieldName = "abono"
        Me.col1_Abono.MinWidth = 30
        Me.col1_Abono.Name = "col1_Abono"
        Me.col1_Abono.OptionsColumn.AllowEdit = False
        Me.col1_Abono.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Abono.Visible = True
        Me.col1_Abono.Width = 112
        '
        'col1_Saldodocto
        '
        Me.col1_Saldodocto.Caption = "Saldodocto"
        Me.col1_Saldodocto.DisplayFormat.FormatString = "C2"
        Me.col1_Saldodocto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Saldodocto.FieldName = "saldodocto"
        Me.col1_Saldodocto.MinWidth = 30
        Me.col1_Saldodocto.Name = "col1_Saldodocto"
        Me.col1_Saldodocto.OptionsColumn.AllowEdit = False
        Me.col1_Saldodocto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Saldodocto.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "saldodocto", "SUMA={0:C2}")})
        Me.col1_Saldodocto.Visible = True
        Me.col1_Saldodocto.Width = 112
        '
        'col1_Referencia
        '
        Me.col1_Referencia.Caption = "referencia"
        Me.col1_Referencia.FieldName = "referencia"
        Me.col1_Referencia.MinWidth = 30
        Me.col1_Referencia.Name = "col1_Referencia"
        Me.col1_Referencia.OptionsColumn.AllowEdit = False
        Me.col1_Referencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Referencia.Visible = True
        Me.col1_Referencia.Width = 112
        '
        'col1_Fechavence
        '
        Me.col1_Fechavence.FieldName = "fechavence"
        Me.col1_Fechavence.MinWidth = 30
        Me.col1_Fechavence.Name = "col1_Fechavence"
        Me.col1_Fechavence.OptionsColumn.AllowEdit = False
        Me.col1_Fechavence.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fechavence.Visible = True
        Me.col1_Fechavence.Width = 112
        '
        'col1_vencido
        '
        Me.col1_vencido.Caption = "Vencido"
        Me.col1_vencido.DisplayFormat.FormatString = "c2"
        Me.col1_vencido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_vencido.FieldName = "Vencido"
        Me.col1_vencido.MinWidth = 30
        Me.col1_vencido.Name = "col1_vencido"
        Me.col1_vencido.OptionsColumn.AllowEdit = False
        Me.col1_vencido.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_vencido.Visible = True
        Me.col1_vencido.Width = 112
        '
        'col1_IdVendedor
        '
        Me.col1_IdVendedor.Caption = "IdVendedor"
        Me.col1_IdVendedor.FieldName = "id_vendedor"
        Me.col1_IdVendedor.MinWidth = 30
        Me.col1_IdVendedor.Name = "col1_IdVendedor"
        Me.col1_IdVendedor.OptionsColumn.AllowEdit = False
        Me.col1_IdVendedor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdVendedor.Width = 112
        '
        'col1_Concepto
        '
        Me.col1_Concepto.Caption = "Concepto"
        Me.col1_Concepto.FieldName = "concepto"
        Me.col1_Concepto.MinWidth = 30
        Me.col1_Concepto.Name = "col1_Concepto"
        Me.col1_Concepto.OptionsColumn.AllowEdit = False
        Me.col1_Concepto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Concepto.Width = 112
        '
        'col1_Moneda
        '
        Me.col1_Moneda.Caption = "Moneda"
        Me.col1_Moneda.FieldName = "moneda"
        Me.col1_Moneda.MinWidth = 30
        Me.col1_Moneda.Name = "col1_Moneda"
        Me.col1_Moneda.OptionsColumn.AllowEdit = False
        Me.col1_Moneda.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Moneda.Width = 112
        '
        'col1_Total
        '
        Me.col1_Total.Caption = "Total"
        Me.col1_Total.DisplayFormat.FormatString = "C2"
        Me.col1_Total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Total.FieldName = "total"
        Me.col1_Total.MinWidth = 30
        Me.col1_Total.Name = "col1_Total"
        Me.col1_Total.OptionsColumn.AllowEdit = False
        Me.col1_Total.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Total.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Total.Width = 112
        '
        'col1_Importe
        '
        Me.col1_Importe.Caption = "Importe"
        Me.col1_Importe.FieldName = "importe"
        Me.col1_Importe.MinWidth = 30
        Me.col1_Importe.Name = "col1_Importe"
        Me.col1_Importe.OptionsColumn.AllowEdit = False
        Me.col1_Importe.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Importe.Width = 112
        '
        'col1_Iva
        '
        Me.col1_Iva.Caption = "Iva"
        Me.col1_Iva.FieldName = "iva"
        Me.col1_Iva.MinWidth = 30
        Me.col1_Iva.Name = "col1_Iva"
        Me.col1_Iva.OptionsColumn.AllowEdit = False
        Me.col1_Iva.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Iva.Width = 112
        '
        'col1_Tcambio
        '
        Me.col1_Tcambio.Caption = "Tcambio"
        Me.col1_Tcambio.FieldName = "tcambio"
        Me.col1_Tcambio.MinWidth = 30
        Me.col1_Tcambio.Name = "col1_Tcambio"
        Me.col1_Tcambio.OptionsColumn.AllowEdit = False
        Me.col1_Tcambio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tcambio.Width = 112
        '
        'col1_Cvedocto
        '
        Me.col1_Cvedocto.Caption = "Cvedocto"
        Me.col1_Cvedocto.FieldName = "cvedocto"
        Me.col1_Cvedocto.MinWidth = 30
        Me.col1_Cvedocto.Name = "col1_Cvedocto"
        Me.col1_Cvedocto.OptionsColumn.AllowEdit = False
        Me.col1_Cvedocto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Cvedocto.Width = 112
        '
        'col1_FolioAclaracion
        '
        Me.col1_FolioAclaracion.Caption = "FolioAclaracion"
        Me.col1_FolioAclaracion.FieldName = "folio_aclaracion"
        Me.col1_FolioAclaracion.MinWidth = 30
        Me.col1_FolioAclaracion.Name = "col1_FolioAclaracion"
        Me.col1_FolioAclaracion.OptionsColumn.AllowEdit = False
        Me.col1_FolioAclaracion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_FolioAclaracion.Width = 112
        '
        'col1_Fum
        '
        Me.col1_Fum.Caption = "Fum"
        Me.col1_Fum.FieldName = "fum"
        Me.col1_Fum.MinWidth = 30
        Me.col1_Fum.Name = "col1_Fum"
        Me.col1_Fum.OptionsColumn.AllowEdit = False
        Me.col1_Fum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fum.Width = 112
        '
        'col1_IdUsuario
        '
        Me.col1_IdUsuario.Caption = "IdUsuario"
        Me.col1_IdUsuario.FieldName = "id_usuario"
        Me.col1_IdUsuario.MinWidth = 30
        Me.col1_IdUsuario.Name = "col1_IdUsuario"
        Me.col1_IdUsuario.OptionsColumn.AllowEdit = False
        Me.col1_IdUsuario.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdUsuario.Width = 112
        '
        'col1_Feccap
        '
        Me.col1_Feccap.Caption = "Feccap"
        Me.col1_Feccap.FieldName = "feccap"
        Me.col1_Feccap.MinWidth = 30
        Me.col1_Feccap.Name = "col1_Feccap"
        Me.col1_Feccap.OptionsColumn.AllowEdit = False
        Me.col1_Feccap.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Feccap.Width = 112
        '
        'col1_Autonumsuc
        '
        Me.col1_Autonumsuc.Caption = "Autonumsuc"
        Me.col1_Autonumsuc.FieldName = "autonumsuc"
        Me.col1_Autonumsuc.MinWidth = 30
        Me.col1_Autonumsuc.Name = "col1_Autonumsuc"
        Me.col1_Autonumsuc.OptionsColumn.AllowEdit = False
        Me.col1_Autonumsuc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Autonumsuc.Width = 112
        '
        'col1_Poliza
        '
        Me.col1_Poliza.Caption = "Poliza"
        Me.col1_Poliza.FieldName = "poliza"
        Me.col1_Poliza.MinWidth = 30
        Me.col1_Poliza.Name = "col1_Poliza"
        Me.col1_Poliza.OptionsColumn.AllowEdit = False
        Me.col1_Poliza.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Poliza.Width = 112
        '
        'col1_Fechapoliza
        '
        Me.col1_Fechapoliza.Caption = "Fechapoliza"
        Me.col1_Fechapoliza.FieldName = "fechapoliza"
        Me.col1_Fechapoliza.MinWidth = 30
        Me.col1_Fechapoliza.Name = "col1_Fechapoliza"
        Me.col1_Fechapoliza.OptionsColumn.AllowEdit = False
        Me.col1_Fechapoliza.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fechapoliza.Width = 112
        '
        'col1_AplContable
        '
        Me.col1_AplContable.Caption = "AplContable"
        Me.col1_AplContable.FieldName = "apl_contable"
        Me.col1_AplContable.MinWidth = 30
        Me.col1_AplContable.Name = "col1_AplContable"
        Me.col1_AplContable.OptionsColumn.AllowEdit = False
        Me.col1_AplContable.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_AplContable.Width = 112
        '
        'col1_FecUltmovto
        '
        Me.col1_FecUltmovto.Caption = "FecUltmovto"
        Me.col1_FecUltmovto.FieldName = "fec_ultmovto"
        Me.col1_FecUltmovto.MinWidth = 30
        Me.col1_FecUltmovto.Name = "col1_FecUltmovto"
        Me.col1_FecUltmovto.OptionsColumn.AllowEdit = False
        Me.col1_FecUltmovto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_FecUltmovto.Width = 112
        '
        'col1_SaldoF
        '
        Me.col1_SaldoF.Caption = "SaldoF"
        Me.col1_SaldoF.DisplayFormat.FormatString = "C2"
        Me.col1_SaldoF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_SaldoF.FieldName = "saldo_f"
        Me.col1_SaldoF.MinWidth = 30
        Me.col1_SaldoF.Name = "col1_SaldoF"
        Me.col1_SaldoF.OptionsColumn.AllowEdit = False
        Me.col1_SaldoF.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_SaldoF.Width = 112
        '
        'col1_VencidoF
        '
        Me.col1_VencidoF.Caption = "VencidoF"
        Me.col1_VencidoF.FieldName = "vencido_f"
        Me.col1_VencidoF.MinWidth = 30
        Me.col1_VencidoF.Name = "col1_VencidoF"
        Me.col1_VencidoF.OptionsColumn.AllowEdit = False
        Me.col1_VencidoF.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_VencidoF.Width = 112
        '
        'col1_DiasvencF
        '
        Me.col1_DiasvencF.Caption = "DiasvencF"
        Me.col1_DiasvencF.FieldName = "diasvenc_f"
        Me.col1_DiasvencF.MinWidth = 30
        Me.col1_DiasvencF.Name = "col1_DiasvencF"
        Me.col1_DiasvencF.OptionsColumn.AllowEdit = False
        Me.col1_DiasvencF.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_DiasvencF.Width = 112
        '
        'col1_ImpuestosTrasladados
        '
        Me.col1_ImpuestosTrasladados.Caption = "ImpuestosTrasladados"
        Me.col1_ImpuestosTrasladados.FieldName = "impuestos_trasladados"
        Me.col1_ImpuestosTrasladados.MinWidth = 30
        Me.col1_ImpuestosTrasladados.Name = "col1_ImpuestosTrasladados"
        Me.col1_ImpuestosTrasladados.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestosTrasladados.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestosTrasladados.Width = 112
        '
        'col1_ImpuestosRetenidos
        '
        Me.col1_ImpuestosRetenidos.Caption = "ImpuestosRetenidos"
        Me.col1_ImpuestosRetenidos.FieldName = "impuestos_retenidos"
        Me.col1_ImpuestosRetenidos.MinWidth = 30
        Me.col1_ImpuestosRetenidos.Name = "col1_ImpuestosRetenidos"
        Me.col1_ImpuestosRetenidos.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestosRetenidos.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestosRetenidos.Width = 112
        '
        'col1_TotalImpuestos
        '
        Me.col1_TotalImpuestos.Caption = "TotalImpuestos"
        Me.col1_TotalImpuestos.FieldName = "total_impuestos"
        Me.col1_TotalImpuestos.MinWidth = 30
        Me.col1_TotalImpuestos.Name = "col1_TotalImpuestos"
        Me.col1_TotalImpuestos.OptionsColumn.AllowEdit = False
        Me.col1_TotalImpuestos.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TotalImpuestos.Width = 112
        '
        'col1_TasaIva
        '
        Me.col1_TasaIva.Caption = "TasaIva"
        Me.col1_TasaIva.FieldName = "tasa_iva"
        Me.col1_TasaIva.MinWidth = 30
        Me.col1_TasaIva.Name = "col1_TasaIva"
        Me.col1_TasaIva.OptionsColumn.AllowEdit = False
        Me.col1_TasaIva.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TasaIva.Width = 112
        '
        'col1_ImpuestoIva
        '
        Me.col1_ImpuestoIva.Caption = "ImpuestoIva"
        Me.col1_ImpuestoIva.FieldName = "impuesto_iva"
        Me.col1_ImpuestoIva.MinWidth = 30
        Me.col1_ImpuestoIva.Name = "col1_ImpuestoIva"
        Me.col1_ImpuestoIva.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIva.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestoIva.Width = 112
        '
        'col1_TasaIeps
        '
        Me.col1_TasaIeps.Caption = "TasaIeps"
        Me.col1_TasaIeps.FieldName = "tasa_ieps"
        Me.col1_TasaIeps.MinWidth = 30
        Me.col1_TasaIeps.Name = "col1_TasaIeps"
        Me.col1_TasaIeps.OptionsColumn.AllowEdit = False
        Me.col1_TasaIeps.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TasaIeps.Width = 112
        '
        'col1_ImpuestoIeps
        '
        Me.col1_ImpuestoIeps.Caption = "ImpuestoIeps"
        Me.col1_ImpuestoIeps.FieldName = "impuesto_ieps"
        Me.col1_ImpuestoIeps.MinWidth = 30
        Me.col1_ImpuestoIeps.Name = "col1_ImpuestoIeps"
        Me.col1_ImpuestoIeps.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIeps.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestoIeps.Width = 112
        '
        'col1_TasaIvaret
        '
        Me.col1_TasaIvaret.Caption = "TasaIvaret"
        Me.col1_TasaIvaret.FieldName = "tasa_ivaret"
        Me.col1_TasaIvaret.MinWidth = 30
        Me.col1_TasaIvaret.Name = "col1_TasaIvaret"
        Me.col1_TasaIvaret.OptionsColumn.AllowEdit = False
        Me.col1_TasaIvaret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TasaIvaret.Width = 112
        '
        'col1_ImpuestoIvaret
        '
        Me.col1_ImpuestoIvaret.Caption = "ImpuestoIvaret"
        Me.col1_ImpuestoIvaret.FieldName = "impuesto_ivaret"
        Me.col1_ImpuestoIvaret.MinWidth = 30
        Me.col1_ImpuestoIvaret.Name = "col1_ImpuestoIvaret"
        Me.col1_ImpuestoIvaret.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIvaret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestoIvaret.Width = 112
        '
        'col1_TasaIsrret
        '
        Me.col1_TasaIsrret.Caption = "TasaIsrret"
        Me.col1_TasaIsrret.FieldName = "tasa_isrret"
        Me.col1_TasaIsrret.MinWidth = 30
        Me.col1_TasaIsrret.Name = "col1_TasaIsrret"
        Me.col1_TasaIsrret.OptionsColumn.AllowEdit = False
        Me.col1_TasaIsrret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TasaIsrret.Width = 112
        '
        'col1_ImpuestoIsrret
        '
        Me.col1_ImpuestoIsrret.Caption = "ImpuestoIsrret"
        Me.col1_ImpuestoIsrret.FieldName = "impuesto_isrret"
        Me.col1_ImpuestoIsrret.MinWidth = 30
        Me.col1_ImpuestoIsrret.Name = "col1_ImpuestoIsrret"
        Me.col1_ImpuestoIsrret.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIsrret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestoIsrret.Width = 112
        '
        'col1_IdCcosto
        '
        Me.col1_IdCcosto.Caption = "IdCcosto"
        Me.col1_IdCcosto.FieldName = "id_ccosto"
        Me.col1_IdCcosto.MinWidth = 30
        Me.col1_IdCcosto.Name = "col1_IdCcosto"
        Me.col1_IdCcosto.OptionsColumn.AllowEdit = False
        Me.col1_IdCcosto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdCcosto.Width = 112
        '
        'col1_Flujo
        '
        Me.col1_Flujo.Caption = "Flujo"
        Me.col1_Flujo.FieldName = "flujo"
        Me.col1_Flujo.MinWidth = 30
        Me.col1_Flujo.Name = "col1_Flujo"
        Me.col1_Flujo.OptionsColumn.AllowEdit = False
        Me.col1_Flujo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Flujo.Width = 112
        '
        'gridBand3
        '
        Me.gridBand3.AppearanceHeader.Options.UseTextOptions = True
        Me.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridBand3.Caption = "Bonificación"
        Me.gridBand3.Columns.Add(Me.col1_TasaOtroimp)
        Me.gridBand3.Columns.Add(Me.col1_ImpuestoOtroimp)
        Me.gridBand3.MinWidth = 15
        Me.gridBand3.Name = "gridBand3"
        Me.gridBand3.Visible = False
        Me.gridBand3.VisibleIndex = -1
        Me.gridBand3.Width = 112
        '
        'col1_TasaOtroimp
        '
        Me.col1_TasaOtroimp.Caption = "%Boni"
        Me.col1_TasaOtroimp.DisplayFormat.FormatString = "P2"
        Me.col1_TasaOtroimp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_TasaOtroimp.FieldName = "tasa_otroimp"
        Me.col1_TasaOtroimp.MinWidth = 30
        Me.col1_TasaOtroimp.Name = "col1_TasaOtroimp"
        Me.col1_TasaOtroimp.OptionsColumn.AllowEdit = False
        Me.col1_TasaOtroimp.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TasaOtroimp.Width = 112
        '
        'col1_ImpuestoOtroimp
        '
        Me.col1_ImpuestoOtroimp.Caption = "Importe"
        Me.col1_ImpuestoOtroimp.DisplayFormat.FormatString = "C2"
        Me.col1_ImpuestoOtroimp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_ImpuestoOtroimp.FieldName = "impuesto_otroimp"
        Me.col1_ImpuestoOtroimp.MinWidth = 30
        Me.col1_ImpuestoOtroimp.Name = "col1_ImpuestoOtroimp"
        Me.col1_ImpuestoOtroimp.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoOtroimp.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestoOtroimp.Visible = True
        Me.col1_ImpuestoOtroimp.Width = 112
        '
        'gridBand2
        '
        Me.gridBand2.Columns.Add(Me.col1_SwSel)
        Me.gridBand2.MinWidth = 15
        Me.gridBand2.Name = "gridBand2"
        Me.gridBand2.VisibleIndex = 1
        Me.gridBand2.Width = 112
        '
        'col1_SwSel
        '
        Me.col1_SwSel.Caption = "Sel"
        Me.col1_SwSel.FieldName = "sw_sel"
        Me.col1_SwSel.MinWidth = 30
        Me.col1_SwSel.Name = "col1_SwSel"
        Me.col1_SwSel.Width = 112
        '
        'Col1_Cargoo
        '
        Me.Col1_Cargoo.Caption = "Cargo"
        Me.Col1_Cargoo.FieldName = "Cargo"
        Me.Col1_Cargoo.MinWidth = 30
        Me.Col1_Cargoo.Name = "Col1_Cargoo"
        Me.Col1_Cargoo.Visible = True
        Me.Col1_Cargoo.Width = 112
        '
        'lbl_FechaIni
        '
        Me.lbl_FechaIni.AutoSize = True
        Me.lbl_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_FechaIni.Location = New System.Drawing.Point(11, 61)
        Me.lbl_FechaIni.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_FechaIni.Name = "lbl_FechaIni"
        Me.lbl_FechaIni.Size = New System.Drawing.Size(104, 35)
        Me.lbl_FechaIni.TabIndex = 1
        Me.lbl_FechaIni.Text = "Cuenta"
        '
        'txt_Cuenta
        '
        Me.txt_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cuenta.Location = New System.Drawing.Point(173, 62)
        Me.txt_Cuenta.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_Cuenta.Name = "txt_Cuenta"
        Me.txt_Cuenta.Size = New System.Drawing.Size(298, 42)
        Me.txt_Cuenta.TabIndex = 0
        Me.txt_Cuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 123)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 35)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Nombre"
        '
        'txt_Nombre
        '
        Me.txt_Nombre.BackColor = System.Drawing.Color.White
        Me.txt_Nombre.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(170, 120)
        Me.txt_Nombre.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.ReadOnly = True
        Me.txt_Nombre.Size = New System.Drawing.Size(535, 42)
        Me.txt_Nombre.TabIndex = 10
        '
        'btn_BuscarCliente
        '
        Me.btn_BuscarCliente.Location = New System.Drawing.Point(480, 58)
        Me.btn_BuscarCliente.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btn_BuscarCliente.Name = "btn_BuscarCliente"
        Me.btn_BuscarCliente.Size = New System.Drawing.Size(63, 48)
        Me.btn_BuscarCliente.TabIndex = 1
        Me.btn_BuscarCliente.Text = "..."
        Me.btn_BuscarCliente.UseVisualStyleBackColor = True
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.ImageOptions.Image = CType(resources.GetObject("btn_Refrescar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_Refrescar.Location = New System.Drawing.Point(797, 45)
        Me.btn_Refrescar.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(54, 61)
        Me.btn_Refrescar.TabIndex = 2
        '
        'Btn_Cancel
        '
        Me.Btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Btn_Cancel.Location = New System.Drawing.Point(553, 42)
        Me.Btn_Cancel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Btn_Cancel.Name = "Btn_Cancel"
        Me.Btn_Cancel.Size = New System.Drawing.Size(63, 48)
        Me.Btn_Cancel.TabIndex = 20
        Me.Btn_Cancel.Text = "..."
        Me.Btn_Cancel.UseVisualStyleBackColor = True
        Me.Btn_Cancel.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Location = New System.Drawing.Point(456, 11)
        Me.txt_TipoCliente.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(19, 40)
        Me.txt_TipoCliente.TabIndex = 36
        Me.txt_TipoCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_TipoCliente.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion2.Location = New System.Drawing.Point(424, 11)
        Me.txt_Direccion2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(19, 40)
        Me.txt_Direccion2.TabIndex = 35
        Me.txt_Direccion2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion2.Visible = False
        '
        'txt_Empresa1
        '
        Me.txt_Empresa1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa1.Location = New System.Drawing.Point(483, 11)
        Me.txt_Empresa1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_Empresa1.Name = "txt_Empresa1"
        Me.txt_Empresa1.Size = New System.Drawing.Size(19, 40)
        Me.txt_Empresa1.TabIndex = 38
        Me.txt_Empresa1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Empresa1.Visible = False
        '
        'txt_Caja
        '
        Me.txt_Caja.Location = New System.Drawing.Point(652, 15)
        Me.txt_Caja.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(43, 26)
        Me.txt_Caja.TabIndex = 20
        Me.txt_Caja.Text = "0001"
        Me.txt_Caja.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Location = New System.Drawing.Point(706, 15)
        Me.txt_Cajero.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(43, 26)
        Me.txt_Cajero.TabIndex = 21
        Me.txt_Cajero.Text = "0001"
        Me.txt_Cajero.Visible = False
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion.Location = New System.Drawing.Point(396, 11)
        Me.txt_Direccion.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(19, 40)
        Me.txt_Direccion.TabIndex = 34
        Me.txt_Direccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion.Visible = False
        '
        'txt_FormatoTicket
        '
        Me.txt_FormatoTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FormatoTicket.Location = New System.Drawing.Point(512, 11)
        Me.txt_FormatoTicket.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_FormatoTicket.Name = "txt_FormatoTicket"
        Me.txt_FormatoTicket.Size = New System.Drawing.Size(19, 40)
        Me.txt_FormatoTicket.TabIndex = 39
        Me.txt_FormatoTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_FormatoTicket.Visible = False
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(405, 57)
        Me.lstImpresoras.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(367, 28)
        Me.lstImpresoras.TabIndex = 22
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(405, 8)
        Me.Label31.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(155, 35)
        Me.Label31.TabIndex = 23
        Me.Label31.Text = "Impresora:"
        '
        'txt_MargenesTicket
        '
        Me.txt_MargenesTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_MargenesTicket.Location = New System.Drawing.Point(609, 11)
        Me.txt_MargenesTicket.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_MargenesTicket.Name = "txt_MargenesTicket"
        Me.txt_MargenesTicket.Size = New System.Drawing.Size(19, 40)
        Me.txt_MargenesTicket.TabIndex = 37
        Me.txt_MargenesTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_MargenesTicket.Visible = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(202, 8)
        Me.Label36.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(131, 35)
        Me.Label36.TabIndex = 41
        Me.Label36.Text = "Sucursal:"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txt_pvence6)
        Me.GroupControl1.Controls.Add(Me.txt_pvence5)
        Me.GroupControl1.Controls.Add(Me.txt_pvence4)
        Me.GroupControl1.Controls.Add(Me.txt_pvence3)
        Me.GroupControl1.Controls.Add(Me.txt_pvence2)
        Me.GroupControl1.Controls.Add(Me.txt_pvence1)
        Me.GroupControl1.Controls.Add(Me.GroupBox2)
        Me.GroupControl1.Controls.Add(Me.GroupBox1)
        Me.GroupControl1.Controls.Add(Me.WindowsUIButtonPanel1)
        Me.GroupControl1.Controls.Add(Me.Btn_Cancel)
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.btn_BuscarCliente)
        Me.GroupControl1.Controls.Add(Me.txt_Nombre)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txt_Cuenta)
        Me.GroupControl1.Controls.Add(Me.lbl_FechaIni)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl1.Location = New System.Drawing.Point(0, 269)
        Me.GroupControl1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1389, 449)
        Me.GroupControl1.TabIndex = 2
        '
        'txt_pvence6
        '
        Me.txt_pvence6.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pvence6.Location = New System.Drawing.Point(1292, 66)
        Me.txt_pvence6.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_pvence6.Name = "txt_pvence6"
        Me.txt_pvence6.ReadOnly = True
        Me.txt_pvence6.Size = New System.Drawing.Size(78, 41)
        Me.txt_pvence6.TabIndex = 66
        Me.txt_pvence6.Visible = False
        '
        'txt_pvence5
        '
        Me.txt_pvence5.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pvence5.Location = New System.Drawing.Point(1292, 55)
        Me.txt_pvence5.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_pvence5.Name = "txt_pvence5"
        Me.txt_pvence5.ReadOnly = True
        Me.txt_pvence5.Size = New System.Drawing.Size(78, 41)
        Me.txt_pvence5.TabIndex = 65
        Me.txt_pvence5.Visible = False
        '
        'txt_pvence4
        '
        Me.txt_pvence4.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pvence4.Location = New System.Drawing.Point(1292, 58)
        Me.txt_pvence4.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_pvence4.Name = "txt_pvence4"
        Me.txt_pvence4.ReadOnly = True
        Me.txt_pvence4.Size = New System.Drawing.Size(78, 41)
        Me.txt_pvence4.TabIndex = 64
        Me.txt_pvence4.Visible = False
        '
        'txt_pvence3
        '
        Me.txt_pvence3.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pvence3.Location = New System.Drawing.Point(1292, 59)
        Me.txt_pvence3.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_pvence3.Name = "txt_pvence3"
        Me.txt_pvence3.ReadOnly = True
        Me.txt_pvence3.Size = New System.Drawing.Size(78, 41)
        Me.txt_pvence3.TabIndex = 63
        Me.txt_pvence3.Visible = False
        '
        'txt_pvence2
        '
        Me.txt_pvence2.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pvence2.Location = New System.Drawing.Point(1292, 59)
        Me.txt_pvence2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_pvence2.Name = "txt_pvence2"
        Me.txt_pvence2.ReadOnly = True
        Me.txt_pvence2.Size = New System.Drawing.Size(78, 41)
        Me.txt_pvence2.TabIndex = 62
        Me.txt_pvence2.Visible = False
        '
        'txt_pvence1
        '
        Me.txt_pvence1.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pvence1.Location = New System.Drawing.Point(1292, 55)
        Me.txt_pvence1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_pvence1.Name = "txt_pvence1"
        Me.txt_pvence1.ReadOnly = True
        Me.txt_pvence1.Size = New System.Drawing.Size(78, 41)
        Me.txt_pvence1.TabIndex = 42
        Me.txt_pvence1.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txt_vence6)
        Me.GroupBox2.Controls.Add(Me.txt_vence5)
        Me.GroupBox2.Controls.Add(Me.txt_vence4)
        Me.GroupBox2.Controls.Add(Me.txt_vence3)
        Me.GroupBox2.Controls.Add(Me.txt_vence2)
        Me.GroupBox2.Controls.Add(Me.txt_vence1)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(676, 223)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(707, 102)
        Me.GroupBox2.TabIndex = 61
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Desgloce Vencido"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(599, 77)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 22)
        Me.Label12.TabIndex = 70
        Me.Label12.Text = "+1Año"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(479, 80)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(67, 22)
        Me.Label11.TabIndex = 69
        Me.Label11.Text = "91-365"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(367, 80)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 22)
        Me.Label10.TabIndex = 68
        Me.Label10.Text = "61-90"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(254, 79)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 22)
        Me.Label9.TabIndex = 67
        Me.Label9.Text = "31-60"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(144, 77)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 22)
        Me.Label8.TabIndex = 66
        Me.Label8.Text = "16-30"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(33, 77)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 22)
        Me.Label7.TabIndex = 60
        Me.Label7.Text = "01-15"
        '
        'txt_vence6
        '
        Me.txt_vence6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt_vence6.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vence6.Location = New System.Drawing.Point(578, 41)
        Me.txt_vence6.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_vence6.Name = "txt_vence6"
        Me.txt_vence6.Size = New System.Drawing.Size(103, 27)
        Me.txt_vence6.TabIndex = 65
        Me.txt_vence6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_vence5
        '
        Me.txt_vence5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt_vence5.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vence5.Location = New System.Drawing.Point(459, 42)
        Me.txt_vence5.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_vence5.Name = "txt_vence5"
        Me.txt_vence5.Size = New System.Drawing.Size(103, 27)
        Me.txt_vence5.TabIndex = 64
        Me.txt_vence5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_vence4
        '
        Me.txt_vence4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt_vence4.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vence4.Location = New System.Drawing.Point(345, 43)
        Me.txt_vence4.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_vence4.Name = "txt_vence4"
        Me.txt_vence4.Size = New System.Drawing.Size(103, 27)
        Me.txt_vence4.TabIndex = 63
        Me.txt_vence4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_vence3
        '
        Me.txt_vence3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt_vence3.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vence3.Location = New System.Drawing.Point(233, 42)
        Me.txt_vence3.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_vence3.Name = "txt_vence3"
        Me.txt_vence3.Size = New System.Drawing.Size(103, 27)
        Me.txt_vence3.TabIndex = 62
        Me.txt_vence3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_vence2
        '
        Me.txt_vence2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt_vence2.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vence2.Location = New System.Drawing.Point(121, 42)
        Me.txt_vence2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_vence2.Name = "txt_vence2"
        Me.txt_vence2.Size = New System.Drawing.Size(103, 27)
        Me.txt_vence2.TabIndex = 61
        Me.txt_vence2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_vence1
        '
        Me.txt_vence1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt_vence1.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vence1.Location = New System.Drawing.Point(11, 42)
        Me.txt_vence1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_vence1.Name = "txt_vence1"
        Me.txt_vence1.Size = New System.Drawing.Size(103, 27)
        Me.txt_vence1.TabIndex = 60
        Me.txt_vence1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Txt_anticipo)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txt_vencido)
        Me.GroupBox1.Controls.Add(Me.txt_sfinal)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 213)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(639, 102)
        Me.GroupBox1.TabIndex = 60
        Me.GroupBox1.TabStop = False
        '
        'Txt_anticipo
        '
        Me.Txt_anticipo.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Txt_anticipo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_anticipo.Location = New System.Drawing.Point(472, 52)
        Me.Txt_anticipo.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Txt_anticipo.Name = "Txt_anticipo"
        Me.Txt_anticipo.Size = New System.Drawing.Size(141, 29)
        Me.Txt_anticipo.TabIndex = 59
        Me.Txt_anticipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(479, 22)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(106, 27)
        Me.Label6.TabIndex = 58
        Me.Label6.Text = "Anticipos:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(59, 23)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(117, 27)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "Saldo Final"
        '
        'txt_vencido
        '
        Me.txt_vencido.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt_vencido.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vencido.Location = New System.Drawing.Point(255, 52)
        Me.txt_vencido.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_vencido.Name = "txt_vencido"
        Me.txt_vencido.Size = New System.Drawing.Size(173, 29)
        Me.txt_vencido.TabIndex = 57
        Me.txt_vencido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sfinal
        '
        Me.txt_sfinal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt_sfinal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_sfinal.Location = New System.Drawing.Point(48, 52)
        Me.txt_sfinal.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_sfinal.Name = "txt_sfinal"
        Me.txt_sfinal.Size = New System.Drawing.Size(174, 29)
        Me.txt_sfinal.TabIndex = 55
        Me.txt_sfinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(276, 23)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 27)
        Me.Label5.TabIndex = 56
        Me.Label5.Text = "Vencido:"
        '
        'WindowsUIButtonPanel1
        '
        Me.WindowsUIButtonPanel1.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.WindowsUIButtonPanel1.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.WindowsUIButtonPanel1.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.WindowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.WindowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseFont = True
        Me.WindowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.WindowsUIButtonPanel1.AppearanceButton.Normal.FontSizeDelta = -1
        Me.WindowsUIButtonPanel1.AppearanceButton.Normal.Options.UseFont = True
        Me.WindowsUIButtonPanel1.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.WindowsUIButtonPanel1.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.WindowsUIButtonPanel1.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.WindowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.WindowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseFont = True
        Me.WindowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.WindowsUIButtonPanel1.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.Image = CType(resources.GetObject("WindowsUIButtonImageOptions1.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions2.Image = CType(resources.GetObject("WindowsUIButtonImageOptions2.Image"), System.Drawing.Image)
        Me.WindowsUIButtonPanel1.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Imprimir", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Salir", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, Nothing, -1, False)})
        Me.WindowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.WindowsUIButtonPanel1.Controls.Add(Me.Label1)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.txt_sucursal)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox2)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.Label3)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.ComboBox1)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox3)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox4)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox5)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox6)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox7)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox8)
        Me.WindowsUIButtonPanel1.Controls.Add(Me.TextBox9)
        Me.WindowsUIButtonPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.WindowsUIButtonPanel1.EnableImageTransparency = True
        Me.WindowsUIButtonPanel1.ForeColor = System.Drawing.Color.White
        Me.WindowsUIButtonPanel1.Location = New System.Drawing.Point(2, 347)
        Me.WindowsUIButtonPanel1.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.WindowsUIButtonPanel1.MaximumSize = New System.Drawing.Size(0, 100)
        Me.WindowsUIButtonPanel1.MinimumSize = New System.Drawing.Size(98, 100)
        Me.WindowsUIButtonPanel1.Name = "WindowsUIButtonPanel1"
        Me.WindowsUIButtonPanel1.Size = New System.Drawing.Size(1385, 100)
        Me.WindowsUIButtonPanel1.TabIndex = 41
        Me.WindowsUIButtonPanel1.Text = "windowsUIButtonPanel"
        Me.WindowsUIButtonPanel1.UseButtonBackgroundImages = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(160, 9)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 35)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "Sucursal:"
        '
        'txt_sucursal
        '
        Me.txt_sucursal.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_sucursal.Location = New System.Drawing.Point(149, 49)
        Me.txt_sucursal.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txt_sucursal.Name = "txt_sucursal"
        Me.txt_sucursal.ReadOnly = True
        Me.txt_sucursal.Size = New System.Drawing.Size(213, 41)
        Me.txt_sucursal.TabIndex = 40
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(609, 11)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(19, 40)
        Me.TextBox2.TabIndex = 37
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox2.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(405, 8)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(155, 35)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Impresora:"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(405, 57)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(367, 27)
        Me.ComboBox1.TabIndex = 22
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(512, 11)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(19, 40)
        Me.TextBox3.TabIndex = 39
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox3.Visible = False
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(396, 11)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(19, 40)
        Me.TextBox4.TabIndex = 34
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox4.Visible = False
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(706, 15)
        Me.TextBox5.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(43, 27)
        Me.TextBox5.TabIndex = 21
        Me.TextBox5.Text = "0001"
        Me.TextBox5.Visible = False
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(652, 15)
        Me.TextBox6.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(43, 27)
        Me.TextBox6.TabIndex = 20
        Me.TextBox6.Text = "0001"
        Me.TextBox6.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(483, 11)
        Me.TextBox7.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(19, 40)
        Me.TextBox7.TabIndex = 38
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox7.Visible = False
        '
        'TextBox8
        '
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(424, 11)
        Me.TextBox8.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(19, 40)
        Me.TextBox8.TabIndex = 35
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox8.Visible = False
        '
        'TextBox9
        '
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(456, 11)
        Me.TextBox9.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(19, 40)
        Me.TextBox9.TabIndex = 36
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox9.Visible = False
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions3.Image = CType(resources.GetObject("WindowsUIButtonImageOptions3.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions3.ImageUri.Uri = "Apply"
        WindowsUIButtonImageOptions4.Image = CType(resources.GetObject("WindowsUIButtonImageOptions4.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions5.Image = CType(resources.GetObject("WindowsUIButtonImageOptions5.Image"), System.Drawing.Image)
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Terminar", True, WindowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Terminar", -1, True), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", True, WindowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Cancelar", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Nuevo", True, WindowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Nuevo", -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label36)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_MargenesTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label31)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lstImpresoras)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_FormatoTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Cajero)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Caja)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Empresa1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_TipoCliente)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(2, 347)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 100)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(98, 100)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(1585, 100)
        Me.PanelFiltros_Acciones.TabIndex = 41
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.Cg_ayuda})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'Cg_ayuda
        '
        Me.Cg_ayuda.Controls.Add(Me.DockPanel1_Container)
        Me.Cg_ayuda.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.Cg_ayuda.ID = New System.Guid("42c8b141-b4f3-481a-829c-3048319e5244")
        Me.Cg_ayuda.Location = New System.Drawing.Point(1389, 0)
        Me.Cg_ayuda.Name = "Cg_ayuda"
        Me.Cg_ayuda.OriginalSize = New System.Drawing.Size(200, 200)
        Me.Cg_ayuda.Size = New System.Drawing.Size(200, 718)
        Me.Cg_ayuda.Text = "AYUDA - TECLAS RAPIDAS"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.GridC_2)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(7, 38)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(189, 676)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'GridC_2
        '
        Me.GridC_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_2.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.GridC_2.Location = New System.Drawing.Point(0, 0)
        Me.GridC_2.MainView = Me.GridV_2
        Me.GridC_2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.GridC_2.Name = "GridC_2"
        Me.GridC_2.Size = New System.Drawing.Size(189, 676)
        Me.GridC_2.TabIndex = 12
        Me.GridC_2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_2})
        '
        'GridV_2
        '
        Me.GridV_2.ColumnPanelRowHeight = 0
        Me.GridV_2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col2_Accion, Me.col2_Combinacion})
        Me.GridV_2.DetailHeight = 538
        Me.GridV_2.FixedLineWidth = 3
        Me.GridV_2.FooterPanelHeight = 0
        Me.GridV_2.GridControl = Me.GridC_2
        Me.GridV_2.GroupRowHeight = 0
        Me.GridV_2.LevelIndent = 0
        Me.GridV_2.Name = "GridV_2"
        Me.GridV_2.OptionsView.ShowGroupPanel = False
        Me.GridV_2.PreviewIndent = 0
        Me.GridV_2.RowHeight = 0
        Me.GridV_2.ViewCaptionHeight = 0
        '
        'col2_Accion
        '
        Me.col2_Accion.Caption = "Acción"
        Me.col2_Accion.FieldName = "Accion"
        Me.col2_Accion.MinWidth = 30
        Me.col2_Accion.Name = "col2_Accion"
        Me.col2_Accion.OptionsColumn.AllowEdit = False
        Me.col2_Accion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Accion.Visible = True
        Me.col2_Accion.VisibleIndex = 0
        Me.col2_Accion.Width = 112
        '
        'col2_Combinacion
        '
        Me.col2_Combinacion.Caption = "Combinación"
        Me.col2_Combinacion.FieldName = "Combinacion"
        Me.col2_Combinacion.MinWidth = 30
        Me.col2_Combinacion.Name = "col2_Combinacion"
        Me.col2_Combinacion.OptionsColumn.AllowEdit = False
        Me.col2_Combinacion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Combinacion.Visible = True
        Me.col2_Combinacion.VisibleIndex = 1
        Me.col2_Combinacion.Width = 112
        '
        'pdvCliEdoCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1589, 718)
        Me.Controls.Add(Me.GridC_1)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.Cg_ayuda)
        Me.Name = "pdvCliEdoCuenta"
        Me.Text = "pdvCliEdoCuenta"
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.WindowsUIButtonPanel1.ResumeLayout(False)
        Me.WindowsUIButtonPanel1.PerformLayout()
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Cg_ayuda.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents col1_CveDocumento As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdCliente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdEmpresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdSucursal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Ejercicio As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipocartera As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipodoc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Serie As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numero As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Referencia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fechavence As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_vencido As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdVendedor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Concepto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Moneda As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Total As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Importe As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Iva As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tcambio As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cargo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Abono As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cvedocto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_FolioAclaracion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fum As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdUsuario As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Feccap As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Autonumsuc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Poliza As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fechapoliza As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_AplContable As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_FecUltmovto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_SaldoF As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_VencidoF As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_DiasvencF As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestosTrasladados As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestosRetenidos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TotalImpuestos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIva As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIva As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIeps As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIeps As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIvaret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIvaret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIsrret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIsrret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdCcosto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Flujo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Saldodocto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_SwSel As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaOtroimp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoOtroimp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents lbl_FechaIni As Label
    Friend WithEvents txt_Cuenta As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents btn_BuscarCliente As Button
    Friend WithEvents btn_Refrescar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Btn_Cancel As Button
    Private WithEvents txt_TipoCliente As TextBox
    Private WithEvents txt_Direccion2 As TextBox
    Private WithEvents txt_Empresa1 As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents txt_Cajero As TextBox
    Private WithEvents txt_Direccion As TextBox
    Private WithEvents txt_FormatoTicket As TextBox
    Friend WithEvents lstImpresoras As ComboBox
    Friend WithEvents Label31 As Label
    Private WithEvents txt_MargenesTicket As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Private WithEvents WindowsUIButtonPanel1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_sucursal As TextBox
    Private WithEvents TextBox2 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ComboBox1 As ComboBox
    Private WithEvents TextBox3 As TextBox
    Private WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Private WithEvents TextBox7 As TextBox
    Private WithEvents TextBox8 As TextBox
    Private WithEvents TextBox9 As TextBox
    Friend WithEvents Col1_Cargoo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand3 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents Cg_ayuda As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents GridC_2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col2_Accion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Combinacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label4 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Txt_anticipo As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_vencido As TextBox
    Friend WithEvents txt_sfinal As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txt_vence5 As TextBox
    Friend WithEvents txt_vence4 As TextBox
    Friend WithEvents txt_vence3 As TextBox
    Friend WithEvents txt_vence2 As TextBox
    Friend WithEvents txt_vence1 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
    Friend WithEvents txt_vence6 As TextBox
    Friend WithEvents txt_pvence6 As TextBox
    Friend WithEvents txt_pvence5 As TextBox
    Friend WithEvents txt_pvence4 As TextBox
    Friend WithEvents txt_pvence3 As TextBox
    Friend WithEvents txt_pvence2 As TextBox
    Friend WithEvents txt_pvence1 As TextBox
End Class
