﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvRecibirPagos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvRecibirPagos))
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions3 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txt_Sucursal = New System.Windows.Forms.TextBox()
        Me.txt_MargenesTicket = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.txt_FormatoTicket = New System.Windows.Forms.TextBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.txt_Empresa1 = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.pop_Menu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.pop_Opc1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.pop_Opc2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.pop_Opc3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_CveDocumento = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdCliente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdEmpresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdSucursal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Ejercicio = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipocartera = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipodoc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Serie = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numero = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Referencia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fechavence = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fechapago = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdVendedor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Concepto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Moneda = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Total = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Importe = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Iva = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tcambio = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cargo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Abono = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Cvedocto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_FolioAclaracion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fum = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdUsuario = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Feccap = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Autonumsuc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Poliza = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fechapoliza = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_AplContable = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_FecUltmovto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_SaldoF = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_VencidoF = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_DiasvencF = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestosTrasladados = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestosRetenidos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TotalImpuestos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIva = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIva = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIeps = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIeps = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIvaret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIvaret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_TasaIsrret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoIsrret = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdCcosto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Flujo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Saldodocto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_SwSel = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.gridBand3 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_TasaOtroimp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImpuestoOtroimp = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Txt_CodigoBonifciacion = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblFormaPago = New System.Windows.Forms.Label()
        Me.cbo_FormaPago = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtp_Fecha = New System.Windows.Forms.DateTimePicker()
        Me.Btn_Cancel = New System.Windows.Forms.Button()
        Me.btn_Refrescar = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_TotalAPagar = New DevExpress.XtraEditors.TextEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_Total = New DevExpress.XtraEditors.TextEdit()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.btn_BuscarCliente = New System.Windows.Forms.Button()
        Me.txt_Estatus = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Tarjeta = New System.Windows.Forms.TextBox()
        Me.txt_Cuenta = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_FechaIni = New System.Windows.Forms.Label()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.hideContainerRight = New DevExpress.XtraBars.Docking.AutoHideContainer()
        Me.CG_Ayuda = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.GridC_2 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col2_Accion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Combinacion = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.PanelFiltros_Acciones.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pop_Menu.SuspendLayout()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txt_TotalAPagar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hideContainerRight.SuspendLayout()
        Me.CG_Ayuda.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.PanelFiltros_Acciones)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl3.Location = New System.Drawing.Point(0, 612)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(809, 87)
        Me.GroupControl3.TabIndex = 2
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.Image = CType(resources.GetObject("WindowsUIButtonImageOptions1.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions1.ImageUri.Uri = "Apply"
        WindowsUIButtonImageOptions2.Image = CType(resources.GetObject("WindowsUIButtonImageOptions2.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions3.Image = CType(resources.GetObject("WindowsUIButtonImageOptions3.Image"), System.Drawing.Image)
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Terminar", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Terminar", -1, True), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Cancelar", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Nuevo", True, WindowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Nuevo", -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label36)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Sucursal)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_MargenesTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label31)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lstImpresoras)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_FormatoTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Cajero)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Caja)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Empresa1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_TipoCliente)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(2, 20)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 65)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(65, 65)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(805, 65)
        Me.PanelFiltros_Acciones.TabIndex = 40
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(135, 5)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(88, 23)
        Me.Label36.TabIndex = 41
        Me.Label36.Text = "Sucursal:"
        '
        'txt_Sucursal
        '
        Me.txt_Sucursal.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Sucursal.Location = New System.Drawing.Point(135, 32)
        Me.txt_Sucursal.Name = "txt_Sucursal"
        Me.txt_Sucursal.ReadOnly = True
        Me.txt_Sucursal.Size = New System.Drawing.Size(130, 30)
        Me.txt_Sucursal.TabIndex = 40
        '
        'txt_MargenesTicket
        '
        Me.txt_MargenesTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_MargenesTicket.Location = New System.Drawing.Point(406, 7)
        Me.txt_MargenesTicket.Name = "txt_MargenesTicket"
        Me.txt_MargenesTicket.Size = New System.Drawing.Size(14, 29)
        Me.txt_MargenesTicket.TabIndex = 37
        Me.txt_MargenesTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_MargenesTicket.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(270, 5)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(103, 23)
        Me.Label31.TabIndex = 23
        Me.Label31.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(270, 37)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(246, 21)
        Me.lstImpresoras.TabIndex = 22
        '
        'txt_FormatoTicket
        '
        Me.txt_FormatoTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FormatoTicket.Location = New System.Drawing.Point(341, 7)
        Me.txt_FormatoTicket.Name = "txt_FormatoTicket"
        Me.txt_FormatoTicket.Size = New System.Drawing.Size(14, 29)
        Me.txt_FormatoTicket.TabIndex = 39
        Me.txt_FormatoTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_FormatoTicket.Visible = False
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion.Location = New System.Drawing.Point(264, 7)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion.TabIndex = 34
        Me.txt_Direccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Location = New System.Drawing.Point(471, 10)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(30, 21)
        Me.txt_Cajero.TabIndex = 21
        Me.txt_Cajero.Text = "0001"
        Me.txt_Cajero.Visible = False
        '
        'txt_Caja
        '
        Me.txt_Caja.Location = New System.Drawing.Point(435, 10)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(30, 21)
        Me.txt_Caja.TabIndex = 20
        Me.txt_Caja.Text = "0001"
        Me.txt_Caja.Visible = False
        '
        'txt_Empresa1
        '
        Me.txt_Empresa1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa1.Location = New System.Drawing.Point(322, 7)
        Me.txt_Empresa1.Name = "txt_Empresa1"
        Me.txt_Empresa1.Size = New System.Drawing.Size(14, 29)
        Me.txt_Empresa1.TabIndex = 38
        Me.txt_Empresa1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Empresa1.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion2.Location = New System.Drawing.Point(283, 7)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion2.TabIndex = 35
        Me.txt_Direccion2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion2.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Location = New System.Drawing.Point(304, 7)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(14, 29)
        Me.txt_TipoCliente.TabIndex = 36
        Me.txt_TipoCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_TipoCliente.Visible = False
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(788, 340)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "DOCUMENTOS PENDIENTES"
        '
        'GridC_1
        '
        Me.GridC_1.ContextMenuStrip = Me.pop_Menu
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(784, 315)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'pop_Menu
        '
        Me.pop_Menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.pop_Opc1, Me.pop_Opc2, Me.pop_Opc3})
        Me.pop_Menu.Name = "pop_Menu"
        Me.pop_Menu.Size = New System.Drawing.Size(186, 70)
        '
        'pop_Opc1
        '
        Me.pop_Opc1.Name = "pop_Opc1"
        Me.pop_Opc1.Size = New System.Drawing.Size(185, 22)
        Me.pop_Opc1.Text = "Selecionar hasta aquí"
        '
        'pop_Opc2
        '
        Me.pop_Opc2.Name = "pop_Opc2"
        Me.pop_Opc2.Size = New System.Drawing.Size(185, 22)
        Me.pop_Opc2.Text = "Seleccionar Todo"
        '
        'pop_Opc3
        '
        Me.pop_Opc3.Name = "pop_Opc3"
        Me.pop_Opc3.Size = New System.Drawing.Size(185, 22)
        Me.pop_Opc3.Text = "Invertir Selección"
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1, Me.gridBand2, Me.gridBand3})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_SwSel, Me.col1_CveDocumento, Me.col1_IdCliente, Me.col1_IdEmpresa, Me.col1_IdSucursal, Me.col1_Ejercicio, Me.col1_Fecha, Me.col1_Tipocartera, Me.col1_Tipodoc, Me.col1_Serie, Me.col1_Numero, Me.col1_Referencia, Me.col1_Fechavence, Me.col1_Fechapago, Me.col1_IdVendedor, Me.col1_Concepto, Me.col1_Moneda, Me.col1_Importe, Me.col1_Iva, Me.col1_Total, Me.col1_Tcambio, Me.col1_Cargo, Me.col1_Abono, Me.col1_Cvedocto, Me.col1_Saldodocto, Me.col1_FolioAclaracion, Me.col1_Fum, Me.col1_IdUsuario, Me.col1_Feccap, Me.col1_Autonumsuc, Me.col1_Poliza, Me.col1_Fechapoliza, Me.col1_AplContable, Me.col1_FecUltmovto, Me.col1_SaldoF, Me.col1_VencidoF, Me.col1_DiasvencF, Me.col1_ImpuestosTrasladados, Me.col1_ImpuestosRetenidos, Me.col1_TotalImpuestos, Me.col1_TasaIva, Me.col1_ImpuestoIva, Me.col1_TasaIeps, Me.col1_ImpuestoIeps, Me.col1_TasaIvaret, Me.col1_ImpuestoIvaret, Me.col1_TasaIsrret, Me.col1_ImpuestoIsrret, Me.col1_TasaOtroimp, Me.col1_ImpuestoOtroimp, Me.col1_IdCcosto, Me.col1_Flujo})
        Me.GridV_1.CustomizationFormBounds = New System.Drawing.Rectangle(577, 602, 266, 236)
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Columns.Add(Me.col1_CveDocumento)
        Me.GridBand1.Columns.Add(Me.col1_IdCliente)
        Me.GridBand1.Columns.Add(Me.col1_IdEmpresa)
        Me.GridBand1.Columns.Add(Me.col1_IdSucursal)
        Me.GridBand1.Columns.Add(Me.col1_Ejercicio)
        Me.GridBand1.Columns.Add(Me.col1_Fecha)
        Me.GridBand1.Columns.Add(Me.col1_Tipocartera)
        Me.GridBand1.Columns.Add(Me.col1_Tipodoc)
        Me.GridBand1.Columns.Add(Me.col1_Serie)
        Me.GridBand1.Columns.Add(Me.col1_Numero)
        Me.GridBand1.Columns.Add(Me.col1_Referencia)
        Me.GridBand1.Columns.Add(Me.col1_Fechavence)
        Me.GridBand1.Columns.Add(Me.col1_Fechapago)
        Me.GridBand1.Columns.Add(Me.col1_IdVendedor)
        Me.GridBand1.Columns.Add(Me.col1_Concepto)
        Me.GridBand1.Columns.Add(Me.col1_Moneda)
        Me.GridBand1.Columns.Add(Me.col1_Total)
        Me.GridBand1.Columns.Add(Me.col1_Importe)
        Me.GridBand1.Columns.Add(Me.col1_Iva)
        Me.GridBand1.Columns.Add(Me.col1_Tcambio)
        Me.GridBand1.Columns.Add(Me.col1_Cargo)
        Me.GridBand1.Columns.Add(Me.col1_Abono)
        Me.GridBand1.Columns.Add(Me.col1_Cvedocto)
        Me.GridBand1.Columns.Add(Me.col1_FolioAclaracion)
        Me.GridBand1.Columns.Add(Me.col1_Fum)
        Me.GridBand1.Columns.Add(Me.col1_IdUsuario)
        Me.GridBand1.Columns.Add(Me.col1_Feccap)
        Me.GridBand1.Columns.Add(Me.col1_Autonumsuc)
        Me.GridBand1.Columns.Add(Me.col1_Poliza)
        Me.GridBand1.Columns.Add(Me.col1_Fechapoliza)
        Me.GridBand1.Columns.Add(Me.col1_AplContable)
        Me.GridBand1.Columns.Add(Me.col1_FecUltmovto)
        Me.GridBand1.Columns.Add(Me.col1_SaldoF)
        Me.GridBand1.Columns.Add(Me.col1_VencidoF)
        Me.GridBand1.Columns.Add(Me.col1_DiasvencF)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestosTrasladados)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestosRetenidos)
        Me.GridBand1.Columns.Add(Me.col1_TotalImpuestos)
        Me.GridBand1.Columns.Add(Me.col1_TasaIva)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIva)
        Me.GridBand1.Columns.Add(Me.col1_TasaIeps)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIeps)
        Me.GridBand1.Columns.Add(Me.col1_TasaIvaret)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIvaret)
        Me.GridBand1.Columns.Add(Me.col1_TasaIsrret)
        Me.GridBand1.Columns.Add(Me.col1_ImpuestoIsrret)
        Me.GridBand1.Columns.Add(Me.col1_IdCcosto)
        Me.GridBand1.Columns.Add(Me.col1_Flujo)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 750
        '
        'col1_CveDocumento
        '
        Me.col1_CveDocumento.Caption = "CveDocumento"
        Me.col1_CveDocumento.FieldName = "cve_documento"
        Me.col1_CveDocumento.Name = "col1_CveDocumento"
        Me.col1_CveDocumento.OptionsColumn.AllowEdit = False
        Me.col1_CveDocumento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CveDocumento.Visible = True
        '
        'col1_IdCliente
        '
        Me.col1_IdCliente.Caption = "IdCliente"
        Me.col1_IdCliente.FieldName = "id_cliente"
        Me.col1_IdCliente.Name = "col1_IdCliente"
        Me.col1_IdCliente.OptionsColumn.AllowEdit = False
        Me.col1_IdCliente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_IdEmpresa
        '
        Me.col1_IdEmpresa.Caption = "IdEmpresa"
        Me.col1_IdEmpresa.FieldName = "id_empresa"
        Me.col1_IdEmpresa.Name = "col1_IdEmpresa"
        Me.col1_IdEmpresa.OptionsColumn.AllowEdit = False
        Me.col1_IdEmpresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_IdSucursal
        '
        Me.col1_IdSucursal.Caption = "IdSucursal"
        Me.col1_IdSucursal.FieldName = "id_sucursal"
        Me.col1_IdSucursal.Name = "col1_IdSucursal"
        Me.col1_IdSucursal.OptionsColumn.AllowEdit = False
        Me.col1_IdSucursal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdSucursal.Visible = True
        '
        'col1_Ejercicio
        '
        Me.col1_Ejercicio.Caption = "Ejercicio"
        Me.col1_Ejercicio.FieldName = "ejercicio"
        Me.col1_Ejercicio.Name = "col1_Ejercicio"
        Me.col1_Ejercicio.OptionsColumn.AllowEdit = False
        Me.col1_Ejercicio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Ejercicio.Visible = True
        '
        'col1_Fecha
        '
        Me.col1_Fecha.Caption = "Fecha"
        Me.col1_Fecha.FieldName = "fecha"
        Me.col1_Fecha.Name = "col1_Fecha"
        Me.col1_Fecha.OptionsColumn.AllowEdit = False
        Me.col1_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fecha.Visible = True
        '
        'col1_Tipocartera
        '
        Me.col1_Tipocartera.Caption = "Tipocartera"
        Me.col1_Tipocartera.FieldName = "tipocartera"
        Me.col1_Tipocartera.Name = "col1_Tipocartera"
        Me.col1_Tipocartera.OptionsColumn.AllowEdit = False
        Me.col1_Tipocartera.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Tipodoc
        '
        Me.col1_Tipodoc.Caption = "Tipodoc"
        Me.col1_Tipodoc.FieldName = "tipodoc"
        Me.col1_Tipodoc.Name = "col1_Tipodoc"
        Me.col1_Tipodoc.OptionsColumn.AllowEdit = False
        Me.col1_Tipodoc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Serie
        '
        Me.col1_Serie.Caption = "Serie"
        Me.col1_Serie.FieldName = "serie"
        Me.col1_Serie.Name = "col1_Serie"
        Me.col1_Serie.OptionsColumn.AllowEdit = False
        Me.col1_Serie.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Numero
        '
        Me.col1_Numero.Caption = "Numero"
        Me.col1_Numero.FieldName = "numero"
        Me.col1_Numero.Name = "col1_Numero"
        Me.col1_Numero.OptionsColumn.AllowEdit = False
        Me.col1_Numero.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Referencia
        '
        Me.col1_Referencia.Caption = "Referencia"
        Me.col1_Referencia.FieldName = "referencia"
        Me.col1_Referencia.Name = "col1_Referencia"
        Me.col1_Referencia.OptionsColumn.AllowEdit = False
        Me.col1_Referencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Referencia.Visible = True
        '
        'col1_Fechavence
        '
        Me.col1_Fechavence.Caption = "Fechavence"
        Me.col1_Fechavence.FieldName = "fechavence"
        Me.col1_Fechavence.Name = "col1_Fechavence"
        Me.col1_Fechavence.OptionsColumn.AllowEdit = False
        Me.col1_Fechavence.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fechavence.Visible = True
        '
        'col1_Fechapago
        '
        Me.col1_Fechapago.Caption = "Fechapago"
        Me.col1_Fechapago.FieldName = "fechapago"
        Me.col1_Fechapago.Name = "col1_Fechapago"
        Me.col1_Fechapago.OptionsColumn.AllowEdit = False
        Me.col1_Fechapago.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fechapago.Visible = True
        '
        'col1_IdVendedor
        '
        Me.col1_IdVendedor.Caption = "IdVendedor"
        Me.col1_IdVendedor.FieldName = "id_vendedor"
        Me.col1_IdVendedor.Name = "col1_IdVendedor"
        Me.col1_IdVendedor.OptionsColumn.AllowEdit = False
        Me.col1_IdVendedor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Concepto
        '
        Me.col1_Concepto.Caption = "Concepto"
        Me.col1_Concepto.FieldName = "concepto"
        Me.col1_Concepto.Name = "col1_Concepto"
        Me.col1_Concepto.OptionsColumn.AllowEdit = False
        Me.col1_Concepto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Concepto.Visible = True
        '
        'col1_Moneda
        '
        Me.col1_Moneda.Caption = "Moneda"
        Me.col1_Moneda.FieldName = "moneda"
        Me.col1_Moneda.Name = "col1_Moneda"
        Me.col1_Moneda.OptionsColumn.AllowEdit = False
        Me.col1_Moneda.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Moneda.Visible = True
        '
        'col1_Total
        '
        Me.col1_Total.Caption = "Total"
        Me.col1_Total.DisplayFormat.FormatString = "C2"
        Me.col1_Total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Total.FieldName = "total"
        Me.col1_Total.Name = "col1_Total"
        Me.col1_Total.OptionsColumn.AllowEdit = False
        Me.col1_Total.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Total.Visible = True
        '
        'col1_Importe
        '
        Me.col1_Importe.Caption = "Importe"
        Me.col1_Importe.FieldName = "importe"
        Me.col1_Importe.Name = "col1_Importe"
        Me.col1_Importe.OptionsColumn.AllowEdit = False
        Me.col1_Importe.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Iva
        '
        Me.col1_Iva.Caption = "Iva"
        Me.col1_Iva.FieldName = "iva"
        Me.col1_Iva.Name = "col1_Iva"
        Me.col1_Iva.OptionsColumn.AllowEdit = False
        Me.col1_Iva.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Tcambio
        '
        Me.col1_Tcambio.Caption = "Tcambio"
        Me.col1_Tcambio.FieldName = "tcambio"
        Me.col1_Tcambio.Name = "col1_Tcambio"
        Me.col1_Tcambio.OptionsColumn.AllowEdit = False
        Me.col1_Tcambio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Cargo
        '
        Me.col1_Cargo.Caption = "Cargo"
        Me.col1_Cargo.FieldName = "cargo"
        Me.col1_Cargo.Name = "col1_Cargo"
        Me.col1_Cargo.OptionsColumn.AllowEdit = False
        Me.col1_Cargo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Abono
        '
        Me.col1_Abono.Caption = "Abono"
        Me.col1_Abono.FieldName = "abono"
        Me.col1_Abono.Name = "col1_Abono"
        Me.col1_Abono.OptionsColumn.AllowEdit = False
        Me.col1_Abono.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Cvedocto
        '
        Me.col1_Cvedocto.Caption = "Cvedocto"
        Me.col1_Cvedocto.FieldName = "cvedocto"
        Me.col1_Cvedocto.Name = "col1_Cvedocto"
        Me.col1_Cvedocto.OptionsColumn.AllowEdit = False
        Me.col1_Cvedocto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_FolioAclaracion
        '
        Me.col1_FolioAclaracion.Caption = "FolioAclaracion"
        Me.col1_FolioAclaracion.FieldName = "folio_aclaracion"
        Me.col1_FolioAclaracion.Name = "col1_FolioAclaracion"
        Me.col1_FolioAclaracion.OptionsColumn.AllowEdit = False
        Me.col1_FolioAclaracion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Fum
        '
        Me.col1_Fum.Caption = "Fum"
        Me.col1_Fum.FieldName = "fum"
        Me.col1_Fum.Name = "col1_Fum"
        Me.col1_Fum.OptionsColumn.AllowEdit = False
        Me.col1_Fum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_IdUsuario
        '
        Me.col1_IdUsuario.Caption = "IdUsuario"
        Me.col1_IdUsuario.FieldName = "id_usuario"
        Me.col1_IdUsuario.Name = "col1_IdUsuario"
        Me.col1_IdUsuario.OptionsColumn.AllowEdit = False
        Me.col1_IdUsuario.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Feccap
        '
        Me.col1_Feccap.Caption = "Feccap"
        Me.col1_Feccap.FieldName = "feccap"
        Me.col1_Feccap.Name = "col1_Feccap"
        Me.col1_Feccap.OptionsColumn.AllowEdit = False
        Me.col1_Feccap.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Autonumsuc
        '
        Me.col1_Autonumsuc.Caption = "Autonumsuc"
        Me.col1_Autonumsuc.FieldName = "autonumsuc"
        Me.col1_Autonumsuc.Name = "col1_Autonumsuc"
        Me.col1_Autonumsuc.OptionsColumn.AllowEdit = False
        Me.col1_Autonumsuc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Poliza
        '
        Me.col1_Poliza.Caption = "Poliza"
        Me.col1_Poliza.FieldName = "poliza"
        Me.col1_Poliza.Name = "col1_Poliza"
        Me.col1_Poliza.OptionsColumn.AllowEdit = False
        Me.col1_Poliza.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Fechapoliza
        '
        Me.col1_Fechapoliza.Caption = "Fechapoliza"
        Me.col1_Fechapoliza.FieldName = "fechapoliza"
        Me.col1_Fechapoliza.Name = "col1_Fechapoliza"
        Me.col1_Fechapoliza.OptionsColumn.AllowEdit = False
        Me.col1_Fechapoliza.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_AplContable
        '
        Me.col1_AplContable.Caption = "AplContable"
        Me.col1_AplContable.FieldName = "apl_contable"
        Me.col1_AplContable.Name = "col1_AplContable"
        Me.col1_AplContable.OptionsColumn.AllowEdit = False
        Me.col1_AplContable.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_FecUltmovto
        '
        Me.col1_FecUltmovto.Caption = "FecUltmovto"
        Me.col1_FecUltmovto.FieldName = "fec_ultmovto"
        Me.col1_FecUltmovto.Name = "col1_FecUltmovto"
        Me.col1_FecUltmovto.OptionsColumn.AllowEdit = False
        Me.col1_FecUltmovto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_SaldoF
        '
        Me.col1_SaldoF.Caption = "SaldoF"
        Me.col1_SaldoF.FieldName = "saldo_f"
        Me.col1_SaldoF.Name = "col1_SaldoF"
        Me.col1_SaldoF.OptionsColumn.AllowEdit = False
        Me.col1_SaldoF.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_VencidoF
        '
        Me.col1_VencidoF.Caption = "VencidoF"
        Me.col1_VencidoF.FieldName = "vencido_f"
        Me.col1_VencidoF.Name = "col1_VencidoF"
        Me.col1_VencidoF.OptionsColumn.AllowEdit = False
        Me.col1_VencidoF.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_DiasvencF
        '
        Me.col1_DiasvencF.Caption = "DiasvencF"
        Me.col1_DiasvencF.FieldName = "diasvenc_f"
        Me.col1_DiasvencF.Name = "col1_DiasvencF"
        Me.col1_DiasvencF.OptionsColumn.AllowEdit = False
        Me.col1_DiasvencF.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_ImpuestosTrasladados
        '
        Me.col1_ImpuestosTrasladados.Caption = "ImpuestosTrasladados"
        Me.col1_ImpuestosTrasladados.FieldName = "impuestos_trasladados"
        Me.col1_ImpuestosTrasladados.Name = "col1_ImpuestosTrasladados"
        Me.col1_ImpuestosTrasladados.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestosTrasladados.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_ImpuestosRetenidos
        '
        Me.col1_ImpuestosRetenidos.Caption = "ImpuestosRetenidos"
        Me.col1_ImpuestosRetenidos.FieldName = "impuestos_retenidos"
        Me.col1_ImpuestosRetenidos.Name = "col1_ImpuestosRetenidos"
        Me.col1_ImpuestosRetenidos.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestosRetenidos.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_TotalImpuestos
        '
        Me.col1_TotalImpuestos.Caption = "TotalImpuestos"
        Me.col1_TotalImpuestos.FieldName = "total_impuestos"
        Me.col1_TotalImpuestos.Name = "col1_TotalImpuestos"
        Me.col1_TotalImpuestos.OptionsColumn.AllowEdit = False
        Me.col1_TotalImpuestos.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_TasaIva
        '
        Me.col1_TasaIva.Caption = "TasaIva"
        Me.col1_TasaIva.FieldName = "tasa_iva"
        Me.col1_TasaIva.Name = "col1_TasaIva"
        Me.col1_TasaIva.OptionsColumn.AllowEdit = False
        Me.col1_TasaIva.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_ImpuestoIva
        '
        Me.col1_ImpuestoIva.Caption = "ImpuestoIva"
        Me.col1_ImpuestoIva.FieldName = "impuesto_iva"
        Me.col1_ImpuestoIva.Name = "col1_ImpuestoIva"
        Me.col1_ImpuestoIva.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIva.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_TasaIeps
        '
        Me.col1_TasaIeps.Caption = "TasaIeps"
        Me.col1_TasaIeps.FieldName = "tasa_ieps"
        Me.col1_TasaIeps.Name = "col1_TasaIeps"
        Me.col1_TasaIeps.OptionsColumn.AllowEdit = False
        Me.col1_TasaIeps.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_ImpuestoIeps
        '
        Me.col1_ImpuestoIeps.Caption = "ImpuestoIeps"
        Me.col1_ImpuestoIeps.FieldName = "impuesto_ieps"
        Me.col1_ImpuestoIeps.Name = "col1_ImpuestoIeps"
        Me.col1_ImpuestoIeps.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIeps.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_TasaIvaret
        '
        Me.col1_TasaIvaret.Caption = "TasaIvaret"
        Me.col1_TasaIvaret.FieldName = "tasa_ivaret"
        Me.col1_TasaIvaret.Name = "col1_TasaIvaret"
        Me.col1_TasaIvaret.OptionsColumn.AllowEdit = False
        Me.col1_TasaIvaret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_ImpuestoIvaret
        '
        Me.col1_ImpuestoIvaret.Caption = "ImpuestoIvaret"
        Me.col1_ImpuestoIvaret.FieldName = "impuesto_ivaret"
        Me.col1_ImpuestoIvaret.Name = "col1_ImpuestoIvaret"
        Me.col1_ImpuestoIvaret.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIvaret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_TasaIsrret
        '
        Me.col1_TasaIsrret.Caption = "TasaIsrret"
        Me.col1_TasaIsrret.FieldName = "tasa_isrret"
        Me.col1_TasaIsrret.Name = "col1_TasaIsrret"
        Me.col1_TasaIsrret.OptionsColumn.AllowEdit = False
        Me.col1_TasaIsrret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_ImpuestoIsrret
        '
        Me.col1_ImpuestoIsrret.Caption = "ImpuestoIsrret"
        Me.col1_ImpuestoIsrret.FieldName = "impuesto_isrret"
        Me.col1_ImpuestoIsrret.Name = "col1_ImpuestoIsrret"
        Me.col1_ImpuestoIsrret.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoIsrret.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_IdCcosto
        '
        Me.col1_IdCcosto.Caption = "IdCcosto"
        Me.col1_IdCcosto.FieldName = "id_ccosto"
        Me.col1_IdCcosto.Name = "col1_IdCcosto"
        Me.col1_IdCcosto.OptionsColumn.AllowEdit = False
        Me.col1_IdCcosto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Flujo
        '
        Me.col1_Flujo.Caption = "Flujo"
        Me.col1_Flujo.FieldName = "flujo"
        Me.col1_Flujo.Name = "col1_Flujo"
        Me.col1_Flujo.OptionsColumn.AllowEdit = False
        Me.col1_Flujo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'gridBand2
        '
        Me.gridBand2.Columns.Add(Me.col1_Saldodocto)
        Me.gridBand2.Columns.Add(Me.col1_SwSel)
        Me.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right
        Me.gridBand2.Name = "gridBand2"
        Me.gridBand2.VisibleIndex = 1
        Me.gridBand2.Width = 150
        '
        'col1_Saldodocto
        '
        Me.col1_Saldodocto.Caption = "Saldodocto"
        Me.col1_Saldodocto.DisplayFormat.FormatString = "C2"
        Me.col1_Saldodocto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Saldodocto.FieldName = "saldodocto"
        Me.col1_Saldodocto.Name = "col1_Saldodocto"
        Me.col1_Saldodocto.OptionsColumn.AllowEdit = False
        Me.col1_Saldodocto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Saldodocto.Visible = True
        '
        'col1_SwSel
        '
        Me.col1_SwSel.Caption = "Sel"
        Me.col1_SwSel.FieldName = "sw_sel"
        Me.col1_SwSel.Name = "col1_SwSel"
        Me.col1_SwSel.Visible = True
        '
        'gridBand3
        '
        Me.gridBand3.AppearanceHeader.Options.UseTextOptions = True
        Me.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.gridBand3.Caption = "Bonificación"
        Me.gridBand3.Columns.Add(Me.col1_TasaOtroimp)
        Me.gridBand3.Columns.Add(Me.col1_ImpuestoOtroimp)
        Me.gridBand3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right
        Me.gridBand3.Name = "gridBand3"
        Me.gridBand3.VisibleIndex = 2
        Me.gridBand3.Width = 150
        '
        'col1_TasaOtroimp
        '
        Me.col1_TasaOtroimp.Caption = "%Boni"
        Me.col1_TasaOtroimp.DisplayFormat.FormatString = "P2"
        Me.col1_TasaOtroimp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_TasaOtroimp.FieldName = "tasa_otroimp"
        Me.col1_TasaOtroimp.Name = "col1_TasaOtroimp"
        Me.col1_TasaOtroimp.OptionsColumn.AllowEdit = False
        Me.col1_TasaOtroimp.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_TasaOtroimp.Visible = True
        '
        'col1_ImpuestoOtroimp
        '
        Me.col1_ImpuestoOtroimp.Caption = "Importe"
        Me.col1_ImpuestoOtroimp.DisplayFormat.FormatString = "C2"
        Me.col1_ImpuestoOtroimp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_ImpuestoOtroimp.FieldName = "impuesto_otroimp"
        Me.col1_ImpuestoOtroimp.Name = "col1_ImpuestoOtroimp"
        Me.col1_ImpuestoOtroimp.OptionsColumn.AllowEdit = False
        Me.col1_ImpuestoOtroimp.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImpuestoOtroimp.Visible = True
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Txt_CodigoBonifciacion)
        Me.GroupControl1.Controls.Add(Me.Label8)
        Me.GroupControl1.Controls.Add(Me.lblFormaPago)
        Me.GroupControl1.Controls.Add(Me.cbo_FormaPago)
        Me.GroupControl1.Controls.Add(Me.Label7)
        Me.GroupControl1.Controls.Add(Me.dtp_Fecha)
        Me.GroupControl1.Controls.Add(Me.Btn_Cancel)
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.txt_TotalAPagar)
        Me.GroupControl1.Controls.Add(Me.Label6)
        Me.GroupControl1.Controls.Add(Me.txt_Total)
        Me.GroupControl1.Controls.Add(Me.Label27)
        Me.GroupControl1.Controls.Add(Me.btn_BuscarCliente)
        Me.GroupControl1.Controls.Add(Me.txt_Estatus)
        Me.GroupControl1.Controls.Add(Me.txt_RFC)
        Me.GroupControl1.Controls.Add(Me.Label4)
        Me.GroupControl1.Controls.Add(Me.Label5)
        Me.GroupControl1.Controls.Add(Me.txt_Nombre)
        Me.GroupControl1.Controls.Add(Me.txt_Empresa)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Controls.Add(Me.txt_Tarjeta)
        Me.GroupControl1.Controls.Add(Me.txt_Cuenta)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.lbl_FechaIni)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl1.Location = New System.Drawing.Point(0, 340)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(809, 272)
        Me.GroupControl1.TabIndex = 0
        '
        'Txt_CodigoBonifciacion
        '
        Me.Txt_CodigoBonifciacion.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_CodigoBonifciacion.Location = New System.Drawing.Point(185, 28)
        Me.Txt_CodigoBonifciacion.Name = "Txt_CodigoBonifciacion"
        Me.Txt_CodigoBonifciacion.Size = New System.Drawing.Size(272, 30)
        Me.Txt_CodigoBonifciacion.TabIndex = 27
        Me.Txt_CodigoBonifciacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(7, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(172, 23)
        Me.Label8.TabIndex = 28
        Me.Label8.Text = "Codigo Bonificación"
        '
        'lblFormaPago
        '
        Me.lblFormaPago.AutoSize = True
        Me.lblFormaPago.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormaPago.Location = New System.Drawing.Point(357, 71)
        Me.lblFormaPago.Name = "lblFormaPago"
        Me.lblFormaPago.Size = New System.Drawing.Size(110, 23)
        Me.lblFormaPago.TabIndex = 24
        Me.lblFormaPago.Text = "Forma Pago"
        '
        'cbo_FormaPago
        '
        Me.cbo_FormaPago.DisplayMember = "valor"
        Me.cbo_FormaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_FormaPago.FormattingEnabled = True
        Me.cbo_FormaPago.Items.AddRange(New Object() {"01 - EFECTIVO", "02 - ODP", "03 - SPEI"})
        Me.cbo_FormaPago.Location = New System.Drawing.Point(500, 68)
        Me.cbo_FormaPago.Name = "cbo_FormaPago"
        Me.cbo_FormaPago.Size = New System.Drawing.Size(278, 32)
        Me.cbo_FormaPago.TabIndex = 2
        Me.cbo_FormaPago.ValueMember = "valor"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(463, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(107, 23)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Fecha Pago"
        '
        'dtp_Fecha
        '
        Me.dtp_Fecha.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_Fecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtp_Fecha.Location = New System.Drawing.Point(576, 27)
        Me.dtp_Fecha.Name = "dtp_Fecha"
        Me.dtp_Fecha.Size = New System.Drawing.Size(142, 30)
        Me.dtp_Fecha.TabIndex = 21
        '
        'Btn_Cancel
        '
        Me.Btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Btn_Cancel.Location = New System.Drawing.Point(783, 27)
        Me.Btn_Cancel.Name = "Btn_Cancel"
        Me.Btn_Cancel.Size = New System.Drawing.Size(42, 31)
        Me.Btn_Cancel.TabIndex = 20
        Me.Btn_Cancel.Text = "..."
        Me.Btn_Cancel.UseVisualStyleBackColor = True
        Me.Btn_Cancel.Visible = False
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.ImageOptions.Image = CType(resources.GetObject("btn_Refrescar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_Refrescar.Location = New System.Drawing.Point(735, 20)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Refrescar.TabIndex = 2
        '
        'txt_TotalAPagar
        '
        Me.txt_TotalAPagar.EditValue = ""
        Me.txt_TotalAPagar.Location = New System.Drawing.Point(97, 203)
        Me.txt_TotalAPagar.Name = "txt_TotalAPagar"
        Me.txt_TotalAPagar.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TotalAPagar.Properties.Appearance.Options.UseFont = True
        Me.txt_TotalAPagar.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_TotalAPagar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Black
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.White
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_TotalAPagar.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_TotalAPagar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_TotalAPagar.Properties.Mask.EditMask = "c"
        Me.txt_TotalAPagar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_TotalAPagar.Properties.ReadOnly = True
        Me.txt_TotalAPagar.Size = New System.Drawing.Size(221, 54)
        Me.txt_TotalAPagar.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 216)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 30)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "A Pagar"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_Total
        '
        Me.txt_Total.EditValue = ""
        Me.txt_Total.Location = New System.Drawing.Point(551, 203)
        Me.txt_Total.Name = "txt_Total"
        Me.txt_Total.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.Appearance.Options.UseFont = True
        Me.txt_Total.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Total.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Total.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Black
        Me.txt_Total.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Total.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Total.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Total.Properties.Mask.EditMask = "c"
        Me.txt_Total.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Total.Size = New System.Drawing.Size(221, 54)
        Me.txt_Total.TabIndex = 9
        '
        'Label27
        '
        Me.Label27.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(324, 216)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(221, 30)
        Me.Label27.TabIndex = 17
        Me.Label27.Text = "PAGO"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_BuscarCliente
        '
        Me.btn_BuscarCliente.Location = New System.Drawing.Point(303, 67)
        Me.btn_BuscarCliente.Name = "btn_BuscarCliente"
        Me.btn_BuscarCliente.Size = New System.Drawing.Size(42, 31)
        Me.btn_BuscarCliente.TabIndex = 1
        Me.btn_BuscarCliente.Text = "..."
        Me.btn_BuscarCliente.UseVisualStyleBackColor = True
        '
        'txt_Estatus
        '
        Me.txt_Estatus.BackColor = System.Drawing.Color.White
        Me.txt_Estatus.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Estatus.Location = New System.Drawing.Point(585, 106)
        Me.txt_Estatus.Name = "txt_Estatus"
        Me.txt_Estatus.ReadOnly = True
        Me.txt_Estatus.Size = New System.Drawing.Size(192, 30)
        Me.txt_Estatus.TabIndex = 5
        Me.txt_Estatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_RFC
        '
        Me.txt_RFC.BackColor = System.Drawing.Color.White
        Me.txt_RFC.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(353, 106)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.ReadOnly = True
        Me.txt_RFC.Size = New System.Drawing.Size(150, 30)
        Me.txt_RFC.TabIndex = 4
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(509, 108)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 23)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Estatus"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(304, 108)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 23)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "RFC"
        '
        'txt_Nombre
        '
        Me.txt_Nombre.BackColor = System.Drawing.Color.White
        Me.txt_Nombre.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Location = New System.Drawing.Point(97, 172)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.ReadOnly = True
        Me.txt_Nombre.Size = New System.Drawing.Size(681, 30)
        Me.txt_Nombre.TabIndex = 7
        '
        'txt_Empresa
        '
        Me.txt_Empresa.BackColor = System.Drawing.Color.White
        Me.txt_Empresa.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa.Location = New System.Drawing.Point(97, 138)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.ReadOnly = True
        Me.txt_Empresa.Size = New System.Drawing.Size(680, 30)
        Me.txt_Empresa.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(5, 175)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 23)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Nombre"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(5, 141)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 23)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Empresa"
        '
        'txt_Tarjeta
        '
        Me.txt_Tarjeta.BackColor = System.Drawing.Color.White
        Me.txt_Tarjeta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Tarjeta.Location = New System.Drawing.Point(97, 102)
        Me.txt_Tarjeta.Name = "txt_Tarjeta"
        Me.txt_Tarjeta.ReadOnly = True
        Me.txt_Tarjeta.Size = New System.Drawing.Size(200, 30)
        Me.txt_Tarjeta.TabIndex = 3
        Me.txt_Tarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Cuenta
        '
        Me.txt_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cuenta.Location = New System.Drawing.Point(97, 68)
        Me.txt_Cuenta.Name = "txt_Cuenta"
        Me.txt_Cuenta.Size = New System.Drawing.Size(200, 30)
        Me.txt_Cuenta.TabIndex = 0
        Me.txt_Cuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 105)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 23)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Tarjeta"
        '
        'lbl_FechaIni
        '
        Me.lbl_FechaIni.AutoSize = True
        Me.lbl_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_FechaIni.Location = New System.Drawing.Point(5, 71)
        Me.lbl_FechaIni.Name = "lbl_FechaIni"
        Me.lbl_FechaIni.Size = New System.Drawing.Size(69, 23)
        Me.lbl_FechaIni.TabIndex = 1
        Me.lbl_FechaIni.Text = "Cuenta"
        '
        'DockManager1
        '
        Me.DockManager1.AutoHideContainers.AddRange(New DevExpress.XtraBars.Docking.AutoHideContainer() {Me.hideContainerRight})
        Me.DockManager1.Form = Me
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'hideContainerRight
        '
        Me.hideContainerRight.BackColor = System.Drawing.SystemColors.Control
        Me.hideContainerRight.Controls.Add(Me.CG_Ayuda)
        Me.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.hideContainerRight.Location = New System.Drawing.Point(788, 0)
        Me.hideContainerRight.Name = "hideContainerRight"
        Me.hideContainerRight.Size = New System.Drawing.Size(21, 340)
        '
        'CG_Ayuda
        '
        Me.CG_Ayuda.Controls.Add(Me.DockPanel1_Container)
        Me.CG_Ayuda.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.CG_Ayuda.ID = New System.Guid("234d4c82-6833-474c-8637-7252e94fa3b6")
        Me.CG_Ayuda.Location = New System.Drawing.Point(0, 0)
        Me.CG_Ayuda.Name = "CG_Ayuda"
        Me.CG_Ayuda.OriginalSize = New System.Drawing.Size(200, 200)
        Me.CG_Ayuda.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.CG_Ayuda.SavedIndex = 0
        Me.CG_Ayuda.Size = New System.Drawing.Size(200, 699)
        Me.CG_Ayuda.Text = "AYUDA - TECLAS RAPIDAS"
        Me.CG_Ayuda.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.GridC_2)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 26)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(193, 670)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'GridC_2
        '
        Me.GridC_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_2.Location = New System.Drawing.Point(0, 0)
        Me.GridC_2.MainView = Me.GridV_2
        Me.GridC_2.Name = "GridC_2"
        Me.GridC_2.Size = New System.Drawing.Size(193, 670)
        Me.GridC_2.TabIndex = 11
        Me.GridC_2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_2})
        '
        'GridV_2
        '
        Me.GridV_2.ColumnPanelRowHeight = 0
        Me.GridV_2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col2_Accion, Me.col2_Combinacion})
        Me.GridV_2.FixedLineWidth = 3
        Me.GridV_2.FooterPanelHeight = 0
        Me.GridV_2.GridControl = Me.GridC_2
        Me.GridV_2.GroupRowHeight = 0
        Me.GridV_2.LevelIndent = 0
        Me.GridV_2.Name = "GridV_2"
        Me.GridV_2.OptionsView.ShowGroupPanel = False
        Me.GridV_2.PreviewIndent = 0
        Me.GridV_2.RowHeight = 0
        Me.GridV_2.ViewCaptionHeight = 0
        '
        'col2_Accion
        '
        Me.col2_Accion.Caption = "Acción"
        Me.col2_Accion.FieldName = "Accion"
        Me.col2_Accion.Name = "col2_Accion"
        Me.col2_Accion.OptionsColumn.AllowEdit = False
        Me.col2_Accion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Accion.Visible = True
        Me.col2_Accion.VisibleIndex = 0
        '
        'col2_Combinacion
        '
        Me.col2_Combinacion.Caption = "Combinación"
        Me.col2_Combinacion.FieldName = "Combinacion"
        Me.col2_Combinacion.Name = "col2_Combinacion"
        Me.col2_Combinacion.OptionsColumn.AllowEdit = False
        Me.col2_Combinacion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Combinacion.Visible = True
        Me.col2_Combinacion.VisibleIndex = 1
        '
        'pdvRecibirPagos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Btn_Cancel
        Me.ClientSize = New System.Drawing.Size(809, 699)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.hideContainerRight)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl3)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "pdvRecibirPagos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recibir Pagos"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pop_Menu.ResumeLayout(False)
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txt_TotalAPagar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hideContainerRight.ResumeLayout(False)
        Me.CG_Ayuda.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label1 As Label
    Friend WithEvents lbl_FechaIni As Label
    Friend WithEvents txt_Estatus As TextBox
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_Nombre As TextBox
    Friend WithEvents txt_Empresa As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Tarjeta As TextBox
    Friend WithEvents txt_Cuenta As TextBox
    Friend WithEvents btn_BuscarCliente As Button
    Friend WithEvents txt_Total As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label27 As Label
    Friend WithEvents txt_TotalAPagar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label6 As Label
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents col1_CveDocumento As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdCliente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdEmpresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdSucursal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Ejercicio As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipocartera As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipodoc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Serie As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numero As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Referencia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fechavence As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fechapago As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdVendedor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Concepto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Moneda As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Total As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Importe As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Iva As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tcambio As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cargo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Abono As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Cvedocto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_FolioAclaracion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fum As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdUsuario As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Feccap As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Autonumsuc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Poliza As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fechapoliza As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_AplContable As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_FecUltmovto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_SaldoF As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_VencidoF As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_DiasvencF As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestosTrasladados As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestosRetenidos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TotalImpuestos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIva As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIva As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIeps As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIeps As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIvaret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIvaret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaIsrret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoIsrret As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_TasaOtroimp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImpuestoOtroimp As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdCcosto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Flujo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Saldodocto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_SwSel As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Private WithEvents txt_FormatoTicket As TextBox
    Private WithEvents txt_Empresa1 As TextBox
    Private WithEvents txt_MargenesTicket As TextBox
    Private WithEvents txt_TipoCliente As TextBox
    Private WithEvents txt_Direccion2 As TextBox
    Private WithEvents txt_Direccion As TextBox
    Friend WithEvents btn_Refrescar As DevExpress.XtraEditors.SimpleButton
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents CG_Ayuda As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents GridC_2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col2_Accion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Combinacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents hideContainerRight As DevExpress.XtraBars.Docking.AutoHideContainer
    Friend WithEvents Btn_Cancel As Button
    Friend WithEvents pop_Menu As ContextMenuStrip
    Friend WithEvents pop_Opc1 As ToolStripMenuItem
    Friend WithEvents pop_Opc2 As ToolStripMenuItem
    Friend WithEvents pop_Opc3 As ToolStripMenuItem
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents gridBand3 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents Label7 As Label
    Friend WithEvents dtp_Fecha As DateTimePicker
    Friend WithEvents Label36 As Label
    Friend WithEvents txt_Sucursal As TextBox
    Friend WithEvents lblFormaPago As Label
    Friend WithEvents cbo_FormaPago As ComboBox
    Friend WithEvents Txt_CodigoBonifciacion As TextBox
    Friend WithEvents Label8 As Label
End Class
