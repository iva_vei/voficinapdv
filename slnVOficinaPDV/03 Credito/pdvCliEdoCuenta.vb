﻿Imports DevExpress.XtraBars
Imports DevExpress.XtraGrid.Views.Base
Public Class pdvCliEdoCuenta

    Public TecladoAtajos As List(Of Principal.clsTecladoAtajos)
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim APagar As Double = 0
        Try
            oDatos = New Datos_Viscoi
            If txt_Cuenta.Text <> "" Then
                If oDatos.EDOCTACLIENTES_SEL(Globales.oAmbientes.Id_Empresa, txt_Cuenta.Text, dtDatos, Mensaje) Then
                    GridC_1.DataSource = dtDatos
                    GridC_1.RefreshDataSource()
                    GridV_1.BestFitColumns()
                Else
                    GridC_1.DataSource = Nothing
                    GridC_1.RefreshDataSource()
                End If

                If oDatos.RecuperaAntiguedadSaldos(Globales.oAmbientes.Id_Empresa, "todas", txt_Cuenta.Text, "todos", "todos", "Pesos", Today, "4", "0", "", "", "", "1111111", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        txt_sfinal.Text = dtDatos.Rows(0).Item("saldo")
                        txt_vencido.Text = dtDatos.Rows(0).Item("vencido")
                        Txt_anticipo.Text = dtDatos.Rows(0).Item("anticipo")
                        txt_vence1.Text = dtDatos.Rows(0).Item("vencido1")
                        txt_vence2.Text = dtDatos.Rows(0).Item("vencido2")
                        txt_vence3.Text = dtDatos.Rows(0).Item("vencido3")
                        txt_vence4.Text = dtDatos.Rows(0).Item("vencido4")
                        txt_vence5.Text = dtDatos.Rows(0).Item("vencido5")
                        txt_vence6.Text = dtDatos.Rows(0).Item("vencido6")
                        txt_pvence1.Text = dtDatos.Rows(0).Item("p_vencido1")
                        txt_pvence2.Text = dtDatos.Rows(0).Item("p_vencido2")
                        txt_pvence3.Text = dtDatos.Rows(0).Item("p_vencido3")
                        txt_pvence4.Text = dtDatos.Rows(0).Item("p_vencido4")
                        txt_pvence5.Text = dtDatos.Rows(0).Item("p_vencido5")
                        txt_pvence6.Text = dtDatos.Rows(0).Item("p_vencido6")

                    Else

                    End If

                Else
                    '       
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub


    Private Sub pdvCliEdoCuenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim DatosConexion() As String
        Try
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

        txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal
    End Sub
    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()

    End Sub

    Private Sub btn_BuscarCliente_Click(sender As Object, e As EventArgs) Handles btn_BuscarCliente.Click
        Dim oFrm As pdvCuentas
        Try
            oFrm = New pdvCuentas
            oFrm.Tipo = ""
            oFrm.TipoForma = pdvCuentas.eTipoForma.Cliente
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Cuenta.Text = oFrm.Cuenta
                txt_Nombre.Text = oFrm.Nombre



                Call Refrescar()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub WindowsUIButtonPanel1_Click(sender As Object, e As EventArgs) Handles WindowsUIButtonPanel1.Click

    End Sub

    Private Sub WindowsUIButtonPanel1_ButtonClick(sender As Object, e As DevExpress.XtraBars.Docking2010.ButtonEventArgs) Handles WindowsUIButtonPanel1.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Caption
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim oDatos As New Datos_Viscoi
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0

        ''Dim Total As Double
        ''Dim PagoContado As Double
        ''Dim PagoDE As Double
        ''Dim PagoTotal As Double
        Try
            Select Case tag.ToUpper
                Case "AYUDA"
                    Cg_ayuda.Visible = If(Cg_ayuda.Visible, False, True)
                Case "IMPRIMIR"
                    Call Imprimir(txt_FormatoTicket.Text)
                Case "SALIR"
                    Me.Close()
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub Imprimir(ByVal Formato As String)
        Dim Reporte As Object
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entEdocuenta
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool


        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.EDOCTACLIENTES_SEL(Globales.oAmbientes.Id_Empresa, txt_Cuenta.Text, dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then

                    oTicket = New entEdocuenta
                    oTicket.Cliente = txt_Cuenta.Text & "-" & txt_Nombre.Text
                    oTicket.Vencido = txt_vencido.Text
                    oTicket.Saldo = txt_sfinal.Text
                    oTicket.Vencido1 = txt_vence1.Text
                    oTicket.Vencido2 = txt_vence2.Text
                    oTicket.Vencido3 = txt_vence3.Text
                    oTicket.Vencido4 = txt_vence4.Text
                    oTicket.Vencido5 = txt_vence5.Text
                    oTicket.Vencido6 = txt_vence6.Text
                    oTicket.P_vencido1 = txt_pvence1.Text
                    oTicket.P_vencido2 = txt_pvence2.Text
                    oTicket.P_vencido3 = txt_pvence3.Text
                    oTicket.P_vencido4 = txt_pvence4.Text
                    oTicket.P_vencido5 = txt_pvence5.Text
                    oTicket.P_vencido6 = txt_pvence6.Text




                    oTicket.Fill(dtDatos)

                    If oDatos.PVTA_Recupera_Cuentas(txt_Cuenta.Text, "", Globales.oAmbientes.Id_Empresa, "", "", "", "", dtDatos, Mensaje) Then
                        If dtDatos.Rows.Count > 0 Then
                            oTicket.Direccion = dtDatos.Rows(0).Item("dircasa1")
                            oTicket.Estado = dtDatos.Rows(0).Item("estado")
                            oTicket.Municipio = dtDatos.Rows(0).Item("municipio")
                            oTicket.Vendedor = dtDatos.Rows(0).Item("agente")
                            oTicket.Limite = dtDatos.Rows(0).Item("limite")
                            oTicket.Disponible = dtDatos(0).Item("disponible")
                            oTicket.Coordinador = dtDatos(0).Item("id_coordinador")
                            oTicket.Cobrador = dtDatos(0).Item("id_cobrador")
                            oTicket.Ciudad = dtDatos(0).Item("cdcasa")



                        End If
                    Else
                        ' GridC_1.DataSource = Nothing
                        'GridC_1.RefreshDataSource()
                    End If
                    '                    If oDatos.PVTA_Recupera_Cuentas(txt_Cuenta.Text, txt_Tarjeta.Text, txt_Empresa.Text, txt_Nombre.Text, txt_RFC.Text, txt_Estatus.Text, _Tipo, dtDatos, Mensaje) Then
                    'GridC_1.DataSource = dtDatos

                    '  Else
                    'GridC_1.DataSource = Nothing
                    'GridC_1.RefreshDataSource()
                    ' End If




                    Dim MargenAba As Integer = 0
                    Dim MargenIzq As Integer = 0
                    Dim MargenDer As Integer = 0
                    Dim MargenArr As Integer = 0

                    Dim printBase As DevExpress.XtraPrinting.PrintToolBase
                    Select Case Formato
                        Case "SEAFON"
                            Reporte = New XtraRep_Edocta
                            TryCast(Reporte, XtraRep_Edocta).Empresa = Globales.oAmbientes.Id_Empresa
                            TryCast(Reporte, XtraRep_Edocta).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_Edocta).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_Edocta).PrintingSystem)
                        Case "EPSON"
                            Reporte = New XtraRep_Edocta
                            TryCast(Reporte, XtraRep_Edocta).Empresa = Globales.oAmbientes.Id_Empresa
                            TryCast(Reporte, XtraRep_Edocta).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_Edocta).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_Edocta).PrintingSystem)
                        Case Else
                            Reporte = New XtraRep_Edocta
                            TryCast(Reporte, XtraRep_Edocta).Empresa = Globales.oAmbientes.Id_Empresa
                            TryCast(Reporte, XtraRep_Edocta).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_Edocta).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_Edocta).PrintingSystem)
                    End Select
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()


                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If


            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_pvence6_TextChanged(sender As Object, e As EventArgs) Handles txt_pvence6.TextChanged

    End Sub

    Private Sub txt_Cuenta_LostFocus(sender As Object, e As EventArgs) Handles txt_Cuenta.LostFocus
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi
            If txt_Cuenta.Text <> "" Then
                If oDatos.PVTA_Recupera_Cuentas(txt_Cuenta.Text, "", "", "", "", "", "", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        txt_Nombre.Text = dtDatos.Rows(0).Item("nombre").ToString.Trim


                        Call Refrescar()
                    Else
                        txt_Nombre.Text = ""
                        txt_Cuenta.Text = ""

                    End If
                End If
                Else
                    txt_Nombre.Text = ""
                    txt_Cuenta.Text = ""
                End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub txt_Cuenta_TextChanged(sender As Object, e As EventArgs) Handles txt_Cuenta.TextChanged

    End Sub
End Class
