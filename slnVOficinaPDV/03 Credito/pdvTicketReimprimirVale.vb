﻿Public Class pdvTicketReimprimirVale
    Private Sub pdvTicketReimprimir_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatosSuc As DataTable = Nothing
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Dim PDatCaja As String = ""

        Try
            oDatos = New Datos_Viscoi
            Dim pd As New Printing.PrintDocument
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer
            txt_Empresa.Text = Globales.oAmbientes.Id_Empresa
            If oDatos.PVTA_Recupera_Sucursales(txt_Empresa.Text, "", dtDatosSuc, Msj) Then
                cbo_Sucursal.DataSource = dtDatosSuc
                cbo_Sucursal.SelectedIndex = 0
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    PDatCaja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next
            cbo_Sucursal.Text = PDatSucursal

            If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text, "", "TODAS", dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                    txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper
                End If
            End If

            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                lstImpresoras.Enabled = True
            Else
                lstImpresoras.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        Try
            If txt_Autonumsuc.Text <> "" Then
                ImprimirTicket(txt_FormatoTicket.Text)
            Else
                MessageBox.Show("Debes teclar un Folio para reimprimir", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ImprimirTicket(ByVal Formato As String)
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Dim Pic1 As PictureBox
        Dim ImprimirODP As Boolean
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_Canje_Impresion(Today.Date _
                                              , txt_Autonumsuc.Text _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , Mensaje) Then

                Formato = dtDatos.Rows(0).Item("unidadvta")
                If dtDatos.Rows(0).Item("nomcorto") = "SI" Then
                    ImprimirODP = True
                Else
                    ImprimirODP = False
                End If

                Pic1 = New PictureBox
                    If oDatos.PVTA_Recupera_ClienteFinal(dtDatos1.Rows(0).Item("id_cliente"), "", "", "", dtDatos2, Mensaje) Then
                        Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", dtDatos1.Rows(0).Item("id_cliente"), 0, Pic1)
                    End If

                    DEFinal = DEActual + DEAcumulados - DEUtilizados

                    oTicket = New entTicketVenta
                    oTicket.Fill(dtDatos, dtDatos1, dtDatos2)

                oTicket.Empresa = oTicket.Id_empresa
                oTicket.Rfc = txt_RFC.Text
                    oTicket.Direccion = txt_Direccion.Text
                    oTicket.Direccion2 = txt_Direccion2.Text
                    oTicket.Cajero = txt_Cajero.Text
                    oTicket.Caja = txt_Caja.Text
                    oTicket.Id_cliente1 = dtDatos1.Rows(0).Item("id_cliente1")
                    oTicket.Nom_cliente1 = dtDatos1.Rows(0).Item("nom_cliente1")

                    If dtDatos1.Rows(0).Item("nom_cliente").Length > 0 Then
                        oTicket.Lealtad_nombre = dtDatos1.Rows(0).Item("nom_cliente")
                        oTicket.Puntos_saldo_ant = PuntosActual
                        oTicket.Puntos_acumulados = PuntosAcumulados
                        oTicket.Puntos_utilizados = PuntosUtilizados
                        oTicket.Puntos_saldo_act = PuntosFinal

                        oTicket.De_saldo_ant = DEActual
                        oTicket.De_acumulados = DEAcumulados
                        oTicket.De_utilizados = DEUtilizados
                        oTicket.De_saldo_act = DEFinal
                    Else
                        oTicket.Lealtad_nombre = 0
                        oTicket.Puntos_saldo_ant = 0
                        oTicket.Puntos_acumulados = 0
                        oTicket.Puntos_utilizados = 0
                        oTicket.Puntos_saldo_act = 0

                        oTicket.De_saldo_ant = 0
                        oTicket.De_acumulados = 0
                        oTicket.De_utilizados = 0
                        oTicket.De_saldo_act = 0
                    End If

                    Dim MargenAba As Integer = 0
                    Dim MargenIzq As Integer = 0
                    Dim MargenDer As Integer = 0
                    Dim MargenArr As Integer = 0
                    Try
                        MargenAba = txt_MargenesTicket.Text.Split(",")(0)
                    Catch ex As Exception
                        MargenAba = 0
                    End Try
                    Try
                        MargenIzq = txt_MargenesTicket.Text.Split(",")(1)
                    Catch ex As Exception
                        MargenIzq = 0
                    End Try
                    Try
                        MargenDer = txt_MargenesTicket.Text.Split(",")(2)
                    Catch ex As Exception
                        MargenDer = 0
                    End Try
                    Try
                        MargenArr = txt_MargenesTicket.Text.Split(",")(3)
                    Catch ex As Exception
                        MargenArr = 0
                    End Try
                    Select Case Formato
                        Case "CARTA"
                            Reporte = New xtraRepTicketValeCarta
                            'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                            TryCast(Reporte, xtraRepTicketValeCarta).MargenAbj = MargenAba
                            TryCast(Reporte, xtraRepTicketValeCarta).MargenIzq = MargenIzq
                            TryCast(Reporte, xtraRepTicketValeCarta).MargenDer = MargenDer
                            TryCast(Reporte, xtraRepTicketValeCarta).MargenArr = MargenArr
                            TryCast(Reporte, xtraRepTicketValeCarta).SwReimpresion = True
                            TryCast(Reporte, xtraRepTicketValeCarta).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                            TryCast(Reporte, xtraRepTicketValeCarta).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketValeCarta).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeCarta).PrintingSystem)

                        Case "SEAFON"
                            Reporte = New xtraRepTicketVale
                            'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                            TryCast(Reporte, xtraRepTicketVale).MargenAbj = MargenAba
                            TryCast(Reporte, xtraRepTicketVale).MargenIzq = MargenIzq
                            TryCast(Reporte, xtraRepTicketVale).MargenDer = MargenDer
                            TryCast(Reporte, xtraRepTicketVale).MargenArr = MargenArr
                            TryCast(Reporte, xtraRepTicketValeCarta).SwReimpresion = True
                            TryCast(Reporte, xtraRepTicketVale).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                            TryCast(Reporte, xtraRepTicketVale).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketVale).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVale).PrintingSystem)
                        Case "EPSON"
                            Reporte = New xtraRepTicketVale
                            'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                            TryCast(Reporte, xtraRepTicketVale).MargenAbj = MargenAba
                            TryCast(Reporte, xtraRepTicketVale).MargenIzq = MargenIzq
                            TryCast(Reporte, xtraRepTicketVale).MargenDer = MargenDer
                            TryCast(Reporte, xtraRepTicketVale).MargenArr = MargenArr
                            TryCast(Reporte, xtraRepTicketValeCarta).SwReimpresion = True
                            TryCast(Reporte, xtraRepTicketVale).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                            TryCast(Reporte, xtraRepTicketVale).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketVale).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVale).PrintingSystem)
                        Case Else
                            Reporte = New xtraRepTicketVale
                            TryCast(Reporte, xtraRepTicketVale).MargenAbj = MargenAba
                            TryCast(Reporte, xtraRepTicketVale).MargenIzq = MargenIzq
                            TryCast(Reporte, xtraRepTicketVale).MargenDer = MargenDer
                            TryCast(Reporte, xtraRepTicketVale).MargenArr = MargenArr
                            TryCast(Reporte, xtraRepTicketValeCarta).SwReimpresion = True
                            TryCast(Reporte, xtraRepTicketVale).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                            TryCast(Reporte, xtraRepTicketVale).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketVale).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVale).PrintingSystem)
                    End Select
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()
                ''lstImpresoras.SelectedIndex = 5
                ''printBase.Print(lstImpresoras.Text)


                If dtDatos.Rows(0).Item("tipo") = "DISTRIBUIDOR PROMOCION" Then
                    Reporte = New xtraRepTicketBlancoOficio
                    'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenAbj = MargenAba
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenIzq = MargenIzq
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenDer = MargenDer
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenArr = MargenArr
                    TryCast(Reporte, xtraRepTicketBlancoOficio).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                    TryCast(Reporte, xtraRepTicketBlancoOficio).ods.DataSource = oTicket
                    TryCast(Reporte, xtraRepTicketBlancoOficio).CreateDocument()
                    printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketBlancoOficio).PrintingSystem)
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()
                End If

                If dtDatos1.Rows(0).Item("id_cliente1") <> dtDatos1.Rows(0).Item("id_cliente") Then
                        Select Case Formato
                            Case "CARTA"
                                Reporte = New XtraRep_CartaPrestaNombre
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Foto = Pic1.Image
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.Empresa
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.Direccion
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)

                            Case "SEAFON"
                                Reporte = New XtraRep_CartaPrestaNombre
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Foto = Pic1.Image
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.FacEmpresa
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.FacDireccion
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                            Case "EPSON"
                                Reporte = New XtraRep_CartaPrestaNombre
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Foto = Pic1.Image
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.FacEmpresa
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.FacDireccion
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                            Case Else
                                Reporte = New XtraRep_CartaPrestaNombre
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.FacEmpresa
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.FacDireccion
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                                TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                        End Select
                        printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                        printTool.ShowPreviewDialog()
                    End If

                    If ImprimirODP Then
                        Select Case Formato
                            Case "CARTA"
                                Reporte = New xtraRepTicketValeODP
                                TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeODP).PrintingSystem)

                            Case "SEAFON"
                                Reporte = New xtraRepTicketValeODP
                                TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeODP).PrintingSystem)
                            Case "EPSON"
                                Reporte = New xtraRepTicketValeODP
                                TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeODP).PrintingSystem)
                            Case Else
                                Reporte = New xtraRepTicketValeODP
                                TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                        End Select
                        printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                        printTool.ShowPreviewDialog()
                    End If
                    If oDatos.PVTA_Recupera_Desglose1(txt_Autonumsuc.Text _
                                                    , Globales.oAmbientes.Id_Empresa _
                                                    , Globales.oAmbientes.Id_Sucursal _
                                                    , CDate("01/01/1900") _
                                                    , "" _
                                                    , "" _
                                                    , "CREDITO" _
                                                    , dtDatos _
                                                    , Mensaje) Then
                        If dtDatos.Rows.Count > 0 Then
                            If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                                    , txt_Autonumsuc.Text _
                                                    , Globales.oAmbientes.Id_Empresa _
                                                    , Globales.oAmbientes.Id_Sucursal _
                                                    , dtDatos _
                                                    , Mensaje) Then
                                If dtDatos.Rows.Count > 0 Then
                                    Dim oPagare As New entPagare
                                    oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                    oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                    oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                    oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                    oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                    oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                    oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                    oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                    oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                    oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                    Select Case Formato
                                        Case "SEAFON"
                                            Reporte = New xtraRepTicketPagare
                                            TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                            TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                            TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                            TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                            TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                            TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                        Case "EPSON"
                                            Reporte = New xtraRepTicketPagare
                                            TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                            TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                            TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                            TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                            TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                            TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                        Case Else
                                            Reporte = New xtraRepTicketPagare
                                            TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                            TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                            TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                            TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                            TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                            TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                    End Select
                                    printBase.Print(lstImpresoras.Text)

                                End If
                            End If
                        End If
                    End If
                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class