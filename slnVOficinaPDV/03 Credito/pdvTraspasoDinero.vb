﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvTraspasoDinero
    Dim _Accion As Boolean
    Dim MonedaNacional As String = ""
    Dim _IdEmpresa As String
    Dim _IdSucursal As String
    Dim _Impresora As String
    Dim _TipoOperacion As String
    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public Property Impresora As String
        Get
            Return _Impresora
        End Get
        Set(value As String)
            _Impresora = value
        End Set
    End Property

    Public Property IdSucursal As String
        Get
            Return _IdSucursal
        End Get
        Set(value As String)
            _IdSucursal = value
        End Set
    End Property

    Public Property IdEmpresa As String
        Get
            Return _IdEmpresa
        End Get
        Set(value As String)
            _IdEmpresa = value
        End Set
    End Property

    Public Property TipoOperacion As String
        Get
            Return _TipoOperacion
        End Get
        Set(value As String)
            _TipoOperacion = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Try

            Cbo_Bovedas.Enabled = False
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_ctabancos_BOVEDA_Sel(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, Globales.oAmbientes.oUsuario.Id_usuario, dtDatos, Msj) Then
                Cbo_Bovedas.DataSource = dtDatos
                Cbo_Bovedas.Refresh()
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If

            If TipoOperacion = "CAJA" Then
                RG_Cajas.SelectedIndex = 0
            Else
                RG_Cajas.SelectedIndex = 1
            End If
            ''Call RG_TipoCortes_SelectedIndexChanged(Nothing, Nothing)
            txt_Total.Text = Format(0.0, "C2")

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub
    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Aceptar_Click_1(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim Msj As String = ""
        Dim Origen As String = ""
        Dim Destino As String = ""
        Dim oDatos As New Datos_Viscoi
        Try
            _Accion = False

            If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
                Origen = Cbo_Bovedas.SelectedValue
                Destino = txt_Caja.Text
            Else
                Origen = txt_Caja.Text
                Destino = Cbo_Bovedas.SelectedValue
            End If

            If oDatos.PVTA_Traspaso_Dinero_INS(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , Origen _
                                        , Destino _
                                        , Globales.oAmbientes.oUsuario.Id_usuario _
                                        , Globales.oAmbientes.Valor(txt_Total.Text) _
                                        , Msj) Then
                MessageBox.Show("Entrega Efectuada con Exito.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)

                Me.Close()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Cancelar_Click_1(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
        Catch ex As Exception
            _Accion = False
        End Try
    End Sub
    Private Sub pdvEntregas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim oDatos = New Datos_Viscoi
        Dim Mensaje As String = ""
        If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, txt_Cajero.Text, 0, Mensaje) Then
            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
                ''no hago nada
            Else
                Application.Exit()
            End If
        End If
    End Sub

    Private Sub RG_Cajas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RG_Cajas.SelectedIndexChanged
        Dim oDatos = New Datos_Viscoi
        Dim Mensaje As String = ""
        Dim caja As String = ""
        If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
            Cbo_Bovedas.Enabled = True
            caja = Cbo_Bovedas.SelectedValue
            Lbl_Sentido.Text = "<<======================="
        Else
            Cbo_Bovedas.Enabled = False
            caja = txt_Caja.Text
            Lbl_Sentido.Text = "=======================>>"
        End If
        If Not caja Is Nothing Then

            If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, caja, txt_Cajero.Text, 0, Mensaje) Then
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()

            End If
        Else
            MessageBox.Show("Debe seleccionar una Caja", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End If
    End Sub
End Class