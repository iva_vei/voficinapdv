﻿Imports System.Drawing.Printing

Public Class xtraRepTicketBlancoOficio
    Public SwReimpresion As Boolean = False
    Public SwDevolucion As Boolean = False
    Public TipoCliente As String
    Public MargenDer As Integer = 0
    Public MargenIzq As Integer = 0
    Public MargenArr As Integer = 0
    Public MargenAbj As Integer = 0
    Public Contenido As String = ""

    Private Sub xtraRepTicketVenta_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Me.Margins.Right = Me.Margins.Right + MargenDer
        Me.Margins.Left = Me.Margins.Left + MargenIzq
        Me.Margins.Top = Me.Margins.Top + MargenArr
        Me.Margins.Bottom = Me.Margins.Bottom + MargenAbj
        Try
            ''Me.Watermark.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Factura_WaterMark.png"))
        Catch ex As Exception

        End Try
        If SwReimpresion Then
            ''Me.Watermark.Text = "REIMPRESION"
            Me.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal
            ''.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Reimpresion.png"))
        End If

    End Sub

End Class