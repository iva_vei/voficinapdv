﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCanjes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvCanjes))
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions3 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions4 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions5 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions6 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions7 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions8 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim WindowsUIButtonImageOptions9 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.txt_MensajeNotificacion = New System.Windows.Forms.TextBox()
        Me.txt_ClienteCondicion = New System.Windows.Forms.TextBox()
        Me.txt_ClienteEstatus = New System.Windows.Forms.TextBox()
        Me.txt_PlanCredito = New System.Windows.Forms.TextBox()
        Me.txt_Estatus = New System.Windows.Forms.TextBox()
        Me.txt_IdLealtad = New System.Windows.Forms.TextBox()
        Me.txt_PinPad = New System.Windows.Forms.TextBox()
        Me.txt_MargenesTicket = New System.Windows.Forms.TextBox()
        Me.txt_FormatoTicket = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txt_Corte = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txt_ClienteDescto = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_Referencia = New System.Windows.Forms.TextBox()
        Me.txt_DesctoGlobal = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.txt_Pediddo = New System.Windows.Forms.TextBox()
        Me.txt_Vendedor2 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txt_Sucursal = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txt_NombreCanje = New System.Windows.Forms.TextBox()
        Me.txt_CodigoCanje = New System.Windows.Forms.TextBox()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.txt_Vendedor1 = New System.Windows.Forms.TextBox()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Cbo_Estado = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btn_BuscarColonia = New DevExpress.XtraEditors.SimpleButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_CP = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_Colonia = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.cbo_Bancos = New System.Windows.Forms.TextBox()
        Me.Cbo_Banco_Tipo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Txt_Banco_Cuenta = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.Txt_FolioDigital = New DevExpress.XtraEditors.TextEdit()
        Me.Btn_FolioDigital = New DevExpress.XtraEditors.SimpleButton()
        Me.cboTipoCredito = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cbo_Serie = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_IdCliente = New DevExpress.XtraEditors.TextEdit()
        Me.txt_ClienteNombre = New DevExpress.XtraEditors.TextEdit()
        Me.btn_Cliente = New DevExpress.XtraEditors.SimpleButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lbl_ClienteFinal = New System.Windows.Forms.Label()
        Me.cbo_Desembolso = New System.Windows.Forms.ComboBox()
        Me.txt_IdClienteFinal = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Importe = New DevExpress.XtraEditors.TextEdit()
        Me.txt_NombreClienteFinal = New DevExpress.XtraEditors.TextEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_FinalCliente = New DevExpress.XtraEditors.SimpleButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbl_Serie = New System.Windows.Forms.Label()
        Me.Cbo_Plazos = New System.Windows.Forms.ComboBox()
        Me.txt_Folio = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_Folio = New System.Windows.Forms.Label()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_Subtotal = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Autonumsuc = New DevExpress.XtraEditors.TextEdit()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_ImporteTotal = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Total = New DevExpress.XtraEditors.TextEdit()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.hideContainerRight = New DevExpress.XtraBars.Docking.AutoHideContainer()
        Me.CG_Ayuda = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.GridC_2 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col2_Accion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col2_Combinacion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.lbl_Mensaje = New System.Windows.Forms.Label()
        Me.txt_Calle = New System.Windows.Forms.TextBox()
        Me.Cbo_Municipio = New System.Windows.Forms.TextBox()
        Me.txt_NumInt = New System.Windows.Forms.TextBox()
        Me.txt_NumExt = New System.Windows.Forms.TextBox()
        Me.PanelFiltros_Acciones.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.Txt_FolioDigital.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IdCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ClienteNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IdClienteFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Importe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NombreClienteFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Folio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txt_Subtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Autonumsuc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ImporteTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hideContainerRight.SuspendLayout()
        Me.CG_Ayuda.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.Image = CType(resources.GetObject("WindowsUIButtonImageOptions1.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions2.Image = CType(resources.GetObject("WindowsUIButtonImageOptions2.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions3.ImageUri.Uri = "icon%20builder/actions_question"
        WindowsUIButtonImageOptions4.EnableTransparency = True
        WindowsUIButtonImageOptions4.ImageUri.Uri = "New;Size32x32;GrayScaled"
        WindowsUIButtonImageOptions5.Image = CType(resources.GetObject("WindowsUIButtonImageOptions5.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions6.ImageUri.Uri = "Edit/Delete;Size32x32;GrayScaled"
        WindowsUIButtonImageOptions7.Image = CType(resources.GetObject("WindowsUIButtonImageOptions7.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions7.ImageUri.Uri = "Add"
        WindowsUIButtonImageOptions8.Image = CType(resources.GetObject("WindowsUIButtonImageOptions8.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions8.ImageUri.Uri = "Apply"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cliente Nuevo", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "CliNuevo", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Cliente Modificar", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "CliModificar", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUISeparator(), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Ayuda", True, WindowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Ayuda", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Nuevo", True, WindowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Nuevo", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Codigo", True, WindowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Codigo", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Eliminar", True, WindowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Eliminar", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUISeparator(), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Articulos", True, WindowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Articulos", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Terminar", True, WindowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Terminar", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Facturar", True, WindowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, False, "Facturar", -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_MensajeNotificacion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_ClienteCondicion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_ClienteEstatus)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_PlanCredito)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Estatus)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_IdLealtad)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_PinPad)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_MargenesTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_FormatoTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label34)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Corte)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label33)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_ClienteDescto)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Referencia)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_DesctoGlobal)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label31)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lstImpresoras)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_RFC)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Empresa)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_TipoCliente)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Pediddo)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Vendedor2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label36)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Sucursal)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label35)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_NombreCanje)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_CodigoCanje)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Cajero)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Caja)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Vendedor1)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(0, 734)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 80)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(87, 80)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(1497, 80)
        Me.PanelFiltros_Acciones.TabIndex = 4
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'txt_MensajeNotificacion
        '
        Me.txt_MensajeNotificacion.Location = New System.Drawing.Point(983, 60)
        Me.txt_MensajeNotificacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_MensajeNotificacion.Name = "txt_MensajeNotificacion"
        Me.txt_MensajeNotificacion.Size = New System.Drawing.Size(39, 22)
        Me.txt_MensajeNotificacion.TabIndex = 34
        Me.txt_MensajeNotificacion.Visible = False
        '
        'txt_ClienteCondicion
        '
        Me.txt_ClienteCondicion.Location = New System.Drawing.Point(935, 62)
        Me.txt_ClienteCondicion.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_ClienteCondicion.Name = "txt_ClienteCondicion"
        Me.txt_ClienteCondicion.Size = New System.Drawing.Size(39, 22)
        Me.txt_ClienteCondicion.TabIndex = 33
        Me.txt_ClienteCondicion.Visible = False
        '
        'txt_ClienteEstatus
        '
        Me.txt_ClienteEstatus.Location = New System.Drawing.Point(885, 62)
        Me.txt_ClienteEstatus.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_ClienteEstatus.Name = "txt_ClienteEstatus"
        Me.txt_ClienteEstatus.Size = New System.Drawing.Size(39, 22)
        Me.txt_ClienteEstatus.TabIndex = 32
        Me.txt_ClienteEstatus.Visible = False
        '
        'txt_PlanCredito
        '
        Me.txt_PlanCredito.Location = New System.Drawing.Point(839, 60)
        Me.txt_PlanCredito.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_PlanCredito.Name = "txt_PlanCredito"
        Me.txt_PlanCredito.Size = New System.Drawing.Size(39, 22)
        Me.txt_PlanCredito.TabIndex = 31
        Me.txt_PlanCredito.Visible = False
        '
        'txt_Estatus
        '
        Me.txt_Estatus.Location = New System.Drawing.Point(1271, 9)
        Me.txt_Estatus.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Estatus.Name = "txt_Estatus"
        Me.txt_Estatus.Size = New System.Drawing.Size(39, 22)
        Me.txt_Estatus.TabIndex = 30
        Me.txt_Estatus.Visible = False
        '
        'txt_IdLealtad
        '
        Me.txt_IdLealtad.Location = New System.Drawing.Point(1271, 36)
        Me.txt_IdLealtad.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_IdLealtad.Name = "txt_IdLealtad"
        Me.txt_IdLealtad.Size = New System.Drawing.Size(39, 22)
        Me.txt_IdLealtad.TabIndex = 29
        Me.txt_IdLealtad.Visible = False
        '
        'txt_PinPad
        '
        Me.txt_PinPad.Location = New System.Drawing.Point(1223, 9)
        Me.txt_PinPad.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_PinPad.Name = "txt_PinPad"
        Me.txt_PinPad.Size = New System.Drawing.Size(39, 22)
        Me.txt_PinPad.TabIndex = 28
        Me.txt_PinPad.Visible = False
        '
        'txt_MargenesTicket
        '
        Me.txt_MargenesTicket.Location = New System.Drawing.Point(1223, 36)
        Me.txt_MargenesTicket.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_MargenesTicket.Name = "txt_MargenesTicket"
        Me.txt_MargenesTicket.Size = New System.Drawing.Size(39, 22)
        Me.txt_MargenesTicket.TabIndex = 27
        Me.txt_MargenesTicket.Visible = False
        '
        'txt_FormatoTicket
        '
        Me.txt_FormatoTicket.Location = New System.Drawing.Point(1175, 36)
        Me.txt_FormatoTicket.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_FormatoTicket.Name = "txt_FormatoTicket"
        Me.txt_FormatoTicket.Size = New System.Drawing.Size(39, 22)
        Me.txt_FormatoTicket.TabIndex = 26
        Me.txt_FormatoTicket.Visible = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(632, 42)
        Me.Label34.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(70, 29)
        Me.Label34.TabIndex = 24
        Me.Label34.Text = "Corte"
        '
        'txt_Corte
        '
        Me.txt_Corte.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.txt_Corte.Location = New System.Drawing.Point(724, 42)
        Me.txt_Corte.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Corte.Name = "txt_Corte"
        Me.txt_Corte.ReadOnly = True
        Me.txt_Corte.Size = New System.Drawing.Size(100, 36)
        Me.txt_Corte.TabIndex = 23
        Me.txt_Corte.Text = "0001"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(632, 4)
        Me.Label33.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(82, 29)
        Me.Label33.TabIndex = 22
        Me.Label33.Text = "Cajero"
        '
        'txt_ClienteDescto
        '
        Me.txt_ClienteDescto.Location = New System.Drawing.Point(887, 10)
        Me.txt_ClienteDescto.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_ClienteDescto.Name = "txt_ClienteDescto"
        Me.txt_ClienteDescto.Size = New System.Drawing.Size(39, 22)
        Me.txt_ClienteDescto.TabIndex = 21
        Me.txt_ClienteDescto.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Location = New System.Drawing.Point(1127, 36)
        Me.txt_Direccion2.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(39, 22)
        Me.txt_Direccion2.TabIndex = 20
        Me.txt_Direccion2.Visible = False
        '
        'txt_Referencia
        '
        Me.txt_Referencia.Location = New System.Drawing.Point(1127, 10)
        Me.txt_Referencia.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Referencia.Name = "txt_Referencia"
        Me.txt_Referencia.Size = New System.Drawing.Size(39, 22)
        Me.txt_Referencia.TabIndex = 19
        Me.txt_Referencia.Visible = False
        '
        'txt_DesctoGlobal
        '
        Me.txt_DesctoGlobal.Location = New System.Drawing.Point(1079, 36)
        Me.txt_DesctoGlobal.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_DesctoGlobal.Name = "txt_DesctoGlobal"
        Me.txt_DesctoGlobal.Size = New System.Drawing.Size(39, 22)
        Me.txt_DesctoGlobal.TabIndex = 17
        Me.txt_DesctoGlobal.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(9, 46)
        Me.Label31.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(131, 29)
        Me.Label31.TabIndex = 16
        Me.Label31.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(155, 48)
        Me.lstImpresoras.Margin = New System.Windows.Forms.Padding(4)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(465, 24)
        Me.lstImpresoras.TabIndex = 15
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Location = New System.Drawing.Point(1031, 36)
        Me.txt_Direccion.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(39, 22)
        Me.txt_Direccion.TabIndex = 14
        Me.txt_Direccion.Visible = False
        '
        'txt_RFC
        '
        Me.txt_RFC.Location = New System.Drawing.Point(1079, 10)
        Me.txt_RFC.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(39, 22)
        Me.txt_RFC.TabIndex = 13
        Me.txt_RFC.Visible = False
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Location = New System.Drawing.Point(1031, 10)
        Me.txt_Empresa.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(39, 22)
        Me.txt_Empresa.TabIndex = 12
        Me.txt_Empresa.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Location = New System.Drawing.Point(983, 36)
        Me.txt_TipoCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(39, 22)
        Me.txt_TipoCliente.TabIndex = 11
        Me.txt_TipoCliente.Visible = False
        '
        'txt_Pediddo
        '
        Me.txt_Pediddo.Location = New System.Drawing.Point(935, 36)
        Me.txt_Pediddo.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Pediddo.Name = "txt_Pediddo"
        Me.txt_Pediddo.Size = New System.Drawing.Size(39, 22)
        Me.txt_Pediddo.TabIndex = 10
        Me.txt_Pediddo.Text = "0001"
        Me.txt_Pediddo.Visible = False
        '
        'txt_Vendedor2
        '
        Me.txt_Vendedor2.Location = New System.Drawing.Point(839, 36)
        Me.txt_Vendedor2.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Vendedor2.Name = "txt_Vendedor2"
        Me.txt_Vendedor2.Size = New System.Drawing.Size(39, 22)
        Me.txt_Vendedor2.TabIndex = 8
        Me.txt_Vendedor2.Text = "0001        "
        Me.txt_Vendedor2.Visible = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(29, 4)
        Me.Label36.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(109, 29)
        Me.Label36.TabIndex = 7
        Me.Label36.Text = "Sucursal:"
        '
        'txt_Sucursal
        '
        Me.txt_Sucursal.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Sucursal.Location = New System.Drawing.Point(155, 4)
        Me.txt_Sucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Sucursal.Name = "txt_Sucursal"
        Me.txt_Sucursal.ReadOnly = True
        Me.txt_Sucursal.Size = New System.Drawing.Size(172, 36)
        Me.txt_Sucursal.TabIndex = 6
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(370, 4)
        Me.Label35.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(68, 29)
        Me.Label35.TabIndex = 5
        Me.Label35.Text = "Caja:"
        '
        'txt_NombreCanje
        '
        Me.txt_NombreCanje.Location = New System.Drawing.Point(983, 10)
        Me.txt_NombreCanje.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_NombreCanje.Name = "txt_NombreCanje"
        Me.txt_NombreCanje.Size = New System.Drawing.Size(39, 22)
        Me.txt_NombreCanje.TabIndex = 4
        Me.txt_NombreCanje.Text = "0001"
        Me.txt_NombreCanje.Visible = False
        '
        'txt_CodigoCanje
        '
        Me.txt_CodigoCanje.Location = New System.Drawing.Point(935, 10)
        Me.txt_CodigoCanje.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_CodigoCanje.Name = "txt_CodigoCanje"
        Me.txt_CodigoCanje.Size = New System.Drawing.Size(39, 22)
        Me.txt_CodigoCanje.TabIndex = 3
        Me.txt_CodigoCanje.Text = "0001"
        Me.txt_CodigoCanje.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.txt_Cajero.Location = New System.Drawing.Point(724, 4)
        Me.txt_Cajero.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.ReadOnly = True
        Me.txt_Cajero.Size = New System.Drawing.Size(100, 36)
        Me.txt_Cajero.TabIndex = 2
        Me.txt_Cajero.Text = "0001"
        '
        'txt_Caja
        '
        Me.txt_Caja.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Caja.Location = New System.Drawing.Point(448, 4)
        Me.txt_Caja.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.ReadOnly = True
        Me.txt_Caja.Size = New System.Drawing.Size(172, 36)
        Me.txt_Caja.TabIndex = 1
        Me.txt_Caja.Text = "CAJA 0001"
        '
        'txt_Vendedor1
        '
        Me.txt_Vendedor1.Location = New System.Drawing.Point(839, 10)
        Me.txt_Vendedor1.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Vendedor1.Name = "txt_Vendedor1"
        Me.txt_Vendedor1.Size = New System.Drawing.Size(39, 22)
        Me.txt_Vendedor1.TabIndex = 0
        Me.txt_Vendedor1.Text = "0001        "
        Me.txt_Vendedor1.Visible = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Panel1MinSize = 21
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer3)
        Me.SplitContainer1.Size = New System.Drawing.Size(1497, 734)
        Me.SplitContainer1.SplitterDistance = 1006
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 5
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Margin = New System.Windows.Forms.Padding(4)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupControl3)
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupControl5)
        Me.SplitContainer2.Panel1MinSize = 300
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.PanelControl1)
        Me.SplitContainer2.Size = New System.Drawing.Size(1006, 734)
        Me.SplitContainer2.SplitterDistance = 565
        Me.SplitContainer2.SplitterWidth = 5
        Me.SplitContainer2.TabIndex = 23
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl2.Controls.Add(Me.Cbo_Municipio)
        Me.GroupControl2.Controls.Add(Me.Label13)
        Me.GroupControl2.Controls.Add(Me.Cbo_Estado)
        Me.GroupControl2.Controls.Add(Me.Label14)
        Me.GroupControl2.Controls.Add(Me.btn_BuscarColonia)
        Me.GroupControl2.Controls.Add(Me.Label17)
        Me.GroupControl2.Controls.Add(Me.txt_Calle)
        Me.GroupControl2.Controls.Add(Me.txt_CP)
        Me.GroupControl2.Controls.Add(Me.txt_NumInt)
        Me.GroupControl2.Controls.Add(Me.Label20)
        Me.GroupControl2.Controls.Add(Me.txt_Colonia)
        Me.GroupControl2.Controls.Add(Me.txt_NumExt)
        Me.GroupControl2.Controls.Add(Me.Label22)
        Me.GroupControl2.Location = New System.Drawing.Point(0, 408)
        Me.GroupControl2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(1006, 119)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "DIRECCION"
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(7, 75)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(128, 30)
        Me.Label13.TabIndex = 73
        Me.Label13.Text = "Municipio"
        '
        'Cbo_Estado
        '
        Me.Cbo_Estado.BackColor = System.Drawing.Color.White
        Me.Cbo_Estado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Cbo_Estado.Location = New System.Drawing.Point(659, 71)
        Me.Cbo_Estado.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbo_Estado.Name = "Cbo_Estado"
        Me.Cbo_Estado.ReadOnly = True
        Me.Cbo_Estado.Size = New System.Drawing.Size(315, 34)
        Me.Cbo_Estado.TabIndex = 4
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(528, 75)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(123, 30)
        Me.Label14.TabIndex = 70
        Me.Label14.Text = "Estado"
        '
        'btn_BuscarColonia
        '
        Me.btn_BuscarColonia.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_BuscarColonia.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_BuscarColonia.Location = New System.Drawing.Point(231, 293)
        Me.btn_BuscarColonia.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_BuscarColonia.Name = "btn_BuscarColonia"
        Me.btn_BuscarColonia.Size = New System.Drawing.Size(85, 37)
        Me.btn_BuscarColonia.TabIndex = 66
        Me.btn_BuscarColonia.Text = "Buscar"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(7, 28)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(125, 30)
        Me.Label17.TabIndex = 52
        Me.Label17.Text = "Calle"
        '
        'txt_CP
        '
        Me.txt_CP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_CP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CP.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CP.Location = New System.Drawing.Point(7, 293)
        Me.txt_CP.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_CP.Name = "txt_CP"
        Me.txt_CP.Size = New System.Drawing.Size(576, 34)
        Me.txt_CP.TabIndex = 4
        Me.txt_CP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(7, 258)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(322, 30)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "C. P."
        '
        'txt_Colonia
        '
        Me.txt_Colonia.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Colonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Colonia.Enabled = False
        Me.txt_Colonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Colonia.Location = New System.Drawing.Point(328, 294)
        Me.txt_Colonia.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Colonia.Name = "txt_Colonia"
        Me.txt_Colonia.Size = New System.Drawing.Size(670, 34)
        Me.txt_Colonia.TabIndex = 5
        Me.txt_Colonia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label22
        '
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(331, 261)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(307, 30)
        Me.Label22.TabIndex = 56
        Me.Label22.Text = "Colonia"
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.Controls.Add(Me.cbo_Bancos)
        Me.GroupControl3.Controls.Add(Me.Cbo_Banco_Tipo)
        Me.GroupControl3.Controls.Add(Me.Label11)
        Me.GroupControl3.Controls.Add(Me.Txt_Banco_Cuenta)
        Me.GroupControl3.Controls.Add(Me.Label12)
        Me.GroupControl3.Controls.Add(Me.Label18)
        Me.GroupControl3.Location = New System.Drawing.Point(0, 289)
        Me.GroupControl3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(1006, 111)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "DATOS BANCARIOS"
        '
        'cbo_Bancos
        '
        Me.cbo_Bancos.BackColor = System.Drawing.Color.White
        Me.cbo_Bancos.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Bancos.Location = New System.Drawing.Point(341, 62)
        Me.cbo_Bancos.Margin = New System.Windows.Forms.Padding(4)
        Me.cbo_Bancos.MaximumSize = New System.Drawing.Size(332, 29)
        Me.cbo_Bancos.Name = "cbo_Bancos"
        Me.cbo_Bancos.ReadOnly = True
        Me.cbo_Bancos.Size = New System.Drawing.Size(308, 34)
        Me.cbo_Bancos.TabIndex = 1
        '
        'Cbo_Banco_Tipo
        '
        Me.Cbo_Banco_Tipo.BackColor = System.Drawing.Color.White
        Me.Cbo_Banco_Tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Cbo_Banco_Tipo.Location = New System.Drawing.Point(686, 62)
        Me.Cbo_Banco_Tipo.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbo_Banco_Tipo.MaximumSize = New System.Drawing.Size(332, 29)
        Me.Cbo_Banco_Tipo.Name = "Cbo_Banco_Tipo"
        Me.Cbo_Banco_Tipo.ReadOnly = True
        Me.Cbo_Banco_Tipo.Size = New System.Drawing.Size(292, 29)
        Me.Cbo_Banco_Tipo.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(4, 30)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(306, 27)
        Me.Label11.TabIndex = 52
        Me.Label11.Text = "Cuenta"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Txt_Banco_Cuenta
        '
        Me.Txt_Banco_Cuenta.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Txt_Banco_Cuenta.BackColor = System.Drawing.Color.White
        Me.Txt_Banco_Cuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Banco_Cuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Banco_Cuenta.Location = New System.Drawing.Point(7, 62)
        Me.Txt_Banco_Cuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.Txt_Banco_Cuenta.Name = "Txt_Banco_Cuenta"
        Me.Txt_Banco_Cuenta.ReadOnly = True
        Me.Txt_Banco_Cuenta.Size = New System.Drawing.Size(303, 34)
        Me.Txt_Banco_Cuenta.TabIndex = 0
        Me.Txt_Banco_Cuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(690, 30)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(284, 30)
        Me.Label12.TabIndex = 65
        Me.Label12.Text = "Tipo Cuenta"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(340, 30)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(309, 30)
        Me.Label18.TabIndex = 63
        Me.Label18.Text = "Banco"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl5.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl5.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl5.Controls.Add(Me.lbl_Mensaje)
        Me.GroupControl5.Controls.Add(Me.Txt_FolioDigital)
        Me.GroupControl5.Controls.Add(Me.Btn_FolioDigital)
        Me.GroupControl5.Controls.Add(Me.cboTipoCredito)
        Me.GroupControl5.Controls.Add(Me.Label1)
        Me.GroupControl5.Controls.Add(Me.Cbo_Serie)
        Me.GroupControl5.Controls.Add(Me.Label7)
        Me.GroupControl5.Controls.Add(Me.txt_IdCliente)
        Me.GroupControl5.Controls.Add(Me.txt_ClienteNombre)
        Me.GroupControl5.Controls.Add(Me.btn_Cliente)
        Me.GroupControl5.Controls.Add(Me.Label9)
        Me.GroupControl5.Controls.Add(Me.lbl_ClienteFinal)
        Me.GroupControl5.Controls.Add(Me.cbo_Desembolso)
        Me.GroupControl5.Controls.Add(Me.txt_IdClienteFinal)
        Me.GroupControl5.Controls.Add(Me.Txt_Importe)
        Me.GroupControl5.Controls.Add(Me.txt_NombreClienteFinal)
        Me.GroupControl5.Controls.Add(Me.Label6)
        Me.GroupControl5.Controls.Add(Me.btn_FinalCliente)
        Me.GroupControl5.Controls.Add(Me.Label4)
        Me.GroupControl5.Controls.Add(Me.lbl_Serie)
        Me.GroupControl5.Controls.Add(Me.Cbo_Plazos)
        Me.GroupControl5.Controls.Add(Me.txt_Folio)
        Me.GroupControl5.Controls.Add(Me.lbl_Folio)
        Me.GroupControl5.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl5.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(1006, 287)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "DATOS GENERALES"
        '
        'Txt_FolioDigital
        '
        Me.Txt_FolioDigital.Location = New System.Drawing.Point(777, 32)
        Me.Txt_FolioDigital.Margin = New System.Windows.Forms.Padding(4)
        Me.Txt_FolioDigital.Name = "Txt_FolioDigital"
        Me.Txt_FolioDigital.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_FolioDigital.Properties.Appearance.Options.UseFont = True
        Me.Txt_FolioDigital.Properties.Appearance.Options.UseTextOptions = True
        Me.Txt_FolioDigital.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Txt_FolioDigital.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_FolioDigital.Size = New System.Drawing.Size(201, 36)
        Me.Txt_FolioDigital.TabIndex = 45
        '
        'Btn_FolioDigital
        '
        Me.Btn_FolioDigital.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.Btn_FolioDigital.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.Btn_FolioDigital.Location = New System.Drawing.Point(660, 32)
        Me.Btn_FolioDigital.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_FolioDigital.Name = "Btn_FolioDigital"
        Me.Btn_FolioDigital.Size = New System.Drawing.Size(109, 37)
        Me.Btn_FolioDigital.TabIndex = 44
        Me.Btn_FolioDigital.Text = "Folio Digital"
        '
        'cboTipoCredito
        '
        Me.cboTipoCredito.DisplayMember = "valor"
        Me.cboTipoCredito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoCredito.FormattingEnabled = True
        Me.cboTipoCredito.Items.AddRange(New Object() {"04", "06", "08", "10", "12"})
        Me.cboTipoCredito.Location = New System.Drawing.Point(167, 32)
        Me.cboTipoCredito.Margin = New System.Windows.Forms.Padding(4)
        Me.cboTipoCredito.Name = "cboTipoCredito"
        Me.cboTipoCredito.Size = New System.Drawing.Size(484, 37)
        Me.cboTipoCredito.TabIndex = 43
        Me.cboTipoCredito.ValueMember = "valor"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 36)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 29)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "T. Credito"
        '
        'Cbo_Serie
        '
        Me.Cbo_Serie.DisplayMember = "valor"
        Me.Cbo_Serie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cbo_Serie.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cbo_Serie.FormattingEnabled = True
        Me.Cbo_Serie.Items.AddRange(New Object() {"04", "06", "08", "10", "12"})
        Me.Cbo_Serie.Location = New System.Drawing.Point(98, 162)
        Me.Cbo_Serie.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbo_Serie.Name = "Cbo_Serie"
        Me.Cbo_Serie.Size = New System.Drawing.Size(68, 37)
        Me.Cbo_Serie.TabIndex = 41
        Me.Cbo_Serie.ValueMember = "valor"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 78)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(137, 29)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Distribuidor"
        '
        'txt_IdCliente
        '
        Me.txt_IdCliente.Location = New System.Drawing.Point(167, 75)
        Me.txt_IdCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_IdCliente.Name = "txt_IdCliente"
        Me.txt_IdCliente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IdCliente.Properties.Appearance.Options.UseFont = True
        Me.txt_IdCliente.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IdCliente.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_IdCliente.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IdCliente.Size = New System.Drawing.Size(124, 36)
        Me.txt_IdCliente.TabIndex = 0
        '
        'txt_ClienteNombre
        '
        Me.txt_ClienteNombre.Location = New System.Drawing.Point(352, 75)
        Me.txt_ClienteNombre.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_ClienteNombre.Name = "txt_ClienteNombre"
        Me.txt_ClienteNombre.Properties.AllowFocused = False
        Me.txt_ClienteNombre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteNombre.Properties.Appearance.Options.UseFont = True
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_ClienteNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ClienteNombre.Properties.ReadOnly = True
        Me.txt_ClienteNombre.Size = New System.Drawing.Size(626, 36)
        Me.txt_ClienteNombre.TabIndex = 2
        '
        'btn_Cliente
        '
        Me.btn_Cliente.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.Location = New System.Drawing.Point(299, 75)
        Me.btn_Cliente.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_Cliente.Name = "btn_Cliente"
        Me.btn_Cliente.Size = New System.Drawing.Size(48, 37)
        Me.btn_Cliente.TabIndex = 1
        Me.btn_Cliente.Text = "..."
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(162, 212)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(150, 29)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Desembolso"
        '
        'lbl_ClienteFinal
        '
        Me.lbl_ClienteFinal.AutoSize = True
        Me.lbl_ClienteFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ClienteFinal.Location = New System.Drawing.Point(12, 123)
        Me.lbl_ClienteFinal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_ClienteFinal.Name = "lbl_ClienteFinal"
        Me.lbl_ClienteFinal.Size = New System.Drawing.Size(89, 29)
        Me.lbl_ClienteFinal.TabIndex = 30
        Me.lbl_ClienteFinal.Text = "Cliente"
        '
        'cbo_Desembolso
        '
        Me.cbo_Desembolso.DisplayMember = "valor"
        Me.cbo_Desembolso.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Desembolso.FormattingEnabled = True
        Me.cbo_Desembolso.Items.AddRange(New Object() {"01 - EFECTIVO", "02 - ODP", "03 - SPEI"})
        Me.cbo_Desembolso.Location = New System.Drawing.Point(352, 208)
        Me.cbo_Desembolso.Margin = New System.Windows.Forms.Padding(4)
        Me.cbo_Desembolso.Name = "cbo_Desembolso"
        Me.cbo_Desembolso.Size = New System.Drawing.Size(626, 37)
        Me.cbo_Desembolso.TabIndex = 10
        Me.cbo_Desembolso.ValueMember = "valor"
        '
        'txt_IdClienteFinal
        '
        Me.txt_IdClienteFinal.Enabled = False
        Me.txt_IdClienteFinal.Location = New System.Drawing.Point(167, 119)
        Me.txt_IdClienteFinal.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_IdClienteFinal.Name = "txt_IdClienteFinal"
        Me.txt_IdClienteFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IdClienteFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_IdClienteFinal.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IdClienteFinal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_IdClienteFinal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IdClienteFinal.Size = New System.Drawing.Size(124, 36)
        Me.txt_IdClienteFinal.TabIndex = 3
        '
        'Txt_Importe
        '
        Me.Txt_Importe.Location = New System.Drawing.Point(581, 162)
        Me.Txt_Importe.Margin = New System.Windows.Forms.Padding(4)
        Me.Txt_Importe.Name = "Txt_Importe"
        Me.Txt_Importe.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Importe.Properties.Appearance.Options.UseFont = True
        Me.Txt_Importe.Properties.Appearance.Options.UseTextOptions = True
        Me.Txt_Importe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Txt_Importe.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Importe.Size = New System.Drawing.Size(188, 36)
        Me.Txt_Importe.TabIndex = 8
        '
        'txt_NombreClienteFinal
        '
        Me.txt_NombreClienteFinal.Location = New System.Drawing.Point(352, 119)
        Me.txt_NombreClienteFinal.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_NombreClienteFinal.Name = "txt_NombreClienteFinal"
        Me.txt_NombreClienteFinal.Properties.AllowFocused = False
        Me.txt_NombreClienteFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreClienteFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreClienteFinal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_NombreClienteFinal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreClienteFinal.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_NombreClienteFinal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_NombreClienteFinal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_NombreClienteFinal.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_NombreClienteFinal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreClienteFinal.Properties.ReadOnly = True
        Me.txt_NombreClienteFinal.Size = New System.Drawing.Size(626, 36)
        Me.txt_NombreClienteFinal.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(476, 165)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 29)
        Me.Label6.TabIndex = 38
        Me.Label6.Text = "Importe"
        '
        'btn_FinalCliente
        '
        Me.btn_FinalCliente.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_FinalCliente.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_FinalCliente.Location = New System.Drawing.Point(299, 119)
        Me.btn_FinalCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_FinalCliente.Name = "btn_FinalCliente"
        Me.btn_FinalCliente.Size = New System.Drawing.Size(48, 37)
        Me.btn_FinalCliente.TabIndex = 4
        Me.btn_FinalCliente.Text = "..."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(777, 165)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 29)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Plazos"
        '
        'lbl_Serie
        '
        Me.lbl_Serie.AutoSize = True
        Me.lbl_Serie.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Serie.Location = New System.Drawing.Point(18, 165)
        Me.lbl_Serie.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_Serie.Name = "lbl_Serie"
        Me.lbl_Serie.Size = New System.Drawing.Size(71, 29)
        Me.lbl_Serie.TabIndex = 32
        Me.lbl_Serie.Text = "Serie"
        '
        'Cbo_Plazos
        '
        Me.Cbo_Plazos.DisplayMember = "valor"
        Me.Cbo_Plazos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cbo_Plazos.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cbo_Plazos.FormattingEnabled = True
        Me.Cbo_Plazos.Items.AddRange(New Object() {"04", "06", "08", "10", "12"})
        Me.Cbo_Plazos.Location = New System.Drawing.Point(872, 162)
        Me.Cbo_Plazos.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbo_Plazos.Name = "Cbo_Plazos"
        Me.Cbo_Plazos.Size = New System.Drawing.Size(106, 37)
        Me.Cbo_Plazos.TabIndex = 9
        Me.Cbo_Plazos.ValueMember = "valor"
        '
        'txt_Folio
        '
        Me.txt_Folio.Location = New System.Drawing.Point(352, 162)
        Me.txt_Folio.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Folio.Name = "txt_Folio"
        Me.txt_Folio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Folio.Properties.Appearance.Options.UseFont = True
        Me.txt_Folio.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Folio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Folio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Folio.Size = New System.Drawing.Size(117, 36)
        Me.txt_Folio.TabIndex = 7
        '
        'lbl_Folio
        '
        Me.lbl_Folio.AutoSize = True
        Me.lbl_Folio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Folio.Location = New System.Drawing.Point(174, 165)
        Me.lbl_Folio.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_Folio.Name = "lbl_Folio"
        Me.lbl_Folio.Size = New System.Drawing.Size(68, 29)
        Me.lbl_Folio.TabIndex = 34
        Me.lbl_Folio.Text = "Folio"
        '
        'PanelControl1
        '
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 121)
        Me.PanelControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1006, 43)
        Me.PanelControl1.TabIndex = 10
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Margin = New System.Windows.Forms.Padding(4)
        Me.SplitContainer3.Name = "SplitContainer3"
        Me.SplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.GroupControl4)
        Me.SplitContainer3.Panel1MinSize = 300
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.GroupControl1)
        Me.SplitContainer3.Size = New System.Drawing.Size(486, 734)
        Me.SplitContainer3.SplitterDistance = 368
        Me.SplitContainer3.SplitterWidth = 5
        Me.SplitContainer3.TabIndex = 0
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl4.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl4.Controls.Add(Me.PictureBox1)
        Me.GroupControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl4.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl4.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(486, 368)
        Me.GroupControl4.TabIndex = 36
        Me.GroupControl4.Text = "FIRMA"
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Image = Global.proyVOficina_PDV.My.Resources.Resources.logo_g
        Me.PictureBox1.Location = New System.Drawing.Point(2, 28)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(482, 338)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl1.Controls.Add(Me.Label8)
        Me.GroupControl1.Controls.Add(Me.txt_Subtotal)
        Me.GroupControl1.Controls.Add(Me.txt_Autonumsuc)
        Me.GroupControl1.Controls.Add(Me.Label15)
        Me.GroupControl1.Controls.Add(Me.Label16)
        Me.GroupControl1.Controls.Add(Me.txt_ImporteTotal)
        Me.GroupControl1.Controls.Add(Me.Txt_Total)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(486, 361)
        Me.GroupControl1.TabIndex = 69
        Me.GroupControl1.Text = "PRESTAMO"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(29, 46)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(135, 36)
        Me.Label8.TabIndex = 53
        Me.Label8.Text = "SubTotal"
        '
        'txt_Subtotal
        '
        Me.txt_Subtotal.EditValue = "$00,000.00"
        Me.txt_Subtotal.Location = New System.Drawing.Point(206, 32)
        Me.txt_Subtotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Subtotal.Name = "txt_Subtotal"
        Me.txt_Subtotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Subtotal.Properties.Appearance.Options.UseFont = True
        Me.txt_Subtotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Subtotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Subtotal.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt_Subtotal.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Subtotal.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_Subtotal.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_Subtotal.Properties.AppearanceDisabled.Options.UseTextOptions = True
        Me.txt_Subtotal.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Subtotal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt_Subtotal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Subtotal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Subtotal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Subtotal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Subtotal.Properties.ReadOnly = True
        Me.txt_Subtotal.Size = New System.Drawing.Size(251, 54)
        Me.txt_Subtotal.TabIndex = 52
        '
        'txt_Autonumsuc
        '
        Me.txt_Autonumsuc.EditValue = ""
        Me.txt_Autonumsuc.Location = New System.Drawing.Point(62, 190)
        Me.txt_Autonumsuc.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Autonumsuc.Name = "txt_Autonumsuc"
        Me.txt_Autonumsuc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Autonumsuc.Properties.Appearance.Options.UseFont = True
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Black
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.White
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Autonumsuc.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Autonumsuc.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Autonumsuc.Properties.ReadOnly = True
        Me.txt_Autonumsuc.Size = New System.Drawing.Size(322, 38)
        Me.txt_Autonumsuc.TabIndex = 49
        Me.txt_Autonumsuc.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(29, 110)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(132, 36)
        Me.Label15.TabIndex = 50
        Me.Label15.Text = "Articulos"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(27, 164)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(148, 46)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = "TOTAL"
        '
        'txt_ImporteTotal
        '
        Me.txt_ImporteTotal.EditValue = "$00,000.00"
        Me.txt_ImporteTotal.Location = New System.Drawing.Point(206, 160)
        Me.txt_ImporteTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_ImporteTotal.Name = "txt_ImporteTotal"
        Me.txt_ImporteTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ImporteTotal.Properties.Appearance.Options.UseFont = True
        Me.txt_ImporteTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_ImporteTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.Options.UseTextOptions = True
        Me.txt_ImporteTotal.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_ImporteTotal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txt_ImporteTotal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ImporteTotal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_ImporteTotal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_ImporteTotal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ImporteTotal.Properties.ReadOnly = True
        Me.txt_ImporteTotal.Size = New System.Drawing.Size(251, 54)
        Me.txt_ImporteTotal.TabIndex = 48
        '
        'Txt_Total
        '
        Me.Txt_Total.EditValue = "$00,000.00"
        Me.Txt_Total.Location = New System.Drawing.Point(206, 96)
        Me.Txt_Total.Margin = New System.Windows.Forms.Padding(4)
        Me.Txt_Total.Name = "Txt_Total"
        Me.Txt_Total.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Total.Properties.Appearance.Options.UseFont = True
        Me.Txt_Total.Properties.Appearance.Options.UseTextOptions = True
        Me.Txt_Total.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Txt_Total.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Txt_Total.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Total.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.Txt_Total.Properties.AppearanceDisabled.Options.UseFont = True
        Me.Txt_Total.Properties.AppearanceDisabled.Options.UseTextOptions = True
        Me.Txt_Total.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.Txt_Total.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Txt_Total.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Total.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.Txt_Total.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.Txt_Total.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Total.Properties.ReadOnly = True
        Me.Txt_Total.Size = New System.Drawing.Size(251, 54)
        Me.Txt_Total.TabIndex = 47
        '
        'DockManager1
        '
        Me.DockManager1.AutoHideContainers.AddRange(New DevExpress.XtraBars.Docking.AutoHideContainer() {Me.hideContainerRight})
        Me.DockManager1.Form = Me
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'hideContainerRight
        '
        Me.hideContainerRight.BackColor = System.Drawing.SystemColors.Control
        Me.hideContainerRight.Controls.Add(Me.CG_Ayuda)
        Me.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.hideContainerRight.Location = New System.Drawing.Point(1497, 0)
        Me.hideContainerRight.Margin = New System.Windows.Forms.Padding(4)
        Me.hideContainerRight.Name = "hideContainerRight"
        Me.hideContainerRight.Size = New System.Drawing.Size(26, 814)
        '
        'CG_Ayuda
        '
        Me.CG_Ayuda.Controls.Add(Me.DockPanel1_Container)
        Me.CG_Ayuda.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.CG_Ayuda.ID = New System.Guid("fcb51672-7b52-45c0-b969-bae18c7e3395")
        Me.CG_Ayuda.Location = New System.Drawing.Point(0, 0)
        Me.CG_Ayuda.Margin = New System.Windows.Forms.Padding(4)
        Me.CG_Ayuda.Name = "CG_Ayuda"
        Me.CG_Ayuda.OriginalSize = New System.Drawing.Size(267, 200)
        Me.CG_Ayuda.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.CG_Ayuda.SavedIndex = 0
        Me.CG_Ayuda.Size = New System.Drawing.Size(267, 814)
        Me.CG_Ayuda.Text = "AYUDA - TECLAS RAPIDAS"
        Me.CG_Ayuda.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.GridC_2)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(6, 32)
        Me.DockPanel1_Container.Margin = New System.Windows.Forms.Padding(4)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(257, 778)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'GridC_2
        '
        Me.GridC_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_2.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.GridC_2.Location = New System.Drawing.Point(0, 0)
        Me.GridC_2.MainView = Me.GridV_2
        Me.GridC_2.Margin = New System.Windows.Forms.Padding(4)
        Me.GridC_2.Name = "GridC_2"
        Me.GridC_2.Size = New System.Drawing.Size(257, 778)
        Me.GridC_2.TabIndex = 10
        Me.GridC_2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_2})
        '
        'GridV_2
        '
        Me.GridV_2.ColumnPanelRowHeight = 0
        Me.GridV_2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col2_Accion, Me.col2_Combinacion})
        Me.GridV_2.DetailHeight = 430
        Me.GridV_2.FixedLineWidth = 3
        Me.GridV_2.FooterPanelHeight = 0
        Me.GridV_2.GridControl = Me.GridC_2
        Me.GridV_2.GroupRowHeight = 0
        Me.GridV_2.LevelIndent = 0
        Me.GridV_2.Name = "GridV_2"
        Me.GridV_2.OptionsView.ShowGroupPanel = False
        Me.GridV_2.PreviewIndent = 0
        Me.GridV_2.RowHeight = 0
        Me.GridV_2.ViewCaptionHeight = 0
        '
        'col2_Accion
        '
        Me.col2_Accion.Caption = "Acción"
        Me.col2_Accion.FieldName = "Accion"
        Me.col2_Accion.MinWidth = 27
        Me.col2_Accion.Name = "col2_Accion"
        Me.col2_Accion.OptionsColumn.AllowEdit = False
        Me.col2_Accion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Accion.Visible = True
        Me.col2_Accion.VisibleIndex = 0
        Me.col2_Accion.Width = 100
        '
        'col2_Combinacion
        '
        Me.col2_Combinacion.Caption = "Combinación"
        Me.col2_Combinacion.FieldName = "Combinacion"
        Me.col2_Combinacion.MinWidth = 27
        Me.col2_Combinacion.Name = "col2_Combinacion"
        Me.col2_Combinacion.OptionsColumn.AllowEdit = False
        Me.col2_Combinacion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col2_Combinacion.Visible = True
        Me.col2_Combinacion.VisibleIndex = 1
        Me.col2_Combinacion.Width = 100
        '
        'lbl_Mensaje
        '
        Me.lbl_Mensaje.BackColor = System.Drawing.Color.Red
        Me.lbl_Mensaje.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lbl_Mensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Mensaje.ForeColor = System.Drawing.Color.White
        Me.lbl_Mensaje.Location = New System.Drawing.Point(2, 255)
        Me.lbl_Mensaje.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_Mensaje.Name = "lbl_Mensaje"
        Me.lbl_Mensaje.Size = New System.Drawing.Size(1002, 30)
        Me.lbl_Mensaje.TabIndex = 71
        Me.lbl_Mensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_Calle
        '
        Me.txt_Calle.BackColor = System.Drawing.Color.White
        Me.txt_Calle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Calle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Calle.Location = New System.Drawing.Point(151, 28)
        Me.txt_Calle.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Calle.Name = "txt_Calle"
        Me.txt_Calle.ReadOnly = True
        Me.txt_Calle.Size = New System.Drawing.Size(476, 34)
        Me.txt_Calle.TabIndex = 0
        Me.txt_Calle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Cbo_Municipio
        '
        Me.Cbo_Municipio.BackColor = System.Drawing.Color.White
        Me.Cbo_Municipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cbo_Municipio.Location = New System.Drawing.Point(151, 71)
        Me.Cbo_Municipio.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbo_Municipio.Name = "Cbo_Municipio"
        Me.Cbo_Municipio.ReadOnly = True
        Me.Cbo_Municipio.Size = New System.Drawing.Size(308, 34)
        Me.Cbo_Municipio.TabIndex = 3
        '
        'txt_NumInt
        '
        Me.txt_NumInt.BackColor = System.Drawing.Color.White
        Me.txt_NumInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumInt.Location = New System.Drawing.Point(823, 32)
        Me.txt_NumInt.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_NumInt.MaximumSize = New System.Drawing.Size(139, 29)
        Me.txt_NumInt.Name = "txt_NumInt"
        Me.txt_NumInt.ReadOnly = True
        Me.txt_NumInt.Size = New System.Drawing.Size(139, 29)
        Me.txt_NumInt.TabIndex = 2
        Me.txt_NumInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_NumExt
        '
        Me.txt_NumExt.BackColor = System.Drawing.Color.White
        Me.txt_NumExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NumExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumExt.Location = New System.Drawing.Point(659, 32)
        Me.txt_NumExt.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_NumExt.MaximumSize = New System.Drawing.Size(139, 29)
        Me.txt_NumExt.Name = "txt_NumExt"
        Me.txt_NumExt.ReadOnly = True
        Me.txt_NumExt.Size = New System.Drawing.Size(139, 29)
        Me.txt_NumExt.TabIndex = 1
        Me.txt_NumExt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pdvCanjes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1523, 814)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.PanelFiltros_Acciones)
        Me.Controls.Add(Me.hideContainerRight)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "pdvCanjes"
        Me.Text = "Canje de Vales"
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.Txt_FolioDigital.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IdCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ClienteNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IdClienteFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Importe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NombreClienteFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Folio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txt_Subtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Autonumsuc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ImporteTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hideContainerRight.ResumeLayout(False)
        Me.CG_Ayuda.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.GridC_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents txt_MensajeNotificacion As TextBox
    Friend WithEvents txt_ClienteCondicion As TextBox
    Friend WithEvents txt_ClienteEstatus As TextBox
    Friend WithEvents txt_PlanCredito As TextBox
    Friend WithEvents txt_Estatus As TextBox
    Friend WithEvents txt_IdLealtad As TextBox
    Friend WithEvents txt_PinPad As TextBox
    Friend WithEvents txt_MargenesTicket As TextBox
    Friend WithEvents txt_FormatoTicket As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents txt_Corte As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents txt_ClienteDescto As TextBox
    Friend WithEvents txt_Direccion2 As TextBox
    Friend WithEvents txt_Referencia As TextBox
    Friend WithEvents txt_DesctoGlobal As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Friend WithEvents txt_Direccion As TextBox
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents txt_Empresa As TextBox
    Friend WithEvents txt_TipoCliente As TextBox
    Friend WithEvents txt_Pediddo As TextBox
    Friend WithEvents txt_Vendedor2 As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents txt_Sucursal As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents txt_NombreCanje As TextBox
    Friend WithEvents txt_CodigoCanje As TextBox
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents txt_Vendedor1 As TextBox
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents SplitContainer2 As SplitContainer
    Friend WithEvents Label4 As Label
    Friend WithEvents Cbo_Plazos As ComboBox
    Friend WithEvents txt_Folio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl_Folio As Label
    Friend WithEvents lbl_Serie As Label
    Friend WithEvents btn_FinalCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_NombreClienteFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IdClienteFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl_ClienteFinal As Label
    Friend WithEvents btn_Cliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_ClienteNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IdCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SplitContainer3 As SplitContainer
    Friend WithEvents Label9 As Label
    Friend WithEvents cbo_Desembolso As ComboBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbo_Bancos As TextBox
    Friend WithEvents Cbo_Banco_Tipo As TextBox
    Private WithEvents Label11 As Label
    Friend WithEvents Txt_Banco_Cuenta As TextBox
    Private WithEvents Label12 As Label
    Private WithEvents Label18 As Label
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Private WithEvents Label13 As Label
    Friend WithEvents Cbo_Estado As TextBox
    Private WithEvents Label14 As Label
    Friend WithEvents btn_BuscarColonia As DevExpress.XtraEditors.SimpleButton
    Private WithEvents Label17 As Label
    Friend WithEvents txt_CP As TextBox
    Private WithEvents Label20 As Label
    Friend WithEvents txt_Colonia As TextBox
    Private WithEvents Label22 As Label
    Friend WithEvents Txt_Importe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents CG_Ayuda As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents GridC_2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col2_Accion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col2_Combinacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents hideContainerRight As DevExpress.XtraBars.Docking.AutoHideContainer
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label8 As Label
    Friend WithEvents txt_Subtotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Autonumsuc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txt_ImporteTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_Total As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Cbo_Serie As ComboBox
    Friend WithEvents cboTipoCredito As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Txt_FolioDigital As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Btn_FolioDigital As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbl_Mensaje As Label
    Friend WithEvents Cbo_Municipio As TextBox
    Friend WithEvents txt_Calle As TextBox
    Friend WithEvents txt_NumInt As TextBox
    Friend WithEvents txt_NumExt As TextBox
End Class
