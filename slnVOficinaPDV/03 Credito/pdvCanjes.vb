﻿Imports DevExpress.XtraBars
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Base

Public Enum eTipoTran1
    VENTA
    MOSTRADOR
    COTIZACION
End Enum
Public Class pdvCanjes
    Private TamanoNombre As Integer
    Public TipoTran As eTipoTran
    Dim Multiplo As Integer
    Public TecladoAtajos As List(Of Principal.clsTecladoAtajos)

    Private Sub ABCPantallas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim DatosConexion() As String
        Dim oDatos As Datos_Viscoi
        Dim dtTipos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal

            SplitContainer2.SplitterDistance = 250
            Dim pd As New Printing.PrintDocument
            Dim Impresoras As String
            Dim btnContado As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Pagar")

            ' Default printer      
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            TamanoNombre = 300

            Try
                oDatos = New Datos_Viscoi
                If oDatos.Crm_Tipos_Sel(Globales.oAmbientes.Id_Empresa, "CREDITO DESEMBOLSO|" + txt_Sucursal.Text.Trim, False, "ACTIVO", dtTipos, Mensaje) Then
                    cbo_Desembolso.DataSource = dtTipos
                    If dtTipos.Rows.Count > 0 Then
                        Me.cbo_Desembolso.SelectedIndex = 0
                    End If
                End If
                If dtTipos.Rows.Count = 0 Then
                    If oDatos.Crm_Tipos_Sel(Globales.oAmbientes.Id_Empresa, "CREDITO DESEMBOLSO|" + Globales.oAmbientes.oUsuario.Perfil.Trim, False, "ACTIVO", dtTipos, Mensaje) Then
                        cbo_Desembolso.DataSource = dtTipos
                        If dtTipos.Rows.Count > 0 Then
                            Me.cbo_Desembolso.SelectedIndex = 0
                        End If
                    End If
                    If dtTipos.Rows.Count = 0 Then
                        If oDatos.Crm_Tipos_Sel(Globales.oAmbientes.Id_Empresa, "CREDITO DESEMBOLSO", False, "ACTIVO", dtTipos, Mensaje) Then

                            cbo_Desembolso.DataSource = dtTipos
                            Me.cbo_Desembolso.SelectedIndex = 0
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try

            oDatos = New Datos_Viscoi
            If oDatos.Crm_Tipos_Sel(Globales.oAmbientes.Id_Empresa, "CREDITOS", False, Me.TipoTran.ToString, dtTipos, Mensaje) Then
                cboTipoCredito.DataSource = dtTipos
                Me.cboTipoCredito.SelectedValue = "NORMAL"
                Me.cboTipoCredito.SelectedIndex = 0
            End If

            Select Case TipoTran
                Case eTipoTran.VALES
                    Me.Text = "Canje de Vales"
                    Me.lbl_Serie.Visible = True
                    Me.Cbo_Serie.Visible = True
                    Me.lbl_Folio.Visible = True
                    Me.txt_Folio.Visible = True
                    Me.lbl_ClienteFinal.Visible = True
                    Me.txt_IdClienteFinal.Visible = True
                    Me.btn_FinalCliente.Visible = True
                    Me.txt_NombreClienteFinal.Visible = True
                    Me.cboTipoCredito.Enabled = False
                Case eTipoTran.PRESTAMOS
                    Me.Text = "Prestamo Personal"
                    Me.lbl_Serie.Visible = False
                    Me.Cbo_Serie.Visible = False
                    Me.lbl_Folio.Visible = False
                    Me.txt_Folio.Visible = False
                    Me.lbl_ClienteFinal.Visible = False
                    Me.txt_IdClienteFinal.Visible = False
                    Me.btn_FinalCliente.Visible = False
                    Me.txt_NombreClienteFinal.Visible = False
                    Me.cboTipoCredito.Enabled = True
            End Select
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer

            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                lstImpresoras.Enabled = True
            Else
                lstImpresoras.Enabled = False
            End If

            GridC_2.DataSource = TecladoAtajos
            GridC_2.RefreshDataSource()
            TamanoNombre = 400

            SplitContainer1.SplitterDistance = 720
            SplitContainer2.SplitterDistance = 380
            SplitContainer3.SplitterDistance = 380
            lbl_Mensaje.Visible = False
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Mostrar_Transaccion()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi

            txt_Empresa.Text = ""
            txt_RFC.Text = ""
            txt_Direccion.Text = ""

            If oDatos.PVTA_Recupera_Transacciones(Today.Date _
                                               , Today.Date _
                                               , 0 _
                                               , txt_Autonumsuc.Text _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , txt_Sucursal.Text _
                                               , txt_Caja.Text _
                                               , txt_Caja.Text _
                                               , txt_Cajero.Text _
                                               , txt_Vendedor1.Text _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , dtDatos2 _
                                               , Mensaje) Then
                If dtDatos1.Rows.Count > 0 Then
                    txt_Empresa.Text = dtDatos1.Rows(0).Item("empresa")
                    txt_RFC.Text = dtDatos1.Rows(0).Item("rfc")
                    txt_Direccion.Text = dtDatos1.Rows(0).Item("direccion")
                    txt_Direccion2.Text = dtDatos2.Rows(0).Item("direccion")
                End If
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            txt_IdCliente.Text = ""
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub Refrescar()
        Call Mostrar_Transaccion()

    End Sub
    Private Sub Crear_Transaccion()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim IdUsuario As Integer = 0
        Dim Autonumsuc As String = ""
        Dim Corte As Integer = 0

        Dim ofrmGral As pdvDatosGenerales
        Try
            oDatos = New Datos_Viscoi
            If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_Caja.Text, txt_Cajero.Text, Corte, Mensaje) Then
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Application.Exit()
            End If
            txt_Corte.Text = Corte.ToString

            txt_ClienteNombre.Text = ""
            txt_ClienteDescto.Text = "0.0"
            txt_ClienteCondicion.Text = ""
            txt_TipoCliente.Text = ""
            txt_ClienteEstatus.Text = ""

            txt_IdClienteFinal.Text = ""
            txt_NombreClienteFinal.Text = ""
            txt_Calle.Text = ""
            txt_NumExt.Text = ""
            txt_NumInt.Text = ""
            txt_CP.Text = ""
            txt_Colonia.Text = ""
            Cbo_Municipio.Text = ""
            Cbo_Estado.Text = ""
            cbo_Bancos.Text = ""
            Cbo_Banco_Tipo.Text = ""
            Txt_Banco_Cuenta.Text = ""

            Cbo_Serie.Text = ""
            txt_Folio.Text = ""
            Txt_Importe.Text = "0"
            If Cbo_Plazos.Items.Count > 0 Then
                Cbo_Plazos.SelectedIndex = 0
            End If

            Autonumsuc = txt_Autonumsuc.Text

            If oDatos.PVTA_Inserta_Transaccion(Autonumsuc _
                                                   , Globales.oAmbientes.Id_Empresa _
                                                   , txt_Sucursal.Text _
                                                   , txt_Caja.Text _
                                                   , Today.Date _
                                                   , Format(Now(), "hh:mm:ss") _
                                                   , txt_Caja.Text _
                                                   , 0 _
                                                   , txt_Cajero.Text _
                                                   , TipoTran.ToString() _
                                                   , txt_Pediddo.Text _
                                                   , txt_Vendedor1.Text _
                                                   , IIf(txt_IdCliente.Text <> "", txt_IdCliente.Text.Trim(), txt_IdLealtad.Text) _
                                                   , txt_PlanCredito.Text.Trim() _
                                                   , 0, 0 _
                                                   , "" _
                                                   , txt_ClienteNombre.Text _
                                                   , Globales.oAmbientes.oUsuario.Id_usuario _
                                                   , 0, 0, 0, 0 _
                                                   , IIf(TipoTran = eTipoTran1.VENTA, txt_TipoCliente.Text, txt_Referencia.Text) _
                                                   , "CAPTURA" _
                                                   , "VENTA" _
                                                   , txt_ClienteNombre.Text, "", txt_Vendedor2.Text _
                                                   , 0, 0, 0, 0 _
                                                   , 0.0, 0.0, 0.0 _
                                                   , 0, 0.0, 0.0, 0.0 _
                                                   , IIf(txt_IdLealtad.Text <> "", "LEALTAD", ""), Mensaje) Then
                txt_Autonumsuc.Text = Autonumsuc

                Dim DesctoGlobal As Decimal
                If txt_ClienteDescto.Text <> "" Then
                    DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                End If

                If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                        , txt_Sucursal.Text _
                                        , Autonumsuc _
                                        , False _
                                        , DesctoGlobal _
                                        , 0 _
                                        , 0 _
                                        , 0 _
                                        , Mensaje) Then

                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

                ''txt_IdCliente.Focus()
            End If
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Cliente_Click(sender As Object, e As EventArgs) Handles btn_Cliente.Click
        Dim oFrm As pdvCuentas
        Try
            oFrm = New pdvCuentas
            oFrm.Tipo = ""
            oFrm.TipoForma = pdvCuentas.eTipoForma.Cliente
            oFrm.txt_Cuenta.Text = txt_IdClienteFinal.Text
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_IdCliente.Text = oFrm.Cuenta
                Call BuscarCuenta(txt_IdCliente.Text)
                txt_IdCliente.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_FinalCliente_Click(sender As Object, e As EventArgs) Handles btn_FinalCliente.Click
        Dim oFrm As pdvCuentasFinal
        Try
            oFrm = New pdvCuentasFinal
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_IdClienteFinal.Text = oFrm.Cuenta
                txt_NombreClienteFinal.Text = oFrm.Nombre
                BuscarFinalCuenta(txt_IdClienteFinal.Text)

            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub BuscarCuenta(ByRef Tarjeta As String)
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtSerie As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim posExt As Integer
        Dim posInt As Integer
        Try
            oDatos = New Datos_Viscoi

            txt_ClienteDescto.Text = "0.0"

            Cbo_Serie.DataSource = Nothing
            Cbo_Serie.Refresh()

            If txt_IdCliente.Text.Substring(0, 1) <> "C" Then
                If oDatos.PVTA_Recupera_Cuentas("", Tarjeta, "", "", "", "", "", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        txt_IdCliente.Text = dtDatos.Rows(0).Item("cuenta").ToString.Trim
                        txt_ClienteNombre.Text = dtDatos.Rows(0).Item("nombre").ToString.Trim
                        txt_ClienteDescto.Text = dtDatos.Rows(0).Item("descuento")
                        txt_ClienteCondicion.Text = dtDatos.Rows(0).Item("condicion")
                        txt_TipoCliente.Text = dtDatos.Rows(0).Item("tipo")
                        txt_ClienteEstatus.Text = dtDatos.Rows(0).Item("estatus")
                        txt_ClienteNombre.Focus()
                        Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "CRED_SOL_DOCUMENTO|FIRMA", txt_IdCliente.Text, 0, PictureBox1)
                        If oDatos.Credito_Vales_Series_Sel(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_IdCliente.Text, dtSerie, Mensaje) Then
                            Cbo_Serie.DataSource = dtSerie
                            Cbo_Serie.DisplayMember = "valor"
                            Cbo_Serie.ValueMember = "valor"
                        End If
                        If TipoTran = eTipoTran.PRESTAMOS Then
                            If Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("dircasa1"), "|") Then
                                posExt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("dircasa1"), "|")
                                posInt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("dircasa1"), " ")
                                If posExt > 0 Then
                                    txt_Calle.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(0, posExt - 1)
                                Else
                                    txt_Calle.Text = dtDatos.Rows(0).Item("dircasa1")
                                End If
                                If posInt > 0 Then
                                    txt_NumExt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posExt, posInt - posExt - 1).Replace("E:", "").Trim()
                                    txt_NumInt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posInt).Replace("I:", "")
                                Else
                                    If posExt > 0 Then
                                        txt_NumExt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posExt).Replace("E:", "").Trim()
                                    End If
                                    txt_NumInt.Text = ""
                                End If

                            Else
                                posExt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("dircasa1"), "E:")
                                posInt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("dircasa1"), "I:")
                                If posExt > 0 Then
                                    txt_Calle.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(0, posExt - 1)
                                Else
                                    txt_Calle.Text = dtDatos.Rows(0).Item("dircasa1")
                                End If
                                If posInt > 0 Then
                                    txt_NumExt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posExt - 1, posInt - posExt - 1).Replace("E:", "").Trim()
                                    txt_NumInt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posInt - 1).Replace("I:", "")
                                Else
                                    If posExt > 0 Then
                                        txt_NumExt.Text = dtDatos.Rows(0).Item("dircasa1").ToString.Substring(posExt - 1).Replace("E:", "").Trim()
                                    End If
                                    txt_NumInt.Text = ""
                                End If
                            End If
                            txt_CP.Text = dtDatos.Rows(0).Item("cpcasa")
                            txt_Colonia.Text = dtDatos.Rows(0).Item("colcasa")
                            Cbo_Municipio.Text = dtDatos.Rows(0).Item("cdcasa")
                            Cbo_Estado.Text = dtDatos.Rows(0).Item("estado")
                            Try
                                cbo_Bancos.Text = dtDatos.Rows(0).Item("id_banco")
                                Cbo_Banco_Tipo.Text = dtDatos.Rows(0).Item("tipo_banco")
                                Txt_Banco_Cuenta.Text = dtDatos.Rows(0).Item("cuenta_banco")
                            Catch ex As Exception

                            End Try


                        End If

                    Else
                        txt_IdCliente.Text = ""
                        txt_ClienteNombre.Text = ""
                        txt_ClienteDescto.Text = "0.0"
                        txt_ClienteCondicion.Text = ""
                        txt_TipoCliente.Text = ""
                        txt_ClienteEstatus.Text = ""
                        If TipoTran = eTipoTran.PRESTAMOS Then
                            txt_Calle.Text = ""
                            txt_NumExt.Text = ""
                            txt_NumInt.Text = ""
                            txt_CP.Text = ""
                            txt_Colonia.Text = ""
                            Cbo_Municipio.Text = ""
                            Cbo_Estado.Text = ""
                            cbo_Bancos.Text = ""
                            Cbo_Banco_Tipo.Text = ""
                            Txt_Banco_Cuenta.Text = ""
                        End If
                    End If
                Else
                    txt_IdCliente.Text = ""
                    txt_ClienteNombre.Text = ""
                    txt_ClienteCondicion.Text = ""
                    txt_ClienteDescto.Text = "0.0"
                    txt_TipoCliente.Text = ""
                    txt_ClienteEstatus.Text = ""
                    If TipoTran = eTipoTran.PRESTAMOS Then
                        txt_Calle.Text = ""
                        txt_NumExt.Text = ""
                        txt_NumInt.Text = ""
                        txt_CP.Text = ""
                        txt_Colonia.Text = ""
                        Cbo_Municipio.Text = ""
                        Cbo_Estado.Text = ""
                        cbo_Bancos.Text = ""
                        Cbo_Banco_Tipo.Text = ""
                        Txt_Banco_Cuenta.Text = ""
                    End If
                End If
            Else
                If oDatos.PVTA_Empleados("", "", txt_IdCliente.Text.Substring(1), "", "", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        If dtDatos.Rows(0).Item("estatus") = "ALTA" Then

                            txt_IdCliente.Text = dtDatos.Rows(0).Item("id_empleado")
                            txt_ClienteNombre.Text = dtDatos.Rows(0).Item("nombre")
                            txt_ClienteCondicion.Text = ""
                            txt_ClienteDescto.Text = dtDatos.Rows(0).Item("descuento")
                            txt_ClienteEstatus.Text = dtDatos.Rows(0).Item("estatus")
                            txt_TipoCliente.Text = "EMPLEADO"
                            txt_ClienteNombre.Focus()

                        Else
                            MessageBox.Show("El empleado (" & txt_IdCliente.Text & ") no tiene estatus ALTA.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            txt_IdCliente.Text = ""
                            txt_ClienteNombre.Text = ""
                            txt_ClienteCondicion.Text = ""
                            txt_ClienteDescto.Text = "0.0"
                            txt_ClienteEstatus.Text = ""
                        End If
                    Else
                        txt_IdCliente.Text = ""
                        txt_ClienteNombre.Text = ""
                        txt_ClienteCondicion.Text = ""
                        txt_ClienteDescto.Text = "0.0"
                        txt_ClienteEstatus.Text = ""
                    End If
                Else
                    txt_IdCliente.Text = ""
                    txt_ClienteNombre.Text = ""
                    txt_ClienteCondicion.Text = ""
                    txt_ClienteDescto.Text = "0.0"
                    txt_ClienteEstatus.Text = ""
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub BuscarFinalCuenta(ByRef Tarjeta As String)
        Dim Msj As String = ""

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim posExt As Integer
        Dim posInt As Integer
        Dim ValidaIdentidad As Integer

        Try
            oDatos = New Datos_Viscoi

            txt_ClienteDescto.Text = "0.0"
            If oDatos.PVTA_Recupera_ClienteFinal(txt_IdClienteFinal.Text, "", "", "", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_IdClienteFinal.Text = dtDatos.Rows(0).Item("id_cliente").ToString.Trim
                    txt_NombreClienteFinal.Text = dtDatos.Rows(0).Item("nombre_cli").ToString.Trim
                    If Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "|") Then
                        posExt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "|")
                        posInt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "/")
                        If posExt > 0 Then
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(0, posExt - 1)
                        Else
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion")
                        End If
                        If posInt > 0 Then
                            txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt, posInt - posExt - 1).Replace("E:", "").Trim()
                            txt_NumInt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posInt).Replace("I:", "")
                        Else
                            If posExt > 0 Then
                                txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt).Replace("E:", "").Trim()
                            End If
                            txt_NumInt.Text = ""
                        End If

                    Else
                        posExt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "E:")
                        posInt = Microsoft.VisualBasic.InStr(dtDatos.Rows(0).Item("direccion"), "I:")
                        If posExt > 0 Then
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(0, posExt - 1)
                        Else
                            txt_Calle.Text = dtDatos.Rows(0).Item("direccion")
                        End If
                        If posInt > 0 Then
                            txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt - 1, posInt - posExt - 1).Replace("E:", "").Trim()
                            txt_NumInt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posInt - 1).Replace("I:", "")
                        Else
                            If posExt > 0 Then
                                txt_NumExt.Text = dtDatos.Rows(0).Item("direccion").ToString.Substring(posExt - 1).Replace("E:", "").Trim()
                            End If
                            txt_NumInt.Text = ""
                        End If

                    End If
                    txt_CP.Text = dtDatos.Rows(0).Item("cpostal")
                    txt_Colonia.Text = dtDatos.Rows(0).Item("colonia")
                    Cbo_Municipio.Text = dtDatos.Rows(0).Item("municipio")
                    Cbo_Estado.Text = dtDatos.Rows(0).Item("edo")
                    cbo_Bancos.Text = dtDatos.Rows(0).Item("id_banco")
                    Cbo_Banco_Tipo.Text = dtDatos.Rows(0).Item("tipo_banco")
                    Txt_Banco_Cuenta.Text = dtDatos.Rows(0).Item("cuenta_banco")
                    txt_NombreClienteFinal.Focus()



                    If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, "", "", "", "VERIFICA_IDENTIDAD_APP", dtDatos, Mensaje) Then
                        If dtDatos.Rows.Count > 0 Then
                            ValidaIdentidad = Globales.oAmbientes.Valor(dtDatos.Rows(0).Item("valor"))
                        End If
                    End If


                    If (ValidaIdentidad = 1) Then

                        Dim oCam As New frmCamaraValida
                        oCam.StartPosition = FormStartPosition.CenterScreen
                        oCam.ShowDialog()

                        Dim Fotos As DataTable = oCam.dtDatosFotos2


                        Dim oVideo As New frmCapturaVideo
                        oVideo.Cliente = txt_IdClienteFinal.Text
                        oVideo.StartPosition = FormStartPosition.CenterScreen
                        oVideo.ShowDialog()

                        If oCam.PathImagen <> "" Then
                            oDatos = New Datos_Viscoi
                            Dim img_byte As Byte()
                            Dim tabla As String
                            For Each dtRenglon As DataRow In Fotos.Rows
                                img_byte = dtRenglon.Item("picture")
                                tabla = dtRenglon.Item("nombre_documento")

                                If oDatos.Inserta_ImagenesByteApp(Globales.oAmbientes.Id_Empresa, tabla, txt_IdClienteFinal.Text, 1, Today, Globales.oAmbientes.oUsuario.Id_usuario, img_byte, Msj) Then
                                    ''Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", txt_IdClienteFinal.Text, 0, PictureBox1)
                                Else
                                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                End If

                            Next

                            Shell("C:\VOficina\Exe\Huella\FingerRegister.exe CLIENTEFINAL " & txt_IdClienteFinal.Text)

                            Dim dtDatosVerifica As DataTable = Nothing
                            If oDatos.VerificaIdentidad_Sel(Globales.oAmbientes.Id_Empresa, txt_IdClienteFinal.Text, dtDatosVerifica, Msj) Then
                                If dtDatosVerifica.Rows.Count > 0 Then
                                    Dim dtRenglon1 As DataRow = dtDatosVerifica(0)

                                    Dim BodyJson As String = "{" _
                                                           & """ine_front""	:""" & dtRenglon1.Item("ine_front") & """" _
                                                           & ",""ine_back""	:""" & dtRenglon1.Item("ine_back") & """" _
                                                           & ",""selfie""	:""" & dtRenglon1.Item("selfie") & """" _
                                                           & ",""model""	:""E""" _
                                                           & "}"
                                    Dim ApiVerificaMex As New ApiClient()
                                    ApiVerificaMex.Url = dtRenglon1.Item("url")
                                    ApiVerificaMex.AutMetodo = dtRenglon1.Item("method")
                                    ApiVerificaMex.AutToken = dtRenglon1.Item("token")
                                    ApiVerificaMex.JsonContent = BodyJson
                                    Dim sw_verificamex As Boolean

                                    If (ApiVerificaMex.CallApi()) Then
                                        If oDatos.VerificaIdentidad_Upd(Globales.oAmbientes.Id_Empresa, txt_IdClienteFinal.Text, ApiVerificaMex.RepuestaData, sw_verificamex, Msj) Then
                                            ''Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", txt_IdClienteFinal.Text, 0, PictureBox1)
                                            If Not sw_verificamex Then
                                                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                            End If
                                        Else
                                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        End If
                                    Else
                                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ApiVerificaMex.RepuestaCode & " : " & ApiVerificaMex.RepuestaData, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End If
                                Else
                                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & "No se encontraron datos.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                End If
                            Else
                                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        End If
                    Else

                        Dim oCam As New frmcamera
                        oCam.StartPosition = FormStartPosition.CenterScreen
                        oCam.ShowDialog()

                        If oCam.PathImagen <> "" Then
                            oDatos = New Datos_Viscoi
                            If oDatos.Inserta_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", txt_IdClienteFinal.Text, 1, Today, Globales.oAmbientes.oUsuario.Id_usuario, oCam.PictureBox2.Image, Msj) Then
                                ''Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", txt_IdClienteFinal.Text, 0, PictureBox1)
                            Else
                                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If

                            Shell("C:\VOficina\Exe\Huella\FingerRegister.exe CLIENTEFINAL " & txt_IdClienteFinal.Text)
                        Else
                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & "No se capturo Fotografia.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If


                    End If




                Else
                    txt_NombreClienteFinal.Text = ""
                    txt_Calle.Text = ""
                    txt_NumExt.Text = ""
                    txt_NumInt.Text = ""
                    txt_CP.Text = ""
                    txt_Colonia.Text = ""
                    Cbo_Municipio.Text = ""
                    Cbo_Estado.Text = ""
                    cbo_Bancos.Text = ""
                    Cbo_Banco_Tipo.Text = ""
                    Txt_Banco_Cuenta.Text = ""
                End If
            Else
                txt_NombreClienteFinal.Text = ""
                txt_Calle.Text = ""
                txt_NumExt.Text = ""
                txt_NumInt.Text = ""
                txt_CP.Text = ""
                txt_Colonia.Text = ""
                Cbo_Municipio.Text = ""
                Cbo_Estado.Text = ""
                cbo_Bancos.Text = ""
                Cbo_Banco_Tipo.Text = ""
                Txt_Banco_Cuenta.Text = ""
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub txt_IdCliente_LostFocus(sender As Object, e As EventArgs) Handles txt_IdCliente.LostFocus
        Try
            Call Crear_Transaccion()
            If txt_IdCliente.Text <> "" Then
                Call BuscarCuenta(txt_IdCliente.Text)
            Else
                txt_ClienteNombre.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub txt_IdClienteFinal_LostFocus(sender As Object, e As EventArgs) Handles txt_IdClienteFinal.LostFocus
        If txt_IdClienteFinal.Text <> "" Then
            Call BuscarFinalCuenta(txt_IdClienteFinal.Text)
        Else
            txt_NombreClienteFinal.Text = ""
            txt_Calle.Text = ""
            txt_NumExt.Text = ""
            txt_NumInt.Text = ""
            txt_CP.Text = ""
            txt_Colonia.Text = ""
            Cbo_Municipio.Text = ""
            Cbo_Estado.Text = ""
            cbo_Bancos.Text = ""
            Cbo_Banco_Tipo.Text = ""
            Txt_Banco_Cuenta.Text = ""
        End If
    End Sub
    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0

        ''Dim Total As Double
        ''Dim PagoContado As Double
        ''Dim PagoDE As Double
        ''Dim PagoTotal As Double
        Try
            Select Case tag.ToUpper
                Case "AYUDA"
                    CG_Ayuda.Visible = If(CG_Ayuda.Visible, False, True)
                Case "ARTICULOS"
                    Dim oFrm As SelArticulos
                    oFrm = New SelArticulos
                    oFrm.StartPosition = FormStartPosition.CenterParent
                    oFrm.ShowDialog()
                Case "TERMINAR"
                    Select Case TipoTran
                        Case eTipoTran.VALES
                            Call Terminar()
                        Case eTipoTran.PRESTAMOS
                            txt_IdClienteFinal.Text = txt_IdCliente.Text
                            Call Terminar()
                    End Select
                Case "NUEVO"

                    txt_Autonumsuc.Text = ""
                    txt_IdCliente.Text = ""
                    Call Crear_Transaccion()

                    Call Refrescar()
                    txt_IdCliente.Focus()
                Case "CODIGO"

                    Call Refrescar()
                    'Case "ELIMINAR"
                    '    Call ELiminar_Detventas()
                    'Case "CONTADO"
                    '    Call Pagar(0)
                    'Case "D.E."
                    '    Call Pagar(1)
                    'Case "DESCTO GLOBAL"
                    '    Call AplicaDescuentos(False)
                    'Case "DESCTO PARTIDA"
                    '    Call AplicaDescuentos(True)
                    'Case "PRECIODIRECTO PARTIDA"
                    '    Call AplicaPrecioDirecto(True)
                    'Case "FACTURAR"
                    '    Call DatosFacturar()
                    'Case "DATOS GENERALES"
                    '    Call DatosGenerales()
                Case "CLINUEVO"
                    If Not Globales.oAmbientes.oUsuario.ValidaPermiso("pdvClienteFinal", "INSERTARDATOS") Then
                        MessageBox.Show("El usuario no cuenta con acceso a la opción " & "pdvClienteFinal", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If

                    Dim frmClientesFinal As New pdvClienteFinal
                    frmClientesFinal.ShowDialog()
                    If frmClientesFinal.txt_Cliente.Text <> "" Then
                        txt_IdClienteFinal.Text = frmClientesFinal.txt_Cliente.Text
                        Call BuscarFinalCuenta(txt_IdClienteFinal.Text)
                    Else
                        txt_IdClienteFinal.Text = ""
                        txt_NombreClienteFinal.Text = ""
                        txt_Calle.Text = ""
                        txt_NumExt.Text = ""
                        txt_NumInt.Text = ""
                        txt_CP.Text = ""
                        txt_Colonia.Text = ""
                        Cbo_Municipio.Text = ""
                        Cbo_Estado.Text = ""
                        cbo_Bancos.Text = ""
                        Cbo_Banco_Tipo.Text = ""
                        Txt_Banco_Cuenta.Text = ""
                    End If
                Case "CLIMODIFICAR"
                    If Not Globales.oAmbientes.oUsuario.ValidaPermiso("pdvClienteFinal", "ACTUALIZARDATOS") Then
                        MessageBox.Show("El usuario no cuenta con acceso a la opción " & "pdvClienteFinal", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If
                    Dim frmClientesFinal As New pdvClienteFinal
                    frmClientesFinal.txt_Cliente.Text = txt_IdClienteFinal.Text
                    frmClientesFinal.ShowDialog()
                    If frmClientesFinal.txt_Cliente.Text <> "" Then
                        txt_IdClienteFinal.Text = frmClientesFinal.txt_Cliente.Text
                        Call BuscarFinalCuenta(txt_IdClienteFinal.Text)
                    Else
                        txt_IdClienteFinal.Text = ""
                        txt_NombreClienteFinal.Text = ""
                        txt_Calle.Text = ""
                        txt_NumExt.Text = ""
                        txt_NumInt.Text = ""
                        txt_CP.Text = ""
                        txt_Colonia.Text = ""
                        Cbo_Municipio.Text = ""
                        Cbo_Estado.Text = ""
                        cbo_Bancos.Text = ""
                        Cbo_Banco_Tipo.Text = ""
                        Txt_Banco_Cuenta.Text = ""
                    End If
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub Terminar()
        Dim Reporte As Object
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oTicket As entContraVale

        Dim sw_continuar As Boolean = False
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim Autonumsuc As String = ""
        Dim despedida As String = ""

        Dim Puntos_acumular As Double
        Dim DinEle_acumular As Double
        Dim cliente_disponeg2 As Double
        Dim IdLealtad As Integer = 0
        Dim Token As String = ""
        Dim tcambio As Double = 0
        Dim Nau_Importe As Double = 0
        Dim FolioDigital As String = ""

        Dim oLealtad As dllVOficinaLealtad.Lealtad

        Try
            oDatos = New Datos_Viscoi

            sw_continuar = True
            If sw_continuar Then

                If cbo_Desembolso.Text = "03 - SPEI" Then
                    Token = InputBox("Ingrese codigo de usuario, obtenido desde su celular", "Codigo de Usuario")
                End If

                If oDatos.Credito_Vales_Ins(Globales.oAmbientes.Id_Empresa _
                                               , txt_Sucursal.Text _
                                               , txt_Autonumsuc.Text _
                                               , txt_Caja.Text _
                                               , Globales.oAmbientes.oUsuario.Id_usuario _
                                               , txt_IdCliente.Text _
                                               , txt_IdClienteFinal.Text _
                                               , Cbo_Serie.Text _
                                               , Globales.oAmbientes.Valor(txt_Folio.Text) _
                                               , Globales.oAmbientes.Valor(Txt_Importe.Text) _
                                               , Globales.oAmbientes.Valor(Cbo_Plazos.Text) _
                                               , cbo_Desembolso.Text _
                                               , "Pesos" _
                                               , TipoTran.ToString _
                                               , cboTipoCredito.SelectedValue _
                                               , Token _
                                               , Autonumsuc _
                                               , despedida _
                                               , cliente_disponeg2 _
                                               , Nau_Importe _
                                               , Mensaje) Then
                    txt_Autonumsuc.Text = Autonumsuc

                    Call ImprimirTicket(txt_FormatoTicket.Text, despedida)

                    If cliente_disponeg2 > 0 Then
                        If oDatos.CAR_Genera_Folio_Digital_NAU(Autonumsuc _
                                                               , Globales.oAmbientes.Id_Empresa _
                                                                , txt_IdCliente.Text _
                                                                , txt_IdClienteFinal.Text _
                                                                , Nau_Importe _
                                                                , "99" _
                                                                , FolioDigital _
                                                                , Mensaje) Then

                            oTicket = New entContraVale
                            oTicket.Id_empresa = Globales.oAmbientes.Id_Empresa
                            oTicket.Id_sucursal = txt_Sucursal.Text
                            oTicket.Fecha = Format(Now, "dd/MMM/yyyy")
                            oTicket.Hora = Format(Now, "hh:mm")
                            oTicket.Cajero = txt_Cajero.Text
                            oTicket.Id_cliente = txt_IdCliente.Text
                            oTicket.Id_cliente_nom = txt_ClienteNombre.Text
                            oTicket.Id_cliente_final = txt_IdClienteFinal.Text
                            oTicket.Id_cliente_final_nom = txt_NombreClienteFinal.Text
                            oTicket.FolioDigital = FolioDigital
                            oTicket.Importe = Nau_Importe

                            Reporte = New xtraRepContraVale
                            TryCast(Reporte, xtraRepContraVale).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepContraVale).CreateDocument()

                            Dim printBase = New DevExpress.XtraPrinting.PrintToolBase(Reporte.PrintingSystem)
                            printBase.Print(lstImpresoras.Text)

                        End If
                    End If

                    txt_Autonumsuc.Text = ""
                    txt_IdCliente.Text = ""

                    ''If Not oDatos.PVTA_Caja_Valida_Efectivo(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_Caja.Text, dtDatos, Mensaje) Then
                    ''    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ''End If

                    Call Refrescar()
                    Me.Refresh()
                    Crear_Transaccion()
                    txt_IdCliente.Focus()
                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub TraePlazos()
        Dim iImporte As Integer
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            lbl_Mensaje.Text = ""
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", "", "CANJE_CAPITAL_MULTIPLO", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    Multiplo = Globales.oAmbientes.Valor(dtDatos.Rows(0).Item("valor"))
                End If
            End If

            Cbo_Plazos.DataSource = Nothing
            Cbo_Plazos.DisplayMember = "valor"
            Cbo_Plazos.SelectedValue = "valor"
            iImporte = Globales.oAmbientes.Valor(Txt_Importe.Text)
            If iImporte > 0 Then
                If iImporte Mod Multiplo = 0 Then
                    If oDatos.Credito_tasas_Plazo_Sel(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, TipoTran.ToString, txt_TipoCliente.Text, iImporte, dtDatos, Mensaje) Then
                        lbl_Mensaje.Text = ""
                        Cbo_Plazos.DataSource = dtDatos

                        txt_Subtotal.Text = Format(Globales.oAmbientes.Valor(Txt_Importe.Text), "C2")
                        Txt_Total.Text = Format(0, "C2")
                        txt_Subtotal.Text = Format(Globales.oAmbientes.Valor(Txt_Importe.Text) + Globales.oAmbientes.Valor(Txt_Total.Text), "C2")
                    Else
                        Txt_Importe.SelectionStart = Txt_Importe.Text.Length
                        lbl_Mensaje.Text = "No se Encontraron plazos para la sucursal e importe seleccionados."
                    End If
                Else
                    Txt_Importe.SelectionStart = Txt_Importe.Text.Length
                    lbl_Mensaje.Text = "Debes ingresar una cantidad multiplo de $" & Multiplo.ToString & " ."
                End If
            Else
                Txt_Importe.SelectionStart = Txt_Importe.Text.Length
                lbl_Mensaje.Text = "Debes ingresar una cantidad mayor a cero."
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub cbo_Desembolso_LostFocus(sender As Object, e As EventArgs) Handles cbo_Desembolso.LostFocus, Cbo_Plazos.LostFocus
        txt_ImporteTotal.Text = Format(Globales.oAmbientes.Valor(txt_Subtotal.Text) + Globales.oAmbientes.Valor(Txt_Total.Text), "C2")
    End Sub
    Private Sub ImprimirTicket(ByVal Formato As String, ByVal despedida As String)
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Dim Pic1 As PictureBox
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_Canje_Impresion(Today.Date _
                                              , txt_Autonumsuc.Text _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , Mensaje) Then

                Pic1 = New PictureBox
                If oDatos.PVTA_Recupera_ClienteFinal(txt_IdClienteFinal.Text, "", "", "", dtDatos2, Mensaje) Then
                    Call oDatos.Recupera_ImagenesApp(Globales.oAmbientes.Id_Empresa, "clientes-final-foto", txt_IdClienteFinal.Text, 0, Pic1)
                End If

                DEFinal = DEActual + DEAcumulados - DEUtilizados

                oTicket = New entTicketVenta
                oTicket.Fill(dtDatos, dtDatos1, dtDatos2)

                oTicket.Empresa = oTicket.Id_empresa
                oTicket.Rfc = txt_RFC.Text
                oTicket.Direccion = txt_Direccion.Text
                oTicket.Direccion2 = txt_Direccion2.Text
                oTicket.Cajero = txt_Cajero.Text
                oTicket.Caja = txt_Caja.Text
                oTicket.Id_cliente1 = txt_IdCliente.Text
                oTicket.Nom_cliente1 = txt_ClienteNombre.Text

                If txt_NombreCanje.Text.Length > 0 Then
                    oTicket.Lealtad_nombre = txt_NombreCanje.Text
                    oTicket.Puntos_saldo_ant = PuntosActual
                    oTicket.Puntos_acumulados = PuntosAcumulados
                    oTicket.Puntos_utilizados = PuntosUtilizados
                    oTicket.Puntos_saldo_act = PuntosFinal

                    oTicket.De_saldo_ant = DEActual
                    oTicket.De_acumulados = DEAcumulados
                    oTicket.De_utilizados = DEUtilizados
                    oTicket.De_saldo_act = DEFinal
                Else
                    oTicket.Lealtad_nombre = 0
                    oTicket.Puntos_saldo_ant = 0
                    oTicket.Puntos_acumulados = 0
                    oTicket.Puntos_utilizados = 0
                    oTicket.Puntos_saldo_act = 0

                    oTicket.De_saldo_ant = 0
                    oTicket.De_acumulados = 0
                    oTicket.De_utilizados = 0
                    oTicket.De_saldo_act = 0
                End If

                Dim MargenAba As Integer = 0
                Dim MargenIzq As Integer = 0
                Dim MargenDer As Integer = 0
                Dim MargenArr As Integer = 0
                Try
                    MargenAba = txt_MargenesTicket.Text.Split(",")(0)
                Catch ex As Exception
                    MargenAba = 0
                End Try
                Try
                    MargenIzq = txt_MargenesTicket.Text.Split(",")(1)
                Catch ex As Exception
                    MargenIzq = 0
                End Try
                Try
                    MargenDer = txt_MargenesTicket.Text.Split(",")(2)
                Catch ex As Exception
                    MargenDer = 0
                End Try
                Try
                    MargenArr = txt_MargenesTicket.Text.Split(",")(3)
                Catch ex As Exception
                    MargenArr = 0
                End Try
                Select Case Formato
                    Case "CARTA"
                        Reporte = New xtraRepTicketValeCarta
                        'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                        TryCast(Reporte, xtraRepTicketValeCarta).MargenAbj = MargenAba
                        TryCast(Reporte, xtraRepTicketValeCarta).MargenIzq = MargenIzq
                        TryCast(Reporte, xtraRepTicketValeCarta).MargenDer = MargenDer
                        TryCast(Reporte, xtraRepTicketValeCarta).MargenArr = MargenArr
                        TryCast(Reporte, xtraRepTicketValeCarta).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                        TryCast(Reporte, xtraRepTicketValeCarta).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepTicketValeCarta).CreateDocument()
                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeCarta).PrintingSystem)

                    Case "SEAFON"
                        Reporte = New xtraRepTicketVale
                        'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                        TryCast(Reporte, xtraRepTicketVale).MargenAbj = MargenAba
                        TryCast(Reporte, xtraRepTicketVale).MargenIzq = MargenIzq
                        TryCast(Reporte, xtraRepTicketVale).MargenDer = MargenDer
                        TryCast(Reporte, xtraRepTicketVale).MargenArr = MargenArr
                        TryCast(Reporte, xtraRepTicketVale).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                        TryCast(Reporte, xtraRepTicketVale).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepTicketVale).CreateDocument()
                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVale).PrintingSystem)
                    Case "EPSON"
                        Reporte = New xtraRepTicketVale
                        'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                        TryCast(Reporte, xtraRepTicketVale).MargenAbj = MargenAba
                        TryCast(Reporte, xtraRepTicketVale).MargenIzq = MargenIzq
                        TryCast(Reporte, xtraRepTicketVale).MargenDer = MargenDer
                        TryCast(Reporte, xtraRepTicketVale).MargenArr = MargenArr
                        TryCast(Reporte, xtraRepTicketVale).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                        TryCast(Reporte, xtraRepTicketVale).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepTicketVale).CreateDocument()
                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVale).PrintingSystem)
                    Case Else
                        Reporte = New xtraRepTicketVale
                        TryCast(Reporte, xtraRepTicketVale).MargenAbj = MargenAba
                        TryCast(Reporte, xtraRepTicketVale).MargenIzq = MargenIzq
                        TryCast(Reporte, xtraRepTicketVale).MargenDer = MargenDer
                        TryCast(Reporte, xtraRepTicketVale).MargenArr = MargenArr
                        TryCast(Reporte, xtraRepTicketVale).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                        TryCast(Reporte, xtraRepTicketVale).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepTicketVale).CreateDocument()
                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVale).PrintingSystem)
                End Select
                printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                printTool.ShowPreviewDialog()

                If TipoTran = eTipoTran.PRESTAMOS And cboTipoCredito.Text = "DISTRIBUIDOR PROMOCION" Then
                    Reporte = New xtraRepTicketBlancoOficio
                    'TryCast(Reporte, xtraRepTicketVale).xlbl_Despedida.Text = despedida
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenAbj = MargenAba
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenIzq = MargenIzq
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenDer = MargenDer
                    TryCast(Reporte, xtraRepTicketBlancoOficio).MargenArr = MargenArr
                    TryCast(Reporte, xtraRepTicketBlancoOficio).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                    TryCast(Reporte, xtraRepTicketBlancoOficio).ods.DataSource = oTicket
                    TryCast(Reporte, xtraRepTicketBlancoOficio).CreateDocument()
                    printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketBlancoOficio).PrintingSystem)
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()
                End If
                ''lstImpresoras.SelectedIndex = 5
                ''printBase.Print(lstImpresoras.Text)

                If txt_IdCliente.Text <> txt_IdClienteFinal.Text Then
                    Select Case Formato
                        Case "CARTA"
                            Reporte = New XtraRep_CartaPrestaNombre
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Foto = Pic1.Image
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.Empresa
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.Direccion
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)

                        Case "SEAFON"
                            Reporte = New XtraRep_CartaPrestaNombre
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Foto = Pic1.Image
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.FacEmpresa
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.FacDireccion
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                        Case "EPSON"
                            Reporte = New XtraRep_CartaPrestaNombre
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Foto = Pic1.Image
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.FacEmpresa
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.FacDireccion
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                        Case Else
                            Reporte = New XtraRep_CartaPrestaNombre
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Empresa = oTicket.FacEmpresa
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).Direccion = oTicket.FacDireccion
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).ods.DataSource = oTicket
                            TryCast(Reporte, XtraRep_CartaPrestaNombre).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                    End Select
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()
                End If

                If cbo_Desembolso.Text = "02 - ODP" Then
                    Select Case Formato
                        Case "CARTA"
                            Reporte = New xtraRepTicketValeODP
                            TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeODP).PrintingSystem)

                        Case "SEAFON"
                            Reporte = New xtraRepTicketValeODP
                            TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeODP).PrintingSystem)
                        Case "EPSON"
                            Reporte = New xtraRepTicketValeODP
                            TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketValeODP).PrintingSystem)
                        Case Else
                            Reporte = New xtraRepTicketValeODP
                            TryCast(Reporte, xtraRepTicketValeODP).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepTicketValeODP).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, XtraRep_CartaPrestaNombre).PrintingSystem)
                    End Select
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()
                End If
                If oDatos.PVTA_Recupera_Desglose1(txt_Autonumsuc.Text _
                                            , Globales.oAmbientes.Id_Empresa _
                                            , Globales.oAmbientes.Id_Sucursal _
                                            , CDate("01/01/1900") _
                                            , "" _
                                            , "" _
                                            , "CREDITO" _
                                            , dtDatos _
                                            , Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                            , txt_Autonumsuc.Text _
                                            , Globales.oAmbientes.Id_Empresa _
                                            , Globales.oAmbientes.Id_Sucursal _
                                            , dtDatos _
                                            , Mensaje) Then
                            If dtDatos.Rows.Count > 0 Then
                                Dim oPagare As New entPagare
                                oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                Select Case Formato
                                    Case "SEAFON"
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                    Case "EPSON"
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                    Case Else
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                End Select
                                printBase.Print(lstImpresoras.Text)

                            End If
                        End If
                    End If
                End If
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub pdvCanjes_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim msj As String = ""
        Dim DatosConexion() As String
        Try
            oDatos = New Datos_Viscoi
            If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, txt_Cajero.Text, 0, msj) Then
                MessageBox.Show(msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Application.Exit()
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal

            If oDatos.PVTA_Recupera_Cajeros(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_Caja.Text, "", Globales.oAmbientes.oUsuario.Id_usuario, dtDatos, msj) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_Cajero.Text = dtDatos.Rows(0).Item("cajero")

                    If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", txt_Caja.Text, dtDatos, msj) Then
                        If dtDatos.Rows.Count > 0 Then
                            txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                            txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper
                            txt_PinPad.Text = dtDatos.Rows(0).Item("printerdriver1")

                            Dim _Puerto As String
                            Dim _User As String
                            Dim _Pswd As String
                            Dim _IdMerchant As String
                            Dim _Url As String
                            Dim _Modo As String
                            Try
                                If txt_PinPad.Text <> "" Then
                                    If txt_PinPad.Text.Split("|").Length = 7 Then
                                        Globales.oAmbientes.PinPadBanco = txt_PinPad.Text.Split("|")(0).ToString.ToUpper
                                        _Puerto = txt_PinPad.Text.Split("|")(1).ToString.ToUpper
                                        _User = txt_PinPad.Text.Split("|")(2)
                                        _Pswd = txt_PinPad.Text.Split("|")(3)
                                        _IdMerchant = txt_PinPad.Text.Split("|")(4)
                                        _Url = txt_PinPad.Text.Split("|")(5).ToString
                                        _Modo = txt_PinPad.Text.Split("|")(6).ToString.ToUpper

                                        Select Case Globales.oAmbientes.PinPadBanco
                                            Case "BANORTE"
                                                Globales.oAmbientes.oPinPadBanorte = New Globales.clsPinPadBanorte(_Puerto, _User, _Pswd, _IdMerchant, _Modo, _Url, txt_Caja.Text)
                                                Dim oSplash As New pdvPinPadSplash
                                                oSplash.PinAccion = pdvPinPadSplash.PinpadAccion.Inicializa
                                                oSplash.StartPosition = FormStartPosition.CenterScreen
                                                oSplash.ShowDialog()
                                        End Select
                                    Else
                                        MessageBox.Show("Faltan parametros de configuracion para la PINPAD.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End If
                                End If
                            Catch ex As Exception
                                Globales.oAmbientes.PinPadBanco = ""
                                _User = ""
                                _Pswd = ""
                                _IdMerchant = ""
                                _Modo = ""
                                MessageBox.Show("El pinpad no se inicializo corectamente." & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                            End Try


                            If dtDatos.Rows(0).Item("actividad").ToString.Trim.ToUpper = "ENUSO" Then
                                ''dtDatos.Rows(0).Item("corte").ToString()

                                Call ControlBotones("Ok")
                                Call Refrescar()

                                If txt_Autonumsuc.Text = "" Then
                                    Call Crear_Transaccion()
                                    txt_IdCliente.Focus()
                                End If
                            Else
                                MessageBox.Show("Caja(" & txt_Caja.Text & ") esta cerrada, realice apertura de caja primero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Me.Close()
                            End If
                        Else
                            MessageBox.Show("Caja(" & txt_Caja.Text & ") no esta registrada para Sucursal(" & txt_Sucursal.Text & ").", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Me.Close()
                        End If
                    Else
                        MessageBox.Show("Caja(" & txt_Caja.Text & ") no esta registrada.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Me.Close()
                    End If
                Else
                    MessageBox.Show("Usuario no es cajero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.Close()
                End If
            Else
                MessageBox.Show(msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub Txt_Importe_LostFocus(sender As Object, e As EventArgs) Handles Txt_Importe.LostFocus, txt_IdCliente.LostFocus, txt_Folio.LostFocus
        Call TraePlazos()
    End Sub

    Private Sub lbl_Mensaje_TextChanged(sender As Object, e As EventArgs)
        If lbl_Mensaje.Text.Length > 0 Then
            lbl_Mensaje.Visible = True
        Else
            lbl_Mensaje.Visible = False
        End If
    End Sub

    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Tag
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Ayuda()
        Try
            Call ControlBotones("Ayuda")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_NuevaVenta()
        Try
            Call ControlBotones("Nuevo")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_TerminarVenta()
        Try
            Call ControlBotones("Terminar")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_NvoCliente()
        Try
            Call ControlBotones("CliNuevo")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_ModCliente()
        Try
            Call ControlBotones("CliModificar")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Btn_FolioDigital_Click(sender As Object, e As EventArgs) Handles Btn_FolioDigital.Click
        Dim oDatos As Datos_Viscoi
        Dim Mensaje As String = ""
        Dim dtDatos As DataTable = Nothing
        Try
            oDatos = New Datos_Viscoi
            Txt_FolioDigital.Text = InputBox("Teclea el folio digital.", Me.Text)
            If Txt_FolioDigital.Text.Length > 0 Then
                If oDatos.PVTA_Recuepera_FolioDigital_Sel(Globales.oAmbientes.Id_Empresa, Txt_FolioDigital.Text, dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        If dtDatos.Rows(0).Item("estatus") = "VIGENTE" Then
                            txt_IdCliente.Text = dtDatos.Rows(0).Item("id_cliente")
                            Call txt_IdCliente_LostFocus(Nothing, Nothing)

                            txt_IdClienteFinal.Text = dtDatos.Rows(0).Item("canje_id_cliente")
                            Call txt_IdClienteFinal_LostFocus(Nothing, Nothing)

                            Cbo_Serie.Text = dtDatos.Rows(0).Item("serie")
                            txt_Folio.Text = dtDatos.Rows(0).Item("folio")
                            Txt_Importe.Text = dtDatos.Rows(0).Item("importe")
                            Call TraePlazos()
                            Cbo_Plazos.Text = dtDatos.Rows(0).Item("plazos")
                        Else
                            MessageBox.Show("Estatus de Folio Digital no es valido." & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    Else
                        MessageBox.Show("No se encontro Folio Digital.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
            oDatos = Nothing
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

End Class
