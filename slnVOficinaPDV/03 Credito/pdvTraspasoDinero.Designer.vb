﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvTraspasoDinero
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txt_Total = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Corte = New System.Windows.Forms.TextBox()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.Cbo_Bovedas = New System.Windows.Forms.ComboBox()
        Me.RG_Cajas = New DevExpress.XtraEditors.RadioGroup()
        Me.Lbl_Sentido = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.RG_Cajas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Lbl_Sentido)
        Me.Panel1.Controls.Add(Me.txt_Total)
        Me.Panel1.Controls.Add(Me.txt_Corte)
        Me.Panel1.Controls.Add(Me.txt_Cajero)
        Me.Panel1.Controls.Add(Me.txt_Caja)
        Me.Panel1.Controls.Add(Me.btn_Aceptar)
        Me.Panel1.Controls.Add(Me.btn_Cancelar)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 71)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(559, 143)
        Me.Panel1.TabIndex = 1
        '
        'txt_Total
        '
        Me.txt_Total.Location = New System.Drawing.Point(283, 39)
        Me.txt_Total.Name = "txt_Total"
        Me.txt_Total.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.Appearance.Options.UseFont = True
        Me.txt_Total.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Total.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Total.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Total.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Total.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Total.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.txt_Total.Properties.MaskSettings.Set("mask", "C2")
        Me.txt_Total.Size = New System.Drawing.Size(262, 46)
        Me.txt_Total.TabIndex = 19
        '
        'txt_Corte
        '
        Me.txt_Corte.Location = New System.Drawing.Point(319, 107)
        Me.txt_Corte.Name = "txt_Corte"
        Me.txt_Corte.Size = New System.Drawing.Size(30, 20)
        Me.txt_Corte.TabIndex = 18
        Me.txt_Corte.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Location = New System.Drawing.Point(283, 107)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(30, 20)
        Me.txt_Cajero.TabIndex = 13
        Me.txt_Cajero.Visible = False
        '
        'txt_Caja
        '
        Me.txt_Caja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txt_Caja.Location = New System.Drawing.Point(247, 107)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(30, 20)
        Me.txt_Caja.TabIndex = 12
        Me.txt_Caja.Visible = False
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(146, 95)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 10
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(355, 95)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 11
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(18, 42)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(260, 38)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Monto"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.Cbo_Bovedas)
        Me.GroupControl2.Controls.Add(Me.RG_Cajas)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(559, 71)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Tipo de Caja"
        '
        'Cbo_Bovedas
        '
        Me.Cbo_Bovedas.DisplayMember = "valor"
        Me.Cbo_Bovedas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cbo_Bovedas.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cbo_Bovedas.FormattingEnabled = True
        Me.Cbo_Bovedas.Location = New System.Drawing.Point(214, 31)
        Me.Cbo_Bovedas.Name = "Cbo_Bovedas"
        Me.Cbo_Bovedas.Size = New System.Drawing.Size(331, 32)
        Me.Cbo_Bovedas.TabIndex = 22
        Me.Cbo_Bovedas.ValueMember = "id"
        '
        'RG_Cajas
        '
        Me.RG_Cajas.EditValue = "CORTE P"
        Me.RG_Cajas.Location = New System.Drawing.Point(2, 23)
        Me.RG_Cajas.Name = "RG_Cajas"
        Me.RG_Cajas.Properties.Columns = 3
        Me.RG_Cajas.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("CAJA ACTUAL", "Caja Actual", True, "CORTE", ""), New DevExpress.XtraEditors.Controls.RadioGroupItem("BOVEDA", "Boveda", True, "CORTE", "")})
        Me.RG_Cajas.Size = New System.Drawing.Size(311, 46)
        Me.RG_Cajas.TabIndex = 21
        '
        'Lbl_Sentido
        '
        Me.Lbl_Sentido.AutoSize = True
        Me.Lbl_Sentido.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_Sentido.Location = New System.Drawing.Point(55, 3)
        Me.Lbl_Sentido.Name = "Lbl_Sentido"
        Me.Lbl_Sentido.Size = New System.Drawing.Size(414, 31)
        Me.Lbl_Sentido.TabIndex = 20
        Me.Lbl_Sentido.Text = "========================>"
        '
        'pdvTraspasoDinero
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(559, 214)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvTraspasoDinero"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Traspaso de Dinero"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.RG_Cajas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents txt_Corte As TextBox
    Friend WithEvents txt_Total As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Cbo_Bovedas As ComboBox
    Friend WithEvents RG_Cajas As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Lbl_Sentido As Label
End Class
