﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvAplicaPromociones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Disponibles = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Requeridos = New DevExpress.XtraEditors.TextEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nud_Aplicados = New System.Windows.Forms.NumericUpDown()
        Me.nud_Comprados = New System.Windows.Forms.NumericUpDown()
        Me.txt_Nombre = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Articulo = New DevExpress.XtraEditors.TextEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.txt_Valor = New DevExpress.XtraEditors.TextEdit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txt_Disponibles.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Requeridos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nud_Aplicados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nud_Comprados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Nombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Articulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Valor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(337, 163)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(448, 163)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.Label4)
        Me.GroupControl2.Controls.Add(Me.Label3)
        Me.GroupControl2.Controls.Add(Me.txt_Disponibles)
        Me.GroupControl2.Controls.Add(Me.txt_Requeridos)
        Me.GroupControl2.Controls.Add(Me.Label2)
        Me.GroupControl2.Controls.Add(Me.Label1)
        Me.GroupControl2.Controls.Add(Me.nud_Aplicados)
        Me.GroupControl2.Controls.Add(Me.nud_Comprados)
        Me.GroupControl2.Controls.Add(Me.txt_Nombre)
        Me.GroupControl2.Controls.Add(Me.txt_Articulo)
        Me.GroupControl2.Controls.Add(Me.Label7)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(514, 164)
        Me.GroupControl2.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(394, 83)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 24)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Disponibles"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(229, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 24)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Requeridos"
        '
        'txt_Disponibles
        '
        Me.txt_Disponibles.Location = New System.Drawing.Point(388, 108)
        Me.txt_Disponibles.Name = "txt_Disponibles"
        Me.txt_Disponibles.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Disponibles.Properties.Appearance.Options.UseFont = True
        Me.txt_Disponibles.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Disponibles.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Disponibles.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Disponibles.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Disponibles.Properties.Mask.EditMask = "N0"
        Me.txt_Disponibles.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Disponibles.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Disponibles.Properties.ReadOnly = True
        Me.txt_Disponibles.Size = New System.Drawing.Size(121, 30)
        Me.txt_Disponibles.TabIndex = 16
        '
        'txt_Requeridos
        '
        Me.txt_Requeridos.Location = New System.Drawing.Point(229, 108)
        Me.txt_Requeridos.Name = "txt_Requeridos"
        Me.txt_Requeridos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Requeridos.Properties.Appearance.Options.UseFont = True
        Me.txt_Requeridos.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Requeridos.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Requeridos.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Requeridos.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Requeridos.Properties.Mask.EditMask = "N0"
        Me.txt_Requeridos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Requeridos.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Requeridos.Properties.ReadOnly = True
        Me.txt_Requeridos.Size = New System.Drawing.Size(111, 30)
        Me.txt_Requeridos.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(119, 82)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 24)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Aplicados"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 83)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 24)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Comprados"
        '
        'nud_Aplicados
        '
        Me.nud_Aplicados.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nud_Aplicados.Location = New System.Drawing.Point(119, 109)
        Me.nud_Aplicados.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nud_Aplicados.Name = "nud_Aplicados"
        Me.nud_Aplicados.Size = New System.Drawing.Size(104, 30)
        Me.nud_Aplicados.TabIndex = 12
        Me.nud_Aplicados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nud_Comprados
        '
        Me.nud_Comprados.Enabled = False
        Me.nud_Comprados.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nud_Comprados.Location = New System.Drawing.Point(0, 109)
        Me.nud_Comprados.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nud_Comprados.Name = "nud_Comprados"
        Me.nud_Comprados.ReadOnly = True
        Me.nud_Comprados.Size = New System.Drawing.Size(113, 30)
        Me.nud_Comprados.TabIndex = 11
        Me.nud_Comprados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Location = New System.Drawing.Point(157, 34)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Properties.Appearance.Options.UseFont = True
        Me.txt_Nombre.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Nombre.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Nombre.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Nombre.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Nombre.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Nombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Nombre.Properties.ReadOnly = True
        Me.txt_Nombre.Size = New System.Drawing.Size(352, 30)
        Me.txt_Nombre.TabIndex = 10
        '
        'txt_Articulo
        '
        Me.txt_Articulo.Location = New System.Drawing.Point(84, 34)
        Me.txt_Articulo.Name = "txt_Articulo"
        Me.txt_Articulo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Articulo.Properties.Appearance.Options.UseFont = True
        Me.txt_Articulo.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Articulo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Articulo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Articulo.Properties.ReadOnly = True
        Me.txt_Articulo.Size = New System.Drawing.Size(67, 30)
        Me.txt_Articulo.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(5, 37)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 24)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Articulo"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.PictureBox1)
        Me.GroupControl3.Controls.Add(Me.txt_Valor)
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 164)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(514, 349)
        Me.GroupControl3.TabIndex = 2
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Location = New System.Drawing.Point(2, 23)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(329, 324)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        '
        'txt_Valor
        '
        Me.txt_Valor.Location = New System.Drawing.Point(5, 30)
        Me.txt_Valor.Name = "txt_Valor"
        Me.txt_Valor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Valor.Properties.Appearance.Options.UseFont = True
        Me.txt_Valor.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Valor.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Valor.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Valor.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Valor.Properties.Mask.EditMask = "N0"
        Me.txt_Valor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Valor.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Valor.Size = New System.Drawing.Size(31, 30)
        Me.txt_Valor.TabIndex = 19
        Me.txt_Valor.Visible = False
        '
        'pdvAplicaPromociones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(514, 513)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvAplicaPromociones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aplica Promoción"
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txt_Disponibles.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Requeridos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nud_Aplicados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nud_Comprados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Nombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Articulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Valor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_Nombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Articulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents nud_Aplicados As NumericUpDown
    Friend WithEvents nud_Comprados As NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Disponibles As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Requeridos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Valor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PictureBox1 As PictureBox
End Class
