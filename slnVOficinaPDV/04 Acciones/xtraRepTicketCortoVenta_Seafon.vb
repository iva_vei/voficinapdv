﻿Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Public Class xtraRepTicketCortoVenta_Seafon
    Public SwReimpresion As Boolean = False
    Public TipoCliente As String
    Public MargenDer As Integer = 0
    Public MargenIzq As Integer = 0
    Public MargenArr As Integer = 0
    Public MargenAbj As Integer = 0
    Private Sub xtraRepTicketVenta_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Me.Margins.Right = Me.Margins.Right + MargenDer
        Me.Margins.Left = Me.Margins.Left + MargenIzq
        Me.Margins.Top = Me.Margins.Top + MargenArr
        Me.Margins.Bottom = Me.Margins.Bottom + MargenAbj
        If SwReimpresion Then
            Me.Watermark.Text = "REIMPRESION"
            Me.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal

        End If
    End Sub
    Private Sub lbl_DesctoArt_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub

    Private Sub lbl_Descto_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_Descto.BeforePrint
        lbl_Descto.Visible = False

    End Sub
End Class