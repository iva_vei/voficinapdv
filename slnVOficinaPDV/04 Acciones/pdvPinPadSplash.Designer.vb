﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pdvPinPadSplash
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MarqueeProgressBarControl1 = New DevExpress.XtraEditors.MarqueeProgressBarControl()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbl_Accion = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.txt_Mensaje = New System.Windows.Forms.TextBox()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.Btn_Cancelar = New System.Windows.Forms.Button()
        Me.txt_NoControl = New System.Windows.Forms.TextBox()
        CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MarqueeProgressBarControl1
        '
        Me.MarqueeProgressBarControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.MarqueeProgressBarControl1.EditValue = 0
        Me.MarqueeProgressBarControl1.Location = New System.Drawing.Point(0, 88)
        Me.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1"
        Me.MarqueeProgressBarControl1.Size = New System.Drawing.Size(384, 41)
        Me.MarqueeProgressBarControl1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(384, 44)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Estableciondo Conexión"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(0, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(384, 44)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "con PinPad ..."
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(0, 129)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(384, 44)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Acción:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Accion
        '
        Me.lbl_Accion.Dock = System.Windows.Forms.DockStyle.Top
        Me.lbl_Accion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Accion.Location = New System.Drawing.Point(0, 173)
        Me.lbl_Accion.Name = "lbl_Accion"
        Me.lbl_Accion.Size = New System.Drawing.Size(384, 44)
        Me.lbl_Accion.TabIndex = 4
        Me.lbl_Accion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'txt_Mensaje
        '
        Me.txt_Mensaje.Dock = System.Windows.Forms.DockStyle.Top
        Me.txt_Mensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Mensaje.Location = New System.Drawing.Point(0, 217)
        Me.txt_Mensaje.Multiline = True
        Me.txt_Mensaje.Name = "txt_Mensaje"
        Me.txt_Mensaje.Size = New System.Drawing.Size(384, 188)
        Me.txt_Mensaje.TabIndex = 6
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(12, 411)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 38)
        Me.btn_Aceptar.TabIndex = 8
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Location = New System.Drawing.Point(67, 432)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(30, 20)
        Me.txt_Direccion.TabIndex = 17
        Me.txt_Direccion.Visible = False
        '
        'txt_RFC
        '
        Me.txt_RFC.Location = New System.Drawing.Point(103, 411)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(30, 20)
        Me.txt_RFC.TabIndex = 16
        Me.txt_RFC.Visible = False
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Location = New System.Drawing.Point(67, 411)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(30, 20)
        Me.txt_Empresa.TabIndex = 15
        Me.txt_Empresa.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Location = New System.Drawing.Point(103, 432)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(30, 20)
        Me.txt_Direccion2.TabIndex = 21
        Me.txt_Direccion2.Visible = False
        '
        'Btn_Cancelar
        '
        Me.Btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.Btn_Cancelar.Location = New System.Drawing.Point(330, 411)
        Me.Btn_Cancelar.Name = "Btn_Cancelar"
        Me.Btn_Cancelar.Size = New System.Drawing.Size(42, 38)
        Me.Btn_Cancelar.TabIndex = 22
        Me.Btn_Cancelar.UseVisualStyleBackColor = True
        '
        'txt_NoControl
        '
        Me.txt_NoControl.Location = New System.Drawing.Point(139, 429)
        Me.txt_NoControl.Name = "txt_NoControl"
        Me.txt_NoControl.Size = New System.Drawing.Size(150, 20)
        Me.txt_NoControl.TabIndex = 23
        '
        'pdvPinPadSplash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 461)
        Me.ControlBox = False
        Me.Controls.Add(Me.txt_NoControl)
        Me.Controls.Add(Me.Btn_Cancelar)
        Me.Controls.Add(Me.txt_Direccion2)
        Me.Controls.Add(Me.txt_Direccion)
        Me.Controls.Add(Me.txt_RFC)
        Me.Controls.Add(Me.txt_Empresa)
        Me.Controls.Add(Me.btn_Aceptar)
        Me.Controls.Add(Me.txt_Mensaje)
        Me.Controls.Add(Me.lbl_Accion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.MarqueeProgressBarControl1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximumSize = New System.Drawing.Size(400, 500)
        Me.MinimumSize = New System.Drawing.Size(400, 500)
        Me.Name = "pdvPinPadSplash"
        Me.Text = "Interface PinPad"
        CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MarqueeProgressBarControl1 As DevExpress.XtraEditors.MarqueeProgressBarControl
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lbl_Accion As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents txt_Mensaje As TextBox
    Private WithEvents btn_Aceptar As Button
    Friend WithEvents txt_Direccion As TextBox
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents txt_Empresa As TextBox
    Friend WithEvents txt_Direccion2 As TextBox
    Private WithEvents Btn_Cancelar As Button
    Friend WithEvents txt_NoControl As TextBox
End Class
