﻿Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Public Class XtraRep_CartaPrestaNombre
    Public SwReimpresion As Boolean = False
    Public Foto As Image
    Public Empresa As String
    Public Direccion As String
    Public LugExpedicion As String
    Public Fecha As String
    Public Importe As String
    Public Letra As String
    Public Contenido As String = "Por medio del presente instrumento declaro que no estoy fungiendo como PRESTANOMBRE de la distribuidora arriba mencionada, ya que incurrir en esta practica que es considerada un DELITO y el cual se encuentra tipificada en el articulo 400 BIS DEL CODIGO PENAL FEDERAL." & vbNewLine & vbNewLine & "Autorizo a la empresa #EMPRESA# para que mi información sea boletinada así mismo asumir las consecuencias legales ya que sean de carácter MERCANTIL o PENAL que puedan resultar en contra de mi persona o de mi patrimonio."
    Private Sub XtraRep_Pedidos_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Dim cColor As Color = Globales.oAmbientes.PaletaColores(0)
        XrLabel1.ForeColor = cColor
        lbl_Empresa.ForeColor = cColor
        ''lbl_EmpresaDireccion.ForeColor = cColor
        lbl_ECliente.BackColor = cColor
        XrLabel1.BackColor = cColor
        XrLabel11.BackColor = cColor
        XrLabel12.BackColor = cColor
        XrLabel16.BackColor = cColor
        XrLabel15.BorderColor = cColor
        lbl_Distribuidor.BorderColor = cColor
        lbl_Telefono.BackColor = cColor
        lbl_Distribuidor.BackColor = cColor
        XrLabel18.BorderColor = cColor
        XrLabel18.BackColor = cColor
        Try
            XrPic_Logo.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\LogoEmpresa.jpg"))
            Pic_Foto.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Foto))
            Me.Watermark.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Factura_WaterMark1.png"))
            If SwReimpresion Then
                Me.Watermark.Text = "REIMPRESION"
                Me.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal
                ''.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Reimpresion.png"))
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub XrPictureBox6_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub XrTableRow3_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub lbl_Empresa_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)
        ''lbl_EmpresaDireccion.Text = Direccion.Replace("|", vbNewLine)
    End Sub
    Private Sub lbl_Empresa_BeforePrint_1(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_Empresa.BeforePrint
        lbl_Empresa.Text = Empresa
    End Sub

    Private Sub XrLabel25_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)
    End Sub

    Private Sub XrLabel13_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel13.BeforePrint
        XrLabel13.Text = Contenido.Replace("#EMPRESA#", Empresa)
    End Sub
End Class