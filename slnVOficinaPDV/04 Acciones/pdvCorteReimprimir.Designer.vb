﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCorteReimprimir
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.gcDatosTicket = New DevExpress.XtraEditors.GroupControl()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.lbl_Caja = New System.Windows.Forms.Label()
        Me.btn_Buscar = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Corte = New System.Windows.Forms.NumericUpDown()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.lbl_Corte = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cbo_Sucursal = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.txt_Empresa1 = New System.Windows.Forms.TextBox()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_FormatoTicket = New System.Windows.Forms.TextBox()
        Me.txt_MargenesTicket = New System.Windows.Forms.TextBox()
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosTicket.SuspendLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Corte, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcDatosTicket
        '
        Me.gcDatosTicket.Controls.Add(Me.RadioGroup1)
        Me.gcDatosTicket.Controls.Add(Me.Label2)
        Me.gcDatosTicket.Controls.Add(Me.DateTimePicker1)
        Me.gcDatosTicket.Controls.Add(Me.lbl_Caja)
        Me.gcDatosTicket.Controls.Add(Me.btn_Buscar)
        Me.gcDatosTicket.Controls.Add(Me.txt_Corte)
        Me.gcDatosTicket.Controls.Add(Me.txt_Caja)
        Me.gcDatosTicket.Controls.Add(Me.lbl_Corte)
        Me.gcDatosTicket.Controls.Add(Me.Label20)
        Me.gcDatosTicket.Controls.Add(Me.cbo_Sucursal)
        Me.gcDatosTicket.Controls.Add(Me.Label19)
        Me.gcDatosTicket.Controls.Add(Me.txt_Empresa)
        Me.gcDatosTicket.Location = New System.Drawing.Point(1, 12)
        Me.gcDatosTicket.Name = "gcDatosTicket"
        Me.gcDatosTicket.Size = New System.Drawing.Size(508, 194)
        Me.gcDatosTicket.TabIndex = 0
        Me.gcDatosTicket.Text = "Datos del Corte"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.EditValue = "CORTE"
        Me.RadioGroup1.Location = New System.Drawing.Point(7, 26)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("CORTE", "Corte"), New DevExpress.XtraEditors.Controls.RadioGroupItem("ENTREGA", "Entrega")})
        Me.RadioGroup1.Size = New System.Drawing.Size(409, 36)
        Me.RadioGroup1.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(89, 125)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 24)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Fecha"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(7, 151)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 30)
        Me.DateTimePicker1.TabIndex = 2
        '
        'lbl_Caja
        '
        Me.lbl_Caja.AutoSize = True
        Me.lbl_Caja.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Caja.Location = New System.Drawing.Point(236, 123)
        Me.lbl_Caja.Name = "lbl_Caja"
        Me.lbl_Caja.Size = New System.Drawing.Size(47, 24)
        Me.lbl_Caja.TabIndex = 24
        Me.lbl_Caja.Text = "Caja"
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Location = New System.Drawing.Point(446, 55)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(50, 50)
        Me.btn_Buscar.TabIndex = 5
        Me.btn_Buscar.Text = "Buscar"
        '
        'txt_Corte
        '
        Me.txt_Corte.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Corte.Location = New System.Drawing.Point(316, 151)
        Me.txt_Corte.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.txt_Corte.Name = "txt_Corte"
        Me.txt_Corte.Size = New System.Drawing.Size(100, 29)
        Me.txt_Corte.TabIndex = 4
        Me.txt_Corte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Caja
        '
        Me.txt_Caja.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Caja.Location = New System.Drawing.Point(212, 151)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(100, 29)
        Me.txt_Caja.TabIndex = 3
        Me.txt_Caja.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_Corte
        '
        Me.lbl_Corte.AutoSize = True
        Me.lbl_Corte.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Corte.Location = New System.Drawing.Point(338, 123)
        Me.lbl_Corte.Name = "lbl_Corte"
        Me.lbl_Corte.Size = New System.Drawing.Size(55, 24)
        Me.lbl_Corte.TabIndex = 23
        Me.lbl_Corte.Text = "Corte"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(268, 65)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(83, 24)
        Me.Label20.TabIndex = 22
        Me.Label20.Text = "Sucursal"
        '
        'cbo_Sucursal
        '
        Me.cbo_Sucursal.DisplayMember = "id_sucursal"
        Me.cbo_Sucursal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Sucursal.FormattingEnabled = True
        Me.cbo_Sucursal.Location = New System.Drawing.Point(211, 93)
        Me.cbo_Sucursal.Name = "cbo_Sucursal"
        Me.cbo_Sucursal.Size = New System.Drawing.Size(205, 27)
        Me.cbo_Sucursal.TabIndex = 1
        Me.cbo_Sucursal.ValueMember = "id_sucursal"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(66, 65)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(86, 24)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "Empresa"
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa.Location = New System.Drawing.Point(6, 93)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(200, 29)
        Me.txt_Empresa.TabIndex = 0
        Me.txt_Empresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_RFC
        '
        Me.txt_RFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(11, 212)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(14, 29)
        Me.txt_RFC.TabIndex = 5
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_RFC.Visible = False
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion.Location = New System.Drawing.Point(31, 212)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion.TabIndex = 6
        Me.txt_Direccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion2.Location = New System.Drawing.Point(51, 212)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion2.TabIndex = 7
        Me.txt_Direccion2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion2.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Location = New System.Drawing.Point(71, 212)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(14, 29)
        Me.txt_TipoCliente.TabIndex = 8
        Me.txt_TipoCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_TipoCliente.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(24, 218)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(103, 23)
        Me.Label31.TabIndex = 18
        Me.Label31.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(133, 220)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(284, 21)
        Me.lstImpresoras.TabIndex = 1
        '
        'txt_Empresa1
        '
        Me.txt_Empresa1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa1.Location = New System.Drawing.Point(403, 215)
        Me.txt_Empresa1.Name = "txt_Empresa1"
        Me.txt_Empresa1.Size = New System.Drawing.Size(14, 29)
        Me.txt_Empresa1.TabIndex = 19
        Me.txt_Empresa1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Empresa1.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cajero.Location = New System.Drawing.Point(432, 215)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(14, 29)
        Me.txt_Cajero.TabIndex = 20
        Me.txt_Cajero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Cajero.Visible = False
        '
        'txt_FormatoTicket
        '
        Me.txt_FormatoTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FormatoTicket.Location = New System.Drawing.Point(423, 215)
        Me.txt_FormatoTicket.Name = "txt_FormatoTicket"
        Me.txt_FormatoTicket.Size = New System.Drawing.Size(86, 29)
        Me.txt_FormatoTicket.TabIndex = 22
        Me.txt_FormatoTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_MargenesTicket
        '
        Me.txt_MargenesTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_MargenesTicket.Location = New System.Drawing.Point(380, 215)
        Me.txt_MargenesTicket.Name = "txt_MargenesTicket"
        Me.txt_MargenesTicket.Size = New System.Drawing.Size(14, 29)
        Me.txt_MargenesTicket.TabIndex = 23
        Me.txt_MargenesTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_MargenesTicket.Visible = False
        '
        'pdvCorteReimprimir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(519, 250)
        Me.Controls.Add(Me.txt_MargenesTicket)
        Me.Controls.Add(Me.txt_Cajero)
        Me.Controls.Add(Me.txt_Empresa1)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.lstImpresoras)
        Me.Controls.Add(Me.txt_TipoCliente)
        Me.Controls.Add(Me.txt_Direccion2)
        Me.Controls.Add(Me.txt_Direccion)
        Me.Controls.Add(Me.txt_RFC)
        Me.Controls.Add(Me.gcDatosTicket)
        Me.Controls.Add(Me.txt_FormatoTicket)
        Me.Name = "pdvCorteReimprimir"
        Me.Text = "Corte Reimprimir"
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosTicket.ResumeLayout(False)
        Me.gcDatosTicket.PerformLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Corte, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gcDatosTicket As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Buscar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Corte As NumericUpDown
    Friend WithEvents lbl_Corte As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents cbo_Sucursal As ComboBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txt_Empresa As TextBox
    Private WithEvents txt_RFC As TextBox
    Private WithEvents txt_Direccion As TextBox
    Private WithEvents txt_Direccion2 As TextBox
    Private WithEvents txt_TipoCliente As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Private WithEvents txt_Empresa1 As TextBox
    Private WithEvents txt_Cajero As TextBox
    Private WithEvents txt_Caja As TextBox
    Private WithEvents txt_FormatoTicket As TextBox
    Private WithEvents txt_MargenesTicket As TextBox
    Friend WithEvents lbl_Caja As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
End Class
