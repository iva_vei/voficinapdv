﻿Public Class pdvVouchers
    Private _sw_pendientes As Boolean
    Private _id_voucher As Integer
    Private _referencia As String
    Private _numcontrol As String
    Private _autorizacion As String
    Private _Accion As Boolean
    Public Property Sw_pendientes As Boolean
        Get
            Return _sw_pendientes
        End Get
        Set(value As Boolean)
            _sw_pendientes = value
        End Set
    End Property

    Public Property IdVoucher As Integer
        Get
            Return _id_voucher
        End Get
        Set(value As Integer)
            _id_voucher = value
        End Set
    End Property

    Public Property Referencia As String
        Get
            Return _referencia
        End Get
        Set(value As String)
            _referencia = value
        End Set
    End Property

    Public Property Numcontrol As String
        Get
            Return _numcontrol
        End Get
        Set(value As String)
            _numcontrol = value
        End Set
    End Property

    Public Property Autorizacion As String
        Get
            Return _autorizacion
        End Get
        Set(value As String)
            _autorizacion = value
        End Set
    End Property

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            dat_FechaIni.Value = Today
            dat_FechaFin.Value = Today
            ' selecciona la impresora predeterminada  
            Dim pd As New Printing.PrintDocument
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            lstImpresoras.Text = s_Default_Printer

            cbo_Banco.SelectedIndex = 0

            Call Refrescar()

            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                lstImpresoras.Enabled = True
            Else
                lstImpresoras.Enabled = False
            End If
            Dim oDatos As Datos_Viscoi
            Dim dtDatos As DataTable = Nothing
            Dim DatosConexion() As String
            Dim sCaja As String = ""
            Dim Msj As String = ""
            Dim sPinPad As String = ""
            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    sCaja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", sCaja, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    sPinPad = dtDatos.Rows(0).Item("printerdriver1")

                    Dim _Puerto As String
                    Dim _User As String
                    Dim _Pswd As String
                    Dim _IdMerchant As String
                    Dim _Url As String
                    Dim _Modo As String
                    Try
                        If sPinPad <> "" Then
                            If sPinPad.Split("|").Length = 7 Then
                                Globales.oAmbientes.PinPadBanco = sPinPad.Split("|")(0).ToString.ToUpper
                                _Puerto = sPinPad.Split("|")(1).ToString.ToUpper
                                _User = sPinPad.Split("|")(2)
                                _Pswd = sPinPad.Split("|")(3)
                                _IdMerchant = sPinPad.Split("|")(4)
                                _Url = sPinPad.Split("|")(5).ToString
                                _Modo = sPinPad.Split("|")(6).ToString.ToUpper

                                Select Case Globales.oAmbientes.PinPadBanco
                                    Case "BANORTE"
                                        Globales.oAmbientes.oPinPadBanorte = New Globales.clsPinPadBanorte(_Puerto, _User, _Pswd, _IdMerchant, _Modo, _Url, sCaja)
                                        Dim oSplash As New pdvPinPadSplash
                                        oSplash.PinAccion = pdvPinPadSplash.PinpadAccion.Inicializa
                                        oSplash.StartPosition = FormStartPosition.CenterScreen
                                        oSplash.ShowDialog()
                                End Select
                            Else
                                MessageBox.Show("Faltan parametros de configuracion para la PINPAD.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        End If
                    Catch ex As Exception
                        Globales.oAmbientes.PinPadBanco = ""
                        _User = ""
                        _Pswd = ""
                        _IdMerchant = ""
                        _Modo = ""
                        MessageBox.Show("El pinpad no se inicializo corectamente." & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    End Try

                Else
                    MessageBox.Show("Caja(" & sCaja & ") no esta registrada para Sucursal(" & Globales.oAmbientes.Id_Sucursal & ").", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.Close()
                End If
            Else
                MessageBox.Show("Caja(" & sCaja & ") no esta registrada.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Voucher_Sel(Globales.oAmbientes.Id_Empresa, cbo_Banco.Text, dat_FechaIni.Value, dat_FechaFin.Value, _sw_pendientes, dtDatos, Mensaje) Then
                GridC_1.DataSource = dtDatos
                GridC_1.RefreshDataSource()
                GridV_1.BestFitColumns()
            Else
                GridC_1.DataSource = Nothing
                GridC_1.RefreshDataSource()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim sw_continuar As Boolean = True
        Dim Requeridos As Double = 0
        Dim Disponible As Double = 0

        Try

            If GridV_1.RowCount > 0 Then
                _id_voucher = GridV_1.GetFocusedRowCellValue(col1_IdVoucher)
                _referencia = GridV_1.GetFocusedRowCellValue(col1_Referencia)
                _numcontrol = GridV_1.GetFocusedRowCellValue(col1_Numcontrol)
                _autorizacion = GridV_1.GetFocusedRowCellValue(col1_Codigoaut)
                _Accion = True
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _id_voucher = 0
        _referencia = ""
        _numcontrol = ""
        _autorizacion = ""
        _Accion = False
        Me.Close()
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim msj As String = ""
        Dim oVoucher As Globales.clsPinPadBanorte
        Try
            If GridV_1.RowCount > 0 Then
                Select Case cbo_Banco.Text
                    Case "BANORTE"
                        Reporte = New xtraRepVoucherBanorte
                        oVoucher = New Globales.clsPinPadBanorte("", "", "", "", "", "", "")

                        If oVoucher.FillDatos(
                              GridV_1.GetFocusedRowCellValue(col1_Urlbanorte) _
                            , GridV_1.GetFocusedRowCellValue(col1_Puerto) _
                            , GridV_1.GetFocusedRowCellValue(col1_User1) _
                            , GridV_1.GetFocusedRowCellValue(col1_Pswd) _
                            , GridV_1.GetFocusedRowCellValue(col1_Idmerchant) _
                            , GridV_1.GetFocusedRowCellValue(col1_Modo) _
                            , GridV_1.GetFocusedRowCellValue(col1_Caja) _
                            , GridV_1.GetFocusedRowCellValue(col1_IdEmpresa) _
                            , GridV_1.GetFocusedRowCellValue(col1_Empresa) _
                            , GridV_1.GetFocusedRowCellValue(col1_Rfc) _
                            , GridV_1.GetFocusedRowCellValue(col1_Direccion) _
                            , GridV_1.GetFocusedRowCellValue(col1_Direccion2) _
                            , GridV_1.GetFocusedRowCellValue(col1_Tipo) _
                            , GridV_1.GetFocusedRowCellValue(col1_Transaccion) _
                            , GridV_1.GetFocusedRowCellValue(col1_IdSucursal) _
                            , GridV_1.GetFocusedRowCellValue(col1_Fecha) _
                            , GridV_1.GetFocusedRowCellValue(col1_Hora) _
                            , GridV_1.GetFocusedRowCellValue(col1_Banco) _
                            , GridV_1.GetFocusedRowCellValue(col1_Respuesta) _
                            , GridV_1.GetFocusedRowCellValue(col1_Afiliacion) _
                            , GridV_1.GetFocusedRowCellValue(col1_Terminalid) _
                            , GridV_1.GetFocusedRowCellValue(col1_Numcontrol) _
                            , GridV_1.GetFocusedRowCellValue(col1_Numtarjeta) _
                            , GridV_1.GetFocusedRowCellValue(col1_Tiptarjeta) _
                            , GridV_1.GetFocusedRowCellValue(col1_Vigtarjeta) _
                            , GridV_1.GetFocusedRowCellValue(col1_Tittarjeta) _
                            , GridV_1.GetFocusedRowCellValue(col1_Bancoemisor) _
                            , GridV_1.GetFocusedRowCellValue(col1_Codigoaut) _
                            , GridV_1.GetFocusedRowCellValue(col1_Referencia) _
                            , GridV_1.GetFocusedRowCellValue(col1_Importe) _
                            , GridV_1.GetFocusedRowCellValue(col1_Mesesdiferido) _
                            , GridV_1.GetFocusedRowCellValue(col1_Numpagos) _
                            , GridV_1.GetFocusedRowCellValue(col1_Tipoplan) _
                            , msj) Then

                            TryCast(Reporte, xtraRepVoucherBanorte).MargenAbj = 0
                            TryCast(Reporte, xtraRepVoucherBanorte).MargenIzq = 0
                            TryCast(Reporte, xtraRepVoucherBanorte).MargenDer = 0
                            TryCast(Reporte, xtraRepVoucherBanorte).MargenArr = 0
                            TryCast(Reporte, xtraRepVoucherBanorte).TipoCliente = ""
                            TryCast(Reporte, xtraRepVoucherBanorte).ods.DataSource = oVoucher
                            TryCast(Reporte, xtraRepVoucherBanorte).CreateDocument()
                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepVoucherBanorte).PrintingSystem)

                            printBase.Print(lstImpresoras.Text)
                        Else
                            MessageBox.Show(msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    Case Else
                        ''sw_continuar = False
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            If GridV_1.FocusedRowHandle >= 0 Then
                Dim oSplash As New pdvPinPadSplash
                oSplash.PinAccion = pdvPinPadSplash.PinpadAccion.Verificacion
                oSplash.Impresora = lstImpresoras.Text
                oSplash.Total = GridV_1.GetFocusedRowCellValue(col1_Importe.FieldName)
                oSplash.NoControl = GridV_1.GetFocusedRowCellValue(col1_Numcontrol.FieldName)
                oSplash.IdVoucher = GridV_1.GetFocusedRowCellValue(col1_IdVoucher.FieldName)
                oSplash.StartPosition = FormStartPosition.CenterScreen
                oSplash.MesesDiferido = ""
                oSplash.NumPagos = ""
                oSplash.TipoPlan = ""

                oSplash.ShowDialog()
                Call Refrescar()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class