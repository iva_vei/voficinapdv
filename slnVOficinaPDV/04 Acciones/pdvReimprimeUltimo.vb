﻿Public Class pdvReimprimeUltimo
    Dim _Accion As Boolean


    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim DatosConexion() As String
        Try
            Dim pd As New Printing.PrintDocument
            Dim Impresoras As String

            ' Default printer      
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName

            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer
            DatosConexion = System.IO.File.ReadAllLines(Application.StartupPath & "\PuntoDeVenta.txt")
            Globales.oAmbientes.Id_Sucursal = DatosConexion(0)
            txt_Caja.Text = DatosConexion(1)
            txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Try
            _Accion = True
            ImprimirTicket()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub ImprimirTicket()
        Dim Reporte As xtraRepTicketVenta
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_DetVentas_Impresion(Today.Date _
                                              , txt_Autonumsuc.Text _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , Mensaje) Then

                '*************************************************************************
                If dtDatos1.Rows.Count > 0 Then
                    txt_Articulos.Text = Format(dtDatos1.Rows(0).Item("articulos"), "N0")
                    txt_Cantidad.Text = Format(dtDatos1.Rows(0).Item("cantidad"), "N2")
                    txt_SubTotal.Text = Format(dtDatos1.Rows(0).Item("subtotal"), "C2")
                    txt_IVA.Text = Format(dtDatos1.Rows(0).Item("iva"), "C2")
                    txt_Total.Text = Format(dtDatos1.Rows(0).Item("total"), "C2")
                    txt1_Descto.Text = Format(dtDatos1.Rows(0).Item("descuento"), "C2")

                    txt_Contado.Text = Format(dtDatos1.Rows(0).Item("contado"), "C2")
                    txt_DE.Text = Format(dtDatos1.Rows(0).Item("dinelec"), "C2")
                    txt_Cambio.Text = Format(dtDatos1.Rows(0).Item("cambio"), "C2")

                    ''****************************************************************************************
                    ''**                                    PUNTOS                                          **
                    ''****************************************************************************************
                    txt_PuntosAcumulados.Text = Format(dtDatos1.Rows(0).Item("puntos_acumular"), "N0")
                    txt_PuntosUtilizados.Text = Format(dtDatos1.Rows(0).Item("puntos_utilizados"), "N0")

                    txt_PuntosActual.Text = txt_PuntosActual.Text.Replace("$", "")
                    txt_PuntosActual.Text = txt_PuntosActual.Text.Replace(",", "")
                    txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace("$", "")
                    txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace(",", "")
                    txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace("$", "")
                    txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace(",", "")
                    Double.TryParse(txt_PuntosActual.Text, PuntosActual)
                    Double.TryParse(txt_PuntosAcumulados.Text, PuntosAcumulados)
                    Double.TryParse(txt_PuntosUtilizados.Text, PuntosUtilizados)

                    PuntosFinal = PuntosActual + PuntosAcumulados - PuntosUtilizados

                    txt_PuntosFinal.Text = PuntosFinal.ToString

                    ''****************************************************************************************
                    ''**                                    DinEle                                          **
                    ''****************************************************************************************
                    txt_DinEleAcumulados.Text = Format(dtDatos1.Rows(0).Item("de_acumular"), "N0")
                    txt_DinEleUtilizados.Text = Format(dtDatos1.Rows(0).Item("de_utilizados"), "N0")

                    txt_DinEleActual.Text = txt_DinEleActual.Text.Replace("$", "")
                    txt_DinEleActual.Text = txt_DinEleActual.Text.Replace(",", "")
                    txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace("$", "")
                    txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace(",", "")
                    txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace("$", "")
                    txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace(",", "")
                    Double.TryParse(txt_DinEleActual.Text, DEActual)
                    Double.TryParse(txt_DinEleAcumulados.Text, DEAcumulados)
                    Double.TryParse(txt_DinEleUtilizados.Text, DEUtilizados)

                    DEFinal = DEActual + DEAcumulados - DEUtilizados

                    txt_DinEleFinal.Text = DEFinal.ToString

                    txt_DesctoGlobal.Text = dtDatos1.Rows(0).Item("descuento_global")
                Else
                    txt_Articulos.Text = 0
                    txt_Cantidad.Text = 0
                    txt_SubTotal.Text = 0
                    txt_IVA.Text = 0
                    txt_Total.Text = 0
                    txt_Total.Text = 0

                    txt_PuntosAcumulados.Text = "0"
                    txt_PuntosUtilizados.Text = "0"

                    Double.TryParse(txt_PuntosActual.Text, PuntosActual)
                    Double.TryParse(txt_PuntosAcumulados.Text, PuntosAcumulados)
                    Double.TryParse(txt_PuntosUtilizados.Text, PuntosUtilizados)

                    PuntosFinal = PuntosActual + PuntosAcumulados - PuntosUtilizados

                    txt_PuntosFinal.Text = PuntosFinal.ToString

                    txt_DesctoGlobal.Text = "0"
                End If
                '*************************************************************************

                txt_PuntosActual.Text = txt_PuntosActual.Text.Replace("$", "")
                txt_PuntosActual.Text = txt_PuntosActual.Text.Replace(",", "")
                txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace("$", "")
                txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace(",", "")
                txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace("$", "")
                txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace(",", "")
                Double.TryParse(txt_PuntosActual.Text, PuntosActual)
                Double.TryParse(txt_PuntosAcumulados.Text, PuntosAcumulados)
                Double.TryParse(txt_PuntosUtilizados.Text, PuntosUtilizados)

                PuntosFinal = PuntosActual + PuntosAcumulados - PuntosUtilizados

                txt_DinEleActual.Text = txt_DinEleActual.Text.Replace("$", "")
                txt_DinEleActual.Text = txt_DinEleActual.Text.Replace(",", "")
                txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace("$", "")
                txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace(",", "")
                txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace("$", "")
                txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace(",", "")
                Double.TryParse(txt_DinEleActual.Text, DEActual)
                Double.TryParse(txt_DinEleAcumulados.Text, DEAcumulados)
                Double.TryParse(txt_DinEleUtilizados.Text, DEUtilizados)

                DEFinal = DEActual + DEAcumulados - DEUtilizados

                oTicket = New entTicketVenta
                oTicket.Fill(dtDatos, dtDatos1, Nothing)

                oTicket.Empresa = txt_Empresa.Text
                oTicket.Rfc = txt_RFC.Text
                oTicket.Direccion = txt_Direccion.Text
                oTicket.Direccion2 = txt_Direccion2.Text
                oTicket.Cajero = txt_Cajero.Text
                oTicket.Caja = txt_Caja.Text

                If txt_NombreCanje.Text.Length > 0 Then
                    oTicket.Lealtad_nombre = txt_NombreCanje.Text
                    oTicket.Puntos_saldo_ant = PuntosActual
                    oTicket.Puntos_acumulados = PuntosAcumulados
                    oTicket.Puntos_utilizados = PuntosUtilizados
                    oTicket.Puntos_saldo_act = PuntosFinal

                    oTicket.De_saldo_ant = DEActual
                    oTicket.De_acumulados = DEAcumulados
                    oTicket.De_utilizados = DEUtilizados
                    oTicket.De_saldo_act = DEFinal
                End If
                Reporte = New xtraRepTicketVenta
                Reporte.TipoCliente = txt_TipoCliente.Text
                Reporte.ods.DataSource = oTicket
                ''printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                ''printTool.ShowPreviewDialog()

                Reporte.CreateDocument()
                Dim printBase = New DevExpress.XtraPrinting.PrintToolBase(Reporte.PrintingSystem)
                printBase.Print(lstImpresoras.Text)

                ''''Dim objFSO
                ''''Dim objStream
                ''''
                ''''Try
                ''''    objFSO = CreateObject("Scripting.FileSystemObject")
                ''''    objStream = objFSO.CreateTextFile("LPT1") 'Puerto al cual se envía la impresión
                ''''    objStream.Writeline(Chr(27) & "p" & Chr(0) & Chr(100) & Chr(250)) 'Abrir cajón
                ''''    objStream.Close()
                ''''
                ''''Catch ex As Exception
                ''''
                ''''Finally
                ''''    ''objStream.Writeline(Chr(27) & Chr(64)) ' limpia Buffer de la impresora
                ''''    ''objStream.Writeline(Chr(27) & Chr(60)) ' la deja en Posicion Stand BY
                ''''    objFSO = Nothing
                ''''    objStream = Nothing
                ''''End Try
                ''''
                ''''Try
                ''''    FileOpen(1, "LPT1", OpenMode.Output)
                ''''    PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(100) & Chr(250))
                ''''    FileClose(1)
                ''''Catch ex As Exception
                ''''
                ''''End Try
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class