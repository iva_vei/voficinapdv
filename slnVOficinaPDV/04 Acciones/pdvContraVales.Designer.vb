﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvContraVales
    Inherits DevExpress.XtraBars.ToolbarForm.ToolbarForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.gridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Banco = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Hora = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numtarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tittarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Vigtarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Referencia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numcontrol = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Codigoaut = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Importe = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fum = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdVoucher = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Urlbanorte = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Puerto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_User1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Pswd = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Idmerchant = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Modo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Caja = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdEmpresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Empresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Rfc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Direccion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Direccion2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Transaccion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdSucursal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Respuesta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Afiliacion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Terminalid = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tiptarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Bancoemisor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Mesesdiferido = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numpagos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipoplan = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Autonumsuc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.cbo_Banco = New System.Windows.Forms.ComboBox()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dat_FechaFin = New System.Windows.Forms.DateTimePicker()
        Me.dat_FechaIni = New System.Windows.Forms.DateTimePicker()
        Me.btn_Refrescar = New System.Windows.Forms.Button()
        Me.lbl_Cuenta = New System.Windows.Forms.Label()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.RibbonStatusBar1 = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.ToolbarFormManager1 = New DevExpress.XtraBars.ToolbarForm.ToolbarFormManager(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.pdvContraValesLayoutControl1ConvertedLayout = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.GroupControl3item = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.GroupControl1item = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.GroupControl2item = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.btn_Aceptaritem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.btn_Cancelaritem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lstImpresorasitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.dat_FechaFinitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.dat_FechaIniitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Button2item = New DevExpress.XtraLayout.LayoutControlItem()
        Me.cbo_Bancoitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Button1item = New DevExpress.XtraLayout.LayoutControlItem()
        Me.btn_Refrescaritem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lbl_Cuentaitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.GridC_1item = New DevExpress.XtraLayout.LayoutControlItem()
        Me.FluentDesignFormContainer1 = New DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer()
        Me.AccordionControl1 = New DevExpress.XtraBars.Navigation.AccordionControl()
        Me.AccordionControlElement1 = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.FluentDesignFormControl1 = New DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToolbarFormManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pdvContraValesLayoutControl1ConvertedLayout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pdvContraValesLayoutControl1ConvertedLayout.SuspendLayout()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3item, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1item, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2item, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_Aceptaritem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_Cancelaritem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lstImpresorasitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dat_FechaFinitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dat_FechaIniitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Button2item, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbo_Bancoitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Button1item, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_Refrescaritem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbl_Cuentaitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridC_1item, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccordionControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FluentDesignFormControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridC_1
        '
        Me.GridC_1.Location = New System.Drawing.Point(24, 45)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(580, 358)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.gridBand1})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_IdVoucher, Me.col1_Urlbanorte, Me.col1_Puerto, Me.col1_User1, Me.col1_Pswd, Me.col1_Idmerchant, Me.col1_Modo, Me.col1_Caja, Me.col1_IdEmpresa, Me.col1_Empresa, Me.col1_Rfc, Me.col1_Direccion, Me.col1_Direccion2, Me.col1_Tipo, Me.col1_Transaccion, Me.col1_IdSucursal, Me.col1_Fecha, Me.col1_Hora, Me.col1_Banco, Me.col1_Respuesta, Me.col1_Afiliacion, Me.col1_Terminalid, Me.col1_Numcontrol, Me.col1_Numtarjeta, Me.col1_Tiptarjeta, Me.col1_Vigtarjeta, Me.col1_Tittarjeta, Me.col1_Bancoemisor, Me.col1_Codigoaut, Me.col1_Referencia, Me.col1_Importe, Me.col1_Mesesdiferido, Me.col1_Numpagos, Me.col1_Tipoplan, Me.col1_Autonumsuc, Me.col1_Fum})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'gridBand1
        '
        Me.gridBand1.Columns.Add(Me.col1_Banco)
        Me.gridBand1.Columns.Add(Me.col1_Fecha)
        Me.gridBand1.Columns.Add(Me.col1_Hora)
        Me.gridBand1.Columns.Add(Me.col1_Numtarjeta)
        Me.gridBand1.Columns.Add(Me.col1_Tittarjeta)
        Me.gridBand1.Columns.Add(Me.col1_Vigtarjeta)
        Me.gridBand1.Columns.Add(Me.col1_Referencia)
        Me.gridBand1.Columns.Add(Me.col1_Numcontrol)
        Me.gridBand1.Columns.Add(Me.col1_Codigoaut)
        Me.gridBand1.Columns.Add(Me.col1_Importe)
        Me.gridBand1.Columns.Add(Me.col1_Fum)
        Me.gridBand1.Name = "gridBand1"
        Me.gridBand1.VisibleIndex = 0
        Me.gridBand1.Width = 825
        '
        'col1_Banco
        '
        Me.col1_Banco.Caption = "Banco"
        Me.col1_Banco.FieldName = "Banco"
        Me.col1_Banco.Name = "col1_Banco"
        Me.col1_Banco.OptionsColumn.AllowEdit = False
        Me.col1_Banco.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Banco.Visible = True
        '
        'col1_Fecha
        '
        Me.col1_Fecha.Caption = "Fecha"
        Me.col1_Fecha.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.col1_Fecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col1_Fecha.FieldName = "fecha"
        Me.col1_Fecha.Name = "col1_Fecha"
        Me.col1_Fecha.OptionsColumn.AllowEdit = False
        Me.col1_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fecha.Visible = True
        '
        'col1_Hora
        '
        Me.col1_Hora.Caption = "Hora"
        Me.col1_Hora.FieldName = "hora"
        Me.col1_Hora.Name = "col1_Hora"
        Me.col1_Hora.OptionsColumn.AllowEdit = False
        Me.col1_Hora.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Hora.Visible = True
        '
        'col1_Numtarjeta
        '
        Me.col1_Numtarjeta.Caption = "Numtarjeta"
        Me.col1_Numtarjeta.FieldName = "NumTarjeta"
        Me.col1_Numtarjeta.Name = "col1_Numtarjeta"
        Me.col1_Numtarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Numtarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numtarjeta.Visible = True
        '
        'col1_Tittarjeta
        '
        Me.col1_Tittarjeta.Caption = "Tittarjeta"
        Me.col1_Tittarjeta.FieldName = "TitTarjeta"
        Me.col1_Tittarjeta.Name = "col1_Tittarjeta"
        Me.col1_Tittarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Tittarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tittarjeta.Visible = True
        '
        'col1_Vigtarjeta
        '
        Me.col1_Vigtarjeta.Caption = "Vigtarjeta"
        Me.col1_Vigtarjeta.FieldName = "VigTarjeta"
        Me.col1_Vigtarjeta.Name = "col1_Vigtarjeta"
        Me.col1_Vigtarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Vigtarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Vigtarjeta.Visible = True
        '
        'col1_Referencia
        '
        Me.col1_Referencia.Caption = "Referencia"
        Me.col1_Referencia.FieldName = "Referencia"
        Me.col1_Referencia.Name = "col1_Referencia"
        Me.col1_Referencia.OptionsColumn.AllowEdit = False
        Me.col1_Referencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Referencia.Visible = True
        '
        'col1_Numcontrol
        '
        Me.col1_Numcontrol.Caption = "Numcontrol"
        Me.col1_Numcontrol.FieldName = "NumControl"
        Me.col1_Numcontrol.Name = "col1_Numcontrol"
        Me.col1_Numcontrol.OptionsColumn.AllowEdit = False
        Me.col1_Numcontrol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numcontrol.Visible = True
        '
        'col1_Codigoaut
        '
        Me.col1_Codigoaut.Caption = "Codigoaut"
        Me.col1_Codigoaut.FieldName = "CodigoAut"
        Me.col1_Codigoaut.Name = "col1_Codigoaut"
        Me.col1_Codigoaut.OptionsColumn.AllowEdit = False
        Me.col1_Codigoaut.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Codigoaut.Visible = True
        '
        'col1_Importe
        '
        Me.col1_Importe.Caption = "Importe"
        Me.col1_Importe.FieldName = "Importe"
        Me.col1_Importe.Name = "col1_Importe"
        Me.col1_Importe.OptionsColumn.AllowEdit = False
        Me.col1_Importe.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Importe.Visible = True
        '
        'col1_Fum
        '
        Me.col1_Fum.Caption = "Fum"
        Me.col1_Fum.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm"
        Me.col1_Fum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col1_Fum.FieldName = "fum"
        Me.col1_Fum.Name = "col1_Fum"
        Me.col1_Fum.OptionsColumn.AllowEdit = False
        Me.col1_Fum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fum.Visible = True
        '
        'col1_IdVoucher
        '
        Me.col1_IdVoucher.Caption = "IdVoucher"
        Me.col1_IdVoucher.FieldName = "id_voucher"
        Me.col1_IdVoucher.Name = "col1_IdVoucher"
        Me.col1_IdVoucher.OptionsColumn.AllowEdit = False
        Me.col1_IdVoucher.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdVoucher.Visible = True
        '
        'col1_Urlbanorte
        '
        Me.col1_Urlbanorte.Caption = "Urlbanorte"
        Me.col1_Urlbanorte.FieldName = "UrlBanorte"
        Me.col1_Urlbanorte.Name = "col1_Urlbanorte"
        Me.col1_Urlbanorte.OptionsColumn.AllowEdit = False
        Me.col1_Urlbanorte.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Urlbanorte.Visible = True
        '
        'col1_Puerto
        '
        Me.col1_Puerto.Caption = "Puerto"
        Me.col1_Puerto.FieldName = "Puerto"
        Me.col1_Puerto.Name = "col1_Puerto"
        Me.col1_Puerto.OptionsColumn.AllowEdit = False
        Me.col1_Puerto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Puerto.Visible = True
        '
        'col1_User1
        '
        Me.col1_User1.Caption = "User1"
        Me.col1_User1.FieldName = "User1"
        Me.col1_User1.Name = "col1_User1"
        Me.col1_User1.OptionsColumn.AllowEdit = False
        Me.col1_User1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_User1.Visible = True
        '
        'col1_Pswd
        '
        Me.col1_Pswd.Caption = "Pswd"
        Me.col1_Pswd.FieldName = "Pswd"
        Me.col1_Pswd.Name = "col1_Pswd"
        Me.col1_Pswd.OptionsColumn.AllowEdit = False
        Me.col1_Pswd.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Pswd.Visible = True
        '
        'col1_Idmerchant
        '
        Me.col1_Idmerchant.Caption = "Idmerchant"
        Me.col1_Idmerchant.FieldName = "IdMerchant"
        Me.col1_Idmerchant.Name = "col1_Idmerchant"
        Me.col1_Idmerchant.OptionsColumn.AllowEdit = False
        Me.col1_Idmerchant.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Idmerchant.Visible = True
        '
        'col1_Modo
        '
        Me.col1_Modo.Caption = "Modo"
        Me.col1_Modo.FieldName = "Modo"
        Me.col1_Modo.Name = "col1_Modo"
        Me.col1_Modo.OptionsColumn.AllowEdit = False
        Me.col1_Modo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Modo.Visible = True
        '
        'col1_Caja
        '
        Me.col1_Caja.Caption = "Caja"
        Me.col1_Caja.FieldName = "Caja"
        Me.col1_Caja.Name = "col1_Caja"
        Me.col1_Caja.OptionsColumn.AllowEdit = False
        Me.col1_Caja.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Caja.Visible = True
        '
        'col1_IdEmpresa
        '
        Me.col1_IdEmpresa.Caption = "IdEmpresa"
        Me.col1_IdEmpresa.FieldName = "id_empresa"
        Me.col1_IdEmpresa.Name = "col1_IdEmpresa"
        Me.col1_IdEmpresa.OptionsColumn.AllowEdit = False
        Me.col1_IdEmpresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdEmpresa.Visible = True
        '
        'col1_Empresa
        '
        Me.col1_Empresa.Caption = "Empresa"
        Me.col1_Empresa.FieldName = "empresa"
        Me.col1_Empresa.Name = "col1_Empresa"
        Me.col1_Empresa.OptionsColumn.AllowEdit = False
        Me.col1_Empresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Empresa.Visible = True
        '
        'col1_Rfc
        '
        Me.col1_Rfc.Caption = "Rfc"
        Me.col1_Rfc.FieldName = "rfc"
        Me.col1_Rfc.Name = "col1_Rfc"
        Me.col1_Rfc.OptionsColumn.AllowEdit = False
        Me.col1_Rfc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Rfc.Visible = True
        '
        'col1_Direccion
        '
        Me.col1_Direccion.Caption = "Direccion"
        Me.col1_Direccion.FieldName = "direccion"
        Me.col1_Direccion.Name = "col1_Direccion"
        Me.col1_Direccion.OptionsColumn.AllowEdit = False
        Me.col1_Direccion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Direccion.Visible = True
        '
        'col1_Direccion2
        '
        Me.col1_Direccion2.Caption = "Direccion2"
        Me.col1_Direccion2.FieldName = "direccion2"
        Me.col1_Direccion2.Name = "col1_Direccion2"
        Me.col1_Direccion2.OptionsColumn.AllowEdit = False
        Me.col1_Direccion2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Direccion2.Visible = True
        '
        'col1_Tipo
        '
        Me.col1_Tipo.Caption = "Tipo"
        Me.col1_Tipo.FieldName = "tipo"
        Me.col1_Tipo.Name = "col1_Tipo"
        Me.col1_Tipo.OptionsColumn.AllowEdit = False
        Me.col1_Tipo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipo.Visible = True
        '
        'col1_Transaccion
        '
        Me.col1_Transaccion.Caption = "Transaccion"
        Me.col1_Transaccion.FieldName = "transaccion"
        Me.col1_Transaccion.Name = "col1_Transaccion"
        Me.col1_Transaccion.OptionsColumn.AllowEdit = False
        Me.col1_Transaccion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Transaccion.Visible = True
        '
        'col1_IdSucursal
        '
        Me.col1_IdSucursal.Caption = "IdSucursal"
        Me.col1_IdSucursal.FieldName = "id_sucursal"
        Me.col1_IdSucursal.Name = "col1_IdSucursal"
        Me.col1_IdSucursal.OptionsColumn.AllowEdit = False
        Me.col1_IdSucursal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdSucursal.Visible = True
        '
        'col1_Respuesta
        '
        Me.col1_Respuesta.Caption = "Respuesta"
        Me.col1_Respuesta.FieldName = "Respuesta"
        Me.col1_Respuesta.Name = "col1_Respuesta"
        Me.col1_Respuesta.OptionsColumn.AllowEdit = False
        Me.col1_Respuesta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Respuesta.Visible = True
        '
        'col1_Afiliacion
        '
        Me.col1_Afiliacion.Caption = "Afiliacion"
        Me.col1_Afiliacion.FieldName = "Afiliacion"
        Me.col1_Afiliacion.Name = "col1_Afiliacion"
        Me.col1_Afiliacion.OptionsColumn.AllowEdit = False
        Me.col1_Afiliacion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Afiliacion.Visible = True
        '
        'col1_Terminalid
        '
        Me.col1_Terminalid.Caption = "Terminalid"
        Me.col1_Terminalid.FieldName = "TerminalId"
        Me.col1_Terminalid.Name = "col1_Terminalid"
        Me.col1_Terminalid.OptionsColumn.AllowEdit = False
        Me.col1_Terminalid.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Terminalid.Visible = True
        '
        'col1_Tiptarjeta
        '
        Me.col1_Tiptarjeta.Caption = "Tiptarjeta"
        Me.col1_Tiptarjeta.FieldName = "TipTarjeta"
        Me.col1_Tiptarjeta.Name = "col1_Tiptarjeta"
        Me.col1_Tiptarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Tiptarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tiptarjeta.Visible = True
        '
        'col1_Bancoemisor
        '
        Me.col1_Bancoemisor.Caption = "Bancoemisor"
        Me.col1_Bancoemisor.FieldName = "BancoEmisor"
        Me.col1_Bancoemisor.Name = "col1_Bancoemisor"
        Me.col1_Bancoemisor.OptionsColumn.AllowEdit = False
        Me.col1_Bancoemisor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Bancoemisor.Visible = True
        '
        'col1_Mesesdiferido
        '
        Me.col1_Mesesdiferido.Caption = "Mesesdiferido"
        Me.col1_Mesesdiferido.FieldName = "MesesDiferido"
        Me.col1_Mesesdiferido.Name = "col1_Mesesdiferido"
        Me.col1_Mesesdiferido.OptionsColumn.AllowEdit = False
        Me.col1_Mesesdiferido.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Mesesdiferido.Visible = True
        '
        'col1_Numpagos
        '
        Me.col1_Numpagos.Caption = "Numpagos"
        Me.col1_Numpagos.FieldName = "NumPagos"
        Me.col1_Numpagos.Name = "col1_Numpagos"
        Me.col1_Numpagos.OptionsColumn.AllowEdit = False
        Me.col1_Numpagos.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numpagos.Visible = True
        '
        'col1_Tipoplan
        '
        Me.col1_Tipoplan.Caption = "Tipoplan"
        Me.col1_Tipoplan.FieldName = "TipoPlan"
        Me.col1_Tipoplan.Name = "col1_Tipoplan"
        Me.col1_Tipoplan.OptionsColumn.AllowEdit = False
        Me.col1_Tipoplan.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipoplan.Visible = True
        '
        'col1_Autonumsuc
        '
        Me.col1_Autonumsuc.Caption = "Autonumsuc"
        Me.col1_Autonumsuc.FieldName = "autonumsuc"
        Me.col1_Autonumsuc.Name = "col1_Autonumsuc"
        Me.col1_Autonumsuc.OptionsColumn.AllowEdit = False
        Me.col1_Autonumsuc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Autonumsuc.Visible = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(502, 452)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(56, 21)
        Me.Button2.TabIndex = 23
        Me.Button2.Text = "Verificar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbo_Banco
        '
        Me.cbo_Banco.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Banco.FormattingEnabled = True
        Me.cbo_Banco.Items.AddRange(New Object() {"BANORTE"})
        Me.cbo_Banco.Location = New System.Drawing.Point(218, 452)
        Me.cbo_Banco.Name = "cbo_Banco"
        Me.cbo_Banco.Size = New System.Drawing.Size(244, 31)
        Me.cbo_Banco.TabIndex = 21
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(283, 477)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(321, 21)
        Me.lstImpresoras.TabIndex = 19
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(562, 452)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(42, 21)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Imprimir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dat_FechaFin
        '
        Me.dat_FechaFin.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaFin.Location = New System.Drawing.Point(89, 476)
        Me.dat_FechaFin.Name = "dat_FechaFin"
        Me.dat_FechaFin.Size = New System.Drawing.Size(125, 30)
        Me.dat_FechaFin.TabIndex = 5
        '
        'dat_FechaIni
        '
        Me.dat_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaIni.Location = New System.Drawing.Point(91, 452)
        Me.dat_FechaIni.Name = "dat_FechaIni"
        Me.dat_FechaIni.Size = New System.Drawing.Size(58, 30)
        Me.dat_FechaIni.TabIndex = 4
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.Location = New System.Drawing.Point(466, 452)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(32, 21)
        Me.btn_Refrescar.TabIndex = 0
        Me.btn_Refrescar.Text = "..."
        Me.btn_Refrescar.UseVisualStyleBackColor = True
        '
        'lbl_Cuenta
        '
        Me.lbl_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Cuenta.Location = New System.Drawing.Point(24, 452)
        Me.lbl_Cuenta.Name = "lbl_Cuenta"
        Me.lbl_Cuenta.Size = New System.Drawing.Size(63, 20)
        Me.lbl_Cuenta.TabIndex = 1
        Me.lbl_Cuenta.Text = "Fecha Ini"
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(24, 547)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(83, 167)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(111, 547)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(493, 167)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'RibbonStatusBar1
        '
        Me.RibbonStatusBar1.Location = New System.Drawing.Point(260, 718)
        Me.RibbonStatusBar1.Name = "RibbonStatusBar1"
        Me.RibbonStatusBar1.Size = New System.Drawing.Size(628, 20)
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "RibbonPage2"
        '
        'ToolbarFormManager1
        '
        Me.ToolbarFormManager1.DockControls.Add(Me.barDockControlTop)
        Me.ToolbarFormManager1.DockControls.Add(Me.barDockControlBottom)
        Me.ToolbarFormManager1.DockControls.Add(Me.barDockControlLeft)
        Me.ToolbarFormManager1.DockControls.Add(Me.barDockControlRight)
        Me.ToolbarFormManager1.Form = Me
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.ToolbarFormManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(0, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlBottom.Manager = Me.ToolbarFormManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(0, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Manager = Me.ToolbarFormManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 0)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlRight.Manager = Me.ToolbarFormManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 0)
        '
        'pdvContraValesLayoutControl1ConvertedLayout
        '
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.btn_Aceptar)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.btn_Cancelar)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.lstImpresoras)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.dat_FechaFin)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.dat_FechaIni)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.Button2)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.cbo_Banco)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.Button1)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.btn_Refrescar)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.lbl_Cuenta)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Controls.Add(Me.GridC_1)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pdvContraValesLayoutControl1ConvertedLayout.Location = New System.Drawing.Point(260, 0)
        Me.pdvContraValesLayoutControl1ConvertedLayout.Name = "pdvContraValesLayoutControl1ConvertedLayout"
        Me.pdvContraValesLayoutControl1ConvertedLayout.Root = Me.LayoutControlGroup1
        Me.pdvContraValesLayoutControl1ConvertedLayout.Size = New System.Drawing.Size(628, 738)
        Me.pdvContraValesLayoutControl1ConvertedLayout.TabIndex = 8
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.GroupControl3item, Me.GroupControl1item, Me.GroupControl2item})
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(628, 738)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'GroupControl3item
        '
        Me.GroupControl3item.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.btn_Aceptaritem, Me.btn_Cancelaritem})
        Me.GroupControl3item.Location = New System.Drawing.Point(0, 502)
        Me.GroupControl3item.Name = "GroupControl3item"
        Me.GroupControl3item.Size = New System.Drawing.Size(608, 216)
        Me.GroupControl3item.Text = " "
        '
        'GroupControl1item
        '
        Me.GroupControl1item.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lstImpresorasitem, Me.dat_FechaFinitem, Me.dat_FechaIniitem, Me.Button2item, Me.cbo_Bancoitem, Me.Button1item, Me.btn_Refrescaritem, Me.lbl_Cuentaitem})
        Me.GroupControl1item.Location = New System.Drawing.Point(0, 407)
        Me.GroupControl1item.Name = "GroupControl1item"
        Me.GroupControl1item.Size = New System.Drawing.Size(608, 95)
        Me.GroupControl1item.Text = " "
        '
        'GroupControl2item
        '
        Me.GroupControl2item.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.GridC_1item})
        Me.GroupControl2item.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2item.Name = "GroupControl2item"
        Me.GroupControl2item.Size = New System.Drawing.Size(608, 407)
        Me.GroupControl2item.Text = " "
        '
        'btn_Aceptaritem
        '
        Me.btn_Aceptaritem.Control = Me.btn_Aceptar
        Me.btn_Aceptaritem.Location = New System.Drawing.Point(0, 0)
        Me.btn_Aceptaritem.Name = "btn_Aceptaritem"
        Me.btn_Aceptaritem.Size = New System.Drawing.Size(87, 171)
        Me.btn_Aceptaritem.TextSize = New System.Drawing.Size(0, 0)
        Me.btn_Aceptaritem.TextVisible = False
        '
        'btn_Cancelaritem
        '
        Me.btn_Cancelaritem.Control = Me.btn_Cancelar
        Me.btn_Cancelaritem.Location = New System.Drawing.Point(87, 0)
        Me.btn_Cancelaritem.Name = "btn_Cancelaritem"
        Me.btn_Cancelaritem.Size = New System.Drawing.Size(497, 171)
        Me.btn_Cancelaritem.TextSize = New System.Drawing.Size(0, 0)
        Me.btn_Cancelaritem.TextVisible = False
        '
        'lstImpresorasitem
        '
        Me.lstImpresorasitem.Control = Me.lstImpresoras
        Me.lstImpresorasitem.Location = New System.Drawing.Point(194, 25)
        Me.lstImpresorasitem.Name = "lstImpresorasitem"
        Me.lstImpresorasitem.Size = New System.Drawing.Size(390, 25)
        Me.lstImpresorasitem.Text = "Impresora:"
        Me.lstImpresorasitem.TextLocation = DevExpress.Utils.Locations.Left
        Me.lstImpresorasitem.TextSize = New System.Drawing.Size(53, 13)
        '
        'dat_FechaFinitem
        '
        Me.dat_FechaFinitem.Control = Me.dat_FechaFin
        Me.dat_FechaFinitem.Location = New System.Drawing.Point(0, 24)
        Me.dat_FechaFinitem.Name = "dat_FechaFinitem"
        Me.dat_FechaFinitem.Size = New System.Drawing.Size(194, 26)
        Me.dat_FechaFinitem.Text = "FechaFin"
        Me.dat_FechaFinitem.TextLocation = DevExpress.Utils.Locations.Left
        Me.dat_FechaFinitem.TextSize = New System.Drawing.Size(53, 13)
        '
        'dat_FechaIniitem
        '
        Me.dat_FechaIniitem.Control = Me.dat_FechaIni
        Me.dat_FechaIniitem.Location = New System.Drawing.Point(67, 0)
        Me.dat_FechaIniitem.Name = "dat_FechaIniitem"
        Me.dat_FechaIniitem.Size = New System.Drawing.Size(127, 24)
        Me.dat_FechaIniitem.Text = "Banco"
        Me.dat_FechaIniitem.TextLocation = DevExpress.Utils.Locations.Right
        Me.dat_FechaIniitem.TextSize = New System.Drawing.Size(53, 13)
        '
        'Button2item
        '
        Me.Button2item.Control = Me.Button2
        Me.Button2item.Location = New System.Drawing.Point(478, 0)
        Me.Button2item.Name = "Button2item"
        Me.Button2item.Size = New System.Drawing.Size(60, 25)
        Me.Button2item.TextSize = New System.Drawing.Size(0, 0)
        Me.Button2item.TextVisible = False
        '
        'cbo_Bancoitem
        '
        Me.cbo_Bancoitem.Control = Me.cbo_Banco
        Me.cbo_Bancoitem.Location = New System.Drawing.Point(194, 0)
        Me.cbo_Bancoitem.Name = "cbo_Bancoitem"
        Me.cbo_Bancoitem.Size = New System.Drawing.Size(248, 25)
        Me.cbo_Bancoitem.TextSize = New System.Drawing.Size(0, 0)
        Me.cbo_Bancoitem.TextVisible = False
        '
        'Button1item
        '
        Me.Button1item.Control = Me.Button1
        Me.Button1item.Location = New System.Drawing.Point(538, 0)
        Me.Button1item.Name = "Button1item"
        Me.Button1item.Size = New System.Drawing.Size(46, 25)
        Me.Button1item.TextSize = New System.Drawing.Size(0, 0)
        Me.Button1item.TextVisible = False
        '
        'btn_Refrescaritem
        '
        Me.btn_Refrescaritem.Control = Me.btn_Refrescar
        Me.btn_Refrescaritem.Location = New System.Drawing.Point(442, 0)
        Me.btn_Refrescaritem.Name = "btn_Refrescaritem"
        Me.btn_Refrescaritem.Size = New System.Drawing.Size(36, 25)
        Me.btn_Refrescaritem.TextSize = New System.Drawing.Size(0, 0)
        Me.btn_Refrescaritem.TextVisible = False
        '
        'lbl_Cuentaitem
        '
        Me.lbl_Cuentaitem.Control = Me.lbl_Cuenta
        Me.lbl_Cuentaitem.Location = New System.Drawing.Point(0, 0)
        Me.lbl_Cuentaitem.Name = "lbl_Cuentaitem"
        Me.lbl_Cuentaitem.Size = New System.Drawing.Size(67, 24)
        Me.lbl_Cuentaitem.TextSize = New System.Drawing.Size(0, 0)
        Me.lbl_Cuentaitem.TextVisible = False
        '
        'GridC_1item
        '
        Me.GridC_1item.Control = Me.GridC_1
        Me.GridC_1item.Location = New System.Drawing.Point(0, 0)
        Me.GridC_1item.Name = "GridC_1item"
        Me.GridC_1item.Size = New System.Drawing.Size(584, 362)
        Me.GridC_1item.TextSize = New System.Drawing.Size(0, 0)
        Me.GridC_1item.TextVisible = False
        '
        'FluentDesignFormContainer1
        '
        Me.FluentDesignFormContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FluentDesignFormContainer1.Location = New System.Drawing.Point(260, 0)
        Me.FluentDesignFormContainer1.Name = "FluentDesignFormContainer1"
        Me.FluentDesignFormContainer1.Size = New System.Drawing.Size(628, 738)
        Me.FluentDesignFormContainer1.TabIndex = 9
        '
        'AccordionControl1
        '
        Me.AccordionControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.AccordionControl1.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {Me.AccordionControlElement1})
        Me.AccordionControl1.Location = New System.Drawing.Point(0, 0)
        Me.AccordionControl1.Name = "AccordionControl1"
        Me.AccordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Touch
        Me.AccordionControl1.Size = New System.Drawing.Size(260, 738)
        Me.AccordionControl1.TabIndex = 10
        Me.AccordionControl1.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu
        '
        'AccordionControlElement1
        '
        Me.AccordionControlElement1.Name = "AccordionControlElement1"
        Me.AccordionControlElement1.Text = "Element1"
        '
        'FluentDesignFormControl1
        '
        Me.FluentDesignFormControl1.Location = New System.Drawing.Point(0, 0)
        Me.FluentDesignFormControl1.Name = "FluentDesignFormControl1"
        Me.FluentDesignFormControl1.Size = New System.Drawing.Size(888, 0)
        Me.FluentDesignFormControl1.TabIndex = 11
        Me.FluentDesignFormControl1.TabStop = False
        '
        'pdvContraVales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(888, 738)
        Me.Controls.Add(Me.RibbonStatusBar1)
        Me.Controls.Add(Me.pdvContraValesLayoutControl1ConvertedLayout)
        Me.Controls.Add(Me.FluentDesignFormContainer1)
        Me.Controls.Add(Me.AccordionControl1)
        Me.Controls.Add(Me.FluentDesignFormControl1)
        Me.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Glow
        Me.Name = "pdvContraVales"
        Me.Text = "pdvContraVales"
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToolbarFormManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pdvContraValesLayoutControl1ConvertedLayout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pdvContraValesLayoutControl1ConvertedLayout.ResumeLayout(False)
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3item, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1item, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2item, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_Aceptaritem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_Cancelaritem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lstImpresorasitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dat_FechaFinitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dat_FechaIniitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Button2item, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbo_Bancoitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Button1item, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_Refrescaritem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbl_Cuentaitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridC_1item, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccordionControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FluentDesignFormControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents gridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents col1_Banco As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Hora As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numtarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tittarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Vigtarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Referencia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numcontrol As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Codigoaut As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Importe As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fum As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdVoucher As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Urlbanorte As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Puerto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_User1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Pswd As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Idmerchant As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Modo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Caja As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdEmpresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Empresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Rfc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Direccion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Direccion2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Transaccion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdSucursal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Respuesta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Afiliacion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Terminalid As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tiptarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Bancoemisor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Mesesdiferido As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numpagos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipoplan As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Autonumsuc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents Button2 As Button
    Friend WithEvents cbo_Banco As ComboBox
    Friend WithEvents lstImpresoras As ComboBox
    Friend WithEvents Button1 As Button
    Friend WithEvents dat_FechaFin As DateTimePicker
    Friend WithEvents dat_FechaIni As DateTimePicker
    Friend WithEvents btn_Refrescar As Button
    Friend WithEvents lbl_Cuenta As Label
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents RibbonStatusBar1 As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents ToolbarFormManager1 As DevExpress.XtraBars.ToolbarForm.ToolbarFormManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents pdvContraValesLayoutControl1ConvertedLayout As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GroupControl3item As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents btn_Aceptaritem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btn_Cancelaritem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GroupControl1item As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents lstImpresorasitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents dat_FechaFinitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents dat_FechaIniitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Button2item As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbo_Bancoitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Button1item As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btn_Refrescaritem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lbl_Cuentaitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GroupControl2item As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridC_1item As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FluentDesignFormContainer1 As DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer
    Friend WithEvents AccordionControl1 As DevExpress.XtraBars.Navigation.AccordionControl
    Friend WithEvents AccordionControlElement1 As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents FluentDesignFormControl1 As DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl
End Class
