﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvCotizador
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridFormatRule1 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue1 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule2 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue2 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Me.col1_Habilitado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.brn_Aceptar = New System.Windows.Forms.Button()
        Me.brn_Cancelar = New System.Windows.Forms.Button()
        Me.lblmensaje = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1_Articulo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Plazo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Precio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Quincenas = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Abono = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Precio_Final = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'col1_Habilitado
        '
        Me.col1_Habilitado.Caption = "col1_Habilitado"
        Me.col1_Habilitado.FieldName = "habilitado"
        Me.col1_Habilitado.MinWidth = 27
        Me.col1_Habilitado.Name = "col1_Habilitado"
        Me.col1_Habilitado.Width = 100
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.brn_Aceptar)
        Me.GroupControl2.Controls.Add(Me.brn_Cancelar)
        Me.GroupControl2.Controls.Add(Me.lblmensaje)
        Me.GroupControl2.Controls.Add(Me.Panel1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(978, 1615)
        Me.GroupControl2.TabIndex = 0
        '
        'brn_Aceptar
        '
        Me.brn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.brn_Aceptar.Location = New System.Drawing.Point(852, 337)
        Me.brn_Aceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.brn_Aceptar.Name = "brn_Aceptar"
        Me.brn_Aceptar.Size = New System.Drawing.Size(56, 52)
        Me.brn_Aceptar.TabIndex = 3
        Me.brn_Aceptar.UseVisualStyleBackColor = True
        '
        'brn_Cancelar
        '
        Me.brn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.brn_Cancelar.Location = New System.Drawing.Point(916, 337)
        Me.brn_Cancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.brn_Cancelar.Name = "brn_Cancelar"
        Me.brn_Cancelar.Size = New System.Drawing.Size(56, 52)
        Me.brn_Cancelar.TabIndex = 4
        Me.brn_Cancelar.UseVisualStyleBackColor = True
        '
        'lblmensaje
        '
        Me.lblmensaje.AutoSize = True
        Me.lblmensaje.ForeColor = System.Drawing.Color.Red
        Me.lblmensaje.Location = New System.Drawing.Point(11, 320)
        Me.lblmensaje.Name = "lblmensaje"
        Me.lblmensaje.Size = New System.Drawing.Size(708, 16)
        Me.lblmensaje.TabIndex = 2
        Me.lblmensaje.Text = "*ESTE PLAZO SELECCIONADO NO ESTA HABILITADO PARA LA DISTRIBUIDORA, FAVOR DE VALID" &
    "AR CON TU SUPERVISOR"
        Me.lblmensaje.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GridC_1)
        Me.Panel1.Location = New System.Drawing.Point(11, 51)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(960, 265)
        Me.Panel1.TabIndex = 1
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        GridLevelNode1.RelationName = "Level1"
        Me.GridC_1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridC_1.Location = New System.Drawing.Point(0, 0)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(960, 265)
        Me.GridC_1.TabIndex = 1
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 16.0!)
        Me.GridV_1.Appearance.Row.Options.UseFont = True
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col1_Habilitado, Me.col1_Articulo, Me.col1_Plazo, Me.col1_Precio, Me.col1_Quincenas, Me.col1_Abono, Me.col1_Precio_Final})
        Me.GridV_1.DetailHeight = 560
        Me.GridV_1.FixedLineWidth = 5
        GridFormatRule1.ApplyToRow = True
        GridFormatRule1.Column = Me.col1_Habilitado
        GridFormatRule1.Name = "col1_Habilitado"
        FormatConditionRuleValue1.Appearance.Font = New System.Drawing.Font("Tahoma", 16.0!, System.Drawing.FontStyle.Bold)
        FormatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Navy
        FormatConditionRuleValue1.Appearance.Options.UseFont = True
        FormatConditionRuleValue1.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Expression
        FormatConditionRuleValue1.Expression = "[habilitado] = 1 Or [habilitado] = True"
        GridFormatRule1.Rule = FormatConditionRuleValue1
        GridFormatRule2.ApplyToRow = True
        GridFormatRule2.Column = Me.col1_Habilitado
        GridFormatRule2.Name = "col1_Habilitado_Falso"
        FormatConditionRuleValue2.Appearance.BackColor = System.Drawing.Color.Silver
        FormatConditionRuleValue2.Appearance.Font = New System.Drawing.Font("Tahoma", 16.0!, System.Drawing.FontStyle.Bold)
        FormatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue2.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue2.Appearance.Options.UseFont = True
        FormatConditionRuleValue2.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Expression
        FormatConditionRuleValue2.Expression = "[habilitado] = 0 Or [habilitado] = False"
        GridFormatRule2.Rule = FormatConditionRuleValue2
        Me.GridV_1.FormatRules.Add(GridFormatRule1)
        Me.GridV_1.FormatRules.Add(GridFormatRule2)
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.GridV_1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        Me.GridV_1.PreviewIndent = 0
        Me.GridV_1.RowHeight = 0
        '
        'col1_Articulo
        '
        Me.col1_Articulo.Caption = "Clave"
        Me.col1_Articulo.FieldName = "articulo"
        Me.col1_Articulo.MinWidth = 27
        Me.col1_Articulo.Name = "col1_Articulo"
        Me.col1_Articulo.OptionsColumn.AllowEdit = False
        Me.col1_Articulo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Articulo.Visible = True
        Me.col1_Articulo.VisibleIndex = 0
        Me.col1_Articulo.Width = 100
        '
        'col1_Plazo
        '
        Me.col1_Plazo.Caption = "Plazo"
        Me.col1_Plazo.FieldName = "plazo"
        Me.col1_Plazo.MinWidth = 9
        Me.col1_Plazo.Name = "col1_Plazo"
        Me.col1_Plazo.OptionsColumn.AllowEdit = False
        Me.col1_Plazo.Visible = True
        Me.col1_Plazo.VisibleIndex = 1
        Me.col1_Plazo.Width = 100
        '
        'col1_Precio
        '
        Me.col1_Precio.Caption = "Precio"
        Me.col1_Precio.DisplayFormat.FormatString = "C0"
        Me.col1_Precio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Precio.FieldName = "precio"
        Me.col1_Precio.MinWidth = 27
        Me.col1_Precio.Name = "col1_Precio"
        Me.col1_Precio.OptionsColumn.AllowEdit = False
        Me.col1_Precio.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Precio.Width = 100
        '
        'col1_Quincenas
        '
        Me.col1_Quincenas.Caption = "Frecuencia Pago"
        Me.col1_Quincenas.DisplayFormat.FormatString = "N0"
        Me.col1_Quincenas.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Quincenas.FieldName = "frecuencia_pago"
        Me.col1_Quincenas.MinWidth = 27
        Me.col1_Quincenas.Name = "col1_Quincenas"
        Me.col1_Quincenas.OptionsColumn.AllowEdit = False
        Me.col1_Quincenas.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Quincenas.Visible = True
        Me.col1_Quincenas.VisibleIndex = 3
        Me.col1_Quincenas.Width = 100
        '
        'col1_Abono
        '
        Me.col1_Abono.Caption = "Abono"
        Me.col1_Abono.DisplayFormat.FormatString = "C0"
        Me.col1_Abono.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Abono.FieldName = "abono"
        Me.col1_Abono.MinWidth = 27
        Me.col1_Abono.Name = "col1_Abono"
        Me.col1_Abono.OptionsColumn.AllowEdit = False
        Me.col1_Abono.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Abono.Visible = True
        Me.col1_Abono.VisibleIndex = 2
        '
        'col1_Precio_Final
        '
        Me.col1_Precio_Final.Caption = "Precio Final"
        Me.col1_Precio_Final.DisplayFormat.FormatString = "C0"
        Me.col1_Precio_Final.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Precio_Final.FieldName = "precio_final"
        Me.col1_Precio_Final.MinWidth = 27
        Me.col1_Precio_Final.Name = "col1_Precio_Final"
        Me.col1_Precio_Final.OptionsColumn.AllowEdit = False
        Me.col1_Precio_Final.Visible = True
        Me.col1_Precio_Final.VisibleIndex = 4
        Me.col1_Precio_Final.Width = 100
        '
        'pdvCotizador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(978, 400)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "pdvCotizador"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "COTIZACIONES"
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblmensaje As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1_Habilitado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Articulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Plazo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Precio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Quincenas As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Abono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Precio_Final As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents brn_Aceptar As Button
    Friend WithEvents brn_Cancelar As Button
End Class
