﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvBuscaTransacciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1_Autonumsuc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Fecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Hora = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Referencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Cuenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Vendedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_VendedorNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Estatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.chk_SoloCaptura = New System.Windows.Forms.CheckBox()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.btn_Refrescar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dat_FechaFin = New System.Windows.Forms.DateTimePicker()
        Me.lbl_FechaIni = New System.Windows.Forms.Label()
        Me.dat_FechaIni = New System.Windows.Forms.DateTimePicker()
        Me.col1_Total = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(242, 26)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(481, 26)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 485)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(784, 76)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "¿Deseas Imprimir Ticket de Venta?"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(784, 389)
        Me.GroupControl2.TabIndex = 0
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(780, 364)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col1_Autonumsuc, Me.col1_Fecha, Me.col1_Hora, Me.col1_Total, Me.col1_Referencia, Me.col1_Cuenta, Me.col1_Vendedor, Me.col1_VendedorNombre, Me.col1_Estatus})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col1_Autonumsuc
        '
        Me.col1_Autonumsuc.Caption = "Autonumsuc"
        Me.col1_Autonumsuc.FieldName = "autonumsuc"
        Me.col1_Autonumsuc.Name = "col1_Autonumsuc"
        Me.col1_Autonumsuc.OptionsColumn.AllowEdit = False
        Me.col1_Autonumsuc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Autonumsuc.Visible = True
        Me.col1_Autonumsuc.VisibleIndex = 0
        '
        'col1_Fecha
        '
        Me.col1_Fecha.Caption = "Fecha"
        Me.col1_Fecha.FieldName = "fecha"
        Me.col1_Fecha.Name = "col1_Fecha"
        Me.col1_Fecha.OptionsColumn.AllowEdit = False
        Me.col1_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fecha.Visible = True
        Me.col1_Fecha.VisibleIndex = 1
        '
        'col1_Hora
        '
        Me.col1_Hora.Caption = "Hora"
        Me.col1_Hora.FieldName = "hora"
        Me.col1_Hora.Name = "col1_Hora"
        Me.col1_Hora.OptionsColumn.AllowEdit = False
        Me.col1_Hora.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Hora.Visible = True
        Me.col1_Hora.VisibleIndex = 2
        '
        'col1_Referencia
        '
        Me.col1_Referencia.Caption = "Referencia"
        Me.col1_Referencia.FieldName = "referencia"
        Me.col1_Referencia.Name = "col1_Referencia"
        Me.col1_Referencia.OptionsColumn.AllowEdit = False
        Me.col1_Referencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Referencia.Visible = True
        Me.col1_Referencia.VisibleIndex = 4
        '
        'col1_Cuenta
        '
        Me.col1_Cuenta.Caption = "Cuenta"
        Me.col1_Cuenta.FieldName = "cuenta"
        Me.col1_Cuenta.Name = "col1_Cuenta"
        Me.col1_Cuenta.OptionsColumn.AllowEdit = False
        Me.col1_Cuenta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Cuenta.Visible = True
        Me.col1_Cuenta.VisibleIndex = 5
        '
        'col1_Vendedor
        '
        Me.col1_Vendedor.Caption = "Vendedor"
        Me.col1_Vendedor.FieldName = "vendedor"
        Me.col1_Vendedor.Name = "col1_Vendedor"
        Me.col1_Vendedor.OptionsColumn.AllowEdit = False
        Me.col1_Vendedor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Vendedor.Visible = True
        Me.col1_Vendedor.VisibleIndex = 6
        '
        'col1_VendedorNombre
        '
        Me.col1_VendedorNombre.Caption = "Nombre"
        Me.col1_VendedorNombre.FieldName = "vendedor_nombre"
        Me.col1_VendedorNombre.Name = "col1_VendedorNombre"
        Me.col1_VendedorNombre.OptionsColumn.AllowEdit = False
        Me.col1_VendedorNombre.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_VendedorNombre.Visible = True
        Me.col1_VendedorNombre.VisibleIndex = 7
        '
        'col1_Estatus
        '
        Me.col1_Estatus.Caption = "Estatus"
        Me.col1_Estatus.FieldName = "estatus"
        Me.col1_Estatus.Name = "col1_Estatus"
        Me.col1_Estatus.OptionsColumn.AllowEdit = False
        Me.col1_Estatus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Estatus.Visible = True
        Me.col1_Estatus.VisibleIndex = 8
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.chk_SoloCaptura)
        Me.GroupControl1.Controls.Add(Me.RadioGroup1)
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.dat_FechaFin)
        Me.GroupControl1.Controls.Add(Me.lbl_FechaIni)
        Me.GroupControl1.Controls.Add(Me.dat_FechaIni)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 389)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(784, 96)
        Me.GroupControl1.TabIndex = 3
        '
        'chk_SoloCaptura
        '
        Me.chk_SoloCaptura.AutoSize = True
        Me.chk_SoloCaptura.Checked = True
        Me.chk_SoloCaptura.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chk_SoloCaptura.Location = New System.Drawing.Point(459, 73)
        Me.chk_SoloCaptura.Name = "chk_SoloCaptura"
        Me.chk_SoloCaptura.Size = New System.Drawing.Size(88, 17)
        Me.chk_SoloCaptura.TabIndex = 6
        Me.chk_SoloCaptura.Text = "Solo Captura"
        Me.chk_SoloCaptura.UseVisualStyleBackColor = True
        '
        'RadioGroup1
        '
        Me.RadioGroup1.EditValue = "Mostrador"
        Me.RadioGroup1.Location = New System.Drawing.Point(453, 26)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("Cotizacion", "COTIZACIONES"), New DevExpress.XtraEditors.Controls.RadioGroupItem("Mostrador", "VENTAS MOSTRADOR")})
        Me.RadioGroup1.Size = New System.Drawing.Size(260, 44)
        Me.RadioGroup1.TabIndex = 5
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.Location = New System.Drawing.Point(730, 28)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Refrescar.TabIndex = 4
        Me.btn_Refrescar.Text = "..."
        Me.btn_Refrescar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 66)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(105, 23)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Fecha Final"
        '
        'dat_FechaFin
        '
        Me.dat_FechaFin.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaFin.Location = New System.Drawing.Point(126, 60)
        Me.dat_FechaFin.Name = "dat_FechaFin"
        Me.dat_FechaFin.Size = New System.Drawing.Size(134, 30)
        Me.dat_FechaFin.TabIndex = 2
        '
        'lbl_FechaIni
        '
        Me.lbl_FechaIni.AutoSize = True
        Me.lbl_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_FechaIni.Location = New System.Drawing.Point(5, 32)
        Me.lbl_FechaIni.Name = "lbl_FechaIni"
        Me.lbl_FechaIni.Size = New System.Drawing.Size(115, 23)
        Me.lbl_FechaIni.TabIndex = 1
        Me.lbl_FechaIni.Text = "Fecha Inicial"
        '
        'dat_FechaIni
        '
        Me.dat_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaIni.Location = New System.Drawing.Point(126, 26)
        Me.dat_FechaIni.Name = "dat_FechaIni"
        Me.dat_FechaIni.Size = New System.Drawing.Size(134, 30)
        Me.dat_FechaIni.TabIndex = 0
        '
        'col1_Total
        '
        Me.col1_Total.Caption = "Total"
        Me.col1_Total.DisplayFormat.FormatString = "C2"
        Me.col1_Total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Total.FieldName = "total"
        Me.col1_Total.Name = "col1_Total"
        Me.col1_Total.OptionsColumn.AllowEdit = False
        Me.col1_Total.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Total.Visible = True
        Me.col1_Total.VisibleIndex = 3
        Me.col1_Total.Width = 56
        '
        'pdvBuscaTransacciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvBuscaTransacciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "COTIZACIONES / MOSTRADOR"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1_Autonumsuc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Hora As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Referencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Cuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label1 As Label
    Friend WithEvents dat_FechaFin As DateTimePicker
    Friend WithEvents lbl_FechaIni As Label
    Friend WithEvents dat_FechaIni As DateTimePicker
    Friend WithEvents btn_Refrescar As Button
    Friend WithEvents col1_Vendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents col1_VendedorNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chk_SoloCaptura As CheckBox
    Friend WithEvents col1_Estatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Total As DevExpress.XtraGrid.Columns.GridColumn
End Class
