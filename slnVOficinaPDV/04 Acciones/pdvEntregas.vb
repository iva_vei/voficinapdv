﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvEntregas
    Dim _Accion As Boolean
    Dim MonedaNacional As String = ""
    Dim _IdEmpresa As String
    Dim _IdSucursal As String
    Dim _Impresora As String
    Dim _TipoOperacion As String
    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public Property Impresora As String
        Get
            Return _Impresora
        End Get
        Set(value As String)
            _Impresora = value
        End Set
    End Property

    Public Property IdSucursal As String
        Get
            Return _IdSucursal
        End Get
        Set(value As String)
            _IdSucursal = value
        End Set
    End Property

    Public Property IdEmpresa As String
        Get
            Return _IdEmpresa
        End Get
        Set(value As String)
            _IdEmpresa = value
        End Set
    End Property

    Public Property TipoOperacion As String
        Get
            Return _TipoOperacion
        End Get
        Set(value As String)
            _TipoOperacion = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Msj As String = ""
        Dim DatosConexion() As String
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Try
            RG_Cajas.SelectedIndex = 0
            Cbo_Bovedas.Enabled = False

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_TCambios(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, dtDatos, MonedaNacional, Msj) Then
                cbo_Moneda.DataSource = dtDatos
                cbo_Moneda.Text = MonedaNacional
                cbo_Moneda.Refresh()
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If

            If oDatos.PVTA_ctabancos_BOVEDA_Sel(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, Globales.oAmbientes.oUsuario.Id_usuario, dtDatos, Msj) Then
                Cbo_Bovedas.DataSource = dtDatos
                Cbo_Bovedas.Refresh()
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If
            Call MuestraGrid()

            ''Call RG_TipoCortes_SelectedIndexChanged(Nothing, Nothing)
            txt_Total.Text = Format(0.0, "C2")

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub cbo_Moneda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbo_Moneda.SelectedValueChanged
        Try
            txt_TCambio.Text = Format(cbo_Moneda.SelectedValue, "C2")
            If cbo_Moneda.Text = MonedaNacional Then
                txt_TCambio.Enabled = False
            Else
                txt_TCambio.Enabled = True
            End If
        Catch ex As Exception
            txt_TCambio.Text = Format(0.0, "C2")
        End Try
    End Sub
    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub
    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub GridV_1_CellValueChanged(sender As Object, e As CellValueChangedEventArgs) Handles GridV_1.CellValueChanged
        Dim Valor1 As Double
        Try
            Valor1 = Globales.oAmbientes.Valor(GridV_1.GetRowCellValue(e.RowHandle, col1_Valor.FieldName))
            If e.Column.FieldName = col1_Cantidad.FieldName Then
                GridV_1.SetRowCellValue(e.RowHandle, col1_Importe.FieldName, e.Value * Valor1)
                txt_Total.Text = Format(ObtenerTotal(), "C2")
            End If
            If e.Column.FieldName = col1_Importe.FieldName Then
                txt_Total.Text = Format(ObtenerTotal(), "C2")
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Function ObtenerTotal() As Double
        Dim dTotal As Double = 0
        Dim dtDatos As DataTable
        Try
            GridV_1.CloseEditor()
            dtDatos = GridC_1.DataSource
            For Each oRen As DataRow In dtDatos.Rows
                dTotal += oRen.Item("importe")
            Next
        Catch ex As Exception
            dTotal = 0
        End Try
        GridV_1.ShowEditor()
        Return dTotal
    End Function
    Private Sub btn_Aceptar_Click_1(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim Msj As String = ""
        Dim Caja As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dvDatos As DataView
        Dim dtDetalle As DataTable = Nothing
        Dim dtDetalle1 As DataTable = Nothing
        Dim SW_continuar As Boolean = True
        Dim NoCorte As Integer
        Dim Movimiento As String = ""
        Dim ImporteDevolucion As Double = 0.0
        Dim AutoNumSuc As String = ""
        Dim Concepto As String = ""
        Try
            _Accion = False
            GridV_1.CloseEditor()
            dtDetalle = GridC_1.DataSource

            If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
                Caja = Cbo_Bovedas.SelectedValue
            Else
                Caja = txt_Caja.Text
            End If


            For Each dtRen As DataRow In dtDetalle.Rows
                If dtRen.Item("moneda").ToString.ToUpper = "DEVOLUCION" Then
                    ImporteDevolucion += dtRen.Item("importe")
                End If
            Next
            If ImporteDevolucion > 0 Then
                AutoNumSuc = InputBox("Ingresa el AutonumSuc que corresponde a la devolución.", "AutonumSuc Devolución")
                If ImporteDevolucion > RecuperaASDevolucion(AutoNumSuc) Then
                    SW_continuar = False
                    MessageBox.Show("El importe a entregar excede el importe del AutonumSuc(" & AutoNumSuc & ").", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

            dvDatos = dtDetalle.DefaultView
            dtDetalle1 = dvDatos.ToTable(False, New String() {"moneda", "importe", "referencia"})

            If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
                ''no hago nada
            Else
                If RG_TipoCortes.Properties.Items(RG_TipoCortes.SelectedIndex).Value = "CORTE Z" Then
                    If MessageBox.Show("La transacción actual cerrara la caja" & vbNewLine & "¿Desea continuar?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = DialogResult.No Then
                        SW_continuar = False
                    End If
                End If
            End If

            If SW_continuar Then
                If oDatos.PVTA_Inserta_entregas(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , txt_Caja.Text _
                                        , Caja _
                                        , txt_Cajero.Text _
                                        , "" _
                                        , txt_Corte.Text _
                                        , cbo_Moneda.Text _
                                        , txt_TCambio.Text _
                                        , Globales.oAmbientes.oUsuario.Id_usuario _
                                        , IIf(_TipoOperacion = "CORTE", RG_TipoCortes.Properties.Items(RG_TipoCortes.SelectedIndex).Value, "GASTO") _
                                        , dtDetalle1 _
                                        , "" _
                                        , NoCorte _
                                        , Movimiento _
                                        , Msj) Then
                    txt_Corte.Text = NoCorte
                    Call ImprimirCorte(Movimiento)
                    MessageBox.Show("Entrega Efectuada con Exito.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If RG_TipoCortes.Properties.Items(RG_TipoCortes.SelectedIndex).Value = "CORTE Z" _
                    And RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value <> "BOVEDA" Then
                        Application.Exit()
                    End If
                    _Accion = True
                    Me.Close()
                Else
                    If NoCorte < 0 Then
                        Concepto = InputBox(Msj, Me.Text)
                        If Concepto <> "" Then
                            If oDatos.PVTA_Inserta_entregas(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , txt_Caja.Text _
                                        , Caja _
                                        , txt_Cajero.Text _
                                        , "" _
                                        , txt_Corte.Text _
                                        , cbo_Moneda.Text _
                                        , txt_TCambio.Text _
                                        , Globales.oAmbientes.oUsuario.Id_usuario _
                                        , IIf(_TipoOperacion = "CORTE", RG_TipoCortes.Properties.Items(RG_TipoCortes.SelectedIndex).Value, "GASTO") _
                                        , dtDetalle1 _
                                        , Concepto _
                                        , NoCorte _
                                        , Movimiento _
                                        , Msj) Then
                                txt_Corte.Text = NoCorte
                                Call ImprimirCorte(Movimiento)
                                MessageBox.Show("Entrega Efectuada con Exito.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                If RG_TipoCortes.Properties.Items(RG_TipoCortes.SelectedIndex).Value = "CORTE Z" Then
                                    Application.Exit()
                                End If
                                _Accion = True
                                Me.Close()
                            Else
                                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        End If
                    Else
                        MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click_1(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
        Catch ex As Exception
            _Accion = False
        End Try
    End Sub
    Private Sub MuestraGrid()
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Try
            oDatos = New Datos_Viscoi
            If _TipoOperacion = "CORTE" Then
                col1_Referencia.Visible = False
                If oDatos.PVTA_Recupera_Monedas(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "ENUSO", "NO", "SI", dtDatos, MonedaNacional, Msj) Then
                    GridC_1.DataSource = dtDatos
                    GridV_1.ShowEditor()
                    If dtDatos.Rows.Count > 0 Then
                        If dtDatos.Rows(0).Item("activa_importe") = "SI" Then
                            col1_Importe.OptionsColumn.AllowEdit = True
                            col1_Cantidad.Visible = False
                            RG_TipoCortes.Enabled = True
                            RG_TipoCortes.SelectedIndex = 0
                        Else
                            col1_Importe.OptionsColumn.AllowEdit = False
                            col1_Cantidad.Visible = True
                            RG_TipoCortes.Enabled = False
                            RG_TipoCortes.SelectedIndex = 2
                        End If
                    End If
                Else
                    GridC_1.DataSource = Nothing
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                col1_Referencia.Visible = True
                If oDatos.PVTA_Recupera_Gastos(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "ACTIVO", dtDatos, MonedaNacional, Msj) Then
                    GridC_1.DataSource = dtDatos
                    GridV_1.ShowEditor()

                Else
                    GridC_1.DataSource = Nothing
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub ImprimirCorte(ByVal movimiento As String)
        Dim Reporte As Object
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim Caja As String = ""
        Dim oTicket As entCorte
        Dim sw_continuar As Boolean = False
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Try
            oDatos = New Datos_Viscoi

            If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
                Caja = Cbo_Bovedas.SelectedValue
            Else
                Caja = txt_Caja.Text
            End If

            If RG_TipoCortes.Properties.Items(RG_TipoCortes.SelectedIndex).Value = "CORTE P" Then
                If oDatos.PV_Recupera_EntregaParcialCaja(Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , Caja _
                                               , txt_Cajero.Text _
                                               , txt_Corte.Text _
                                               , Today.Date _
                                               , movimiento _
                                               , dtDatos _
                                               , Mensaje) Then
                    sw_continuar = True
                Else
                    sw_continuar = False
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                If oDatos.PV_Recupera_CorteCaja(Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , Caja _
                                               , "Todos" _
                                               , txt_Corte.Text _
                                               , Today.Date _
                                               , dtDatos _
                                               , Mensaje) Then
                    sw_continuar = True
                Else
                    sw_continuar = False
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
            If sw_continuar Then

                oTicket = New entCorte
                oTicket.Fill(dtDatos)

                oTicket.Empresa = _IdEmpresa
                oTicket.Id_sucursal = _IdSucursal
                oTicket.Cajero = txt_Cajero.Text
                oTicket.Caja = Caja
                oTicket.Corte = txt_Corte.Text

                Select Case Globales.oAmbientes.Id_Empresa
                    Case "WILLY", "PIFADIAN"
                        Reporte = New xtraRepCorte_Seafon
                        TryCast(Reporte, xtraRepCorte_Seafon).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepCorte_Seafon).CreateDocument()
                    Case "PAPELERA"
                        Reporte = New xtraRepCorte
                        TryCast(Reporte, xtraRepCorte).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepCorte).CreateDocument()
                    Case "CRECE"
                        Reporte = New xtraRepCorte_Carta
                        TryCast(Reporte, xtraRepCorte_Carta).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepCorte_Carta).CreateDocument()

                    Case Else
                        Reporte = New xtraRepCorte
                        TryCast(Reporte, xtraRepCorte).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepCorte).CreateDocument()
                End Select
                If Globales.oAmbientes.Id_Empresa = "CRECE" Then
                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    printTool.ShowPreviewDialog()
                Else
                    Dim printBase = New DevExpress.XtraPrinting.PrintToolBase(Reporte.PrintingSystem)
                    If RG_TipoCortes.Properties.Items(RG_TipoCortes.SelectedIndex).Value = "CORTE P" Then
                        printBase.PrinterSettings.Copies = 2
                    Else
                        printBase.PrinterSettings.Copies = 1
                    End If
                    printBase.Print(_Impresora)
                End If

            Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            oTicket = Nothing
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub GridC_1_KeyDown(sender As Object, e As KeyEventArgs) Handles GridC_1.KeyDown
        Call Globales.clsFormularios.Grid_Prevenir_Tab(sender, e, RG_TipoCortes)
    End Sub
    Private Function RecuperaASDevolucion(ByVal AutonumSuc As String) As Double
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Msj As String = ""
        Dim Importe As Double = 0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Transacciones(CDate("01/01/1900"), CDate("01/01/1900"), 0, AutonumSuc, Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "", "", "", dtDatos, dtDatos1, dtDatos2, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("tipotrans") = "DEVOLUCION" Then
                        Importe = Math.Abs(dtDatos.Rows(0).Item("importe"))
                    Else
                        Importe = 0.0
                    End If
                Else
                    MessageBox.Show("No se encontro AutonumSuc: " & AutonumSuc, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                Importe = 0.0
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            Importe = 0.0
        End Try
        Return Importe
    End Function

    Private Sub GridC_1_Click(sender As Object, e As EventArgs) Handles GridC_1.Click

    End Sub

    Private Sub pdvEntregas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim oDatos = New Datos_Viscoi
        Dim Mensaje As String = ""
        If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, txt_Cajero.Text, 0, Mensaje) Then
            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
                ''no hago nada
            Else
                Application.Exit()
            End If
        End If
    End Sub

    Private Sub RG_Cajas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RG_Cajas.SelectedIndexChanged
        Dim oDatos = New Datos_Viscoi
        Dim Mensaje As String = ""
        Dim caja As String = ""
        If RG_Cajas.Properties.Items(RG_Cajas.SelectedIndex).Value = "BOVEDA" Then
            Cbo_Bovedas.Enabled = True
            caja = Cbo_Bovedas.SelectedValue
        Else
            Cbo_Bovedas.Enabled = False
            caja = txt_Caja.Text
        End If
        If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, caja, txt_Cajero.Text, 0, Mensaje) Then
            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()

        End If
    End Sub
End Class