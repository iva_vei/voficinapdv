﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvFracciones
    Dim _Accion As Boolean
    Dim _Fraccion As Double

    Public Property Fraccion As Double
        Get
            Return _Fraccion
        End Get
        Set(value As Double)
            _Fraccion = value
        End Set
    End Property

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Private Sub pdvFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _Accion = False
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
            _Fraccion = 1
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Try
            _Accion = True
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub MostarArticulo()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Dim Precio As Double
        Dim Capacidad As Double

        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Articulos("ACTIVO", "", txt_Articulo.Text, "", "", "", "", "", False, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    Precio = dtDatos.Rows(0).Item("precio1")
                    Capacidad = dtDatos.Rows(0).Item("capacidad_operacion")
                    txt_Nombre.Text = dtDatos.Rows(0).Item("nombre")
                    txt_UniVta.Text = dtDatos.Rows(0).Item("unidadvta")
                    txt_Precio.Text = Format(Precio, "C2")
                    txt_Capacidad.Text = Capacidad
                    txt_Cantidad.Text = Capacidad
                    txt_Cantidad.Focus()
                Else
                    MessageBox.Show("Articulo no encontrado", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub txt_Cantidad_TextChanged(sender As Object, e As EventArgs) Handles txt_Cantidad.TextChanged

    End Sub
    Private Sub txt_Articulo_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_Articulo.KeyUp
        Try
            If e.KeyCode = Keys.Enter Then
                Call txt_Articulo_LostFocus(Nothing, Nothing)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub txt_Articulo_LostFocus(sender As Object, e As EventArgs) Handles txt_Articulo.LostFocus
        Call MostarArticulo()
    End Sub

    Private Sub pdvFracciones_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Call btn_Cancelar_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txt_Cantidad_LostFocus(sender As Object, e As EventArgs) Handles txt_Cantidad.LostFocus
        Dim Precio As Double
        Dim Capacidad As Double
        Dim Cantidad As Double
        Dim Total As Double
        Try
            Precio = Double.Parse(txt_Precio.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
            Capacidad = Double.Parse(txt_Capacidad.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
            Cantidad = Double.Parse(txt_Cantidad.Text, Globalization.NumberStyles.AllowDecimalPoint)
            If Capacidad <> 0 Then
                _Fraccion = Math.Truncate((Cantidad / Capacidad) * 1000) / 1000
            Else
                _Fraccion = 1.0
            End If
            Total = _Fraccion * Precio
            txt_Total.Text = Format(Total, "C2")
            _Accion = True
            btn_Aceptar.Focus()
        Catch ex As Exception
            _Accion = False
            txt_Total.Text = txt_Precio.Text
            txt_Cantidad.Text = "1"
            _Fraccion = 1
        End Try
    End Sub
End Class