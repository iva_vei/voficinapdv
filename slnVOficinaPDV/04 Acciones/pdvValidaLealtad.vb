﻿Public Class pdvValidaLealtad
    Private _Accion As Boolean
    Private _IdUserLealtad As String

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public Property IdUserLealtad As String
        Get
            Return _IdUserLealtad
        End Get
        Set(value As String)
            _IdUserLealtad = value
        End Set
    End Property

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            _Accion = False
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Try
            _Accion = False
            If txt_IdLealtad.Text.Length > 0 Then
                If _IdUserLealtad = txt_IdLealtad.Text Then
                    _Accion = True
                    Me.Close()
                Else
                    MessageBox.Show("El usuario del Programa Lealtad es diferente con que se creo la venta.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show("No se encontro usario del Programa de Lealtad con el codigo escaneado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub


    Private Sub txt_CodigoCanje_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_CodigoCanje.KeyDown
        If e.KeyCode = 13 Then
            If txt_CodigoCanje.Text <> "" Then
                Call Consultar_Lealtad()
            End If
        End If
    End Sub

    Private Sub txt_CodigoCanje_LostFocus(sender As Object, e As EventArgs) Handles txt_CodigoCanje.LostFocus
        If txt_CodigoCanje.Text <> "" Then
            Call Consultar_Lealtad()
        End If
    End Sub
    Private Sub Consultar_Lealtad()
        Dim oLealtad As dllVOficinaLealtad.Lealtad
        Dim id_empresa As String = ""
        Dim codigo As String = ""
        Dim r_id As Integer
        Dim r_usuario As String = ""
        Dim r_nombre As String = ""
        Dim r_apellido_pat As String = ""
        Dim r_apellido_mat As String = ""
        Dim r_fecha_nacimiento As Date
        Dim r_id_empresa As String = ""
        Dim r_correo As String = ""
        Dim r_telefono As String = ""
        Dim r_celular As String = ""
        Dim r_direccion As String = ""
        Dim r_colonia As String = ""
        Dim r_codigopostal As String = ""
        Dim r_fecha_registro As Date = CDate("01/01/1900")
        Dim r_fecha_ultima_compra As Date = CDate("01/01/1900")
        Dim r_fecha_ultimo_canje As Date = CDate("01/01/1900")
        Dim r_codigo_vigencia As Date
        Dim r_dinero_electronico As Double
        Dim r_dinero_electronico_vigencia As Date
        Dim r_nivel As String = ""
        Dim r_estatus As String = ""
        Dim r_tipo_cliente As String = ""
        Dim r_puntos As Integer
        Dim r_puntos_vigencia As Date = CDate("01/01/1900")
        Dim msj As String = ""
        Try
            oLealtad = New dllVOficinaLealtad.Lealtad


            id_empresa = Globales.oAmbientes.Id_Empresa
            codigo = txt_CodigoCanje.Text
            If oLealtad.ConsultaUsuarios(id_empresa _
                                    , codigo _
                                    , r_id _
                                    , r_usuario _
                                    , r_nombre _
                                    , r_apellido_pat _
                                    , r_apellido_mat _
                                    , r_fecha_nacimiento _
                                    , r_id_empresa _
                                    , r_correo _
                                    , r_telefono _
                                    , r_celular _
                                    , r_direccion _
                                    , r_colonia _
                                    , r_codigopostal _
                                    , r_fecha_registro _
                                    , r_fecha_ultima_compra _
                                    , r_fecha_ultimo_canje _
                                    , r_codigo_vigencia _
                                    , r_dinero_electronico _
                                    , r_dinero_electronico_vigencia _
                                    , r_nivel _
                                    , r_estatus _
                                    , r_puntos _
                                    , r_puntos_vigencia _
                                    , r_tipo_cliente _
                                    , msj) Then
                txt_IdLealtad.Text = r_id.ToString
                txt_NombreCanje.Text = r_nombre & " " & r_apellido_pat & " " & r_apellido_mat
                txt_NombreCanje1.Text = r_nombre
                txt_Puntos.Text = r_puntos.ToString
                txt_DE.Text = r_dinero_electronico.ToString
                txt_TipoCliente.Text = r_tipo_cliente
                btn_Aceptar.Focus()

            Else
                txt_IdLealtad.Text = ""
                txt_NombreCanje.Text = ""
                txt_NombreCanje1.Text = ""
                txt_Puntos.Text = "0"
                txt_DE.Text = "0"
                txt_TipoCliente.Text = ""
                txt_CodigoCanje.Focus()
                txt_CodigoCanje.SelectAll()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    btn_Cancelar_Click(Nothing, Nothing)
                Case Else

            End Select

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

End Class