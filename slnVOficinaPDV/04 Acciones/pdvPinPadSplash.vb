﻿
Public Class pdvPinPadSplash
    Enum PinpadAccion
        Inicializa
        GenerarLlaves
        Cobro
        Reversa
        Devolucion
        Cancelacion
        Verificacion
    End Enum

    Private HiloBanorteInicializa As System.Threading.Thread
    Private HiloBanorteGenerarLlaves As System.Threading.Thread
    Private HiloBanorteCobro As System.Threading.Thread
    Private HiloBanorteReversa As System.Threading.Thread
    Private HiloBanorteDevolucion As System.Threading.Thread
    Private HiloBanorteCancelacion As System.Threading.Thread
    Private HiloBanorteVerificacion As System.Threading.Thread
    Public PinAccion As PinpadAccion
    Public Accion As Boolean = False
    Public Mensaje As String
    Public Impresora As String
    Private sw_timer As Boolean = True


    Private _Total As String
    Private _CodigoAut As String
    Private _Referencia As String
    Private _MesesDiferido As String
    Private _NumPagos As String
    Private _TipoPlan As String
    Private _NoControl As String

    Private _IdVoucher As Integer

    Public Property Total As String
        Get
            Return _Total
        End Get
        Set(value As String)
            _Total = value
        End Set
    End Property

    Public ReadOnly Property CodigoAut As String
        Get
            Return _CodigoAut
        End Get
        'Set(value As String)
        '    _CodigoAut = value
        'End Set
    End Property

    Public Property Referencia As String
        Get
            Return _Referencia
        End Get
        Set(value As String)
            _Referencia = value
        End Set
    End Property

    Public Property MesesDiferido As String
        Get
            Return _MesesDiferido
        End Get
        Set(value As String)
            _MesesDiferido = value
        End Set
    End Property

    Public Property NumPagos As String
        Get
            Return _NumPagos
        End Get
        Set(value As String)
            _NumPagos = value
        End Set
    End Property

    Public Property TipoPlan As String
        Get
            Return _TipoPlan
        End Get
        Set(value As String)
            _TipoPlan = value
        End Set
    End Property

    Public Property NoControl As String
        Get
            Return _NoControl
        End Get
        Set(value As String)
            _NoControl = value
        End Set
    End Property

    Public Property IdVoucher As Integer
        Get
            Return _IdVoucher
        End Get
        Set(value As Integer)
            _IdVoucher = value
        End Set
    End Property

    Private Sub pdvPinPadSplash_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sw_continuar As Boolean = True
        btn_Aceptar.Enabled = False
        Btn_Cancelar.Enabled = False
        MarqueeProgressBarControl1.Visible = True
        lbl_Accion.Text = PinAccion.ToString()
        Timer1.Enabled = True
        Timer1.Start()

        Select Case Globales.oAmbientes.PinPadBanco
            Case "BANORTE"
                HiloBanorteInicializa = New System.Threading.Thread(AddressOf BanorteInicializa)
                HiloBanorteGenerarLlaves = New System.Threading.Thread(AddressOf BanorteGenerarLlaves)
                HiloBanorteCobro = New System.Threading.Thread(AddressOf BanorteCobro)
                HiloBanorteReversa = New System.Threading.Thread(AddressOf BanorteReversa)
                HiloBanorteDevolucion = New System.Threading.Thread(AddressOf BanorteDevolucion)
                HiloBanorteCancelacion = New System.Threading.Thread(AddressOf BanorteCancelacion)
                HiloBanorteVerificacion = New System.Threading.Thread(AddressOf BanorteVerificacion)
            Case Else
                sw_continuar = False
        End Select
        If sw_continuar Then
            Select Case PinAccion
                Case PinpadAccion.Inicializa : HiloBanorteInicializa.Start()
                Case PinpadAccion.GenerarLlaves : HiloBanorteGenerarLlaves.Start()
                Case PinpadAccion.Cobro : HiloBanorteCobro.Start()
                Case PinpadAccion.Reversa : HiloBanorteReversa.Start()
                Case PinpadAccion.Devolucion : HiloBanorteDevolucion.Start()
                Case PinpadAccion.Cancelacion : HiloBanorteCancelacion.Start()
                Case PinpadAccion.Verificacion : HiloBanorteVerificacion.Start()
            End Select
        End If
    End Sub
    Private Sub BanorteInicializa()
        Dim msj As String = ""
        Try
            sw_timer = True
            Accion = False
            If Not Globales.oAmbientes.oPinPadBanorte Is Nothing Then
                If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                    Accion = False
                    Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                End If
                If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                        Accion = True
                        Mensaje = "Exito"
                    Else
                        Accion = False
                        Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                    End If
                Else
                    Accion = False
                    Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                End If
            End If
        Catch ex As Exception
            Accion = False
            Mensaje = "Faltan parametros de configuracion para la PINPAD." & vbNewLine & ex.Message
        End Try
        If Not Accion Then
            Globales.oAmbientes.PinPadBanco = "N/A"
        End If
        sw_timer = False
    End Sub
    Private Sub BanorteGenerarLlaves()
        Dim msj As String = ""
        Try
            sw_timer = True
            Accion = True
            If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                Accion = False
                Mensaje = "El pinpad NO PUEDE CrearLlaves." & vbNewLine & msj
            End If
        Catch ex As Exception
            Accion = False
            Mensaje = "GenerarLlaves." & vbNewLine & ex.Message
        End Try
        sw_timer = False
    End Sub
    Private Sub BanorteCobro()
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim msj As String = ""

        Dim oDatos1 As Datos_Viscoi
        Dim msj1 As String = ""
        Try
            sw_timer = True
            Accion = False
            Mensaje = "Procesando cobro" & vbNewLine & "Siga las indicaciones en el PinPad"
            If Not Globales.oAmbientes.oPinPadBanorte Is Nothing Then
                If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                    Accion = False
                    Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                End If
                If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                    '    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                    Accion = False
                    Mensaje = "Inicializacion completa se procede al COBRO"
                    _NoControl = txt_NoControl.Text

                    Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                    Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                    Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                    Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                    Globales.oAmbientes.oPinPadBanorte.Tipo = "VENTA"
                    oDatos1 = New Datos_Viscoi

                    _IdVoucher = 0
                    If oDatos1.PVTA_Voucher_Inserta(0 _
                                                        , Globales.oAmbientes.oPinPadBanorte.UrlBanorte _
                                                        , Globales.oAmbientes.oPinPadBanorte.Puerto _
                                                        , Globales.oAmbientes.oPinPadBanorte.User _
                                                        , Globales.oAmbientes.oPinPadBanorte.Pswd _
                                                        , Globales.oAmbientes.oPinPadBanorte.IdMerchant _
                                                        , Globales.oAmbientes.oPinPadBanorte.Modo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Caja _
                                                        , Globales.oAmbientes.Id_Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Rfc _
                                                        , Globales.oAmbientes.oPinPadBanorte.Direccion _
                                                        , "" _
                                                        , Globales.oAmbientes.oPinPadBanorte.Tipo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Transaccion _
                                                        , Globales.oAmbientes.Id_Sucursal _
                                                        , Globales.oAmbientes.oPinPadBanorte.Fecha _
                                                        , Globales.oAmbientes.oPinPadBanorte.Hora _
                                                        , Globales.oAmbientes.oPinPadBanorte.Banco _
                                                        , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                        , Globales.oAmbientes.oPinPadBanorte.Afiliacion _
                                                        , Globales.oAmbientes.oPinPadBanorte.TerminalId _
                                                        , _NoControl _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.VigTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TitTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                        , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                        , "" _
                                                        , _Total _
                                                        , _MesesDiferido _
                                                        , _NumPagos _
                                                        , _TipoPlan _
                                                        , IdVoucher _
                                                        , msj1) Then
                        _IdVoucher = IdVoucher
                    End If

                    Globales.oAmbientes.oPinPadBanorte.NumControl = _NoControl

                    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Cobrar(_NoControl _
                                                                        , _Total _
                                                                        , _CodigoAut _
                                                                        , _Referencia _
                                                                        , _MesesDiferido _
                                                                        , _NumPagos _
                                                                        , _TipoPlan _
                                                                        , msj) Then
                        Accion = True
                        Mensaje = "Exito"

                        If oDatos1.PVTA_Voucher_Inserta(_IdVoucher _
                                                        , Globales.oAmbientes.oPinPadBanorte.UrlBanorte _
                                                        , Globales.oAmbientes.oPinPadBanorte.Puerto _
                                                        , Globales.oAmbientes.oPinPadBanorte.User _
                                                        , Globales.oAmbientes.oPinPadBanorte.Pswd _
                                                        , Globales.oAmbientes.oPinPadBanorte.IdMerchant _
                                                        , Globales.oAmbientes.oPinPadBanorte.Modo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Caja _
                                                        , Globales.oAmbientes.Id_Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Rfc _
                                                        , Globales.oAmbientes.oPinPadBanorte.Direccion _
                                                        , Globales.oAmbientes.oPinPadBanorte.Entry_Mode _
                                                        , Globales.oAmbientes.oPinPadBanorte.Tipo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Transaccion _
                                                        , Globales.oAmbientes.Id_Sucursal _
                                                        , Globales.oAmbientes.oPinPadBanorte.Fecha _
                                                        , Globales.oAmbientes.oPinPadBanorte.Hora _
                                                        , Globales.oAmbientes.oPinPadBanorte.Banco _
                                                        , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                        , Globales.oAmbientes.oPinPadBanorte.Afiliacion _
                                                        , Globales.oAmbientes.oPinPadBanorte.TerminalId _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumControl _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.VigTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TitTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                        , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                        , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                        , Globales.oAmbientes.oPinPadBanorte.Importe _
                                                        , Globales.oAmbientes.oPinPadBanorte.MesesDiferido _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumPagos _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipoPlan _
                                                        , IdVoucher _
                                                        , msj1) Then
                            _IdVoucher = IdVoucher
                            Select Case Globales.oAmbientes.PinPadBanco
                                Case "BANORTE"
                                    Reporte = New xtraRepVoucherBanorte

                                    Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                                    Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                                    Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                                    Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                                    Globales.oAmbientes.oPinPadBanorte.Tipo = "VENTA"

                                    If Globales.oAmbientes.oPinPadBanorte.Entry_Mode = "CHIP" Then
                                        Globales.oAmbientes.oPinPadBanorte.Referencia = Globales.oAmbientes.oPinPadBanorte.Referencia & vbNewLine & "AUTORIZACION CON FIRMA ELECTRONICA"
                                    End If
                                    If Globales.oAmbientes.oPinPadBanorte.Entry_Mode = "CONTACTLESSCHIP" Then
                                        Globales.oAmbientes.oPinPadBanorte.Referencia = Globales.oAmbientes.oPinPadBanorte.Referencia & vbNewLine & "AUTORIZADO SIN FIRMA"
                                    End If

                                    TryCast(Reporte, xtraRepVoucherBanorte).MargenAbj = 0
                                    TryCast(Reporte, xtraRepVoucherBanorte).MargenIzq = 0
                                    TryCast(Reporte, xtraRepVoucherBanorte).MargenDer = 0
                                    TryCast(Reporte, xtraRepVoucherBanorte).MargenArr = 0
                                    TryCast(Reporte, xtraRepVoucherBanorte).TipoCliente = ""
                                    TryCast(Reporte, xtraRepVoucherBanorte).ods.DataSource = Globales.oAmbientes.oPinPadBanorte
                                    TryCast(Reporte, xtraRepVoucherBanorte).CreateDocument()
                                    printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepVoucherBanorte).PrintingSystem)

                                    printBase.Print(Impresora)
                                    'oVoucher = New entVoucher
                                    'oVoucher.Banco = Globales.oAmbientes.PinPadBanco
                                    ''oVoucher.Afiliacion = Globales.oAmbientes.oPinPadBanorte.
                                Case Else
                                    ''sw_continuar = False
                            End Select
                        Else
                            If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                                Accion = False
                                Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                            End If
                            If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                                '    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                                Accion = False
                                Mensaje = "Inicializacion completa se procede al VERIFICACION"

                                If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Verificar(_NoControl _
                                                                            , _Total _
                                                                            , _CodigoAut _
                                                                            , _Referencia _
                                                                            , _MesesDiferido _
                                                                            , _NumPagos _
                                                                            , _TipoPlan _
                                                                            , msj) Then
                                    Accion = True
                                    Mensaje = "Exito"
                                    oDatos1 = New Datos_Viscoi

                                    Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                                    Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                                    Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                                    Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                                    Globales.oAmbientes.oPinPadBanorte.Tipo = "VENTA"

                                    If oDatos1.PVTA_Voucher_Actualiza(_IdVoucher _
                                                                , Globales.oAmbientes.Id_Sucursal _
                                                                , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                                , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                                , Globales.oAmbientes.oPinPadBanorte.Importe _
                                                                , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                                , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                                , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                                , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                                , IdVoucher _
                                                                , msj1) Then
                                        _IdVoucher = IdVoucher
                                    Else
                                        Accion = False
                                        Mensaje = "Error al grabar en Base de Datos, " & vbNewLine & msj1
                                    End If
                                    oDatos1 = Nothing
                                Else
                                    Accion = False
                                    Globales.oAmbientes.oPinPadBanorte.Direccion = msj
                                    Mensaje = "[COBRO/VERIFICACION]." & vbNewLine & msj
                                    If Not oDatos1.PVTA_Voucher_Inserta(_IdVoucher _
                                                            , Globales.oAmbientes.oPinPadBanorte.UrlBanorte _
                                                            , Globales.oAmbientes.oPinPadBanorte.Puerto _
                                                            , Globales.oAmbientes.oPinPadBanorte.User _
                                                            , Globales.oAmbientes.oPinPadBanorte.Pswd _
                                                            , Globales.oAmbientes.oPinPadBanorte.IdMerchant _
                                                            , Globales.oAmbientes.oPinPadBanorte.Modo _
                                                            , Globales.oAmbientes.oPinPadBanorte.Caja _
                                                            , Globales.oAmbientes.Id_Empresa _
                                                            , Globales.oAmbientes.oPinPadBanorte.Empresa _
                                                            , Globales.oAmbientes.oPinPadBanorte.Rfc _
                                                            , Globales.oAmbientes.oPinPadBanorte.Direccion _
                                                            , Globales.oAmbientes.oPinPadBanorte.Direccion2 _
                                                            , Globales.oAmbientes.oPinPadBanorte.Tipo _
                                                            , Globales.oAmbientes.oPinPadBanorte.Transaccion _
                                                            , Globales.oAmbientes.Id_Sucursal _
                                                            , Globales.oAmbientes.oPinPadBanorte.Fecha _
                                                            , Globales.oAmbientes.oPinPadBanorte.Hora _
                                                            , Globales.oAmbientes.oPinPadBanorte.Banco _
                                                            , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                            , Globales.oAmbientes.oPinPadBanorte.Afiliacion _
                                                            , Globales.oAmbientes.oPinPadBanorte.TerminalId _
                                                            , Globales.oAmbientes.oPinPadBanorte.NumControl _
                                                            , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                            , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                            , Globales.oAmbientes.oPinPadBanorte.VigTarjeta _
                                                            , Globales.oAmbientes.oPinPadBanorte.TitTarjeta _
                                                            , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                            , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                            , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                            , _Total _
                                                            , Globales.oAmbientes.oPinPadBanorte.MesesDiferido _
                                                            , Globales.oAmbientes.oPinPadBanorte.NumPagos _
                                                            , Globales.oAmbientes.oPinPadBanorte.TipoPlan _
                                                            , IdVoucher _
                                                            , msj1) Then
                                        Accion = False
                                        Mensaje = "Error al grabar en Base de Datos, Cobro Rechazado." & vbNewLine & msj
                                    End If
                                End If
                            Else
                                Accion = False
                                Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                            End If
                        End If
                        oDatos1 = Nothing
                    Else

                        Accion = False
                        Mensaje = "Cobro." & vbNewLine & IIf(msj = "Exito", "Cancelado por Usuario", msj)

                        oDatos1 = New Datos_Viscoi

                        Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                        Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                        Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                        Globales.oAmbientes.oPinPadBanorte.Direccion2 = msj
                        Globales.oAmbientes.oPinPadBanorte.Tipo = "RECHAZADA"

                        If Not oDatos1.PVTA_Voucher_Inserta(_IdVoucher _
                                                        , Globales.oAmbientes.oPinPadBanorte.UrlBanorte _
                                                        , Globales.oAmbientes.oPinPadBanorte.Puerto _
                                                        , Globales.oAmbientes.oPinPadBanorte.User _
                                                        , Globales.oAmbientes.oPinPadBanorte.Pswd _
                                                        , Globales.oAmbientes.oPinPadBanorte.IdMerchant _
                                                        , Globales.oAmbientes.oPinPadBanorte.Modo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Caja _
                                                        , Globales.oAmbientes.Id_Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Rfc _
                                                        , Globales.oAmbientes.oPinPadBanorte.Direccion _
                                                        , Globales.oAmbientes.oPinPadBanorte.Direccion2 _
                                                        , Globales.oAmbientes.oPinPadBanorte.Tipo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Transaccion _
                                                        , Globales.oAmbientes.Id_Sucursal _
                                                        , Globales.oAmbientes.oPinPadBanorte.Fecha _
                                                        , Globales.oAmbientes.oPinPadBanorte.Hora _
                                                        , Globales.oAmbientes.oPinPadBanorte.Banco _
                                                        , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                        , Globales.oAmbientes.oPinPadBanorte.Afiliacion _
                                                        , Globales.oAmbientes.oPinPadBanorte.TerminalId _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumControl _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.VigTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TitTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                        , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                        , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                        , _Total _
                                                        , Globales.oAmbientes.oPinPadBanorte.MesesDiferido _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumPagos _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipoPlan _
                                                        , IdVoucher _
                                                        , msj1) Then
                            Accion = False
                            Mensaje = "Error al grabar en Base de Datos, Cobro Rechazado." & vbNewLine & msj
                        End If
                        If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                            Accion = False
                            Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                        End If
                        If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                            '    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                            Accion = False
                            Mensaje = "Inicializacion completa se procede al VERIFICACION"

                            If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Verificar(_NoControl _
                                                                        , _Total _
                                                                        , _CodigoAut _
                                                                        , _Referencia _
                                                                        , _MesesDiferido _
                                                                        , _NumPagos _
                                                                        , _TipoPlan _
                                                                        , msj) Then
                                Accion = True
                                Mensaje = "Exito"
                                oDatos1 = New Datos_Viscoi

                                Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                                Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                                Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                                Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                                Globales.oAmbientes.oPinPadBanorte.Tipo = "VENTA"

                                If oDatos1.PVTA_Voucher_Actualiza(_IdVoucher _
                                                            , Globales.oAmbientes.Id_Sucursal _
                                                            , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                            , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                            , Globales.oAmbientes.oPinPadBanorte.Importe _
                                                            , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                            , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                            , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                            , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                            , IdVoucher _
                                                            , msj1) Then
                                    _IdVoucher = IdVoucher

                                    Select Case Globales.oAmbientes.PinPadBanco
                                        Case "BANORTE"
                                            Reporte = New xtraRepVoucherBanorte

                                            Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                                            Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                                            Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                                            Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                                            Globales.oAmbientes.oPinPadBanorte.Tipo = "VENTA"

                                            If Globales.oAmbientes.oPinPadBanorte.Entry_Mode = "CHIP" Then
                                                Globales.oAmbientes.oPinPadBanorte.Referencia = Globales.oAmbientes.oPinPadBanorte.Referencia & vbNewLine & "AUTORIZACION CON FIRMA ELECTRONICA"
                                            End If
                                            If Globales.oAmbientes.oPinPadBanorte.Entry_Mode = "CONTACTLESSCHIP" Then
                                                Globales.oAmbientes.oPinPadBanorte.Referencia = Globales.oAmbientes.oPinPadBanorte.Referencia & vbNewLine & "AUTORIZADO SIN FIRMA"
                                            End If

                                            TryCast(Reporte, xtraRepVoucherBanorte).MargenAbj = 0
                                            TryCast(Reporte, xtraRepVoucherBanorte).MargenIzq = 0
                                            TryCast(Reporte, xtraRepVoucherBanorte).MargenDer = 0
                                            TryCast(Reporte, xtraRepVoucherBanorte).MargenArr = 0
                                            TryCast(Reporte, xtraRepVoucherBanorte).TipoCliente = ""
                                            TryCast(Reporte, xtraRepVoucherBanorte).ods.DataSource = Globales.oAmbientes.oPinPadBanorte
                                            TryCast(Reporte, xtraRepVoucherBanorte).CreateDocument()
                                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepVoucherBanorte).PrintingSystem)

                                            printBase.Print(Impresora)
                                            'oVoucher = New entVoucher
                                            'oVoucher.Banco = Globales.oAmbientes.PinPadBanco
                                            ''oVoucher.Afiliacion = Globales.oAmbientes.oPinPadBanorte.
                                        Case Else
                                            ''sw_continuar = False
                                    End Select
                                Else
                                    Accion = False
                                    Mensaje = "Error al grabar en Base de Datos, " & vbNewLine & msj1
                                End If
                                oDatos1 = Nothing
                            Else
                                Accion = False
                                Globales.oAmbientes.oPinPadBanorte.Direccion = msj
                                Mensaje = "[COBRO/VERIFICACION]." & vbNewLine & msj
                                If Not oDatos1.PVTA_Voucher_Inserta(_IdVoucher _
                                                        , Globales.oAmbientes.oPinPadBanorte.UrlBanorte _
                                                        , Globales.oAmbientes.oPinPadBanorte.Puerto _
                                                        , Globales.oAmbientes.oPinPadBanorte.User _
                                                        , Globales.oAmbientes.oPinPadBanorte.Pswd _
                                                        , Globales.oAmbientes.oPinPadBanorte.IdMerchant _
                                                        , Globales.oAmbientes.oPinPadBanorte.Modo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Caja _
                                                        , Globales.oAmbientes.Id_Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Empresa _
                                                        , Globales.oAmbientes.oPinPadBanorte.Rfc _
                                                        , Globales.oAmbientes.oPinPadBanorte.Direccion _
                                                        , Globales.oAmbientes.oPinPadBanorte.Direccion2 _
                                                        , Globales.oAmbientes.oPinPadBanorte.Tipo _
                                                        , Globales.oAmbientes.oPinPadBanorte.Transaccion _
                                                        , Globales.oAmbientes.Id_Sucursal _
                                                        , Globales.oAmbientes.oPinPadBanorte.Fecha _
                                                        , Globales.oAmbientes.oPinPadBanorte.Hora _
                                                        , Globales.oAmbientes.oPinPadBanorte.Banco _
                                                        , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                        , Globales.oAmbientes.oPinPadBanorte.Afiliacion _
                                                        , Globales.oAmbientes.oPinPadBanorte.TerminalId _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumControl _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.VigTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.TitTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                        , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                        , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                        , _Total _
                                                        , Globales.oAmbientes.oPinPadBanorte.MesesDiferido _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumPagos _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipoPlan _
                                                        , IdVoucher _
                                                        , msj1) Then
                                    Accion = False
                                    Mensaje = "Error al grabar en Base de Datos, Cobro Rechazado." & vbNewLine & msj
                                End If
                            End If
                        Else
                            Accion = False
                            Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                        End If
                    End If
                Else
                    Accion = False
                    Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                End If
                ''    Else
                ''        Accion = False
                ''        Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                ''    End If
                ''
            Else
                Accion = False
                Mensaje = "Inicializa." & vbNewLine & Mensaje
            End If
        Catch ex As Exception
            Accion = False
            Mensaje = "GenerarLlaves." & vbNewLine & ex.Message
        End Try
        sw_timer = False
    End Sub

    Private Sub BanorteReversa()
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim msj As String = ""
        Try
            sw_timer = True
            Accion = False
            Mensaje = "Procesando REVERSA" & vbNewLine & "Siga las indicaciones en el PinPad"
            If _NoControl <> "" And _Referencia <> "" Then
                If Not Globales.oAmbientes.oPinPadBanorte Is Nothing Then
                    If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                        Accion = False
                        Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                    End If
                    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                        If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                            Accion = True
                            Mensaje = "Inicializacion completa se procede al REVERSA"
                            If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Reversa(_NoControl _
                                                                               , _Total _
                                                                               , _CodigoAut _
                                                                               , _Referencia _
                                                                               , _MesesDiferido _
                                                                               , _NumPagos _
                                                                               , _TipoPlan _
                                                                               , msj) Then
                                Accion = True
                                Mensaje = "Exito"

                                Select Case Globales.oAmbientes.PinPadBanco
                                    Case "BANORTE"
                                        Reporte = New xtraRepVoucherBanorte

                                        Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                                        Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                                        Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                                        Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                                        Globales.oAmbientes.oPinPadBanorte.Tipo = "REVERSA"

                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenAbj = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenIzq = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenDer = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenArr = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).TipoCliente = ""
                                        TryCast(Reporte, xtraRepVoucherBanorte).ods.DataSource = Globales.oAmbientes.oPinPadBanorte
                                        TryCast(Reporte, xtraRepVoucherBanorte).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepVoucherBanorte).PrintingSystem)

                                        printBase.Print(Impresora)
                                        'oVoucher = New entVoucher
                                        'oVoucher.Banco = Globales.oAmbientes.PinPadBanco
                                        ''oVoucher.Afiliacion = Globales.oAmbientes.oPinPadBanorte.
                                    Case Else
                                        ''sw_continuar = False
                                End Select
                            Else
                                Accion = False
                                Mensaje = "REVERSA." & vbNewLine & msj
                            End If
                        Else
                            Accion = False
                            Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                        End If
                    Else
                        Accion = False
                        Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                    End If

                Else
                    Accion = False
                    Mensaje = "Inicializa." & vbNewLine & Mensaje
                End If
            Else
                Accion = False
                Mensaje = "No se indico No De control o Referencia"
            End If
        Catch ex As Exception
            Accion = False
            Mensaje = "GenerarLlaves." & vbNewLine & ex.Message
        End Try
        sw_timer = False
    End Sub

    Private Sub BanorteDevolucion()
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim msj As String = ""
        Try
            sw_timer = True
            Accion = False
            Mensaje = "Procesando Devolucion" & vbNewLine & "Siga las indicaciones en el PinPad"
            If _NoControl <> "" And _Referencia <> "" Then
                If Not Globales.oAmbientes.oPinPadBanorte Is Nothing Then
                    If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                        Accion = False
                        Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                    End If
                    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                        If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                            Accion = True
                            Mensaje = "Inicializacion completa se procede al Devolucion"
                            _NoControl = "TRAN" & Globales.oAmbientes.Id_Sucursal.Substring(0, 4) & Format(Now, "yyyyMMddhhmmss")
                            If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Devolucion(_NoControl _
                                                                               , _Total _
                                                                               , _CodigoAut _
                                                                               , _Referencia _
                                                                               , _MesesDiferido _
                                                                               , _NumPagos _
                                                                               , _TipoPlan _
                                                                               , msj) Then
                                Accion = True
                                Mensaje = "Exito"

                                Select Case Globales.oAmbientes.PinPadBanco
                                    Case "BANORTE"
                                        Reporte = New xtraRepVoucherBanorte

                                        Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                                        Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                                        Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                                        Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                                        Globales.oAmbientes.oPinPadBanorte.Tipo = "DEVOLUCION"

                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenAbj = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenIzq = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenDer = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenArr = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).TipoCliente = ""
                                        TryCast(Reporte, xtraRepVoucherBanorte).ods.DataSource = Globales.oAmbientes.oPinPadBanorte
                                        TryCast(Reporte, xtraRepVoucherBanorte).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepVoucherBanorte).PrintingSystem)

                                        printBase.Print(Impresora)
                                        'oVoucher = New entVoucher
                                        'oVoucher.Banco = Globales.oAmbientes.PinPadBanco
                                        ''oVoucher.Afiliacion = Globales.oAmbientes.oPinPadBanorte.
                                    Case Else
                                        ''sw_continuar = False
                                End Select
                            Else
                                Accion = False
                                Mensaje = "Devolucion." & vbNewLine & msj
                            End If
                        Else
                            Accion = False
                            Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                        End If
                    Else
                        Accion = False
                        Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                    End If

                Else
                    Accion = False
                    Mensaje = "Inicializa." & vbNewLine & Mensaje
                End If
            Else
                Accion = False
                Mensaje = "No se indico No De control o Referencia"
            End If
        Catch ex As Exception
            Accion = False
            Mensaje = "GenerarLlaves." & vbNewLine & ex.Message
        End Try
        sw_timer = False
    End Sub

    Private Sub BanorteCancelacion()
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim msj As String = ""
        Try
            sw_timer = True
            Accion = False
            Mensaje = "Procesando Cancelacion" & vbNewLine & "Siga las indicaciones en el PinPad"
            If _NoControl <> "" And _Referencia <> "" Then
                If Not Globales.oAmbientes.oPinPadBanorte Is Nothing Then
                    If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                        Accion = False
                        Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                    End If
                    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                        If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                            Accion = True
                            Mensaje = "Inicializacion completa se procede al Cancelacion"
                            _NoControl = "TRAN" & Globales.oAmbientes.Id_Sucursal.Substring(0, 4) & Format(Now, "yyyyMMddhhmmss")
                            If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Cancelacion(_NoControl _
                                                                               , _Total _
                                                                               , _CodigoAut _
                                                                               , _Referencia _
                                                                               , _MesesDiferido _
                                                                               , _NumPagos _
                                                                               , _TipoPlan _
                                                                               , msj) Then
                                Accion = True
                                Mensaje = "Exito"

                                Select Case Globales.oAmbientes.PinPadBanco
                                    Case "BANORTE"
                                        Reporte = New xtraRepVoucherBanorte

                                        Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                                        Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                                        Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                                        Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                                        Globales.oAmbientes.oPinPadBanorte.Tipo = "CANCELACION"

                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenAbj = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenIzq = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenDer = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).MargenArr = 0
                                        TryCast(Reporte, xtraRepVoucherBanorte).TipoCliente = ""
                                        TryCast(Reporte, xtraRepVoucherBanorte).ods.DataSource = Globales.oAmbientes.oPinPadBanorte
                                        TryCast(Reporte, xtraRepVoucherBanorte).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepVoucherBanorte).PrintingSystem)

                                        printBase.Print(Impresora)
                                        'oVoucher = New entVoucher
                                        'oVoucher.Banco = Globales.oAmbientes.PinPadBanco
                                        ''oVoucher.Afiliacion = Globales.oAmbientes.oPinPadBanorte.
                                    Case Else
                                        ''sw_continuar = False
                                End Select
                            Else
                                Accion = False
                                Mensaje = "Cancelacion." & vbNewLine & msj
                            End If
                        Else
                            Accion = False
                            Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                        End If
                    Else
                        Accion = False
                        Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                    End If

                Else
                    Accion = False
                    Mensaje = "Inicializa." & vbNewLine & Mensaje
                End If
            Else
                Accion = False
                Mensaje = "No se indico No De control o Referencia"
            End If
        Catch ex As Exception
            Accion = False
            Mensaje = "GenerarLlaves." & vbNewLine & ex.Message
        End Try
        sw_timer = False
    End Sub
    Private Sub BanorteVerificacion()
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim msj As String = ""

        Dim oDatos1 As Datos_Viscoi
        Dim msj1 As String = ""
        Try
            sw_timer = True
            Accion = False
            Mensaje = "Procesando VERIFICACION " & vbNewLine & "Siga las indicaciones en el PinPad"
            If _NoControl <> "" Then
                If Not Globales.oAmbientes.oPinPadBanorte Is Nothing Then
                    If Not Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Liberar(msj) Then
                        Accion = False
                        Mensaje = "El pinpad NO SE libero corectamente." & vbNewLine & msj
                    End If
                    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Inicializa(msj) Then
                        '    If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_CrearLlaves_Internet(msj) Then
                        Accion = False
                        Mensaje = "Inicializacion completa se procede al VERIFICACION"
                        ''_NoControl = "TRAN" & Globales.oAmbientes.Id_Sucursal.Substring(0, 4) & Format(Now, "yyyyMMddhhmmss")
                        If Globales.oAmbientes.oPinPadBanorte.fn_Pinpad_Banorte_Verificar(_NoControl _
                                                                        , _Total _
                                                                        , _CodigoAut _
                                                                        , _Referencia _
                                                                        , _MesesDiferido _
                                                                        , _NumPagos _
                                                                        , _TipoPlan _
                                                                        , msj) Then
                            Accion = True
                            Mensaje = "Exito"
                            oDatos1 = New Datos_Viscoi

                            Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                            Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                            Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                            Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                            Globales.oAmbientes.oPinPadBanorte.Tipo = "VENTA"

                            If oDatos1.PVTA_Voucher_Actualiza(_IdVoucher _
                                                        , Globales.oAmbientes.Id_Sucursal _
                                                        , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.Importe _
                                                        , Globales.oAmbientes.oPinPadBanorte.Respuesta _
                                                        , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                        , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                        , IdVoucher _
                                                        , msj1) Then
                                _IdVoucher = IdVoucher
                            Else
                                Accion = False
                                Mensaje = "Error al grabar en Base de Datos, " & vbNewLine & msj1
                            End If
                            oDatos1 = Nothing
                        Else
                            Accion = False

                            oDatos1 = New Datos_Viscoi

                            Globales.oAmbientes.oPinPadBanorte.Empresa = txt_Empresa.Text
                            Globales.oAmbientes.oPinPadBanorte.Rfc = txt_RFC.Text
                            Globales.oAmbientes.oPinPadBanorte.Direccion = txt_Direccion.Text
                            Globales.oAmbientes.oPinPadBanorte.Direccion2 = txt_Direccion2.Text
                            Globales.oAmbientes.oPinPadBanorte.Tipo = "VENTA"

                            If oDatos1.PVTA_Voucher_Actualiza(_IdVoucher _
                                                        , Globales.oAmbientes.Id_Sucursal _
                                                        , Globales.oAmbientes.oPinPadBanorte.Referencia _
                                                        , Globales.oAmbientes.oPinPadBanorte.NumTarjeta _
                                                        , Globales.oAmbientes.oPinPadBanorte.Importe _
                                                        , msj _
                                                        , Globales.oAmbientes.oPinPadBanorte.CodigoAut _
                                                        , Globales.oAmbientes.oPinPadBanorte.BancoEmisor _
                                                        , Globales.oAmbientes.oPinPadBanorte.TipTarjeta _
                                                        , IdVoucher _
                                                        , msj1) Then
                                _IdVoucher = IdVoucher
                            Else
                                Accion = False
                                Mensaje = "Error al grabar en Base de Datos, " & vbNewLine & msj1
                            End If
                            oDatos1 = Nothing

                            Mensaje = "Verificación." & vbNewLine & msj
                        End If
                    Else
                        Accion = False
                        Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                    End If
                    ''    Else
                    ''        Accion = False
                    ''        Mensaje = "El pinpad NO SE inicializo corectamente." & vbNewLine & msj
                    ''    End If
                    ''
                Else
                    Accion = False
                    Mensaje = "Inicializa." & vbNewLine & Mensaje
                End If
            Else
                Accion = False
                Mensaje = "No se envio NoControl"
            End If
        Catch ex As Exception
            Accion = False
            Mensaje = "GenerarLlaves." & vbNewLine & ex.Message
        End Try
        sw_timer = False
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim Hilo1 As System.Threading.Thread = Nothing
        Try
            Select Case PinAccion
                Case PinpadAccion.Inicializa : Hilo1 = HiloBanorteInicializa
                Case PinpadAccion.GenerarLlaves : Hilo1 = HiloBanorteGenerarLlaves
                Case PinpadAccion.Cobro : Hilo1 = HiloBanorteCobro
                Case PinpadAccion.Verificacion : Hilo1 = HiloBanorteVerificacion
            End Select
            If Mensaje Is Nothing Then
                Mensaje = ""
            End If
            txt_Mensaje.Text = IIf(Mensaje.Length > 0, Mensaje, "")
            If Not Hilo1 Is Nothing Then
                If Not sw_timer Then
                    btn_Aceptar.Enabled = True
                    Btn_Cancelar.Enabled = False
                    txt_Mensaje.Text = Mensaje
                    Timer1.Enabled = False
                    MarqueeProgressBarControl1.Visible = False
                    If Accion Then
                        Me.Close()
                    Else
                        txt_Mensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                        txt_Mensaje.BackColor = Color.Red
                        txt_Mensaje.ForeColor = Color.White
                    End If
                Else
                    If Globales.oAmbientes.oPinPadBanorte.NumControl.Length = 0 Then
                        Btn_Cancelar.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception
            Accion = False
        End Try
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Me.Close()
    End Sub
    Private Sub pdvPinPadSplash_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = sw_timer
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Btn_Cancelar.Click
        Me.Close()
    End Sub
End Class