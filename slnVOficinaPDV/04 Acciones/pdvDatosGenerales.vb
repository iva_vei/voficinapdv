﻿Public Class pdvDatosGenerales
    Public TipoTran As eTipoTran
    Public Autonumusc As String
    Public Estatus As String
    Public TecladoAtajos As New List(Of Principal.clsTecladoAtajos)
    Public FormaLlama As Form
    Public opcion As String
    Public MensajeNotificacion As TextBox
    Dim SW_focus As Boolean
    Dim _Corte As Integer
    Dim _Accion As Boolean
    Dim _IdEmpresa As String
    Dim _IdSucursal As String
    Dim _Caja As String
    Dim _Cajero As String
    Dim _Impresora As String

    Public Sub New(ByRef pTipoTran As eTipoTran)
        TipoTran = pTipoTran
        Estatus = ""
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Public Property Caja As String
        Get
            Return _Caja
        End Get
        Set(value As String)
            _Caja = value
        End Set
    End Property

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public Property IdEmpresa As String
        Get
            Return _IdEmpresa
        End Get
        Set(value As String)
            _IdEmpresa = value
        End Set
    End Property

    Public Property IdSucursal As String
        Get
            Return _IdSucursal
        End Get
        Set(value As String)
            _IdSucursal = value
        End Set
    End Property

    Public Property Cajero As String
        Get
            Return _Cajero
        End Get
        Set(value As String)
            _Cajero = value
        End Set
    End Property

    Public Property Impresora As String
        Get
            Return _Impresora
        End Get
        Set(value As String)
            _Impresora = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        SW_focus = False
        Try
            _Accion = False


            txt_Caja.Text = _Caja
            txt_Cajero.Text = _Cajero

            'If Globales.oAmbientes.Id_Empresa = "PAPELERA" Then
            '    Label2.Text = "Institución"
            'End If

            txt_Vendedor1.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VOficina\" & Globales.oAmbientes.Sistema, "id_vendedor1", "")
            txt_Vendedor2.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\VOficina\" & Globales.oAmbientes.Sistema, "id_vendedor2", "")
            txt_Vendedor1.SelectAll()

            txt_CodigoCanje.Text = ""
            txt_NombreCanje.Text = ""
            txt_DE.Text = "0"
            txt_Puntos.Text = "0"
            Select Case TipoTran
                Case eTipoTran.VENTA
                    GC_Mostrador.Visible = False
                    GC_ProgramaLealtad.Visible = True
                Case eTipoTran.MOSTRADOR
                    GC_Mostrador.Visible = True
                    GC_ProgramaLealtad.Visible = False
                Case eTipoTran.COTIZACION
                    GC_Mostrador.Visible = False
                    GC_ProgramaLealtad.Visible = False
            End Select

            Me.Height = IIf(GroupControl2.Visible, GroupControl2.Height, 0) + IIf(GC_Mostrador.Visible, GC_Mostrador.Height, 0) + IIf(GC_ProgramaLealtad.Visible, GC_ProgramaLealtad.Height, 0) + IIf(GC_Vale.Visible, GC_Vale.Height, 0) + 115
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Entregas(ByRef oforma As Object)
        Try
            Call btn_Entregas_Click(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_VTAMostrador(ByVal opc As String)
        Try
            Call btn_Buscar_Click(opc, Nothing)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_VTACotizacion(ByVal opc As String)
        Try
            Call btn_Buscar_Click(opc, Nothing)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Clientes(ByRef oforma As Object)
        Try
            Call btn_Cliente_Click(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Pagos(ByRef oforma As Object)
        Try
            Call Btn_Pagos_Click(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim sw_continuar As Boolean = True
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""

        Try
            _Accion = False
            oDatos = New Datos_Viscoi
            If (TipoTran = eTipoTran.VENTA) Then
                If oDatos.PVTA_Caja_Valida_Efectivo(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, dtDatos, Mensaje) Then
                    If txt_Vendedor1.Text = "" Then
                        sw_continuar = False
                        MessageBox.Show("Debe teclear un vendedor.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                    If sw_continuar Then
                        _Accion = True
                        Me.Close()
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                If txt_Vendedor1.Text = "" Then
                    sw_continuar = False
                    MessageBox.Show("Debe teclear un vendedor.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                If sw_continuar Then
                    _Accion = True
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub


    Private Sub txt_Vendedor1_LostFocus(sender As Object, e As EventArgs) Handles txt_Vendedor1.LostFocus
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        ''If SW_focus Then Return

        Try
            ''SW_focus = True
            txt_VendedorNombre1.Text = ""
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Vendedores(Globales.oAmbientes.Id_Empresa, "", txt_Vendedor1.Text, "ACTIVO", "", False, dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows.Count > 1 Then
                        txt_VendedorNombre1.Text = ""
                        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VOficina\" & Globales.oAmbientes.Sistema, "id_vendedor1", txt_Vendedor1.Text)
                    Else
                        txt_VendedorNombre1.Text = dtDatos.Rows(0).Item("nombre").ToString.Trim
                        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VOficina\" & Globales.oAmbientes.Sistema, "id_vendedor1", txt_Vendedor1.Text)
                        If txt_Vendedor1.Text.Length > 0 Then
                            ''SW_focus = False
                            ''Call txt_Vendedor2_LostFocus(Nothing, Nothing)
                        Else
                            ''txt_Vendedor2.Focus()
                        End If
                    End If
                Else
                    MessageBox.Show("Vendedor no encontrado..", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            ''SW_focus = False
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub txt_Vendedor2_LostFocus(sender As Object, e As EventArgs) Handles txt_Vendedor2.LostFocus
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        ''If SW_focus Then Return

        Try
            ''SW_focus = True
            txt_VendedorNombre2.Text = ""
                oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Vendedores(Globales.oAmbientes.Id_Empresa, "", txt_Vendedor2.Text, "ACTIVO", "", False, dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows.Count > 1 Then
                        txt_VendedorNombre2.Text = ""
                        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VOficina\" & Globales.oAmbientes.Sistema, "id_vendedor2", txt_Vendedor2.Text)
                    Else
                        txt_VendedorNombre2.Text = dtDatos.Rows(0).Item("nombre").ToString.Trim
                        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\VOficina\" & Globales.oAmbientes.Sistema, "id_vendedor2", txt_Vendedor2.Text)
                        If txt_Vendedor2.Text.Length > 0 Then
                            ''SW_focus = False
                            ''Call txt_Cliente_LostFocus(Nothing, Nothing)
                        Else
                            ''txt_Cliente.Focus()
                        End If
                    End If
                Else
                    MessageBox.Show("Vendedor no encontrado..", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            ''SW_focus = False
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_CodigoCanje_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_CodigoCanje.KeyDown
        If e.KeyCode = 13 Then
            If txt_CodigoCanje.Text <> "" Then
                Call Consultar_Lealtad()
            End If
        End If
    End Sub

    Private Sub txt_CodigoCanje_LostFocus(sender As Object, e As EventArgs) Handles txt_CodigoCanje.LostFocus
        If txt_CodigoCanje.Text <> "" Then
            Call Consultar_Lealtad()
        End If
    End Sub
    Private Sub Consultar_Lealtad()
        Dim oLealtad As dllVOficinaLealtad.Lealtad
        Dim id_empresa As String = ""
        Dim codigo As String = ""
        Dim r_id As Integer
        Dim r_usuario As String = ""
        Dim r_nombre As String = ""
        Dim r_apellido_pat As String = ""
        Dim r_apellido_mat As String = ""
        Dim r_fecha_nacimiento As Date
        Dim r_id_empresa As String = ""
        Dim r_correo As String = ""
        Dim r_telefono As String = ""
        Dim r_celular As String = ""
        Dim r_direccion As String = ""
        Dim r_colonia As String = ""
        Dim r_codigopostal As String = ""
        Dim r_fecha_registro As Date = CDate("01/01/1900")
        Dim r_fecha_ultima_compra As Date = CDate("01/01/1900")
        Dim r_fecha_ultimo_canje As Date = CDate("01/01/1900")
        Dim r_codigo_vigencia As Date
        Dim r_dinero_electronico As Double
        Dim r_dinero_electronico_vigencia As Date
        Dim r_nivel As String = ""
        Dim r_estatus As String = ""
        Dim r_tipo_cliente As String = ""
        Dim r_puntos As Integer
        Dim r_puntos_vigencia As Date = CDate("01/01/1900")
        Dim msj As String = ""
        Try
            oLealtad = New dllVOficinaLealtad.Lealtad


            id_empresa = Globales.oAmbientes.Id_Empresa
            codigo = txt_CodigoCanje.Text
            If oLealtad.ConsultaUsuarios(id_empresa _
                                    , codigo _
                                    , r_id _
                                    , r_usuario _
                                    , r_nombre _
                                    , r_apellido_pat _
                                    , r_apellido_mat _
                                    , r_fecha_nacimiento _
                                    , r_id_empresa _
                                    , r_correo _
                                    , r_telefono _
                                    , r_celular _
                                    , r_direccion _
                                    , r_colonia _
                                    , r_codigopostal _
                                    , r_fecha_registro _
                                    , r_fecha_ultima_compra _
                                    , r_fecha_ultimo_canje _
                                    , r_codigo_vigencia _
                                    , r_dinero_electronico _
                                    , r_dinero_electronico_vigencia _
                                    , r_nivel _
                                    , r_estatus _
                                    , r_puntos _
                                    , r_puntos_vigencia _
                                    , r_tipo_cliente _
                                    , msj) Then
                txt_IdLealtad.Text = r_id.ToString
                txt_NombreCanje.Text = r_nombre & " " & r_apellido_pat & " " & r_apellido_mat
                txt_NombreCanje1.Text = r_nombre
                txt_Puntos.Text = r_puntos.ToString
                txt_DE.Text = r_dinero_electronico.ToString
                txt_TipoCliente.Text = r_tipo_cliente
                btn_Aceptar.Focus()
                If r_tipo_cliente = "CLIENTECREDITO" Then
                    Call BuscarCuenta(r_telefono)
                End If
            Else
                txt_IdLealtad.Text = ""
                txt_NombreCanje.Text = ""
                txt_NombreCanje1.Text = ""
                txt_Puntos.Text = "0"
                txt_DE.Text = "0"
                txt_TipoCliente.Text = ""
                txt_CodigoCanje.Focus()
                txt_CodigoCanje.SelectAll()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub AyudaToolTip()

        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem

        Try

            For Each oRen As Principal.clsTecladoAtajos In TecladoAtajos
                SuperToolTip1 = New DevExpress.Utils.SuperToolTip
                SuperToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True
                ToolTipItem1 = New DevExpress.Utils.ToolTipItem
                ToolTipItem1.Text = "<b>" & oRen.Accion & "</b>" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & oRen.Combinacion
                SuperToolTip1.Items.Add(ToolTipItem1)
                Select Case oRen.Accion
                    Case "ENTREGAS"
                        btn_Entregas.SuperTip = SuperToolTip1
                    Case "CLIENTES"
                        btn_Cliente.SuperTip = SuperToolTip1
                    Case "VENTA MOSTRADOR"
                        ToolTipItem2 = New DevExpress.Utils.ToolTipItem
                        ToolTipItem2.Text = "<b>" & oRen.Accion & "</b>" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & oRen.Combinacion
                    Case "COTIZACION"
                        ToolTipItem3 = New DevExpress.Utils.ToolTipItem
                        ToolTipItem3.Text = "<b>" & oRen.Accion & "</b>" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & oRen.Combinacion

                    Case Else

                End Select
            Next
            If Not ToolTipItem2 Is Nothing Or Not ToolTipItem3 Is Nothing Then
                SuperToolTip1 = New DevExpress.Utils.SuperToolTip
                SuperToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True
                If Not ToolTipItem2 Is Nothing Then
                    SuperToolTip1.Items.Add(ToolTipItem2)
                End If
                If Not ToolTipItem3 Is Nothing Then
                    SuperToolTip1.Items.Add(ToolTipItem3)
                End If
                btn_Buscar.SuperTip = SuperToolTip1
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Dim Combinacion As String
        Dim Accion As String
        Try
            If Not SW_focus Then
                SW_focus = True
            Else
                Select Case e.KeyCode
                    'Case System.Windows.Forms.Keys.Escape
                    '    Regresar(Me.ActiveMdiChild)
                    Case System.Windows.Forms.Keys.Enter
                        btn_Aceptar.Focus()
                    Case System.Windows.Forms.Keys.Escape
                        Me.Close()
                    Case Else

                End Select

                Combinacion = IIf(e.Control, "CTRL+", "") _
                            & IIf(e.Alt, "ALT+", "") _
                            & IIf(e.Shift, "SHIFT+", "") _
                            & IIf(e.KeyCode, e.KeyCode.ToString, "")

                Accion = ""
                For Each oRen As Principal.clsTecladoAtajos In TecladoAtajos
                    If oRen.Combinacion = Combinacion Then
                        Accion = oRen.Accion
                    End If
                Next

                Select Case Accion
                    Case "AYUDA"
                        If Not FormaLlama Is Nothing Then
                            TryCast(Me.FormaLlama, pdvPuntoVenta).PVTA_Ayuda()
                        End If
                    Case "ENTREGAS"
                        PVTA_Entregas(Nothing)
                    Case "CLIENTES"
                        PVTA_Clientes(Nothing)
                    Case "VENTA MOSTRADOR"
                        PVTA_VTAMostrador("VM")
                    Case "COTIZACION"
                        PVTA_VTACotizacion("CO")
                    Case "RECIBIR PAGOS"
                        PVTA_Pagos(Nothing)
                    Case "EXISTENCIAS"
                        btn_Existencia_Click(Nothing, Nothing)
                    Case Else

                End Select

            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_Cliente_LostFocus(sender As Object, e As EventArgs) Handles txt_Cliente.LostFocus
        ''If SW_focus Then Return
        Try
            ''SW_focus = True
            If txt_Cliente.Text.Length > 0 Then
                Call BuscarCuenta(txt_Cliente.Text)
            Else
                ''SW_focus = False
            End If
            ''SW_focus = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txt_pedido_LostFocus(sender As Object, e As EventArgs) Handles txt_pedido.LostFocus
        ''If SW_focus Then Return
        Try
            ''SW_focus = True
            If txt_pedido.Text.Length > 0 Then
                ''txt_CodigoCanje.Focus()
            Else
                ''SW_focus = False
            End If
            ''SW_focus = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub pdvDatosGenerales_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        SW_focus = False
        Try
            GC_Vale.Visible = False
            If Globales.oAmbientes.Id_Empresa = "CRECE" _
                Or Globales.oAmbientes.Id_Empresa = "SIXTY" Then
                GC_Vale.Visible = True
                btn_Aceptar.Enabled = False
            End If
            Call AyudaToolTip()
            If txt_Vendedor1.Text.Length > 0 Then
                ''SW_focus = False
                Call txt_Vendedor1_LostFocus(Nothing, Nothing)
                Call txt_Vendedor2_LostFocus(Nothing, Nothing)
                ''SW_focus = True

                txt_Cliente.Focus()
            Else
                txt_Vendedor1.Focus()
                txt_Vendedor1.SelectAll()
            End If
            If opcion <> "" Then
                Select Case opcion
                    Case "AYUDA"
                        If Not FormaLlama Is Nothing Then
                            TryCast(Me.FormaLlama, pdvPuntoVenta).PVTA_Ayuda()
                        End If
                    Case "ENTREGAS"
                        PVTA_Entregas(Nothing)
                    Case "CLIENTES"
                        PVTA_Clientes(Nothing)
                    Case "VENTA MOSTRADOR"
                        PVTA_VTAMostrador("VM")
                    Case "COTIZACION"
                        PVTA_VTACotizacion("CO")
                    Case "RECIBIR PAGOS"
                        PVTA_Pagos(Nothing)
                    Case "EXISTENCIAS"
                        btn_Existencia_Click(Nothing, Nothing)
                    Case Else

                End Select
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        Dim oFrm As pdvBuscaTransacciones
        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""

        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing

        Try
            oFrm = New pdvBuscaTransacciones
            oFrm.TipoTran = TipoTran
            If sender.GetType.Name.ToString = "String" Then
                If sender = "VM" Then
                    oFrm.RadioGroup1.SelectedIndex = 1
                Else
                    oFrm.RadioGroup1.SelectedIndex = 0
                End If
            End If
            oFrm.ShowDialog()
            If oFrm.Accion Then
                Autonumusc = oFrm.Autonumsuc
                Estatus = oFrm.Estatus
                oDatos = New Datos_Viscoi
                If oDatos.PVTA_Recupera_Transacciones(Today.Date _
                                               , Today.Date _
                                               , 0 _
                                               , Autonumusc _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , "" _
                                               , "" _
                                               , "" _
                                               , "" _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , dtDatos2 _
                                               , Msj) Then
                    For Each oRen As DataRow In dtDatos.Rows
                        txt_pedido.Text = oRen.Item("pedido").ToString.Trim()
                        If oRen.Item("cuenta").ToString.Trim() <> "" Then
                            txt_Cliente.Text = oRen.Item("cuenta").ToString.Trim()
                            txt_TipoCliente.Text = oRen.Item("referencia").ToString.Trim()
                            txt_ClienteDescto.Text = oRen.Item("descuento_global")
                            If txt_Cliente.Text <> "" Then
                                Call BuscarCuenta(txt_Cliente.Text)
                            End If
                        End If
                        If oRen.Item("estatus") = "USADO" Then
                            Select Case oRen.Item("tipo")
                                Case "VENTA"
                                    TipoTran = eTipoTran.VENTA
                                Case "MOSTRADOR"
                                    TipoTran = eTipoTran.MOSTRADOR
                                Case "COTIZACION"
                                    TipoTran = eTipoTran.COTIZACION
                            End Select
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Cliente_Click(sender As Object, e As EventArgs) Handles btn_Cliente.Click
        Dim oFrm As pdvCuentas
        Try
            oFrm = New pdvCuentas
            oFrm.Tipo = ""
            oFrm.TipoForma = pdvCuentas.eTipoForma.Cliente
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Cliente.Text = oFrm.Cuenta
                txt_Vendedor1.Text = oFrm.Vendedor
                Call txt_Vendedor1_LostFocus(Nothing, Nothing)
                Call BuscarCuenta(txt_Cliente.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Empleado_Click(sender As Object, e As EventArgs) Handles btn_Empleado.Click
        Dim oFrm As pdvCuentas
        Try
            oFrm = New pdvCuentas
            oFrm.Tipo = ""
            oFrm.TipoForma = pdvCuentas.eTipoForma.Empleados
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Cliente.Text = oFrm.Cuenta
                ''txt_Vendedor1.Text = oFrm.Vendedor
                Call txt_Vendedor1_LostFocus(Nothing, Nothing)
                Call BuscarCuenta(txt_Cliente.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Entregas_Click(sender As Object, e As EventArgs) Handles btn_Entregas.Click
        Dim oForma As pdvEntregas
        Try
            oForma = New pdvEntregas
            oForma.IdEmpresa = _IdEmpresa
            oForma.IdSucursal = _IdSucursal
            oForma.txt_Caja.Text = _Caja
            oForma.txt_Cajero.Text = _Cajero
            oForma.txt_Corte.Text = ""
            oForma.Impresora = _Impresora
            oForma.TipoOperacion = "CORTE"
            oForma.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oForma = Nothing

    End Sub
    Private Sub BuscarCuenta(ByRef Tarjeta As String)
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi

            txt_ClienteDescto.Text = "0.0"
            If txt_Cliente.Text.Substring(0, 1) <> "C" Then
                If oDatos.PVTA_Recupera_Cuentas("", Tarjeta, "", "", "", "", "", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        txt_Cliente.Text = dtDatos.Rows(0).Item("cuenta").ToString.Trim
                        txt_ClienteNombre.Text = dtDatos.Rows(0).Item("nombre").ToString.Trim
                        txt_ClienteDescto.Text = dtDatos.Rows(0).Item("descuento")
                        txt_ClienteCondicion.text = dtDatos.Rows(0).Item("condicion")
                        txt_TipoCliente.Text = "CLIENTECREDITO"
                        txt_ClienteEstatus.Text = dtDatos.Rows(0).Item("estatus")
                        txt_ClienteTipo.Text = dtDatos.Rows(0).Item("tipo")
                        txt_ClienteNombre.Focus()
                    Else
                        txt_Cliente.Text = ""
                        txt_ClienteNombre.Text = ""
                        txt_ClienteDescto.Text = "0.0"
                        txt_ClienteCondicion.Text = ""
                        txt_TipoCliente.Text = ""
                        txt_ClienteEstatus.Text = ""
                        txt_ClienteTipo.Text = ""
                    End If
                Else
                    txt_Cliente.Text = ""
                    txt_ClienteNombre.Text = ""
                    txt_ClienteCondicion.Text = ""
                    txt_ClienteDescto.Text = "0.0"
                    txt_TipoCliente.Text = ""
                    txt_ClienteEstatus.Text = ""
                    txt_ClienteTipo.Text = ""
                End If
            Else
                If oDatos.PVTA_Empleados("", "", txt_Cliente.Text.Substring(1), "", "", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        If dtDatos.Rows(0).Item("estatus") = "ALTA" Then

                            txt_Cliente.Text = dtDatos.Rows(0).Item("id_empleado")
                            txt_ClienteNombre.Text = dtDatos.Rows(0).Item("nombre")
                            txt_ClienteCondicion.Text = ""
                            txt_ClienteDescto.Text = dtDatos.Rows(0).Item("descuento")
                            txt_ClienteEstatus.Text = dtDatos.Rows(0).Item("estatus")
                            txt_TipoCliente.Text = "EMPLEADO"
                            txt_ClienteNombre.Focus()

                        Else
                            MessageBox.Show("El empleado (" & txt_Cliente.Text & ") no tiene estatus ALTA.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            txt_Cliente.Text = ""
                            txt_ClienteNombre.Text = ""
                            txt_ClienteCondicion.Text = ""
                            txt_ClienteDescto.Text = "0.0"
                            txt_ClienteEstatus.Text = ""
                        End If
                    Else
                        txt_Cliente.Text = ""
                        txt_ClienteNombre.Text = ""
                        txt_ClienteCondicion.Text = ""
                        txt_ClienteDescto.Text = "0.0"
                        txt_ClienteEstatus.Text = ""
                    End If
                Else
                    txt_Cliente.Text = ""
                    txt_ClienteNombre.Text = ""
                    txt_ClienteCondicion.Text = ""
                    txt_ClienteDescto.Text = "0.0"
                    txt_ClienteEstatus.Text = ""
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub Btn_Pagos_Click(sender As Object, e As EventArgs) Handles Btn_Pagos.Click
        Dim oForma As pdvRecibirPagos
        Try
            oForma = New pdvRecibirPagos
            oForma.txt_Caja.Text = txt_Caja.Text
            oForma.txt_Cajero.Text = txt_Cajero.Text
            oForma.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Existencia_Click(sender As Object, e As EventArgs) Handles btn_Existencia.Click
        Dim oForma As SelExistencia
        Try
            oForma = New SelExistencia
            oForma.chk_Existencias.Checked = True
            oForma.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub txt_Cliente_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_Cliente.KeyUp
        Try
            If e.KeyCode = 13 Then
                If txt_Cliente.Text.Length > 0 Then
                    Call BuscarCuenta(txt_Cliente.Text)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Vendedor1_Click(sender As Object, e As EventArgs) Handles btn_Vendedor1.Click
        Dim oFrm As pdvVendedores
        Try
            oFrm = New pdvVendedores
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Vendedor1.Text = oFrm.Txt_IdVendedor.Text
                txt_VendedorNombre1.Text = oFrm.Txt_Vendedor.Text.Trim
                Call txt_Vendedor1_LostFocus(Nothing, Nothing)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Vendedor2_Click(sender As Object, e As EventArgs) Handles btn_Vendedor2.Click
        Dim oFrm As pdvVendedores
        Try
            oFrm = New pdvVendedores
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Vendedor2.Text = oFrm.Txt_IdVendedor.Text
                txt_VendedorNombre2.Text = oFrm.Txt_Vendedor.Text
                Call txt_Vendedor2_LostFocus(Nothing, Nothing)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_PedidoWeb_Click(sender As Object, e As EventArgs) Handles btn_PedidoWeb.Click
        Dim oDatos As Datos_Viscoi
        Dim dt1 As DataTable = Nothing
        Dim dt2 As DataTable = Nothing
        Dim dt3 As DataTable = Nothing
        Dim Pedido_ID As Integer
        Dim Msj As String = ""
        Try
            oDatos = New Datos_Viscoi
            Pedido_ID = Integer.Parse(txt_pedido.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
            If oDatos.PVTA_Pedidos_Sel(Globales.oAmbientes.Id_Empresa, Pedido_ID, dt1, dt2, dt3, Msj) Then
                If dt2.Rows.Count > 0 And dt3.Rows.Count > 0 Then
                    For Each oRen As DataRow In dt2.Rows
                        txt_CodigoCanje.Text = oRen.Item("codigo_canje")
                        Call txt_CodigoCanje_LostFocus(Nothing, Nothing)
                        Call btn_Aceptar_Click(Nothing, Nothing)
                    Next
                    txt_PedidoDet.Text = ""
                    For Each oRen As DataRow In dt3.Rows
                        If txt_PedidoDet.Text = "" Then
                            txt_PedidoDet.Text = oRen.Item("lector")
                        Else
                            txt_PedidoDet.Text = txt_PedidoDet.Text & "|" & oRen.Item("lector")
                        End If
                    Next
                Else
                    MessageBox.Show("No se encontraron registros.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles btn_InfoPedidosWeb.Click
        Dim frm1 As LogMasivo
        Dim lstLista As List(Of String)
        Try
            frm1 = New LogMasivo
            If Not MensajeNotificacion Is Nothing Then
                If MensajeNotificacion.Text.Length > 0 Then
                    lstLista = New List(Of String)
                    For Each xCad As String In MensajeNotificacion.Text.Split("|")
                        lstLista.Add(xCad)
                    Next
                    frm1.Log = lstLista
                    frm1.ShowDialog()
                End If
            End If
        Catch ex As Exception

        End Try
        frm1 = Nothing
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim lstLista As List(Of String)
        Try
            If MensajeNotificacion.Text.Length > 0 Then
                lstLista = New List(Of String)
                For Each xCad As String In MensajeNotificacion.Text.Split("|")
                    If xCad <> "" Then
                        lstLista.Add(xCad)
                    End If
                Next
                btn_InfoPedidosWeb.Text = "(" & Format(lstLista.Count, "00") & ")"
            Else
                btn_InfoPedidosWeb.Text = "(00)"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_FinalCliente_Click(sender As Object, e As EventArgs) Handles btn_FinalCliente.Click
        Dim oFrm As pdvCuentasFinal
        Try
            oFrm = New pdvCuentasFinal
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_IdClienteFinal.Text = oFrm.Cuenta
                txt_NombreClienteFinal.Text = oFrm.Nombre
                BuscarFinalCuenta(txt_IdClienteFinal.Text)

            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub BuscarFinalCuenta(ByRef Tarjeta As String)
        Dim Msj As String = ""

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim posExt As Integer
        Dim posInt As Integer
        Dim CodVer As String
        Try
            oDatos = New Datos_Viscoi

            txt_ClienteDescto.Text = "0.0"
            If oDatos.PVTA_Recupera_ClienteFinal(txt_IdClienteFinal.Text, "", "", "", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_IdClienteFinal.Text = dtDatos.Rows(0).Item("id_cliente").ToString.Trim
                    txt_NombreClienteFinal.Text = dtDatos.Rows(0).Item("nombre_cli").ToString.Trim
                    CodVer = InputBox("Se ha enviado un código de verificación al celular del cliente, solicitacelo y teclealo.", "Código de Verificación")
                    If CodVer = dtDatos.Rows(0).Item("fax").ToString.Trim Then
                        btn_Aceptar.Enabled = True
                    Else
                        MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "(" & CodVer & ") Código de Verificación no valido.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txt_IdClienteFinal.Text = ""
                        txt_NombreClienteFinal.Text = ""
                    End If
                Else
                    txt_NombreClienteFinal.Text = ""
                End If
            Else
                txt_NombreClienteFinal.Text = ""
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
End Class