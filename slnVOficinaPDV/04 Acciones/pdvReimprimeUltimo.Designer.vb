﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvReimprimeUltimo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.txt_Autonumsuc = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Sucursal = New System.Windows.Forms.TextBox()
        Me.txt_DinEleFinal = New DevExpress.XtraEditors.TextEdit()
        Me.txt_DinEleUtilizados = New DevExpress.XtraEditors.TextEdit()
        Me.txt_DinEleAcumulados = New DevExpress.XtraEditors.TextEdit()
        Me.txt_DinEleActual = New DevExpress.XtraEditors.TextEdit()
        Me.txt_PuntosFinal = New DevExpress.XtraEditors.TextEdit()
        Me.txt_PuntosUtilizados = New DevExpress.XtraEditors.TextEdit()
        Me.txt_PuntosAcumulados = New DevExpress.XtraEditors.TextEdit()
        Me.txt_PuntosActual = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Cantidad = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Articulos = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IVA = New DevExpress.XtraEditors.TextEdit()
        Me.txt_SubTotal = New DevExpress.XtraEditors.TextEdit()
        Me.txt1_Descto = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Cambio = New DevExpress.XtraEditors.TextEdit()
        Me.txt_DE = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Contado = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Total = New DevExpress.XtraEditors.TextEdit()
        Me.txt_DesctoGlobal = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.txt_NombreCanje = New System.Windows.Forms.TextBox()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txt_DinEleFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_DinEleUtilizados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_DinEleAcumulados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_DinEleActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PuntosFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PuntosUtilizados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PuntosAcumulados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PuntosActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Cantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Articulos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_SubTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_Descto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Cambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_DE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Contado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(109, 48)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(355, 48)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.txt_TipoCliente)
        Me.GroupControl3.Controls.Add(Me.Label31)
        Me.GroupControl3.Controls.Add(Me.txt_NombreCanje)
        Me.GroupControl3.Controls.Add(Me.lstImpresoras)
        Me.GroupControl3.Controls.Add(Me.txt_Direccion2)
        Me.GroupControl3.Controls.Add(Me.txt_Direccion)
        Me.GroupControl3.Controls.Add(Me.txt_RFC)
        Me.GroupControl3.Controls.Add(Me.txt_Empresa)
        Me.GroupControl3.Controls.Add(Me.txt_DesctoGlobal)
        Me.GroupControl3.Controls.Add(Me.txt_Total)
        Me.GroupControl3.Controls.Add(Me.txt_DinEleFinal)
        Me.GroupControl3.Controls.Add(Me.txt_Sucursal)
        Me.GroupControl3.Controls.Add(Me.txt_DinEleUtilizados)
        Me.GroupControl3.Controls.Add(Me.txt_Cajero)
        Me.GroupControl3.Controls.Add(Me.txt_DinEleAcumulados)
        Me.GroupControl3.Controls.Add(Me.txt_Caja)
        Me.GroupControl3.Controls.Add(Me.txt_DinEleActual)
        Me.GroupControl3.Controls.Add(Me.txt_Autonumsuc)
        Me.GroupControl3.Controls.Add(Me.txt_PuntosFinal)
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.txt_PuntosUtilizados)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Controls.Add(Me.txt_PuntosAcumulados)
        Me.GroupControl3.Controls.Add(Me.txt_Contado)
        Me.GroupControl3.Controls.Add(Me.txt_PuntosActual)
        Me.GroupControl3.Controls.Add(Me.txt_DE)
        Me.GroupControl3.Controls.Add(Me.txt_Cantidad)
        Me.GroupControl3.Controls.Add(Me.txt_Cambio)
        Me.GroupControl3.Controls.Add(Me.txt_Articulos)
        Me.GroupControl3.Controls.Add(Me.txt1_Descto)
        Me.GroupControl3.Controls.Add(Me.txt_IVA)
        Me.GroupControl3.Controls.Add(Me.txt_SubTotal)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(514, 95)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "¿Deseas Imprimir Ticket de Venta?"
        '
        'txt_Autonumsuc
        '
        Me.txt_Autonumsuc.Location = New System.Drawing.Point(5, 26)
        Me.txt_Autonumsuc.Name = "txt_Autonumsuc"
        Me.txt_Autonumsuc.Size = New System.Drawing.Size(21, 21)
        Me.txt_Autonumsuc.TabIndex = 2
        Me.txt_Autonumsuc.Visible = False
        '
        'txt_Caja
        '
        Me.txt_Caja.Location = New System.Drawing.Point(32, 26)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(21, 21)
        Me.txt_Caja.TabIndex = 3
        Me.txt_Caja.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Location = New System.Drawing.Point(59, 26)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(21, 21)
        Me.txt_Cajero.TabIndex = 4
        Me.txt_Cajero.Visible = False
        '
        'txt_Sucursal
        '
        Me.txt_Sucursal.Location = New System.Drawing.Point(82, 26)
        Me.txt_Sucursal.Name = "txt_Sucursal"
        Me.txt_Sucursal.Size = New System.Drawing.Size(21, 21)
        Me.txt_Sucursal.TabIndex = 5
        Me.txt_Sucursal.Visible = False
        '
        'txt_DinEleFinal
        '
        Me.txt_DinEleFinal.EditValue = ""
        Me.txt_DinEleFinal.Location = New System.Drawing.Point(0, 577)
        Me.txt_DinEleFinal.Name = "txt_DinEleFinal"
        Me.txt_DinEleFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DinEleFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DinEleFinal.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_DinEleFinal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_DinEleFinal.Properties.ReadOnly = True
        Me.txt_DinEleFinal.Size = New System.Drawing.Size(26, 32)
        Me.txt_DinEleFinal.TabIndex = 57
        Me.txt_DinEleFinal.Visible = False
        '
        'txt_DinEleUtilizados
        '
        Me.txt_DinEleUtilizados.EditValue = ""
        Me.txt_DinEleUtilizados.Location = New System.Drawing.Point(0, 545)
        Me.txt_DinEleUtilizados.Name = "txt_DinEleUtilizados"
        Me.txt_DinEleUtilizados.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DinEleUtilizados.Properties.Appearance.Options.UseFont = True
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DinEleUtilizados.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_DinEleUtilizados.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_DinEleUtilizados.Properties.ReadOnly = True
        Me.txt_DinEleUtilizados.Size = New System.Drawing.Size(26, 32)
        Me.txt_DinEleUtilizados.TabIndex = 56
        Me.txt_DinEleUtilizados.Visible = False
        '
        'txt_DinEleAcumulados
        '
        Me.txt_DinEleAcumulados.EditValue = ""
        Me.txt_DinEleAcumulados.Location = New System.Drawing.Point(0, 513)
        Me.txt_DinEleAcumulados.Name = "txt_DinEleAcumulados"
        Me.txt_DinEleAcumulados.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DinEleAcumulados.Properties.Appearance.Options.UseFont = True
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DinEleAcumulados.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_DinEleAcumulados.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_DinEleAcumulados.Properties.ReadOnly = True
        Me.txt_DinEleAcumulados.Size = New System.Drawing.Size(26, 32)
        Me.txt_DinEleAcumulados.TabIndex = 55
        Me.txt_DinEleAcumulados.Visible = False
        '
        'txt_DinEleActual
        '
        Me.txt_DinEleActual.EditValue = ""
        Me.txt_DinEleActual.Location = New System.Drawing.Point(0, 481)
        Me.txt_DinEleActual.Name = "txt_DinEleActual"
        Me.txt_DinEleActual.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DinEleActual.Properties.Appearance.Options.UseFont = True
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DinEleActual.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_DinEleActual.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_DinEleActual.Properties.ReadOnly = True
        Me.txt_DinEleActual.Size = New System.Drawing.Size(26, 32)
        Me.txt_DinEleActual.TabIndex = 54
        Me.txt_DinEleActual.Visible = False
        '
        'txt_PuntosFinal
        '
        Me.txt_PuntosFinal.EditValue = ""
        Me.txt_PuntosFinal.Location = New System.Drawing.Point(0, 449)
        Me.txt_PuntosFinal.Name = "txt_PuntosFinal"
        Me.txt_PuntosFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PuntosFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_PuntosFinal.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_PuntosFinal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_PuntosFinal.Properties.ReadOnly = True
        Me.txt_PuntosFinal.Size = New System.Drawing.Size(26, 32)
        Me.txt_PuntosFinal.TabIndex = 52
        Me.txt_PuntosFinal.Visible = False
        '
        'txt_PuntosUtilizados
        '
        Me.txt_PuntosUtilizados.EditValue = ""
        Me.txt_PuntosUtilizados.Location = New System.Drawing.Point(0, 417)
        Me.txt_PuntosUtilizados.Name = "txt_PuntosUtilizados"
        Me.txt_PuntosUtilizados.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PuntosUtilizados.Properties.Appearance.Options.UseFont = True
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_PuntosUtilizados.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_PuntosUtilizados.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_PuntosUtilizados.Properties.ReadOnly = True
        Me.txt_PuntosUtilizados.Size = New System.Drawing.Size(26, 32)
        Me.txt_PuntosUtilizados.TabIndex = 51
        Me.txt_PuntosUtilizados.Visible = False
        '
        'txt_PuntosAcumulados
        '
        Me.txt_PuntosAcumulados.EditValue = ""
        Me.txt_PuntosAcumulados.Location = New System.Drawing.Point(0, 385)
        Me.txt_PuntosAcumulados.Name = "txt_PuntosAcumulados"
        Me.txt_PuntosAcumulados.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PuntosAcumulados.Properties.Appearance.Options.UseFont = True
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_PuntosAcumulados.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_PuntosAcumulados.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_PuntosAcumulados.Properties.ReadOnly = True
        Me.txt_PuntosAcumulados.Size = New System.Drawing.Size(26, 32)
        Me.txt_PuntosAcumulados.TabIndex = 50
        Me.txt_PuntosAcumulados.Visible = False
        '
        'txt_PuntosActual
        '
        Me.txt_PuntosActual.EditValue = ""
        Me.txt_PuntosActual.Location = New System.Drawing.Point(0, 353)
        Me.txt_PuntosActual.Name = "txt_PuntosActual"
        Me.txt_PuntosActual.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PuntosActual.Properties.Appearance.Options.UseFont = True
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_PuntosActual.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_PuntosActual.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_PuntosActual.Properties.ReadOnly = True
        Me.txt_PuntosActual.Size = New System.Drawing.Size(26, 32)
        Me.txt_PuntosActual.TabIndex = 49
        Me.txt_PuntosActual.Visible = False
        '
        'txt_Cantidad
        '
        Me.txt_Cantidad.EditValue = ""
        Me.txt_Cantidad.Location = New System.Drawing.Point(0, 321)
        Me.txt_Cantidad.Name = "txt_Cantidad"
        Me.txt_Cantidad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cantidad.Properties.Appearance.Options.UseFont = True
        Me.txt_Cantidad.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Cantidad.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_Cantidad.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Cantidad.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Cantidad.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Cantidad.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Cantidad.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Cantidad.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Cantidad.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Cantidad.Properties.ReadOnly = True
        Me.txt_Cantidad.Size = New System.Drawing.Size(30, 32)
        Me.txt_Cantidad.TabIndex = 43
        Me.txt_Cantidad.Visible = False
        '
        'txt_Articulos
        '
        Me.txt_Articulos.EditValue = ""
        Me.txt_Articulos.Location = New System.Drawing.Point(0, 289)
        Me.txt_Articulos.Name = "txt_Articulos"
        Me.txt_Articulos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Articulos.Properties.Appearance.Options.UseFont = True
        Me.txt_Articulos.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Articulos.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_Articulos.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Articulos.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Articulos.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Articulos.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Articulos.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Articulos.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Articulos.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Articulos.Properties.ReadOnly = True
        Me.txt_Articulos.Size = New System.Drawing.Size(30, 32)
        Me.txt_Articulos.TabIndex = 42
        Me.txt_Articulos.Visible = False
        '
        'txt_IVA
        '
        Me.txt_IVA.EditValue = ""
        Me.txt_IVA.Location = New System.Drawing.Point(0, 257)
        Me.txt_IVA.Name = "txt_IVA"
        Me.txt_IVA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IVA.Properties.Appearance.Options.UseFont = True
        Me.txt_IVA.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_IVA.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_IVA.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_IVA.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_IVA.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_IVA.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_IVA.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_IVA.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_IVA.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_IVA.Properties.ReadOnly = True
        Me.txt_IVA.Size = New System.Drawing.Size(30, 32)
        Me.txt_IVA.TabIndex = 45
        Me.txt_IVA.Visible = False
        '
        'txt_SubTotal
        '
        Me.txt_SubTotal.EditValue = ""
        Me.txt_SubTotal.Location = New System.Drawing.Point(0, 225)
        Me.txt_SubTotal.Name = "txt_SubTotal"
        Me.txt_SubTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SubTotal.Properties.Appearance.Options.UseFont = True
        Me.txt_SubTotal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txt_SubTotal.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_SubTotal.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_SubTotal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_SubTotal.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_SubTotal.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_SubTotal.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_SubTotal.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_SubTotal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_SubTotal.Properties.ReadOnly = True
        Me.txt_SubTotal.Size = New System.Drawing.Size(30, 32)
        Me.txt_SubTotal.TabIndex = 44
        Me.txt_SubTotal.Visible = False
        '
        'txt1_Descto
        '
        Me.txt1_Descto.EditValue = ""
        Me.txt1_Descto.Location = New System.Drawing.Point(0, 171)
        Me.txt1_Descto.Name = "txt1_Descto"
        Me.txt1_Descto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!)
        Me.txt1_Descto.Properties.Appearance.Options.UseFont = True
        Me.txt1_Descto.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LimeGreen
        Me.txt1_Descto.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt1_Descto.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.White
        Me.txt1_Descto.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt1_Descto.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt1_Descto.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt1_Descto.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt1_Descto.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt1_Descto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt1_Descto.Properties.ReadOnly = True
        Me.txt1_Descto.Size = New System.Drawing.Size(30, 54)
        Me.txt1_Descto.TabIndex = 46
        Me.txt1_Descto.Visible = False
        '
        'txt_Cambio
        '
        Me.txt_Cambio.EditValue = ""
        Me.txt_Cambio.Location = New System.Drawing.Point(0, 117)
        Me.txt_Cambio.Name = "txt_Cambio"
        Me.txt_Cambio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!)
        Me.txt_Cambio.Properties.Appearance.Options.UseFont = True
        Me.txt_Cambio.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Cambio.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_Cambio.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Cambio.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Cambio.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Cambio.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Cambio.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Cambio.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Cambio.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Cambio.Properties.ReadOnly = True
        Me.txt_Cambio.Size = New System.Drawing.Size(30, 54)
        Me.txt_Cambio.TabIndex = 53
        Me.txt_Cambio.Visible = False
        '
        'txt_DE
        '
        Me.txt_DE.EditValue = ""
        Me.txt_DE.Location = New System.Drawing.Point(0, 85)
        Me.txt_DE.Name = "txt_DE"
        Me.txt_DE.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DE.Properties.Appearance.Options.UseFont = True
        Me.txt_DE.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_DE.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_DE.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DE.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_DE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_DE.Properties.ReadOnly = True
        Me.txt_DE.Size = New System.Drawing.Size(30, 32)
        Me.txt_DE.TabIndex = 48
        Me.txt_DE.Visible = False
        '
        'txt_Contado
        '
        Me.txt_Contado.EditValue = ""
        Me.txt_Contado.Location = New System.Drawing.Point(0, 53)
        Me.txt_Contado.Name = "txt_Contado"
        Me.txt_Contado.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Contado.Properties.Appearance.Options.UseFont = True
        Me.txt_Contado.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Contado.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_Contado.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Contado.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Contado.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Contado.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Contado.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Contado.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Contado.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Contado.Properties.ReadOnly = True
        Me.txt_Contado.Size = New System.Drawing.Size(30, 32)
        Me.txt_Contado.TabIndex = 47
        Me.txt_Contado.Visible = False
        '
        'txt_Total
        '
        Me.txt_Total.EditValue = ""
        Me.txt_Total.Location = New System.Drawing.Point(476, 26)
        Me.txt_Total.Name = "txt_Total"
        Me.txt_Total.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.Appearance.Options.UseFont = True
        Me.txt_Total.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Black
        Me.txt_Total.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Total.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Total.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Total.Properties.ReadOnly = True
        Me.txt_Total.Size = New System.Drawing.Size(26, 54)
        Me.txt_Total.TabIndex = 17
        Me.txt_Total.Visible = False
        '
        'txt_DesctoGlobal
        '
        Me.txt_DesctoGlobal.Location = New System.Drawing.Point(440, 26)
        Me.txt_DesctoGlobal.Name = "txt_DesctoGlobal"
        Me.txt_DesctoGlobal.Size = New System.Drawing.Size(30, 21)
        Me.txt_DesctoGlobal.TabIndex = 58
        Me.txt_DesctoGlobal.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Location = New System.Drawing.Point(289, 69)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(30, 21)
        Me.txt_Direccion2.TabIndex = 62
        Me.txt_Direccion2.Visible = False
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Location = New System.Drawing.Point(217, 69)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(30, 21)
        Me.txt_Direccion.TabIndex = 61
        Me.txt_Direccion.Visible = False
        '
        'txt_RFC
        '
        Me.txt_RFC.Location = New System.Drawing.Point(253, 48)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(30, 21)
        Me.txt_RFC.TabIndex = 60
        Me.txt_RFC.Visible = False
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Location = New System.Drawing.Point(217, 48)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(30, 21)
        Me.txt_Empresa.TabIndex = 59
        Me.txt_Empresa.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(43, 24)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(103, 23)
        Me.Label31.TabIndex = 64
        Me.Label31.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(152, 26)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(350, 21)
        Me.lstImpresoras.TabIndex = 63
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Location = New System.Drawing.Point(50, 70)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(30, 21)
        Me.txt_TipoCliente.TabIndex = 13
        Me.txt_TipoCliente.Visible = False
        '
        'txt_NombreCanje
        '
        Me.txt_NombreCanje.Location = New System.Drawing.Point(50, 49)
        Me.txt_NombreCanje.Name = "txt_NombreCanje"
        Me.txt_NombreCanje.Size = New System.Drawing.Size(30, 21)
        Me.txt_NombreCanje.TabIndex = 12
        Me.txt_NombreCanje.Visible = False
        '
        'pdvReimprimeUltimo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(514, 95)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.KeyPreview = True
        Me.Name = "pdvReimprimeUltimo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reimprime Ultimo Ticket"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txt_DinEleFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_DinEleUtilizados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_DinEleAcumulados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_DinEleActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PuntosFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PuntosUtilizados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PuntosAcumulados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PuntosActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Cantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Articulos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_SubTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_Descto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Cambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_DE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Contado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents txt_Autonumsuc As TextBox
    Friend WithEvents txt_Sucursal As TextBox
    Friend WithEvents txt_DinEleFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_DinEleUtilizados As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_DinEleAcumulados As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_DinEleActual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_PuntosFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_PuntosUtilizados As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_PuntosAcumulados As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Contado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_PuntosActual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_DE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Cantidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Cambio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Articulos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt1_Descto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IVA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_SubTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Total As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_DesctoGlobal As TextBox
    Friend WithEvents txt_Direccion2 As TextBox
    Friend WithEvents txt_Direccion As TextBox
    Friend WithEvents txt_RFC As TextBox
    Friend WithEvents txt_Empresa As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Friend WithEvents txt_TipoCliente As TextBox
    Friend WithEvents txt_NombreCanje As TextBox
End Class
