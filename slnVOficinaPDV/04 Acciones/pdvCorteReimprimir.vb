﻿Public Class pdvCorteReimprimir
    Private Sub pdvTicketReimprimir_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatosSuc As DataTable = Nothing
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Dim PDatCaja As String = ""

        Try
            DateTimePicker1.Value = Today

            oDatos = New Datos_Viscoi
            Dim pd As New Printing.PrintDocument
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer
            txt_Empresa.Text = Globales.oAmbientes.Id_Empresa
            If oDatos.PVTA_Recupera_Sucursales(txt_Empresa.Text, "", dtDatosSuc, Msj) Then
                cbo_Sucursal.DataSource = dtDatosSuc
                cbo_Sucursal.SelectedIndex = 0
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    PDatCaja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next
            txt_Caja.Text = "TODAS"
            cbo_Sucursal.Text = PDatSucursal
            If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text, "", txt_Caja.Text, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                    txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper
                End If
            End If

            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                lstImpresoras.Enabled = True
            Else
                lstImpresoras.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entCorte
        Dim Reporte As Object
        Dim sw_continuar As Boolean = True
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Try
            If txt_Corte.Text <> "" Then
                oDatos = New Datos_Viscoi
                If RadioGroup1.Properties.Items(RadioGroup1.SelectedIndex).Value = "ENTREGA" Then
                    Dim movimiento As String
                    movimiento = Microsoft.VisualBasic.Right("0000" & cbo_Sucursal.Text.Substring(0, 4).Trim, 4) & Format(txt_Corte.Value, "00000000")
                    If oDatos.PV_Recupera_EntregaParcialCaja(Globales.oAmbientes.Id_Empresa _
                                               , "Todas" _
                                               , "Todas" _
                                               , "Todos" _
                                               , 0 _
                                               , DateTimePicker1.Value _
                                               , movimiento _
                                               , dtDatos _
                                               , Mensaje) Then
                        sw_continuar = True
                    Else
                        sw_continuar = False
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    If oDatos.PV_Recupera_CorteCaja(txt_Empresa.Text _
                                               , cbo_Sucursal.Text _
                                               , txt_Caja.Text _
                                               , "Todos" _
                                               , txt_Corte.Value _
                                               , DateTimePicker1.Value _
                                               , dtDatos _
                                               , Mensaje) Then
                        sw_continuar = True
                    Else
                        sw_continuar = False
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
                If sw_continuar Then
                    oTicket = New entCorte
                    oTicket.Fill(dtDatos)

                    oTicket.Empresa = txt_Empresa.Text
                    oTicket.Id_sucursal = cbo_Sucursal.Text
                    oTicket.Cajero = txt_Cajero.Text
                    oTicket.Caja = txt_Caja.Text
                    oTicket.Corte = txt_Corte.Value



                    Select Case txt_FormatoTicket.Text
                        Case "SEAFON"
                            Reporte = New xtraRepCorte_Seafon
                            TryCast(Reporte, xtraRepCorte_Seafon).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepCorte_Seafon).CreateDocument()
                        Case "EPPSON"
                            Reporte = New xtraRepCorte
                            TryCast(Reporte, xtraRepCorte).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepCorte).CreateDocument()
                        Case "CARTA"
                            Reporte = New xtraRepCorte_Carta
                            TryCast(Reporte, xtraRepCorte_Carta).SwReimpresion = True
                            TryCast(Reporte, xtraRepCorte_Carta).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepCorte_Carta).CreateDocument()
                        Case Else
                            Reporte = New xtraRepCorte
                            TryCast(Reporte, xtraRepCorte).ods.DataSource = oTicket
                            TryCast(Reporte, xtraRepCorte).CreateDocument()

                    End Select
                    ''printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                    ''printTool.ShowPreviewDialog()
                    If Globales.oAmbientes.Id_Empresa = "CRECE" Then
                        printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                        printTool.ShowPreviewDialog()
                    Else
                        Dim printBase = New DevExpress.XtraPrinting.PrintToolBase(Reporte.PrintingSystem)
                        printBase.PrinterSettings.Copies = 1
                        printBase.Print(lstImpresoras.Text)
                    End If
                End If
            Else
                MessageBox.Show("Debes teclar un Folio para reimprimir", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Try
            If RadioGroup1.Properties.Items(RadioGroup1.SelectedIndex).Value = "ENTREGA" Then
                lbl_Caja.Visible = False
                txt_Caja.Visible = False
                lbl_Corte.Text = "Movimiento"
            Else
                lbl_Caja.Visible = True
                txt_Caja.Visible = True
                lbl_Corte.Text = "Corte"
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class