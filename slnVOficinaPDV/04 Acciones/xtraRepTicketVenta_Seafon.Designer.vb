﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class xtraRepTicketVenta_Seafon
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim ObjectConstructorInfo1 As DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo = New DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.lbl_Descto = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Pic_logo = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DetailReport = New DevExpress.XtraReports.UI.DetailReportBand()
        Me.Detail1 = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_DesctoEtiqArt = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_DesctoArt = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.lbl_PieDesctoVal = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_PieDesctoEtiq = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xlbl_despedida = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ods = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource(Me.components)
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.txt_PuntosAct = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_PuntosAct = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_PuntosUtilizados = New DevExpress.XtraReports.UI.XRLabel()
        Me.txt_PuntosUtilizados = New DevExpress.XtraReports.UI.XRLabel()
        Me.txt_PuntosAcumulados = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_PuntosAcumulados = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_PuntosAnt = New DevExpress.XtraReports.UI.XRLabel()
        Me.txt_PuntosAnt = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_TitPuntos = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_TitDinEle = New DevExpress.XtraReports.UI.XRLabel()
        Me.txt_DEAnt = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_DEAnt = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_DEAcumulados = New DevExpress.XtraReports.UI.XRLabel()
        Me.txt_DEAcumulados = New DevExpress.XtraReports.UI.XRLabel()
        Me.txt_DEUtilizados = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_DEUtilizados = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_DEAct = New DevExpress.XtraReports.UI.XRLabel()
        Me.txt_DEAct = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter3 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.cal_Subtotal = New DevExpress.XtraReports.UI.CalculatedField()
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.ods, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 30.0!
        Me.TopMargin.Name = "TopMargin"
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 25.0!
        Me.BottomMargin.Name = "BottomMargin"
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_Descto, Me.XrLabel26, Me.XrLabel27, Me.XrLabel24, Me.XrLabel25, Me.XrLabel2, Me.XrLabel23, Me.XrLabel22, Me.Pic_logo, Me.XrLabel21, Me.XrLabel20, Me.XrLabel19, Me.XrLabel18, Me.XrLine2, Me.XrLabel4, Me.XrLabel3, Me.XrLabel1})
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 591.5873!
        Me.Detail.HierarchyPrintOptions.Indent = 50.8!
        Me.Detail.Name = "Detail"
        '
        'lbl_Descto
        '
        Me.lbl_Descto.CanGrow = False
        Me.lbl_Descto.Dpi = 254.0!
        Me.lbl_Descto.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Descuento]")})
        Me.lbl_Descto.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_Descto.LocationFloat = New DevExpress.Utils.PointFloat(450.6665!, 524.0424!)
        Me.lbl_Descto.Multiline = True
        Me.lbl_Descto.Name = "lbl_Descto"
        Me.lbl_Descto.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_Descto.SizeF = New System.Drawing.SizeF(12.33347!, 42.54492!)
        Me.lbl_Descto.StylePriority.UseFont = False
        Me.lbl_Descto.StylePriority.UseTextAlignment = False
        Me.lbl_Descto.Text = "XrLabel4"
        Me.lbl_Descto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.lbl_Descto.TextFormatString = "{0:c2}"
        '
        'XrLabel26
        '
        Me.XrLabel26.CanGrow = False
        Me.XrLabel26.Dpi = 254.0!
        Me.XrLabel26.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Cajero]")})
        Me.XrLabel26.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(116.4168!, 523.875!)
        Me.XrLabel26.Multiline = True
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(82.02056!, 42.54504!)
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "XrLabel4"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrLabel26.TextFormatString = "{0:d}"
        '
        'XrLabel27
        '
        Me.XrLabel27.CanGrow = False
        Me.XrLabel27.Dpi = 254.0!
        Me.XrLabel27.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 6.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(4.333392!, 523.875!)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel27.SizeF = New System.Drawing.SizeF(112.0834!, 42.54999!)
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.Text = "CAJERO"
        '
        'XrLabel24
        '
        Me.XrLabel24.CanGrow = False
        Me.XrLabel24.Dpi = 254.0!
        Me.XrLabel24.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 6.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(7.979409!, 480.3298!)
        Me.XrLabel24.Multiline = True
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(108.4374!, 42.54999!)
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.Text = "CAJA"
        '
        'XrLabel25
        '
        Me.XrLabel25.CanGrow = False
        Me.XrLabel25.Dpi = 254.0!
        Me.XrLabel25.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Caja]")})
        Me.XrLabel25.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(116.4168!, 480.3298!)
        Me.XrLabel25.Multiline = True
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(82.02068!, 42.54492!)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = "XrLabel4"
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrLabel25.TextFormatString = "{0:d}"
        '
        'XrLabel2
        '
        Me.XrLabel2.CanGrow = False
        Me.XrLabel2.Dpi = 254.0!
        Me.XrLabel2.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Hora]")})
        Me.XrLabel2.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(286.4572!, 480.3298!)
        Me.XrLabel2.Multiline = True
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(164.2093!, 42.54504!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "XrLabel4"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrLabel2.TextFormatString = "{0:d}"
        '
        'XrLabel23
        '
        Me.XrLabel23.CanGrow = False
        Me.XrLabel23.Dpi = 254.0!
        Me.XrLabel23.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 6.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(199.1447!, 480.3299!)
        Me.XrLabel23.Multiline = True
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(87.31244!, 42.54996!)
        Me.XrLabel23.StylePriority.UseFont = False
        Me.XrLabel23.Text = "HORA"
        '
        'XrLabel22
        '
        Me.XrLabel22.CanGrow = False
        Me.XrLabel22.Dpi = 254.0!
        Me.XrLabel22.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 6.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(199.1447!, 437.0789!)
        Me.XrLabel22.Multiline = True
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(93.31248!, 42.54999!)
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.Text = "FECHA"
        '
        'Pic_logo
        '
        Me.Pic_logo.Dpi = 254.0!
        Me.Pic_logo.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 0!)
        Me.Pic_logo.Name = "Pic_logo"
        Me.Pic_logo.SizeF = New System.Drawing.SizeF(438.4952!, 152.7741!)
        Me.Pic_logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'XrLabel21
        '
        Me.XrLabel21.Dpi = 254.0!
        Me.XrLabel21.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Direccion2]")})
        Me.XrLabel21.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(4.333392!, 332.9789!)
        Me.XrLabel21.Multiline = True
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(446.3332!, 100.4066!)
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "XrLabel2"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel20
        '
        Me.XrLabel20.Dpi = 254.0!
        Me.XrLabel20.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Direccion]")})
        Me.XrLabel20.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(4.333392!, 232.5724!)
        Me.XrLabel20.Multiline = True
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(446.3332!, 100.4066!)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "XrLabel2"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel19
        '
        Me.XrLabel19.Dpi = 254.0!
        Me.XrLabel19.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Rfc]")})
        Me.XrLabel19.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(4.333392!, 192.6732!)
        Me.XrLabel19.Multiline = True
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(446.3332!, 39.89917!)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "XrLabel2"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel18
        '
        Me.XrLabel18.Dpi = 254.0!
        Me.XrLabel18.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Empresa]")})
        Me.XrLabel18.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(4.333392!, 152.774!)
        Me.XrLabel18.Multiline = True
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(446.3332!, 39.89919!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "XrLabel2"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 254.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(4.333392!, 566.425!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(446.3331!, 25.16235!)
        '
        'XrLabel4
        '
        Me.XrLabel4.CanGrow = False
        Me.XrLabel4.Dpi = 254.0!
        Me.XrLabel4.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Fecha]")})
        Me.XrLabel4.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 6.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(292.9781!, 437.0789!)
        Me.XrLabel4.Multiline = True
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(157.6883!, 42.54504!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "XrLabel4"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrLabel4.TextFormatString = "{0:d}"
        '
        'XrLabel3
        '
        Me.XrLabel3.CanGrow = False
        Me.XrLabel3.Dpi = 254.0!
        Me.XrLabel3.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Id_sucursal]")})
        Me.XrLabel3.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(4.333392!, 437.0788!)
        Me.XrLabel3.Multiline = True
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(194.1041!, 42.54999!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "XrLabel3"
        '
        'XrLabel1
        '
        Me.XrLabel1.CanGrow = False
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Autonumsuc]")})
        Me.XrLabel1.Font = New DevExpress.Drawing.DXFont("Arial Narrow", 6.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(199.1447!, 523.875!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(251.5219!, 42.54999!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "XrLabel1"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'DetailReport
        '
        Me.DetailReport.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail1, Me.GroupFooter1})
        Me.DetailReport.DataMember = "Detalle"
        Me.DetailReport.DataSource = Me.ods
        Me.DetailReport.Dpi = 254.0!
        Me.DetailReport.Level = 0
        Me.DetailReport.Name = "DetailReport"
        '
        'Detail1
        '
        Me.Detail1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel30, Me.XrLabel11, Me.lbl_DesctoEtiqArt, Me.lbl_DesctoArt, Me.XrLabel14, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5})
        Me.Detail1.Dpi = 254.0!
        Me.Detail1.HeightF = 79.0!
        Me.Detail1.HierarchyPrintOptions.Indent = 50.8!
        Me.Detail1.Name = "Detail1"
        '
        'XrLabel30
        '
        Me.XrLabel30.Dpi = 254.0!
        Me.XrLabel30.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(19.64539!, 53.0!)
        Me.XrLabel30.Multiline = True
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(54.50426!, 26.0!)
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.Text = "D.E."
        '
        'XrLabel11
        '
        Me.XrLabel11.Dpi = 254.0!
        Me.XrLabel11.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[DE_Otorgado]")})
        Me.XrLabel11.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(98.1877!, 53.0!)
        Me.XrLabel11.Multiline = True
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(121.1586!, 26.0!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.Text = "XrLabel8"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel11.TextFormatString = "{0:c2}"
        '
        'lbl_DesctoEtiqArt
        '
        Me.lbl_DesctoEtiqArt.CanGrow = False
        Me.lbl_DesctoEtiqArt.Dpi = 254.0!
        Me.lbl_DesctoEtiqArt.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Visible", "Iif([Tipo]=='DESPUNTOS' Or [Tipo]=='DESCAMBIO' Or [Tipo]=='DESPAGO',false,true)")})
        Me.lbl_DesctoEtiqArt.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_DesctoEtiqArt.LocationFloat = New DevExpress.Utils.PointFloat(34.69097!, 27.0!)
        Me.lbl_DesctoEtiqArt.Multiline = True
        Me.lbl_DesctoEtiqArt.Name = "lbl_DesctoEtiqArt"
        Me.lbl_DesctoEtiqArt.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_DesctoEtiqArt.SizeF = New System.Drawing.SizeF(49.97556!, 26.0!)
        Me.lbl_DesctoEtiqArt.StylePriority.UseFont = False
        Me.lbl_DesctoEtiqArt.Text = "%Descto"
        Me.lbl_DesctoEtiqArt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'lbl_DesctoArt
        '
        Me.lbl_DesctoArt.Dpi = 254.0!
        Me.lbl_DesctoArt.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Visible", "Iif([Tipo]=='DESPUNTOS' Or [Tipo]=='DESCAMBIO' Or [Tipo]=='DESPAGO',false,true)"), New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Descuento]")})
        Me.lbl_DesctoArt.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_DesctoArt.LocationFloat = New DevExpress.Utils.PointFloat(0!, 27.00001!)
        Me.lbl_DesctoArt.Multiline = True
        Me.lbl_DesctoArt.Name = "lbl_DesctoArt"
        Me.lbl_DesctoArt.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_DesctoArt.SizeF = New System.Drawing.SizeF(34.69098!, 26.0!)
        Me.lbl_DesctoArt.StylePriority.UseFont = False
        Me.lbl_DesctoArt.Text = "XrLabel8"
        Me.lbl_DesctoArt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.lbl_DesctoArt.TextFormatString = "{0:n2}"
        '
        'XrLabel14
        '
        Me.XrLabel14.Dpi = 254.0!
        Me.XrLabel14.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Visible", "Iif([Tipo]=='DESPUNTOS' Or [Tipo]=='DESCAMBIO' Or [Tipo]=='DESPAGO',false,true)")})
        Me.XrLabel14.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(165.7842!, 27.00004!)
        Me.XrLabel14.Multiline = True
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(42.54!, 26.0!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "X"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel9
        '
        Me.XrLabel9.Dpi = 254.0!
        Me.XrLabel9.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Total]"), New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Visible", "Iif([Tipo]=='DESPUNTOS' Or [Tipo]=='DESCAMBIO' Or [Tipo]=='DESPAGO',false,true)")})
        Me.XrLabel9.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(329.5078!, 27.00004!)
        Me.XrLabel9.Multiline = True
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(121.1586!, 26.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.Text = "XrLabel9"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel9.TextFormatString = "{0:c2}"
        '
        'XrLabel8
        '
        Me.XrLabel8.Dpi = 254.0!
        Me.XrLabel8.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Preciofinal]")})
        Me.XrLabel8.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(208.3492!, 27.00004!)
        Me.XrLabel8.Multiline = True
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(121.1586!, 26.0!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.Text = "XrLabel8"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel8.TextFormatString = "{0:c2}"
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 254.0!
        Me.XrLabel7.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Cantidad]"), New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Visible", "Iif([Tipo]=='DESPUNTOS' Or [Tipo]=='DESCAMBIO' Or [Tipo]=='DESPAGO',false,true)")})
        Me.XrLabel7.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(84.66652!, 27.00001!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(81.11766!, 26.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.Text = "XrLabel7"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel7.TextFormatString = "{0:n2}"
        '
        'XrLabel6
        '
        Me.XrLabel6.Dpi = 254.0!
        Me.XrLabel6.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Nombre]")})
        Me.XrLabel6.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(103.1877!, 0!)
        Me.XrLabel6.Multiline = True
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(347.4788!, 26.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.Text = "XrLabel6"
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 254.0!
        Me.XrLabel5.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Articulo]")})
        Me.XrLabel5.Font = New DevExpress.Drawing.DXFont("Microsoft Sans Serif", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0.4000079!)
        Me.XrLabel5.Multiline = True
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(103.1876!, 26.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.Text = "XrLabel5"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel31, Me.XrLabel32, Me.lbl_PieDesctoVal, Me.lbl_PieDesctoEtiq, Me.XrLabel28, Me.XrLabel29, Me.xlbl_despedida, Me.XrLabel12, Me.XrLabel10})
        Me.GroupFooter1.Dpi = 254.0!
        Me.GroupFooter1.HeightF = 215.9515!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'lbl_PieDesctoVal
        '
        Me.lbl_PieDesctoVal.Dpi = 254.0!
        Me.lbl_PieDesctoVal.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Descuento]")})
        Me.lbl_PieDesctoVal.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_PieDesctoVal.LocationFloat = New DevExpress.Utils.PointFloat(199.1447!, 127.6349!)
        Me.lbl_PieDesctoVal.Multiline = True
        Me.lbl_PieDesctoVal.Name = "lbl_PieDesctoVal"
        Me.lbl_PieDesctoVal.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_PieDesctoVal.SizeF = New System.Drawing.SizeF(251.5217!, 42.54499!)
        Me.lbl_PieDesctoVal.StylePriority.UseFont = False
        Me.lbl_PieDesctoVal.StylePriority.UseTextAlignment = False
        Me.lbl_PieDesctoVal.Text = "$00.00"
        Me.lbl_PieDesctoVal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_PieDesctoEtiq
        '
        Me.lbl_PieDesctoEtiq.Dpi = 254.0!
        Me.lbl_PieDesctoEtiq.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_PieDesctoEtiq.LocationFloat = New DevExpress.Utils.PointFloat(0!, 127.6349!)
        Me.lbl_PieDesctoEtiq.Multiline = True
        Me.lbl_PieDesctoEtiq.Name = "lbl_PieDesctoEtiq"
        Me.lbl_PieDesctoEtiq.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_PieDesctoEtiq.SizeF = New System.Drawing.SizeF(198.4372!, 42.54499!)
        Me.lbl_PieDesctoEtiq.StylePriority.UseFont = False
        Me.lbl_PieDesctoEtiq.StylePriority.UseTextAlignment = False
        Me.lbl_PieDesctoEtiq.Text = "Usted ahorra:"
        Me.lbl_PieDesctoEtiq.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel28
        '
        Me.XrLabel28.Dpi = 254.0!
        Me.XrLabel28.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([Impiva])")})
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(198.4372!, 42.54499!)
        Me.XrLabel28.Multiline = True
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(252.2293!, 42.54496!)
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel28.Summary = XrSummary2
        Me.XrLabel28.Text = "XrLabel9"
        Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel28.TextFormatString = "{0:c2}"
        '
        'XrLabel29
        '
        Me.XrLabel29.Dpi = 254.0!
        Me.XrLabel29.Font = New DevExpress.Drawing.DXFont("Arial", 9.75!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(19.64539!, 42.545!)
        Me.XrLabel29.Multiline = True
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(178.7921!, 42.54497!)
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.Text = "Impuesto"
        '
        'xlbl_despedida
        '
        Me.xlbl_despedida.Dpi = 254.0!
        Me.xlbl_despedida.Font = New DevExpress.Drawing.DXFont("Arial", 9.75!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.xlbl_despedida.LocationFloat = New DevExpress.Utils.PointFloat(12.17188!, 173.4065!)
        Me.xlbl_despedida.Multiline = True
        Me.xlbl_despedida.Name = "xlbl_despedida"
        Me.xlbl_despedida.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.xlbl_despedida.SizeF = New System.Drawing.SizeF(438.4949!, 42.545!)
        Me.xlbl_despedida.StylePriority.UseFont = False
        Me.xlbl_despedida.StylePriority.UseTextAlignment = False
        Me.xlbl_despedida.Text = "¡Gracias! Por su compra."
        Me.xlbl_despedida.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel12
        '
        Me.XrLabel12.Dpi = 254.0!
        Me.XrLabel12.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([Total])")})
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(198.4372!, 85.08995!)
        Me.XrLabel12.Multiline = True
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(252.2293!, 42.54496!)
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel12.Summary = XrSummary3
        Me.XrLabel12.Text = "XrLabel9"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel12.TextFormatString = "{0:c2}"
        '
        'XrLabel10
        '
        Me.XrLabel10.Dpi = 254.0!
        Me.XrLabel10.Font = New DevExpress.Drawing.DXFont("Arial", 9.75!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(84.66653!, 85.08995!)
        Me.XrLabel10.Multiline = True
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(113.7708!, 42.54497!)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.Text = "Total"
        '
        'ods
        '
        Me.ods.Constructor = ObjectConstructorInfo1
        Me.ods.DataSource = GetType(proyVOficina_PDV.entTicketVenta)
        Me.ods.Name = "ods"
        '
        'XrLabel13
        '
        Me.XrLabel13.BackColor = System.Drawing.Color.Black
        Me.XrLabel13.Dpi = 254.0!
        Me.XrLabel13.Font = New DevExpress.Drawing.DXFont("Arial", 9.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel13.ForeColor = System.Drawing.Color.White
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 0!)
        Me.XrLabel13.Multiline = True
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(438.4949!, 42.54496!)
        Me.XrLabel13.StylePriority.UseBackColor = False
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseForeColor = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Programa de Recompensas"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel16
        '
        Me.XrLabel16.CanGrow = False
        Me.XrLabel16.Dpi = 254.0!
        Me.XrLabel16.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Lealtad_nombre]")})
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 52.91668!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(438.4949!, 42.54!)
        Me.XrLabel16.Text = "XrLabel1"
        Me.XrLabel16.WordWrap = False
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.txt_PuntosAct, Me.lbl_PuntosAct, Me.lbl_PuntosUtilizados, Me.txt_PuntosUtilizados, Me.txt_PuntosAcumulados, Me.lbl_PuntosAcumulados, Me.lbl_PuntosAnt, Me.txt_PuntosAnt, Me.lbl_TitPuntos, Me.XrLabel16, Me.XrLabel13})
        Me.GroupFooter2.Dpi = 254.0!
        Me.GroupFooter2.HeightF = 352.7819!
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'txt_PuntosAct
        '
        Me.txt_PuntosAct.CanGrow = False
        Me.txt_PuntosAct.Dpi = 254.0!
        Me.txt_PuntosAct.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Puntos_saldo_act]")})
        Me.txt_PuntosAct.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 310.2419!)
        Me.txt_PuntosAct.Name = "txt_PuntosAct"
        Me.txt_PuntosAct.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_PuntosAct.SizeF = New System.Drawing.SizeF(225.7707!, 42.53998!)
        Me.txt_PuntosAct.Text = "XrLabel1"
        Me.txt_PuntosAct.TextFormatString = "{0:n0}"
        Me.txt_PuntosAct.WordWrap = False
        '
        'lbl_PuntosAct
        '
        Me.lbl_PuntosAct.Dpi = 254.0!
        Me.lbl_PuntosAct.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_PuntosAct.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 310.2369!)
        Me.lbl_PuntosAct.Multiline = True
        Me.lbl_PuntosAct.Name = "lbl_PuntosAct"
        Me.lbl_PuntosAct.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_PuntosAct.SizeF = New System.Drawing.SizeF(212.7243!, 42.54498!)
        Me.lbl_PuntosAct.StylePriority.UseFont = False
        Me.lbl_PuntosAct.Text = "Saldo Actual"
        '
        'lbl_PuntosUtilizados
        '
        Me.lbl_PuntosUtilizados.Dpi = 254.0!
        Me.lbl_PuntosUtilizados.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_PuntosUtilizados.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 258.6919!)
        Me.lbl_PuntosUtilizados.Multiline = True
        Me.lbl_PuntosUtilizados.Name = "lbl_PuntosUtilizados"
        Me.lbl_PuntosUtilizados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_PuntosUtilizados.SizeF = New System.Drawing.SizeF(212.7243!, 42.54498!)
        Me.lbl_PuntosUtilizados.StylePriority.UseFont = False
        Me.lbl_PuntosUtilizados.Text = "Utilizados"
        '
        'txt_PuntosUtilizados
        '
        Me.txt_PuntosUtilizados.CanGrow = False
        Me.txt_PuntosUtilizados.Dpi = 254.0!
        Me.txt_PuntosUtilizados.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Puntos_utilizados]")})
        Me.txt_PuntosUtilizados.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 258.6969!)
        Me.txt_PuntosUtilizados.Name = "txt_PuntosUtilizados"
        Me.txt_PuntosUtilizados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_PuntosUtilizados.SizeF = New System.Drawing.SizeF(225.7707!, 42.53998!)
        Me.txt_PuntosUtilizados.Text = "XrLabel1"
        Me.txt_PuntosUtilizados.TextFormatString = "{0:n0}"
        Me.txt_PuntosUtilizados.WordWrap = False
        '
        'txt_PuntosAcumulados
        '
        Me.txt_PuntosAcumulados.CanGrow = False
        Me.txt_PuntosAcumulados.Dpi = 254.0!
        Me.txt_PuntosAcumulados.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Puntos_acumulados]")})
        Me.txt_PuntosAcumulados.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 209.1568!)
        Me.txt_PuntosAcumulados.Name = "txt_PuntosAcumulados"
        Me.txt_PuntosAcumulados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_PuntosAcumulados.SizeF = New System.Drawing.SizeF(225.7707!, 42.54001!)
        Me.txt_PuntosAcumulados.Text = "XrLabel1"
        Me.txt_PuntosAcumulados.TextFormatString = "{0:n0}"
        Me.txt_PuntosAcumulados.WordWrap = False
        '
        'lbl_PuntosAcumulados
        '
        Me.lbl_PuntosAcumulados.Dpi = 254.0!
        Me.lbl_PuntosAcumulados.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_PuntosAcumulados.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 209.1518!)
        Me.lbl_PuntosAcumulados.Multiline = True
        Me.lbl_PuntosAcumulados.Name = "lbl_PuntosAcumulados"
        Me.lbl_PuntosAcumulados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_PuntosAcumulados.SizeF = New System.Drawing.SizeF(212.7243!, 42.54497!)
        Me.lbl_PuntosAcumulados.StylePriority.UseFont = False
        Me.lbl_PuntosAcumulados.Text = "Acumulados"
        '
        'lbl_PuntosAnt
        '
        Me.lbl_PuntosAnt.Dpi = 254.0!
        Me.lbl_PuntosAnt.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_PuntosAnt.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 162.6477!)
        Me.lbl_PuntosAnt.Multiline = True
        Me.lbl_PuntosAnt.Name = "lbl_PuntosAnt"
        Me.lbl_PuntosAnt.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_PuntosAnt.SizeF = New System.Drawing.SizeF(212.7243!, 42.54497!)
        Me.lbl_PuntosAnt.StylePriority.UseFont = False
        Me.lbl_PuntosAnt.Text = "Saldo Anterior"
        '
        'txt_PuntosAnt
        '
        Me.txt_PuntosAnt.CanGrow = False
        Me.txt_PuntosAnt.Dpi = 254.0!
        Me.txt_PuntosAnt.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Puntos_saldo_ant]")})
        Me.txt_PuntosAnt.LocationFloat = New DevExpress.Utils.PointFloat(224.8958!, 162.6525!)
        Me.txt_PuntosAnt.Name = "txt_PuntosAnt"
        Me.txt_PuntosAnt.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_PuntosAnt.SizeF = New System.Drawing.SizeF(225.7707!, 42.53999!)
        Me.txt_PuntosAnt.Text = "XrLabel1"
        Me.txt_PuntosAnt.TextFormatString = "{0:n0}"
        Me.txt_PuntosAnt.WordWrap = False
        '
        'lbl_TitPuntos
        '
        Me.lbl_TitPuntos.BackColor = System.Drawing.Color.Black
        Me.lbl_TitPuntos.Dpi = 254.0!
        Me.lbl_TitPuntos.Font = New DevExpress.Drawing.DXFont("Arial", 9.75!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_TitPuntos.ForeColor = System.Drawing.Color.White
        Me.lbl_TitPuntos.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 106.0399!)
        Me.lbl_TitPuntos.Multiline = True
        Me.lbl_TitPuntos.Name = "lbl_TitPuntos"
        Me.lbl_TitPuntos.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_TitPuntos.SizeF = New System.Drawing.SizeF(438.4949!, 42.54498!)
        Me.lbl_TitPuntos.StylePriority.UseBackColor = False
        Me.lbl_TitPuntos.StylePriority.UseFont = False
        Me.lbl_TitPuntos.StylePriority.UseForeColor = False
        Me.lbl_TitPuntos.StylePriority.UseTextAlignment = False
        Me.lbl_TitPuntos.Text = "Puntos"
        Me.lbl_TitPuntos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_TitDinEle
        '
        Me.lbl_TitDinEle.BackColor = System.Drawing.Color.Black
        Me.lbl_TitDinEle.Dpi = 254.0!
        Me.lbl_TitDinEle.Font = New DevExpress.Drawing.DXFont("Arial", 9.75!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_TitDinEle.ForeColor = System.Drawing.Color.White
        Me.lbl_TitDinEle.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 100.5417!)
        Me.lbl_TitDinEle.Multiline = True
        Me.lbl_TitDinEle.Name = "lbl_TitDinEle"
        Me.lbl_TitDinEle.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_TitDinEle.SizeF = New System.Drawing.SizeF(445.9688!, 42.54498!)
        Me.lbl_TitDinEle.StylePriority.UseBackColor = False
        Me.lbl_TitDinEle.StylePriority.UseFont = False
        Me.lbl_TitDinEle.StylePriority.UseForeColor = False
        Me.lbl_TitDinEle.StylePriority.UseTextAlignment = False
        Me.lbl_TitDinEle.Text = "Dinero Electronico"
        Me.lbl_TitDinEle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'txt_DEAnt
        '
        Me.txt_DEAnt.CanGrow = False
        Me.txt_DEAnt.Dpi = 254.0!
        Me.txt_DEAnt.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[De_saldo_ant]")})
        Me.txt_DEAnt.LocationFloat = New DevExpress.Utils.PointFloat(232.3696!, 161.0824!)
        Me.txt_DEAnt.Name = "txt_DEAnt"
        Me.txt_DEAnt.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_DEAnt.SizeF = New System.Drawing.SizeF(225.7707!, 42.53995!)
        Me.txt_DEAnt.Text = "XrLabel1"
        Me.txt_DEAnt.TextFormatString = "{0:n0}"
        Me.txt_DEAnt.WordWrap = False
        '
        'lbl_DEAnt
        '
        Me.lbl_DEAnt.Dpi = 254.0!
        Me.lbl_DEAnt.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_DEAnt.LocationFloat = New DevExpress.Utils.PointFloat(19.64539!, 161.0824!)
        Me.lbl_DEAnt.Multiline = True
        Me.lbl_DEAnt.Name = "lbl_DEAnt"
        Me.lbl_DEAnt.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_DEAnt.SizeF = New System.Drawing.SizeF(212.7243!, 42.54495!)
        Me.lbl_DEAnt.StylePriority.UseFont = False
        Me.lbl_DEAnt.Text = "Saldo Anterior"
        '
        'lbl_DEAcumulados
        '
        Me.lbl_DEAcumulados.Dpi = 254.0!
        Me.lbl_DEAcumulados.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_DEAcumulados.LocationFloat = New DevExpress.Utils.PointFloat(19.64539!, 207.5863!)
        Me.lbl_DEAcumulados.Multiline = True
        Me.lbl_DEAcumulados.Name = "lbl_DEAcumulados"
        Me.lbl_DEAcumulados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_DEAcumulados.SizeF = New System.Drawing.SizeF(212.7243!, 42.54504!)
        Me.lbl_DEAcumulados.StylePriority.UseFont = False
        Me.lbl_DEAcumulados.Text = "Acumulados"
        '
        'txt_DEAcumulados
        '
        Me.txt_DEAcumulados.CanGrow = False
        Me.txt_DEAcumulados.Dpi = 254.0!
        Me.txt_DEAcumulados.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[De_acumulados]")})
        Me.txt_DEAcumulados.LocationFloat = New DevExpress.Utils.PointFloat(232.3696!, 207.5863!)
        Me.txt_DEAcumulados.Name = "txt_DEAcumulados"
        Me.txt_DEAcumulados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_DEAcumulados.SizeF = New System.Drawing.SizeF(225.7707!, 42.53998!)
        Me.txt_DEAcumulados.Text = "XrLabel1"
        Me.txt_DEAcumulados.WordWrap = False
        '
        'txt_DEUtilizados
        '
        Me.txt_DEUtilizados.CanGrow = False
        Me.txt_DEUtilizados.Dpi = 254.0!
        Me.txt_DEUtilizados.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[De_utilizados]")})
        Me.txt_DEUtilizados.LocationFloat = New DevExpress.Utils.PointFloat(232.3696!, 257.1263!)
        Me.txt_DEUtilizados.Name = "txt_DEUtilizados"
        Me.txt_DEUtilizados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_DEUtilizados.SizeF = New System.Drawing.SizeF(225.7707!, 42.54004!)
        Me.txt_DEUtilizados.Text = "XrLabel1"
        Me.txt_DEUtilizados.TextFormatString = "{0:n0}"
        Me.txt_DEUtilizados.WordWrap = False
        '
        'lbl_DEUtilizados
        '
        Me.lbl_DEUtilizados.Dpi = 254.0!
        Me.lbl_DEUtilizados.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_DEUtilizados.LocationFloat = New DevExpress.Utils.PointFloat(19.64539!, 257.1263!)
        Me.lbl_DEUtilizados.Multiline = True
        Me.lbl_DEUtilizados.Name = "lbl_DEUtilizados"
        Me.lbl_DEUtilizados.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_DEUtilizados.SizeF = New System.Drawing.SizeF(212.7243!, 42.54504!)
        Me.lbl_DEUtilizados.StylePriority.UseFont = False
        Me.lbl_DEUtilizados.Text = "Utilizados"
        '
        'lbl_DEAct
        '
        Me.lbl_DEAct.Dpi = 254.0!
        Me.lbl_DEAct.Font = New DevExpress.Drawing.DXFont("Arial", 8.25!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_DEAct.LocationFloat = New DevExpress.Utils.PointFloat(19.64539!, 308.6716!)
        Me.lbl_DEAct.Multiline = True
        Me.lbl_DEAct.Name = "lbl_DEAct"
        Me.lbl_DEAct.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lbl_DEAct.SizeF = New System.Drawing.SizeF(212.7243!, 42.54498!)
        Me.lbl_DEAct.StylePriority.UseFont = False
        Me.lbl_DEAct.Text = "Saldo Actual"
        '
        'txt_DEAct
        '
        Me.txt_DEAct.CanGrow = False
        Me.txt_DEAct.Dpi = 254.0!
        Me.txt_DEAct.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[De_saldo_act]")})
        Me.txt_DEAct.LocationFloat = New DevExpress.Utils.PointFloat(232.3696!, 308.6716!)
        Me.txt_DEAct.Name = "txt_DEAct"
        Me.txt_DEAct.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.txt_DEAct.SizeF = New System.Drawing.SizeF(225.7707!, 42.53998!)
        Me.txt_DEAct.Text = "XrLabel1"
        Me.txt_DEAct.TextFormatString = "{0:n0}"
        Me.txt_DEAct.WordWrap = False
        '
        'GroupFooter3
        '
        Me.GroupFooter3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel15, Me.XrLabel17, Me.txt_DEAct, Me.txt_DEAnt, Me.lbl_DEAnt, Me.lbl_DEAcumulados, Me.txt_DEAcumulados, Me.txt_DEUtilizados, Me.lbl_DEUtilizados, Me.lbl_DEAct, Me.lbl_TitDinEle})
        Me.GroupFooter3.Dpi = 254.0!
        Me.GroupFooter3.HeightF = 351.2166!
        Me.GroupFooter3.Level = 1
        Me.GroupFooter3.Name = "GroupFooter3"
        '
        'XrLabel15
        '
        Me.XrLabel15.BackColor = System.Drawing.Color.Black
        Me.XrLabel15.Dpi = 254.0!
        Me.XrLabel15.Font = New DevExpress.Drawing.DXFont("Arial", 9.0!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel15.ForeColor = System.Drawing.Color.White
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 0!)
        Me.XrLabel15.Multiline = True
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(445.9688!, 42.54496!)
        Me.XrLabel15.StylePriority.UseBackColor = False
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseForeColor = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "Programa de Recompensas"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel17
        '
        Me.XrLabel17.CanGrow = False
        Me.XrLabel17.Dpi = 254.0!
        Me.XrLabel17.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Lealtad_nombre]")})
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(12.17158!, 52.91667!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(445.9688!, 42.54!)
        Me.XrLabel17.Text = "XrLabel1"
        Me.XrLabel17.WordWrap = False
        '
        'cal_Subtotal
        '
        Me.cal_Subtotal.DataMember = "Detalle"
        Me.cal_Subtotal.Expression = "[Total] - [Impiva]"
        Me.cal_Subtotal.Name = "cal_Subtotal"
        '
        'XrLabel31
        '
        Me.XrLabel31.Dpi = 254.0!
        Me.XrLabel31.Font = New DevExpress.Drawing.DXFont("Arial", 9.75!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(19.64539!, 0.00001525879!)
        Me.XrLabel31.Multiline = True
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel31.SizeF = New System.Drawing.SizeF(178.7921!, 42.54497!)
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.Text = "Subtotal"
        '
        'XrLabel32
        '
        Me.XrLabel32.Dpi = 254.0!
        Me.XrLabel32.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([cal_Subtotal])")})
        Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(198.4372!, 0!)
        Me.XrLabel32.Multiline = True
        Me.XrLabel32.Name = "XrLabel32"
        Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel32.SizeF = New System.Drawing.SizeF(252.2293!, 42.54496!)
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel32.Summary = XrSummary1
        Me.XrLabel32.Text = "XrLabel9"
        Me.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel32.TextFormatString = "{0:c2}"
        '
        'xtraRepTicketVenta_Seafon
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMargin, Me.BottomMargin, Me.Detail, Me.DetailReport, Me.GroupFooter2, Me.GroupFooter3})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.cal_Subtotal})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.ods})
        Me.DataSource = Me.ods
        Me.Dpi = 254.0!
        Me.Font = New DevExpress.Drawing.DXFont("Arial", 9.75!)
        Me.Margins = New DevExpress.Drawing.DXMargins(0!, 18.0!, 30.0!, 25.0!)
        Me.PageHeight = 2794
        Me.PageWidth = 481
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.SnapGridSize = 25.0!
        Me.Version = "22.2"
        CType(Me.ods, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents ods As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DetailReport As DevExpress.XtraReports.UI.DetailReportBand
    Friend WithEvents Detail1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents txt_PuntosAct As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_PuntosAct As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_PuntosUtilizados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents txt_PuntosUtilizados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents txt_PuntosAcumulados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_PuntosAcumulados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_PuntosAnt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents txt_PuntosAnt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_TitPuntos As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_TitDinEle As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents txt_DEAnt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_DEAnt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_DEAcumulados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents txt_DEAcumulados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents txt_DEUtilizados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_DEUtilizados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_DEAct As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents txt_DEAct As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xlbl_despedida As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter3 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Pic_logo As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_Descto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_DesctoArt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_PieDesctoVal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_PieDesctoEtiq As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_DesctoEtiqArt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents cal_Subtotal As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel32 As DevExpress.XtraReports.UI.XRLabel
End Class
