﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class XtraRep_FacturaBase
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim QrCodeGenerator1 As DevExpress.XtraPrinting.BarCode.QRCodeGenerator = New DevExpress.XtraPrinting.BarCode.QRCodeGenerator()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraRep_FacturaBase))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.lbl_TotCtoNeto = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_UniCtoIVA = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_UniCtoNeto = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_UniCtoBruto = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_IdRenglon = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.ReportHeaderBand1 = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_EmpresaDireccion = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPic_Logo = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.lbl_ECliente = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_Proveedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_IdProveedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_Empresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_EUniCtoIVA = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_ETotCtoNeto = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_EUniCtoNeto = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_EUniCtoBruto = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_EMarca = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_EIdRen = New DevExpress.XtraReports.UI.XRLabel()
        Me.Title = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DetailCaption1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DetailData1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DetailCaption3 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DetailData3 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DetailData3_Odd = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DetailCaptionBackground3 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageInfo = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DetailReport = New DevExpress.XtraReports.UI.DetailReportBand()
        Me.Detail1 = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_Marca = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrBarCode1 = New DevExpress.XtraReports.UI.XRBarCode()
        Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel43 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel44 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_IVA = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_EIVA = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_ETotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_TOTAL = New DevExpress.XtraReports.UI.XRLabel()
        Me.lbl_ESubtotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.odsDatosFacturas = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource(Me.components)
        Me.RenglonImpar = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.cal_Total_IVA = New DevExpress.XtraReports.UI.CalculatedField()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.odsDatosFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 9.75!)
        Me.Detail.HeightF = 0.4114532!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseFont = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'lbl_TotCtoNeto
        '
        Me.lbl_TotCtoNeto.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Detalle.Subtotal", "{0:c2}")})
        Me.lbl_TotCtoNeto.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.lbl_TotCtoNeto.Name = "lbl_TotCtoNeto"
        Me.lbl_TotCtoNeto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_TotCtoNeto.SizeF = New System.Drawing.SizeF(99.49982!, 23.0!)
        Me.lbl_TotCtoNeto.StylePriority.UseTextAlignment = False
        Me.lbl_TotCtoNeto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'lbl_UniCtoIVA
        '
        Me.lbl_UniCtoIVA.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Detalle.Precioventasiva", "{0:c2}")})
        Me.lbl_UniCtoIVA.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.lbl_UniCtoIVA.Name = "lbl_UniCtoIVA"
        Me.lbl_UniCtoIVA.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_UniCtoIVA.SizeF = New System.Drawing.SizeF(75.83334!, 23.0!)
        Me.lbl_UniCtoIVA.StylePriority.UseTextAlignment = False
        Me.lbl_UniCtoIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'lbl_UniCtoNeto
        '
        Me.lbl_UniCtoNeto.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Detalle.Cantidad")})
        Me.lbl_UniCtoNeto.LocationFloat = New DevExpress.Utils.PointFloat(3.000081!, 0!)
        Me.lbl_UniCtoNeto.Name = "lbl_UniCtoNeto"
        Me.lbl_UniCtoNeto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_UniCtoNeto.SizeF = New System.Drawing.SizeF(72.00002!, 23.0!)
        Me.lbl_UniCtoNeto.StylePriority.UseTextAlignment = False
        Me.lbl_UniCtoNeto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_UniCtoNeto.TextFormatString = "{0:n0}"
        '
        'lbl_UniCtoBruto
        '
        Me.lbl_UniCtoBruto.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Detalle.Articulo", "{0:c2}")})
        Me.lbl_UniCtoBruto.LocationFloat = New DevExpress.Utils.PointFloat(3.166504!, 0!)
        Me.lbl_UniCtoBruto.Name = "lbl_UniCtoBruto"
        Me.lbl_UniCtoBruto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_UniCtoBruto.SizeF = New System.Drawing.SizeF(70.83344!, 23.0!)
        Me.lbl_UniCtoBruto.StylePriority.UseTextAlignment = False
        Me.lbl_UniCtoBruto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'lbl_IdRenglon
        '
        Me.lbl_IdRenglon.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Detalle.Renglon1")})
        Me.lbl_IdRenglon.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.lbl_IdRenglon.Name = "lbl_IdRenglon"
        Me.lbl_IdRenglon.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_IdRenglon.SizeF = New System.Drawing.SizeF(31.19634!, 23.0!)
        Me.lbl_IdRenglon.StylePriority.UseTextAlignment = False
        Me.lbl_IdRenglon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 9.75!)
        Me.TopMarginBand1.HeightF = 28.00001!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        Me.TopMarginBand1.StylePriority.UseFont = False
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2})
        Me.BottomMarginBand1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 9.75!)
        Me.BottomMarginBand1.HeightF = 33.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.StyleName = "Title"
        Me.BottomMarginBand1.StylePriority.UseFont = False
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(650.0!, 10.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrPageInfo2.StyleName = "PageInfo"
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrPageInfo2.TextFormatString = "Page {0} of {1}"
        '
        'ReportHeaderBand1
        '
        Me.ReportHeaderBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel29, Me.XrLabel23, Me.XrLabel24, Me.XrLabel21, Me.XrLabel22, Me.XrLabel19, Me.XrLabel20, Me.XrLabel18, Me.XrLabel16, Me.XrLabel17, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.lbl_EmpresaDireccion, Me.XrPic_Logo, Me.lbl_ECliente, Me.lbl_Proveedor, Me.lbl_IdProveedor, Me.lbl_Empresa, Me.XrLabel15})
        Me.ReportHeaderBand1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 9.75!)
        Me.ReportHeaderBand1.HeightF = 205.2447!
        Me.ReportHeaderBand1.Name = "ReportHeaderBand1"
        Me.ReportHeaderBand1.StylePriority.UseFont = False
        '
        'XrLabel29
        '
        Me.XrLabel29.CanGrow = False
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(508.4165!, 80.49997!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(35.41675!, 23.0!)
        Me.XrLabel29.StylePriority.UseTextAlignment = False
        Me.XrLabel29.Text = "Dias"
        Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel23
        '
        Me.XrLabel23.CanGrow = False
        Me.XrLabel23.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Plazo")})
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(463.625!, 80.5!)
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(44.79153!, 23.0!)
        '
        'XrLabel24
        '
        Me.XrLabel24.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel24.CanGrow = False
        Me.XrLabel24.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel24.ForeColor = System.Drawing.Color.White
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(398.4902!, 80.5!)
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(60.00006!, 23.0!)
        Me.XrLabel24.StylePriority.UseBackColor = False
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.StylePriority.UseForeColor = False
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.Text = "PLAZO"
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel21
        '
        Me.XrLabel21.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel21.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel21.ForeColor = System.Drawing.Color.White
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(547.8331!, 156.4115!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(197.863!, 23.0!)
        Me.XrLabel21.StylePriority.UseBackColor = False
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseForeColor = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "LUGAR EXPEDICION"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel22
        '
        Me.XrLabel22.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel22.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel22.BorderWidth = 3.0!
        Me.XrLabel22.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel22.ForeColor = System.Drawing.Color.Black
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(547.8331!, 180.2447!)
        Me.XrLabel22.Multiline = True
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(197.863!, 25.0!)
        Me.XrLabel22.StylePriority.UseBorderColor = False
        Me.XrLabel22.StylePriority.UseBorders = False
        Me.XrLabel22.StylePriority.UseBorderWidth = False
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.StylePriority.UseForeColor = False
        Me.XrLabel22.StylePriority.UseTextAlignment = False
        Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel19
        '
        Me.XrLabel19.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel19.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel19.BorderWidth = 3.0!
        Me.XrLabel19.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Fecha")})
        Me.XrLabel19.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel19.ForeColor = System.Drawing.Color.Black
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(547.8331!, 128.3332!)
        Me.XrLabel19.Multiline = True
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(197.863!, 25.0!)
        Me.XrLabel19.StylePriority.UseBorderColor = False
        Me.XrLabel19.StylePriority.UseBorders = False
        Me.XrLabel19.StylePriority.UseBorderWidth = False
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseForeColor = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        Me.XrLabel19.TextFormatString = "{0:dd/MMM/yyyy}"
        '
        'XrLabel20
        '
        Me.XrLabel20.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel20.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel20.ForeColor = System.Drawing.Color.White
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(547.8331!, 104.5!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(197.863!, 23.0!)
        Me.XrLabel20.StylePriority.UseBackColor = False
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseForeColor = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "FECHA"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel18
        '
        Me.XrLabel18.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel18.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel18.BorderWidth = 3.0!
        Me.XrLabel18.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Folio")})
        Me.XrLabel18.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel18.ForeColor = System.Drawing.Color.Black
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(649.4998!, 78.49998!)
        Me.XrLabel18.Multiline = True
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(96.19629!, 25.0!)
        Me.XrLabel18.StylePriority.UseBorderColor = False
        Me.XrLabel18.StylePriority.UseBorders = False
        Me.XrLabel18.StylePriority.UseBorderWidth = False
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseForeColor = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel16
        '
        Me.XrLabel16.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel16.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "TipoDocto1")})
        Me.XrLabel16.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel16.ForeColor = System.Drawing.Color.White
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(548.3333!, 54.66677!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(197.863!, 23.0!)
        Me.XrLabel16.StylePriority.UseBackColor = False
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseForeColor = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel17
        '
        Me.XrLabel17.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel17.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel17.BorderWidth = 3.0!
        Me.XrLabel17.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Observ2")})
        Me.XrLabel17.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel17.ForeColor = System.Drawing.Color.Black
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(548.3333!, 78.49998!)
        Me.XrLabel17.Multiline = True
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(101.1665!, 25.0!)
        Me.XrLabel17.StylePriority.UseBorderColor = False
        Me.XrLabel17.StylePriority.UseBorders = False
        Me.XrLabel17.StylePriority.UseBorderWidth = False
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseForeColor = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel12
        '
        Me.XrLabel12.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel12.CanGrow = False
        Me.XrLabel12.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel12.ForeColor = System.Drawing.Color.White
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(226.0098!, 80.5!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(60.00006!, 23.0!)
        Me.XrLabel12.StylePriority.UseBackColor = False
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseForeColor = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "RFC:"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel11
        '
        Me.XrLabel11.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel11.CanGrow = False
        Me.XrLabel11.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel11.ForeColor = System.Drawing.Color.White
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(21.66667!, 80.49998!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel11.StylePriority.UseBackColor = False
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseForeColor = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "Id Cliente: "
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel10
        '
        Me.XrLabel10.CanGrow = False
        Me.XrLabel10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Rfc")})
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(287.3529!, 80.5!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrLabel9
        '
        Me.XrLabel9.CanGrow = False
        Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Edofac")})
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(448.2084!, 176.2447!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(95.625!, 23.0!)
        '
        'XrLabel8
        '
        Me.XrLabel8.CanGrow = False
        Me.XrLabel8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Ciudadfac")})
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(318.2084!, 176.2447!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(130.0!, 23.0!)
        '
        'XrLabel7
        '
        Me.XrLabel7.CanGrow = False
        Me.XrLabel7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Cpostalfac")})
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(245.5875!, 176.2447!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(71.62088!, 23.0!)
        '
        'XrLabel6
        '
        Me.XrLabel6.CanGrow = False
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Coloniafac")})
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(21.66667!, 176.2447!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(223.9208!, 23.0!)
        '
        'XrLabel5
        '
        Me.XrLabel5.CanGrow = False
        Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Direccionfac")})
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(21.66667!, 131.823!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(522.1667!, 44.42171!)
        '
        'lbl_EmpresaDireccion
        '
        Me.lbl_EmpresaDireccion.CanGrow = False
        Me.lbl_EmpresaDireccion.Font = New DevExpress.Drawing.DXFont("Tahoma", 5.25!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_EmpresaDireccion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_EmpresaDireccion.LocationFloat = New DevExpress.Utils.PointFloat(131.6668!, 16.0!)
        Me.lbl_EmpresaDireccion.Multiline = True
        Me.lbl_EmpresaDireccion.Name = "lbl_EmpresaDireccion"
        Me.lbl_EmpresaDireccion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_EmpresaDireccion.SizeF = New System.Drawing.SizeF(489.512!, 35.67947!)
        Me.lbl_EmpresaDireccion.StyleName = "Title"
        Me.lbl_EmpresaDireccion.StylePriority.UseFont = False
        Me.lbl_EmpresaDireccion.StylePriority.UseForeColor = False
        Me.lbl_EmpresaDireccion.StylePriority.UseTextAlignment = False
        Me.lbl_EmpresaDireccion.Text = "DOMICILIO"
        Me.lbl_EmpresaDireccion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrPic_Logo
        '
        Me.XrPic_Logo.LocationFloat = New DevExpress.Utils.PointFloat(2.99998!, 5.839742!)
        Me.XrPic_Logo.Name = "XrPic_Logo"
        Me.XrPic_Logo.SizeF = New System.Drawing.SizeF(125.0175!, 45.83973!)
        Me.XrPic_Logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'lbl_ECliente
        '
        Me.lbl_ECliente.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_ECliente.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_ECliente.ForeColor = System.Drawing.Color.White
        Me.lbl_ECliente.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 55.5!)
        Me.lbl_ECliente.Name = "lbl_ECliente"
        Me.lbl_ECliente.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_ECliente.SizeF = New System.Drawing.SizeF(535.5001!, 23.0!)
        Me.lbl_ECliente.StylePriority.UseBackColor = False
        Me.lbl_ECliente.StylePriority.UseFont = False
        Me.lbl_ECliente.StylePriority.UseForeColor = False
        Me.lbl_ECliente.StylePriority.UseTextAlignment = False
        Me.lbl_ECliente.Text = "CLIENTE"
        Me.lbl_ECliente.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_Proveedor
        '
        Me.lbl_Proveedor.CanGrow = False
        Me.lbl_Proveedor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Nomfac")})
        Me.lbl_Proveedor.LocationFloat = New DevExpress.Utils.PointFloat(21.66667!, 105.9115!)
        Me.lbl_Proveedor.Name = "lbl_Proveedor"
        Me.lbl_Proveedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_Proveedor.SizeF = New System.Drawing.SizeF(523.8334!, 22.99999!)
        '
        'lbl_IdProveedor
        '
        Me.lbl_IdProveedor.CanGrow = False
        Me.lbl_IdProveedor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Id_cliente")})
        Me.lbl_IdProveedor.LocationFloat = New DevExpress.Utils.PointFloat(124.0098!, 80.5!)
        Me.lbl_IdProveedor.Name = "lbl_IdProveedor"
        Me.lbl_IdProveedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_IdProveedor.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'lbl_Empresa
        '
        Me.lbl_Empresa.CanGrow = False
        Me.lbl_Empresa.Font = New DevExpress.Drawing.DXFont("Tahoma", 12.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_Empresa.ForeColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_Empresa.LocationFloat = New DevExpress.Utils.PointFloat(131.6668!, 0!)
        Me.lbl_Empresa.Name = "lbl_Empresa"
        Me.lbl_Empresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_Empresa.SizeF = New System.Drawing.SizeF(489.512!, 16.0!)
        Me.lbl_Empresa.StyleName = "Title"
        Me.lbl_Empresa.StylePriority.UseFont = False
        Me.lbl_Empresa.StylePriority.UseForeColor = False
        Me.lbl_Empresa.StylePriority.UseTextAlignment = False
        Me.lbl_Empresa.Text = "EMPRESA"
        Me.lbl_Empresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel15
        '
        Me.XrLabel15.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel15.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel15.BorderWidth = 3.0!
        Me.XrLabel15.CanGrow = False
        Me.XrLabel15.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel15.ForeColor = System.Drawing.Color.Black
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 78.49998!)
        Me.XrLabel15.Multiline = True
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(533.8333!, 126.7447!)
        Me.XrLabel15.StylePriority.UseBorderColor = False
        Me.XrLabel15.StylePriority.UseBorders = False
        Me.XrLabel15.StylePriority.UseBorderWidth = False
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseForeColor = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'lbl_EUniCtoIVA
        '
        Me.lbl_EUniCtoIVA.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_EUniCtoIVA.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_EUniCtoIVA.ForeColor = System.Drawing.Color.White
        Me.lbl_EUniCtoIVA.LocationFloat = New DevExpress.Utils.PointFloat(569.5296!, 0!)
        Me.lbl_EUniCtoIVA.Name = "lbl_EUniCtoIVA"
        Me.lbl_EUniCtoIVA.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_EUniCtoIVA.SizeF = New System.Drawing.SizeF(75.0!, 23.0!)
        Me.lbl_EUniCtoIVA.StyleName = "Title"
        Me.lbl_EUniCtoIVA.StylePriority.UseBackColor = False
        Me.lbl_EUniCtoIVA.StylePriority.UseFont = False
        Me.lbl_EUniCtoIVA.StylePriority.UseForeColor = False
        Me.lbl_EUniCtoIVA.StylePriority.UseTextAlignment = False
        Me.lbl_EUniCtoIVA.Text = "PRECIO"
        Me.lbl_EUniCtoIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_ETotCtoNeto
        '
        Me.lbl_ETotCtoNeto.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_ETotCtoNeto.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_ETotCtoNeto.ForeColor = System.Drawing.Color.White
        Me.lbl_ETotCtoNeto.LocationFloat = New DevExpress.Utils.PointFloat(646.1963!, 0!)
        Me.lbl_ETotCtoNeto.Name = "lbl_ETotCtoNeto"
        Me.lbl_ETotCtoNeto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_ETotCtoNeto.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.lbl_ETotCtoNeto.StyleName = "Title"
        Me.lbl_ETotCtoNeto.StylePriority.UseBackColor = False
        Me.lbl_ETotCtoNeto.StylePriority.UseFont = False
        Me.lbl_ETotCtoNeto.StylePriority.UseForeColor = False
        Me.lbl_ETotCtoNeto.StylePriority.UseTextAlignment = False
        Me.lbl_ETotCtoNeto.Text = "IMPORTE"
        Me.lbl_ETotCtoNeto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_EUniCtoNeto
        '
        Me.lbl_EUniCtoNeto.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_EUniCtoNeto.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_EUniCtoNeto.ForeColor = System.Drawing.Color.White
        Me.lbl_EUniCtoNeto.LocationFloat = New DevExpress.Utils.PointFloat(493.6963!, 0!)
        Me.lbl_EUniCtoNeto.Name = "lbl_EUniCtoNeto"
        Me.lbl_EUniCtoNeto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_EUniCtoNeto.SizeF = New System.Drawing.SizeF(75.0!, 23.0!)
        Me.lbl_EUniCtoNeto.StyleName = "Title"
        Me.lbl_EUniCtoNeto.StylePriority.UseBackColor = False
        Me.lbl_EUniCtoNeto.StylePriority.UseFont = False
        Me.lbl_EUniCtoNeto.StylePriority.UseForeColor = False
        Me.lbl_EUniCtoNeto.StylePriority.UseTextAlignment = False
        Me.lbl_EUniCtoNeto.Text = "CANT"
        Me.lbl_EUniCtoNeto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_EUniCtoBruto
        '
        Me.lbl_EUniCtoBruto.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_EUniCtoBruto.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_EUniCtoBruto.ForeColor = System.Drawing.Color.White
        Me.lbl_EUniCtoBruto.LocationFloat = New DevExpress.Utils.PointFloat(416.6963!, 0!)
        Me.lbl_EUniCtoBruto.Name = "lbl_EUniCtoBruto"
        Me.lbl_EUniCtoBruto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_EUniCtoBruto.SizeF = New System.Drawing.SizeF(75.0!, 23.0!)
        Me.lbl_EUniCtoBruto.StyleName = "Title"
        Me.lbl_EUniCtoBruto.StylePriority.UseBackColor = False
        Me.lbl_EUniCtoBruto.StylePriority.UseFont = False
        Me.lbl_EUniCtoBruto.StylePriority.UseForeColor = False
        Me.lbl_EUniCtoBruto.StylePriority.UseTextAlignment = False
        Me.lbl_EUniCtoBruto.Text = "ART"
        Me.lbl_EUniCtoBruto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_EMarca
        '
        Me.lbl_EMarca.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_EMarca.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_EMarca.ForeColor = System.Drawing.Color.White
        Me.lbl_EMarca.LocationFloat = New DevExpress.Utils.PointFloat(35.52963!, 0.00001144409!)
        Me.lbl_EMarca.Name = "lbl_EMarca"
        Me.lbl_EMarca.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_EMarca.SizeF = New System.Drawing.SizeF(380.0!, 23.0!)
        Me.lbl_EMarca.StyleName = "Title"
        Me.lbl_EMarca.StylePriority.UseBackColor = False
        Me.lbl_EMarca.StylePriority.UseFont = False
        Me.lbl_EMarca.StylePriority.UseForeColor = False
        Me.lbl_EMarca.StylePriority.UseTextAlignment = False
        Me.lbl_EMarca.Text = "CONCEPTO"
        Me.lbl_EMarca.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_EIdRen
        '
        Me.lbl_EIdRen.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_EIdRen.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_EIdRen.ForeColor = System.Drawing.Color.White
        Me.lbl_EIdRen.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.lbl_EIdRen.Name = "lbl_EIdRen"
        Me.lbl_EIdRen.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_EIdRen.SizeF = New System.Drawing.SizeF(35.0!, 23.0!)
        Me.lbl_EIdRen.StyleName = "Title"
        Me.lbl_EIdRen.StylePriority.UseBackColor = False
        Me.lbl_EIdRen.StylePriority.UseFont = False
        Me.lbl_EIdRen.StylePriority.UseForeColor = False
        Me.lbl_EIdRen.StylePriority.UseTextAlignment = False
        Me.lbl_EIdRen.Text = "REN"
        Me.lbl_EIdRen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'Title
        '
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.BorderColor = System.Drawing.Color.Black
        Me.Title.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.Title.BorderWidth = 1.0!
        Me.Title.Font = New DevExpress.Drawing.DXFont("Tahoma", 14.0!)
        Me.Title.ForeColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.Title.Name = "Title"
        '
        'DetailCaption1
        '
        Me.DetailCaption1.BackColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.DetailCaption1.BorderColor = System.Drawing.Color.White
        Me.DetailCaption1.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.DetailCaption1.BorderWidth = 2.0!
        Me.DetailCaption1.Font = New DevExpress.Drawing.DXFont("Tahoma", 8.0!, DevExpress.Drawing.DXFontStyle.Bold)
        Me.DetailCaption1.ForeColor = System.Drawing.Color.White
        Me.DetailCaption1.Name = "DetailCaption1"
        Me.DetailCaption1.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.DetailCaption1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'DetailData1
        '
        Me.DetailData1.BackColor = System.Drawing.Color.Transparent
        Me.DetailData1.BorderColor = System.Drawing.Color.Transparent
        Me.DetailData1.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.DetailData1.BorderWidth = 2.0!
        Me.DetailData1.Font = New DevExpress.Drawing.DXFont("Tahoma", 8.0!)
        Me.DetailData1.ForeColor = System.Drawing.Color.Black
        Me.DetailData1.Name = "DetailData1"
        Me.DetailData1.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.DetailData1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'DetailCaption3
        '
        Me.DetailCaption3.BackColor = System.Drawing.Color.Transparent
        Me.DetailCaption3.BorderColor = System.Drawing.Color.Transparent
        Me.DetailCaption3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.DetailCaption3.Font = New DevExpress.Drawing.DXFont("Tahoma", 8.0!, DevExpress.Drawing.DXFontStyle.Bold)
        Me.DetailCaption3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.DetailCaption3.Name = "DetailCaption3"
        Me.DetailCaption3.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.DetailCaption3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'DetailData3
        '
        Me.DetailData3.Font = New DevExpress.Drawing.DXFont("Tahoma", 8.0!)
        Me.DetailData3.ForeColor = System.Drawing.Color.Black
        Me.DetailData3.Name = "DetailData3"
        Me.DetailData3.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.DetailData3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'DetailData3_Odd
        '
        Me.DetailData3_Odd.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.DetailData3_Odd.BorderColor = System.Drawing.Color.Transparent
        Me.DetailData3_Odd.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.DetailData3_Odd.BorderWidth = 1.0!
        Me.DetailData3_Odd.Font = New DevExpress.Drawing.DXFont("Tahoma", 8.0!)
        Me.DetailData3_Odd.ForeColor = System.Drawing.Color.Black
        Me.DetailData3_Odd.Name = "DetailData3_Odd"
        Me.DetailData3_Odd.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.DetailData3_Odd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'DetailCaptionBackground3
        '
        Me.DetailCaptionBackground3.BackColor = System.Drawing.Color.Transparent
        Me.DetailCaptionBackground3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(206, Byte), Integer), CType(CType(206, Byte), Integer))
        Me.DetailCaptionBackground3.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.DetailCaptionBackground3.BorderWidth = 2.0!
        Me.DetailCaptionBackground3.Name = "DetailCaptionBackground3"
        '
        'PageInfo
        '
        Me.PageInfo.Font = New DevExpress.Drawing.DXFont("Tahoma", 8.0!, DevExpress.Drawing.DXFontStyle.Bold)
        Me.PageInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.PageInfo.Name = "PageInfo"
        Me.PageInfo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        '
        'DetailReport
        '
        Me.DetailReport.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail1, Me.GroupFooter1, Me.GroupHeader1})
        Me.DetailReport.DataMember = "Detalle"
        Me.DetailReport.DataSource = Me.odsDatosFacturas
        Me.DetailReport.Level = 0
        Me.DetailReport.Name = "DetailReport"
        '
        'Detail1
        '
        Me.Detail1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 7.8!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.Detail1.HeightF = 20.0!
        Me.Detail1.KeepTogether = True
        Me.Detail1.Name = "Detail1"
        Me.Detail1.StylePriority.UseFont = False
        '
        'XrTable1
        '
        Me.XrTable1.EvenStyleName = "RenglonImpar"
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(750.0!, 20.0!)
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell6, Me.XrTableCell7, Me.XrTableCell8, Me.XrTableCell9})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 0.79999987262223726R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_IdRenglon})
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Text = "XrTableCell1"
        Me.XrTableCell1.Weight = 0.35R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_Marca})
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Text = "XrTableCell2"
        Me.XrTableCell2.Weight = 4.3119332566687207R
        '
        'lbl_Marca
        '
        Me.lbl_Marca.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Detalle.Concepto")})
        Me.lbl_Marca.LocationFloat = New DevExpress.Utils.PointFloat(0.000009536743!, 0!)
        Me.lbl_Marca.Name = "lbl_Marca"
        Me.lbl_Marca.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_Marca.SizeF = New System.Drawing.SizeF(384.3333!, 23.0!)
        Me.lbl_Marca.StylePriority.UseTextAlignment = False
        Me.lbl_Marca.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_UniCtoBruto})
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.Text = "XrTableCell6"
        Me.XrTableCell6.Weight = 0.91964742664771415R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_UniCtoNeto})
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Text = "XrTableCell7"
        Me.XrTableCell7.Weight = 0.84144421746903575R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_UniCtoIVA})
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Text = "XrTableCell8"
        Me.XrTableCell8.Weight = 0.86949150335313941R
        '
        'XrTableCell9
        '
        Me.XrTableCell9.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_TotCtoNeto})
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.Text = "XrTableCell9"
        Me.XrTableCell9.Weight = 1.1219261495004884R
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode1, Me.XrLabel35, Me.XrLabel36, Me.XrLabel33, Me.XrLabel34, Me.XrLabel31, Me.XrLabel32, Me.XrLabel30, Me.XrLabel27, Me.XrLabel28, Me.XrLabel26, Me.XrLabel25, Me.XrLabel13, Me.XrLabel14, Me.XrLabel4, Me.XrLabel3, Me.XrLabel1, Me.XrLabel2, Me.XrLabel43, Me.XrLabel44, Me.lbl_IVA, Me.lbl_EIVA, Me.lbl_ETotal, Me.lbl_TOTAL, Me.lbl_ESubtotal})
        Me.GroupFooter1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.GroupFooter1.HeightF = 416.3375!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.StylePriority.UseFont = False
        '
        'XrBarCode1
        '
        Me.XrBarCode1.AutoModule = True
        Me.XrBarCode1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Codigo_QR")})
        Me.XrBarCode1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.25!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrBarCode1.LocationFloat = New DevExpress.Utils.PointFloat(4.999987!, 0!)
        Me.XrBarCode1.Name = "XrBarCode1"
        Me.XrBarCode1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        Me.XrBarCode1.ShowText = False
        Me.XrBarCode1.SizeF = New System.Drawing.SizeF(157.18!, 157.1763!)
        Me.XrBarCode1.StylePriority.UseFont = False
        QrCodeGenerator1.CompactionMode = DevExpress.XtraPrinting.BarCode.QRCodeCompactionMode.[Byte]
        QrCodeGenerator1.ErrorCorrectionLevel = DevExpress.XtraPrinting.BarCode.QRCodeErrorCorrectionLevel.H
        Me.XrBarCode1.Symbology = QrCodeGenerator1
        '
        'XrLabel35
        '
        Me.XrLabel35.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel35.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel35.ForeColor = System.Drawing.Color.White
        Me.XrLabel35.LocationFloat = New DevExpress.Utils.PointFloat(184.8214!, 106.1732!)
        Me.XrLabel35.Name = "XrLabel35"
        Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel35.SizeF = New System.Drawing.SizeF(361.1248!, 22.99999!)
        Me.XrLabel35.StylePriority.UseBackColor = False
        Me.XrLabel35.StylePriority.UseFont = False
        Me.XrLabel35.StylePriority.UseForeColor = False
        Me.XrLabel35.StylePriority.UseTextAlignment = False
        Me.XrLabel35.Text = "CORREOS"
        Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel36
        '
        Me.XrLabel36.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel36.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel36.BorderWidth = 3.0!
        Me.XrLabel36.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Observ1")})
        Me.XrLabel36.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 9.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel36.ForeColor = System.Drawing.Color.Black
        Me.XrLabel36.LocationFloat = New DevExpress.Utils.PointFloat(184.8214!, 130.0063!)
        Me.XrLabel36.Multiline = True
        Me.XrLabel36.Name = "XrLabel36"
        Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel36.SizeF = New System.Drawing.SizeF(361.1248!, 27.17!)
        Me.XrLabel36.StylePriority.UseBorderColor = False
        Me.XrLabel36.StylePriority.UseBorders = False
        Me.XrLabel36.StylePriority.UseBorderWidth = False
        Me.XrLabel36.StylePriority.UseFont = False
        Me.XrLabel36.StylePriority.UseForeColor = False
        Me.XrLabel36.StylePriority.UseTextAlignment = False
        Me.XrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel33
        '
        Me.XrLabel33.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel33.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel33.BorderWidth = 3.0!
        Me.XrLabel33.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FacFormaPago")})
        Me.XrLabel33.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 9.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel33.ForeColor = System.Drawing.Color.Black
        Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(184.0417!, 78.00318!)
        Me.XrLabel33.Multiline = True
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.SizeF = New System.Drawing.SizeF(361.1247!, 27.17!)
        Me.XrLabel33.StylePriority.UseBorderColor = False
        Me.XrLabel33.StylePriority.UseBorders = False
        Me.XrLabel33.StylePriority.UseBorderWidth = False
        Me.XrLabel33.StylePriority.UseFont = False
        Me.XrLabel33.StylePriority.UseForeColor = False
        Me.XrLabel33.StylePriority.UseTextAlignment = False
        Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel34
        '
        Me.XrLabel34.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel34.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel34.ForeColor = System.Drawing.Color.White
        Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(184.0417!, 54.16997!)
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.SizeF = New System.Drawing.SizeF(361.1248!, 23.0!)
        Me.XrLabel34.StylePriority.UseBackColor = False
        Me.XrLabel34.StylePriority.UseFont = False
        Me.XrLabel34.StylePriority.UseForeColor = False
        Me.XrLabel34.StylePriority.UseTextAlignment = False
        Me.XrLabel34.Text = "FORMA DE PAGO"
        Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel31
        '
        Me.XrLabel31.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel31.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel31.ForeColor = System.Drawing.Color.White
        Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(182.7085!, 1.166789!)
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.SizeF = New System.Drawing.SizeF(361.1248!, 23.0!)
        Me.XrLabel31.StylePriority.UseBackColor = False
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.StylePriority.UseForeColor = False
        Me.XrLabel31.StylePriority.UseTextAlignment = False
        Me.XrLabel31.Text = "USO DEL CFDI"
        Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel32
        '
        Me.XrLabel32.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel32.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel32.BorderWidth = 3.0!
        Me.XrLabel32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FacUsoCFDI")})
        Me.XrLabel32.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 9.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel32.ForeColor = System.Drawing.Color.Black
        Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(182.7085!, 25.0!)
        Me.XrLabel32.Multiline = True
        Me.XrLabel32.Name = "XrLabel32"
        Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel32.SizeF = New System.Drawing.SizeF(361.1246!, 27.17!)
        Me.XrLabel32.StylePriority.UseBorderColor = False
        Me.XrLabel32.StylePriority.UseBorders = False
        Me.XrLabel32.StylePriority.UseBorderWidth = False
        Me.XrLabel32.StylePriority.UseFont = False
        Me.XrLabel32.StylePriority.UseForeColor = False
        Me.XrLabel32.StylePriority.UseTextAlignment = False
        Me.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel30
        '
        Me.XrLabel30.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.XrLabel30.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Subtotal")})
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(648.3333!, 0!)
        Me.XrLabel30.Multiline = True
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel30.StylePriority.UseBackColor = False
        Me.XrLabel30.Text = "XrLabel30"
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel30.TextFormatString = "{0:$#,##0.00}"
        '
        'XrLabel27
        '
        Me.XrLabel27.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel27.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel27.BorderWidth = 3.0!
        Me.XrLabel27.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Pedido_mov")})
        Me.XrLabel27.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel27.ForeColor = System.Drawing.Color.Black
        Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(547.8329!, 97.83312!)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.SizeF = New System.Drawing.SizeF(197.863!, 59.34327!)
        Me.XrLabel27.StylePriority.UseBorderColor = False
        Me.XrLabel27.StylePriority.UseBorders = False
        Me.XrLabel27.StylePriority.UseBorderWidth = False
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.StylePriority.UseForeColor = False
        Me.XrLabel27.StylePriority.UseTextAlignment = False
        Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel28
        '
        Me.XrLabel28.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel28.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel28.ForeColor = System.Drawing.Color.White
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(547.8329!, 73.99995!)
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(197.863!, 23.0!)
        Me.XrLabel28.StylePriority.UseBackColor = False
        Me.XrLabel28.StylePriority.UseFont = False
        Me.XrLabel28.StylePriority.UseForeColor = False
        Me.XrLabel28.StylePriority.UseTextAlignment = False
        Me.XrLabel28.Text = "ORDEN DE COMPRA"
        Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel26
        '
        Me.XrLabel26.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel26.BorderWidth = 3.0!
        Me.XrLabel26.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel26.ForeColor = System.Drawing.Color.Black
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(541.5298!, 397.107!)
        Me.XrLabel26.Multiline = True
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(204.6666!, 19.2305!)
        Me.XrLabel26.StylePriority.UseBorderColor = False
        Me.XrLabel26.StylePriority.UseBorders = False
        Me.XrLabel26.StylePriority.UseBorderWidth = False
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseForeColor = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "FIRMA"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel25
        '
        Me.XrLabel25.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel25.ForeColor = System.Drawing.Color.Black
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 277.8636!)
        Me.XrLabel25.Multiline = True
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(735.696!, 119.2434!)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseForeColor = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = resources.GetString("XrLabel25.Text")
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel13
        '
        Me.XrLabel13.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NoCertificadoSAT")})
        Me.XrLabel13.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.25!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel13.ForeColor = System.Drawing.Color.Black
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(548.3333!, 254.8635!)
        Me.XrLabel13.Multiline = True
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(197.8629!, 23.00002!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseForeColor = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel14
        '
        Me.XrLabel14.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel14.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel14.ForeColor = System.Drawing.Color.White
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(416.6963!, 254.8635!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(129.25!, 23.0!)
        Me.XrLabel14.StylePriority.UseBackColor = False
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseForeColor = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "Certidicado SAT"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel4
        '
        Me.XrLabel4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Uuid")})
        Me.XrLabel4.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 8.25!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel4.ForeColor = System.Drawing.Color.Black
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(76.74996!, 254.8635!)
        Me.XrLabel4.Multiline = True
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(339.9463!, 23.00002!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseForeColor = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel3
        '
        Me.XrLabel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel3.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel3.ForeColor = System.Drawing.Color.White
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 254.8635!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(66.74995!, 23.0!)
        Me.XrLabel3.StylePriority.UseBackColor = False
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseForeColor = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "UUID"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel1
        '
        Me.XrLabel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel1.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel1.ForeColor = System.Drawing.Color.White
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(4.999987!, 208.8803!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(740.696!, 23.0!)
        Me.XrLabel1.StylePriority.UseBackColor = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseForeColor = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "SELLO DIGITAL SAT"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel2
        '
        Me.XrLabel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel2.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel2.BorderWidth = 3.0!
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "SelloSAT")})
        Me.XrLabel2.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel2.ForeColor = System.Drawing.Color.Black
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(4.999987!, 232.7135!)
        Me.XrLabel2.Multiline = True
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(740.696!, 21.15!)
        Me.XrLabel2.StylePriority.UseBorderColor = False
        Me.XrLabel2.StylePriority.UseBorders = False
        Me.XrLabel2.StylePriority.UseBorderWidth = False
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseForeColor = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel43
        '
        Me.XrLabel43.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel43.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel43.ForeColor = System.Drawing.Color.White
        Me.XrLabel43.LocationFloat = New DevExpress.Utils.PointFloat(4.999987!, 162.0623!)
        Me.XrLabel43.Name = "XrLabel43"
        Me.XrLabel43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel43.SizeF = New System.Drawing.SizeF(740.696!, 23.00001!)
        Me.XrLabel43.StylePriority.UseBackColor = False
        Me.XrLabel43.StylePriority.UseFont = False
        Me.XrLabel43.StylePriority.UseForeColor = False
        Me.XrLabel43.StylePriority.UseTextAlignment = False
        Me.XrLabel43.Text = "SELLO DIGITAL CFDI"
        Me.XrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel44
        '
        Me.XrLabel44.BorderColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.XrLabel44.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel44.BorderWidth = 3.0!
        Me.XrLabel44.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "SelloCFD")})
        Me.XrLabel44.Font = New DevExpress.Drawing.DXFont("Trebuchet MS", 6.0!, DevExpress.Drawing.DXFontStyle.Regular, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.XrLabel44.ForeColor = System.Drawing.Color.Black
        Me.XrLabel44.LocationFloat = New DevExpress.Utils.PointFloat(4.999987!, 185.0623!)
        Me.XrLabel44.Multiline = True
        Me.XrLabel44.Name = "XrLabel44"
        Me.XrLabel44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel44.SizeF = New System.Drawing.SizeF(740.696!, 21.14999!)
        Me.XrLabel44.StylePriority.UseBorderColor = False
        Me.XrLabel44.StylePriority.UseBorders = False
        Me.XrLabel44.StylePriority.UseBorderWidth = False
        Me.XrLabel44.StylePriority.UseFont = False
        Me.XrLabel44.StylePriority.UseForeColor = False
        Me.XrLabel44.StylePriority.UseTextAlignment = False
        Me.XrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'lbl_IVA
        '
        Me.lbl_IVA.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.lbl_IVA.CanGrow = False
        Me.lbl_IVA.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Impuesto_iva")})
        Me.lbl_IVA.LocationFloat = New DevExpress.Utils.PointFloat(649.4998!, 25.0!)
        Me.lbl_IVA.Name = "lbl_IVA"
        Me.lbl_IVA.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_IVA.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.lbl_IVA.StylePriority.UseBackColor = False
        Me.lbl_IVA.StylePriority.UseTextAlignment = False
        Me.lbl_IVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_IVA.TextFormatString = "{0:$#,##0.00}"
        Me.lbl_IVA.WordWrap = False
        '
        'lbl_EIVA
        '
        Me.lbl_EIVA.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_EIVA.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_EIVA.ForeColor = System.Drawing.Color.White
        Me.lbl_EIVA.LocationFloat = New DevExpress.Utils.PointFloat(548.3333!, 25.0!)
        Me.lbl_EIVA.Name = "lbl_EIVA"
        Me.lbl_EIVA.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_EIVA.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.lbl_EIVA.StyleName = "Title"
        Me.lbl_EIVA.StylePriority.UseBackColor = False
        Me.lbl_EIVA.StylePriority.UseFont = False
        Me.lbl_EIVA.StylePriority.UseForeColor = False
        Me.lbl_EIVA.StylePriority.UseTextAlignment = False
        Me.lbl_EIVA.Text = "I.V.A."
        Me.lbl_EIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_ETotal
        '
        Me.lbl_ETotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_ETotal.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_ETotal.ForeColor = System.Drawing.Color.White
        Me.lbl_ETotal.LocationFloat = New DevExpress.Utils.PointFloat(548.3333!, 49.99998!)
        Me.lbl_ETotal.Name = "lbl_ETotal"
        Me.lbl_ETotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_ETotal.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.lbl_ETotal.StyleName = "Title"
        Me.lbl_ETotal.StylePriority.UseBackColor = False
        Me.lbl_ETotal.StylePriority.UseFont = False
        Me.lbl_ETotal.StylePriority.UseForeColor = False
        Me.lbl_ETotal.StylePriority.UseTextAlignment = False
        Me.lbl_ETotal.Text = "TOTAL"
        Me.lbl_ETotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'lbl_TOTAL
        '
        Me.lbl_TOTAL.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.lbl_TOTAL.CanGrow = False
        Me.lbl_TOTAL.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Total")})
        Me.lbl_TOTAL.LocationFloat = New DevExpress.Utils.PointFloat(650.5002!, 49.5!)
        Me.lbl_TOTAL.Name = "lbl_TOTAL"
        Me.lbl_TOTAL.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_TOTAL.SizeF = New System.Drawing.SizeF(99.49982!, 23.0!)
        Me.lbl_TOTAL.StylePriority.UseBackColor = False
        Me.lbl_TOTAL.StylePriority.UseTextAlignment = False
        Me.lbl_TOTAL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_TOTAL.TextFormatString = "{0:$#,##0.00}"
        Me.lbl_TOTAL.WordWrap = False
        '
        'lbl_ESubtotal
        '
        Me.lbl_ESubtotal.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left
        Me.lbl_ESubtotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.lbl_ESubtotal.Font = New DevExpress.Drawing.DXFont("Tahoma", 10.2!, DevExpress.Drawing.DXFontStyle.Bold, DevExpress.Drawing.DXGraphicsUnit.Point, New DevExpress.Drawing.DXFontAdditionalProperty() {New DevExpress.Drawing.DXFontAdditionalProperty("GdiCharSet", CType(0, Byte))})
        Me.lbl_ESubtotal.ForeColor = System.Drawing.Color.White
        Me.lbl_ESubtotal.LocationFloat = New DevExpress.Utils.PointFloat(548.3333!, 0!)
        Me.lbl_ESubtotal.Name = "lbl_ESubtotal"
        Me.lbl_ESubtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_ESubtotal.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.lbl_ESubtotal.StyleName = "Title"
        Me.lbl_ESubtotal.StylePriority.UseBackColor = False
        Me.lbl_ESubtotal.StylePriority.UseFont = False
        Me.lbl_ESubtotal.StylePriority.UseForeColor = False
        Me.lbl_ESubtotal.StylePriority.UseTextAlignment = False
        Me.lbl_ESubtotal.Text = "SUB TOTAL"
        Me.lbl_ESubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lbl_EUniCtoIVA, Me.lbl_ETotCtoNeto, Me.lbl_EUniCtoNeto, Me.lbl_EUniCtoBruto, Me.lbl_EMarca, Me.lbl_EIdRen})
        Me.GroupHeader1.HeightF = 23.00001!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'odsDatosFacturas
        '
        Me.odsDatosFacturas.DataSource = GetType(proyVOficina_PDV.Facturas)
        Me.odsDatosFacturas.Name = "odsDatosFacturas"
        '
        'RenglonImpar
        '
        Me.RenglonImpar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.RenglonImpar.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.RenglonImpar.Name = "RenglonImpar"
        Me.RenglonImpar.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'cal_Total_IVA
        '
        Me.cal_Total_IVA.DataMember = "Detalle"
        Me.cal_Total_IVA.Expression = "[Total_costo_iva]-[Total_costo_neto]"
        Me.cal_Total_IVA.Name = "cal_Total_IVA"
        '
        'XtraRep_FacturaBase
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeaderBand1, Me.DetailReport})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.cal_Total_IVA})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.odsDatosFacturas})
        Me.DataSource = Me.odsDatosFacturas
        Me.Margins = New DevExpress.Drawing.DXMargins(50, 50, 28, 33)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.Title, Me.DetailCaption1, Me.DetailData1, Me.DetailCaption3, Me.DetailData3, Me.DetailData3_Odd, Me.DetailCaptionBackground3, Me.PageInfo, Me.RenglonImpar})
        Me.Version = "20.1"
        Me.Watermark.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource("img", resources.GetString("XtraRep_FacturaBase.Watermark.ImageSource"))
        Me.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Stretch
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.odsDatosFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents odsDatosFacturas As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents ReportHeaderBand1 As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents lbl_Empresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Title As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DetailCaption1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DetailData1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DetailCaption3 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DetailData3 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DetailData3_Odd As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DetailCaptionBackground3 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents PageInfo As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents lbl_Proveedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_IdProveedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_ECliente As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPic_Logo As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents lbl_IdRenglon As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_EUniCtoNeto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_EUniCtoBruto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_EMarca As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_EIdRen As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_UniCtoBruto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_EUniCtoIVA As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_ETotCtoNeto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_TotCtoNeto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_UniCtoIVA As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_UniCtoNeto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DetailReport As DevExpress.XtraReports.UI.DetailReportBand
    Friend WithEvents Detail1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents RenglonImpar As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents lbl_ESubtotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_IVA As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_EIVA As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_ETotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lbl_TOTAL As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents cal_Total_IVA As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents XrLabel44 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel43 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents lbl_EmpresaDireccion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_Marca As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrBarCode1 As DevExpress.XtraReports.UI.XRBarCode
End Class
