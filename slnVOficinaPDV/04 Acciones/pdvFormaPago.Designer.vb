﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvFormaPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cbo_FormaPago = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GC_Recompensas = New DevExpress.XtraEditors.GroupControl()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_ImporteDEMAX = New DevExpress.XtraEditors.TextEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Txt_DE_Observacion = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_CodigoCanje = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Total = New DevExpress.XtraEditors.TextEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_PagoRecibido = New DevExpress.XtraEditors.TextEdit()
        Me.brn_Aceptar = New System.Windows.Forms.Button()
        Me.brn_Cancelar = New System.Windows.Forms.Button()
        Me.GC_General = New DevExpress.XtraEditors.GroupControl()
        Me.txt_TCambio = New DevExpress.XtraEditors.TextEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GC_Botones = New DevExpress.XtraEditors.GroupControl()
        Me.txt_NoControl = New DevExpress.XtraEditors.TextEdit()
        Me.Cbo_Institucion = New DevExpress.XtraEditors.TextEdit()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_ClienteTipo = New DevExpress.XtraEditors.TextEdit()
        Me.txt_ClienteEstatus = New DevExpress.XtraEditors.TextEdit()
        Me.txt_PinPad = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Cliente = New DevExpress.XtraEditors.TextEdit()
        Me.GC_Adicionales = New DevExpress.XtraEditors.GroupControl()
        Me.btn_AplicarVoucher = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cbo_PlanCredito = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_Autonumsuc = New DevExpress.XtraEditors.TextEdit()
        Me.cbo_Bancos = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lbl_Autorizacion = New System.Windows.Forms.Label()
        Me.txt_Autorizacion = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_Referencia = New System.Windows.Forms.Label()
        Me.txt_Referencia = New DevExpress.XtraEditors.TextEdit()
        Me.GC_Vale = New DevExpress.XtraEditors.GroupControl()
        Me.Btn_Nuevo = New System.Windows.Forms.Button()
        Me.Btn_Buscar = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_Folio = New DevExpress.XtraEditors.TextEdit()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_NombreClienteFinal = New DevExpress.XtraEditors.TextEdit()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_IdClienteFinal = New DevExpress.XtraEditors.TextEdit()
        CType(Me.GC_Recompensas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Recompensas.SuspendLayout()
        CType(Me.txt_ImporteDEMAX.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_DE_Observacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PagoRecibido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_General, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_General.SuspendLayout()
        CType(Me.txt_TCambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_Botones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Botones.SuspendLayout()
        CType(Me.txt_NoControl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cbo_Institucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ClienteTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ClienteEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PinPad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Cliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_Adicionales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Adicionales.SuspendLayout()
        CType(Me.txt_Autonumsuc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Autorizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_Vale, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Vale.SuspendLayout()
        CType(Me.txt_Folio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NombreClienteFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IdClienteFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbo_FormaPago
        '
        Me.cbo_FormaPago.DisplayMember = "moneda"
        Me.cbo_FormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo_FormaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_FormaPago.FormattingEnabled = True
        Me.cbo_FormaPago.Items.AddRange(New Object() {"CONTADO", "D. E."})
        Me.cbo_FormaPago.Location = New System.Drawing.Point(17, 60)
        Me.cbo_FormaPago.Name = "cbo_FormaPago"
        Me.cbo_FormaPago.Size = New System.Drawing.Size(121, 32)
        Me.cbo_FormaPago.TabIndex = 0
        Me.cbo_FormaPago.ValueMember = "moneda"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(141, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Forma de &Pago"
        '
        'GC_Recompensas
        '
        Me.GC_Recompensas.Controls.Add(Me.Label6)
        Me.GC_Recompensas.Controls.Add(Me.txt_ImporteDEMAX)
        Me.GC_Recompensas.Controls.Add(Me.Label5)
        Me.GC_Recompensas.Controls.Add(Me.Txt_DE_Observacion)
        Me.GC_Recompensas.Controls.Add(Me.Label4)
        Me.GC_Recompensas.Controls.Add(Me.txt_CodigoCanje)
        Me.GC_Recompensas.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_Recompensas.Location = New System.Drawing.Point(0, 106)
        Me.GC_Recompensas.Name = "GC_Recompensas"
        Me.GC_Recompensas.Size = New System.Drawing.Size(562, 140)
        Me.GC_Recompensas.TabIndex = 1
        Me.GC_Recompensas.Text = "Programa Lealtad"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 104)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(113, 24)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Impote D. E."
        '
        'txt_ImporteDEMAX
        '
        Me.txt_ImporteDEMAX.Location = New System.Drawing.Point(146, 101)
        Me.txt_ImporteDEMAX.Name = "txt_ImporteDEMAX"
        Me.txt_ImporteDEMAX.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ImporteDEMAX.Properties.Appearance.Options.UseFont = True
        Me.txt_ImporteDEMAX.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ImporteDEMAX.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_ImporteDEMAX.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_ImporteDEMAX.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_ImporteDEMAX.Properties.Mask.EditMask = "C2"
        Me.txt_ImporteDEMAX.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_ImporteDEMAX.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_ImporteDEMAX.Properties.ReadOnly = True
        Me.txt_ImporteDEMAX.Size = New System.Drawing.Size(121, 30)
        Me.txt_ImporteDEMAX.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(5, 70)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(140, 24)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Nombre"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Txt_DE_Observacion
        '
        Me.Txt_DE_Observacion.Location = New System.Drawing.Point(147, 62)
        Me.Txt_DE_Observacion.Name = "Txt_DE_Observacion"
        Me.Txt_DE_Observacion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_DE_Observacion.Properties.Appearance.Options.UseFont = True
        Me.Txt_DE_Observacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_DE_Observacion.Size = New System.Drawing.Size(389, 30)
        Me.Txt_DE_Observacion.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 24)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Codigo Canje"
        '
        'txt_CodigoCanje
        '
        Me.txt_CodigoCanje.Location = New System.Drawing.Point(147, 26)
        Me.txt_CodigoCanje.Name = "txt_CodigoCanje"
        Me.txt_CodigoCanje.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CodigoCanje.Properties.Appearance.Options.UseFont = True
        Me.txt_CodigoCanje.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CodigoCanje.Size = New System.Drawing.Size(120, 30)
        Me.txt_CodigoCanje.TabIndex = 0
        '
        'txt_Total
        '
        Me.txt_Total.Location = New System.Drawing.Point(284, 62)
        Me.txt_Total.Name = "txt_Total"
        Me.txt_Total.Properties.AllowFocused = False
        Me.txt_Total.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.Appearance.Options.UseFont = True
        Me.txt_Total.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Total.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Total.Properties.Mask.EditMask = "C2"
        Me.txt_Total.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Total.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Total.Properties.ReadOnly = True
        Me.txt_Total.Size = New System.Drawing.Size(121, 30)
        Me.txt_Total.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(285, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 24)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Total a Pagar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(411, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(134, 24)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Pago &Recibido"
        '
        'txt_PagoRecibido
        '
        Me.txt_PagoRecibido.Location = New System.Drawing.Point(415, 62)
        Me.txt_PagoRecibido.Name = "txt_PagoRecibido"
        Me.txt_PagoRecibido.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt_PagoRecibido.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PagoRecibido.Properties.Appearance.Options.UseBackColor = True
        Me.txt_PagoRecibido.Properties.Appearance.Options.UseFont = True
        Me.txt_PagoRecibido.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_PagoRecibido.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_PagoRecibido.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txt_PagoRecibido.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PagoRecibido.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_PagoRecibido.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_PagoRecibido.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_PagoRecibido.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_PagoRecibido.Properties.Mask.EditMask = "C2"
        Me.txt_PagoRecibido.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_PagoRecibido.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_PagoRecibido.Size = New System.Drawing.Size(121, 30)
        Me.txt_PagoRecibido.TabIndex = 3
        '
        'brn_Aceptar
        '
        Me.brn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.brn_Aceptar.Location = New System.Drawing.Point(146, 56)
        Me.brn_Aceptar.Name = "brn_Aceptar"
        Me.brn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.brn_Aceptar.TabIndex = 0
        Me.brn_Aceptar.UseVisualStyleBackColor = True
        '
        'brn_Cancelar
        '
        Me.brn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.brn_Cancelar.Location = New System.Drawing.Point(352, 56)
        Me.brn_Cancelar.Name = "brn_Cancelar"
        Me.brn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.brn_Cancelar.TabIndex = 1
        Me.brn_Cancelar.UseVisualStyleBackColor = True
        '
        'GC_General
        '
        Me.GC_General.Controls.Add(Me.txt_TCambio)
        Me.GC_General.Controls.Add(Me.Label8)
        Me.GC_General.Controls.Add(Me.txt_PagoRecibido)
        Me.GC_General.Controls.Add(Me.cbo_FormaPago)
        Me.GC_General.Controls.Add(Me.Label1)
        Me.GC_General.Controls.Add(Me.Label3)
        Me.GC_General.Controls.Add(Me.txt_Total)
        Me.GC_General.Controls.Add(Me.Label2)
        Me.GC_General.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_General.Location = New System.Drawing.Point(0, 0)
        Me.GC_General.Name = "GC_General"
        Me.GC_General.Size = New System.Drawing.Size(562, 106)
        Me.GC_General.TabIndex = 0
        '
        'txt_TCambio
        '
        Me.txt_TCambio.Location = New System.Drawing.Point(151, 62)
        Me.txt_TCambio.Name = "txt_TCambio"
        Me.txt_TCambio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TCambio.Properties.Appearance.Options.UseFont = True
        Me.txt_TCambio.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TCambio.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_TCambio.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_TCambio.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_TCambio.Properties.Mask.EditMask = "C2"
        Me.txt_TCambio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_TCambio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_TCambio.Size = New System.Drawing.Size(121, 30)
        Me.txt_TCambio.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(151, 29)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(118, 24)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "&Tipo Cambio"
        '
        'GC_Botones
        '
        Me.GC_Botones.Controls.Add(Me.txt_NoControl)
        Me.GC_Botones.Controls.Add(Me.Cbo_Institucion)
        Me.GC_Botones.Controls.Add(Me.Label10)
        Me.GC_Botones.Controls.Add(Me.txt_ClienteTipo)
        Me.GC_Botones.Controls.Add(Me.txt_ClienteEstatus)
        Me.GC_Botones.Controls.Add(Me.txt_PinPad)
        Me.GC_Botones.Controls.Add(Me.TextEdit1)
        Me.GC_Botones.Controls.Add(Me.txt_Cliente)
        Me.GC_Botones.Controls.Add(Me.brn_Aceptar)
        Me.GC_Botones.Controls.Add(Me.brn_Cancelar)
        Me.GC_Botones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GC_Botones.Location = New System.Drawing.Point(0, 515)
        Me.GC_Botones.Name = "GC_Botones"
        Me.GC_Botones.Size = New System.Drawing.Size(562, 111)
        Me.GC_Botones.TabIndex = 3
        '
        'txt_NoControl
        '
        Me.txt_NoControl.Location = New System.Drawing.Point(102, 61)
        Me.txt_NoControl.Name = "txt_NoControl"
        Me.txt_NoControl.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NoControl.Properties.Appearance.Options.UseFont = True
        Me.txt_NoControl.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NoControl.Size = New System.Drawing.Size(22, 30)
        Me.txt_NoControl.TabIndex = 33
        Me.txt_NoControl.Visible = False
        '
        'Cbo_Institucion
        '
        Me.Cbo_Institucion.Location = New System.Drawing.Point(148, 24)
        Me.Cbo_Institucion.Name = "Cbo_Institucion"
        Me.Cbo_Institucion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cbo_Institucion.Properties.Appearance.Options.UseFont = True
        Me.Cbo_Institucion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Cbo_Institucion.Size = New System.Drawing.Size(389, 30)
        Me.Cbo_Institucion.TabIndex = 32
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(8, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 24)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Institucion"
        '
        'txt_ClienteTipo
        '
        Me.txt_ClienteTipo.Location = New System.Drawing.Point(66, 61)
        Me.txt_ClienteTipo.Name = "txt_ClienteTipo"
        Me.txt_ClienteTipo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteTipo.Properties.Appearance.Options.UseFont = True
        Me.txt_ClienteTipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ClienteTipo.Size = New System.Drawing.Size(22, 30)
        Me.txt_ClienteTipo.TabIndex = 6
        Me.txt_ClienteTipo.Visible = False
        '
        'txt_ClienteEstatus
        '
        Me.txt_ClienteEstatus.Location = New System.Drawing.Point(38, 61)
        Me.txt_ClienteEstatus.Name = "txt_ClienteEstatus"
        Me.txt_ClienteEstatus.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteEstatus.Properties.Appearance.Options.UseFont = True
        Me.txt_ClienteEstatus.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ClienteEstatus.Size = New System.Drawing.Size(22, 30)
        Me.txt_ClienteEstatus.TabIndex = 5
        Me.txt_ClienteEstatus.Visible = False
        '
        'txt_PinPad
        '
        Me.txt_PinPad.Location = New System.Drawing.Point(214, 61)
        Me.txt_PinPad.Name = "txt_PinPad"
        Me.txt_PinPad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PinPad.Properties.Appearance.Options.UseFont = True
        Me.txt_PinPad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_PinPad.Size = New System.Drawing.Size(120, 30)
        Me.txt_PinPad.TabIndex = 4
        Me.txt_PinPad.Visible = False
        '
        'TextEdit1
        '
        Me.TextEdit1.Enabled = False
        Me.TextEdit1.Location = New System.Drawing.Point(430, 61)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit1.Size = New System.Drawing.Size(120, 30)
        Me.TextEdit1.TabIndex = 3
        Me.TextEdit1.Visible = False
        '
        'txt_Cliente
        '
        Me.txt_Cliente.Location = New System.Drawing.Point(10, 61)
        Me.txt_Cliente.Name = "txt_Cliente"
        Me.txt_Cliente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cliente.Properties.Appearance.Options.UseFont = True
        Me.txt_Cliente.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Cliente.Size = New System.Drawing.Size(22, 30)
        Me.txt_Cliente.TabIndex = 2
        Me.txt_Cliente.Visible = False
        '
        'GC_Adicionales
        '
        Me.GC_Adicionales.Controls.Add(Me.btn_AplicarVoucher)
        Me.GC_Adicionales.Controls.Add(Me.Label20)
        Me.GC_Adicionales.Controls.Add(Me.cbo_PlanCredito)
        Me.GC_Adicionales.Controls.Add(Me.Label9)
        Me.GC_Adicionales.Controls.Add(Me.txt_Autonumsuc)
        Me.GC_Adicionales.Controls.Add(Me.cbo_Bancos)
        Me.GC_Adicionales.Controls.Add(Me.Label7)
        Me.GC_Adicionales.Controls.Add(Me.lbl_Autorizacion)
        Me.GC_Adicionales.Controls.Add(Me.txt_Autorizacion)
        Me.GC_Adicionales.Controls.Add(Me.lbl_Referencia)
        Me.GC_Adicionales.Controls.Add(Me.txt_Referencia)
        Me.GC_Adicionales.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_Adicionales.Location = New System.Drawing.Point(0, 246)
        Me.GC_Adicionales.Name = "GC_Adicionales"
        Me.GC_Adicionales.Size = New System.Drawing.Size(562, 166)
        Me.GC_Adicionales.TabIndex = 2
        Me.GC_Adicionales.Text = "Datos Adicionales"
        '
        'btn_AplicarVoucher
        '
        Me.btn_AplicarVoucher.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_AplicarVoucher.Location = New System.Drawing.Point(272, 131)
        Me.btn_AplicarVoucher.Name = "btn_AplicarVoucher"
        Me.btn_AplicarVoucher.Size = New System.Drawing.Size(264, 26)
        Me.btn_AplicarVoucher.TabIndex = 27
        Me.btn_AplicarVoucher.Text = "Aplicar Voucher Pendiente"
        Me.btn_AplicarVoucher.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(6, 28)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(112, 24)
        Me.Label20.TabIndex = 26
        Me.Label20.Text = "Plan Crédito"
        '
        'cbo_PlanCredito
        '
        Me.cbo_PlanCredito.DisplayMember = "combo"
        Me.cbo_PlanCredito.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_PlanCredito.FormattingEnabled = True
        Me.cbo_PlanCredito.Location = New System.Drawing.Point(146, 26)
        Me.cbo_PlanCredito.Name = "cbo_PlanCredito"
        Me.cbo_PlanCredito.Size = New System.Drawing.Size(390, 27)
        Me.cbo_PlanCredito.TabIndex = 25
        Me.cbo_PlanCredito.ValueMember = "tipo"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(4, 131)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 24)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "AutonumSuc"
        '
        'txt_Autonumsuc
        '
        Me.txt_Autonumsuc.Location = New System.Drawing.Point(146, 128)
        Me.txt_Autonumsuc.Name = "txt_Autonumsuc"
        Me.txt_Autonumsuc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Autonumsuc.Properties.Appearance.Options.UseFont = True
        Me.txt_Autonumsuc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Autonumsuc.Size = New System.Drawing.Size(120, 30)
        Me.txt_Autonumsuc.TabIndex = 11
        '
        'cbo_Bancos
        '
        Me.cbo_Bancos.DisplayMember = "id_banco"
        Me.cbo_Bancos.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Bancos.FormattingEnabled = True
        Me.cbo_Bancos.Items.AddRange(New Object() {"CONTADO", "D. E."})
        Me.cbo_Bancos.Location = New System.Drawing.Point(272, 93)
        Me.cbo_Bancos.Name = "cbo_Bancos"
        Me.cbo_Bancos.Size = New System.Drawing.Size(264, 32)
        Me.cbo_Bancos.TabIndex = 2
        Me.cbo_Bancos.ValueMember = "id_banco"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(280, 62)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 24)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Bancos"
        '
        'lbl_Autorizacion
        '
        Me.lbl_Autorizacion.AutoSize = True
        Me.lbl_Autorizacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Autorizacion.Location = New System.Drawing.Point(4, 95)
        Me.lbl_Autorizacion.Name = "lbl_Autorizacion"
        Me.lbl_Autorizacion.Size = New System.Drawing.Size(114, 24)
        Me.lbl_Autorizacion.TabIndex = 9
        Me.lbl_Autorizacion.Text = "Autorización"
        '
        'txt_Autorizacion
        '
        Me.txt_Autorizacion.Location = New System.Drawing.Point(146, 92)
        Me.txt_Autorizacion.Name = "txt_Autorizacion"
        Me.txt_Autorizacion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Autorizacion.Properties.Appearance.Options.UseFont = True
        Me.txt_Autorizacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Autorizacion.Size = New System.Drawing.Size(120, 30)
        Me.txt_Autorizacion.TabIndex = 1
        '
        'lbl_Referencia
        '
        Me.lbl_Referencia.AutoSize = True
        Me.lbl_Referencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Referencia.Location = New System.Drawing.Point(5, 59)
        Me.lbl_Referencia.Name = "lbl_Referencia"
        Me.lbl_Referencia.Size = New System.Drawing.Size(101, 24)
        Me.lbl_Referencia.TabIndex = 7
        Me.lbl_Referencia.Text = "Referencia"
        '
        'txt_Referencia
        '
        Me.txt_Referencia.Location = New System.Drawing.Point(147, 56)
        Me.txt_Referencia.Name = "txt_Referencia"
        Me.txt_Referencia.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Referencia.Properties.Appearance.Options.UseFont = True
        Me.txt_Referencia.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Referencia.Size = New System.Drawing.Size(120, 30)
        Me.txt_Referencia.TabIndex = 0
        '
        'GC_Vale
        '
        Me.GC_Vale.Controls.Add(Me.Btn_Nuevo)
        Me.GC_Vale.Controls.Add(Me.Btn_Buscar)
        Me.GC_Vale.Controls.Add(Me.Label11)
        Me.GC_Vale.Controls.Add(Me.txt_Folio)
        Me.GC_Vale.Controls.Add(Me.Label12)
        Me.GC_Vale.Controls.Add(Me.txt_NombreClienteFinal)
        Me.GC_Vale.Controls.Add(Me.Label13)
        Me.GC_Vale.Controls.Add(Me.txt_IdClienteFinal)
        Me.GC_Vale.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_Vale.Location = New System.Drawing.Point(0, 412)
        Me.GC_Vale.Name = "GC_Vale"
        Me.GC_Vale.Size = New System.Drawing.Size(562, 103)
        Me.GC_Vale.TabIndex = 4
        Me.GC_Vale.Text = "Vale"
        '
        'Btn_Nuevo
        '
        Me.Btn_Nuevo.Image = Global.proyVOficina_PDV.My.Resources.Resources.agregar_boton
        Me.Btn_Nuevo.Location = New System.Drawing.Point(308, 26)
        Me.Btn_Nuevo.Name = "Btn_Nuevo"
        Me.Btn_Nuevo.Size = New System.Drawing.Size(30, 30)
        Me.Btn_Nuevo.TabIndex = 13
        Me.Btn_Nuevo.UseVisualStyleBackColor = True
        '
        'Btn_Buscar
        '
        Me.Btn_Buscar.Location = New System.Drawing.Point(272, 26)
        Me.Btn_Buscar.Name = "Btn_Buscar"
        Me.Btn_Buscar.Size = New System.Drawing.Size(30, 30)
        Me.Btn_Buscar.TabIndex = 12
        Me.Btn_Buscar.Text = "..."
        Me.Btn_Buscar.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(372, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 24)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Folio"
        '
        'txt_Folio
        '
        Me.txt_Folio.Location = New System.Drawing.Point(430, 26)
        Me.txt_Folio.Name = "txt_Folio"
        Me.txt_Folio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Folio.Properties.Appearance.Options.UseFont = True
        Me.txt_Folio.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Folio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Folio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Folio.Size = New System.Drawing.Size(105, 30)
        Me.txt_Folio.TabIndex = 10
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(5, 62)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(140, 24)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Nombre"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_NombreClienteFinal
        '
        Me.txt_NombreClienteFinal.Location = New System.Drawing.Point(147, 62)
        Me.txt_NombreClienteFinal.Name = "txt_NombreClienteFinal"
        Me.txt_NombreClienteFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreClienteFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreClienteFinal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreClienteFinal.Properties.ReadOnly = True
        Me.txt_NombreClienteFinal.Size = New System.Drawing.Size(389, 30)
        Me.txt_NombreClienteFinal.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(5, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(114, 24)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "Cliente Final"
        '
        'txt_IdClienteFinal
        '
        Me.txt_IdClienteFinal.Location = New System.Drawing.Point(147, 26)
        Me.txt_IdClienteFinal.Name = "txt_IdClienteFinal"
        Me.txt_IdClienteFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IdClienteFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_IdClienteFinal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IdClienteFinal.Properties.ReadOnly = True
        Me.txt_IdClienteFinal.Size = New System.Drawing.Size(120, 30)
        Me.txt_IdClienteFinal.TabIndex = 0
        '
        'pdvFormaPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(562, 626)
        Me.ControlBox = False
        Me.Controls.Add(Me.GC_Botones)
        Me.Controls.Add(Me.GC_Vale)
        Me.Controls.Add(Me.GC_Adicionales)
        Me.Controls.Add(Me.GC_Recompensas)
        Me.Controls.Add(Me.GC_General)
        Me.KeyPreview = True
        Me.Name = "pdvFormaPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Forma de Pago"
        CType(Me.GC_Recompensas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Recompensas.ResumeLayout(False)
        Me.GC_Recompensas.PerformLayout()
        CType(Me.txt_ImporteDEMAX.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_DE_Observacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PagoRecibido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_General, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_General.ResumeLayout(False)
        Me.GC_General.PerformLayout()
        CType(Me.txt_TCambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_Botones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Botones.ResumeLayout(False)
        Me.GC_Botones.PerformLayout()
        CType(Me.txt_NoControl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cbo_Institucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ClienteTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ClienteEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PinPad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Cliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_Adicionales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Adicionales.ResumeLayout(False)
        Me.GC_Adicionales.PerformLayout()
        CType(Me.txt_Autonumsuc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Autorizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_Vale, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Vale.ResumeLayout(False)
        Me.GC_Vale.PerformLayout()
        CType(Me.txt_Folio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NombreClienteFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IdClienteFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cbo_FormaPago As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GC_Recompensas As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txt_Total As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_PagoRecibido As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_ImporteDEMAX As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As Label
    Friend WithEvents Txt_DE_Observacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_CodigoCanje As DevExpress.XtraEditors.TextEdit
    Friend WithEvents brn_Aceptar As Button
    Friend WithEvents brn_Cancelar As Button
    Friend WithEvents GC_General As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GC_Botones As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txt_TCambio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As Label
    Friend WithEvents GC_Adicionales As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lbl_Autorizacion As Label
    Friend WithEvents txt_Autorizacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl_Referencia As Label
    Friend WithEvents txt_Referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbo_Bancos As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_Cliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_PinPad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_Autonumsuc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label20 As Label
    Friend WithEvents cbo_PlanCredito As ComboBox
    Friend WithEvents txt_ClienteEstatus As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_AplicarVoucher As Button
    Friend WithEvents GC_Vale As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label12 As Label
    Friend WithEvents txt_NombreClienteFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As Label
    Friend WithEvents txt_IdClienteFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label11 As Label
    Friend WithEvents txt_Folio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_ClienteTipo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Cbo_Institucion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label10 As Label
    Friend WithEvents txt_NoControl As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Btn_Buscar As Button
    Friend WithEvents Btn_Nuevo As Button
End Class
