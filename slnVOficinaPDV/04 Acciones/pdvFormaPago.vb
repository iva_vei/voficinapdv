﻿Imports DevExpress.XtraEditors.Controls

Public Class pdvFormaPago
    Public Impresora As String
    Public Tipo As String
    Public Condicion As String
    Dim _Autonumsuc As String
    Dim _Corte As Integer
    Dim _Cajero As String
    Dim _Caja As String
    Dim _Movimiento As String = ""
    Dim MonedaNacional As String = ""
    Dim _SWRequiereAutorizacion As Boolean = False
    Dim Empresa As String = ""
    Dim Rfc As String = ""
    Dim Direccion As String = ""
    Dim sw_VentaHuella As String = ""
    Public Property Autonumsuc As String
        Get
            Return _Autonumsuc
        End Get
        Set(value As String)
            _Autonumsuc = value
        End Set
    End Property

    Public Property Cajero As String
        Get
            Return _Cajero
        End Get
        Set(value As String)
            _Cajero = value
        End Set
    End Property

    Public Property Corte As Integer
        Get
            Return _Corte
        End Get
        Set(value As Integer)
            _Corte = value
        End Set
    End Property

    Public Property Caja As String
        Get
            Return _Caja
        End Get
        Set(value As String)
            _Caja = value
        End Set
    End Property

    Public Property Movimiento As String
        Get
            Return _Movimiento
        End Get
        Set(value As String)
            _Movimiento = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Msj As String = ""
        Try
            Tipo = ""
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_Sucursales(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, dtDatosSuc, Msj) Then
                If dtDatosSuc.Rows.Count > 0 Then
                    Direccion = dtDatosSuc.Rows(0).Item("dir1")
                    Empresa = dtDatosSuc.Rows(0).Item("empresa1")
                    Rfc = dtDatosSuc.Rows(0).Item("rfc1")
                End If
            End If

            'If oDatos.PVTA_Recupera_Vendedores(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "", "ESCUELA", True, dtDatos, Msj) Then
            '    Cbo_Institucion.DataSource = dtDatos
            'Else
            '    Cbo_Institucion.DataSource = Nothing
            'End If

            Dim stipoaux As String = ""
            If oDatos.Recupera_tiposcredito(Globales.oAmbientes.Id_Empresa, "TODOS", "", dtDatos, Msj) Then
                cbo_PlanCredito.DataSource = dtDatos

                For Each sAux As DataRow In dtDatos.Rows
                    If sAux.Item("tipo").ToString.Substring(1, 2) = Condicion Then
                        stipoaux = sAux.Item("tipo")
                    End If
                Next
                If stipoaux <> "" Then
                    cbo_PlanCredito.SelectedValue = stipoaux
                End If
            Else
                cbo_PlanCredito.DataSource = Nothing
            End If

            If oDatos.PVTA_Recupera_Monedas(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "", "ENUSO", "SI", "NO", dtDatos, MonedaNacional, Msj) Then
                If Not (txt_ClienteEstatus.Text = "NORM" Or txt_ClienteEstatus.Text = "ORO" Or txt_ClienteEstatus.Text = "ACTIVO") Then
                    For Each oRen As DataRow In dtDatos.Rows
                        If oRen.RowState <> DataRowState.Deleted Then
                            If oRen.Item("moneda") = "CREDITO" Then
                                oRen.Delete()
                            End If
                        End If
                    Next
                Else
                    'If txt_ClienteTipo.Text = "CONT" Then
                    '    For Each oRen As DataRow In dtDatos.Rows
                    '        If oRen.RowState <> DataRowState.Deleted Then
                    '            If oRen.Item("moneda") = "CREDITO" Then
                    '                oRen.Delete()
                    '            End If
                    '        End If
                    '    Next
                    'End If
                End If
                If Globales.oAmbientes.PinPadBanco = "N/A" Then
                    For Each oRen As DataRow In dtDatos.Rows
                        If oRen.RowState <> DataRowState.Deleted Then
                            If oRen.Item("moneda") = "TDC" _
                            Or oRen.Item("moneda") = "TDB" _
                            Or oRen.Item("tdc") Then
                                oRen.Delete()
                            End If
                        End If
                    Next
                End If

                cbo_FormaPago.DataSource = dtDatos
                cbo_FormaPago.SelectedValue = MonedaNacional
                cbo_FormaPago.Refresh()
                TextEdit1.Text = MonedaNacional
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If

            If oDatos.PVTA_Recupera_Bancos(dtDatos, Msj) Then
                cbo_Bancos.DataSource = dtDatos
                cbo_Bancos.Refresh()
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If

            GC_Adicionales.Visible = False
            GC_Vale.Visible = False

            Me.Height = 0
            Me.Height = IIf(GC_General.Visible, GC_General.Height + 30, 0) + IIf(GC_Adicionales.Visible, GC_Adicionales.Height + 30, 0) + IIf(GC_Vale.Visible, GC_Vale.Height + 30, 0) + +IIf(GC_Recompensas.Visible, GC_Recompensas.Height + 30, 0) + +IIf(GC_Botones.Visible, GC_Botones.Height + 30, 0) + 50

            txt_PagoRecibido.Focus()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub pdvFormaPago_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Try
            GC_Recompensas.Enabled = False
            If cbo_FormaPago.Text = "D.E." Then
                GC_Recompensas.Enabled = True
            End If

            txt_PagoRecibido.Focus()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub AceptarPago(ByVal sw_voucher_pendiente As Boolean)
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim ImporteTotal As Double
        Dim ImporteDEMAX As Double
        Dim Importe As Double
        Dim TCambio As Double
        Dim Cambio As Double
        Dim sw_continuar As Boolean = False
        Dim MesesDiferido As String = ""
        Dim NumPagos As String = ""
        Dim TipoPlan As String = ""
        Dim sPlanCredito As String = ""
        Dim CodigoAut As String = ""
        Dim Referencia As String = ""
        Dim Autorizacion As String = ""

        Tipo = "CONTADO"
        Try
            oDatos = New Datos_Viscoi

            If txt_PagoRecibido.Text = "" Then
                txt_PagoRecibido.Text = "0.0"
            End If

            If txt_TCambio.Text = "" Then
                txt_PagoRecibido.Text = "1.0"
            End If

            ImporteTotal = CDbl(txt_Total.Text)
            Importe = CDbl(txt_PagoRecibido.Text)
            TCambio = CDbl(txt_TCambio.Text)
            ImporteDEMAX = CDbl(txt_ImporteDEMAX.Text)

            MesesDiferido = ""
            NumPagos = ""
            TipoPlan = ""
            txt_Autorizacion.BackColor = Color.White
            _SWRequiereAutorizacion = True
            If oDatos.PVTA_Recupera_Monedas(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, cbo_FormaPago.SelectedValue, "", "SI", "NO", dtDatos, "", Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    _SWRequiereAutorizacion = dtDatos.Rows(0).Item("autorizacion")
                    If dtDatos.Rows(0).Item("efectivo") Then
                        Tipo = "EFECTIVO"
                    End If
                    If dtDatos.Rows(0).Item("tdc") Then
                        Tipo = "TDC"
                        Try
                            MesesDiferido = dtDatos.Rows(0).Item("pagosdif_mesesdif")
                            NumPagos = dtDatos.Rows(0).Item("pagosdif_mesesnum")
                            TipoPlan = dtDatos.Rows(0).Item("pagosdif_planpago")
                        Catch ex As Exception
                            MesesDiferido = ""
                            NumPagos = ""
                            TipoPlan = ""
                        End Try
                    End If
                    If dtDatos.Rows(0).Item("puntos") Then
                        Tipo = "PUNTOS"
                    End If
                    If dtDatos.Rows(0).Item("dinelect") Then
                        Tipo = "D. E."
                    End If
                    If dtDatos.Rows(0).Item("empleado") Then
                        Tipo = "EMPLEADO"
                    End If
                    If dtDatos.Rows(0).Item("cveventas") = "CR" Then
                        Tipo = "CREDITO"
                        If txt_Cliente.Text = "" Then
                            MessageBox.Show("Debes elejir un cliente para esta forma de pago.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Return
                        End If
                    End If

                    sw_continuar = True
                Else
                    Mensaje = "No existen datos en la tabla MONEDAS"
                End If
            Else
                Mensaje = "Error al consultar tabla MONEDAS " & Mensaje
            End If

            If Tipo = "EMPLEADO" Then
                sw_VentaHuella = "NO"
                If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, "PDV", "", "VTAEMPLEADOHUELLA", dtDatos, Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        sw_VentaHuella = dtDatos.Rows(0).Item("valor")
                    End If
                End If

                'If sw_VentaHuella = "SI" Then

                '    Dim formVerificar As New VerificarForm()
                '    formVerificar.empresaVerification = Globales.oAmbientes.Id_Empresa
                '    formVerificar.empleadoVerification = txt_Referencia.Text

                '    Dim result As DialogResult = formVerificar.ShowDialog()


                '    If result = DialogResult.OK Then
                '        sw_continuar = True
                '    Else
                '        MessageBox.Show("Verificación fallida o cancelada.", "Resultado", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '        Mensaje = "La verificacion biometrica del usuario fallo"
                '        sw_continuar = False
                '    End If
                'Else
                sw_continuar = True
                'End If
            End If


                If Tipo <> "EFECTIVO" Then
                If Importe > ImporteTotal Then
                    MessageBox.Show("El monto " & cbo_FormaPago.Text & " no puede superar el monto de total de la compra", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return
                End If
                If Importe > ImporteDEMAX And Tipo = "D. E." Then
                    MessageBox.Show("No cuentas con esa cantidad de dinero electronico", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return
                End If
            End If
            Dim IdVoucher As Integer

            If Tipo = "TDC" Or Tipo = "TDB" Or Tipo = "TDD" Or _SWRequiereAutorizacion Then
                If txt_NoControl.Text <> "" Then
                    Select Case Globales.oAmbientes.PinPadBanco
                        Case "BANORTE"
                            Me.Enabled = False
                            If sw_voucher_pendiente Then
                                Dim oSplash As New pdvVouchers
                                oSplash.Sw_pendientes = True
                                oSplash.ShowDialog()
                                sw_continuar = oSplash.Accion
                                If sw_continuar Then
                                    Me.Enabled = True
                                    txt_Autorizacion.Text = oSplash.Autorizacion
                                    txt_Referencia.Text = oSplash.Referencia
                                    _Movimiento = oSplash.Numcontrol
                                    IdVoucher = oSplash.IdVoucher
                                Else
                                    txt_Autorizacion.Text = ""
                                    sw_continuar = False
                                    Me.Close()
                                    Mensaje = "No se selecciono ningun voucher."
                                End If
                            Else

                                Globales.oAmbientes.oPinPadBanorte = New Globales.clsPinPadBanorte(Globales.oAmbientes.oPinPadBanorte.Puerto _
                                , Globales.oAmbientes.oPinPadBanorte.User _
                                , Globales.oAmbientes.oPinPadBanorte.Pswd _
                                , Globales.oAmbientes.oPinPadBanorte.IdMerchant _
                                , Globales.oAmbientes.oPinPadBanorte.Modo _
                                , Globales.oAmbientes.oPinPadBanorte.UrlBanorte _
                                , Globales.oAmbientes.oPinPadBanorte.Caja)
                                Dim oSplash As New pdvPinPadSplash
                                oSplash.Mensaje = ""
                                oSplash.Accion = False
                                oSplash.PinAccion = pdvPinPadSplash.PinpadAccion.Cobro
                                oSplash.Impresora = Me.Impresora
                                oSplash.Total = txt_PagoRecibido.Text
                                oSplash.StartPosition = FormStartPosition.CenterScreen
                                oSplash.MesesDiferido = MesesDiferido
                                oSplash.NumPagos = NumPagos
                                oSplash.TipoPlan = TipoPlan
                                oSplash.txt_Empresa.Text = Empresa
                                oSplash.txt_RFC.Text = Rfc
                                oSplash.txt_Direccion.Text = Direccion
                                oSplash.txt_NoControl.Text = txt_NoControl.Text
                                oSplash.ShowDialog()
                                sw_continuar = oSplash.Accion
                                If sw_continuar Then
                                    Me.Enabled = True
                                    txt_Autorizacion.Text = oSplash.CodigoAut
                                    txt_Referencia.Text = oSplash.Referencia
                                    _Movimiento = oSplash.NoControl
                                    IdVoucher = oSplash.IdVoucher
                                Else
                                    txt_Autorizacion.Text = ""
                                    sw_continuar = False
                                    Me.Close()
                                    Mensaje = "PINPAD NO RESPONDE ADECUADAMENTE"
                                End If
                            End If
                        Case Else
                            If _SWRequiereAutorizacion Then
                                sw_continuar = False
                                Mensaje = "PINPAD NO CONFIGURADA"
                            Else
                                sw_continuar = True
                            End If
                    End Select
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "No Extiste Numero de control par Voucher.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

            If sw_continuar Then
                sPlanCredito = ""
                If Not cbo_PlanCredito.SelectedValue Is Nothing Then
                    sPlanCredito = cbo_PlanCredito.SelectedValue
                End If

                Referencia = IIf(cbo_FormaPago.SelectedValue = "DEVOLUCION", txt_Autonumsuc.Text, txt_Referencia.Text)
                Referencia = IIf(cbo_FormaPago.SelectedValue = "VALES", txt_Folio.Text, Referencia)
                _Movimiento = IIf(cbo_FormaPago.SelectedValue = "VALES", txt_IdClienteFinal.Text, _Movimiento)
                Autorizacion = txt_Autorizacion.Text

                If cbo_FormaPago.SelectedValue = "D. E." Then
                    Autorizacion = txt_CodigoCanje.Text
                End If

                If oDatos.PVTA_Inserta_Desglose(Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , Autonumsuc _
                                               , Caja _
                                               , Corte _
                                               , Cajero _
                                               , Tipo _
                                               , cbo_FormaPago.SelectedValue _
                                               , Referencia _
                                               , TCambio _
                                               , Importe _
                                               , "Operado" _
                                               , _Movimiento _
                                               , Autorizacion _
                                               , Caja _
                                               , IdVoucher _
                                               , sPlanCredito _
                                               , Cbo_Institucion.Text _
                                               , txt_IdClienteFinal.Text _
                                               , Mensaje) Then

                    If Importe * TCambio > ImporteTotal And Tipo = "EFECTIVO" Then
                        Cambio = Importe * TCambio - ImporteTotal
                        Cambio *= -1
                        If oDatos.PVTA_Inserta_Desglose(Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , Autonumsuc _
                                                   , Caja _
                                                   , Corte _
                                                   , Cajero _
                                                   , "CAMBIO" _
                                                   , MonedaNacional _
                                                   , "CAMBIO" _
                                                   , 1.0 _
                                                   , Cambio _
                                                   , "Operado" _
                                                   , _Movimiento _
                                                   , "" _
                                                   , Caja _
                                                   , 0 _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , Mensaje) Then
                        Else
                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                If Mensaje.StartsWith("[") Then
                    txt_Autorizacion.BackColor = Color.LightSalmon
                    txt_Autorizacion.Focus()
                Else
                    Me.Close()
                End If
            Else
                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub brn_Aceptar_Click(sender As Object, e As EventArgs) Handles brn_Aceptar.Click
        Try
            Call AceptarPago(False)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub brn_Cancelar_Click(sender As Object, e As EventArgs) Handles brn_Cancelar.Click
        Me.Close()
    End Sub

    Private Sub txt_TCambio_LostFocus(sender As Object, e As EventArgs) Handles txt_TCambio.LostFocus
        If txt_CodigoCanje.Visible Then
            txt_CodigoCanje.Focus()
        Else
            brn_Aceptar.Focus()
        End If
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    brn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select

            If Control.ModifierKeys = Keys.Alt Then
                Select Case e.KeyCode
                    Case Keys.P
                        e.Handled = True
                        cbo_FormaPago.Focus()
                    Case Keys.R
                        e.Handled = True
                        txt_PagoRecibido.Focus()
                    Case Keys.T
                        e.Handled = True
                        txt_TCambio.Focus()
                End Select
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub cbo_FormaPago_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbo_FormaPago.SelectedValueChanged
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim SFormaPAgo As String = ""

        Try
            oDatos = New Datos_Viscoi

            cbo_Bancos.Enabled = False
            txt_Referencia.Enabled = True
            txt_Autorizacion.Enabled = True
            GC_Recompensas.Enabled = True
            txt_PagoRecibido.ReadOnly = False
            cbo_PlanCredito.Enabled = False
            btn_AplicarVoucher.Enabled = False
            If Not cbo_FormaPago.SelectedValue Is Nothing Then
                SFormaPAgo = cbo_FormaPago.SelectedValue
            End If
            If oDatos.PVTA_Recupera_Monedas(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, SFormaPAgo, "", "SI", "NO", dtDatos, "", Msj) Then

                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("autorizacion") Or dtDatos.Rows(0).Item("referencia") Then
                        txt_Referencia.Enabled = dtDatos.Rows(0).Item("referencia")
                        txt_Autorizacion.Enabled = dtDatos.Rows(0).Item("referencia")
                        cbo_Bancos.Enabled = True
                    End If
                    If dtDatos.Rows(0).Item("efectivo") Then
                        cbo_Bancos.Enabled = False
                        txt_Referencia.Enabled = False
                        txt_Autorizacion.Enabled = False
                        txt_Autonumsuc.Enabled = False
                    End If
                    If dtDatos.Rows(0).Item("puntos") Then
                        cbo_Bancos.Enabled = False
                        txt_Referencia.Enabled = False
                        txt_Autorizacion.Enabled = False
                        txt_Autonumsuc.Enabled = False
                    End If
                    If dtDatos.Rows(0).Item("dinelect") Then
                        cbo_Bancos.Enabled = False
                        txt_Referencia.Enabled = False
                        txt_Autorizacion.Enabled = False
                        txt_Autonumsuc.Enabled = False
                        txt_PagoRecibido.ReadOnly = True
                    End If
                    If dtDatos.Rows(0).Item("moneda") = "DEVOLUCION" Then
                        cbo_Bancos.Enabled = False
                        txt_Referencia.Enabled = False
                        txt_Autorizacion.Enabled = False
                        txt_Autonumsuc.Enabled = True
                        txt_PagoRecibido.ReadOnly = True
                    End If
                    If dtDatos.Rows(0).Item("moneda") = "CREDITO" Then
                        cbo_PlanCredito.Enabled = True
                    End If
                    If dtDatos.Rows(0).Item("moneda") = "TDC" _
                    Or dtDatos.Rows(0).Item("moneda") = "TDB" _
                    Or dtDatos.Rows(0).Item("moneda") = "TDD" _
                    Or dtDatos.Rows(0).Item("tdc") Then
                        ''txt_PagoRecibido.ReadOnly = True
                        txt_NoControl.Text = "TRAN" & Globales.oAmbientes.Id_Sucursal.Substring(0, 4) & Format(Now, "yyyyMMddhhmmss")
                        txt_PagoRecibido.Text = txt_Total.Text
                        btn_AplicarVoucher.Enabled = True
                    End If
                End If
                    If SFormaPAgo <> "D. E." Then
                    GC_Recompensas.Enabled = False
                End If

                txt_TCambio.Text = Format(dtDatos.Rows(0).Item("valor"), "C2")
            End If

            If SFormaPAgo = MonedaNacional Then
                txt_TCambio.Enabled = False
            Else
                txt_TCambio.Enabled = False
            End If

            Dim stipoaux As String = ""
            If oDatos.Recupera_tiposcredito(Globales.oAmbientes.Id_Empresa, "TODOS", SFormaPAgo, dtDatos, Msj) Then
                cbo_PlanCredito.DataSource = dtDatos
                If dtDatos.Rows.Count > 0 Then
                    cbo_PlanCredito.SelectedIndex = 0
                Else
                    cbo_PlanCredito.SelectedIndex = -1
                    cbo_PlanCredito.Text = ""
                End If


                For Each sAux As DataRow In dtDatos.Rows
                    If sAux.Item("tipo").ToString.Substring(0, 2) = Condicion Then
                        stipoaux = sAux.Item("tipo")
                    End If
                Next
                If stipoaux <> "" Then
                    cbo_PlanCredito.SelectedValue = stipoaux
                End If
            Else
                cbo_PlanCredito.DataSource = Nothing
                cbo_PlanCredito.SelectedIndex = -1
                cbo_PlanCredito.Text = ""
            End If

            GC_Adicionales.Visible = IIf(cbo_FormaPago.Text <> "VALES", True, False)
            GC_Vale.Visible = IIf(cbo_FormaPago.Text = "VALES", True, False)
            If cbo_FormaPago.SelectedValue = "VALES" Then
                txt_IdClienteFinal.Text = ""
                txt_NombreClienteFinal.Text = ""
                brn_Aceptar.Enabled = False
            End If

            Me.Height = 0
            Me.Height = IIf(GC_General.Visible, GC_General.Height + 30, 0) + IIf(GC_Adicionales.Visible, GC_Adicionales.Height + 30, 0) + IIf(GC_Vale.Visible, GC_Vale.Height + 30, 0) + +IIf(GC_Recompensas.Visible, GC_Recompensas.Height + 30, 0) + +IIf(GC_Botones.Visible, GC_Botones.Height + 30, 0)

            ''Me.Height = IIf(GC_General.Visible, GC_General.Height, 0) + IIf(GC_Adicionales.Visible, GC_Adicionales.Height, 0) + IIf(GC_Recompensas.Visible, GC_Recompensas.Height, 0) + 108
            Me.CenterToScreen()
        Catch ex As Exception
            txt_TCambio.Text = Format(0.0, "C2")
        End Try
    End Sub

    Private Sub txt_Autonumsuc_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_Autonumsuc.KeyUp

        Try
            If e.KeyCode = 13 Then
                txt_PagoRecibido.Text = RecuperaASDevolucion(txt_Autonumsuc.Text)
            End If
        Catch ex As Exception
            txt_PagoRecibido.Text = "0.0"
        End Try
    End Sub
    Private Function RecuperaASDevolucion(ByVal AutonumSuc As String) As Double
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Msj As String = ""
        Dim Importe As Double = 0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Transacciones(CDate("01/01/1900"), CDate("01/01/1900"), 0, AutonumSuc, Globales.oAmbientes.Id_Empresa, "TODOS", "OPER", "", "", "", dtDatos, dtDatos1, dtDatos2, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("tipotrans") = "DEVUELTO" Then
                        Importe = Math.Abs(dtDatos.Rows(0).Item("importe"))
                    Else
                        Importe = 0.0
                    End If
                Else
                    MessageBox.Show("No se encotro AutonumSuc: " & AutonumSuc, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                Importe = 0.0
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            Importe = 0.0
        End Try
        Return Importe
    End Function

    Private Sub btn_AplicarVoucher_Click(sender As Object, e As EventArgs) Handles btn_AplicarVoucher.Click
        Try
            Call AceptarPago(True)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_CodigoCanje_LostFocus(sender As Object, e As EventArgs) Handles txt_CodigoCanje.LostFocus
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Dim Importe As Double = 0
        Dim ImporteDE As Double = 0
        Dim ImportePago As Double = 0

        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Monedero_Electronico_Sel(Globales.oAmbientes.Id_Empresa, txt_Cliente.Text, txt_CodigoCanje.Text, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_PagoRecibido.Text = Format(dtDatos.Rows(0).Item("importe"), "C2")
                    txt_ImporteDEMAX.Text = Format(dtDatos.Rows(0).Item("importe"), "C2")
                    Txt_DE_Observacion.Text = dtDatos.Rows(0).Item("observacion")

                    ImporteDE = CDbl(txt_ImporteDEMAX.Text)
                    ImportePago = CDbl(txt_Total.Text)
                    If ImporteDE > ImportePago Then
                        txt_PagoRecibido.Text = Format(ImportePago, "C2")
                    End If
                Else
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                Importe = 0.0
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Btn_Buscar_Click(sender As Object, e As EventArgs) Handles Btn_Buscar.Click
        Dim oFrm As pdvCuentasFinal
        Try
            oFrm = New pdvCuentasFinal
            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_IdClienteFinal.Text = oFrm.Cuenta
                txt_NombreClienteFinal.Text = oFrm.Nombre
                BuscarFinalCuenta(txt_IdClienteFinal.Text)

            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Btn_Nuevo_Click(sender As Object, e As EventArgs) Handles Btn_Nuevo.Click
        If Not Globales.oAmbientes.oUsuario.ValidaPermiso("pdvClienteFinal", "INSERTARDATOS") Then
            MessageBox.Show("El usuario no cuenta con acceso a la opción " & "pdvClienteFinal", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If
        Dim frmClientesFinal As New pdvClienteFinal
        frmClientesFinal.ShowDialog()
        If frmClientesFinal.txt_Cliente.Text <> "" Then
            txt_IdClienteFinal.Text = frmClientesFinal.txt_Cliente.Text
            Call BuscarFinalCuenta(txt_IdClienteFinal.Text)
        Else
            txt_IdClienteFinal.Text = ""
            txt_NombreClienteFinal.Text = ""
        End If
    End Sub
    Private Sub BuscarFinalCuenta(ByRef Tarjeta As String)
        Dim Msj As String = ""

        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim posExt As Integer
        Dim posInt As Integer
        Dim CodVer As String
        Try
            oDatos = New Datos_Viscoi
            brn_Aceptar.Enabled = False
            If oDatos.PVTA_Recupera_ClienteFinal(txt_IdClienteFinal.Text, "", "", "", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_IdClienteFinal.Text = dtDatos.Rows(0).Item("id_cliente").ToString.Trim
                    txt_NombreClienteFinal.Text = dtDatos.Rows(0).Item("nombre_cli").ToString.Trim
                    If dtDatos.Rows(0).Item("fax").ToString.Trim <> "@" Then
                        CodVer = InputBox("Se ha enviado un código de verificación al celular del cliente, solicitacelo y teclealo.", "Código de Verificación")
                        If CodVer = dtDatos.Rows(0).Item("fax").ToString.Trim Then
                            brn_Aceptar.Enabled = True
                        Else
                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "(" & CodVer & ") Código de Verificación no valido.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            txt_IdClienteFinal.Text = ""
                            txt_NombreClienteFinal.Text = ""
                        End If
                    Else
                        brn_Aceptar.Enabled = True
                    End If

                Else
                    txt_IdClienteFinal.Text = ""
                    txt_NombreClienteFinal.Text = ""
                End If
            Else
                txt_IdClienteFinal.Text = ""
                txt_NombreClienteFinal.Text = ""
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
End Class