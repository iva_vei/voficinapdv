﻿Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Public Class XtraRep_FacturaCuadros
    Public Empresa As String
    Public Direccion As String
    Public LugExpedicion As String
    Public FacCP As String
    Public Fecha As String
    Public Importe As String
    Public Letra As String
    Public SW_ImprimirDirecion As Boolean
    Public SW_Global As Boolean
    Private Sub XtraRep_Pedidos_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        XrPic_Logo.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\LogoEmpresa.jpg"))
    End Sub
    Private Sub XrPictureBox6_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub XrTableRow3_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub lbl_Empresa_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_EmpresaDireccion.BeforePrint
        lbl_EmpresaDireccion.Text = Direccion.Replace("|", vbNewLine)
    End Sub
    Private Sub lbl_Empresa_BeforePrint_1(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_Empresa.BeforePrint
        lbl_Empresa.CanShrink = True
        lbl_Empresa.Text = Empresa
    End Sub

    Private Sub XrLabel25_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel25.BeforePrint
        XrLabel25.Text = XrLabel25.Text.Replace("#EMPRESA#", Empresa).Replace("#LUGAR#", LugExpedicion).Replace("#FECHA#", Fecha).Replace("#IMPORTE#", Importe).Replace("#LETRA#", Letra)
    End Sub
    Private Sub XrLabel52_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel52.BeforePrint
        XrLabel52.Text = XrLabel52.Text.Replace("#LETRA#", Letra)
    End Sub

    Private Sub XrLabel49_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel49.BeforePrint
        XrLabel49.Text = FacCP
    End Sub

    Private Sub Direccion_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Lbl_Colonia.BeforePrint _
                                                                                    , lbl_CodigoP.BeforePrint _
                                                                                    , lbl_Ciudad.BeforePrint _
                                                                                    , lbl_Estado.BeforePrint
        Lbl_Direccion.Visible = SW_ImprimirDirecion
        Lbl_Colonia.Visible = SW_ImprimirDirecion
        lbl_CodigoP.Visible = SW_ImprimirDirecion
        lbl_Ciudad.Visible = SW_ImprimirDirecion
        lbl_Estado.Visible = SW_ImprimirDirecion
    End Sub

    Private Sub lbl_OC_CFDIREL_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_OC_CFDIREL.BeforePrint
        Try
            If lbl_TipoComprobante.Text = "NCREDITO" Then
                lbl_OC_CFDIREL.Text = "CFDI Relacionados"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub lbl_Proveedor_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_Proveedor.BeforePrint
        sender.text = sender.text.ToString.Replace("&apos;", "'").Replace("&amp;", "&")
    End Sub

    Private Sub lbl_EUniCtoNeto_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_EUniCtoNeto.BeforePrint
        sender.text = IIf(XrLabel16.Text = "PAGOS", "NUM PARC", sender.text)
    End Sub

    Private Sub XrLabel30_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_SubTotFact.BeforePrint

        If XrLabel16.Text = "PAGOS" Then
            lbl_SubTotPagos.Visible = True
            lbl_SubTotFact.Visible = False
        Else
            lbl_SubTotPagos.Visible = False
            lbl_SubTotFact.Visible = True
        End If
    End Sub

    'Private Sub ValoresAbsolutos_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_UniCtoNeto.BeforePrint _
    '                                                                                        , lbl_UniCtoIVA.BeforePrint _
    '                                                                                        , lbl_TotCtoNeto.BeforePrint _
    '                                                                                        , lbl_Descuento.BeforePrint _
    '                                                                                        , lbl_IVA_Impuesto.BeforePrint _
    '                                                                                        , lbl_IVA_tipofact.BeforePrint _
    '                                                                                        , lbl_Tasa_Iva.BeforePrint _
    '                                                                                        , lbl_Impuesto_IVA.BeforePrint _
    '                                                                                        , lbl_IVA.BeforePrint _
    '                                                                                        , XrLabel30.BeforePrint _
    '                                                                                        , lbl_TOTAL.BeforePrint
    '    Try
    '        Dim EtiquetaValor As DevExpress.XtraReports.UI.XRLabel
    '        EtiquetaValor = sender
    '        If CDbl(EtiquetaValor.Text) < 0.0 Then
    '            EtiquetaValor.Text = Math.Abs(CDbl(EtiquetaValor.Text))
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub
End Class