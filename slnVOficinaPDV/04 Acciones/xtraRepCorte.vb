﻿Imports System.Drawing.Printing

Public Class xtraRepCorte
    Public TipoCliente As String

    Private Sub xtraRepTicketVenta_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Pic_logo.ImageUrl = Application.StartupPath & "\imagenes\logoempresa.jpg"

    End Sub

    Private Sub DetailReport_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles DetailReport.BeforePrint
        If XrLabel5.Text = "------------:" Or XrLabel5.Text = "-----FIN----" Then
            XrLabel7.Visible = False
            XrLabel6.Visible = False
        Else
            XrLabel7.Visible = True
            XrLabel6.Visible = True
        End If
    End Sub
End Class