﻿Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Public Class XtraRep_CotizacionBase
    Public Empresa As String
    Public Direccion As String
    Public LugExpedicion As String
    Public Fecha As String
    Public Importe As String
    Public Letra As String
    Private Sub XtraRep_Pedidos_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Dim cColor As Color = Globales.oAmbientes.PaletaColores(0)
        XrLabel1.ForeColor = cColor
        lbl_Empresa.ForeColor = cColor
        lbl_EmpresaDireccion.ForeColor = cColor
        lbl_EIdRen.BackColor = cColor
        lbl_EMarca.BackColor = cColor
        lbl_EUniCtoBruto.BackColor = cColor
        lbl_EUniCtoNeto.BackColor = cColor
        lbl_EUniCtoIVA.BackColor = cColor
        lbl_ETotCtoNeto.BackColor = cColor
        lbl_EIVA.BackColor = cColor
        lbl_ETotal.BackColor = cColor
        lbl_ECliente.BackColor = cColor
        lblPrecioBruto.BackColor = cColor
        XrLabel1.BackColor = cColor
        XrLabel11.BackColor = cColor
        XrLabel12.BackColor = cColor
        XrLabel24.BackColor = cColor
        XrLabel16.BackColor = cColor
        XrLabel18.BorderColor = cColor
        XrLabel19.BorderColor = cColor
        XrLabel20.BackColor = cColor
        XrLabel21.BackColor = cColor
        XrLabel22.BorderColor = cColor
        XrLabel15.BorderColor = cColor
        XrLabel2.BorderColor = cColor
        Try
            XrPic_Logo.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\LogoEmpresa.jpg"))
            Me.Watermark.ImageSource = Nothing
            'Me.Watermark.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Factura_WaterMark.png"))
        Catch ex As Exception
        End Try
    End Sub
    Private Sub XrPictureBox6_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub XrTableRow3_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub lbl_Empresa_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_EmpresaDireccion.BeforePrint
        lbl_EmpresaDireccion.Text = Direccion.Replace("|", vbNewLine)
    End Sub
    Private Sub lbl_Empresa_BeforePrint_1(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_Empresa.BeforePrint
        lbl_Empresa.Text = Empresa
    End Sub

    Private Sub XrLabel25_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)
    End Sub

    Private Sub XrLabel22_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel22.BeforePrint
        XrLabel22.Text = LugExpedicion
    End Sub
End Class