﻿Imports System.Drawing.Printing

Public Class xtraRepTicketValeCarta
    Public SwReimpresion As Boolean = False
    Public SwDevolucion As Boolean = False
    Public TipoCliente As String
    Public MargenDer As Integer = 0
    Public MargenIzq As Integer = 0
    Public MargenArr As Integer = 0
    Public MargenAbj As Integer = 0
    Public Contenido As String = ""

    Private Sub xtraRepTicketVenta_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Me.Margins.Right = Me.Margins.Right + MargenDer
        Me.Margins.Left = Me.Margins.Left + MargenIzq
        Me.Margins.Top = Me.Margins.Top + MargenArr
        Me.Margins.Bottom = Me.Margins.Bottom + MargenAbj
        Try
            Pic_logo.ImageUrl = Application.StartupPath & "\imagenes\logoempresa.jpg"
            Me.Watermark.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Factura_WaterMark1.png"))
        Catch ex As Exception

        End Try
        If SwReimpresion Then
            Me.Watermark.Text = "REIMPRESION"
            Me.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal
            ''.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Reimpresion.png"))
        End If

        If TipoCliente = "CLIENTE" _
        Or TipoCliente = "MAYORISTA" Then
            GroupFooter2.Visible = True
            GroupFooter3.Visible = False
        Else
            GroupFooter2.Visible = False
            GroupFooter3.Visible = True
        End If

        If TipoCliente = "CLIENTE1" _
        Or TipoCliente = "MAYORISTA1" _
        Or TipoCliente = "1" _
        Or TipoCliente = "" Then
            GroupFooter2.Visible = False
            GroupFooter3.Visible = False
        End If

        GroupFooter2.Visible = False
        GroupFooter3.Visible = False

        GF_Devolucion.Visible = SwDevolucion

    End Sub

End Class