﻿Public Class pdvBuscaTransacciones
    Dim _Accion As Boolean
    Dim _TipoTran As eTipoTran
    Dim _Autonumsuc As String
    Dim _Estatus As String

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public ReadOnly Property Autonumsuc As String
        Get
            Return _Autonumsuc
        End Get
    End Property

    Public Property TipoTran As eTipoTran
        Get
            Return _TipoTran
        End Get
        Set(value As eTipoTran)
            _TipoTran = value
        End Set
    End Property

    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(value As String)
            _Estatus = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            dat_FechaIni.Value = Today.Date
            dat_FechaFin.Value = Today.Date
            Call Refrescar()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim sTipoTran As String = ""
        Dim Estatus As String = "CAPTURA"
        Try
            oDatos = New Datos_Viscoi
            sTipoTran = RadioGroup1.Properties.Items(RadioGroup1.SelectedIndex).Value
            Estatus = IIf(chk_SoloCaptura.Checked, "CAPTURA", "USADO")
            If oDatos.PVTA_Recupera_Tmp_Transacciones(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, dat_FechaIni.Value, dat_FechaFin.Value, "", sTipoTran, Estatus, dtDatos, Mensaje) Then
                GridC_1.DataSource = dtDatos
                GridC_1.RefreshDataSource()
                GridV_1.BestFitColumns()
            Else
                GridC_1.DataSource = Nothing
                GridC_1.RefreshDataSource()
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim sw_continuar As Boolean = True
        Dim Requeridos As Double = 0
        Dim Disponible As Double = 0

        Try
            _Accion = True
            If GridV_1.RowCount > 0 Then
                _Autonumsuc = GridV_1.GetFocusedRowCellValue(col1_Autonumsuc)
                _Estatus = GridV_1.GetFocusedRowCellValue(col1_Estatus)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Try
            Call Refrescar()
        Catch ex As Exception

        End Try
    End Sub
End Class