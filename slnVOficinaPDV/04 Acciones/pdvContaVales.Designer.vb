﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pdvContaVales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.gridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_IdCliente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdClienteFinal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ClienteFinal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ImporteVale = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_PagoQuincenal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_ContraVale = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Refrescar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_Empresa = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cbo_Sucursal = New System.Windows.Forms.ComboBox()
        Me.dat_FechaFin = New System.Windows.Forms.DateTimePicker()
        Me.dat_FechaIni = New System.Windows.Forms.DateTimePicker()
        Me.lbl_Tarjeta = New System.Windows.Forms.Label()
        Me.lbl_Cuenta = New System.Windows.Forms.Label()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        Me.EntClienteFinalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BandedGridColumn3 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BandedGridColumn4 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BandedGridColumn5 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BandedGridColumn6 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BandedGridColumn7 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Hora = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EntClienteFinalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(916, 389)
        Me.GroupControl2.TabIndex = 2
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(912, 364)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.gridBand1})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_IdClienteFinal, Me.col1_ClienteFinal, Me.col1_IdCliente, Me.col1_ImporteVale, Me.col1_ContraVale, Me.col1_PagoQuincenal})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'gridBand1
        '
        Me.gridBand1.Columns.Add(Me.col1_IdCliente)
        Me.gridBand1.Columns.Add(Me.col1_IdClienteFinal)
        Me.gridBand1.Columns.Add(Me.col1_ClienteFinal)
        Me.gridBand1.Columns.Add(Me.col1_ImporteVale)
        Me.gridBand1.Columns.Add(Me.col1_PagoQuincenal)
        Me.gridBand1.Columns.Add(Me.col1_ContraVale)
        Me.gridBand1.Name = "gridBand1"
        Me.gridBand1.VisibleIndex = 0
        Me.gridBand1.Width = 498
        '
        'col1_IdCliente
        '
        Me.col1_IdCliente.Caption = "IdCliente"
        Me.col1_IdCliente.FieldName = "idcliente"
        Me.col1_IdCliente.Name = "col1_IdCliente"
        Me.col1_IdCliente.OptionsColumn.AllowEdit = False
        Me.col1_IdCliente.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdCliente.Visible = True
        Me.col1_IdCliente.Width = 123
        '
        'col1_IdClienteFinal
        '
        Me.col1_IdClienteFinal.Caption = "IdClienteFinal"
        Me.col1_IdClienteFinal.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.col1_IdClienteFinal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col1_IdClienteFinal.FieldName = "IdClienteFinal"
        Me.col1_IdClienteFinal.Name = "col1_IdClienteFinal"
        Me.col1_IdClienteFinal.OptionsColumn.AllowEdit = False
        Me.col1_IdClienteFinal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdClienteFinal.Visible = True
        '
        'col1_ClienteFinal
        '
        Me.col1_ClienteFinal.Caption = "ClienteFinal"
        Me.col1_ClienteFinal.FieldName = "clientefinal"
        Me.col1_ClienteFinal.Name = "col1_ClienteFinal"
        Me.col1_ClienteFinal.OptionsColumn.AllowEdit = False
        Me.col1_ClienteFinal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ClienteFinal.Visible = True
        '
        'col1_ImporteVale
        '
        Me.col1_ImporteVale.Caption = "ImporteVale"
        Me.col1_ImporteVale.FieldName = "importevale"
        Me.col1_ImporteVale.Name = "col1_ImporteVale"
        Me.col1_ImporteVale.OptionsColumn.AllowEdit = False
        Me.col1_ImporteVale.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ImporteVale.Visible = True
        '
        'col1_PagoQuincenal
        '
        Me.col1_PagoQuincenal.Caption = "PagoQuincenal"
        Me.col1_PagoQuincenal.FieldName = "PagoQuincenal"
        Me.col1_PagoQuincenal.Name = "col1_PagoQuincenal"
        Me.col1_PagoQuincenal.OptionsColumn.AllowEdit = False
        Me.col1_PagoQuincenal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_PagoQuincenal.Visible = True
        '
        'col1_ContraVale
        '
        Me.col1_ContraVale.Caption = "ContraVale"
        Me.col1_ContraVale.FieldName = "ContraVale"
        Me.col1_ContraVale.Name = "col1_ContraVale"
        Me.col1_ContraVale.OptionsColumn.AllowEdit = False
        Me.col1_ContraVale.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_ContraVale.Visible = True
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.txt_Empresa)
        Me.GroupControl1.Controls.Add(Me.Label31)
        Me.GroupControl1.Controls.Add(Me.cbo_Sucursal)
        Me.GroupControl1.Controls.Add(Me.dat_FechaFin)
        Me.GroupControl1.Controls.Add(Me.dat_FechaIni)
        Me.GroupControl1.Controls.Add(Me.lbl_Tarjeta)
        Me.GroupControl1.Controls.Add(Me.lbl_Cuenta)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 389)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(916, 96)
        Me.GroupControl1.TabIndex = 3
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.Location = New System.Drawing.Point(664, 29)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(79, 42)
        Me.btn_Refrescar.TabIndex = 23
        Me.btn_Refrescar.Text = "Buscar..."
        Me.btn_Refrescar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(270, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 23)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Empresa:"
        '
        'txt_Empresa
        '
        Me.txt_Empresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.txt_Empresa.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa.FormattingEnabled = True
        Me.txt_Empresa.Items.AddRange(New Object() {"BANORTE"})
        Me.txt_Empresa.Location = New System.Drawing.Point(364, 29)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(284, 31)
        Me.txt_Empresa.TabIndex = 21
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(270, 63)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(88, 23)
        Me.Label31.TabIndex = 20
        Me.Label31.Text = "Sucursal:"
        '
        'cbo_Sucursal
        '
        Me.cbo_Sucursal.FormattingEnabled = True
        Me.cbo_Sucursal.Location = New System.Drawing.Point(364, 65)
        Me.cbo_Sucursal.Name = "cbo_Sucursal"
        Me.cbo_Sucursal.Size = New System.Drawing.Size(284, 21)
        Me.cbo_Sucursal.TabIndex = 19
        '
        'dat_FechaFin
        '
        Me.dat_FechaFin.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaFin.Location = New System.Drawing.Point(117, 63)
        Me.dat_FechaFin.Name = "dat_FechaFin"
        Me.dat_FechaFin.Size = New System.Drawing.Size(145, 30)
        Me.dat_FechaFin.TabIndex = 5
        '
        'dat_FechaIni
        '
        Me.dat_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaIni.Location = New System.Drawing.Point(117, 29)
        Me.dat_FechaIni.Name = "dat_FechaIni"
        Me.dat_FechaIni.Size = New System.Drawing.Size(145, 30)
        Me.dat_FechaIni.TabIndex = 4
        '
        'lbl_Tarjeta
        '
        Me.lbl_Tarjeta.AutoSize = True
        Me.lbl_Tarjeta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Tarjeta.Location = New System.Drawing.Point(5, 63)
        Me.lbl_Tarjeta.Name = "lbl_Tarjeta"
        Me.lbl_Tarjeta.Size = New System.Drawing.Size(85, 23)
        Me.lbl_Tarjeta.TabIndex = 3
        Me.lbl_Tarjeta.Text = "FechaFin"
        '
        'lbl_Cuenta
        '
        Me.lbl_Cuenta.AutoSize = True
        Me.lbl_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Cuenta.Location = New System.Drawing.Point(5, 29)
        Me.lbl_Cuenta.Name = "lbl_Cuenta"
        Me.lbl_Cuenta.Size = New System.Drawing.Size(88, 23)
        Me.lbl_Cuenta.TabIndex = 1
        Me.lbl_Cuenta.Text = "Fecha Ini"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 485)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(916, 84)
        Me.GroupControl3.TabIndex = 4
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(242, 26)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(481, 26)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'EntClienteFinalBindingSource
        '
        Me.EntClienteFinalBindingSource.DataSource = GetType(proyVOficina_PDV.entClienteFinal)
        '
        'BandedGridColumn3
        '
        Me.BandedGridColumn3.Caption = "IdCliente"
        Me.BandedGridColumn3.Name = "BandedGridColumn3"
        Me.BandedGridColumn3.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.BandedGridColumn3.Visible = True
        Me.BandedGridColumn3.Width = 123
        '
        'BandedGridColumn4
        '
        Me.BandedGridColumn4.Caption = "IdCliente"
        Me.BandedGridColumn4.Name = "BandedGridColumn4"
        Me.BandedGridColumn4.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.BandedGridColumn4.Visible = True
        Me.BandedGridColumn4.Width = 123
        '
        'BandedGridColumn5
        '
        Me.BandedGridColumn5.Caption = "IdCliente"
        Me.BandedGridColumn5.Name = "BandedGridColumn5"
        Me.BandedGridColumn5.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn5.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.BandedGridColumn5.Visible = True
        Me.BandedGridColumn5.Width = 123
        '
        'BandedGridColumn6
        '
        Me.BandedGridColumn6.Caption = "Fecha"
        Me.BandedGridColumn6.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.BandedGridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.BandedGridColumn6.FieldName = "fecha"
        Me.BandedGridColumn6.Name = "BandedGridColumn6"
        Me.BandedGridColumn6.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn6.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.BandedGridColumn6.Visible = True
        '
        'BandedGridColumn7
        '
        Me.BandedGridColumn7.Caption = "Fecha"
        Me.BandedGridColumn7.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.BandedGridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.BandedGridColumn7.FieldName = "fecha"
        Me.BandedGridColumn7.Name = "BandedGridColumn7"
        Me.BandedGridColumn7.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn7.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.BandedGridColumn7.Visible = True
        '
        'col1_Hora
        '
        Me.col1_Hora.Caption = "Hora"
        Me.col1_Hora.FieldName = "hora"
        Me.col1_Hora.Name = "col1_Hora"
        Me.col1_Hora.OptionsColumn.AllowEdit = False
        Me.col1_Hora.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Hora.Visible = True
        '
        'pdvContaVales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(916, 569)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Name = "pdvContaVales"
        Me.Text = "Conta Vales"
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EntClienteFinalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents gridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents col1_IdCliente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdClienteFinal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ClienteFinal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ImporteVale As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_PagoQuincenal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_ContraVale As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_Empresa As ComboBox
    Friend WithEvents Label31 As Label
    Friend WithEvents cbo_Sucursal As ComboBox
    Friend WithEvents dat_FechaFin As DateTimePicker
    Friend WithEvents dat_FechaIni As DateTimePicker
    Friend WithEvents lbl_Tarjeta As Label
    Friend WithEvents lbl_Cuenta As Label
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
    Friend WithEvents EntClienteFinalBindingSource As BindingSource
    Friend WithEvents BandedGridColumn3 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BandedGridColumn4 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BandedGridColumn5 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BandedGridColumn6 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BandedGridColumn7 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Hora As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents btn_Refrescar As Button
End Class
