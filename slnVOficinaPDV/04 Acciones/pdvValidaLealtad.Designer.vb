﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvValidaLealtad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GC_ProgramaLealtad = New DevExpress.XtraEditors.GroupControl()
        Me.txt_IdLealtad = New DevExpress.XtraEditors.TextEdit()
        Me.txt_TipoCliente = New DevExpress.XtraEditors.TextEdit()
        Me.txt_NombreCanje1 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Puntos = New DevExpress.XtraEditors.TextEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_DE = New DevExpress.XtraEditors.TextEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_NombreCanje = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_CodigoCanje = New DevExpress.XtraEditors.TextEdit()
        Me.DefaultToolTipController1 = New DevExpress.Utils.DefaultToolTipController(Me.components)
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        CType(Me.GC_ProgramaLealtad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_ProgramaLealtad.SuspendLayout()
        CType(Me.txt_IdLealtad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_TipoCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NombreCanje1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Puntos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_DE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NombreCanje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GC_ProgramaLealtad
        '
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_IdLealtad)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_TipoCliente)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_NombreCanje1)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_Puntos)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label1)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label6)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_DE)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label5)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_NombreCanje)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label4)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_CodigoCanje)
        Me.GC_ProgramaLealtad.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_ProgramaLealtad.Location = New System.Drawing.Point(0, 0)
        Me.GC_ProgramaLealtad.Name = "GC_ProgramaLealtad"
        Me.GC_ProgramaLealtad.Size = New System.Drawing.Size(514, 185)
        Me.GC_ProgramaLealtad.TabIndex = 1
        Me.GC_ProgramaLealtad.Text = "Programa Lealtad"
        Me.GC_ProgramaLealtad.ToolTipController = Me.DefaultToolTipController1.DefaultController
        '
        'txt_IdLealtad
        '
        Me.txt_IdLealtad.EditValue = ""
        Me.txt_IdLealtad.Location = New System.Drawing.Point(296, 23)
        Me.txt_IdLealtad.Name = "txt_IdLealtad"
        Me.txt_IdLealtad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IdLealtad.Properties.Appearance.Options.UseFont = True
        Me.txt_IdLealtad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IdLealtad.Size = New System.Drawing.Size(42, 30)
        Me.txt_IdLealtad.TabIndex = 17
        Me.txt_IdLealtad.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.EditValue = ""
        Me.txt_TipoCliente.Location = New System.Drawing.Point(453, 138)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Properties.Appearance.Options.UseFont = True
        Me.txt_TipoCliente.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_TipoCliente.Size = New System.Drawing.Size(42, 30)
        Me.txt_TipoCliente.TabIndex = 15
        Me.txt_TipoCliente.Visible = False
        '
        'txt_NombreCanje1
        '
        Me.txt_NombreCanje1.EditValue = ""
        Me.txt_NombreCanje1.Location = New System.Drawing.Point(454, 100)
        Me.txt_NombreCanje1.Name = "txt_NombreCanje1"
        Me.txt_NombreCanje1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreCanje1.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreCanje1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreCanje1.Size = New System.Drawing.Size(42, 30)
        Me.txt_NombreCanje1.TabIndex = 14
        Me.txt_NombreCanje1.Visible = False
        '
        'txt_Puntos
        '
        Me.txt_Puntos.Location = New System.Drawing.Point(169, 136)
        Me.txt_Puntos.Name = "txt_Puntos"
        Me.txt_Puntos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos.Properties.Appearance.Options.UseFont = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LimeGreen
        Me.txt_Puntos.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_Puntos.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos.Properties.AppearanceReadOnly.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.txt_Puntos.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Puntos.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.txt_Puntos.Properties.Mask.EditMask = "N0"
        Me.txt_Puntos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Puntos.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Puntos.Properties.ReadOnly = True
        Me.txt_Puntos.Size = New System.Drawing.Size(121, 32)
        Me.txt_Puntos.TabIndex = 3
        '
        'Label1
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label1, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 137)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 24)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Puntos"
        '
        'Label6
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label6, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 101)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(113, 24)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Impote D. E."
        '
        'txt_DE
        '
        Me.txt_DE.Location = New System.Drawing.Point(170, 98)
        Me.txt_DE.Name = "txt_DE"
        Me.txt_DE.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DE.Properties.Appearance.Options.UseFont = True
        Me.txt_DE.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txt_DE.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_DE.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DE.Properties.AppearanceReadOnly.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.txt_DE.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DE.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_DE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.txt_DE.Properties.Mask.EditMask = "C2"
        Me.txt_DE.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_DE.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_DE.Properties.ReadOnly = True
        Me.txt_DE.Size = New System.Drawing.Size(121, 32)
        Me.txt_DE.TabIndex = 2
        '
        'Label5
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label5, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(5, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 24)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Nombre"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt_NombreCanje
        '
        Me.txt_NombreCanje.Location = New System.Drawing.Point(169, 62)
        Me.txt_NombreCanje.Name = "txt_NombreCanje"
        Me.txt_NombreCanje.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreCanje.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreCanje.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreCanje.Size = New System.Drawing.Size(326, 30)
        Me.txt_NombreCanje.TabIndex = 1
        '
        'Label4
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label4, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 24)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Codigo Canje"
        '
        'txt_CodigoCanje
        '
        Me.txt_CodigoCanje.Location = New System.Drawing.Point(170, 26)
        Me.txt_CodigoCanje.Name = "txt_CodigoCanje"
        Me.txt_CodigoCanje.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CodigoCanje.Properties.Appearance.Options.UseFont = True
        Me.txt_CodigoCanje.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CodigoCanje.Size = New System.Drawing.Size(120, 30)
        Me.txt_CodigoCanje.TabIndex = 0
        '
        'DefaultToolTipController1
        '
        '
        '
        '
        Me.DefaultToolTipController1.DefaultController.KeepWhileHovered = True
        '
        'btn_Aceptar
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.btn_Aceptar, DevExpress.Utils.DefaultBoolean.[Default])
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(118, 29)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.btn_Cancelar, DevExpress.Utils.DefaultBoolean.[Default])
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(377, 27)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.AllowHtmlText = True
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 185)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(514, 76)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.ToolTipController = Me.DefaultToolTipController1.DefaultController
        '
        'pdvValidaLealtad
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me, DevExpress.Utils.DefaultBoolean.[Default])
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(514, 261)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GC_ProgramaLealtad)
        Me.KeyPreview = True
        Me.Name = "pdvValidaLealtad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Valida Canje Lealtad"
        CType(Me.GC_ProgramaLealtad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_ProgramaLealtad.ResumeLayout(False)
        Me.GC_ProgramaLealtad.PerformLayout()
        CType(Me.txt_IdLealtad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_TipoCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NombreCanje1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Puntos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_DE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NombreCanje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GC_ProgramaLealtad As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_Puntos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_DE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_NombreCanje As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_CodigoCanje As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_NombreCanje1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_TipoCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DefaultToolTipController1 As DevExpress.Utils.DefaultToolTipController
    Friend WithEvents txt_IdLealtad As DevExpress.XtraEditors.TextEdit
End Class
