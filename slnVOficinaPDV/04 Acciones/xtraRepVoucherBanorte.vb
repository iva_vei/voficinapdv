﻿Imports System.Drawing.Printing

Public Class xtraRepVoucherBanorte
    Public TipoCliente As String
    Public MargenDer As Integer = 0
    Public MargenIzq As Integer = 0
    Public MargenArr As Integer = 0
    Public MargenAbj As Integer = 0

    Private Sub xtraRepTicketVenta_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Me.Margins.Right = Me.Margins.Right + MargenDer
        Me.Margins.Left = Me.Margins.Left + MargenIzq
        Me.Margins.Top = Me.Margins.Top + MargenArr
        Me.Margins.Bottom = Me.Margins.Bottom + MargenAbj
        Pic_logo.ImageUrl = Application.StartupPath & "\imagenes\logoempresa.jpg"

    End Sub

    Private Sub XrLabel_MSI_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel_MSI.BeforePrint
        Dim oDatos As Globales.clsPinPadBanorte
        Try
            oDatos = ods.DataSource
            If oDatos.NumPagos.Trim <> "" Then
                XrLabel_MSI.Text = "Compra a " & oDatos.NumPagos & " Meses " & vbNewLine & oDatos.NomTipoPlan
                If oDatos.MesesDiferido.Trim <> "" Then
                    XrLabel_MSI.Text = XrLabel_MSI.Text & vbNewLine & "Diferido a " & oDatos.MesesDiferido & " meses"
                End If
            Else
                XrLabel_MSI.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class