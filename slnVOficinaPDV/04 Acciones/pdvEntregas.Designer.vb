﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvEntregas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1_DEscripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Cantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Valor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Importe = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Referencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RG_TipoCortes = New DevExpress.XtraEditors.RadioGroup()
        Me.txt_Total = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Corte = New System.Windows.Forms.TextBox()
        Me.txt_TCambio = New DevExpress.XtraEditors.TextEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbo_Moneda = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.Cbo_Bovedas = New System.Windows.Forms.ComboBox()
        Me.RG_Cajas = New DevExpress.XtraEditors.RadioGroup()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.RG_TipoCortes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_TCambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.RG_Cajas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.GridC_1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 71)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(559, 326)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Teclea los importes a entregar:"
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(555, 301)
        Me.GridC_1.TabIndex = 1
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Appearance.FocusedRow.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridV_1.Appearance.FocusedRow.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.GridV_1.Appearance.FocusedRow.Options.UseFont = True
        Me.GridV_1.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridV_1.Appearance.Row.Options.UseFont = True
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col1_DEscripcion, Me.col1_Cantidad, Me.col1_Valor, Me.col1_Importe, Me.col1_Referencia})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col1_DEscripcion
        '
        Me.col1_DEscripcion.Caption = "Descripción"
        Me.col1_DEscripcion.FieldName = "descripcion"
        Me.col1_DEscripcion.Name = "col1_DEscripcion"
        Me.col1_DEscripcion.OptionsColumn.AllowEdit = False
        Me.col1_DEscripcion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_DEscripcion.Visible = True
        Me.col1_DEscripcion.VisibleIndex = 0
        '
        'col1_Cantidad
        '
        Me.col1_Cantidad.Caption = "Cantidad"
        Me.col1_Cantidad.FieldName = "cantidad"
        Me.col1_Cantidad.Name = "col1_Cantidad"
        Me.col1_Cantidad.Visible = True
        Me.col1_Cantidad.VisibleIndex = 1
        '
        'col1_Valor
        '
        Me.col1_Valor.Caption = "Valor"
        Me.col1_Valor.FieldName = "valor"
        Me.col1_Valor.Name = "col1_Valor"
        '
        'col1_Importe
        '
        Me.col1_Importe.AppearanceCell.Options.UseTextOptions = True
        Me.col1_Importe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.col1_Importe.Caption = "Importe"
        Me.col1_Importe.DisplayFormat.FormatString = "C2"
        Me.col1_Importe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Importe.FieldName = "importe"
        Me.col1_Importe.Name = "col1_Importe"
        Me.col1_Importe.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Importe.Visible = True
        Me.col1_Importe.VisibleIndex = 2
        '
        'col1_Referencia
        '
        Me.col1_Referencia.Caption = "Referencia"
        Me.col1_Referencia.FieldName = "referencia"
        Me.col1_Referencia.Name = "col1_Referencia"
        Me.col1_Referencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Referencia.Visible = True
        Me.col1_Referencia.VisibleIndex = 3
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RG_TipoCortes)
        Me.Panel1.Controls.Add(Me.txt_Total)
        Me.Panel1.Controls.Add(Me.txt_Corte)
        Me.Panel1.Controls.Add(Me.txt_TCambio)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.cbo_Moneda)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txt_Cajero)
        Me.Panel1.Controls.Add(Me.txt_Caja)
        Me.Panel1.Controls.Add(Me.btn_Aceptar)
        Me.Panel1.Controls.Add(Me.btn_Cancelar)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 397)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(559, 212)
        Me.Panel1.TabIndex = 1
        '
        'RG_TipoCortes
        '
        Me.RG_TipoCortes.EditValue = "CORTE P"
        Me.RG_TipoCortes.Location = New System.Drawing.Point(18, 93)
        Me.RG_TipoCortes.Name = "RG_TipoCortes"
        Me.RG_TipoCortes.Properties.Columns = 4
        Me.RG_TipoCortes.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("CORTE P", "Entrega &Parcial", True, "CORTE", ""), New DevExpress.XtraEditors.Controls.RadioGroupItem("CORTE X", "Cambio de Corte (&X)", True, "CORTE", ""), New DevExpress.XtraEditors.Controls.RadioGroupItem("CORTE Z", "Entrega Final (&Z)", True, "CORTE", "")})
        Me.RG_TipoCortes.Size = New System.Drawing.Size(529, 58)
        Me.RG_TipoCortes.TabIndex = 20
        '
        'txt_Total
        '
        Me.txt_Total.Location = New System.Drawing.Point(283, 39)
        Me.txt_Total.Name = "txt_Total"
        Me.txt_Total.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.Appearance.Options.UseFont = True
        Me.txt_Total.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Total.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Total.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Total.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Total.Properties.Mask.EditMask = "C2"
        Me.txt_Total.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Total.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Total.Properties.ReadOnly = True
        Me.txt_Total.Size = New System.Drawing.Size(262, 48)
        Me.txt_Total.TabIndex = 19
        '
        'txt_Corte
        '
        Me.txt_Corte.Location = New System.Drawing.Point(319, 169)
        Me.txt_Corte.Name = "txt_Corte"
        Me.txt_Corte.Size = New System.Drawing.Size(30, 20)
        Me.txt_Corte.TabIndex = 18
        Me.txt_Corte.Visible = False
        '
        'txt_TCambio
        '
        Me.txt_TCambio.Location = New System.Drawing.Point(395, 4)
        Me.txt_TCambio.Name = "txt_TCambio"
        Me.txt_TCambio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TCambio.Properties.Appearance.Options.UseFont = True
        Me.txt_TCambio.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_TCambio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_TCambio.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_TCambio.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TCambio.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_TCambio.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_TCambio.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_TCambio.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_TCambio.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_TCambio.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_TCambio.Properties.Mask.EditMask = "C2"
        Me.txt_TCambio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_TCambio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_TCambio.Properties.ReadOnly = True
        Me.txt_TCambio.Size = New System.Drawing.Size(150, 30)
        Me.txt_TCambio.TabIndex = 15
        Me.txt_TCambio.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(279, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(118, 24)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "&Tipo Cambio"
        Me.Label8.Visible = False
        '
        'cbo_Moneda
        '
        Me.cbo_Moneda.DisplayMember = "moneda"
        Me.cbo_Moneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Moneda.FormattingEnabled = True
        Me.cbo_Moneda.Location = New System.Drawing.Point(123, 4)
        Me.cbo_Moneda.Name = "cbo_Moneda"
        Me.cbo_Moneda.Size = New System.Drawing.Size(150, 32)
        Me.cbo_Moneda.TabIndex = 14
        Me.cbo_Moneda.ValueMember = "tcambio"
        Me.cbo_Moneda.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 24)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "&Moneda"
        Me.Label1.Visible = False
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Location = New System.Drawing.Point(283, 169)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(30, 20)
        Me.txt_Cajero.TabIndex = 13
        Me.txt_Cajero.Visible = False
        '
        'txt_Caja
        '
        Me.txt_Caja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txt_Caja.Location = New System.Drawing.Point(247, 169)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(30, 20)
        Me.txt_Caja.TabIndex = 12
        Me.txt_Caja.Visible = False
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(146, 157)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 10
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(355, 157)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 11
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(18, 42)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(260, 38)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Total a Entregar"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.Cbo_Bovedas)
        Me.GroupControl2.Controls.Add(Me.RG_Cajas)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(559, 71)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Tipo de Caja"
        '
        'Cbo_Bovedas
        '
        Me.Cbo_Bovedas.DisplayMember = "valor"
        Me.Cbo_Bovedas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cbo_Bovedas.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cbo_Bovedas.FormattingEnabled = True
        Me.Cbo_Bovedas.Location = New System.Drawing.Point(214, 31)
        Me.Cbo_Bovedas.Name = "Cbo_Bovedas"
        Me.Cbo_Bovedas.Size = New System.Drawing.Size(331, 32)
        Me.Cbo_Bovedas.TabIndex = 22
        Me.Cbo_Bovedas.ValueMember = "id"
        '
        'RG_Cajas
        '
        Me.RG_Cajas.EditValue = "CORTE P"
        Me.RG_Cajas.Location = New System.Drawing.Point(2, 23)
        Me.RG_Cajas.Name = "RG_Cajas"
        Me.RG_Cajas.Properties.Columns = 3
        Me.RG_Cajas.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("CAJA ACTUAL", "Caja Actual", True, "CORTE", ""), New DevExpress.XtraEditors.Controls.RadioGroupItem("BOVEDA", "Boveda", True, "CORTE", "")})
        Me.RG_Cajas.Size = New System.Drawing.Size(311, 46)
        Me.RG_Cajas.TabIndex = 21
        '
        'pdvEntregas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(559, 609)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvEntregas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entregas"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.RG_TipoCortes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_TCambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.RG_Cajas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1_DEscripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Importe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents txt_TCambio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As Label
    Friend WithEvents cbo_Moneda As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_Corte As TextBox
    Friend WithEvents txt_Total As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RG_TipoCortes As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents col1_Referencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Cantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Valor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Cbo_Bovedas As ComboBox
    Friend WithEvents RG_Cajas As DevExpress.XtraEditors.RadioGroup
End Class
