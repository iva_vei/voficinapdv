﻿Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Public Class XtraRep_FacturaBase
    Public Empresa As String
    Public Direccion As String
    Public LugExpedicion As String
    Public FacCP As String
    Public Fecha As String
    Public Importe As String
    Public Letra As String
    Public SW_Global As Boolean
    Private Sub XtraRep_Pedidos_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Dim cColor As Color = Globales.oAmbientes.PaletaColores(0)
        lbl_Empresa.ForeColor = cColor
        lbl_EmpresaDireccion.ForeColor = cColor
        lbl_EIdRen.BackColor = cColor
        lbl_EMarca.BackColor = cColor
        lbl_EUniCtoBruto.BackColor = cColor
        lbl_EUniCtoNeto.BackColor = cColor
        lbl_EUniCtoIVA.BackColor = cColor
        lbl_ETotCtoNeto.BackColor = cColor
        lbl_ESubtotal.BackColor = cColor
        lbl_EIVA.BackColor = cColor
        lbl_ETotal.BackColor = cColor
        XrLabel3.BackColor = cColor
        XrLabel14.BackColor = cColor
        lbl_ECliente.BackColor = cColor
        XrLabel43.BackColor = cColor
        XrLabel1.BackColor = cColor
        XrLabel11.BackColor = cColor
        XrLabel12.BackColor = cColor
        XrLabel24.BackColor = cColor
        XrLabel16.BackColor = cColor
        XrLabel17.BorderColor = cColor
        XrLabel18.BorderColor = cColor
        XrLabel19.BorderColor = cColor
        XrLabel20.BackColor = cColor
        XrLabel21.BackColor = cColor
        XrLabel28.BackColor = cColor
        XrLabel31.BackColor = cColor
        XrLabel34.BackColor = cColor
        XrLabel35.BackColor = cColor

        XrLabel22.BorderColor = cColor
        XrLabel15.BorderColor = cColor
        XrLabel44.BorderColor = cColor
        XrLabel2.BorderColor = cColor
        XrLabel26.BorderColor = cColor
        XrLabel27.BorderColor = cColor
        XrLabel32.BorderColor = cColor
        XrLabel33.BorderColor = cColor
        XrLabel36.BorderColor = cColor

        XrPic_Logo.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\LogoEmpresa.jpg"))
        Me.Watermark.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Factura_WaterMark.png"))
    End Sub
    Private Sub XrPictureBox6_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub XrTableRow3_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs)

    End Sub
    Private Sub lbl_Empresa_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_EmpresaDireccion.BeforePrint
        lbl_EmpresaDireccion.Text = Direccion.Replace("|", vbNewLine)
    End Sub
    Private Sub lbl_Empresa_BeforePrint_1(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_Empresa.BeforePrint
        lbl_Empresa.Text = Empresa
    End Sub

    Private Sub XrLabel25_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel25.BeforePrint
        XrLabel25.Text = XrLabel25.Text.Replace("#EMPRESA#", Empresa).Replace("#LUGAR#", LugExpedicion).Replace("#FECHA#", Fecha).Replace("#IMPORTE#", Importe).Replace("#LETRA#", Letra)
    End Sub

    Private Sub XrLabel22_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel22.BeforePrint
        XrLabel22.Text = LugExpedicion
    End Sub

End Class