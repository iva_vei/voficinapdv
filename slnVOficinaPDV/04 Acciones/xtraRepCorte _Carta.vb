﻿Imports System.Drawing.Printing

Public Class xtraRepCorte_Carta
    Public SwReimpresion As Boolean = False
    Public TipoCliente As String

    Private Sub xtraRepTicketVenta_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Pic_logo.ImageUrl = Application.StartupPath & "\imagenes\logoempresa.jpg"
        If SwReimpresion Then
            Me.Watermark.Text = "REIMPRESION"
            Me.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal
            ''.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Reimpresion.png"))
        End If
    End Sub

    Private Sub DetailReport_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles DetailReport.BeforePrint
        If XrLabel5.Text = "------------:" Or XrLabel5.Text = "-----FIN----" Then
            XrLabel7.Visible = False
            XrLabel6.Visible = False
        Else
            XrLabel7.Visible = True
            XrLabel6.Visible = True
        End If
        'If Globales.oAmbientes.Valor(XrLabel6.Text) <> 0 Then
        '    XrLabel6.Visible = True
        'Else
        '    XrLabel6.Visible = False
        'End If
    End Sub

    Private Sub XrLabel6_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel6.BeforePrint

    End Sub
End Class