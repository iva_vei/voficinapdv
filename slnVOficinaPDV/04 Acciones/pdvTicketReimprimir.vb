﻿Public Class pdvTicketReimprimir
    Private NumCopias As Integer

    Private Sub pdvTicketReimprimir_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatosSuc As DataTable = Nothing
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""

        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Dim PDatCaja As String = ""

        Try
            oDatos = New Datos_Viscoi
            Dim pd As New Printing.PrintDocument
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer
            txt_Empresa.Text = Globales.oAmbientes.Id_Empresa
            If oDatos.PVTA_Recupera_Sucursales(txt_Empresa.Text, "", dtDatosSuc, Msj) Then
                cbo_Sucursal.DataSource = dtDatosSuc
                cbo_Sucursal.SelectedIndex = 0
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    PDatCaja = slinea.Substring(30, 4)
                    Exit For
                End If
            Next
            cbo_Sucursal.Text = PDatSucursal

            NumCopias = 1
            If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text, "", PDatCaja, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                    txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper
                    NumCopias = Globales.oAmbientes.Valor(dtDatos.Rows(0).Item("printername2"))
                    NumCopias = IIf(NumCopias = 0, 1, NumCopias)
                End If
            End If

            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                lstImpresoras.Enabled = True
            Else
                lstImpresoras.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        Try
            If txt_Autonumsuc.Text <> "" Then
                ImprimirTicket(txt_FormatoTicket.Text)
            Else
                MessageBox.Show("Debes teclar un Folio para reimprimir", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ImprimirTicket(ByVal Formato As String)
        Dim Reporte As Object
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_Transacciones(CDate("1900-01-01") _
                                               , CDate("1900-01-01") _
                                               , 0 _
                                               , txt_Autonumsuc.Text _
                                               , txt_Empresa.Text _
                                               , cbo_Sucursal.Text _
                                               , "OPER" _
                                               , "" _
                                               , "" _
                                               , "" _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , dtDatos2 _
                                               , Mensaje) Then
                If dtDatos1.Rows.Count > 0 Then
                    txt_Empresa1.Text = dtDatos1.Rows(0).Item("empresa")
                    txt_RFC.Text = dtDatos1.Rows(0).Item("rfc")
                    txt_Direccion.Text = dtDatos1.Rows(0).Item("direccion")
                    txt_Direccion2.Text = dtDatos2.Rows(0).Item("direccion")

                    txt_Cajero.Text = dtDatos.Rows(0).Item("cajero")
                    txt_Caja.Text = dtDatos.Rows(0).Item("caja")
                    txt_TipoCliente.Text = dtDatos.Rows(0).Item("referencia")

                    If oDatos.PVTA_Recupera_DetVentas_Impresion(CDate("1900-01-01") _
                                              , txt_Autonumsuc.Text _
                                               , txt_Empresa.Text _
                                               , cbo_Sucursal.Text _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , Mensaje) Then


                        oTicket = New entTicketVenta
                        oTicket.Fill(dtDatos, dtDatos1, Nothing)

                        oTicket.Empresa = txt_Empresa1.Text
                        oTicket.Rfc = txt_RFC.Text
                        oTicket.Direccion = txt_Direccion.Text
                        oTicket.Direccion2 = txt_Direccion2.Text
                        oTicket.Cajero = txt_Cajero.Text
                        oTicket.Caja = txt_Caja.Text
                        ''If txt_NombreCanje.Text.Length > 0 Then
                        ''    oTicket.Lealtad_nombre = txt_NombreCanje.Text
                        ''    oTicket.Puntos_saldo_ant = PuntosActual
                        ''    oTicket.Puntos_acumulados = PuntosAcumulados
                        ''    oTicket.Puntos_utilizados = PuntosUtilizados
                        ''    oTicket.Puntos_saldo_act = PuntosFinal
                        ''
                        ''    oTicket.De_saldo_ant = DEActual
                        ''    oTicket.De_acumulados = DEAcumulados
                        ''    oTicket.De_utilizados = DEUtilizados
                        ''    oTicket.De_saldo_act = DEFinal
                        ''Else
                        ''    oTicket.Lealtad_nombre = 0
                        ''    oTicket.Puntos_saldo_ant = 0
                        ''    oTicket.Puntos_acumulados = 0
                        ''    oTicket.Puntos_utilizados = 0
                        ''    oTicket.Puntos_saldo_act = 0
                        ''
                        ''    oTicket.De_saldo_ant = 0
                        ''    oTicket.De_acumulados = 0
                        ''    oTicket.De_utilizados = 0
                        ''    oTicket.De_saldo_act = 0
                        ''End If

                        Dim MargenAba As Integer = 0
                        Dim MargenIzq As Integer = 0
                        Dim MargenDer As Integer = 0
                        Dim MargenArr As Integer = 0

                        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
                        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
                        Select Case Formato
                            Case "CARTA"
                                Reporte = New xtraRepTicketVentaCarta
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVentaCarta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVentaCarta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVentaCarta).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVentaCarta).SwReimpresion = False
                                TryCast(Reporte, xtraRepTicketVentaCarta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVentaCarta).PrintingSystem)

                            Case "SEAFON"
                                Reporte = New xtraRepTicketVenta_Seafon
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).SwReimpresion = True
                                TryCast(Reporte, xtraRepTicketVenta_Seafon).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta_Seafon).PrintingSystem)
                            Case "EPSON"
                                Reporte = New xtraRepTicketVenta
                                TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVenta).SwReimpresion = True
                                TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                            Case Else
                                Reporte = New xtraRepTicketVenta
                                TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                                TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                                TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                                TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                                TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text
                                TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                                TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                        End Select
                        If Formato = "CARTA" Then
                            printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                            printTool.ShowPreviewDialog()
                        Else
                            ''lstImpresoras.SelectedIndex = 5
                            For iI = 0 To NumCopias - 1
                                printBase.Print(lstImpresoras.Text)
                            Next
                        End If

                        If oDatos.PVTA_Recupera_Desglose1(txt_Autonumsuc.Text _
                                                , txt_Empresa.Text _
                                                , cbo_Sucursal.Text _
                                                , CDate("01/01/1900") _
                                                , "" _
                                                , "" _
                                                , "CREDITO" _
                                                , dtDatos _
                                                , Mensaje) Then
                            If dtDatos.Rows.Count > 0 Then
                                If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                                        , txt_Autonumsuc.Text _
                                                        , txt_Empresa.Text _
                                                        , cbo_Sucursal.Text _
                                                        , dtDatos _
                                                        , Mensaje) Then
                                    If dtDatos.Rows.Count > 0 Then
                                        Dim oPagare As New entPagare
                                        oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                        oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                        oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                        oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                        oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                        oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                        oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                        oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                        oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                        oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                        Select Case Formato
                                            Case "SEAFON"
                                                Reporte = New xtraRepTicketPagare_Seafon
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare_Seafon).PrintingSystem)
                                            Case "EPSON"
                                                Reporte = New xtraRepTicketPagare
                                                TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                            Case Else
                                                Reporte = New xtraRepTicketPagare
                                                TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                        End Select
                                        For iI = 0 To NumCopias - 1
                                            printBase.Print(lstImpresoras.Text)
                                        Next

                                    End If
                                End If
                            End If
                        End If

                        If oDatos.PVTA_Recupera_Desglose1(txt_Autonumsuc.Text _
                                                , txt_Empresa.Text _
                                                , cbo_Sucursal.Text _
                                                , CDate("01/01/1900") _
                                                , "" _
                                                , "" _
                                                , "VALES" _
                                                , dtDatos _
                                                , Mensaje) Then
                            If dtDatos.Rows.Count > 0 Then
                                If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                                        , txt_Autonumsuc.Text _
                                                        , txt_Empresa.Text _
                                                        , cbo_Sucursal.Text _
                                                        , dtDatos _
                                                        , Mensaje) Then
                                    If dtDatos.Rows.Count > 0 Then
                                        Dim oPagare As New entPagare
                                        oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                        oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                        oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                        oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                        oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                        oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                        oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                        oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                        oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                        oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                        Select Case Formato
                                            Case "SEAFON"
                                                Reporte = New xtraRepTicketPagare_Seafon
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare_Seafon).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare_Seafon).PrintingSystem)
                                            Case "EPSON"
                                                Reporte = New xtraRepTicketPagare
                                                TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                            Case Else
                                                Reporte = New xtraRepTicketPagare
                                                TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                                TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                                TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                                TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                                TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                                TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                                printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                        End Select
                                        For iI = 0 To NumCopias - 1
                                            printBase.Print(lstImpresoras.Text)
                                        Next

                                    End If
                                End If
                            End If
                        End If

                    Else
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                End If
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class