﻿Public Class pdvLstPrecios
    Dim _Autonumsuc As String
    Dim _Renglon As Integer

    Public Property Autonumsuc As String
        Get
            Return _Autonumsuc
        End Get
        Set(value As String)
            _Autonumsuc = value
        End Set
    End Property

    Public Property Renglon As Integer
        Get
            Return _Renglon
        End Get
        Set(value As Integer)
            _Renglon = value
        End Set
    End Property

    Private Sub SBtnPublico_Click(sender As Object, e As EventArgs) Handles btn_publico.Click
        Call LstPreciosOpc(1) ' 1 PUB, 2 MAY
    End Sub

    Private Sub CkBoxTodos_CheckedChanged(sender As Object, e As EventArgs) Handles CkBoxTodos.CheckedChanged
        If CkBoxTodos.Checked Then
            Txt_RenglonCantidad.Text = "0"
            Txt_RenglonCantidad.Enabled = False
        Else
            Txt_RenglonCantidad.Enabled = True
        End If
    End Sub

    Private Sub btn_mayoreo_Click(sender As Object, e As EventArgs) Handles btn_mayoreo.Click
        Call LstPreciosOpc(2) ' 1 PUB, 2 MAY
    End Sub

    Private Sub LstPreciosOpc(Tipo As Integer)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi

        Renglon = Txt_RenglonCantidad.Text

        Try
            If oDatos.LstPrecios(Globales.oAmbientes.Id_Empresa _
                               , Globales.oAmbientes.Id_Sucursal _
                               , Autonumsuc _
                               , Renglon _
                               , Tipo _
                               , Msj) Then
                If Msj.ToUpper <> "EXITO" Then
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                Me.Close()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Private Sub pdvLstPrecios_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class