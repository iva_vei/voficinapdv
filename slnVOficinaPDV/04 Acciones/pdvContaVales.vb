﻿
Imports VOficinaTimbrador
Imports VOficinaTimbrador40

Public Class pdvContaVales
    Private Sub pdvContraVale_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatosSuc As DataTable = Nothing
        Dim Msj As String = ""

        Dim DatosConexion() As String
        Dim PDatSucursal As String = ""
        Dim sCaja As String = ""

        Try
            dat_FechaIni.Value = Today
            dat_FechaFin.Value = Today

            oDatos = New Datos_Viscoi


            txt_Empresa.Text = Globales.oAmbientes.Id_Empresa
            If oDatos.PVTA_Recupera_Sucursales(txt_Empresa.Text, "", dtDatosSuc, Msj) Then
                cbo_Sucursal.DataSource = dtDatosSuc
                cbo_Sucursal.SelectedIndex = 0
            End If

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    PDatSucursal = slinea.Substring(14, 12)
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    Exit For
                End If
            Next
            cbo_Sucursal.Text = PDatSucursal


            'cbo_Sucursal.Text = Globales.oAmbientes.Id_Sucursal
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_ContravaleRpt(Globales.oAmbientes.Id_Empresa, cbo_Sucursal.Text, dat_FechaIni.Value, dat_FechaFin.Value, dtDatos, Mensaje) Then
                If dtDatos IsNot Nothing AndAlso dtDatos.Rows.Count > 0 Then
                    ' Configurar columnas del grid si no están configuradas
                    If GridC_1.DataSource Is Nothing Then
                        GridC_1.DataSource = dtDatos
                        GridV_1.PopulateColumns()

                        ' Opcional: renombrar las columnas si es necesario
                        'GridV_1.Columns("col1_IdClienteFinal").Caption = "id_ciente"
                        'GridV_1.Columns("col1_ClienteFinal").Caption   = "id_clientefinal"
                        'GridV_1.Columns("col1_IdCliente").Caption      = "cliente_final"
                        'GridV_1.Columns("col1_ImporteVale").Caption    = "importe_vale"
                        'GridV_1.Columns("col1_ContraVale").Caption     = "pago_quincenal"
                        'GridV_1.Columns("col1_PagoQuincenal").Caption  = "contravale"
                    Else
                        ' Actualizar los datos del grid
                        GridC_1.DataSource = dtDatos
                    End If
                    GridC_1.RefreshDataSource()
                    GridV_1.BestFitColumns()
                Else
                    GridC_1.DataSource = Nothing '
                    GridC_1.RefreshDataSource()
                    MessageBox.Show("No se encontraron datos en la Tabla")
                End If
            Else
                MessageBox.Show("Error al obtener datos: " & Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            oDatos = Nothing
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Me.Close()
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs) Handles btn_Refrescar.Click
        Call Refrescar()
    End Sub
End Class
