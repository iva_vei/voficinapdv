﻿Imports DevExpress.XtraBars
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid

Public Enum eTipoTran
    VENTA
    MOSTRADOR
    COTIZACION
    VALES
    PRESTAMOS
End Enum
Public Class pdvPuntoVenta
    Public TipoTran As eTipoTran
    Public TecladoAtajos As List(Of Principal.clsTecladoAtajos)
    Private TamanoNombre As Integer
    Private HiloPedidosWeb As System.Threading.Thread
    Private sw_timer As Boolean
    Private MensajeNotificacion As String
    Private PedidosPendientes As Integer
    Private iTimer As Integer
    Private NumCopias As Integer

    Private Modo As Globales.TipoABC
    Private selectedRow As Integer = 0
    Private sw_datosalmacenados As Boolean = False

    Public Sub New(ByVal pTipoTran As eTipoTran)
        TipoTran = pTipoTran
        InitializeComponent()
        ''Me.layoutControl.OptionsFocus.EnableAutoTabOrder = False
        TamanoNombre = 300
        Me.GridV_1.Appearance.Row.ForeColor = System.Drawing.Color.Black

        txt_PuntosActual.Visible = False
        txt_PuntosAcumulados.Visible = False
        txt_PuntosUtilizados.Visible = False
        txt_PuntosFinal.Visible = False
        lbl_PuntosActual.Visible = False
        lbl_PuntosAcumulados.Visible = False
        lbl_PuntosUtilizados.Visible = False
        lbl_PuntosFinal.Visible = False

        txt_DinEleActual.Visible = False
        txt_DinEleAcumulados.Visible = False
        txt_DinEleUtilizados.Visible = False
        txt_DinEleFinal.Visible = False
        txt_DinEleActual.Visible = False
        txt_DinEleAcumulados.Visible = False
        txt_DinEleUtilizados.Visible = False
        txt_DinEleFinal.Visible = False

        CG_Ayuda.Visible = False
    End Sub
    Private Sub ABCPantallas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim pd As New Printing.PrintDocument
            Dim Impresoras As String
            Dim btnContado As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Pagar")
            Dim oDatos As Datos_Viscoi
            Dim dtDatos As DataTable = Nothing
            Dim Mensaje As String = ""

            oDatos = New Datos_Viscoi
            Globales.oAmbientes.ImagenAvisosTicket = ""
            If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "PDV", "", "IMAGENAVISOSTICKET", dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then
                    Globales.oAmbientes.ImagenAvisosTicket = dtDatos.Rows(0).Item("valor")
                End If
            End If

            ' Default printer      
            Dim s_Default_Printer As String = pd.PrinterSettings.PrinterName

            Select Case TipoTran
                Case eTipoTran.VENTA
                    Me.Text = "VENTA"
                Case eTipoTran.COTIZACION
                    Me.Text = "COTIZACION"
                Case eTipoTran.MOSTRADOR
                    Me.Text = "NOTA MOSTRADOR"
                Case Else
                    Me.Text = TipoTran.ToString
            End Select
            ' recorre las impresoras instaladas  
            For Each Impresoras In Printing.PrinterSettings.InstalledPrinters
                lstImpresoras.Items.Add(Impresoras.ToString)
            Next
            ' selecciona la impresora predeterminada  
            lstImpresoras.Text = s_Default_Printer

            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                lstImpresoras.Enabled = True
            Else
                lstImpresoras.Enabled = False
            End If

            If TipoTran <> eTipoTran.VENTA Then
                CG_ProgramaLealtad.Visible = False
                btnContado.Visible = False
            Else
                CG_ProgramaLealtad.Visible = True
                btnContado.Visible = True
            End If

            If Globales.oAmbientes.oUsuario.Id_usuario = "ICC" Then
                col1_Nombre.OptionsColumn.AllowEdit = True
            Else
                col1_Nombre.OptionsColumn.AllowEdit = False
            End If

            GridC_2.DataSource = TecladoAtajos
            GridC_2.RefreshDataSource()
            TamanoNombre = 400

            sw_timer = False
            HiloPedidosWeb = New System.Threading.Thread(AddressOf PedidosWeb)
            MensajeNotificacion = "VOficina 5+"
            PedidosPendientes = 0
            iTimer = 0


        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Ayuda()
        Try
            Call ControlBotones("Ayuda")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_NuevaVenta()
        Try
            Call ControlBotones("Nuevo")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Lectura()
        Try
            Call ControlBotones("Codigo")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_EliminarPartida()
        Try
            Call ControlBotones("Eliminar")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_LST_PRECIO()
        Try
            Call ControlBotones("LISTA PRECIOS")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Descto_Global()
        Try
            Call ControlBotones("Descto Global")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Descto_Partida()
        Try
            Call ControlBotones("Descto Partida")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_PrecioDirecto_Partida()
        Try
            Call ControlBotones("PrecioDirecto Partida")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_PagarVenta()
        Try
            If TipoTran = eTipoTran.VENTA Then
                Call ControlBotones("Contado")
            Else
                MessageBox.Show("No se puede recibir pago en COTIZACIONES / VENTAS DE MOSTRADOR (" & txt_Estatus.Text.ToUpper & ").", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_BuscarArticulo()
        Try
            Call ControlBotones("Articulos")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_TerminarVenta()
        Try
            Call ControlBotones("Terminar")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Facturar()
        Try
            Call ControlBotones("Facturar")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_DatosGenerales()
        Try
            Call ControlBotones("Datos Generales")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_VTAMostrador()
        Try
            Call VTAMostrador()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub PVTA_Fraccionar()
        Try
            Call Fraccionar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub Refrescar()
        txt_Codigo.Enabled = IIf(txt_Estatus.Text.ToUpper = "CAPTURA" Or txt_Estatus.Text.ToUpper = "", True, False)
        Call Mostrar_Transaccion()
        Call Mostrar_DetVentas()
    End Sub
    Public Sub Imprimir()
        Dim Formularios As Globales.clsFormularios
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing

        Dim oTicket As entTicketVenta
        Try

            ''If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "IMPRIMIR") Then
            ''    MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            ''    Return
            ''End If

            ''Formularios = New Globales.clsFormularios
            ''Formularios.Imprimir(Me.Name, "Formulario Plantilla", GridC_1)
            If TipoTran = eTipoTran.VENTA Then
                ImprimirTicket(txt_Autonumsuc.Text, txt_FormatoTicket.Text, "")
            Else
                Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
                Dim Mensaje As String = ""
                Dim oDatos_Supply As New Datos_Viscoi

                Try
                    If txt_Autonumsuc.Text.Length > 0 Then
                        If MessageBox.Show("¿Desea imprimir?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            oDatos = New Datos_Viscoi
                            If oDatos.PVTA_Recupera_DetVentas_Cotizacion(CDate("01/01/1900") _
                                                  , txt_Autonumsuc.Text _
                                                   , Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , dtDatos _
                                                   , dtDatos1 _
                                                   , dtDatos2 _
                                                   , Mensaje) Then
                                If TipoTran = eTipoTran.COTIZACION Then
                                    oTicket = New entTicketVenta
                                    oTicket.Fill(dtDatos, dtDatos1, dtDatos2)
                                    Dim ForamtoOC As New XtraRep_CotizacionBase
                                    ForamtoOC.Empresa = oTicket.FacEmpresa
                                    ForamtoOC.Direccion = oTicket.FacDireccion
                                    ForamtoOC.LugExpedicion = oTicket.LugExpedicion
                                    ForamtoOC.Fecha = oTicket.Fecha
                                    ForamtoOC.Importe = 0.0
                                    ForamtoOC.Letra = ""
                                    ForamtoOC.odsDatosTicket.DataSource = oTicket
                                    printTool = New DevExpress.XtraReports.UI.ReportPrintTool(ForamtoOC)
                                    printTool.ShowPreviewDialog()
                                    ForamtoOC.ExportToPdf("C:\Windows\Temp\" & txt_Autonumsuc.Text & ".pdf")
                                Else
                                    Dim Reporte1 As Object
                                    Dim printBase As DevExpress.XtraPrinting.PrintToolBase
                                    oTicket = New entTicketVenta
                                    oTicket.Fill(dtDatos, dtDatos1, dtDatos2)
                                    oTicket.Empresa = txt_Empresa.Text
                                    oTicket.Rfc = txt_RFC.Text
                                    oTicket.Direccion = txt_Direccion.Text
                                    oTicket.Direccion2 = txt_Direccion2.Text
                                    oTicket.Cajero = txt_Cajero.Text
                                    oTicket.Caja = txt_Caja.Text

                                    Select Case txt_FormatoTicket.Text
                                        Case "SEAFON"
                                            Reporte1 = New xtraRepTicketCortoVenta_Seafon
                                            TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).MargenAbj = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).MargenIzq = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).MargenDer = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).MargenArr = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                                            TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).ods.DataSource = oTicket
                                            TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).CreateDocument()
                                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte1, xtraRepTicketCortoVenta_Seafon).PrintingSystem)
                                            printBase.PrinterSettings.Copies = 2
                                            printBase.Print(lstImpresoras.Text)

                                        Case "EPSON"
                                            Reporte1 = New xtraRepTicketCortoVenta
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenAbj = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenIzq = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenDer = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenArr = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).ods.DataSource = oTicket
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).CreateDocument()
                                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte1, xtraRepTicketCortoVenta).PrintingSystem)
                                            printBase.PrinterSettings.Copies = 2
                                            printBase.Print(lstImpresoras.Text)

                                        Case Else
                                            Reporte1 = New xtraRepTicketCortoVenta
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenAbj = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenIzq = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenDer = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).MargenArr = 0
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).ods.DataSource = oTicket
                                            TryCast(Reporte1, xtraRepTicketCortoVenta).CreateDocument()
                                            printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte1, xtraRepTicketCortoVenta).PrintingSystem)
                                            printBase.PrinterSettings.Copies = 2
                                            printBase.Print(lstImpresoras.Text)

                                    End Select
                                    ''printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                                    ''printTool.ShowPreviewDialog()
                                End If

                            End If
                        End If
                    End If
                Catch ex As Exception
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
                Formularios = Nothing
                oDatos_Supply = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Excel()
        Dim Formularios As Globales.clsFormularios
        Try
            ''If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "EXCEL") Then
            ''    MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            ''    Return
            ''End If

            Formularios = New Globales.clsFormularios
            Formularios.ExportarXLS(Me.Name, "Formulario Plantilla", GridC_1, Globales.oAmbientes.Sistema, Application.ProductName, "ICC")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Formularios = Nothing
    End Sub
    Public Sub Regresar()
        Try
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Botones(ByVal tag As String)
        'Dim btnNuevo As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Nuevo")
        'Dim btnEditar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Editar")
        'Dim btnEliminar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Eliminar")
        'Dim btnOk As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Ok")
        'Dim btnCancelar As DevExpress.XtraEditors.ButtonPanel.BaseButton = PanelFiltros_Acciones.Buttons.Item("Cancelar")
        '
        'Select Case tag
        '    Case "Nuevo", "Editar", "Eliminar", "Consulta"
        '        btnNuevo.Visible = False
        '        btnEditar.Visible = False
        '        btnEliminar.Visible = False
        '        btnOk.Visible = True
        '        btnCancelar.Visible = True
        '
        '    Case "Ok", "Cancelar", "Consulta"
        '        btnNuevo.Visible = True
        '        btnEditar.Visible = True
        '        btnEliminar.Visible = True
        '        btnOk.Visible = False
        '        btnCancelar.Visible = False
        'End Select
    End Sub
    Private Sub ControlBotones(ByVal tag As String)
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim pass As String = ""
        Dim IdUsuario As Integer = 0

        ''Dim Total As Double
        ''Dim PagoContado As Double
        ''Dim PagoDE As Double
        ''Dim PagoTotal As Double
        Try
            Select Case tag.ToUpper
                Case "AYUDA"
                    CG_Ayuda.Visible = If(CG_Ayuda.Visible, False, True)
                Case "ARTICULOS"
                    Dim oFrm As SelArticulos
                    oFrm = New SelArticulos
                    oFrm.StartPosition = FormStartPosition.CenterParent
                    oFrm.ShowDialog()
                    txt_Codigo.Text = txt_Codigo.Text & oFrm.txt_Articulo.Text
                    txt_Codigo.Focus()
                    If txt_Codigo.Text.Length > 0 Then
                        txt_Codigo.Select(0, txt_Codigo.Text.Length)
                    End If
                Case "TERMINAR"
                    Select Case TipoTran
                        Case eTipoTran.VENTA
                            Call Terminar()
                        Case Else
                            Call TerminarNoVenta()
                    End Select
                Case "NUEVO"
                    ''If Not Globales.oAmbientes.oUsuario.ValidaPermiso(Me.Name, "INSERTARDATOS") Then
                    ''    MessageBox.Show("El usuario no cuenta con acceso a la opción " & Me.Name, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ''    Return
                    ''End If

                    Modo = Globales.TipoABC.Nuevo

                    If txt_Autonumsuc.Text <> "" Then
                        If TipoTran = eTipoTran.VENTA Then
                            If MessageBox.Show("Esta transacción se grabara en la auditoria." & vbNewLine & "La transacción actual se cancelara" & vbNewLine & "¿Desea iniciar nueva venta?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = DialogResult.Yes Then
                                GridC_1.DataSource = Nothing
                                GridC_1.RefreshDataSource()

                                If oDatos.PVTA_Recupera_Desglose(txt_Autonumsuc.Text _
                                                                    , Globales.oAmbientes.Id_Empresa _
                                                                    , Globales.oAmbientes.Id_Sucursal _
                                                                    , CDate("01/01/1900") _
                                                                    , "" _
                                                                    , "" _
                                                                    , "TDC" _
                                                                    , dtDatos _
                                                                    , Msj) Then
                                    If dtDatos.Rows.Count > 0 Then
                                        Dim oSplash As pdvPinPadSplash
                                        Dim sw_continuar As Boolean = True
                                        'For Each dtRenglon As DataRow In dtDatos.Rows
                                        '    If dtRenglon.Item("referencia").ToString.Trim <> "" And dtRenglon.Item("movimiento").ToString.Trim <> "" Then
                                        '        oSplash = New pdvPinPadSplash
                                        '        oSplash.PinAccion = pdvPinPadSplash.PinpadAccion.Reversa
                                        '        oSplash.Impresora = lstImpresoras.Text
                                        '        oSplash.Total = txt_Total.Text
                                        '        oSplash.StartPosition = FormStartPosition.CenterScreen
                                        '        oSplash.MesesDiferido = ""
                                        '        oSplash.NumPagos = ""
                                        '        oSplash.TipoPlan = ""
                                        '        oSplash.Referencia = dtRenglon.Item("referencia").ToString.Trim
                                        '        oSplash.NoControl = dtRenglon.Item("movimiento").ToString.Trim
                                        '        oSplash.ShowDialog()
                                        '        sw_continuar = IIf(Not oSplash.Accion, oSplash.Accion, sw_continuar)
                                        '    End If
                                        'Next
                                        If sw_continuar Then
                                            If oDatos.PVTA_Transacciones_Estatus(Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , txt_Autonumsuc.Text _
                                                   , "Cancelado" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , Msj) Then

                                                txt_Autonumsuc.Text = ""
                                                Call Crear_Transaccion()
                                            Else
                                                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                            End If
                                        End If
                                    Else
                                        If oDatos.PVTA_Transacciones_Estatus(Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , txt_Autonumsuc.Text _
                                                   , "Cancelado" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , Msj) Then

                                            txt_Autonumsuc.Text = ""
                                            Call Crear_Transaccion()
                                        Else
                                            MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                        End If
                                    End If


                                Else
                                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If
                            End If
                        Else
                            GridC_1.DataSource = Nothing
                            GridC_1.RefreshDataSource()

                            txt_Autonumsuc.Text = ""
                            Call Crear_Transaccion()
                        End If
                    Else
                        Call Crear_Transaccion()
                    End If
                    Call Refrescar()
                    Call ControlBotones("Codigo")
                Case "CODIGO"
                    txt_Codigo.Focus()
                    If txt_Codigo.Text.Length > 0 Then
                        txt_Codigo.Select(0, txt_Codigo.Text.Length)
                    End If

                    Call Refrescar()
                Case "ELIMINAR"
                    Call ELiminar_Detventas()
                Case "CONTADO"
                    Call Pagar(0)
                Case "D.E."
                    Call Pagar(1)
                Case "LISTA PRECIOS"
                    Call AplicaDescuentoLista()
                Case "DESCTO GLOBAL"
                    Call AplicaDescuentos(False)
                Case "DESCTO PARTIDA"

                    Dim Mensaje As String = ""

                    If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "PDV", "", "TIPO_SISTEMA_COTIZADOR", dtDatos, Mensaje) Then
                        If dtDatos.Rows.Count > 0 Then
                            Globales.oAmbientes.TipoEmpresa = dtDatos.Rows(0).Item("valor")

                            If (Globales.oAmbientes.TipoEmpresa = "FINANCIERA") Then
                                Call AplicaCotizador(True)
                            Else
                                Call AplicaDescuentos(True)
                            End If
                        End If
                    End If
                Case "PRECIODIRECTO PARTIDA"
                        Call AplicaPrecioDirecto(True)
                Case "FACTURAR"
                    Call DatosFacturar()
                Case "DATOS GENERALES"
                    Call DatosGenerales()
            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub PanelFiltros_Acciones_Click(sender As Object, e As EventArgs) Handles PanelFiltros_Acciones.ButtonClick
        Try
            Dim tag As String = DirectCast(CType(e, Docking2010.ButtonEventArgs).Button, DevExpress.XtraEditors.ButtonPanel.BaseButton).Tag
            Call ControlBotones(tag)
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs)
        Call ControlBotones("Consulta")
    End Sub
    Private Sub Crear_Transaccion()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim IdUsuario As Integer = 0
        Dim Autonumsuc As String = ""
        Dim Corte As Integer = 0

        Dim ofrmGral As pdvDatosGenerales
        Try
            txt_PuntosActual.Visible = False
            txt_PuntosAcumulados.Visible = False
            txt_PuntosUtilizados.Visible = False
            txt_PuntosFinal.Visible = False
            lbl_PuntosActual.Visible = False
            lbl_PuntosAcumulados.Visible = False
            lbl_PuntosUtilizados.Visible = False
            lbl_PuntosFinal.Visible = False

            txt_DinEleActual.Visible = False
            txt_DinEleAcumulados.Visible = False
            txt_DinEleUtilizados.Visible = False
            txt_DinEleFinal.Visible = False
            txt_DinEleActual.Visible = False
            txt_DinEleAcumulados.Visible = False
            txt_DinEleUtilizados.Visible = False
            txt_DinEleFinal.Visible = False

            oDatos = New Datos_Viscoi
            If Not oDatos.PVTA_Recupera_Corte_Estatus(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, txt_Cajero.Text, Corte, Mensaje) Then
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Application.Exit()
            End If
            txt_Corte.Text = Corte.ToString

            Autonumsuc = txt_Autonumsuc.Text

            txt_PuntosActual.Text = "0"
            txt_PuntosAcumulados.Text = "0"
            txt_PuntosUtilizados.Text = "0"
            txt_PuntosFinal.Text = "0"

            txt_DinEleActual.Text = "0"
            txt_DinEleAcumulados.Text = "0"
            txt_DinEleUtilizados.Text = "0"
            txt_DinEleFinal.Text = "0"

            ofrmGral = New pdvDatosGenerales(TipoTran)
            ofrmGral.IdEmpresa = txt_Empresa.Text
            ofrmGral.IdSucursal = txt_Sucursal.Text
            ofrmGral.Caja = txt_Caja.Text
            ofrmGral.Cajero = txt_Cajero.Text
            ofrmGral.Impresora = lstImpresoras.Text
            ofrmGral.TecladoAtajos = TecladoAtajos
            ofrmGral.FormaLlama = Me
            ofrmGral.MensajeNotificacion = txt_MensajeNotificacion
            ofrmGral.ShowDialog()
            If ofrmGral.Accion Then
                TipoTran = ofrmGral.TipoTran
                txt_NombreCanje.Text = ""
                txt_Vendedor1.Text = ofrmGral.txt_Vendedor1.Text
                txt_Vendedor2.Text = ofrmGral.txt_Vendedor2.Text
                txt_Cliente.Text = ofrmGral.txt_Cliente.Text.Trim()
                txt_ClienteNombre.Text = ofrmGral.txt_ClienteNombre.Text
                txt_ClienteCondicion.Text = ofrmGral.txt_ClienteCondicion.Text
                txt_Pediddo.Text = ofrmGral.txt_pedido.Text
                txt_CodigoCanje.Text = ofrmGral.txt_CodigoCanje.Text
                txt_TipoCliente.Text = ofrmGral.txt_TipoCliente.Text
                txt_ClienteTipo.Text = ofrmGral.txt_ClienteTipo.Text
                txt_ClienteEstatus.Text = ofrmGral.txt_ClienteEstatus.Text
                If ofrmGral.txt_ClienteDescto.Text = "" Then
                    txt_ClienteDescto.Text = "0.0"
                    txt_DesctoGlobal.Text = "0.0"
                Else
                    If txt_ClienteEstatus.Text = "NORM" Or txt_ClienteEstatus.Text = "ORO" Or txt_ClienteEstatus.Text = "ACTIVO" Then
                        txt_ClienteDescto.Text = Double.Parse(ofrmGral.txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint) / 100.0
                        txt_DesctoGlobal.Text = Double.Parse(ofrmGral.txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint) / 100.0
                    Else
                        MessageBox.Show("El estatus del cliente no es NORM u ORO el cliente no cuenta con beneficio de descuento.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        txt_ClienteDescto.Text = "0"
                        txt_DesctoGlobal.Text = "0"
                    End If
                End If

                txt_IdClienteFinal.Text = ofrmGral.txt_IdClienteFinal.Text
                txt_NombreClienteFinal.Text = ofrmGral.txt_NombreClienteFinal.Text
                txt_Folio.Text = ofrmGral.txt_Folio.Text

                txt_NombreCanje.Text = ofrmGral.txt_NombreCanje.Text
                txt_IdLealtad.Text = ofrmGral.txt_IdLealtad.Text
                txt_Referencia.Text = ofrmGral.txt_Referencia.Text
                txt_PlanCredito.Text = txt_IdClienteFinal.Text ''ofrmGral.cbo_PlanCredito.SelectedValue

                If txt_TipoCliente.Text = "CLIENTE" _
                Or txt_TipoCliente.Text = "MAYORISTA" Then
                    txt_PuntosFinal.Visible = True
                    lbl_PuntosFinal.Visible = True
                    txt_PuntosUtilizados.Visible = True
                    lbl_PuntosUtilizados.Visible = True
                    txt_PuntosAcumulados.Visible = True
                    lbl_PuntosAcumulados.Visible = True
                    txt_PuntosActual.Visible = True
                    lbl_PuntosActual.Visible = True

                    lbl_DinEleActual.Visible = False
                    txt_DinEleActual.Visible = False
                    lbl_DinEleAcumulados.Visible = False
                    txt_DinEleAcumulados.Visible = False
                    lbl_DinEleUtilizados.Visible = False
                    txt_DinEleUtilizados.Visible = False
                    lbl_DinEleFinal.Visible = False
                    txt_DinEleFinal.Visible = False
                Else
                    txt_PuntosActual.Visible = False
                    txt_PuntosAcumulados.Visible = False
                    txt_PuntosUtilizados.Visible = False
                    txt_PuntosFinal.Visible = False
                    lbl_PuntosActual.Visible = False
                    lbl_PuntosAcumulados.Visible = False
                    lbl_PuntosUtilizados.Visible = False
                    lbl_PuntosFinal.Visible = False

                    txt_DinEleFinal.Visible = True
                    lbl_DinEleFinal.Visible = True
                    txt_DinEleUtilizados.Visible = True
                    lbl_DinEleUtilizados.Visible = True
                    txt_DinEleAcumulados.Visible = True
                    lbl_DinEleAcumulados.Visible = True
                    txt_DinEleActual.Visible = True
                    lbl_DinEleActual.Visible = True
                End If

                txt_PuntosActual.Text = ofrmGral.txt_Puntos.Text
                txt_DinEleActual.Text = ofrmGral.txt_DE.Text

                txt_Estatus.Text = ofrmGral.Estatus


                If ofrmGral.Autonumusc = "" Then
                    CG_ProgramaLealtad.Visible = IIf(txt_CodigoCanje.Text <> "", True, False)

                    col1_DEOtorgago.Visible = CG_ProgramaLealtad.Visible
                    col1_DEPOtorgado.Visible = CG_ProgramaLealtad.Visible

                    If oDatos.PVTA_Inserta_Transaccion(Autonumsuc _
                                                   , Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , txt_Caja.Text _
                                                   , Today.Date _
                                                   , Format(Now(), "hh:mm:ss") _
                                                   , txt_Caja.Text _
                                                   , 0 _
                                                   , txt_Cajero.Text _
                                                   , TipoTran.ToString() _
                                                   , txt_Pediddo.Text _
                                                   , txt_Vendedor1.Text _
                                                   , IIf(txt_Cliente.Text <> "", txt_Cliente.Text.Trim(), txt_IdLealtad.Text) _
                                                   , txt_PlanCredito.Text.Trim() _
                                                   , 0, 0 _
                                                   , "" _
                                                   , txt_ClienteNombre.Text _
                                                   , Globales.oAmbientes.oUsuario.Id_usuario _
                                                   , 0, 0, 0, 0 _
                                                   , IIf(TipoTran = eTipoTran.VENTA, txt_TipoCliente.Text, txt_Referencia.Text) _
                                                   , "CAPTURA" _
                                                   , "VENTA" _
                                                   , txt_ClienteNombre.Text, "", txt_Vendedor2.Text _
                                                   , 0, 0, 0, 0 _
                                                   , 0.0, 0.0, 0.0 _
                                                   , 0, 0.0, 0.0, 0.0 _
                                                   , IIf(txt_IdLealtad.Text <> "", "LEALTAD", ""), Mensaje) Then
                        GridC_1.DataSource = dtDatos
                        txt_Autonumsuc.Text = Autonumsuc

                        Dim DesctoGlobal As Decimal
                        If txt_ClienteDescto.Text <> "" Then
                            DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                        End If

                        If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , Autonumsuc _
                                        , False _
                                        , DesctoGlobal _
                                        , 0 _
                                        , 0 _
                                        , 0 _
                                        , Mensaje) Then

                        Else
                            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If

                        Select Case TipoTran
                            Case eTipoTran.VENTA
                                If txt_TipoCliente.Text <> "CLIENTE" Then
                                    If ofrmGral.txt_NombreCanje1.Text <> "" Then
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_NombreCanje1.Text.Trim() <> "", "Hola " & ofrmGral.txt_NombreCanje1.Text.Trim() & "!!!", "Hola!!!")
                                    Else
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_ClienteNombre.Text.Trim() <> "", "Hola " & ofrmGral.txt_Cliente.Text.Trim() & "-" & ofrmGral.txt_ClienteNombre.Text.Trim() & "!!!", "Hola!!!")
                                    End If
                                Else
                                    If ofrmGral.txt_NombreCanje1.Text <> "" Then
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_NombreCanje1.Text.Trim() <> "", "Hola " & ofrmGral.txt_NombreCanje1.Text.Trim() & "!!!", "Hola!!!")
                                    Else
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_ClienteNombre.Text.Trim() <> "", "Hola " & ofrmGral.txt_Cliente.Text.Trim() & "-" & ofrmGral.txt_ClienteNombre.Text.Trim() & "!!!", "Hola!!!")
                                    End If
                                End If
                            Case eTipoTran.MOSTRADOR
                                lbl_NombreCanje.Text = Autonumsuc & " - " & txt_Referencia.Text
                                If ofrmGral.txt_NombreCanje1.Text <> "" Then
                                    lbl_NombreCanje.Text = lbl_NombreCanje.Text + " - " + IIf(ofrmGral.txt_NombreCanje1.Text <> "", "Hola " & ofrmGral.txt_NombreCanje1.Text & "!!!", "Hola!!!")
                                Else
                                    lbl_NombreCanje.Text = lbl_NombreCanje.Text + " - " + IIf(ofrmGral.txt_ClienteNombre.Text <> "", "Hola " & ofrmGral.txt_Cliente.Text.Trim() & "-" & ofrmGral.txt_ClienteNombre.Text & "!!!", "Hola!!!")
                                End If
                            Case eTipoTran.COTIZACION
                                lbl_NombreCanje.Text = "COTIZACION: " & Autonumsuc
                        End Select
                        If ofrmGral.txt_PedidoDet.Text <> "" Then
                            Dim Lector() As String = ofrmGral.txt_PedidoDet.Text.Split("|")
                            If Lector.Length > 0 Then
                                For iI = 0 To Lector.Length - 1
                                    txt_Codigo.Text = Lector(iI)
                                    Call Inserta_DetVentas()
                                Next
                            End If
                        End If
                        txt_Codigo.Focus()
                    End If
                Else
                    ''txt_Autonumsuc.Text = ofrmGral.Autonumusc
                    If ofrmGral.Estatus.ToUpper = "CAPTURA" Then

                        Select Case TipoTran
                            Case eTipoTran.VENTA
                                If txt_TipoCliente.Text <> "CLIENTE" Then
                                    If ofrmGral.txt_NombreCanje1.Text <> "" Then
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_NombreCanje1.Text.Trim() <> "", "Hola " & ofrmGral.txt_NombreCanje1.Text.Trim() & "!!!", "Hola!!!")
                                    Else
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_ClienteNombre.Text.Trim() <> "", "Hola " & ofrmGral.txt_Cliente.Text.Trim() & "-" & ofrmGral.txt_ClienteNombre.Text.Trim() & "!!!", "Hola!!!")
                                    End If
                                Else
                                    If ofrmGral.txt_NombreCanje1.Text <> "" Then
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_NombreCanje1.Text.Trim() <> "", "Hola " & ofrmGral.txt_NombreCanje1.Text.Trim() & "!!!", "Hola!!!")
                                    Else
                                        lbl_NombreCanje.Text = IIf(ofrmGral.txt_ClienteNombre.Text.Trim() <> "", "Hola " & ofrmGral.txt_Cliente.Text.Trim() & "-" & ofrmGral.txt_ClienteNombre.Text.Trim() & "!!!", "Hola!!!")
                                    End If
                                End If
                            Case eTipoTran.MOSTRADOR
                                lbl_NombreCanje.Text = Autonumsuc & " - " & txt_Referencia.Text
                            Case eTipoTran.COTIZACION
                                lbl_NombreCanje.Text = "COTIZACION: " & Autonumsuc
                        End Select

                        If oDatos.PVTA_Inserta_Transaccion(Autonumsuc _
                                                       , Globales.oAmbientes.Id_Empresa _
                                                       , Globales.oAmbientes.Id_Sucursal _
                                                       , txt_Caja.Text _
                                                       , Today.Date _
                                                       , Format(Now(), "hh:mm:ss") _
                                                       , txt_Caja.Text _
                                                       , 0 _
                                                       , txt_Cajero.Text _
                                                       , TipoTran.ToString() _
                                                       , txt_Pediddo.Text _
                                                       , txt_Vendedor1.Text _
                                                       , txt_Cliente.Text.Trim() _
                                                       , "" _
                                                       , 0, 0 _
                                                       , txt_ClienteNombre.Text _
                                                       , Globales.oAmbientes.Id_Sucursal _
                                                       , Globales.oAmbientes.oUsuario.Id_usuario _
                                                       , 0, 0, 0, 0 _
                                                       , IIf(txt_IdLealtad.Text <> "", "LEALTAD", "") _
                                                       , "CAPTURA" _
                                                       , "VENTA" _
                                                       , "", "", txt_Vendedor2.Text _
                                                       , 0, 0, 0, 0 _
                                                       , 0.0, 0.0, 0.0 _
                                                       , 0, 0.0, 0.0, 0.0 _
                                                       , IIf(TipoTran = eTipoTran.VENTA, txt_TipoCliente.Text, txt_Referencia.Text), Mensaje) Then

                            txt_Autonumsuc.Text = Autonumsuc
                            If ofrmGral.Estatus.ToUpper = "CAPTURA" Then
                                If oDatos.PVTA_Inserta_TmpTransacciones(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, ofrmGral.Autonumusc, txt_Autonumsuc.Text, Mensaje) Then
                                    If oDatos.PVTA_Recupera_Transacciones(Today.Date _
                                                           , Today.Date _
                                                           , 0 _
                                                           , txt_Autonumsuc.Text _
                                                           , Globales.oAmbientes.Id_Empresa _
                                                           , Globales.oAmbientes.Id_Sucursal _
                                                           , "" _
                                                           , "" _
                                                           , "" _
                                                           , "" _
                                                           , dtDatos _
                                                           , Nothing _
                                                           , Nothing _
                                                           , Mensaje) Then
                                        For Each oRen As DataRow In dtDatos.Rows
                                            txt_Pediddo.Text = oRen.Item("pedido").ToString.Trim()
                                            txt_Cliente.Text = oRen.Item("cuenta").ToString.Trim()
                                            txt_TipoCliente.Text = oRen.Item("referencia").ToString.Trim()
                                            ''USAMOS EL DESCUENTO DEL CLIENTE EN CURSO
                                            ''txt_ClienteDescto.Text = oRen.Item("descuento_global")
                                            ''txt_DesctoGlobal.Text = oRen.Item("descuento_global")


                                            Dim DesctoGlobal As Decimal
                                            If txt_ClienteDescto.Text <> "" Then
                                                DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                                            End If

                                            If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                                            , Globales.oAmbientes.Id_Sucursal _
                                                            , txt_Autonumsuc.Text _
                                                            , False _
                                                            , DesctoGlobal _
                                                            , 0 _
                                                            , 0 _
                                                            , 0 _
                                                            , Mensaje) Then

                                            Else
                                                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                            End If
                                        Next
                                    End If
                                Else
                                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                End If
                            End If
                        End If

                        Call Refrescar()
                        txt_Codigo.Focus()
                    Else
                        MessageBox.Show("La venta de mostrador o cotización (" & ofrmGral.Autonumusc & ") ya se encuentra usado o cancelado", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Call Refrescar()
                    End If
                End If
            Else
                Me.Close()
            End If
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Inserta_DetVentas()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim Renglon As Integer = 0
        Dim Articulo As String = ""
        Dim Articulo1 As String = ""
        Dim Nombre As String = ""
        Dim Cantidad As Integer = 0
        Dim Preciofinal As Double = 0.0

        Dim Promociones() As String
        Dim IdPromocion As Integer = 0
        Dim TipoPromocion As String = ""
        Dim DescPromocion As String = ""
        Dim Valor1Promocion As Double = 0.0

        Dim TCambio As Double = 0.0
        Dim Importe As Double = 0.0
        Dim oFrm As pdvAplicaPromociones

        Dim PuntosActual As Double
        Dim PuntosUtiliz As Double
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Inserta_DetVentas_Articulo(Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , txt_Autonumsuc.Text _
                                               , txt_Codigo.Text _
                                               , "*" _
                                               , txt_Vendedor1.Text _
                                               , "" _
                                               , 0 _
                                               , "" _
                                               , Renglon _
                                               , Articulo _
                                               , Nombre _
                                               , Cantidad _
                                               , Preciofinal _
                                               , Mensaje) Then
                Dim DesctoGlobal As Decimal
                If txt_ClienteDescto.Text <> "" Then
                    DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                End If

                If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , txt_Autonumsuc.Text _
                                        , False _
                                        , DesctoGlobal _
                                        , 0 _
                                        , 0 _
                                        , 0 _
                                        , Mensaje) Then

                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

                Call Refrescar()

                If txt_TipoCliente.Text = "CLIENTE" _
                    Or txt_TipoCliente.Text = "MAYORISTA" Then

                    Articulo1 = ("0000000000" & Articulo)
                    Articulo1 = Articulo1.Substring(Articulo1.Length - 10)
                    If oDatos.PVTA_Determina_Promocion_Puntos(Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , "" _
                                               , "" _
                                               , "" _
                                               , "A" & Articulo1 _
                                               , dtDatos1 _
                                               , Mensaje) Then

                        ''''MessageBox.Show(Articulo1)
                        If dtDatos1.Rows.Count > 0 Then
                            Promociones = dtDatos1.Rows(0).Item("promocion").ToString.Substring(1).Split("@")
                            IdPromocion = Integer.Parse(Promociones(0))
                            TipoPromocion = Promociones(2)
                            If Promociones(2) <> "DINEROE" Then

                                DescPromocion = Promociones(3)
                                Valor1Promocion = Double.Parse(Promociones(4), System.Globalization.NumberStyles.Any)
                                PuntosActual = Double.Parse(txt_PuntosActual.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                                PuntosUtiliz = Double.Parse(txt_PuntosUtilizados.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                                If IdPromocion > 0 And PuntosActual - PuntosUtiliz >= Valor1Promocion Then
                                    oFrm = New pdvAplicaPromociones
                                    oFrm.txt_Articulo.Text = Articulo
                                    oFrm.txt_Nombre.Text = Nombre
                                    oFrm.txt_Valor.Text = Valor1Promocion.ToString
                                    oFrm.nud_Comprados.Maximum = Cantidad
                                    oFrm.nud_Comprados.Value = Cantidad
                                    oFrm.nud_Aplicados.Maximum = Cantidad
                                    If Cantidad * Valor1Promocion < PuntosActual - PuntosUtiliz Then
                                        oFrm.nud_Aplicados.Value = Cantidad
                                    Else
                                        oFrm.nud_Aplicados.Value = Math.Floor((PuntosActual - PuntosUtiliz) / Valor1Promocion)
                                    End If
                                    oFrm.txt_Disponibles.Text = (PuntosActual - PuntosUtiliz).ToString
                                    oFrm.txt_Nombre.Text = Nombre
                                    oFrm.ShowDialog()
                                    If oFrm.Accion Then
                                        Importe = Preciofinal * oFrm.nud_Aplicados.Value
                                        TCambio = oFrm.nud_Aplicados.Value * Valor1Promocion
                                        If oDatos.PVTA_Inserta_Desglose(Globales.oAmbientes.Id_Empresa _
                                                       , Globales.oAmbientes.Id_Sucursal _
                                                       , txt_Autonumsuc.Text _
                                                       , txt_Caja.Text _
                                                       , 0 _
                                                       , txt_Cajero.Text _
                                                       , "PUNTOS" _
                                                       , "PUNTOS" _
                                                       , Articulo _
                                                       , TCambio _
                                                       , Importe _
                                                       , "Operado" _
                                                       , "" _
                                                       , "" _
                                                       , txt_Caja.Text _
                                                       , Renglon _
                                                       , "" _
                                                       , "" _
                                                       , "" _
                                                       , Mensaje) Then
                                            Call Refrescar()
                                        Else
                                            MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Else
                    End If
                End If
                txt_Codigo.Text = ""
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txt_Codigo_KeyDown(sender As Object, e As KeyEventArgs) Handles txt_Codigo.KeyDown
        Try
            If e.KeyCode = 13 Then
                Call Inserta_DetVentas()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Mostrar_Transaccion()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Dim Mensaje As String = ""
        Try
            oDatos = New Datos_Viscoi

            txt_Empresa.Text = ""
            txt_RFC.Text = ""
            txt_Direccion.Text = ""

            If oDatos.PVTA_Recupera_Transacciones(Today.Date _
                                               , Today.Date _
                                               , 0 _
                                               , txt_Autonumsuc.Text _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , txt_Caja.Text _
                                               , txt_Caja.Text _
                                               , txt_Cajero.Text _
                                               , txt_Vendedor1.Text _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , dtDatos2 _
                                               , Mensaje) Then
                If dtDatos1.Rows.Count > 0 Then
                    txt_Empresa.Text = dtDatos1.Rows(0).Item("empresa")
                    txt_RFC.Text = dtDatos1.Rows(0).Item("rfc")
                    txt_Direccion.Text = dtDatos1.Rows(0).Item("direccion")
                    txt_Direccion2.Text = dtDatos2.Rows(0).Item("direccion")
                End If
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Mostrar_DetVentas()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim Mensaje As String = ""

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DinEleActual As Double = 0.0
        Dim DinEleAcumulados As Double = 0.0
        Dim DinEleUtilizados As Double = 0.0
        Dim DinEleFinal As Double = 0.0

        Dim TotalPagado As Double = 0.0
        Try
            oDatos = New Datos_Viscoi

            col1_Movimiento.Visible = True
            TamanoNombre = IIf(col1_Nombre.Width < 300, 300, col1_Nombre.Width)
            If txt_Autonumsuc.Text.Length > 0 Then
                If oDatos.PVTA_Recupera_DetVentas(Today.Date _
                                              , txt_Autonumsuc.Text _
                                              , Globales.oAmbientes.Id_Empresa _
                                              , Globales.oAmbientes.Id_Sucursal _
                                              , dtDatos _
                                              , dtDatos1 _
                                              , Mensaje) Then
                    GridC_1.DataSource = dtDatos
                    GridC_1.RefreshDataSource()
                    GridV_1.BestFitColumns()
                    col1_Nombre.Width = TamanoNombre

                    If dtDatos1.Rows.Count > 0 Then
                        txt_Articulos.Text = Format(dtDatos1.Rows(0).Item("articulos"), "N0")
                        txt_Cantidad.Text = Format(dtDatos1.Rows(0).Item("cantidad"), "N2")
                        txt_SubTotal.Text = Format(dtDatos1.Rows(0).Item("subtotal"), "C2")
                        txt_IVA.Text = Format(dtDatos1.Rows(0).Item("iva"), "C2")
                        txt_Total.Text = Format(dtDatos1.Rows(0).Item("total"), "C2")
                        txt1_Descto.Text = Format(dtDatos1.Rows(0).Item("descuento"), "C2")

                        txt_Contado.Text = Format(dtDatos1.Rows(0).Item("contado"), "C2")
                        txt_DE.Text = Format(dtDatos1.Rows(0).Item("dinelec"), "C2")
                        txt_Cambio.Text = Format(dtDatos1.Rows(0).Item("cambio"), "C2")

                        TotalPagado = dtDatos1.Rows(0).Item("contado") + dtDatos1.Rows(0).Item("tdc") + dtDatos1.Rows(0).Item("tdb") + dtDatos1.Rows(0).Item("dinelec") + dtDatos1.Rows(0).Item("puntos") + dtDatos1.Rows(0).Item("credito")
                        If dtDatos1.Rows(0).Item("total") - TotalPagado > 0 Then
                            txt_APagar.Text = Format(dtDatos1.Rows(0).Item("total") - TotalPagado, "C2")
                        Else
                            txt_APagar.Text = Format(0, "C2")
                        End If
                        ''****************************************************************************************
                        ''**                                    PUNTOS                                          **
                        ''****************************************************************************************
                        If txt_CodigoCanje.Text <> "" Then
                            txt_PuntosAcumulados.Text = Format(dtDatos1.Rows(0).Item("puntos_acumular"), "N0")
                            txt_PuntosUtilizados.Text = Format(dtDatos1.Rows(0).Item("puntos_utilizados"), "N0")
                        Else
                            txt_PuntosAcumulados.Text = "0"
                            txt_PuntosUtilizados.Text = "0"
                        End If
                        txt_PuntosActual.Text = txt_PuntosActual.Text.Replace("$", "")
                        txt_PuntosActual.Text = txt_PuntosActual.Text.Replace(",", "")
                        txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace("$", "")
                        txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace(",", "")
                        txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace("$", "")
                        txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace(",", "")
                        Double.TryParse(txt_PuntosActual.Text, PuntosActual)
                        Double.TryParse(txt_PuntosAcumulados.Text, PuntosAcumulados)
                        Double.TryParse(txt_PuntosUtilizados.Text, PuntosUtilizados)

                        PuntosFinal = PuntosActual + PuntosAcumulados - PuntosUtilizados

                        txt_PuntosFinal.Text = PuntosFinal.ToString

                        ''****************************************************************************************
                        ''**                                    DinEle                                          **
                        ''****************************************************************************************
                        txt_DinEleAcumulados.Text = Format(dtDatos1.Rows(0).Item("de_acumular"), "N0")
                        txt_DinEleUtilizados.Text = Format(dtDatos1.Rows(0).Item("de_utilizados"), "N0")

                        txt_DinEleActual.Text = txt_DinEleActual.Text.Replace("$", "")
                        txt_DinEleActual.Text = txt_DinEleActual.Text.Replace(",", "")
                        txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace("$", "")
                        txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace(",", "")
                        txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace("$", "")
                        txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace(",", "")
                        Double.TryParse(txt_DinEleActual.Text, DinEleActual)
                        Double.TryParse(txt_DinEleAcumulados.Text, DinEleAcumulados)
                        Double.TryParse(txt_DinEleUtilizados.Text, DinEleUtilizados)

                        DinEleFinal = DinEleActual + DinEleAcumulados - DinEleUtilizados

                        txt_DinEleFinal.Text = DinEleFinal.ToString

                        txt_DesctoGlobal.Text = dtDatos1.Rows(0).Item("descuento_global")
                    Else
                        txt_Articulos.Text = 0
                        txt_Cantidad.Text = 0
                        txt_SubTotal.Text = 0
                        txt_IVA.Text = 0
                        txt_Total.Text = 0
                        txt_Total.Text = 0

                        txt_PuntosAcumulados.Text = "0"
                        txt_PuntosUtilizados.Text = "0"

                        Double.TryParse(txt_PuntosActual.Text, PuntosActual)
                        Double.TryParse(txt_PuntosAcumulados.Text, PuntosAcumulados)
                        Double.TryParse(txt_PuntosUtilizados.Text, PuntosUtilizados)

                        PuntosFinal = PuntosActual + PuntosAcumulados - PuntosUtilizados

                        txt_PuntosFinal.Text = PuntosFinal.ToString

                        txt_DesctoGlobal.Text = "0"
                    End If
                Else
                    GridC_1.DataSource = Nothing
                    GridC_1.RefreshDataSource()

                    txt_Articulos.Text = 0
                    txt_Cantidad.Text = 0
                    txt_SubTotal.Text = 0
                    txt_IVA.Text = 0
                    txt_Total.Text = 0
                    txt_Total.Text = 0

                    txt_PuntosAcumulados.Text = "0"
                    txt_PuntosUtilizados.Text = "0"

                    Double.TryParse(txt_PuntosActual.Text, PuntosActual)
                    Double.TryParse(txt_PuntosAcumulados.Text, PuntosAcumulados)
                    Double.TryParse(txt_PuntosUtilizados.Text, PuntosUtilizados)

                    PuntosFinal = PuntosActual + PuntosAcumulados - PuntosUtilizados

                    txt_PuntosFinal.Text = PuntosFinal.ToString

                    txt_DesctoGlobal.Text = "0"
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                GridC_1.DataSource = Nothing
                GridC_1.RefreshDataSource()
                GridV_1.BestFitColumns()
            End If
            GridC_1.Refresh()
            GridV_1.FocusedColumn = col1_Importe
            GridV_1.FocusedRowHandle = GridV_1.RowCount - 1
            GridV_1.LeftCoord = 9999
            col1_Sep.Width = 25
            GridC_1.Refresh()
            txt_Codigo.Focus()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Pagar(ByVal formapago As Integer)
        Dim Total As Double = 0
        Dim Contado As Double = 0
        Dim DE As Double = 0
        Dim DE_Utilizado As Double = 0
        Dim TotalPago As Double

        'Dim APagar As Double = 0
        Try

            Double.TryParse(txt_Total.Text.Replace("$", "").Replace(",", ""), Total)
            Double.TryParse(txt_Contado.Text.Replace("$", "").Replace(",", ""), Contado)
            Double.TryParse(txt_DinEleActual.Text, DE)
            Double.TryParse(txt_DinEleUtilizados.Text, DE_Utilizado)
            Double.TryParse(txt_APagar.Text.Replace("$", ""), TotalPago)
            'TotalPago = Total - APagar

            If TotalPago > 0 Then
                Dim ofrm As New pdvFormaPago
                ofrm.cbo_FormaPago.SelectedIndex = formapago
                ''ofrm.cbo_Moneda.SelectedIndex = 0
                ofrm.txt_TCambio.Text = "1.0"
                ofrm.txt_Total.Text = TotalPago
                ofrm.txt_PagoRecibido.Text = TotalPago
                ofrm.txt_PagoRecibido.Select()
                ofrm.Autonumsuc = txt_Autonumsuc.Text
                ofrm.Caja = txt_Caja.Text
                ofrm.Cajero = txt_Cajero.Text
                ofrm.Corte = 0
                ofrm.txt_ImporteDEMAX.Text = DE - DE_Utilizado
                ofrm.txt_Cliente.Text = txt_Cliente.Text.Trim()
                ofrm.txt_ClienteEstatus.Text = txt_ClienteEstatus.Text.Trim()
                ofrm.txt_ClienteTipo.Text = txt_ClienteTipo.Text.Trim()
                ofrm.Txt_DE_Observacion.Text = txt_NombreCanje.Text.Trim()
                ofrm.txt_PinPad.Text = txt_PinPad.Text
                ofrm.Impresora = lstImpresoras.Text
                ofrm.Condicion = txt_ClienteCondicion.Text.Trim()
                ofrm.txt_IdClienteFinal.Text = txt_IdClienteFinal.Text.Trim()
                ofrm.txt_NombreClienteFinal.Text = txt_NombreClienteFinal.Text.Trim()
                ofrm.txt_Folio.Text = txt_Folio.Text.Trim()
                ofrm.ShowDialog()
                Call Refrescar()

                Double.TryParse(txt_APagar.Text.Replace("$", ""), TotalPago)
                If TotalPago = 0 Then
                    Call Terminar()
                End If
            Else
                MessageBox.Show("El importe ya esta cubierto.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Terminar()
        Dim sw_continuar As Boolean = False
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim Autonumsuc1 As String = ""
        Dim Autonumsuc2 As String = ""
        Dim Autonumsuc3 As String = ""
        Dim Autonumsuc4 As String = ""
        Dim Autonumsuc5 As String = ""
        Dim Autonumsuc6 As String = ""
        Dim Autonumsuc7 As String = ""
        Dim despedida As String = ""

        Dim Puntos_acumular As Double
        Dim DinEle_acumular As Double
        Dim IdLealtad As Integer = 0

        Dim tcambio As Double = 0

        Dim oLealtad As dllVOficinaLealtad.Lealtad

        Try
            oDatos = New Datos_Viscoi

            If txt_CodigoCanje.Text <> "" Then
                Dim oFrmLealtad As New pdvValidaLealtad
                oFrmLealtad.IdUserLealtad = txt_IdLealtad.Text
                oFrmLealtad.ShowDialog()
                sw_continuar = oFrmLealtad.Accion
                If sw_continuar Then
                    txt_CodigoCanje.Text = oFrmLealtad.txt_CodigoCanje.Text
                End If
            Else
                sw_continuar = True
            End If
            If sw_continuar Then
                If oDatos.PVTA_Transacciones_Estatus(Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , txt_Autonumsuc.Text _
                                               , "Operado" _
                                               , Autonumsuc1 _
                                               , Autonumsuc2 _
                                               , Autonumsuc3 _
                                               , Autonumsuc4 _
                                               , Autonumsuc5 _
                                               , Autonumsuc6 _
                                               , Autonumsuc7 _
                                               , despedida _
                                               , Mensaje) Then
                    txt_Autonumsuc.Text = Autonumsuc1


                    oLealtad = New dllVOficinaLealtad.Lealtad
                    If txt_CodigoCanje.Text <> "" Then
                        If txt_TipoCliente.Text = "CLIENTE" _
                        Or txt_TipoCliente.Text = "MAYORISTA" Then
                            txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace("$", "")
                            txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace(",", "")
                            Double.TryParse(txt_PuntosAcumulados.Text, Puntos_acumular)
                            If Puntos_acumular > 0 Then
                                If Not oLealtad.Movimientos_ABC(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_CodigoCanje.Text, txt_Autonumsuc.Text, "PUNTOS", "ABONO", Puntos_acumular, "OPERADO", Globales.oAmbientes.oUsuario.Id_usuario, IdLealtad, Mensaje) Then
                                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If
                            End If
                        Else
                            txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace("$", "")
                            txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace(",", "")
                            Double.TryParse(txt_DinEleAcumulados.Text, DinEle_acumular)
                            If DinEle_acumular > 0 Then
                                If Not oLealtad.Movimientos_ABC(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_CodigoCanje.Text, txt_Autonumsuc.Text, "DINELECT", "ABONO", DinEle_acumular, "OPERADO", Globales.oAmbientes.oUsuario.Id_usuario, IdLealtad, Mensaje) Then
                                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If
                            End If
                        End If
                    End If
                    If txt_CodigoCanje.Text <> "" Then
                        If oDatos.PVTA_Recupera_Desglose1(txt_Autonumsuc.Text _
                                                    , Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , CDate("01/01/1900") _
                                                   , "", "" _
                                                   , "PUNTOS" _
                                                   , dtDatos1 _
                                                   , Mensaje) Then
                            tcambio = 0
                            For Each oRenglon As DataRow In dtDatos1.Rows
                                tcambio += oRenglon.Item("importe")

                            Next
                            If tcambio > 0 Then
                                If Not oLealtad.Movimientos_ABC(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_CodigoCanje.Text, txt_Autonumsuc.Text, "PUNTOS", "CARGO", tcambio, "OPERADO", Globales.oAmbientes.oUsuario.Id_usuario, IdLealtad, Mensaje) Then
                                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If
                            End If
                        End If
                        If oDatos.PVTA_Recupera_Desglose1(txt_Autonumsuc.Text _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , CDate("01/01/1900") _
                                                , "", "" _
                                                , "D. E." _
                                                , dtDatos1 _
                                                , Mensaje) Then
                            tcambio = 0
                            For Each oRenglon As DataRow In dtDatos1.Rows
                                tcambio += oRenglon.Item("importe")

                            Next
                            If tcambio > 0 Then
                                If Not oLealtad.Movimientos_ABC(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_CodigoCanje.Text, txt_Autonumsuc.Text, "DINELECT", "CARGO", tcambio, "OPERADO", Globales.oAmbientes.oUsuario.Id_usuario, IdLealtad, Mensaje) Then
                                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If
                            End If
                        End If
                    End If

                    Dim oTermina As New pdvTerminaVenta
                    oTermina.lbl_Cambio.Text = txt_Cambio.Text
                    oTermina.lbl_Despedida.Text = despedida
                    oTermina.ShowDialog()
                    If oTermina.Accion Then
                        Call ImprimirTicket(Autonumsuc1, txt_FormatoTicket.Text, despedida)
                        If Autonumsuc2.Length > 0 Then
                            Call ImprimirTicket(Autonumsuc2, txt_FormatoTicket.Text, despedida)
                        End If
                        If Autonumsuc3.Length > 0 Then
                            Call ImprimirTicket(Autonumsuc3, txt_FormatoTicket.Text, despedida)
                        End If
                        If Autonumsuc4.Length > 0 Then
                            Call ImprimirTicket(Autonumsuc4, txt_FormatoTicket.Text, despedida)
                        End If
                        If Autonumsuc5.Length > 0 Then
                            Call ImprimirTicket(Autonumsuc5, txt_FormatoTicket.Text, despedida)
                        End If
                        If Autonumsuc6.Length > 0 Then
                            Call ImprimirTicket(Autonumsuc6, txt_FormatoTicket.Text, despedida)
                        End If
                        If Autonumsuc7.Length > 0 Then
                            Call ImprimirTicket(Autonumsuc7, txt_FormatoTicket.Text, despedida)
                        End If
                    End If
                    If txt_TipoCliente.Text.Trim() <> "EMPLEADO" And txt_Cliente.Text.Trim <> "" Then
                        Dim frm1 As pdvFactura
                        Dim frm2 As pdvFactura40

                        Dim dtDatosF As DataTable = Nothing
                        Dim Msj As String = ""
                        Try
                            If oDatos.PVTA_Recupera_SatInfo(Globales.oAmbientes.Id_Empresa _
                                                               , dtDatosF _
                                                               , Msj) Then
                                If dtDatosF.Rows.Count > 0 Then
                                    If dtDatosF.Rows(0).Item("EmisorVersion") = "3.3" Then
                                        frm1 = New pdvFactura
                                        frm1.txt_Autonumsuc.Text = txt_Autonumsuc.Text
                                        frm1.ShowDialog()
                                    Else
                                        frm2 = New pdvFactura40
                                        frm2.SW_CerrarAuto = True
                                        frm2.txt_Autonumsuc.Text = txt_Autonumsuc.Text
                                        frm2.ShowDialog()
                                    End If
                                Else
                                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & "No se puede determinar version de CFDI", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                End If
                            Else
                                MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        Catch ex As Exception
                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Try
                    End If

                    If oDatos.Actualiza_Nube_Bitacora_Autorizacione(txt_Autonumsuc.Text, Mensaje) Then
                    End If

                    txt_Autonumsuc.Text = ""
                    txt_DE.Text = "0"
                    txt_Contado.Text = "0"
                    lbl_NombreCanje.Text = "Hola!!!"

                    If Not oDatos.PVTA_Caja_Valida_Efectivo(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, txt_Caja.Text, dtDatos, Mensaje) Then
                        MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                    Call Refrescar()
                    Me.Refresh()
                    Crear_Transaccion()
                    txt_Codigo.Focus()
                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub TerminarNoVenta()
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim Mensaje As String = ""
        Dim ASVtaCot As String = ""
        Try
            ASVtaCot = txt_Autonumsuc.Text
            If oDatos.PVTA_Transacciones_Estatus(Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , txt_Autonumsuc.Text _
                                               , TipoTran.ToString() _
                                               , txt_Autonumsuc.Text _
                                               , "" _
                                                 , "" _
                                                , "" _
                                                , "" _
                                                , "" _
                                                , "" _
                                                , "" _
                                               , Mensaje) Then
                txt_Autonumsuc.Text = ASVtaCot
                Call Imprimir()

                txt_Autonumsuc.Text = ""
                txt_DE.Text = "0"
                txt_Contado.Text = "0"
                lbl_NombreCanje.Text = "Hola!!!"

                Call Refrescar()
                Me.Refresh()
                Crear_Transaccion()
                txt_Codigo.Focus()
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub ImprimirTicket(ByVal Autonumsuc As String, ByVal Formato As String, ByVal despedida As String)
        Dim Reporte As Object
        Dim printBase As DevExpress.XtraPrinting.PrintToolBase
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim oTicket As entTicketVenta

        Dim PuntosActual As Double = 0.0
        Dim PuntosAcumulados As Double = 0.0
        Dim PuntosUtilizados As Double = 0.0
        Dim PuntosFinal As Double = 0.0

        Dim DEActual As Double = 0.0
        Dim DEAcumulados As Double = 0.0
        Dim DEUtilizados As Double = 0.0
        Dim DEFinal As Double = 0.0
        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Recupera_DetVentas_Impresion(Today.Date _
                                              , Autonumsuc _
                                               , Globales.oAmbientes.Id_Empresa _
                                               , Globales.oAmbientes.Id_Sucursal _
                                               , dtDatos _
                                               , dtDatos1 _
                                               , Mensaje) Then
                If txt_CodigoCanje.Text <> "" Then
                    txt_PuntosActual.Text = txt_PuntosActual.Text.Replace("$", "")
                    txt_PuntosActual.Text = txt_PuntosActual.Text.Replace(",", "")
                    txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace("$", "")
                    txt_PuntosAcumulados.Text = txt_PuntosAcumulados.Text.Replace(",", "")
                    txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace("$", "")
                    txt_PuntosUtilizados.Text = txt_PuntosUtilizados.Text.Replace(",", "")
                Else
                    txt_PuntosActual.Text = "0"
                    txt_PuntosActual.Text = "0"
                    txt_PuntosAcumulados.Text = "0"
                    txt_PuntosAcumulados.Text = "0"
                    txt_PuntosUtilizados.Text = "0"
                    txt_PuntosUtilizados.Text = "0"
                End If

                Double.TryParse(txt_PuntosActual.Text, PuntosActual)
                Double.TryParse(txt_PuntosAcumulados.Text, PuntosAcumulados)
                Double.TryParse(txt_PuntosUtilizados.Text, PuntosUtilizados)

                PuntosFinal = PuntosActual + PuntosAcumulados - PuntosUtilizados

                txt_DinEleActual.Text = txt_DinEleActual.Text.Replace("$", "")
                txt_DinEleActual.Text = txt_DinEleActual.Text.Replace(",", "")
                txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace("$", "")
                txt_DinEleAcumulados.Text = txt_DinEleAcumulados.Text.Replace(",", "")
                txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace("$", "")
                txt_DinEleUtilizados.Text = txt_DinEleUtilizados.Text.Replace(",", "")
                Double.TryParse(txt_DinEleActual.Text, DEActual)
                Double.TryParse(txt_DinEleAcumulados.Text, DEAcumulados)
                Double.TryParse(txt_DinEleUtilizados.Text, DEUtilizados)

                DEFinal = DEActual + DEAcumulados - DEUtilizados

                oTicket = New entTicketVenta
                oTicket.Fill(dtDatos, dtDatos1, Nothing)

                oTicket.Empresa = txt_Empresa.Text
                oTicket.Rfc = txt_RFC.Text
                oTicket.Direccion = txt_Direccion.Text
                oTicket.Direccion2 = txt_Direccion2.Text
                oTicket.Cajero = txt_Cajero.Text
                oTicket.Caja = txt_Caja.Text


                If txt_NombreCanje.Text.Length > 0 Then
                    oTicket.Lealtad_nombre = txt_NombreCanje.Text
                    oTicket.Puntos_saldo_ant = PuntosActual
                    oTicket.Puntos_acumulados = PuntosAcumulados
                    oTicket.Puntos_utilizados = PuntosUtilizados
                    oTicket.Puntos_saldo_act = PuntosFinal

                    oTicket.De_saldo_ant = DEActual
                    oTicket.De_acumulados = DEAcumulados
                    oTicket.De_utilizados = DEUtilizados
                    oTicket.De_saldo_act = DEFinal
                Else
                    oTicket.Lealtad_nombre = 0
                    oTicket.Puntos_saldo_ant = 0
                    oTicket.Puntos_acumulados = 0
                    oTicket.Puntos_utilizados = 0
                    oTicket.Puntos_saldo_act = 0

                    oTicket.De_saldo_ant = 0
                    oTicket.De_acumulados = 0
                    oTicket.De_utilizados = 0
                    oTicket.De_saldo_act = 0
                End If

                Dim MargenAba As Integer = 0
                Dim MargenIzq As Integer = 0
                Dim MargenDer As Integer = 0
                Dim MargenArr As Integer = 0
                Try
                    MargenAba = txt_MargenesTicket.Text.Split(",")(0)
                Catch ex As Exception
                    MargenAba = 0
                End Try
                Try
                    MargenIzq = txt_MargenesTicket.Text.Split(",")(1)
                Catch ex As Exception
                    MargenIzq = 0
                End Try
                Try
                    MargenDer = txt_MargenesTicket.Text.Split(",")(2)
                Catch ex As Exception
                    MargenDer = 0
                End Try
                Try
                    MargenArr = txt_MargenesTicket.Text.Split(",")(3)
                Catch ex As Exception
                    MargenArr = 0
                End Try
                Select Case Formato
                    Case "SEAFON"
                        Reporte = New xtraRepTicketVenta_Seafon
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).xlbl_despedida.Text = despedida
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenAbj = MargenAba
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenIzq = MargenIzq
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenDer = MargenDer
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).MargenArr = MargenArr
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepTicketVenta_Seafon).CreateDocument()
                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta_Seafon).PrintingSystem)
                    Case "EPSON"
                        Reporte = New xtraRepTicketVenta
                        TryCast(Reporte, xtraRepTicketVenta).xlbl_Despedida.Text = despedida
                        TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                        TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                        TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                        TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                        TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                        TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                    Case Else
                        Reporte = New xtraRepTicketVenta
                        TryCast(Reporte, xtraRepTicketVenta).MargenAbj = MargenAba
                        TryCast(Reporte, xtraRepTicketVenta).MargenIzq = MargenIzq
                        TryCast(Reporte, xtraRepTicketVenta).MargenDer = MargenDer
                        TryCast(Reporte, xtraRepTicketVenta).MargenArr = MargenArr
                        TryCast(Reporte, xtraRepTicketVenta).TipoCliente = txt_TipoCliente.Text & IIf(txt_Empresa.Text = "PAPELERA", "1", "")
                        TryCast(Reporte, xtraRepTicketVenta).ods.DataSource = oTicket
                        TryCast(Reporte, xtraRepTicketVenta).CreateDocument()
                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketVenta).PrintingSystem)
                End Select
                ''printTool = New DevExpress.XtraReports.UI.ReportPrintTool(Reporte)
                ''printTool.ShowPreviewDialog()
                ''lstImpresoras.SelectedIndex = 5
                For iI = 0 To NumCopias - 1
                    printBase.Print(lstImpresoras.Text)
                Next
                If oDatos.PVTA_Recupera_Desglose1(Autonumsuc _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , CDate("01/01/1900") _
                                                , "" _
                                                , "" _
                                                , "CREDITO" _
                                                , dtDatos _
                                                , Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                                , Autonumsuc _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , dtDatos _
                                                , Mensaje) Then
                            If dtDatos.Rows.Count > 0 Then
                                Dim oPagare As New entPagare
                                oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                Select Case Formato
                                    Case "SEAFON"
                                        Reporte = New xtraRepTicketPagare_Seafon
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare_Seafon).PrintingSystem)
                                    Case "EPSON"
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                    Case Else
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                End Select
                                For iI = 0 To NumCopias - 1
                                    printBase.Print(lstImpresoras.Text)
                                Next
                            End If
                        End If
                    End If
                End If

                If oDatos.PVTA_Recupera_Desglose1(Autonumsuc _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , CDate("01/01/1900") _
                                                , "" _
                                                , "" _
                                                , "VALES" _
                                                , dtDatos _
                                                , Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                                , Autonumsuc _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , dtDatos _
                                                , Mensaje) Then
                            If dtDatos.Rows.Count > 0 Then
                                Dim oPagare As New entPagare
                                oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                Select Case Formato
                                    Case "SEAFON"
                                        Reporte = New xtraRepTicketPagare_Seafon
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare_Seafon).PrintingSystem)
                                    Case "EPSON"
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                    Case Else
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                End Select
                                For iI = 0 To NumCopias - 1
                                    printBase.Print(lstImpresoras.Text)
                                Next
                            End If
                        End If
                    End If
                End If
                If oDatos.PVTA_Recupera_Desglose1(Autonumsuc _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , CDate("01/01/1900") _
                                                , "" _
                                                , "" _
                                                , "EMPLEADO" _
                                                , dtDatos _
                                                , Mensaje) Then
                    If dtDatos.Rows.Count > 0 Then
                        If oDatos.PVTA_Recupera_Pagare(CDate("01/01/1900") _
                                                , Autonumsuc _
                                                , Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , dtDatos _
                                                , Mensaje) Then
                            If dtDatos.Rows.Count > 0 Then
                                Dim oPagare As New entPagare
                                oPagare.Empresa = dtDatos.Rows(0).Item("Empresa")
                                oPagare.EmpDireccion = dtDatos.Rows(0).Item("EmpDireccion")
                                oPagare.Sucursal = dtDatos.Rows(0).Item("Sucursal")
                                oPagare.SucDireccion = dtDatos.Rows(0).Item("SucDireccion")
                                oPagare.Cliente = dtDatos.Rows(0).Item("Cliente")
                                oPagare.CliDireccion = dtDatos.Rows(0).Item("CliDireccion")
                                oPagare.Autonumsuc = dtDatos.Rows(0).Item("Autonumsuc")
                                oPagare.Cajero = dtDatos.Rows(0).Item("Cajero")
                                oPagare.Fecha = dtDatos.Rows(0).Item("Fecha")
                                oPagare.Mensaje = dtDatos.Rows(0).Item("Mensaje")
                                Select Case Formato
                                    Case "SEAFON"
                                        Reporte = New xtraRepTicketPagare_Seafon
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare_Seafon).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare_Seafon).PrintingSystem)
                                    Case "EPSON"
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                    Case Else
                                        Reporte = New xtraRepTicketPagare
                                        TryCast(Reporte, xtraRepTicketPagare).MargenAbj = MargenAba
                                        TryCast(Reporte, xtraRepTicketPagare).MargenIzq = MargenIzq
                                        TryCast(Reporte, xtraRepTicketPagare).MargenDer = MargenDer
                                        TryCast(Reporte, xtraRepTicketPagare).MargenArr = MargenArr
                                        TryCast(Reporte, xtraRepTicketPagare).ods.DataSource = oPagare
                                        TryCast(Reporte, xtraRepTicketPagare).CreateDocument()
                                        printBase = New DevExpress.XtraPrinting.PrintToolBase(TryCast(Reporte, xtraRepTicketPagare).PrintingSystem)
                                End Select
                                For iI = 0 To NumCopias - 1
                                    printBase.Print(lstImpresoras.Text)
                                Next
                            End If
                        End If
                    End If
                End If
                ''''Dim objFSO
                ''''Dim objStream
                ''''
                ''''Try
                ''''    objFSO = CreateObject("Scripting.FileSystemObject")
                ''''    objStream = objFSO.CreateTextFile("LPT1") 'Puerto al cual se envía la impresión
                ''''    objStream.Writeline(Chr(27) & "p" & Chr(0) & Chr(100) & Chr(250)) 'Abrir cajón
                ''''    objStream.Close()
                ''''
                ''''Catch ex As Exception
                ''''
                ''''Finally
                ''''    ''objStream.Writeline(Chr(27) & Chr(64)) ' limpia Buffer de la impresora
                ''''    ''objStream.Writeline(Chr(27) & Chr(60)) ' la deja en Posicion Stand BY
                ''''    objFSO = Nothing
                ''''    objStream = Nothing
                ''''End Try
                ''''
                ''''Try
                ''''    FileOpen(1, "LPT1", OpenMode.Output)
                ''''    PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(100) & Chr(250))
                ''''    FileClose(1)
                ''''Catch ex As Exception
                ''''
                ''''End Try
            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            oTicket = New entTicketVenta
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub pdvPuntoVenta_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim msj As String = ""
        Dim DatosConexion() As String
        Try
            Modo = Globales.TipoABC.Indice

            DatosConexion = System.IO.File.ReadAllLines("C:\PuntoVenta.dat", System.Text.Encoding.Default)
            For Each slinea As String In DatosConexion
                If slinea.Substring(0, 1) <> "*" Then
                    Globales.oAmbientes.Id_Sucursal = slinea.Substring(14, 12)
                    txt_Caja.Text = slinea.Substring(30, 4)
                    Exit For
                End If
            Next

            txt_Sucursal.Text = Globales.oAmbientes.Id_Sucursal

            If oDatos.PVTA_Recupera_Cajeros(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, txt_Caja.Text, "", Globales.oAmbientes.oUsuario.Id_usuario, dtDatos, msj) Then
                If dtDatos.Rows.Count > 0 Then
                    txt_Cajero.Text = dtDatos.Rows(0).Item("cajero")

                    NumCopias = 1
                    If oDatos.PVTA_Recupera_Cajas(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", txt_Caja.Text, dtDatos, msj) Then
                        If dtDatos.Rows.Count > 0 Then
                            txt_FormatoTicket.Text = dtDatos.Rows(0).Item("printername1").ToString.ToUpper
                            txt_MargenesTicket.Text = dtDatos.Rows(0).Item("printerport1").ToString.ToUpper
                            txt_PinPad.Text = dtDatos.Rows(0).Item("printerdriver1")
                            NumCopias = Globales.oAmbientes.Valor(dtDatos.Rows(0).Item("printername2"))
                            NumCopias = IIf(NumCopias = 0, 1, NumCopias)

                            Dim _Puerto As String
                            Dim _User As String
                            Dim _Pswd As String
                            Dim _IdMerchant As String
                            Dim _Url As String
                            Dim _Modo As String
                            Try
                                If txt_PinPad.Text <> "" Then
                                    If txt_PinPad.Text.Split("|").Length = 7 Then
                                        Globales.oAmbientes.PinPadBanco = txt_PinPad.Text.Split("|")(0).ToString.ToUpper
                                        _Puerto = txt_PinPad.Text.Split("|")(1).ToString.ToUpper
                                        _User = txt_PinPad.Text.Split("|")(2)
                                        _Pswd = txt_PinPad.Text.Split("|")(3)
                                        _IdMerchant = txt_PinPad.Text.Split("|")(4)
                                        _Url = txt_PinPad.Text.Split("|")(5).ToString
                                        _Modo = txt_PinPad.Text.Split("|")(6).ToString.ToUpper

                                        Select Case Globales.oAmbientes.PinPadBanco
                                            Case "BANORTE"
                                                Globales.oAmbientes.oPinPadBanorte = New Globales.clsPinPadBanorte(_Puerto, _User, _Pswd, _IdMerchant, _Modo, _Url, txt_Caja.Text)
                                                Dim oSplash As New pdvPinPadSplash
                                                oSplash.PinAccion = pdvPinPadSplash.PinpadAccion.Inicializa
                                                oSplash.StartPosition = FormStartPosition.CenterScreen
                                                oSplash.ShowDialog()
                                        End Select
                                    Else
                                        MessageBox.Show("Faltan parametros de configuracion para la PINPAD.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End If
                                End If
                            Catch ex As Exception
                                Globales.oAmbientes.PinPadBanco = ""
                                _User = ""
                                _Pswd = ""
                                _IdMerchant = ""
                                _Modo = ""
                                MessageBox.Show("El pinpad no se inicializo corectamente." & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                            End Try


                            If dtDatos.Rows(0).Item("actividad").ToString.Trim.ToUpper = "ENUSO" Then
                                ''dtDatos.Rows(0).Item("corte").ToString()

                                Call ControlBotones("Ok")
                                Call Botones("Ok")

                                sw_datosalmacenados = False
                                If oDatos.PVTA_Recupera_Transacciones(Today, Today, 0, "", Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, "", txt_Caja.Text, "", "", dtDatos, Nothing, Nothing, msj) Then
                                    If dtDatos.Rows.Count > 0 Then
                                        If MessageBox.Show("Existe una transacción PENDIENTE ¿deseas recuperarla?", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                                            txt_Autonumsuc.Text = dtDatos.Rows(0).Item("autonumsuc")
                                            sw_datosalmacenados = True
                                        End If
                                    End If
                                End If

                                Call Refrescar()
                                If sw_datosalmacenados Then
                                    If oDatos.PVTA_Recupera_Transacciones(Today.Date _
                                                       , Today.Date _
                                                       , 0 _
                                                       , txt_Autonumsuc.Text _
                                                       , Globales.oAmbientes.Id_Empresa _
                                                       , Globales.oAmbientes.Id_Sucursal _
                                                       , "" _
                                                       , "" _
                                                       , "" _
                                                       , "" _
                                                       , dtDatos _
                                                       , Nothing _
                                                       , Nothing _
                                                       , msj) Then
                                        For Each oRen As DataRow In dtDatos.Rows
                                            txt_Pediddo.Text = oRen.Item("pedido").ToString.Trim()
                                            txt_Cliente.Text = oRen.Item("cuenta").ToString.Trim()
                                            txt_TipoCliente.Text = oRen.Item("referencia").ToString.Trim()
                                            txt_ClienteDescto.Text = oRen.Item("descuento_global")
                                            txt_DesctoGlobal.Text = oRen.Item("descuento_global")

                                            Dim DesctoGlobal As Double
                                            If txt_ClienteDescto.Text <> "" Then
                                                DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                                            End If

                                            If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                                                        , Globales.oAmbientes.Id_Sucursal _
                                                                        , txt_Autonumsuc.Text _
                                                                        , False _
                                                                        , DesctoGlobal _
                                                                        , 0 _
                                                                        , 0 _
                                                                        , 0 _
                                                                        , msj) Then

                                            Else
                                                MessageBox.Show(msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                            End If
                                        Next
                                    End If
                                    Call DatosGenerales()
                                End If

                                If txt_Autonumsuc.Text = "" Then
                                    Call Crear_Transaccion()
                                    txt_Codigo.Focus()
                                End If
                            Else
                                MessageBox.Show("Caja(" & txt_Caja.Text & ") esta cerrada, realice apertura de caja primero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Me.Close()
                            End If
                        Else
                            MessageBox.Show("Caja(" & txt_Caja.Text & ") no esta registrada para Sucursal(" & txt_Sucursal.Text & ").", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Me.Close()
                        End If
                    Else
                        MessageBox.Show("Caja(" & txt_Caja.Text & ") no esta registrada.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Me.Close()
                    End If
                Else
                    MessageBox.Show("Usuario no es cajero.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.Close()
                End If
            Else
                MessageBox.Show(msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub

    Private Sub ELiminar_Detventas()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim Articulo As String
        Dim Renglon As Integer
        Try
            oDatos = New Datos_Viscoi

            If GridV_1.RowCount > 0 Then
                Articulo = GridV_1.GetFocusedRowCellValue(col1_Articulo)
                Renglon = GridV_1.GetFocusedRowCellValue(col1_Renglon)

                If oDatos.PVTA_Elimina_DetVentas(Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , txt_Autonumsuc.Text _
                                                   , Articulo _
                                                   , Renglon _
                                                   , Mensaje) Then
                    Call Refrescar()
                Else
                    MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub AplicaDescuentoLista()
        Dim oForma As pdvLstPrecios

        Try
            oForma = New pdvLstPrecios
            oForma.Txt_RenglonCantidad.Text = GridV_1.GetFocusedRowCellValue(col1_Renglon) ' selectedRow

            oForma.Autonumsuc = txt_Autonumsuc.Text
            If selectedRow > 0 Then
                oForma.CkBoxTodos.Checked = False
            Else
                oForma.CkBoxTodos.Checked = True
            End If

            selectedRow = 0

            oForma.ShowDialog()

            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oForma = Nothing
    End Sub

    Private Sub AplicaDescuentos(ByVal SW_Partida As Boolean)
        Dim Descto_Global As Double
        Dim Descto As Double
        Dim oForma As pdvAplicaDescuentos
        Try
            oForma = New pdvAplicaDescuentos
            Descto_Global = Double.Parse(txt_DesctoGlobal.Text.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)
            oForma.Autonumsuc = txt_Autonumsuc.Text
            oForma.txt_DesctoGlobal.Text = IIf(txt_ClienteDescto.Text = "", "0", txt_ClienteDescto.Text)
            oForma.txt_DesctoGlobal.Text = Descto_Global * 100
            If GridV_1.RowCount > 0 And SW_Partida Then
                If GridV_1.GetFocusedRowCellValue(col1_Articulo).ToString.Length > 0 Then
                    Descto = Double.Parse(GridV_1.GetFocusedRowCellValue(col1_Descuento).ToString.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)
                    oForma.txt_Articulo.Text = GridV_1.GetFocusedRowCellValue(col1_Articulo)
                    oForma.txt_Nombre.Text = GridV_1.GetFocusedRowCellValue(col1_Nombre)
                    oForma.txt_Renglon.Text = GridV_1.GetFocusedRowCellValue(col1_Renglon)
                    oForma.txt_Precio5.Text = GridV_1.GetFocusedRowCellValue(col1_Precio5)
                    oForma.txt_PrecioActual.Text = GridV_1.GetFocusedRowCellValue(col1_PrecioAct)
                    oForma.txt_Descto.Text = Descto * 100
                    oForma.txt_PrecioFinal.Text = GridV_1.GetFocusedRowCellValue(col1_PrecioNeto)
                End If
            Else
                oForma.txt_Renglon.Text = "0"
                oForma.txt_Descto.Text = "0"
            End If
            oForma.txt_DesctoGlobal.Enabled = Not SW_Partida
            oForma.GC_Partida.Visible = SW_Partida
            oForma.PrecioDirecto = False
            oForma.ShowDialog()
            Descto_Global = Double.Parse(oForma.txt_DesctoGlobal.Text.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)
            txt_ClienteDescto.Text = Descto_Global / 100.0
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oForma = Nothing
    End Sub

    Private Sub AplicaCotizador(ByVal SW_Partida As Boolean)
        Dim Descto_Global As Double
        Dim Descto As Double
        Dim oForma As pdvCotizador

        Try
            oForma = New pdvCotizador
            Descto_Global = Double.Parse(txt_DesctoGlobal.Text.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)


            If GridV_1.RowCount > 0 And SW_Partida Then
                If GridV_1.GetFocusedRowCellValue(col1_Articulo).ToString.Length > 0 Then
                    Descto = Double.Parse(GridV_1.GetFocusedRowCellValue(col1_Descuento).ToString.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)

                End If

            End If
            'GridV_1.GetFocusedRowCellValue(col1_Articulo).ToString.Replace("$", "").Replace(",", "").Replace("%", "")
            oForma.Articulo = GridV_1.GetFocusedRowCellValue(col1_Articulo).ToString.Replace("$", "").Replace(",", "").Replace("%", "")
            oForma.Cliente = txt_Cliente.Text.Trim()
            oForma.Autonumsuc = txt_Autonumsuc.Text

            oForma.ShowDialog()
            txt_ClienteDescto.Text = Descto_Global / 100.0
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oForma = Nothing
    End Sub

    Private Sub AplicaPrecioDirecto(ByVal SW_Partida As Boolean)
        Dim Descto_Global As Double
        Dim Descto As Double
        Dim oForma As pdvAplicaDescuentos
        Try
            oForma = New pdvAplicaDescuentos
            Descto_Global = Double.Parse(txt_DesctoGlobal.Text.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)
            oForma.Autonumsuc = txt_Autonumsuc.Text
            oForma.txt_DesctoGlobal.Text = IIf(txt_ClienteDescto.Text = "", "0", txt_ClienteDescto.Text)
            oForma.txt_DesctoGlobal.Text = Descto_Global * 100
            If GridV_1.RowCount > 0 And SW_Partida Then
                If GridV_1.GetFocusedRowCellValue(col1_Articulo).ToString.Length > 0 Then
                    Descto = Double.Parse(GridV_1.GetFocusedRowCellValue(col1_Descuento).ToString.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)
                    oForma.txt_Articulo.Text = GridV_1.GetFocusedRowCellValue(col1_Articulo)
                    oForma.txt_Nombre.Text = GridV_1.GetFocusedRowCellValue(col1_Nombre)
                    oForma.txt_Renglon.Text = GridV_1.GetFocusedRowCellValue(col1_Renglon)
                    oForma.txt_Precio5.Text = GridV_1.GetFocusedRowCellValue(col1_Precio5)
                    oForma.txt_PrecioActual.Text = GridV_1.GetFocusedRowCellValue(col1_PrecioAct)
                    oForma.txt_Descto.Text = Descto * 100
                    oForma.txt_PrecioFinal.Text = GridV_1.GetFocusedRowCellValue(col1_PrecioNeto)
                End If
            Else
                oForma.txt_Renglon.Text = "0"
                oForma.txt_Descto.Text = "0"
            End If
            oForma.txt_DesctoGlobal.Enabled = Not SW_Partida
            oForma.GC_Partida.Visible = SW_Partida
            oForma.PrecioDirecto = True
            oForma.ShowDialog()
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oForma = Nothing
    End Sub
    Private Sub DatosGenerales(ByVal Optional opcion As String = "")
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""

        Dim ofrmGral As pdvDatosGenerales
        Try
            ofrmGral = New pdvDatosGenerales(TipoTran)
            ofrmGral.IdEmpresa = txt_Empresa.Text
            ofrmGral.IdSucursal = txt_Sucursal.Text
            ofrmGral.Caja = txt_Caja.Text
            ofrmGral.Cajero = txt_Cajero.Text
            ofrmGral.Impresora = lstImpresoras.Text
            ofrmGral.TecladoAtajos = TecladoAtajos
            ofrmGral.FormaLlama = Me
            ofrmGral.MensajeNotificacion = txt_MensajeNotificacion
            ''------------------------------------------
            ofrmGral.txt_Vendedor1.Text = txt_Vendedor1.Text
            ofrmGral.txt_Vendedor2.Text = txt_Vendedor2.Text
            ofrmGral.txt_Cliente.Text = txt_Cliente.Text.Trim()
            ofrmGral.txt_ClienteNombre.Text = txt_ClienteNombre.Text
            ofrmGral.txt_ClienteCondicion.Text = txt_ClienteCondicion.Text
            ofrmGral.txt_CodigoCanje.Text = txt_CodigoCanje.Text
            ofrmGral.txt_NombreCanje.Text = txt_NombreCanje.Text
            ofrmGral.txt_pedido.Text = txt_Pediddo.Text
            ofrmGral.txt_TipoCliente.Text = txt_TipoCliente.Text
            ofrmGral.txt_ClienteDescto.Text = txt_ClienteDescto.Text
            ofrmGral.txt_IdLealtad.Text = txt_IdLealtad.Text
            ofrmGral.txt_Referencia.Text = txt_Referencia.Text
            ofrmGral.txt_Caja.Text = txt_Caja.Text
            ofrmGral.txt_Cajero.Text = txt_Cajero.Text
            ofrmGral.txt_IdClienteFinal.Text = txt_IdClienteFinal.Text
            ofrmGral.txt_NombreClienteFinal.Text = txt_NombreClienteFinal.Text
            ofrmGral.txt_Folio.Text = txt_Folio.Text
            ''------------------------------------------
            ofrmGral.opcion = opcion
            ofrmGral.ShowDialog()

            txt_Vendedor1.Text = ofrmGral.txt_Vendedor1.Text
            txt_Vendedor2.Text = ofrmGral.txt_Vendedor2.Text
            txt_Cliente.Text = ofrmGral.txt_Cliente.Text.Trim()
            txt_ClienteNombre.Text = ofrmGral.txt_ClienteNombre.Text
            txt_ClienteCondicion.Text = ofrmGral.txt_ClienteCondicion.Text
            txt_CodigoCanje.Text = ofrmGral.txt_CodigoCanje.Text
            txt_NombreCanje.Text = ofrmGral.txt_NombreCanje.Text
            txt_Pediddo.Text = ofrmGral.txt_pedido.Text
            txt_TipoCliente.Text = ofrmGral.txt_TipoCliente.Text
            txt_ClienteTipo.Text = ofrmGral.txt_ClienteTipo.Text
            txt_ClienteEstatus.Text = ofrmGral.txt_ClienteEstatus.Text
            If txt_ClienteEstatus.Text = "NORM" Or txt_ClienteEstatus.Text = "ORO" Then
                txt_ClienteDescto.Text = Double.Parse(ofrmGral.txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint) / 100.0
                txt_DesctoGlobal.Text = Double.Parse(ofrmGral.txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint) / 100.0
            Else
                MessageBox.Show("El estatus del cliente no es NORM elcliente no cuenta con beneficio de descuento", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
                txt_ClienteDescto.Text = "0"
                txt_DesctoGlobal.Text = "0"
            End If

            txt_IdClienteFinal.Text = ofrmGral.txt_IdClienteFinal.Text
            txt_NombreClienteFinal.Text = ofrmGral.txt_NombreClienteFinal.Text
            txt_Folio.Text = ofrmGral.txt_Folio.Text

            txt_IdLealtad.Text = ofrmGral.txt_IdLealtad.Text
            txt_Referencia.Text = ofrmGral.txt_Referencia.Text
            txt_PlanCredito.Text = txt_IdClienteFinal.Text 'ofrmGral.cbo_PlanCredito.SelectedValue

            If txt_TipoCliente.Text <> "CLIENTE" Then
                If ofrmGral.txt_NombreCanje1.Text <> "" Then
                    lbl_NombreCanje.Text = IIf(ofrmGral.txt_NombreCanje1.Text.Trim() <> "", "Hola " & ofrmGral.txt_NombreCanje1.Text.Trim() & "!!!", "Hola!!!")
                Else
                    lbl_NombreCanje.Text = IIf(ofrmGral.txt_ClienteNombre.Text.Trim() <> "", "Hola " & ofrmGral.txt_Cliente.Text.Trim() & "-" & ofrmGral.txt_ClienteNombre.Text.Trim() & "!!!", "Hola!!!")
                End If
            Else
                If ofrmGral.txt_NombreCanje1.Text <> "" Then
                    lbl_NombreCanje.Text = IIf(ofrmGral.txt_NombreCanje1.Text.Trim() <> "", "Hola " & ofrmGral.txt_NombreCanje1.Text.Trim() & "!!!", "Hola!!!")
                Else
                    lbl_NombreCanje.Text = IIf(ofrmGral.txt_ClienteNombre.Text.Trim() <> "", "Hola " & ofrmGral.txt_Cliente.Text.Trim() & "-" & ofrmGral.txt_ClienteNombre.Text.Trim() & "!!!", "Hola!!!")
                End If
            End If

            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Inserta_Transaccion(txt_Autonumsuc.Text _
                                                   , Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , txt_Caja.Text _
                                                   , Today.Date _
                                                   , Format(Now(), "hh:mm:ss") _
                                                   , txt_Caja.Text _
                                                   , 0 _
                                                   , txt_Cajero.Text _
                                                   , TipoTran.ToString() _
                                                   , txt_Pediddo.Text _
                                                   , txt_Vendedor1.Text _
                                                   , txt_Cliente.Text.Trim() _
                                                   , txt_PlanCredito.Text.Trim() _
                                                   , 0, 0 _
                                                   , txt_ClienteNombre.Text _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , Globales.oAmbientes.oUsuario.Id_usuario _
                                                   , 0, 0, 0, 0 _
                                                   , IIf(txt_IdLealtad.Text <> "", "LEALTAD", "") _
                                                   , "CAPTURA" _
                                                   , "VENTA" _
                                                   , "", "", txt_Vendedor2.Text _
                                                   , 0, 0, 0, 0 _
                                                   , 0.0, 0.0, 0.0 _
                                                   , 0, 0.0, 0.0, 0.0 _
                                                   , IIf(TipoTran = eTipoTran.VENTA, txt_TipoCliente.Text, txt_Referencia.Text), Mensaje) Then

            End If

            Dim DesctoGlobal As Decimal
            If txt_ClienteDescto.Text <> "" Then
                DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
            End If
            If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , txt_Autonumsuc.Text _
                                        , False _
                                        , DesctoGlobal _
                                        , 0 _
                                        , 0 _
                                        , 0 _
                                        , Mensaje) Then

            Else
                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            If ofrmGral.Estatus.ToUpper = "CAPTURA" Then
                If oDatos.PVTA_Inserta_TmpTransacciones(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, ofrmGral.Autonumusc, txt_Autonumsuc.Text, Mensaje) Then
                    If oDatos.PVTA_Recupera_Transacciones(Today.Date _
                                                       , Today.Date _
                                                       , 0 _
                                                       , txt_Autonumsuc.Text _
                                                       , Globales.oAmbientes.Id_Empresa _
                                                       , Globales.oAmbientes.Id_Sucursal _
                                                       , "" _
                                                       , "" _
                                                       , "" _
                                                       , "" _
                                                       , dtDatos _
                                                       , Nothing _
                                                       , Nothing _
                                                       , Mensaje) Then
                        For Each oRen As DataRow In dtDatos.Rows
                            txt_Pediddo.Text = oRen.Item("pedido").ToString.Trim()
                            txt_Cliente.Text = oRen.Item("cuenta").ToString.Trim()
                            txt_TipoCliente.Text = oRen.Item("referencia").ToString.Trim()
                            txt_ClienteDescto.Text = oRen.Item("descuento_global")
                            txt_DesctoGlobal.Text = oRen.Item("descuento_global")

                            If txt_ClienteDescto.Text <> "" Then
                                DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                            End If

                            If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                                        , Globales.oAmbientes.Id_Sucursal _
                                                        , txt_Autonumsuc.Text _
                                                        , False _
                                                        , DesctoGlobal _
                                                        , 0 _
                                                        , 0 _
                                                        , 0 _
                                                        , Mensaje) Then

                            Else
                                MessageBox.Show(Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        Next
                    End If
                Else
                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
            Call Refrescar()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        ofrmGral = Nothing
    End Sub
    Private Sub VTAMostrador()
        Dim oFrm As pdvBuscaTransacciones
        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""

        Dim dtDatos As DataTable = Nothing
        Dim dtDatos1 As DataTable = Nothing
        Dim dtDatos2 As DataTable = Nothing
        Try
            oDatos = New Datos_Viscoi
            oFrm = New pdvBuscaTransacciones
            oFrm.TipoTran = TipoTran
            oFrm.RadioGroup1.SelectedIndex = 1
            oFrm.ShowDialog()
            If oFrm.Accion Then
                If oFrm.Autonumsuc <> "" Then
                    If oFrm.Estatus.ToUpper = "CAPTURA" Then
                        If oDatos.PVTA_Inserta_TmpTransacciones(Globales.oAmbientes.Id_Empresa, txt_Sucursal.Text, oFrm.Autonumsuc, txt_Autonumsuc.Text, Msj) Then
                            If oDatos.PVTA_Recupera_Transacciones(Today.Date _
                                                   , Today.Date _
                                                   , 0 _
                                                   , txt_Autonumsuc.Text _
                                                   , Globales.oAmbientes.Id_Empresa _
                                                   , Globales.oAmbientes.Id_Sucursal _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , "" _
                                                   , dtDatos _
                                                   , dtDatos1 _
                                                   , dtDatos2 _
                                                   , Msj) Then
                                For Each oRen As DataRow In dtDatos.Rows
                                    txt_Pediddo.Text = oRen.Item("pedido").ToString.Trim()
                                    txt_Cliente.Text = oRen.Item("cuenta").ToString.Trim()
                                    txt_TipoCliente.Text = oRen.Item("referencia").ToString.Trim()
                                    txt_ClienteDescto.Text = oRen.Item("descuento_global")
                                    txt_DesctoGlobal.Text = oRen.Item("descuento_global")


                                    Dim DesctoGlobal As Decimal
                                    If txt_ClienteDescto.Text <> "" Then
                                        DesctoGlobal = Double.Parse(txt_ClienteDescto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.AllowDecimalPoint)
                                    End If

                                    If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                                    , Globales.oAmbientes.Id_Sucursal _
                                                    , txt_Autonumsuc.Text _
                                                    , False _
                                                    , DesctoGlobal _
                                                    , 0 _
                                                    , 0 _
                                                    , 0 _
                                                    , Msj) Then

                                    Else
                                        MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End If
                                Next
                            End If

                            Call Refrescar()
                        Else
                            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    Else
                        MessageBox.Show("La venta de mostrador o cotización (" & oFrm.Autonumsuc & ") ya se encuentra usado o cancelado", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Call Refrescar()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub DatosFacturar()
        Dim oFrm As pdvFactura
        Try
            oFrm = New pdvFactura

            oFrm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Fraccionar()
        Dim oFrm As pdvFracciones
        Try
            oFrm = New pdvFracciones

            oFrm.ShowDialog()
            If oFrm.Accion Then
                txt_Codigo.Text = oFrm.Fraccion & "*" & oFrm.txt_Articulo.Text
            Else
                txt_Codigo.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub pdvPuntoVenta_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Try
            SplitContainer3.SplitterDistance = SplitContainer3.Width - IIf(CG_Venta.Visible, 245, 0) - IIf(CG_ProgramaLealtad.Visible, 245, 0) - IIf(CG_Ayuda.Visible, 245, 0)
        Catch ex As Exception
            SplitContainer3.SplitterDistance = SplitContainer3.Panel1MinSize
        End Try
    End Sub
    Private Sub CG_VisibleChanged(sender As Object, e As EventArgs) Handles CG_Ayuda.VisibleChanged _
        , CG_ProgramaLealtad.VisibleChanged _
        , CG_Venta.VisibleChanged
        Try
            SplitContainer3.SplitterDistance = SplitContainer3.Width - IIf(CG_Venta.Visible, 245, 0) - IIf(CG_ProgramaLealtad.Visible, 245, 0) - IIf(CG_Ayuda.Visible, 245, 0)
        Catch ex As Exception
            SplitContainer3.SplitterDistance = SplitContainer3.Panel1MinSize
        End Try
    End Sub
    Public Sub PVTA_Clientes()
        Try
            Call DatosGenerales("CLIENTES")
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub GridV_1_CellValueChanged(sender As Object, e As CellValueChangedEventArgs) Handles GridV_1.CellValueChanged
        Dim oDatos As Datos_Viscoi
        Dim Mensaje As String
        Dim Nombre As String
        Dim Renglon As Integer = 0
        Try
            If e.Column.FieldName = col1_Nombre.FieldName Then
                oDatos = New Datos_Viscoi
                Mensaje = ""
                Nombre = ""
                Renglon = GridV_1.GetRowCellValue(e.RowHandle, col1_Renglon.FieldName)
                Nombre = GridV_1.GetRowCellValue(e.RowHandle, col1_Nombre.FieldName)
                If Not oDatos.Actualiza_DetVentas_Nombre(Globales.oAmbientes.Id_Empresa _
                                                    , Globales.oAmbientes.Id_Sucursal _
                                                    , txt_Autonumsuc.Text _
                                                    , Renglon _
                                                    , Nombre _
                                                    , Mensaje) Then

                    MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & Mensaje, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub GridV_1_RowClick(ByVal sender As Object, ByVal e As EventArgs) Handles GridV_1.RowClick
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView
        view = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)

        ' Obtener el número de fila seleccionado
        Dim selectedRowAux As Integer
        selectedRowAux = view.FocusedRowHandle

        selectedRow = selectedRowAux + 1

    End Sub

    Private Sub GridV_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles GridV_1.MouseDown
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView
        view = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)

        ' Obtener la información de la ubicación del clic
        Dim hitInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo
        hitInfo = view.CalcHitInfo(e.X, e.Y)

        ' Si se hace clic fuera de las filas
        If hitInfo.InRow = False Then
            selectedRow = 0
        End If
    End Sub


    Public Sub PVTA_VTACotizacion()
        MessageBox.Show("Utiliza esta funcion desde la pantalla de GENERALES.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub PedidosWeb()
        Dim oDatos As Datos_Viscoi
        Dim Mensaje As String

        Try
            oDatos = New Datos_Viscoi
            MensajeNotificacion = ""
            PedidosPendientes = 0
            If oDatos.PVTA_PedidosNotificacion_Sel(Globales.oAmbientes.Id_Empresa _
                                                , Globales.oAmbientes.Id_Sucursal _
                                                , PedidosPendientes _
                                                , MensajeNotificacion) Then
                sw_timer = True
            Else
                PedidosPendientes = 0
                sw_timer = False
                MensajeNotificacion = "VOficina 5+"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            lbl_Sistema.Text = "VOficina 5+"

            If Not HiloPedidosWeb Is Nothing Then
                If sw_timer Then
                    lbl_Sistema.Text = MensajeNotificacion
                    If PedidosPendientes > 0 Then
                        lbl_Sistema.BackColor = Color.Yellow
                        lbl_Sistema.ForeColor = Color.Black
                        lbl_Sistema.BorderStyle = BorderStyle.FixedSingle

                        lbl_Sistema.Text = "(" & PedidosPendientes.ToString & ")PEDIDOS PENDIENTES"
                        txt_MensajeNotificacion.Text = MensajeNotificacion
                    Else
                        lbl_Sistema.BackColor = Color.Transparent
                        lbl_Sistema.ForeColor = Color.Black
                        lbl_Sistema.Text = "VOficina 5+"
                        txt_MensajeNotificacion.Text = ""
                    End If
                    Timer1.Enabled = True
                Else
                    lbl_Sistema.BackColor = Color.Transparent
                    lbl_Sistema.ForeColor = Color.Black
                    lbl_Sistema.Text = "VOficina 5+"
                    txt_MensajeNotificacion.Text = ""
                    lbl_Sistema.BorderStyle = BorderStyle.None
                End If
            End If

            iTimer += 1
            If iTimer > 60 Then
                iTimer = 0
                If Not HiloPedidosWeb Is Nothing Then
                    If HiloPedidosWeb.ThreadState = Threading.ThreadState.Stopped Then
                        HiloPedidosWeb = New System.Threading.Thread(AddressOf PedidosWeb)
                    End If
                    HiloPedidosWeb.Start()
                End If
            End If
        Catch ex As Exception
            iTimer = 0
        End Try
    End Sub

    Private Sub lbl_Sistema_DoubleClick(sender As Object, e As EventArgs) Handles lbl_Sistema.DoubleClick
        Dim frm1 As LogMasivo
        Dim lstLista As List(Of String)
        Try
            frm1 = New LogMasivo
            If PedidosPendientes > 0 Then
                lstLista = New List(Of String)
                For Each xCad As String In MensajeNotificacion.Split("|")
                    lstLista.Add(xCad)
                Next
                frm1.Log = lstLista
                frm1.ShowDialog()
            End If
        Catch ex As Exception

        End Try
        frm1 = Nothing
    End Sub


End Class
