﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvDatosGenerales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DefaultToolTipController1 = New DevExpress.Utils.DefaultToolTipController(Me.components)
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.txt_ClienteEstatus = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_ClienteTipo = New System.Windows.Forms.TextBox()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btn_InfoPedidosWeb = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_PedidoDet = New DevExpress.XtraEditors.TextEdit()
        Me.btn_PedidoWeb = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Empleado = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Vendedor2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Vendedor1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Cliente = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_pedido = New DevExpress.XtraEditors.TextEdit()
        Me.txt_ClienteNombre = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Cliente = New DevExpress.XtraEditors.TextEdit()
        Me.txt_VendedorNombre2 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Vendedor2 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_VendedorNombre1 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Vendedor1 = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Buscar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Existencia = New DevExpress.XtraEditors.SimpleButton()
        Me.Btn_Pagos = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Entregas = New DevExpress.XtraEditors.SimpleButton()
        Me.GC_Mostrador = New DevExpress.XtraEditors.GroupControl()
        Me.txt_Referencia = New DevExpress.XtraEditors.TextEdit()
        Me.txt_CodigoCanje = New DevExpress.XtraEditors.TextEdit()
        Me.txt_NombreCanje = New DevExpress.XtraEditors.TextEdit()
        Me.txt_DE = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Puntos = New DevExpress.XtraEditors.TextEdit()
        Me.txt_NombreCanje1 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_TipoCliente = New DevExpress.XtraEditors.TextEdit()
        Me.txt_ClienteDescto = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IdLealtad = New DevExpress.XtraEditors.TextEdit()
        Me.GC_ProgramaLealtad = New DevExpress.XtraEditors.GroupControl()
        Me.txt_ClienteCondicion = New DevExpress.XtraEditors.TextEdit()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GC_Vale = New DevExpress.XtraEditors.GroupControl()
        Me.btn_FinalCliente = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Folio = New DevExpress.XtraEditors.TextEdit()
        Me.txt_NombreClienteFinal = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IdClienteFinal = New DevExpress.XtraEditors.TextEdit()
        Me.Btn_Nuevo = New System.Windows.Forms.Button()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txt_PedidoDet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_pedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ClienteNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Cliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_VendedorNombre2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Vendedor2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_VendedorNombre1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Vendedor1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GC_Mostrador, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Mostrador.SuspendLayout()
        CType(Me.txt_Referencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NombreCanje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_DE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Puntos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NombreCanje1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_TipoCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ClienteDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IdLealtad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_ProgramaLealtad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_ProgramaLealtad.SuspendLayout()
        CType(Me.txt_ClienteCondicion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_Vale, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Vale.SuspendLayout()
        CType(Me.txt_Folio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NombreClienteFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IdClienteFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultToolTipController1
        '
        '
        '
        '
        Me.DefaultToolTipController1.DefaultController.KeepWhileHovered = True
        '
        'btn_Aceptar
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.btn_Aceptar, DevExpress.Utils.DefaultBoolean.[Default])
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(304, 27)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.btn_Cancelar, DevExpress.Utils.DefaultBoolean.[Default])
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(377, 27)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label8, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(4, 180)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 24)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Pedido"
        '
        'Label3
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label3, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(5, 111)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 24)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Cliente"
        '
        'Label2
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label2, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(5, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 24)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Vendedor 2"
        '
        'Label7
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label7, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(5, 37)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 24)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Vendedor"
        '
        'Label12
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label12, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(5, 29)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 24)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "Referencia"
        '
        'txt_Cajero
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.txt_Cajero, DevExpress.Utils.DefaultBoolean.[Default])
        Me.txt_Cajero.Location = New System.Drawing.Point(484, 27)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(30, 21)
        Me.txt_Cajero.TabIndex = 26
        Me.txt_Cajero.Text = "0001"
        Me.txt_Cajero.Visible = False
        '
        'txt_Caja
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.txt_Caja, DevExpress.Utils.DefaultBoolean.[Default])
        Me.txt_Caja.Location = New System.Drawing.Point(484, 50)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(30, 21)
        Me.txt_Caja.TabIndex = 25
        Me.txt_Caja.Text = "0001"
        Me.txt_Caja.Visible = False
        '
        'txt_ClienteEstatus
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.txt_ClienteEstatus, DevExpress.Utils.DefaultBoolean.[Default])
        Me.txt_ClienteEstatus.Location = New System.Drawing.Point(431, 26)
        Me.txt_ClienteEstatus.Name = "txt_ClienteEstatus"
        Me.txt_ClienteEstatus.Size = New System.Drawing.Size(30, 21)
        Me.txt_ClienteEstatus.TabIndex = 27
        Me.txt_ClienteEstatus.Text = "0001"
        Me.txt_ClienteEstatus.Visible = False
        '
        'Label4
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label4, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 24)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Codigo Canje"
        '
        'Label5
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label5, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 24)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Nombre"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label6, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 101)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(113, 24)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Impote D. E."
        '
        'Label1
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label1, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 137)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 24)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Puntos"
        '
        'Label11
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label11, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(321, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 24)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Folio"
        '
        'Label9
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label9, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 70)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(109, 24)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Nombre"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Label13, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(6, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(114, 24)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Cliente Final"
        '
        'txt_ClienteTipo
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.txt_ClienteTipo, DevExpress.Utils.DefaultBoolean.[Default])
        Me.txt_ClienteTipo.Location = New System.Drawing.Point(431, 53)
        Me.txt_ClienteTipo.Name = "txt_ClienteTipo"
        Me.txt_ClienteTipo.Size = New System.Drawing.Size(30, 21)
        Me.txt_ClienteTipo.TabIndex = 28
        Me.txt_ClienteTipo.Text = "0001"
        Me.txt_ClienteTipo.Visible = False
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btn_InfoPedidosWeb)
        Me.GroupControl2.Controls.Add(Me.txt_PedidoDet)
        Me.GroupControl2.Controls.Add(Me.btn_PedidoWeb)
        Me.GroupControl2.Controls.Add(Me.btn_Empleado)
        Me.GroupControl2.Controls.Add(Me.btn_Vendedor2)
        Me.GroupControl2.Controls.Add(Me.btn_Vendedor1)
        Me.GroupControl2.Controls.Add(Me.btn_Cliente)
        Me.GroupControl2.Controls.Add(Me.txt_pedido)
        Me.GroupControl2.Controls.Add(Me.Label8)
        Me.GroupControl2.Controls.Add(Me.txt_ClienteNombre)
        Me.GroupControl2.Controls.Add(Me.txt_Cliente)
        Me.GroupControl2.Controls.Add(Me.Label3)
        Me.GroupControl2.Controls.Add(Me.txt_VendedorNombre2)
        Me.GroupControl2.Controls.Add(Me.txt_Vendedor2)
        Me.GroupControl2.Controls.Add(Me.Label2)
        Me.GroupControl2.Controls.Add(Me.txt_VendedorNombre1)
        Me.GroupControl2.Controls.Add(Me.txt_Vendedor1)
        Me.GroupControl2.Controls.Add(Me.Label7)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(514, 217)
        Me.GroupControl2.TabIndex = 0
        Me.GroupControl2.ToolTipController = Me.DefaultToolTipController1.DefaultController
        '
        'btn_InfoPedidosWeb
        '
        Me.btn_InfoPedidosWeb.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_InfoPedidosWeb.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_InfoPedidosWeb.ImageOptions.Image = Global.proyVOficina_PDV.My.Resources.Resources.docs
        Me.btn_InfoPedidosWeb.Location = New System.Drawing.Point(431, 177)
        Me.btn_InfoPedidosWeb.Name = "btn_InfoPedidosWeb"
        Me.btn_InfoPedidosWeb.Size = New System.Drawing.Size(65, 30)
        Me.btn_InfoPedidosWeb.TabIndex = 21
        Me.btn_InfoPedidosWeb.Text = "(00)"
        '
        'txt_PedidoDet
        '
        Me.txt_PedidoDet.EditValue = ""
        Me.txt_PedidoDet.Location = New System.Drawing.Point(73, 177)
        Me.txt_PedidoDet.Name = "txt_PedidoDet"
        Me.txt_PedidoDet.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PedidoDet.Properties.Appearance.Options.UseFont = True
        Me.txt_PedidoDet.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_PedidoDet.Size = New System.Drawing.Size(42, 30)
        Me.txt_PedidoDet.TabIndex = 20
        Me.txt_PedidoDet.Visible = False
        '
        'btn_PedidoWeb
        '
        Me.btn_PedidoWeb.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_PedidoWeb.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_PedidoWeb.Location = New System.Drawing.Point(325, 177)
        Me.btn_PedidoWeb.Name = "btn_PedidoWeb"
        Me.btn_PedidoWeb.Size = New System.Drawing.Size(100, 30)
        Me.btn_PedidoWeb.TabIndex = 19
        Me.btn_PedidoWeb.Text = "Buscar Pedido Web"
        '
        'btn_Empleado
        '
        Me.btn_Empleado.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Empleado.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Empleado.Location = New System.Drawing.Point(431, 108)
        Me.btn_Empleado.Name = "btn_Empleado"
        Me.btn_Empleado.Size = New System.Drawing.Size(64, 30)
        Me.btn_Empleado.TabIndex = 9
        Me.btn_Empleado.Text = "Empleado"
        '
        'btn_Vendedor2
        '
        Me.btn_Vendedor2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Vendedor2.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Vendedor2.Location = New System.Drawing.Point(194, 70)
        Me.btn_Vendedor2.Name = "btn_Vendedor2"
        Me.btn_Vendedor2.Size = New System.Drawing.Size(36, 30)
        Me.btn_Vendedor2.TabIndex = 6
        Me.btn_Vendedor2.Text = "..."
        '
        'btn_Vendedor1
        '
        Me.btn_Vendedor1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Vendedor1.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Vendedor1.Location = New System.Drawing.Point(194, 34)
        Me.btn_Vendedor1.Name = "btn_Vendedor1"
        Me.btn_Vendedor1.Size = New System.Drawing.Size(36, 30)
        Me.btn_Vendedor1.TabIndex = 4
        Me.btn_Vendedor1.Text = "..."
        '
        'btn_Cliente
        '
        Me.btn_Cliente.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Cliente.Location = New System.Drawing.Point(361, 108)
        Me.btn_Cliente.Name = "btn_Cliente"
        Me.btn_Cliente.Size = New System.Drawing.Size(64, 30)
        Me.btn_Cliente.TabIndex = 8
        Me.btn_Cliente.Text = "Cliente"
        '
        'txt_pedido
        '
        Me.txt_pedido.Location = New System.Drawing.Point(121, 177)
        Me.txt_pedido.Name = "txt_pedido"
        Me.txt_pedido.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pedido.Properties.Appearance.Options.UseFont = True
        Me.txt_pedido.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_pedido.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_pedido.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_pedido.Size = New System.Drawing.Size(193, 30)
        Me.txt_pedido.TabIndex = 9
        '
        'txt_ClienteNombre
        '
        Me.txt_ClienteNombre.Location = New System.Drawing.Point(121, 141)
        Me.txt_ClienteNombre.Name = "txt_ClienteNombre"
        Me.txt_ClienteNombre.Properties.AllowFocused = False
        Me.txt_ClienteNombre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteNombre.Properties.Appearance.Options.UseFont = True
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_ClienteNombre.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_ClienteNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ClienteNombre.Properties.ReadOnly = True
        Me.txt_ClienteNombre.Size = New System.Drawing.Size(375, 30)
        Me.txt_ClienteNombre.TabIndex = 2
        '
        'txt_Cliente
        '
        Me.txt_Cliente.Location = New System.Drawing.Point(121, 108)
        Me.txt_Cliente.Name = "txt_Cliente"
        Me.txt_Cliente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Cliente.Properties.Appearance.Options.UseFont = True
        Me.txt_Cliente.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Cliente.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Cliente.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Cliente.Size = New System.Drawing.Size(234, 30)
        Me.txt_Cliente.TabIndex = 7
        '
        'txt_VendedorNombre2
        '
        Me.txt_VendedorNombre2.Location = New System.Drawing.Point(236, 70)
        Me.txt_VendedorNombre2.Name = "txt_VendedorNombre2"
        Me.txt_VendedorNombre2.Properties.AllowFocused = False
        Me.txt_VendedorNombre2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_VendedorNombre2.Properties.Appearance.Options.UseFont = True
        Me.txt_VendedorNombre2.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_VendedorNombre2.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_VendedorNombre2.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_VendedorNombre2.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_VendedorNombre2.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_VendedorNombre2.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_VendedorNombre2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_VendedorNombre2.Properties.ReadOnly = True
        Me.txt_VendedorNombre2.Size = New System.Drawing.Size(259, 30)
        Me.txt_VendedorNombre2.TabIndex = 1
        '
        'txt_Vendedor2
        '
        Me.txt_Vendedor2.Location = New System.Drawing.Point(121, 70)
        Me.txt_Vendedor2.Name = "txt_Vendedor2"
        Me.txt_Vendedor2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Vendedor2.Properties.Appearance.Options.UseFont = True
        Me.txt_Vendedor2.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Vendedor2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Vendedor2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Vendedor2.Size = New System.Drawing.Size(67, 30)
        Me.txt_Vendedor2.TabIndex = 5
        '
        'txt_VendedorNombre1
        '
        Me.txt_VendedorNombre1.Location = New System.Drawing.Point(236, 34)
        Me.txt_VendedorNombre1.Name = "txt_VendedorNombre1"
        Me.txt_VendedorNombre1.Properties.AllowFocused = False
        Me.txt_VendedorNombre1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_VendedorNombre1.Properties.Appearance.Options.UseFont = True
        Me.txt_VendedorNombre1.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_VendedorNombre1.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_VendedorNombre1.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_VendedorNombre1.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_VendedorNombre1.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_VendedorNombre1.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_VendedorNombre1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_VendedorNombre1.Properties.ReadOnly = True
        Me.txt_VendedorNombre1.Size = New System.Drawing.Size(259, 30)
        Me.txt_VendedorNombre1.TabIndex = 0
        '
        'txt_Vendedor1
        '
        Me.txt_Vendedor1.Location = New System.Drawing.Point(121, 34)
        Me.txt_Vendedor1.Name = "txt_Vendedor1"
        Me.txt_Vendedor1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Vendedor1.Properties.Appearance.Options.UseFont = True
        Me.txt_Vendedor1.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Vendedor1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Vendedor1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Vendedor1.Size = New System.Drawing.Size(67, 30)
        Me.txt_Vendedor1.TabIndex = 3
        '
        'GroupControl3
        '
        Me.GroupControl3.AllowHtmlText = True
        Me.GroupControl3.Controls.Add(Me.txt_ClienteTipo)
        Me.GroupControl3.Controls.Add(Me.txt_ClienteEstatus)
        Me.GroupControl3.Controls.Add(Me.txt_Cajero)
        Me.GroupControl3.Controls.Add(Me.txt_Caja)
        Me.GroupControl3.Controls.Add(Me.btn_Buscar)
        Me.GroupControl3.Controls.Add(Me.btn_Existencia)
        Me.GroupControl3.Controls.Add(Me.Btn_Pagos)
        Me.GroupControl3.Controls.Add(Me.btn_Entregas)
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 563)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(514, 94)
        Me.GroupControl3.TabIndex = 3
        Me.GroupControl3.ToolTipController = Me.DefaultToolTipController1.DefaultController
        '
        'btn_Buscar
        '
        Me.btn_Buscar.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Buscar.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Buscar.Location = New System.Drawing.Point(450, 27)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Buscar.TabIndex = 24
        Me.btn_Buscar.Text = "M / C"
        '
        'btn_Existencia
        '
        Me.btn_Existencia.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Existencia.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Existencia.Location = New System.Drawing.Point(206, 27)
        Me.btn_Existencia.Name = "btn_Existencia"
        Me.btn_Existencia.Size = New System.Drawing.Size(67, 42)
        Me.btn_Existencia.TabIndex = 23
        Me.btn_Existencia.Text = "E&xistencias"
        '
        'Btn_Pagos
        '
        Me.Btn_Pagos.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.Btn_Pagos.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.Btn_Pagos.Location = New System.Drawing.Point(108, 27)
        Me.Btn_Pagos.Name = "Btn_Pagos"
        Me.Btn_Pagos.Size = New System.Drawing.Size(67, 42)
        Me.Btn_Pagos.TabIndex = 22
        Me.Btn_Pagos.Text = "Recibir &Pagos"
        '
        'btn_Entregas
        '
        Me.btn_Entregas.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Entregas.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_Entregas.Location = New System.Drawing.Point(10, 27)
        Me.btn_Entregas.Name = "btn_Entregas"
        Me.btn_Entregas.Size = New System.Drawing.Size(67, 42)
        Me.btn_Entregas.TabIndex = 21
        Me.btn_Entregas.Text = "&Entregas"
        '
        'GC_Mostrador
        '
        Me.GC_Mostrador.AppearanceCaption.Options.UseTextOptions = True
        Me.GC_Mostrador.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GC_Mostrador.Controls.Add(Me.Label12)
        Me.GC_Mostrador.Controls.Add(Me.txt_Referencia)
        Me.GC_Mostrador.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_Mostrador.Location = New System.Drawing.Point(0, 501)
        Me.GC_Mostrador.Name = "GC_Mostrador"
        Me.GC_Mostrador.Size = New System.Drawing.Size(514, 62)
        Me.GC_Mostrador.TabIndex = 2
        Me.GC_Mostrador.Text = "Mostrador"
        Me.GC_Mostrador.ToolTipController = Me.DefaultToolTipController1.DefaultController
        '
        'txt_Referencia
        '
        Me.txt_Referencia.Location = New System.Drawing.Point(170, 26)
        Me.txt_Referencia.Name = "txt_Referencia"
        Me.txt_Referencia.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Referencia.Properties.Appearance.Options.UseFont = True
        Me.txt_Referencia.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Referencia.Size = New System.Drawing.Size(325, 30)
        Me.txt_Referencia.TabIndex = 0
        '
        'txt_CodigoCanje
        '
        Me.txt_CodigoCanje.Location = New System.Drawing.Point(170, 26)
        Me.txt_CodigoCanje.Name = "txt_CodigoCanje"
        Me.txt_CodigoCanje.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CodigoCanje.Properties.Appearance.Options.UseFont = True
        Me.txt_CodigoCanje.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CodigoCanje.Size = New System.Drawing.Size(120, 30)
        Me.txt_CodigoCanje.TabIndex = 0
        '
        'txt_NombreCanje
        '
        Me.txt_NombreCanje.Location = New System.Drawing.Point(169, 62)
        Me.txt_NombreCanje.Name = "txt_NombreCanje"
        Me.txt_NombreCanje.Properties.AllowFocused = False
        Me.txt_NombreCanje.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreCanje.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreCanje.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreCanje.Size = New System.Drawing.Size(326, 30)
        Me.txt_NombreCanje.TabIndex = 1
        '
        'txt_DE
        '
        Me.txt_DE.Location = New System.Drawing.Point(170, 98)
        Me.txt_DE.Name = "txt_DE"
        Me.txt_DE.Properties.AllowFocused = False
        Me.txt_DE.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DE.Properties.Appearance.Options.UseFont = True
        Me.txt_DE.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txt_DE.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_DE.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DE.Properties.AppearanceReadOnly.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.txt_DE.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DE.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DE.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_DE.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.txt_DE.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_DE.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.txt_DE.Properties.MaskSettings.Set("mask", "C2")
        Me.txt_DE.Properties.ReadOnly = True
        Me.txt_DE.Size = New System.Drawing.Size(121, 32)
        Me.txt_DE.TabIndex = 2
        '
        'txt_Puntos
        '
        Me.txt_Puntos.Location = New System.Drawing.Point(169, 136)
        Me.txt_Puntos.Name = "txt_Puntos"
        Me.txt_Puntos.Properties.AllowFocused = False
        Me.txt_Puntos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos.Properties.Appearance.Options.UseFont = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LimeGreen
        Me.txt_Puntos.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.Black
        Me.txt_Puntos.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos.Properties.AppearanceReadOnly.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.txt_Puntos.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Puntos.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Puntos.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.txt_Puntos.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Puntos.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.txt_Puntos.Properties.MaskSettings.Set("mask", "N0")
        Me.txt_Puntos.Properties.ReadOnly = True
        Me.txt_Puntos.Size = New System.Drawing.Size(121, 32)
        Me.txt_Puntos.TabIndex = 3
        '
        'txt_NombreCanje1
        '
        Me.txt_NombreCanje1.EditValue = ""
        Me.txt_NombreCanje1.Location = New System.Drawing.Point(454, 100)
        Me.txt_NombreCanje1.Name = "txt_NombreCanje1"
        Me.txt_NombreCanje1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreCanje1.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreCanje1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreCanje1.Size = New System.Drawing.Size(42, 30)
        Me.txt_NombreCanje1.TabIndex = 14
        Me.txt_NombreCanje1.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.EditValue = ""
        Me.txt_TipoCliente.Location = New System.Drawing.Point(453, 138)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Properties.Appearance.Options.UseFont = True
        Me.txt_TipoCliente.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_TipoCliente.Size = New System.Drawing.Size(42, 30)
        Me.txt_TipoCliente.TabIndex = 15
        Me.txt_TipoCliente.Visible = False
        '
        'txt_ClienteDescto
        '
        Me.txt_ClienteDescto.EditValue = ""
        Me.txt_ClienteDescto.Location = New System.Drawing.Point(405, 138)
        Me.txt_ClienteDescto.Name = "txt_ClienteDescto"
        Me.txt_ClienteDescto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteDescto.Properties.Appearance.Options.UseFont = True
        Me.txt_ClienteDescto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ClienteDescto.Size = New System.Drawing.Size(42, 30)
        Me.txt_ClienteDescto.TabIndex = 16
        Me.txt_ClienteDescto.Visible = False
        '
        'txt_IdLealtad
        '
        Me.txt_IdLealtad.EditValue = ""
        Me.txt_IdLealtad.Location = New System.Drawing.Point(296, 23)
        Me.txt_IdLealtad.Name = "txt_IdLealtad"
        Me.txt_IdLealtad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IdLealtad.Properties.Appearance.Options.UseFont = True
        Me.txt_IdLealtad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IdLealtad.Size = New System.Drawing.Size(42, 30)
        Me.txt_IdLealtad.TabIndex = 17
        Me.txt_IdLealtad.Visible = False
        '
        'GC_ProgramaLealtad
        '
        Me.GC_ProgramaLealtad.AppearanceCaption.Options.UseTextOptions = True
        Me.GC_ProgramaLealtad.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_ClienteCondicion)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_IdLealtad)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_ClienteDescto)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_TipoCliente)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_NombreCanje1)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_Puntos)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label1)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label6)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_DE)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label5)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_NombreCanje)
        Me.GC_ProgramaLealtad.Controls.Add(Me.Label4)
        Me.GC_ProgramaLealtad.Controls.Add(Me.txt_CodigoCanje)
        Me.GC_ProgramaLealtad.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_ProgramaLealtad.Location = New System.Drawing.Point(0, 316)
        Me.GC_ProgramaLealtad.Name = "GC_ProgramaLealtad"
        Me.GC_ProgramaLealtad.Size = New System.Drawing.Size(514, 185)
        Me.GC_ProgramaLealtad.TabIndex = 1
        Me.GC_ProgramaLealtad.Text = "Programa Lealtad"
        Me.GC_ProgramaLealtad.ToolTipController = Me.DefaultToolTipController1.DefaultController
        '
        'txt_ClienteCondicion
        '
        Me.txt_ClienteCondicion.EditValue = ""
        Me.txt_ClienteCondicion.Location = New System.Drawing.Point(406, 99)
        Me.txt_ClienteCondicion.Name = "txt_ClienteCondicion"
        Me.txt_ClienteCondicion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ClienteCondicion.Properties.Appearance.Options.UseFont = True
        Me.txt_ClienteCondicion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ClienteCondicion.Size = New System.Drawing.Size(42, 30)
        Me.txt_ClienteCondicion.TabIndex = 18
        Me.txt_ClienteCondicion.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'GC_Vale
        '
        Me.GC_Vale.AppearanceCaption.Options.UseTextOptions = True
        Me.GC_Vale.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GC_Vale.Controls.Add(Me.Btn_Nuevo)
        Me.GC_Vale.Controls.Add(Me.btn_FinalCliente)
        Me.GC_Vale.Controls.Add(Me.Label11)
        Me.GC_Vale.Controls.Add(Me.txt_Folio)
        Me.GC_Vale.Controls.Add(Me.Label9)
        Me.GC_Vale.Controls.Add(Me.txt_NombreClienteFinal)
        Me.GC_Vale.Controls.Add(Me.Label13)
        Me.GC_Vale.Controls.Add(Me.txt_IdClienteFinal)
        Me.GC_Vale.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_Vale.Location = New System.Drawing.Point(0, 217)
        Me.GC_Vale.Name = "GC_Vale"
        Me.GC_Vale.Size = New System.Drawing.Size(514, 99)
        Me.GC_Vale.TabIndex = 4
        Me.GC_Vale.Text = "Vale"
        Me.GC_Vale.ToolTipController = Me.DefaultToolTipController1.DefaultController
        '
        'btn_FinalCliente
        '
        Me.btn_FinalCliente.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_FinalCliente.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.[True]
        Me.btn_FinalCliente.Location = New System.Drawing.Point(247, 26)
        Me.btn_FinalCliente.Name = "btn_FinalCliente"
        Me.btn_FinalCliente.Size = New System.Drawing.Size(36, 30)
        Me.btn_FinalCliente.TabIndex = 18
        Me.btn_FinalCliente.Text = "..."
        '
        'txt_Folio
        '
        Me.txt_Folio.Location = New System.Drawing.Point(377, 26)
        Me.txt_Folio.Name = "txt_Folio"
        Me.txt_Folio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Folio.Properties.Appearance.Options.UseFont = True
        Me.txt_Folio.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Folio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Folio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Folio.Size = New System.Drawing.Size(118, 30)
        Me.txt_Folio.TabIndex = 16
        '
        'txt_NombreClienteFinal
        '
        Me.txt_NombreClienteFinal.Location = New System.Drawing.Point(121, 64)
        Me.txt_NombreClienteFinal.Name = "txt_NombreClienteFinal"
        Me.txt_NombreClienteFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NombreClienteFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_NombreClienteFinal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_NombreClienteFinal.Properties.ReadOnly = True
        Me.txt_NombreClienteFinal.Size = New System.Drawing.Size(389, 30)
        Me.txt_NombreClienteFinal.TabIndex = 13
        '
        'txt_IdClienteFinal
        '
        Me.txt_IdClienteFinal.Location = New System.Drawing.Point(121, 28)
        Me.txt_IdClienteFinal.Name = "txt_IdClienteFinal"
        Me.txt_IdClienteFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IdClienteFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_IdClienteFinal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IdClienteFinal.Properties.ReadOnly = True
        Me.txt_IdClienteFinal.Size = New System.Drawing.Size(120, 30)
        Me.txt_IdClienteFinal.TabIndex = 12
        '
        'Btn_Nuevo
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me.Btn_Nuevo, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Btn_Nuevo.Image = Global.proyVOficina_PDV.My.Resources.Resources.agregar_boton
        Me.Btn_Nuevo.Location = New System.Drawing.Point(289, 26)
        Me.Btn_Nuevo.Name = "Btn_Nuevo"
        Me.Btn_Nuevo.Size = New System.Drawing.Size(30, 30)
        Me.Btn_Nuevo.TabIndex = 19
        Me.Btn_Nuevo.UseVisualStyleBackColor = True
        '
        'pdvDatosGenerales
        '
        Me.DefaultToolTipController1.SetAllowHtmlText(Me, DevExpress.Utils.DefaultBoolean.[Default])
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(514, 657)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GC_Mostrador)
        Me.Controls.Add(Me.GC_ProgramaLealtad)
        Me.Controls.Add(Me.GC_Vale)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvDatosGenerales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales Venta"
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txt_PedidoDet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_pedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ClienteNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Cliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_VendedorNombre2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Vendedor2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_VendedorNombre1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Vendedor1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.GC_Mostrador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Mostrador.ResumeLayout(False)
        Me.GC_Mostrador.PerformLayout()
        CType(Me.txt_Referencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_CodigoCanje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NombreCanje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_DE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Puntos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NombreCanje1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_TipoCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ClienteDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IdLealtad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_ProgramaLealtad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_ProgramaLealtad.ResumeLayout(False)
        Me.GC_ProgramaLealtad.PerformLayout()
        CType(Me.txt_ClienteCondicion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_Vale, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Vale.ResumeLayout(False)
        Me.GC_Vale.PerformLayout()
        CType(Me.txt_Folio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NombreClienteFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IdClienteFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_VendedorNombre1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Vendedor1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_VendedorNombre2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Vendedor2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_Cliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_ClienteNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_pedido As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As Label
    Friend WithEvents GC_Mostrador As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label12 As Label
    Friend WithEvents txt_Referencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_Buscar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Existencia As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Btn_Pagos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Entregas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DefaultToolTipController1 As DevExpress.Utils.DefaultToolTipController
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents btn_Vendedor2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Vendedor1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Cliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Empleado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_ClienteEstatus As TextBox
    Friend WithEvents txt_CodigoCanje As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_NombreCanje As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_DE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label6 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_Puntos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_NombreCanje1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_TipoCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_ClienteDescto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IdLealtad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GC_ProgramaLealtad As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txt_ClienteCondicion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_PedidoWeb As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_PedidoDet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_InfoPedidosWeb As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Timer1 As Timer
    Friend WithEvents GC_Vale As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label11 As Label
    Friend WithEvents txt_Folio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_NombreClienteFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As Label
    Friend WithEvents txt_IdClienteFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_FinalCliente As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_ClienteTipo As TextBox
    Friend WithEvents Btn_Nuevo As Button
End Class
