﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvTicketCancelacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim GridFormatRule1 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue1 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule2 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue2 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule3 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue3 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule4 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue4 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule5 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue5 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule6 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue6 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule7 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue7 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule8 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue8 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule9 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue9 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule10 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue10 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule11 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue11 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule12 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue12 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule13 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue13 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule14 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue14 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim WindowsUIButtonImageOptions1 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pdvTicketCancelacion))
        Dim WindowsUIButtonImageOptions2 As DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions = New DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions()
        Me.col1_Tipo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Promocion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Importe = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Cantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Descuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Renglon = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDatosTicket = New DevExpress.XtraEditors.GroupControl()
        Me.txt_AS_Cancela = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_Buscar = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Autonumsuc = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cbo_Sucursal = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_Empresa = New System.Windows.Forms.TextBox()
        Me.txt_RFC = New System.Windows.Forms.TextBox()
        Me.txt_Direccion = New System.Windows.Forms.TextBox()
        Me.txt_Direccion2 = New System.Windows.Forms.TextBox()
        Me.txt_TipoCliente = New System.Windows.Forms.TextBox()
        Me.txt_FormatoTicket = New System.Windows.Forms.TextBox()
        Me.txt_MargenesTicket = New System.Windows.Forms.TextBox()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_FacFolio = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_FacSerie = New System.Windows.Forms.TextBox()
        Me.chk_Factura = New System.Windows.Forms.CheckBox()
        Me.txt_Devolucion = New DevExpress.XtraEditors.TextEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_Total = New DevExpress.XtraEditors.TextEdit()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1_Articulo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_NomCorto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Nombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_PrecioAct = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_PrecioNeto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_CantidadOri = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_UniVta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Sep = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_DEOtorgago = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Division = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Depto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Familia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Precio5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelFiltros_Acciones = New DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel()
        Me.txt_Empresa1 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txt_Corte = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txt_Cajero = New System.Windows.Forms.TextBox()
        Me.txt_Caja = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatosTicket.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txt_Devolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.PanelFiltros_Acciones.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'col1_Tipo
        '
        Me.col1_Tipo.Caption = "Tipo"
        Me.col1_Tipo.FieldName = "tipo"
        Me.col1_Tipo.Name = "col1_Tipo"
        Me.col1_Tipo.OptionsColumn.AllowEdit = False
        Me.col1_Tipo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Promocion
        '
        Me.col1_Promocion.Caption = "Promocion"
        Me.col1_Promocion.FieldName = "promocion"
        Me.col1_Promocion.Name = "col1_Promocion"
        Me.col1_Promocion.OptionsColumn.AllowEdit = False
        Me.col1_Promocion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Importe
        '
        Me.col1_Importe.Caption = "Importe"
        Me.col1_Importe.DisplayFormat.FormatString = "C2"
        Me.col1_Importe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Importe.FieldName = "total"
        Me.col1_Importe.MaxWidth = 125
        Me.col1_Importe.Name = "col1_Importe"
        Me.col1_Importe.OptionsColumn.AllowEdit = False
        Me.col1_Importe.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Importe.Visible = True
        Me.col1_Importe.VisibleIndex = 10
        Me.col1_Importe.Width = 125
        '
        'col1_Cantidad
        '
        Me.col1_Cantidad.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.col1_Cantidad.AppearanceCell.Options.UseBackColor = True
        Me.col1_Cantidad.Caption = "Cantidad"
        Me.col1_Cantidad.DisplayFormat.FormatString = "N2"
        Me.col1_Cantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Cantidad.FieldName = "cantidad"
        Me.col1_Cantidad.Name = "col1_Cantidad"
        Me.col1_Cantidad.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Cantidad.Visible = True
        Me.col1_Cantidad.VisibleIndex = 7
        '
        'col1_Descuento
        '
        Me.col1_Descuento.Caption = "Descto"
        Me.col1_Descuento.DisplayFormat.FormatString = "P2"
        Me.col1_Descuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_Descuento.FieldName = "descuento"
        Me.col1_Descuento.Name = "col1_Descuento"
        Me.col1_Descuento.OptionsColumn.AllowEdit = False
        Me.col1_Descuento.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Descuento.Visible = True
        Me.col1_Descuento.VisibleIndex = 5
        '
        'col1_Renglon
        '
        Me.col1_Renglon.Caption = "Ren"
        Me.col1_Renglon.FieldName = "renglon"
        Me.col1_Renglon.Name = "col1_Renglon"
        Me.col1_Renglon.OptionsColumn.AllowEdit = False
        Me.col1_Renglon.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Renglon.Visible = True
        Me.col1_Renglon.VisibleIndex = 0
        '
        'gcDatosTicket
        '
        Me.gcDatosTicket.Controls.Add(Me.txt_AS_Cancela)
        Me.gcDatosTicket.Controls.Add(Me.Label5)
        Me.gcDatosTicket.Controls.Add(Me.btn_Buscar)
        Me.gcDatosTicket.Controls.Add(Me.txt_Autonumsuc)
        Me.gcDatosTicket.Controls.Add(Me.Label21)
        Me.gcDatosTicket.Controls.Add(Me.Label20)
        Me.gcDatosTicket.Controls.Add(Me.cbo_Sucursal)
        Me.gcDatosTicket.Controls.Add(Me.Label19)
        Me.gcDatosTicket.Controls.Add(Me.txt_Empresa)
        Me.gcDatosTicket.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcDatosTicket.Location = New System.Drawing.Point(0, 0)
        Me.gcDatosTicket.Name = "gcDatosTicket"
        Me.gcDatosTicket.Size = New System.Drawing.Size(1088, 103)
        Me.gcDatosTicket.TabIndex = 1
        Me.gcDatosTicket.Text = "Datos del Ticket"
        '
        'txt_AS_Cancela
        '
        Me.txt_AS_Cancela.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_AS_Cancela.Location = New System.Drawing.Point(829, 55)
        Me.txt_AS_Cancela.Name = "txt_AS_Cancela"
        Me.txt_AS_Cancela.ReadOnly = True
        Me.txt_AS_Cancela.Size = New System.Drawing.Size(200, 29)
        Me.txt_AS_Cancela.TabIndex = 24
        Me.txt_AS_Cancela.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(816, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(227, 24)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Autonumsuc Cancelación"
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Location = New System.Drawing.Point(647, 34)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(50, 50)
        Me.btn_Buscar.TabIndex = 3
        Me.btn_Buscar.Text = "Buscar"
        '
        'txt_Autonumsuc
        '
        Me.txt_Autonumsuc.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Autonumsuc.Location = New System.Drawing.Point(430, 55)
        Me.txt_Autonumsuc.Name = "txt_Autonumsuc"
        Me.txt_Autonumsuc.Size = New System.Drawing.Size(200, 29)
        Me.txt_Autonumsuc.TabIndex = 2
        Me.txt_Autonumsuc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(492, 31)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(117, 24)
        Me.Label21.TabIndex = 23
        Me.Label21.Text = "Autonumsuc"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(273, 31)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(83, 24)
        Me.Label20.TabIndex = 22
        Me.Label20.Text = "Sucursal"
        '
        'cbo_Sucursal
        '
        Me.cbo_Sucursal.DisplayMember = "id_sucursal"
        Me.cbo_Sucursal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Sucursal.FormattingEnabled = True
        Me.cbo_Sucursal.Location = New System.Drawing.Point(216, 55)
        Me.cbo_Sucursal.Name = "cbo_Sucursal"
        Me.cbo_Sucursal.Size = New System.Drawing.Size(200, 27)
        Me.cbo_Sucursal.TabIndex = 1
        Me.cbo_Sucursal.ValueMember = "id_sucursal"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(70, 31)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(86, 24)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "Empresa"
        '
        'txt_Empresa
        '
        Me.txt_Empresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa.Location = New System.Drawing.Point(10, 55)
        Me.txt_Empresa.Name = "txt_Empresa"
        Me.txt_Empresa.Size = New System.Drawing.Size(200, 29)
        Me.txt_Empresa.TabIndex = 0
        Me.txt_Empresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_RFC
        '
        Me.txt_RFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_RFC.Location = New System.Drawing.Point(767, 12)
        Me.txt_RFC.Name = "txt_RFC"
        Me.txt_RFC.Size = New System.Drawing.Size(14, 29)
        Me.txt_RFC.TabIndex = 5
        Me.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_RFC.Visible = False
        '
        'txt_Direccion
        '
        Me.txt_Direccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion.Location = New System.Drawing.Point(787, 12)
        Me.txt_Direccion.Name = "txt_Direccion"
        Me.txt_Direccion.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion.TabIndex = 6
        Me.txt_Direccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion.Visible = False
        '
        'txt_Direccion2
        '
        Me.txt_Direccion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Direccion2.Location = New System.Drawing.Point(807, 12)
        Me.txt_Direccion2.Name = "txt_Direccion2"
        Me.txt_Direccion2.Size = New System.Drawing.Size(14, 29)
        Me.txt_Direccion2.TabIndex = 7
        Me.txt_Direccion2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Direccion2.Visible = False
        '
        'txt_TipoCliente
        '
        Me.txt_TipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TipoCliente.Location = New System.Drawing.Point(827, 12)
        Me.txt_TipoCliente.Name = "txt_TipoCliente"
        Me.txt_TipoCliente.Size = New System.Drawing.Size(14, 29)
        Me.txt_TipoCliente.TabIndex = 8
        Me.txt_TipoCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_TipoCliente.Visible = False
        '
        'txt_FormatoTicket
        '
        Me.txt_FormatoTicket.Enabled = False
        Me.txt_FormatoTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FormatoTicket.Location = New System.Drawing.Point(535, 1)
        Me.txt_FormatoTicket.Name = "txt_FormatoTicket"
        Me.txt_FormatoTicket.Size = New System.Drawing.Size(120, 29)
        Me.txt_FormatoTicket.TabIndex = 22
        Me.txt_FormatoTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_MargenesTicket
        '
        Me.txt_MargenesTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_MargenesTicket.Location = New System.Drawing.Point(847, 12)
        Me.txt_MargenesTicket.Name = "txt_MargenesTicket"
        Me.txt_MargenesTicket.Size = New System.Drawing.Size(14, 29)
        Me.txt_MargenesTicket.TabIndex = 23
        Me.txt_MargenesTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_MargenesTicket.Visible = False
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Controls.Add(Me.txt_FacFolio)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txt_FacSerie)
        Me.GroupControl1.Controls.Add(Me.chk_Factura)
        Me.GroupControl1.Controls.Add(Me.txt_Devolucion)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.txt_Total)
        Me.GroupControl1.Controls.Add(Me.Label27)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Right
        Me.GroupControl1.Location = New System.Drawing.Point(783, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(305, 342)
        Me.GroupControl1.TabIndex = 24
        Me.GroupControl1.Text = "VENTA"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(204, 214)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 24)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Folio"
        Me.Label3.Visible = False
        '
        'txt_FacFolio
        '
        Me.txt_FacFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FacFolio.Location = New System.Drawing.Point(161, 241)
        Me.txt_FacFolio.Name = "txt_FacFolio"
        Me.txt_FacFolio.ReadOnly = True
        Me.txt_FacFolio.Size = New System.Drawing.Size(139, 29)
        Me.txt_FacFolio.TabIndex = 24
        Me.txt_FacFolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_FacFolio.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(47, 214)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 24)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Serie"
        Me.Label2.Visible = False
        '
        'txt_FacSerie
        '
        Me.txt_FacSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FacSerie.Location = New System.Drawing.Point(4, 241)
        Me.txt_FacSerie.Name = "txt_FacSerie"
        Me.txt_FacSerie.ReadOnly = True
        Me.txt_FacSerie.Size = New System.Drawing.Size(139, 29)
        Me.txt_FacSerie.TabIndex = 22
        Me.txt_FacSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_FacSerie.Visible = False
        '
        'chk_Factura
        '
        Me.chk_Factura.AutoSize = True
        Me.chk_Factura.Location = New System.Drawing.Point(225, 194)
        Me.chk_Factura.Name = "chk_Factura"
        Me.chk_Factura.Size = New System.Drawing.Size(75, 17)
        Me.chk_Factura.TabIndex = 21
        Me.chk_Factura.Text = "Facturado"
        Me.chk_Factura.UseVisualStyleBackColor = True
        Me.chk_Factura.Visible = False
        '
        'txt_Devolucion
        '
        Me.txt_Devolucion.Dock = System.Windows.Forms.DockStyle.Top
        Me.txt_Devolucion.EditValue = ""
        Me.txt_Devolucion.Location = New System.Drawing.Point(2, 137)
        Me.txt_Devolucion.Name = "txt_Devolucion"
        Me.txt_Devolucion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Devolucion.Properties.Appearance.Options.UseFont = True
        Me.txt_Devolucion.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Devolucion.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Red
        Me.txt_Devolucion.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Devolucion.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Devolucion.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Devolucion.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Devolucion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Devolucion.Properties.ReadOnly = True
        Me.txt_Devolucion.Size = New System.Drawing.Size(301, 54)
        Me.txt_Devolucion.TabIndex = 20
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(2, 107)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(301, 30)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "DEVOLUCION"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_Total
        '
        Me.txt_Total.Dock = System.Windows.Forms.DockStyle.Top
        Me.txt_Total.EditValue = ""
        Me.txt_Total.Location = New System.Drawing.Point(2, 53)
        Me.txt_Total.Name = "txt_Total"
        Me.txt_Total.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Total.Properties.Appearance.Options.UseFont = True
        Me.txt_Total.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Black
        Me.txt_Total.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.White
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Total.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Total.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Total.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.txt_Total.Properties.ReadOnly = True
        Me.txt_Total.Size = New System.Drawing.Size(301, 54)
        Me.txt_Total.TabIndex = 18
        '
        'Label27
        '
        Me.Label27.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label27.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(2, 23)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(301, 30)
        Me.Label27.TabIndex = 17
        Me.Label27.Text = "TOTAL"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(783, 342)
        Me.GroupControl2.TabIndex = 24
        Me.GroupControl2.Text = "Detalle de Compra"
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(779, 317)
        Me.GridC_1.TabIndex = 9
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col1_Renglon, Me.col1_Articulo, Me.col1_NomCorto, Me.col1_Nombre, Me.col1_PrecioAct, Me.col1_PrecioNeto, Me.col1_Descuento, Me.col1_CantidadOri, Me.col1_Cantidad, Me.col1_UniVta, Me.col1_Sep, Me.col1_Importe, Me.col1_DEOtorgago, Me.col1_Promocion, Me.col1_Division, Me.col1_Depto, Me.col1_Familia, Me.col1_Tipo, Me.col1_Precio5})
        Me.GridV_1.DetailHeight = 284
        GridFormatRule1.ApplyToRow = True
        GridFormatRule1.Column = Me.col1_Tipo
        GridFormatRule1.Name = "Format0"
        FormatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.LimeGreen
        FormatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Black
        FormatConditionRuleValue1.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue1.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue1.Value1 = "DESPUNTOS"
        GridFormatRule1.Rule = FormatConditionRuleValue1
        GridFormatRule2.ApplyToRow = True
        GridFormatRule2.Column = Me.col1_Promocion
        GridFormatRule2.Name = "Format1"
        FormatConditionRuleValue2.Appearance.BackColor = System.Drawing.Color.LightSkyBlue
        FormatConditionRuleValue2.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.NotEqual
        FormatConditionRuleValue2.Value1 = ""
        GridFormatRule2.Rule = FormatConditionRuleValue2
        GridFormatRule3.Column = Me.col1_Tipo
        GridFormatRule3.ColumnApplyTo = Me.col1_Importe
        GridFormatRule3.Name = "Format2"
        FormatConditionRuleValue3.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue3.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue3.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue3.Value1 = "DESPAGO"
        GridFormatRule3.Rule = FormatConditionRuleValue3
        GridFormatRule4.Column = Me.col1_Tipo
        GridFormatRule4.ColumnApplyTo = Me.col1_Cantidad
        GridFormatRule4.Name = "Format3"
        FormatConditionRuleValue4.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue4.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue4.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue4.Value1 = "DESPAGO"
        GridFormatRule4.Rule = FormatConditionRuleValue4
        GridFormatRule5.Column = Me.col1_Tipo
        GridFormatRule5.ColumnApplyTo = Me.col1_Importe
        GridFormatRule5.Name = "Format4"
        FormatConditionRuleValue5.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue5.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue5.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue5.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue5.Value1 = "DESPAGO2"
        GridFormatRule5.Rule = FormatConditionRuleValue5
        GridFormatRule6.Column = Me.col1_Tipo
        GridFormatRule6.ColumnApplyTo = Me.col1_Cantidad
        GridFormatRule6.Name = "Format5"
        FormatConditionRuleValue6.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue6.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue6.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue6.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue6.Value1 = "DESPAGO2"
        GridFormatRule6.Rule = FormatConditionRuleValue6
        GridFormatRule7.Column = Me.col1_Tipo
        GridFormatRule7.ColumnApplyTo = Me.col1_Importe
        GridFormatRule7.Name = "Format6"
        FormatConditionRuleValue7.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue7.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue7.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue7.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue7.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue7.Value1 = "DESCAMBIO"
        GridFormatRule7.Rule = FormatConditionRuleValue7
        GridFormatRule8.Column = Me.col1_Tipo
        GridFormatRule8.ColumnApplyTo = Me.col1_Cantidad
        GridFormatRule8.Name = "Format7"
        FormatConditionRuleValue8.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue8.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue8.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue8.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue8.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue8.Value1 = "DESCAMBIO"
        GridFormatRule8.Rule = FormatConditionRuleValue8
        GridFormatRule9.Column = Me.col1_Tipo
        GridFormatRule9.ColumnApplyTo = Me.col1_Descuento
        GridFormatRule9.Name = "Format8"
        FormatConditionRuleValue9.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue9.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue9.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue9.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue9.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue9.Value1 = "DESPAGO"
        GridFormatRule9.Rule = FormatConditionRuleValue9
        GridFormatRule10.Column = Me.col1_Tipo
        GridFormatRule10.ColumnApplyTo = Me.col1_Descuento
        GridFormatRule10.Name = "Format9"
        FormatConditionRuleValue10.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue10.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue10.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue10.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue10.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue10.Value1 = "DESPAGO2"
        GridFormatRule10.Rule = FormatConditionRuleValue10
        GridFormatRule11.Column = Me.col1_Tipo
        GridFormatRule11.ColumnApplyTo = Me.col1_Descuento
        GridFormatRule11.Name = "Format10"
        FormatConditionRuleValue11.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue11.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue11.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue11.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue11.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue11.Value1 = "DESCAMBIO"
        GridFormatRule11.Rule = FormatConditionRuleValue11
        GridFormatRule12.Column = Me.col1_Tipo
        GridFormatRule12.ColumnApplyTo = Me.col1_Renglon
        GridFormatRule12.Name = "Format11"
        FormatConditionRuleValue12.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue12.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue12.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue12.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue12.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue12.Value1 = "DESPAGO"
        GridFormatRule12.Rule = FormatConditionRuleValue12
        GridFormatRule13.Column = Me.col1_Tipo
        GridFormatRule13.ColumnApplyTo = Me.col1_Renglon
        GridFormatRule13.Name = "Format12"
        FormatConditionRuleValue13.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue13.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue13.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue13.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue13.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue13.Value1 = "DESPAGO2"
        GridFormatRule13.Rule = FormatConditionRuleValue13
        GridFormatRule14.Column = Me.col1_Tipo
        GridFormatRule14.ColumnApplyTo = Me.col1_Renglon
        GridFormatRule14.Name = "Format13"
        FormatConditionRuleValue14.Appearance.BackColor = System.Drawing.Color.White
        FormatConditionRuleValue14.Appearance.ForeColor = System.Drawing.Color.White
        FormatConditionRuleValue14.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue14.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue14.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue14.Value1 = "DESCAMBIO"
        GridFormatRule14.Rule = FormatConditionRuleValue14
        Me.GridV_1.FormatRules.Add(GridFormatRule1)
        Me.GridV_1.FormatRules.Add(GridFormatRule2)
        Me.GridV_1.FormatRules.Add(GridFormatRule3)
        Me.GridV_1.FormatRules.Add(GridFormatRule4)
        Me.GridV_1.FormatRules.Add(GridFormatRule5)
        Me.GridV_1.FormatRules.Add(GridFormatRule6)
        Me.GridV_1.FormatRules.Add(GridFormatRule7)
        Me.GridV_1.FormatRules.Add(GridFormatRule8)
        Me.GridV_1.FormatRules.Add(GridFormatRule9)
        Me.GridV_1.FormatRules.Add(GridFormatRule10)
        Me.GridV_1.FormatRules.Add(GridFormatRule11)
        Me.GridV_1.FormatRules.Add(GridFormatRule12)
        Me.GridV_1.FormatRules.Add(GridFormatRule13)
        Me.GridV_1.FormatRules.Add(GridFormatRule14)
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsFind.AllowFindPanel = False
        Me.GridV_1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridV_1.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.GridV_1.OptionsSelection.EnableAppearanceHideSelection = False
        Me.GridV_1.OptionsSelection.EnableAppearanceHotTrackedRow = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridV_1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col1_Articulo
        '
        Me.col1_Articulo.Caption = "Articulo"
        Me.col1_Articulo.FieldName = "articulo"
        Me.col1_Articulo.Name = "col1_Articulo"
        Me.col1_Articulo.OptionsColumn.AllowEdit = False
        Me.col1_Articulo.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Articulo.Visible = True
        Me.col1_Articulo.VisibleIndex = 1
        '
        'col1_NomCorto
        '
        Me.col1_NomCorto.Caption = "NomCorot"
        Me.col1_NomCorto.FieldName = "nomcorto"
        Me.col1_NomCorto.Name = "col1_NomCorto"
        Me.col1_NomCorto.OptionsColumn.AllowEdit = False
        Me.col1_NomCorto.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Nombre
        '
        Me.col1_Nombre.Caption = "Nombre"
        Me.col1_Nombre.FieldName = "nombre"
        Me.col1_Nombre.MaxWidth = 300
        Me.col1_Nombre.Name = "col1_Nombre"
        Me.col1_Nombre.OptionsColumn.AllowEdit = False
        Me.col1_Nombre.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Nombre.Visible = True
        Me.col1_Nombre.VisibleIndex = 2
        '
        'col1_PrecioAct
        '
        Me.col1_PrecioAct.Caption = "Actual"
        Me.col1_PrecioAct.DisplayFormat.FormatString = "C2"
        Me.col1_PrecioAct.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_PrecioAct.FieldName = "precioventa"
        Me.col1_PrecioAct.Name = "col1_PrecioAct"
        Me.col1_PrecioAct.OptionsColumn.AllowEdit = False
        Me.col1_PrecioAct.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_PrecioAct.Visible = True
        Me.col1_PrecioAct.VisibleIndex = 3
        '
        'col1_PrecioNeto
        '
        Me.col1_PrecioNeto.Caption = "Precio"
        Me.col1_PrecioNeto.DisplayFormat.FormatString = "C2"
        Me.col1_PrecioNeto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_PrecioNeto.FieldName = "preciofinal"
        Me.col1_PrecioNeto.Name = "col1_PrecioNeto"
        Me.col1_PrecioNeto.OptionsColumn.AllowEdit = False
        Me.col1_PrecioNeto.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_PrecioNeto.Visible = True
        Me.col1_PrecioNeto.VisibleIndex = 4
        '
        'col1_CantidadOri
        '
        Me.col1_CantidadOri.Caption = "Cant. Ori."
        Me.col1_CantidadOri.FieldName = "impcanc"
        Me.col1_CantidadOri.Name = "col1_CantidadOri"
        Me.col1_CantidadOri.OptionsColumn.AllowEdit = False
        Me.col1_CantidadOri.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_CantidadOri.Visible = True
        Me.col1_CantidadOri.VisibleIndex = 6
        '
        'col1_UniVta
        '
        Me.col1_UniVta.Caption = "Uni. Vta."
        Me.col1_UniVta.FieldName = "unidadvta"
        Me.col1_UniVta.Name = "col1_UniVta"
        Me.col1_UniVta.OptionsColumn.AllowEdit = False
        Me.col1_UniVta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_UniVta.Visible = True
        Me.col1_UniVta.VisibleIndex = 8
        '
        'col1_Sep
        '
        Me.col1_Sep.Name = "col1_Sep"
        Me.col1_Sep.OptionsColumn.AllowEdit = False
        Me.col1_Sep.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Sep.Visible = True
        Me.col1_Sep.VisibleIndex = 9
        Me.col1_Sep.Width = 25
        '
        'col1_DEOtorgago
        '
        Me.col1_DEOtorgago.Caption = "D.E. Otorgado"
        Me.col1_DEOtorgago.DisplayFormat.FormatString = "C2"
        Me.col1_DEOtorgago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.col1_DEOtorgago.FieldName = "de_otorgago"
        Me.col1_DEOtorgago.Name = "col1_DEOtorgago"
        '
        'col1_Division
        '
        Me.col1_Division.Caption = "Division"
        Me.col1_Division.FieldName = "division"
        Me.col1_Division.Name = "col1_Division"
        Me.col1_Division.OptionsColumn.AllowEdit = False
        Me.col1_Division.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Depto
        '
        Me.col1_Depto.Caption = "Depto"
        Me.col1_Depto.FieldName = "depto"
        Me.col1_Depto.Name = "col1_Depto"
        Me.col1_Depto.OptionsColumn.AllowEdit = False
        Me.col1_Depto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Familia
        '
        Me.col1_Familia.Caption = "Familia"
        Me.col1_Familia.FieldName = "familia"
        Me.col1_Familia.Name = "col1_Familia"
        Me.col1_Familia.OptionsColumn.AllowEdit = False
        Me.col1_Familia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'col1_Precio5
        '
        Me.col1_Precio5.Caption = "Precio5"
        Me.col1_Precio5.FieldName = "precio5"
        Me.col1_Precio5.Name = "col1_Precio5"
        Me.col1_Precio5.OptionsColumn.AllowEdit = False
        Me.col1_Precio5.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.PanelFiltros_Acciones)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl3.Location = New System.Drawing.Point(0, 445)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(1088, 75)
        Me.GroupControl3.TabIndex = 25
        '
        'PanelFiltros_Acciones
        '
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(130, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Normal.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.FontSizeDelta = -1
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseBackColor = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseFont = True
        Me.PanelFiltros_Acciones.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.PanelFiltros_Acciones.BackColor = System.Drawing.Color.Gray
        WindowsUIButtonImageOptions1.Image = CType(resources.GetObject("WindowsUIButtonImageOptions1.Image"), System.Drawing.Image)
        WindowsUIButtonImageOptions1.ImageUri.Uri = "Apply"
        WindowsUIButtonImageOptions2.ImageUri.Uri = "Close"
        Me.PanelFiltros_Acciones.Buttons.AddRange(New DevExpress.XtraEditors.ButtonPanel.IBaseButton() {New DevExpress.XtraBars.Docking2010.WindowsUIButton("Terminar", True, WindowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Terminar", -1, False), New DevExpress.XtraBars.Docking2010.WindowsUIButton("Limpiar", True, WindowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, True, Nothing, True, False, True, "Limpiar", -1, False)})
        Me.PanelFiltros_Acciones.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Empresa1)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_RFC)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label34)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Direccion2)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Corte)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_TipoCliente)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label33)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_MargenesTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label4)
        Me.PanelFiltros_Acciones.Controls.Add(Me.lstImpresoras)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_FormatoTicket)
        Me.PanelFiltros_Acciones.Controls.Add(Me.Label35)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Cajero)
        Me.PanelFiltros_Acciones.Controls.Add(Me.txt_Caja)
        Me.PanelFiltros_Acciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFiltros_Acciones.EnableImageTransparency = True
        Me.PanelFiltros_Acciones.ForeColor = System.Drawing.Color.White
        Me.PanelFiltros_Acciones.Location = New System.Drawing.Point(2, 8)
        Me.PanelFiltros_Acciones.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PanelFiltros_Acciones.MaximumSize = New System.Drawing.Size(0, 65)
        Me.PanelFiltros_Acciones.MinimumSize = New System.Drawing.Size(65, 65)
        Me.PanelFiltros_Acciones.Name = "PanelFiltros_Acciones"
        Me.PanelFiltros_Acciones.Size = New System.Drawing.Size(1084, 65)
        Me.PanelFiltros_Acciones.TabIndex = 24
        Me.PanelFiltros_Acciones.Text = "windowsUIButtonPanel"
        Me.PanelFiltros_Acciones.UseButtonBackgroundImages = False
        '
        'txt_Empresa1
        '
        Me.txt_Empresa1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Empresa1.Location = New System.Drawing.Point(747, 12)
        Me.txt_Empresa1.Name = "txt_Empresa1"
        Me.txt_Empresa1.Size = New System.Drawing.Size(14, 29)
        Me.txt_Empresa1.TabIndex = 25
        Me.txt_Empresa1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt_Empresa1.Visible = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(367, 2)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(54, 23)
        Me.Label34.TabIndex = 24
        Me.Label34.Text = "Corte"
        '
        'txt_Corte
        '
        Me.txt_Corte.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.txt_Corte.Location = New System.Drawing.Point(436, 2)
        Me.txt_Corte.Name = "txt_Corte"
        Me.txt_Corte.Size = New System.Drawing.Size(76, 30)
        Me.txt_Corte.TabIndex = 23
        Me.txt_Corte.Text = "0001"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(207, 1)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(63, 23)
        Me.Label33.TabIndex = 22
        Me.Label33.Text = "Cajero"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(103, 23)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(112, 37)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(350, 21)
        Me.lstImpresoras.TabIndex = 15
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(10, 1)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(53, 23)
        Me.Label35.TabIndex = 5
        Me.Label35.Text = "Caja:"
        '
        'txt_Cajero
        '
        Me.txt_Cajero.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.txt_Cajero.Location = New System.Drawing.Point(276, 1)
        Me.txt_Cajero.Name = "txt_Cajero"
        Me.txt_Cajero.Size = New System.Drawing.Size(76, 30)
        Me.txt_Cajero.TabIndex = 2
        Me.txt_Cajero.Text = "0001"
        '
        'txt_Caja
        '
        Me.txt_Caja.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Caja.Location = New System.Drawing.Point(69, 1)
        Me.txt_Caja.Name = "txt_Caja"
        Me.txt_Caja.Size = New System.Drawing.Size(130, 30)
        Me.txt_Caja.TabIndex = 1
        Me.txt_Caja.Text = "CAJA 0001"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupControl2)
        Me.Panel1.Controls.Add(Me.GroupControl1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 103)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1088, 342)
        Me.Panel1.TabIndex = 26
        '
        'pdvTicketCancelacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1088, 520)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.gcDatosTicket)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "pdvTicketCancelacion"
        Me.Text = "Ticket"
        CType(Me.gcDatosTicket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatosTicket.ResumeLayout(False)
        Me.gcDatosTicket.PerformLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txt_Devolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.PanelFiltros_Acciones.ResumeLayout(False)
        Me.PanelFiltros_Acciones.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gcDatosTicket As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Buscar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Autonumsuc As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents cbo_Sucursal As ComboBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txt_Empresa As TextBox
    Private WithEvents txt_RFC As TextBox
    Private WithEvents txt_Direccion As TextBox
    Private WithEvents txt_Direccion2 As TextBox
    Private WithEvents txt_TipoCliente As TextBox
    Private WithEvents txt_FormatoTicket As TextBox
    Private WithEvents txt_MargenesTicket As TextBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1_Renglon As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Articulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_NomCorto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Nombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_PrecioAct As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_PrecioNeto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Descuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Cantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_UniVta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Sep As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Importe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_DEOtorgago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Promocion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Division As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Depto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Familia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Tipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Precio5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Panel1 As Panel
    Friend WithEvents txt_Total As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label27 As Label
    Friend WithEvents txt_Devolucion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_FacFolio As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_FacSerie As TextBox
    Friend WithEvents chk_Factura As CheckBox
    Private WithEvents PanelFiltros_Acciones As DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel
    Friend WithEvents Label34 As Label
    Friend WithEvents txt_Corte As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Friend WithEvents Label35 As Label
    Friend WithEvents txt_Cajero As TextBox
    Friend WithEvents txt_Caja As TextBox
    Friend WithEvents col1_CantidadOri As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txt_AS_Cancela As TextBox
    Friend WithEvents Label5 As Label
    Private WithEvents txt_Empresa1 As TextBox
End Class
