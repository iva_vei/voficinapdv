﻿Imports System.Drawing.Printing

Public Class xtraRepTicketCortoVentaDet
    Public SwReimpresion As Boolean = False
    Public TipoCliente As String
    Public MargenDer As Integer = 0
    Public MargenIzq As Integer = 0
    Public MargenArr As Integer = 0
    Public MargenAbj As Integer = 0

    Private Sub xtraRepTicketVenta_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.BeforePrint
        Me.Margins.Right = Me.Margins.Right + MargenDer
        Me.Margins.Left = Me.Margins.Left + MargenIzq
        Me.Margins.Top = Me.Margins.Top + MargenArr
        Me.Margins.Bottom = Me.Margins.Bottom + MargenAbj
        If SwReimpresion Then
            Me.Watermark.Text = "REIMPRESION"
            Me.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal
            ''.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New Bitmap(Application.StartupPath & "\Imagenes\Reimpresion.png"))
        End If
    End Sub

    Private Sub XrLabel30_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles XrLabel30.BeforePrint
        Try
            XrLabel31.Visible = False
            If CDbl(XrLabel30.Text) <> 0 Then
                XrLabel31.Visible = True
                XrLabel30.Visible = True
            Else
                XrLabel31.Visible = False
                XrLabel30.Visible = False
                XrLabel31.Text = ""
            End If
        Catch ex As Exception
            XrLabel31.Visible = True
            XrLabel30.Visible = True
        End Try
    End Sub

    Private Sub lbl_Descto_BeforePrint(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles lbl_Descto.BeforePrint
        lbl_Descto.Visible = False
        Try
            If CDbl(lbl_Descto.Text) <> 0 Then
                XrLabel32.Visible = True
                XrLabel33.Visible = True
                XrLabel33.Text = lbl_Descto.Text
            Else
                XrLabel32.Visible = False
                XrLabel33.Visible = False
            End If
        Catch ex As Exception
            XrLabel32.Visible = True
            XrLabel33.Visible = True
            XrLabel33.Text = lbl_Descto.Text
        End Try
    End Sub
End Class