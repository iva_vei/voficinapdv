﻿Imports DevExpress.XtraGrid.Views.Base

Public Class pdvAplicaDescuentos
    Dim _Accion As Boolean
    Dim _Autonumsuc As String
    Dim _Renglon As Integer
    Dim _PrecioDirecto As Boolean

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public Property Autonumsuc As String
        Get
            Return _Autonumsuc
        End Get
        Set(value As String)
            _Autonumsuc = value
        End Set
    End Property

    Public Property Renglon As Integer
        Get
            Return _Renglon
        End Get
        Set(value As Integer)
            _Renglon = value
        End Set
    End Property

    Public Property PrecioDirecto As Boolean
        Get
            Return _PrecioDirecto
        End Get
        Set(value As Boolean)
            _PrecioDirecto = value
        End Set
    End Property

    Private Sub pdvAplicaDescuentos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter
                    btn_Aceptar.Focus()
                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub
    Private Sub btn_Aceptar_Click_1(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDetalle As DataTable = Nothing
        Dim Descto_Global As Double = 0.0
        Dim Descto As Double = 0.0
        Dim PrecioFinal As Double = 0.0
        Try
            _Accion = False
            Renglon = Integer.Parse(txt_Renglon.Text, System.Globalization.NumberStyles.Any)
            PrecioFinal = Double.Parse(txt_PrecioFinal.Text.Replace("$", "").Replace(",", ""), System.Globalization.NumberStyles.Any)
            Descto_Global = Double.Parse(txt_DesctoGlobal.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.Any)
            Descto = Double.Parse(txt_Descto.Text.Replace("$", "").Replace(",", ""), Globalization.NumberStyles.Any)

            Descto_Global = Descto_Global / 100.0
            Descto = Descto / 100.0

            ''Descto = Math.Round(Descto - Descto_Global, 4)

            If oDatos.PVTA_Inserta_DetVentas_Descuentos(Globales.oAmbientes.Id_Empresa _
                                        , Globales.oAmbientes.Id_Sucursal _
                                        , Autonumsuc _
                                        , PrecioDirecto _
                                        , Descto_Global _
                                        , Renglon _
                                        , IIf(PrecioDirecto, 0, Descto) _
                                        , IIf(PrecioDirecto, PrecioFinal, 0) _
                                        , Msj) Then
                If Msj.ToUpper <> "EXITO" Then
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                _Accion = True
                Me.Close()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click_1(sender As Object, e As EventArgs) Handles btn_Cancelar.Click
        Try
            _Accion = False
        Catch ex As Exception
            _Accion = False
        End Try
    End Sub
    Private Sub txt_Descto_EditValueChanged(sender As Object, e As EventArgs) Handles txt_Descto.EditValueChanged
        Dim PrecioActual As Double = 0
        Dim PrecioFinal As Double = 0
        Dim Precio5 As Double = 0
        Dim Descto As Double = 0.0
        Try
            PrecioFinal = Double.Parse(txt_PrecioFinal.Text.Replace("$", "").Replace(",", ""), System.Globalization.NumberStyles.Any)
            Descto = Double.Parse(txt_Descto.Text.Replace("$", "").Replace(",", ""), System.Globalization.NumberStyles.Any)
            PrecioActual = Double.Parse(txt_PrecioActual.Text.Replace("$", "").Replace(",", ""), System.Globalization.NumberStyles.Any)

            Descto = Descto / 100.0

            If PrecioActual * (1.0 - Descto) < Precio5 Then
                PrecioFinal = Precio5
            Else
                PrecioFinal = PrecioActual * (1.0 - Descto)
            End If
        Catch ex As Exception
            PrecioFinal = PrecioActual
        Finally
            If Not _PrecioDirecto Then
                txt_PrecioFinal.Text = Format(PrecioFinal, "C2")
            End If
        End Try
    End Sub
    Private Sub pdvAplicaDescuentos_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Msj As String = ""
        Try
            If Not GC_Partida.Visible Then
                Me.Height = Me.Height - GC_Partida.Height
            End If
            If PrecioDirecto Then
                txt_PrecioFinal.Properties.ReadOnly = False
                txt_Descto.Properties.ReadOnly = True
                txt_PrecioFinal.Focus()
            Else
                txt_PrecioFinal.Properties.ReadOnly = True
                txt_Descto.Properties.ReadOnly = False
                txt_Descto.Focus()
            End If
            oDatos = New Datos_Viscoi
            If oDatos.PVTA_Recupera_ParametrosControl(Globales.oAmbientes.Id_Empresa _
                                                         , "" _
                                                         , "VOFICINA" _
                                                         , "" _
                                                         , "DESCTO_PERMITIR" _
                                                         , dtDatos _
                                                         , Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    If dtDatos.Rows(0).Item("valor") = "SI" Then
                    Else
                        MessageBox.Show("El punto de venta no esta configurado para permitir Descuentos manuales.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Me.Close()
                    End If
                Else
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.Close()
                End If
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub txt_PrecioFinal_LostFocus(sender As Object, e As EventArgs) Handles txt_PrecioFinal.LostFocus
        Dim PrecioActual As Double = 0
        Dim PrecioFinal As Double = 0
        Dim Precio5 As Double = 0
        Dim Descto As Double = 0.0
        Try
            PrecioFinal = Double.Parse(txt_PrecioFinal.Text.Replace("$", "").Replace(",", ""), System.Globalization.NumberStyles.Any)
            Descto = Double.Parse(txt_Descto.Text.Replace("$", "").Replace(",", ""), System.Globalization.NumberStyles.Any)
            PrecioActual = Double.Parse(txt_PrecioActual.Text.Replace("$", "").Replace(",", ""), System.Globalization.NumberStyles.Any)

            If PrecioFinal <> 0 Then
                Descto = (PrecioActual - PrecioFinal) / PrecioActual
            Else
                Descto = 0
            End If
            txt_Descto.Text = Descto * 100

        Catch ex As Exception

        End Try
    End Sub
End Class