﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pdvLstPrecios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_publico = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_mayoreo = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.CkBoxTodos = New System.Windows.Forms.CheckBox()
        Me.Txt_RenglonCantidad = New DevExpress.XtraEditors.TextEdit()
        CType(Me.Txt_RenglonCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_publico
        '
        Me.btn_publico.Appearance.Font = New System.Drawing.Font("Tahoma", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_publico.Appearance.Options.UseFont = True
        Me.btn_publico.Location = New System.Drawing.Point(37, 12)
        Me.btn_publico.Name = "btn_publico"
        Me.btn_publico.Size = New System.Drawing.Size(194, 104)
        Me.btn_publico.TabIndex = 0
        Me.btn_publico.Text = "PUBLICO"
        '
        'btn_mayoreo
        '
        Me.btn_mayoreo.Appearance.Font = New System.Drawing.Font("Tahoma", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_mayoreo.Appearance.Options.UseFont = True
        Me.btn_mayoreo.Location = New System.Drawing.Point(291, 12)
        Me.btn_mayoreo.Name = "btn_mayoreo"
        Me.btn_mayoreo.Size = New System.Drawing.Size(187, 104)
        Me.btn_mayoreo.TabIndex = 1
        Me.btn_mayoreo.Text = "MAYOREO"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl1.Location = New System.Drawing.Point(37, 169)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(194, 39)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "RENGLON"
        '
        'CkBoxTodos
        '
        Me.CkBoxTodos.AutoSize = True
        Me.CkBoxTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBoxTodos.Location = New System.Drawing.Point(333, 230)
        Me.CkBoxTodos.Name = "CkBoxTodos"
        Me.CkBoxTodos.Size = New System.Drawing.Size(99, 29)
        Me.CkBoxTodos.TabIndex = 4
        Me.CkBoxTodos.Text = "Todos"
        Me.CkBoxTodos.UseVisualStyleBackColor = True
        '
        'Txt_RenglonCantidad
        '
        Me.Txt_RenglonCantidad.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.Txt_RenglonCantidad.Location = New System.Drawing.Point(291, 162)
        Me.Txt_RenglonCantidad.Name = "Txt_RenglonCantidad"
        Me.Txt_RenglonCantidad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 16.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_RenglonCantidad.Properties.Appearance.Options.UseFont = True
        Me.Txt_RenglonCantidad.Size = New System.Drawing.Size(187, 46)
        Me.Txt_RenglonCantidad.TabIndex = 5
        '
        'pdvLstPrecios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(506, 280)
        Me.Controls.Add(Me.Txt_RenglonCantidad)
        Me.Controls.Add(Me.CkBoxTodos)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.btn_mayoreo)
        Me.Controls.Add(Me.btn_publico)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(528, 336)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(528, 336)
        Me.Name = "pdvLstPrecios"
        Me.RightToLeftLayout = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lista Precios"
        CType(Me.Txt_RenglonCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_publico As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_mayoreo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CkBoxTodos As CheckBox
    Friend WithEvents Txt_RenglonCantidad As DevExpress.XtraEditors.TextEdit
End Class
