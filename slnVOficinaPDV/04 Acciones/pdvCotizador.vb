﻿Public Class pdvCotizador
    Dim _Accion As Boolean
    Dim _TipoTran As eTipoTran
    Dim _Autonumsuc As String
    Dim _Cliente As String
    Dim _Articulo As String

    Public Property Articulo As String
        Get
            Return _Articulo
        End Get
        Set(value As String)
            _Articulo = value
        End Set
    End Property
    Public Property Autonumsuc As String
        Get
            Return _Autonumsuc
        End Get
        Set(value As String)
            _Autonumsuc = value
        End Set
    End Property

    Public Property Accion As Boolean
        Get
            Return _Accion
        End Get
        Set(value As Boolean)
            _Accion = value
        End Set
    End Property

    Public Property TipoTran As eTipoTran
        Get
            Return _TipoTran
        End Get
        Set(value As eTipoTran)
            _TipoTran = value
        End Set
    End Property

    Public Property Cliente As String
        Get
            Return _Cliente
        End Get
        Set(value As String)
            _Cliente = value
        End Set
    End Property

    Private Sub pvdFormaPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Call Refrescar()

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Refrescar()
        Dim oDatos As Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim Mensaje As String = ""
        Dim sTipoTran As String = ""

        Try
            oDatos = New Datos_Viscoi

            If oDatos.PVTA_Cotizaciones(Globales.oAmbientes.Id_Empresa, Globales.oAmbientes.Id_Sucursal, Articulo, sTipoTran, Cliente, dtDatos, Mensaje) Then
                GridC_1.DataSource = dtDatos
                GridC_1.RefreshDataSource()
                GridV_1.BestFitColumns()
            Else
                GridC_1.DataSource = Nothing
                GridC_1.RefreshDataSource()
            End If

        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        oDatos = Nothing
    End Sub
    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs)
        Dim sw_continuar As Boolean = True
        Dim Requeridos As Double = 0
        Dim Disponible As Double = 0

        Try
            _Accion = True
            If GridV_1.RowCount > 0 Then
                _Autonumsuc = GridV_1.GetFocusedRowCellValue(col1_Articulo)
                '_Estatus = GridV_1.GetFocusedRowCellValue(col1_Estatus)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Cancelar_Click(sender As Object, e As EventArgs)
        _Accion = False
        Me.Close()
    End Sub

    Private Sub frmPrincipal_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            Select Case e.KeyCode
                'Case System.Windows.Forms.Keys.Escape
                '    Regresar(Me.ActiveMdiChild)
                Case System.Windows.Forms.Keys.Enter

                Case System.Windows.Forms.Keys.Escape
                    Me.Close()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & " " & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Refrescar_Click(sender As Object, e As EventArgs)
        Call Refrescar()
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Call Refrescar()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GridC_1_Click(sender As Object, e As EventArgs) Handles GridC_1.Click
        'Descto = Double.Parse(GridV_1.GetFocusedRowCellValue().ToString.Replace("$", "").Replace(",", "").Replace("%", ""), System.Globalization.NumberStyles.Any)
        Dim Habilitado As String
        Habilitado = GridV_1.GetFocusedRowCellValue(col1_Habilitado).ToString()

        If (Habilitado = "1") Then
            lblmensaje.Visible = False
        Else
            lblmensaje.Visible = True
        End If

    End Sub

    Private Sub brn_Cancelar_Click(sender As Object, e As EventArgs) Handles brn_Cancelar.Click
        _Accion = False
        Me.Close()
    End Sub

    Private Sub GridC_1_DoubleClick(sender As Object, e As EventArgs) Handles GridC_1.DoubleClick
        Dim Habilitado As String
        Dim Msj As String = ""
        Dim oDatos As New Datos_Viscoi
        Dim dtDatos As DataTable = Nothing
        Dim dtDetalle As DataTable = Nothing
        Dim Quincenas As Double = 0.0
        Dim Descto As Double = 0.0
        Dim PrecioFinal As Double = 0.0

        Habilitado = GridV_1.GetFocusedRowCellValue(col1_Habilitado).ToString()
        PrecioFinal = GridV_1.GetFocusedRowCellValue(col1_Precio_Final).ToString()
        Quincenas = GridV_1.GetFocusedRowCellValue(col1_Quincenas).ToString()

        If (Habilitado = "1") Then

            If oDatos.PVTA_Inserta_DetVentas_Cotizacion(Globales.oAmbientes.Id_Empresa _
                                       , Globales.oAmbientes.Id_Sucursal _
                                       , Autonumsuc _
                                       , Quincenas _
                                       , PrecioFinal _
                                       , Articulo _
                                       , Msj) Then
                If Msj.ToUpper <> "EXITO" Then
                    MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                _Accion = True
                Me.Close()
            Else
                MessageBox.Show(Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Else
                MessageBox.Show("El plazo seleccionado esta deshabilitado por la distribuidora, favor de contactarla para habilitarlo", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class