﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvVendedores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1_IdVendedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col1_Vendedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Txt_IdVendedor = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Vendedor = New DevExpress.XtraEditors.TextEdit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_IdVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Vendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(242, 26)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(481, 26)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.Txt_Vendedor)
        Me.GroupControl3.Controls.Add(Me.Txt_IdVendedor)
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 389)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(784, 82)
        Me.GroupControl3.TabIndex = 2
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(784, 389)
        Me.GroupControl2.TabIndex = 1
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(780, 364)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col1_IdVendedor, Me.col1_Vendedor})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'col1_IdVendedor
        '
        Me.col1_IdVendedor.Caption = "ID"
        Me.col1_IdVendedor.FieldName = "id_vendedor"
        Me.col1_IdVendedor.Name = "col1_IdVendedor"
        Me.col1_IdVendedor.OptionsColumn.AllowEdit = False
        Me.col1_IdVendedor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdVendedor.Visible = True
        Me.col1_IdVendedor.VisibleIndex = 0
        '
        'col1_Vendedor
        '
        Me.col1_Vendedor.Caption = "Vendedor"
        Me.col1_Vendedor.FieldName = "nombre"
        Me.col1_Vendedor.Name = "col1_Vendedor"
        Me.col1_Vendedor.OptionsColumn.AllowEdit = False
        Me.col1_Vendedor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Vendedor.Visible = True
        Me.col1_Vendedor.VisibleIndex = 1
        '
        'Txt_IdVendedor
        '
        Me.Txt_IdVendedor.Location = New System.Drawing.Point(5, 26)
        Me.Txt_IdVendedor.Name = "Txt_IdVendedor"
        Me.Txt_IdVendedor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_IdVendedor.Properties.Appearance.Options.UseFont = True
        Me.Txt_IdVendedor.Properties.Appearance.Options.UseTextOptions = True
        Me.Txt_IdVendedor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Txt_IdVendedor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_IdVendedor.Size = New System.Drawing.Size(15, 30)
        Me.Txt_IdVendedor.TabIndex = 2
        Me.Txt_IdVendedor.Visible = False
        '
        'Txt_Vendedor
        '
        Me.Txt_Vendedor.Location = New System.Drawing.Point(26, 26)
        Me.Txt_Vendedor.Name = "Txt_Vendedor"
        Me.Txt_Vendedor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Vendedor.Properties.Appearance.Options.UseFont = True
        Me.Txt_Vendedor.Properties.Appearance.Options.UseTextOptions = True
        Me.Txt_Vendedor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Txt_Vendedor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Vendedor.Size = New System.Drawing.Size(15, 30)
        Me.Txt_Vendedor.TabIndex = 3
        Me.Txt_Vendedor.Visible = False
        '
        'pdvVendedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 471)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvVendedores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VENDEDORES"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_IdVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Vendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1_IdVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col1_Vendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Txt_Vendedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_IdVendedor As DevExpress.XtraEditors.TextEdit
End Class
