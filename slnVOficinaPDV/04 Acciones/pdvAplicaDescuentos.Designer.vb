﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvAplicaDescuentos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txt_DesctoGlobal = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Nombre = New DevExpress.XtraEditors.TextEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_Articulo = New DevExpress.XtraEditors.TextEdit()
        Me.txt_PrecioActual = New DevExpress.XtraEditors.TextEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Descto = New DevExpress.XtraEditors.TextEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GC_GLOBAL = New DevExpress.XtraEditors.GroupControl()
        Me.GC_Partida = New DevExpress.XtraEditors.GroupControl()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_PrecioFinal = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Precio5 = New DevExpress.XtraEditors.TextEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_Renglon = New DevExpress.XtraEditors.TextEdit()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        CType(Me.txt_DesctoGlobal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Nombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Articulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PrecioActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Descto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_GLOBAL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_GLOBAL.SuspendLayout()
        CType(Me.GC_Partida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GC_Partida.SuspendLayout()
        CType(Me.txt_PrecioFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Precio5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Renglon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_DesctoGlobal
        '
        Me.txt_DesctoGlobal.Location = New System.Drawing.Point(275, 37)
        Me.txt_DesctoGlobal.Name = "txt_DesctoGlobal"
        Me.txt_DesctoGlobal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt_DesctoGlobal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DesctoGlobal.Properties.Appearance.Options.UseBackColor = True
        Me.txt_DesctoGlobal.Properties.Appearance.Options.UseFont = True
        Me.txt_DesctoGlobal.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_DesctoGlobal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_DesctoGlobal.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_DesctoGlobal.Properties.Mask.EditMask = "N2"
        Me.txt_DesctoGlobal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_DesctoGlobal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_DesctoGlobal.Size = New System.Drawing.Size(144, 46)
        Me.txt_DesctoGlobal.TabIndex = 0
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Location = New System.Drawing.Point(12, 67)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Properties.Appearance.Options.UseFont = True
        Me.txt_Nombre.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Nombre.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Nombre.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Nombre.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Nombre.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Nombre.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Nombre.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Nombre.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Nombre.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Nombre.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Nombre.Properties.ReadOnly = True
        Me.txt_Nombre.Size = New System.Drawing.Size(551, 30)
        Me.txt_Nombre.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(16, 37)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 24)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Articulo"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(10, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(566, 38)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Descuento                      %"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_Articulo
        '
        Me.txt_Articulo.Location = New System.Drawing.Point(95, 34)
        Me.txt_Articulo.Name = "txt_Articulo"
        Me.txt_Articulo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Articulo.Properties.Appearance.Options.UseFont = True
        Me.txt_Articulo.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Articulo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Articulo.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Articulo.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Articulo.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Articulo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Articulo.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Articulo.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Articulo.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Articulo.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Articulo.Properties.ReadOnly = True
        Me.txt_Articulo.Size = New System.Drawing.Size(150, 30)
        Me.txt_Articulo.TabIndex = 0
        '
        'txt_PrecioActual
        '
        Me.txt_PrecioActual.Location = New System.Drawing.Point(301, 106)
        Me.txt_PrecioActual.Name = "txt_PrecioActual"
        Me.txt_PrecioActual.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PrecioActual.Properties.Appearance.Options.UseFont = True
        Me.txt_PrecioActual.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_PrecioActual.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_PrecioActual.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_PrecioActual.Properties.Mask.EditMask = "C2"
        Me.txt_PrecioActual.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_PrecioActual.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_PrecioActual.Properties.ReadOnly = True
        Me.txt_PrecioActual.Size = New System.Drawing.Size(262, 48)
        Me.txt_PrecioActual.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(282, 38)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Precio Actual"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_Descto
        '
        Me.txt_Descto.Location = New System.Drawing.Point(301, 160)
        Me.txt_Descto.Name = "txt_Descto"
        Me.txt_Descto.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt_Descto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Descto.Properties.Appearance.Options.UseBackColor = True
        Me.txt_Descto.Properties.Appearance.Options.UseFont = True
        Me.txt_Descto.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Descto.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_Descto.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Descto.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Descto.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Descto.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Descto.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Descto.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Descto.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Descto.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Descto.Properties.Mask.EditMask = "N2"
        Me.txt_Descto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Descto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Descto.Size = New System.Drawing.Size(213, 46)
        Me.txt_Descto.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(282, 38)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Descuento Partida"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GC_GLOBAL
        '
        Me.GC_GLOBAL.Controls.Add(Me.txt_DesctoGlobal)
        Me.GC_GLOBAL.Controls.Add(Me.Label7)
        Me.GC_GLOBAL.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_GLOBAL.Location = New System.Drawing.Point(0, 0)
        Me.GC_GLOBAL.Name = "GC_GLOBAL"
        Me.GC_GLOBAL.Size = New System.Drawing.Size(568, 100)
        Me.GC_GLOBAL.TabIndex = 0
        Me.GC_GLOBAL.Text = "DESCUENTO GLOBAL"
        '
        'GC_Partida
        '
        Me.GC_Partida.Controls.Add(Me.Label5)
        Me.GC_Partida.Controls.Add(Me.txt_PrecioFinal)
        Me.GC_Partida.Controls.Add(Me.Label4)
        Me.GC_Partida.Controls.Add(Me.txt_Precio5)
        Me.GC_Partida.Controls.Add(Me.Label1)
        Me.GC_Partida.Controls.Add(Me.txt_Renglon)
        Me.GC_Partida.Controls.Add(Me.txt_Descto)
        Me.GC_Partida.Controls.Add(Me.Label8)
        Me.GC_Partida.Controls.Add(Me.Label3)
        Me.GC_Partida.Controls.Add(Me.txt_Nombre)
        Me.GC_Partida.Controls.Add(Me.txt_PrecioActual)
        Me.GC_Partida.Controls.Add(Me.txt_Articulo)
        Me.GC_Partida.Controls.Add(Me.Label2)
        Me.GC_Partida.Dock = System.Windows.Forms.DockStyle.Top
        Me.GC_Partida.Location = New System.Drawing.Point(0, 100)
        Me.GC_Partida.Name = "GC_Partida"
        Me.GC_Partida.Size = New System.Drawing.Size(568, 271)
        Me.GC_Partida.TabIndex = 1
        Me.GC_Partida.Text = "DESCUENTO A PARTIDA"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(520, 165)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 38)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "%"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_PrecioFinal
        '
        Me.txt_PrecioFinal.Location = New System.Drawing.Point(301, 214)
        Me.txt_PrecioFinal.Name = "txt_PrecioFinal"
        Me.txt_PrecioFinal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt_PrecioFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PrecioFinal.Properties.Appearance.Options.UseBackColor = True
        Me.txt_PrecioFinal.Properties.Appearance.Options.UseFont = True
        Me.txt_PrecioFinal.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_PrecioFinal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_PrecioFinal.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_PrecioFinal.Properties.Mask.EditMask = "C2"
        Me.txt_PrecioFinal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_PrecioFinal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_PrecioFinal.Properties.ReadOnly = True
        Me.txt_PrecioFinal.Size = New System.Drawing.Size(262, 48)
        Me.txt_PrecioFinal.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(14, 217)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(282, 38)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Precio Final"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_Precio5
        '
        Me.txt_Precio5.Location = New System.Drawing.Point(251, 34)
        Me.txt_Precio5.Name = "txt_Precio5"
        Me.txt_Precio5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Precio5.Properties.Appearance.Options.UseFont = True
        Me.txt_Precio5.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Precio5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Precio5.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Precio5.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Precio5.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Precio5.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Precio5.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Precio5.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Precio5.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Precio5.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Precio5.Properties.Mask.EditMask = "C2"
        Me.txt_Precio5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Precio5.Properties.ReadOnly = True
        Me.txt_Precio5.Size = New System.Drawing.Size(150, 30)
        Me.txt_Precio5.TabIndex = 1
        Me.txt_Precio5.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(420, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 24)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Renglon"
        '
        'txt_Renglon
        '
        Me.txt_Renglon.Location = New System.Drawing.Point(508, 34)
        Me.txt_Renglon.Name = "txt_Renglon"
        Me.txt_Renglon.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Renglon.Properties.Appearance.Options.UseFont = True
        Me.txt_Renglon.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Renglon.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Renglon.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_Renglon.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Renglon.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.txt_Renglon.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_Renglon.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_Renglon.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_Renglon.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.txt_Renglon.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Renglon.Properties.Mask.EditMask = "C2"
        Me.txt_Renglon.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Renglon.Properties.ReadOnly = True
        Me.txt_Renglon.Size = New System.Drawing.Size(55, 30)
        Me.txt_Renglon.TabIndex = 20
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(472, 37)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(312, 37)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TextEdit1)
        Me.Panel1.Controls.Add(Me.btn_Aceptar)
        Me.Panel1.Controls.Add(Me.btn_Cancelar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 371)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(568, 106)
        Me.Panel1.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(282, 38)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Código Supervisor"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(74, 64)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Appearance.Options.UseTextOptions = True
        Me.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TextEdit1.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.TextEdit1.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.TextEdit1.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.TextEdit1.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.TextEdit1.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.TextEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.TextEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TextEdit1.Properties.Mask.EditMask = "C2"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Size = New System.Drawing.Size(150, 30)
        Me.TextEdit1.TabIndex = 0
        '
        'pdvAplicaDescuentos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(568, 477)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GC_Partida)
        Me.Controls.Add(Me.GC_GLOBAL)
        Me.KeyPreview = True
        Me.Name = "pdvAplicaDescuentos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aplica Descuentos"
        CType(Me.txt_DesctoGlobal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Nombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Articulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PrecioActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Descto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_GLOBAL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_GLOBAL.ResumeLayout(False)
        CType(Me.GC_Partida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GC_Partida.ResumeLayout(False)
        Me.GC_Partida.PerformLayout()
        CType(Me.txt_PrecioFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Precio5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Renglon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_Nombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As Label
    Friend WithEvents txt_DesctoGlobal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Articulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_PrecioActual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_Descto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As Label
    Friend WithEvents GC_GLOBAL As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GC_Partida As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_Renglon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents txt_Precio5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_PrecioFinal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
End Class
