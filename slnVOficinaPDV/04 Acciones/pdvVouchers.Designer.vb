﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pdvVouchers
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridC_1 = New DevExpress.XtraGrid.GridControl()
        Me.GridV_1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridView()
        Me.gridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.col1_Banco = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Hora = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numtarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tittarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Vigtarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Referencia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numcontrol = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Codigoaut = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Importe = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Fum = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdVoucher = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Urlbanorte = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Puerto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_User1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Pswd = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Idmerchant = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Modo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Caja = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdEmpresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Empresa = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Rfc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Direccion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Direccion2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Transaccion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_IdSucursal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Respuesta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Afiliacion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Terminalid = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tiptarjeta = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Bancoemisor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Mesesdiferido = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Numpagos = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Tipoplan = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.col1_Autonumsuc = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbo_Banco = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lstImpresoras = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dat_FechaFin = New System.Windows.Forms.DateTimePicker()
        Me.dat_FechaIni = New System.Windows.Forms.DateTimePicker()
        Me.btn_Refrescar = New System.Windows.Forms.Button()
        Me.lbl_Tarjeta = New System.Windows.Forms.Label()
        Me.lbl_Cuenta = New System.Windows.Forms.Label()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Image = Global.proyVOficina_PDV.My.Resources.Resources.ok
        Me.btn_Aceptar.Location = New System.Drawing.Point(242, 26)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Image = Global.proyVOficina_PDV.My.Resources.Resources.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(481, 26)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Options.UseTextOptions = True
        Me.GroupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GroupControl3.AppearanceCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GroupControl3.Controls.Add(Me.btn_Aceptar)
        Me.GroupControl3.Controls.Add(Me.btn_Cancelar)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 485)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(860, 75)
        Me.GroupControl3.TabIndex = 2
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridC_1)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(860, 389)
        Me.GroupControl2.TabIndex = 1
        '
        'GridC_1
        '
        Me.GridC_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridC_1.Location = New System.Drawing.Point(2, 23)
        Me.GridC_1.MainView = Me.GridV_1
        Me.GridC_1.Name = "GridC_1"
        Me.GridC_1.Size = New System.Drawing.Size(856, 364)
        Me.GridC_1.TabIndex = 0
        Me.GridC_1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridV_1})
        '
        'GridV_1
        '
        Me.GridV_1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.gridBand1})
        Me.GridV_1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.col1_IdVoucher, Me.col1_Urlbanorte, Me.col1_Puerto, Me.col1_User1, Me.col1_Pswd, Me.col1_Idmerchant, Me.col1_Modo, Me.col1_Caja, Me.col1_IdEmpresa, Me.col1_Empresa, Me.col1_Rfc, Me.col1_Direccion, Me.col1_Direccion2, Me.col1_Tipo, Me.col1_Transaccion, Me.col1_IdSucursal, Me.col1_Fecha, Me.col1_Hora, Me.col1_Banco, Me.col1_Respuesta, Me.col1_Afiliacion, Me.col1_Terminalid, Me.col1_Numcontrol, Me.col1_Numtarjeta, Me.col1_Tiptarjeta, Me.col1_Vigtarjeta, Me.col1_Tittarjeta, Me.col1_Bancoemisor, Me.col1_Codigoaut, Me.col1_Referencia, Me.col1_Importe, Me.col1_Mesesdiferido, Me.col1_Numpagos, Me.col1_Tipoplan, Me.col1_Autonumsuc, Me.col1_Fum})
        Me.GridV_1.GridControl = Me.GridC_1
        Me.GridV_1.Name = "GridV_1"
        Me.GridV_1.OptionsView.ColumnAutoWidth = False
        Me.GridV_1.OptionsView.ShowGroupPanel = False
        '
        'gridBand1
        '
        Me.gridBand1.Columns.Add(Me.col1_Banco)
        Me.gridBand1.Columns.Add(Me.col1_Fecha)
        Me.gridBand1.Columns.Add(Me.col1_Hora)
        Me.gridBand1.Columns.Add(Me.col1_Numtarjeta)
        Me.gridBand1.Columns.Add(Me.col1_Tittarjeta)
        Me.gridBand1.Columns.Add(Me.col1_Vigtarjeta)
        Me.gridBand1.Columns.Add(Me.col1_Referencia)
        Me.gridBand1.Columns.Add(Me.col1_Numcontrol)
        Me.gridBand1.Columns.Add(Me.col1_Codigoaut)
        Me.gridBand1.Columns.Add(Me.col1_Importe)
        Me.gridBand1.Columns.Add(Me.col1_Fum)
        Me.gridBand1.Name = "gridBand1"
        Me.gridBand1.VisibleIndex = 0
        Me.gridBand1.Width = 825
        '
        'col1_Banco
        '
        Me.col1_Banco.Caption = "Banco"
        Me.col1_Banco.FieldName = "Banco"
        Me.col1_Banco.Name = "col1_Banco"
        Me.col1_Banco.OptionsColumn.AllowEdit = False
        Me.col1_Banco.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Banco.Visible = True
        '
        'col1_Fecha
        '
        Me.col1_Fecha.Caption = "Fecha"
        Me.col1_Fecha.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.col1_Fecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col1_Fecha.FieldName = "fecha"
        Me.col1_Fecha.Name = "col1_Fecha"
        Me.col1_Fecha.OptionsColumn.AllowEdit = False
        Me.col1_Fecha.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fecha.Visible = True
        '
        'col1_Hora
        '
        Me.col1_Hora.Caption = "Hora"
        Me.col1_Hora.FieldName = "hora"
        Me.col1_Hora.Name = "col1_Hora"
        Me.col1_Hora.OptionsColumn.AllowEdit = False
        Me.col1_Hora.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Hora.Visible = True
        '
        'col1_Numtarjeta
        '
        Me.col1_Numtarjeta.Caption = "Numtarjeta"
        Me.col1_Numtarjeta.FieldName = "NumTarjeta"
        Me.col1_Numtarjeta.Name = "col1_Numtarjeta"
        Me.col1_Numtarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Numtarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numtarjeta.Visible = True
        '
        'col1_Tittarjeta
        '
        Me.col1_Tittarjeta.Caption = "Tittarjeta"
        Me.col1_Tittarjeta.FieldName = "TitTarjeta"
        Me.col1_Tittarjeta.Name = "col1_Tittarjeta"
        Me.col1_Tittarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Tittarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tittarjeta.Visible = True
        '
        'col1_Vigtarjeta
        '
        Me.col1_Vigtarjeta.Caption = "Vigtarjeta"
        Me.col1_Vigtarjeta.FieldName = "VigTarjeta"
        Me.col1_Vigtarjeta.Name = "col1_Vigtarjeta"
        Me.col1_Vigtarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Vigtarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Vigtarjeta.Visible = True
        '
        'col1_Referencia
        '
        Me.col1_Referencia.Caption = "Referencia"
        Me.col1_Referencia.FieldName = "Referencia"
        Me.col1_Referencia.Name = "col1_Referencia"
        Me.col1_Referencia.OptionsColumn.AllowEdit = False
        Me.col1_Referencia.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Referencia.Visible = True
        '
        'col1_Numcontrol
        '
        Me.col1_Numcontrol.Caption = "Numcontrol"
        Me.col1_Numcontrol.FieldName = "NumControl"
        Me.col1_Numcontrol.Name = "col1_Numcontrol"
        Me.col1_Numcontrol.OptionsColumn.AllowEdit = False
        Me.col1_Numcontrol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numcontrol.Visible = True
        '
        'col1_Codigoaut
        '
        Me.col1_Codigoaut.Caption = "Codigoaut"
        Me.col1_Codigoaut.FieldName = "CodigoAut"
        Me.col1_Codigoaut.Name = "col1_Codigoaut"
        Me.col1_Codigoaut.OptionsColumn.AllowEdit = False
        Me.col1_Codigoaut.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Codigoaut.Visible = True
        '
        'col1_Importe
        '
        Me.col1_Importe.Caption = "Importe"
        Me.col1_Importe.FieldName = "Importe"
        Me.col1_Importe.Name = "col1_Importe"
        Me.col1_Importe.OptionsColumn.AllowEdit = False
        Me.col1_Importe.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Importe.Visible = True
        '
        'col1_Fum
        '
        Me.col1_Fum.Caption = "Fum"
        Me.col1_Fum.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm"
        Me.col1_Fum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.col1_Fum.FieldName = "fum"
        Me.col1_Fum.Name = "col1_Fum"
        Me.col1_Fum.OptionsColumn.AllowEdit = False
        Me.col1_Fum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Fum.Visible = True
        '
        'col1_IdVoucher
        '
        Me.col1_IdVoucher.Caption = "IdVoucher"
        Me.col1_IdVoucher.FieldName = "id_voucher"
        Me.col1_IdVoucher.Name = "col1_IdVoucher"
        Me.col1_IdVoucher.OptionsColumn.AllowEdit = False
        Me.col1_IdVoucher.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdVoucher.Visible = True
        '
        'col1_Urlbanorte
        '
        Me.col1_Urlbanorte.Caption = "Urlbanorte"
        Me.col1_Urlbanorte.FieldName = "UrlBanorte"
        Me.col1_Urlbanorte.Name = "col1_Urlbanorte"
        Me.col1_Urlbanorte.OptionsColumn.AllowEdit = False
        Me.col1_Urlbanorte.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Urlbanorte.Visible = True
        '
        'col1_Puerto
        '
        Me.col1_Puerto.Caption = "Puerto"
        Me.col1_Puerto.FieldName = "Puerto"
        Me.col1_Puerto.Name = "col1_Puerto"
        Me.col1_Puerto.OptionsColumn.AllowEdit = False
        Me.col1_Puerto.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Puerto.Visible = True
        '
        'col1_User1
        '
        Me.col1_User1.Caption = "User1"
        Me.col1_User1.FieldName = "User1"
        Me.col1_User1.Name = "col1_User1"
        Me.col1_User1.OptionsColumn.AllowEdit = False
        Me.col1_User1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_User1.Visible = True
        '
        'col1_Pswd
        '
        Me.col1_Pswd.Caption = "Pswd"
        Me.col1_Pswd.FieldName = "Pswd"
        Me.col1_Pswd.Name = "col1_Pswd"
        Me.col1_Pswd.OptionsColumn.AllowEdit = False
        Me.col1_Pswd.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Pswd.Visible = True
        '
        'col1_Idmerchant
        '
        Me.col1_Idmerchant.Caption = "Idmerchant"
        Me.col1_Idmerchant.FieldName = "IdMerchant"
        Me.col1_Idmerchant.Name = "col1_Idmerchant"
        Me.col1_Idmerchant.OptionsColumn.AllowEdit = False
        Me.col1_Idmerchant.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Idmerchant.Visible = True
        '
        'col1_Modo
        '
        Me.col1_Modo.Caption = "Modo"
        Me.col1_Modo.FieldName = "Modo"
        Me.col1_Modo.Name = "col1_Modo"
        Me.col1_Modo.OptionsColumn.AllowEdit = False
        Me.col1_Modo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Modo.Visible = True
        '
        'col1_Caja
        '
        Me.col1_Caja.Caption = "Caja"
        Me.col1_Caja.FieldName = "Caja"
        Me.col1_Caja.Name = "col1_Caja"
        Me.col1_Caja.OptionsColumn.AllowEdit = False
        Me.col1_Caja.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Caja.Visible = True
        '
        'col1_IdEmpresa
        '
        Me.col1_IdEmpresa.Caption = "IdEmpresa"
        Me.col1_IdEmpresa.FieldName = "id_empresa"
        Me.col1_IdEmpresa.Name = "col1_IdEmpresa"
        Me.col1_IdEmpresa.OptionsColumn.AllowEdit = False
        Me.col1_IdEmpresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdEmpresa.Visible = True
        '
        'col1_Empresa
        '
        Me.col1_Empresa.Caption = "Empresa"
        Me.col1_Empresa.FieldName = "empresa"
        Me.col1_Empresa.Name = "col1_Empresa"
        Me.col1_Empresa.OptionsColumn.AllowEdit = False
        Me.col1_Empresa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Empresa.Visible = True
        '
        'col1_Rfc
        '
        Me.col1_Rfc.Caption = "Rfc"
        Me.col1_Rfc.FieldName = "rfc"
        Me.col1_Rfc.Name = "col1_Rfc"
        Me.col1_Rfc.OptionsColumn.AllowEdit = False
        Me.col1_Rfc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Rfc.Visible = True
        '
        'col1_Direccion
        '
        Me.col1_Direccion.Caption = "Direccion"
        Me.col1_Direccion.FieldName = "direccion"
        Me.col1_Direccion.Name = "col1_Direccion"
        Me.col1_Direccion.OptionsColumn.AllowEdit = False
        Me.col1_Direccion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Direccion.Visible = True
        '
        'col1_Direccion2
        '
        Me.col1_Direccion2.Caption = "Direccion2"
        Me.col1_Direccion2.FieldName = "direccion2"
        Me.col1_Direccion2.Name = "col1_Direccion2"
        Me.col1_Direccion2.OptionsColumn.AllowEdit = False
        Me.col1_Direccion2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Direccion2.Visible = True
        '
        'col1_Tipo
        '
        Me.col1_Tipo.Caption = "Tipo"
        Me.col1_Tipo.FieldName = "tipo"
        Me.col1_Tipo.Name = "col1_Tipo"
        Me.col1_Tipo.OptionsColumn.AllowEdit = False
        Me.col1_Tipo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipo.Visible = True
        '
        'col1_Transaccion
        '
        Me.col1_Transaccion.Caption = "Transaccion"
        Me.col1_Transaccion.FieldName = "transaccion"
        Me.col1_Transaccion.Name = "col1_Transaccion"
        Me.col1_Transaccion.OptionsColumn.AllowEdit = False
        Me.col1_Transaccion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Transaccion.Visible = True
        '
        'col1_IdSucursal
        '
        Me.col1_IdSucursal.Caption = "IdSucursal"
        Me.col1_IdSucursal.FieldName = "id_sucursal"
        Me.col1_IdSucursal.Name = "col1_IdSucursal"
        Me.col1_IdSucursal.OptionsColumn.AllowEdit = False
        Me.col1_IdSucursal.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_IdSucursal.Visible = True
        '
        'col1_Respuesta
        '
        Me.col1_Respuesta.Caption = "Respuesta"
        Me.col1_Respuesta.FieldName = "Respuesta"
        Me.col1_Respuesta.Name = "col1_Respuesta"
        Me.col1_Respuesta.OptionsColumn.AllowEdit = False
        Me.col1_Respuesta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Respuesta.Visible = True
        '
        'col1_Afiliacion
        '
        Me.col1_Afiliacion.Caption = "Afiliacion"
        Me.col1_Afiliacion.FieldName = "Afiliacion"
        Me.col1_Afiliacion.Name = "col1_Afiliacion"
        Me.col1_Afiliacion.OptionsColumn.AllowEdit = False
        Me.col1_Afiliacion.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Afiliacion.Visible = True
        '
        'col1_Terminalid
        '
        Me.col1_Terminalid.Caption = "Terminalid"
        Me.col1_Terminalid.FieldName = "TerminalId"
        Me.col1_Terminalid.Name = "col1_Terminalid"
        Me.col1_Terminalid.OptionsColumn.AllowEdit = False
        Me.col1_Terminalid.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Terminalid.Visible = True
        '
        'col1_Tiptarjeta
        '
        Me.col1_Tiptarjeta.Caption = "Tiptarjeta"
        Me.col1_Tiptarjeta.FieldName = "TipTarjeta"
        Me.col1_Tiptarjeta.Name = "col1_Tiptarjeta"
        Me.col1_Tiptarjeta.OptionsColumn.AllowEdit = False
        Me.col1_Tiptarjeta.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tiptarjeta.Visible = True
        '
        'col1_Bancoemisor
        '
        Me.col1_Bancoemisor.Caption = "Bancoemisor"
        Me.col1_Bancoemisor.FieldName = "BancoEmisor"
        Me.col1_Bancoemisor.Name = "col1_Bancoemisor"
        Me.col1_Bancoemisor.OptionsColumn.AllowEdit = False
        Me.col1_Bancoemisor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Bancoemisor.Visible = True
        '
        'col1_Mesesdiferido
        '
        Me.col1_Mesesdiferido.Caption = "Mesesdiferido"
        Me.col1_Mesesdiferido.FieldName = "MesesDiferido"
        Me.col1_Mesesdiferido.Name = "col1_Mesesdiferido"
        Me.col1_Mesesdiferido.OptionsColumn.AllowEdit = False
        Me.col1_Mesesdiferido.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Mesesdiferido.Visible = True
        '
        'col1_Numpagos
        '
        Me.col1_Numpagos.Caption = "Numpagos"
        Me.col1_Numpagos.FieldName = "NumPagos"
        Me.col1_Numpagos.Name = "col1_Numpagos"
        Me.col1_Numpagos.OptionsColumn.AllowEdit = False
        Me.col1_Numpagos.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Numpagos.Visible = True
        '
        'col1_Tipoplan
        '
        Me.col1_Tipoplan.Caption = "Tipoplan"
        Me.col1_Tipoplan.FieldName = "TipoPlan"
        Me.col1_Tipoplan.Name = "col1_Tipoplan"
        Me.col1_Tipoplan.OptionsColumn.AllowEdit = False
        Me.col1_Tipoplan.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Tipoplan.Visible = True
        '
        'col1_Autonumsuc
        '
        Me.col1_Autonumsuc.Caption = "Autonumsuc"
        Me.col1_Autonumsuc.FieldName = "autonumsuc"
        Me.col1_Autonumsuc.Name = "col1_Autonumsuc"
        Me.col1_Autonumsuc.OptionsColumn.AllowEdit = False
        Me.col1_Autonumsuc.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
        Me.col1_Autonumsuc.Visible = True
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Button2)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.cbo_Banco)
        Me.GroupControl1.Controls.Add(Me.Label31)
        Me.GroupControl1.Controls.Add(Me.lstImpresoras)
        Me.GroupControl1.Controls.Add(Me.Button1)
        Me.GroupControl1.Controls.Add(Me.dat_FechaFin)
        Me.GroupControl1.Controls.Add(Me.dat_FechaIni)
        Me.GroupControl1.Controls.Add(Me.btn_Refrescar)
        Me.GroupControl1.Controls.Add(Me.lbl_Tarjeta)
        Me.GroupControl1.Controls.Add(Me.lbl_Cuenta)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 389)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(860, 96)
        Me.GroupControl1.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(663, 25)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(55, 42)
        Me.Button2.TabIndex = 23
        Me.Button2.Text = "Verificar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(270, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 23)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Banco"
        '
        'cbo_Banco
        '
        Me.cbo_Banco.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_Banco.FormattingEnabled = True
        Me.cbo_Banco.Items.AddRange(New Object() {"BANORTE"})
        Me.cbo_Banco.Location = New System.Drawing.Point(379, 29)
        Me.cbo_Banco.Name = "cbo_Banco"
        Me.cbo_Banco.Size = New System.Drawing.Size(211, 31)
        Me.cbo_Banco.TabIndex = 21
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(270, 63)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(103, 23)
        Me.Label31.TabIndex = 20
        Me.Label31.Text = "Impresora:"
        '
        'lstImpresoras
        '
        Me.lstImpresoras.FormattingEnabled = True
        Me.lstImpresoras.Location = New System.Drawing.Point(379, 70)
        Me.lstImpresoras.Name = "lstImpresoras"
        Me.lstImpresoras.Size = New System.Drawing.Size(284, 21)
        Me.lstImpresoras.TabIndex = 19
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(724, 25)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(55, 42)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Imprimir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dat_FechaFin
        '
        Me.dat_FechaFin.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaFin.Location = New System.Drawing.Point(117, 63)
        Me.dat_FechaFin.Name = "dat_FechaFin"
        Me.dat_FechaFin.Size = New System.Drawing.Size(145, 30)
        Me.dat_FechaFin.TabIndex = 5
        '
        'dat_FechaIni
        '
        Me.dat_FechaIni.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dat_FechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dat_FechaIni.Location = New System.Drawing.Point(117, 29)
        Me.dat_FechaIni.Name = "dat_FechaIni"
        Me.dat_FechaIni.Size = New System.Drawing.Size(145, 30)
        Me.dat_FechaIni.TabIndex = 4
        '
        'btn_Refrescar
        '
        Me.btn_Refrescar.Location = New System.Drawing.Point(596, 28)
        Me.btn_Refrescar.Name = "btn_Refrescar"
        Me.btn_Refrescar.Size = New System.Drawing.Size(42, 42)
        Me.btn_Refrescar.TabIndex = 0
        Me.btn_Refrescar.Text = "..."
        Me.btn_Refrescar.UseVisualStyleBackColor = True
        '
        'lbl_Tarjeta
        '
        Me.lbl_Tarjeta.AutoSize = True
        Me.lbl_Tarjeta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Tarjeta.Location = New System.Drawing.Point(5, 63)
        Me.lbl_Tarjeta.Name = "lbl_Tarjeta"
        Me.lbl_Tarjeta.Size = New System.Drawing.Size(85, 23)
        Me.lbl_Tarjeta.TabIndex = 3
        Me.lbl_Tarjeta.Text = "FechaFin"
        '
        'lbl_Cuenta
        '
        Me.lbl_Cuenta.AutoSize = True
        Me.lbl_Cuenta.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Cuenta.Location = New System.Drawing.Point(5, 29)
        Me.lbl_Cuenta.Name = "lbl_Cuenta"
        Me.lbl_Cuenta.Size = New System.Drawing.Size(88, 23)
        Me.lbl_Cuenta.TabIndex = 1
        Me.lbl_Cuenta.Text = "Fecha Ini"
        '
        'pdvVouchers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 560)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.KeyPreview = True
        Me.Name = "pdvVouchers"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VOUCHERS"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridC_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridV_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Aceptar As Button
    Friend WithEvents btn_Cancelar As Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridC_1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl

    Friend WithEvents col1_IdVoucher As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Urlbanorte As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Puerto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_User1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Pswd As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Idmerchant As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Modo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Caja As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdEmpresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Empresa As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Rfc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Direccion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Direccion2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Transaccion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_IdSucursal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Hora As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Banco As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Respuesta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Afiliacion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Terminalid As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numcontrol As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numtarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tiptarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Vigtarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tittarjeta As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Bancoemisor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Codigoaut As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Referencia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Importe As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Mesesdiferido As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Numpagos As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Tipoplan As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Autonumsuc As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents col1_Fum As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn

    Friend WithEvents lbl_Tarjeta As Label
    Friend WithEvents lbl_Cuenta As Label
    Friend WithEvents btn_Refrescar As Button
    Friend WithEvents GridV_1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridView
    Friend WithEvents dat_FechaIni As DateTimePicker
    Friend WithEvents gridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents dat_FechaFin As DateTimePicker
    Friend WithEvents Button1 As Button
    Friend WithEvents Label31 As Label
    Friend WithEvents lstImpresoras As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cbo_Banco As ComboBox
    Friend WithEvents Button2 As Button
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
End Class
