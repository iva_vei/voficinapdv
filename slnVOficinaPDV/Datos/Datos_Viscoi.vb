﻿Imports System.Data.SqlClient
Public Class Datos_Viscoi
    Private Const PA_ESTRUCTURA_SEL As String = "pa_Estructura_SEL"
    Private Const FN_OPCIONES_PROCESO_STR_SEL As String = "fn_Opciones_Proceso_Str_Sel"
    Private Const FN_OPCIONES_PROCESO_NUM_SEL As String = "fn_Opciones_Proceso_Num_Sel"
    Private Const SP_INSERTA_IMAGENESAPP As String = "sp_Inserta_ImagenesApp"
    Private Const SP_RECUPERA_IMAGENESAPP As String = "sp_Recupera_ImagenesApp"
    Private Const SP_RECUPERA_DOCUMENTOS As String = "sp_Recupera_Documentos"
    Private Const SP_PVTA_RECUPERA_ARTICULOS As String = "sp_PVTA_Recupera_Articulos"
    Private Const FN_PVTA_DETERMINA_PROMOCION As String = "fn_PVTA_Determina_Promocion"
    Private Const FN_PVTA_DETERMINA_PROMOCION_PUNTOS As String = "fn_PVTA_Determina_Promocion_Puntos"
    Private Const SP_PVTA_RECUPERA_CAJAS As String = "sp_PVTA_Recupera_Cajas"
    Private Const SP_PVTA_RECUPERA_CAJAS_ABIERTAS As String = "sp_PVTA_Recupera_Cajas_Abiertas"
    Private Const SP_PVTA_RECUPERA_CAJAS_CERRADAS As String = "sp_PVTA_Recupera_Cajas_Cerradas"
    Private Const SP_PVTA_RECUPERA_CAJEROS As String = "sp_PVTA_Recupera_Cajeros"
    Private Const SP_PVTA_RECUPERA_CLIENTES As String = "sp_PVTA_Recupera_Clientes"
    Private Const SP_PVTA_RECUPERA_DESGLOSE As String = "sp_PVTA_Recupera_Desglose"
    Private Const SP_PVTA_RECUPERA_DESGLOSE1 As String = "sp_PVTA_Recupera_Desglose1"
    Private Const SP_PVTA_RECUPERA_DETVENTAS As String = "sp_PVTA_Recupera_DetVentas"
    Private Const SP_PVTA_RECUPERA_DETVENTAS_IMPRESION As String = "sp_PVTA_Recupera_DetVentas_Impresion"
    Private Const SP_PVTA_RECUPERA_DETVENTAS_COTIZACION As String = "sp_PVTA_Recupera_DetVentas_Cotizacion"
    Private Const SP_PVTA_RECUPERA_DETVENTASPROMOCIONES As String = "sp_PVTA_Recupera_DetVentasPromociones"
    Private Const SP_PVTA_RECUPERA_EMPRESA As String = "sp_PVTA_Recupera_Empresa"
    Private Const SP_PVTA_RECUPERA_EMPRESAS As String = "sp_PVTA_Recupera_Empresas"
    Private Const SP_PVTA_RECUPERA_ENTREGADINERO As String = "sp_PVTA_Recupera_EntregaDinero"
    Private Const SP_PVTA_RECUPERA_FORMASPAGO As String = "sp_PVTA_Recupera_FormasPago"
    Private Const SP_PVTA_RECUPERA_LISTAPRECIOS As String = "sp_PVTA_Recupera_ListaPrecios"
    Private Const SP_PVTA_RECUPERA_MONEDAS As String = "sp_PVTA_Recupera_Monedas"
    Private Const SP_PVTA_RECUPERA_MOVIMIENTOENTREGAS As String = "sp_PVTA_Recupera_MovimientoEntregas"
    Private Const SP_PVTA_RECUPERA_PARAMETROSCONTROL As String = "sp_PVTA_Recupera_ParametrosControl"
    Private Const SP_PVTA_RECUPERA_SUCURSALES As String = "sp_PVTA_Recupera_Sucursales"
    Private Const SP_PVTA_RECUPERA_TRANSACCIONES As String = "sp_PVTA_Recupera_Transacciones"
    Private Const SP_PVTA_RECUPERA_TRANSACCIONES_TICKET As String = "sp_PVTA_Recupera_Transacciones_Ticket"
    Private Const SP_PVTA_RECUPERA_TRANSACCIONES_TICKET_DUPLICADO As String = "sp_PVTA_Recupera_Transacciones_Ticket_Duplicado"
    Private Const SP_PVTA_RECUPERA_TRANSACCIONESCLIENTES As String = "sp_PVTA_Recupera_TransaccionesClientes"
    Private Const SP_PVTA_RECUPERA_VENTAS_ESTRUCTURA As String = "sp_PVTA_Recupera_Ventas_Estructura"
    Private Const SP_PVTA_RECUPERA_VENTASPORRANGOFECHA As String = "sp_PVTA_Recupera_VentasPorRangoFecha"
    Private Const SP_PVTA_INSERTA_CAJAS As String = "sp_PVTA_Inserta_Cajas"
    Private Const SP_PVTA_INSERTA_CAJEROS As String = "sp_PVTA_Inserta_Cajeros"
    Private Const SP_PVTA_INSERTA_CORTE As String = "sp_PVTA_Inserta_Corte"
    Private Const SP_PVTA_INSERTA_DESGLOSE As String = "sp_PVTA_Inserta_Desglose"
    Private Const SP_PVTA_INSERTA_DETVENTAS As String = "sp_PVTA_Inserta_DetVentas"
    Private Const SP_PVTA_INSERTA_DETVENTAS_IMPRESION As String = "sp_PVTA_Inserta_DetVentas_Impresion"
    Private Const SP_PVTA_INSERTA_DETVENTASPROMOCIONES As String = "sp_PVTA_Inserta_DetVentasPromociones"
    Private Const SP_PVTA_INSERTA_ENTREGADINERO As String = "sp_PVTA_Inserta_EntregaDinero"
    Private Const SP_PVTA_INSERTA_EXISTENCIAS As String = "sp_PVTA_Inserta_Existencias"
    Private Const SP_PVTA_INSERTA_MOVTOSINVENT As String = "sp_PVTA_Inserta_MovtosInvent"
    Private Const SP_PVTA_INSERTA_MOVTOSINVENTDET As String = "sp_PVTA_Inserta_MovtosInventDet"
    Private Const SP_PVTA_INSERTA_TRANSACCION As String = "sp_PVTA_Inserta_Transaccion"
    Private Const SP_PVTA_INSERTA_TRANSACCIONESCLIENTES As String = "sp_PVTA_Inserta_TransaccionesClientes"
    Private Const SP_PVTA_INSERTA_DETVENTAS_ARTICULO As String = "sp_PVTA_Inserta_DetVentas_Articulo"
    Private Const SP_PVTA_INSERTA_TMPTRANSACCIONES As String = "sp_PVTA_Inserta_TmpTransacciones"
    Private Const SP_PVTA_TRANSACCIONES_ESTATUS As String = "sp_PVTA_Transacciones_Estatus"
    Private Const SP_PVTA_RECUPERA_VENDEDORES As String = "sp_PVTA_Recupera_Vendedores"
    Private Const SP_PVTA_ELIMINA_DETVENTAS As String = "sp_PVTA_Elimina_DetVentas"
    Private Const SP_PVTA_RECUPERA_TCAMBIOS As String = "sp_PVTA_Recupera_TCambios"
    Private Const SP_PVTA_RECUPERA_ENTREGAS_DENOMINACION As String = "sp_PVTA_Recupera_entregas_denominacion"
    Private Const SP_PVTA_INSERTA_ENTREGAS As String = "sp_PVTA_Inserta_entregas"
    Private Const SP_PVTA_INSERTA_DETVENTAS_DESCUENTOS As String = "sp_PVTA_Inserta_DetVentas_Descuentos"
    Private Const SP_PVTA_INSERTA_DETVENTAS_COTIZACION As String = "sp_PVTA_Inserta_DetVentas_Cotizacion"
    Private Const SP_PVTA_RECUPERA_TMP_TRANSACCIONES As String = "sp_PVTA_Recupera_Tmp_Transacciones"
    Private Const SP_PVTA_RECUPERA_COTIZACIONES As String = "sp_PVTA_Recupera_Cotizaciones"
    Private Const SP_PVTA_CONTRAVALERPT As String = "sp_PVTA_ContravaleRpt"
    Private Const SP_PVTA_RECUPERA_CUENTAS As String = "sp_PVTA_Recupera_Cuentas"
    Private Const SP_PVTA_RECUPERA_CUENTASFINAL As String = "sp_PVTA_Recupera_CuentasFinal"
    Private Const SP_PVTA_RECUPERA_GASTOS As String = "sp_PVTA_Recupera_Gastos"
    Private Const SP_PV_RECUPERA_CORTECAJA As String = "sp_PV_Recupera_CorteCaja"
    Private Const SP_PV_RECUPERA_ENTREGAPARCIALCAJA As String = "sp_PV_Recupera_EntregaParcialCaja"
    Private Const SP_PVTA_RECUPERA_BANCOS As String = "sp_PVTA_Recupera_Bancos"
    Private Const SP_PVTA_RECUPERA_CORTE_ESTATUS As String = "sp_PVTA_Recupera_Corte_Estatus"
    Private Const SP_PVTA_RECUPERA_CLIEDOCTA As String = "sp_PVTA_Recupera_cliedocta"
    Private Const SP_PVTA_INSERTA_PAGO_CLIEDOCTA As String = "sp_PVTA_Inserta_Pago_cliedocta"
    Private Const SP_PVTA_RECUPERA_CLIEDOCTA_FINAL As String = "sp_PVTA_Recupera_cliedocta_final"
    Private Const SP_PVTA_INSERTA_PAGO_CLIEDOCTA_FINAL As String = "sp_PVTA_Inserta_Pago_cliedocta_final"
    Private Const SP_PVTA_RECUPERA_SATUSOCDFI As String = "sp_PVTA_Recupera_SatUsoCDFI"
    Private Const SP_PVTA_RECUPERA_SATREGIMENFISCAL As String = "sp_PVTA_Recupera_SatRegimenFiscal"
    Private Const SP_PVTA_RECUPERA_SATREGIMENFISCAL40 As String = "sp_PVTA_Recupera_SatRegimenFiscal40"
    Private Const SP_PVTA_RECUPERA_SATFORMAPAGO As String = "sp_PVTA_Recupera_SatFormaPago"
    Private Const SP_PVTA_RECUPERA_SATINFO As String = "sp_PVTA_Recupera_SatInfo"
    Private Const SP_INSERTA_CUENTASLUGARENTREGA As String = "sp_Inserta_CuentasLugarEntrega"
    Private Const SP_RECUPERA_CUENTASLUGARENTREGA As String = "sp_Recupera_CuentasLugarEntrega"
    Private Const SP_INSERTA_TICKETFACTURA As String = "sp_Inserta_TicketFactura"
    Private Const SP_INSERTA_FACTURAGLOBAL As String = "sp_Inserta_FacturaGlobal"
    Private Const SP_RECUPERA_TICKETFACTURA As String = "sp_Recupera_TicketFactura"
    Private Const SP_ACTUALIZA_TICKETFACTURA_DIRENTREGA As String = "sp_ACtualiza_TicketFactura_DirEntrega"
    Private Const SP_ACTUALIZA_TICKETFACTURA_DATOSCFDI As String = "sp_ACtualiza_TicketFactura_DatosCFDI"
    Private Const SP_INSERTA_CUENTA As String = "sp_Inserta_Cuenta"
    Private Const SP_ACTUALIZA_TICKETFACTURA_CUENTA As String = "sp_ACtualiza_TicketFactura_Cuenta"
    Private Const SP_ACTUALIZA_TICKETFACTURA_DATOSCUENTA As String = "sp_ACtualiza_TicketFactura_DatosCuenta"
    Private Const SP_ACTUALIZA_TICKETFACTURA_CFDI_GEN As String = "sp_Actualiza_TicketFactura_CFDI_Gen"
    Private Const SP_ACTUALIZA_TICKETFACTURA_ESTATUS As String = "sp_ACtualiza_TicketFactura_Estatus"
    Private Const SP_PVTA_CAJA_VALIDA_EFECTIVO As String = "sp_PVTA_Caja_Valida_Efectivo"
    Private Const SP_PVTA_ACTUALIZA_CAJA_ABRIR As String = "SP_PVTA_ACTUALIZA_CAJA_ABRIR"
    Private Const SP_PVTA_EMPLEADOS As String = "SP_PVTA_EMPLEADOS"
    Private Const SP_PVTA_ACTUALIZA_DETVENTAS_NOMBRE As String = "sp_PVTA_Actualiza_DetVentas_Nombre"
    Private Const SP_VERIFICAIDENTIDAD_SEL As String = "sp_VerificaIdentidad_Sel"
    Private Const SP_VERIFICAIDENTIDAD_UPD As String = "sp_VerificaIdentidad_Upd"
    Private Const SP_ACTUALIZA_NUBE_BITACORA_AUTORIZACIONE As String = "sp_Actualiza_Nube_Bitacora_Autorizacione"
    Private Const SP_RECUPERA_TIPOSCREDITO As String = "sp_Recupera_tiposcredito"
    Private Const SP_INSERTA_CLIENTEFINAL As String = "sp_Inserta_ClienteFinal"
    Private Const SP_PVTA_RECUEPERA_FOLIODIGITAL_SEL As String = "sp_PVTA_Recuepera_FolioDigital_Sel"

    Private Const SP_INSERTA_DETVENTAS_CANCELACION As String = "sp_Inserta_Detventas_Cancelacion"
    Private Const SP_ACTUALIZA_DETVENTAS_CANTIDADES As String = "sp_Actualiza_Detventas_Cantidades"
    Private Const SP_PVTA_TRANSACCIONES_CANCELACION_ESTATUS As String = "sp_PVTA_Transacciones_Cancelacion_Estatus"
    Private Const SP_PVTA_RECUPERA_NEGADOS_MOTIVOS As String = "sp_PVTA_Recupera_negados_motivos"
    Private Const SP_PVTA_INSERTA_ARTICULOS_NEGADOS As String = "sp_PVTA_Inserta_articulos_negados"
    Private Const SP_PVTA_RECUPERA_PAGARE As String = "sp_PVTA_Recupera_Pagare"
    Private Const SP_PVTA_PEDIDOS_SEL As String = "sp_PVTA_Pedidos_Sel"
    Private Const SP_PVTA_PEDIDOSNOTIFICACION_SEL As String = "sp_PVTA_PedidosNotificacion_Sel"
    Private Const SP_PVTA_VOUCHER_INSERTA As String = "sp_PVTA_Voucher_Inserta"
    Private Const SP_PVTA_VOUCHER_ACTUALIZA As String = "sp_PVTA_Voucher_Actualiza"
    Private Const SP_PVTA_VOUCHER_SEL As String = "sp_PVTA_Voucher_Sel"
    Private Const SP_GEO_ESTADOS As String = "sp_Geo_Estados"
    Private Const SP_GEO_MUNICIPIOS As String = "sp_Geo_Municipios"
    Private Const SP_GEO_ASENTAMIENTOS As String = "sp_Geo_Asentamientos"
    Private Const SP_PVTA_RECUPERA_CLIENTEFINAL As String = "sp_PVTA_Recupera_ClienteFinal"
    Private Const SP_CREDITO_TASAS_PLAZO_SEL As String = "sp_Credito_tasas_Plazo_Sel"
    Private Const SP_CREDITO_VALES_INS As String = "sp_Credito_Vales_Ins"
    Private Const SP_EDOCTACLIENTES_SEL As String = "SP_EdoctaClientes_Sel"
    Private Const SP_PVTA_RECUPERA_CANJE_IMPRESION As String = "sp_PVTA_Recupera_Canje_Impresion"
    Private Const SP_RECUPERA_ANTIGUEDAD_SALDOS As String = "sp_CAR_Recupera_Antiguedad_Saldos"
    Private Const SP_CLI_APLICACIONES_SEL As String = "sp_CLI_Aplicaciones_Sel"
    Private Const SP_CREDITO_VALES_SERIES_SEL As String = "sp_Credito_Vales_Series_Sel"
    Private Const SP_CRM_TIPOS_SEL As String = "sp_crm_tipos_sel"
    Private Const SP_PVTA_CTABANCOS_BOVEDA_SEL As String = "sp_PVTA_ctabancos_BOVEDA_Sel"
    Private Const SP_PVTA_TRASPASO_DINERO_INS As String = "sp_PVTA_Traspaso_Dinero_INS"
    Private Const SP_CAR_GENERA_FOLIO_DIGITAL_NAU As String = "sp_CAR_Genera_Folio_Digital_NAU"
    Private Const SP_INSERTA_TICKETFACTURA_REFACTURA As String = "sp_Inserta_TicketFactura_Refactura"
    Private Const SP_TIM_CANC_ACTUALIZA_TICKETFACTURA_CFDI_GEN As String = "sp_Tim_Canc_Actualiza_TicketFactura_CFDI_Gen"

    Private Const SP_PVTA_TRANSACCIONES_PUBMAY As String = "sp_PVTA_Transacciones_PUBMAY"
    Private Const SP_PVTA_SAT_CFDI_FECHAACTUAL As String = "sp_PVTA_Sat_CFDI_FechaActual"
    Private Const SP_PVTA_MONEDERO_ELECTRONICO_SEL As String = "sp_PVTA_monedero_electronico_Sel"
    Private Const SP_CLIENTES_FINALES_DATOS_BANCARIOS_INS As String = "sp_clientes_finales_datos_bancarios_Ins"

    Private Const SP_PVTA_LSTPRECIOS As String = "sp_PVTA_LstPrecios"

    Public Function Test(ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = "SELECT valor = 1"
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Resultado = False
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                Else
                    Resultado = False
                End If
            End If
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function SQLQuery(ByVal sSQLquery As String, ByVal sBaseDatos As String, ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        ''Dim dtDatos As DataTable

        Try
            If sBaseDatos = "#EJECUTAR_CONTROL#" Then
                cn = Globales.oAmbientes.oControl.ObtenerConexion()
            Else
                cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            End If
            strCommand.CommandText = sSQLquery
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Resultado = True
            ''If dts.Tables.Count > 0 Then
            ''    dtDatos = dts.Tables(0)
            ''    If dtDatos.Rows.Count > 0 Then
            ''        Resultado = True
            ''    Else
            ''        Resultado = False
            ''    End If
            ''End If
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Busca_Estructura_Sel(ByRef dtDatos As DataTable,
                                   ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oControl.ObtenerConexion()
            strCommand.CommandText = PA_ESTRUCTURA_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            'strCommand.Parameters.AddWithValue("@sistema", sistema)
            'strCommand.Parameters.AddWithValue("@producto", producto)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                '_usuarios_permisos = dts.Tables(3)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_ImagenesApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String _
                                   , ByVal fecha As Date _
                                   , ByVal id_usuario As String _
                                   , ByVal foto As Byte() _
                                   , ByRef dtDatos As DataTable _
                                   , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_IMAGENESAPP
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@tabla", tabla)
            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@renglon", renglon)
            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@foto", foto)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                '_usuarios_permisos = dts.Tables(3)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Recupera_ImagenesApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String _
                                   , ByRef dtDatos As DataTable _
                                   , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_RECUPERA_IMAGENESAPP
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@tabla", tabla)
            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@renglon", renglon)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                '_usuarios_permisos = dts.Tables(3)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Recupera_Documentos(ByVal id_empresa As String _
                                   , ByRef dtDatos As DataTable _
                                   , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_RECUPERA_DOCUMENTOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                '_usuarios_permisos = dts.Tables(3)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Estructura_SEL(ByRef dtDatos As DataTable,
                                   ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = PA_ESTRUCTURA_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            'strCommand.Parameters.AddWithValue("@sistema", sistema)
            'strCommand.Parameters.AddWithValue("@producto", producto)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                '_usuarios_permisos = dts.Tables(3)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Opciones_Proceso_Str_Sel(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal clave_division As String _
                                , ByVal clave_depto As String _
                                , ByVal clave_familia As String _
                                , ByVal clave_articulo As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = "SELECT promocion = dbo." & FN_PVTA_DETERMINA_PROMOCION & "('" & id_empresa & "','" & id_sucursal & "','" & clave_division & "','" & clave_depto & "','" & clave_familia & "','" & clave_articulo & "')"
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Determina_Promocion(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal clave_division As String _
                                , ByVal clave_depto As String _
                                , ByVal clave_familia As String _
                                , ByVal clave_articulo As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = "SELECT promocion = dbo." & FN_PVTA_DETERMINA_PROMOCION & "('" & id_empresa & "','" & id_sucursal & "','" & clave_division & "','" & clave_depto & "','" & clave_familia & "','" & clave_articulo & "')"
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Determina_Promocion_Puntos(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal clave_division As String _
                                , ByVal clave_depto As String _
                                , ByVal clave_familia As String _
                                , ByVal clave_articulo As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = "SELECT promocion = dbo." & FN_PVTA_DETERMINA_PROMOCION_PUNTOS & "('" & id_empresa & "','" & id_sucursal & "','" & clave_division & "','" & clave_depto & "','" & clave_familia & "','" & clave_articulo & "')"
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Articulos(ByVal estatus As String _
                                , ByVal codigoref As String _
                                , ByVal codigobarras As String _
                                , ByVal marca As String _
                                , ByVal clase As String _
                                , ByVal nombre As String _
                                , ByVal nomcorto As String _
                                , ByVal estructura As String _
                                , ByVal sw_existencias As Boolean _
                                , ByRef dtDatos As DataTable _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_ARTICULOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@codigoref", codigoref)
            strCommand.Parameters.AddWithValue("@codigo", codigobarras)
            strCommand.Parameters.AddWithValue("@marca", marca)
            strCommand.Parameters.AddWithValue("@clase", clase)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@nomcorto", nomcorto)
            strCommand.Parameters.AddWithValue("@estructura", estructura)
            strCommand.Parameters.AddWithValue("@sw_existencias", sw_existencias)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Cajas(ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal id_pventa As String _
, ByVal caja As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CAJAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@caja", caja)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Cajas_Abiertas(ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CAJAS_ABIERTAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Cajas_Cerradas(ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CAJAS_CERRADAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Cajeros(ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal id_pventa As String _
, ByVal cajero As String _
, ByVal id_usuario As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CAJEROS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Clientes(ByVal id_cliente As String _
, ByVal nombre As String _
, ByVal telefono As String _
, ByVal rfc As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CLIENTES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@telefono", telefono)
            strCommand.Parameters.AddWithValue("@rfc", rfc)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Desglose(ByVal autonumsuc As String _
                                , ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal fecha As Date _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal tipo As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_DESGLOSE
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@tipo", tipo)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Desglose1(ByVal autonumsuc As String _
                                , ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal fecha As Date _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal tipo As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_DESGLOSE1
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@tipo", tipo)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_DetVentas(ByVal fecha As Date _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef dtDatos1 As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_DETVENTAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                dtDatos1 = dts.Tables(1)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_DetVentas_Impresion(ByVal fecha As Date _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef dtDatos1 As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_DETVENTAS_IMPRESION
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                dtDatos1 = dts.Tables(1)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_DetVentas_Cotizacion(ByVal fecha As Date _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef dtDatos1 As DataTable _
                               , ByRef dtDatos2 As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_DETVENTAS_COTIZACION
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                dtDatos1 = dts.Tables(1)
                dtDatos2 = dts.Tables(2)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_DetVentasPromociones(ByVal renglon As Integer _
, ByVal transaccion As Double _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal articulo As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_DETVENTASPROMOCIONES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@renglon", renglon)
            strCommand.Parameters.AddWithValue("@transaccion", transaccion)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@articulo", articulo)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Empresa(ByVal id_usuario As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_EMPRESA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Empresas(ByVal id_empresa As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_EMPRESAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_EntregaDinero(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal corte As Integer _
                                , ByVal fecha As Date _
                                , ByVal top As Integer _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_ENTREGADINERO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@top", top)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_FormasPago(ByRef dtDatos As DataTable _
                                           , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_FORMASPAGO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn



            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_ListaPrecios(ByVal codigo As String _
, ByVal descripcion As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_LISTAPRECIOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@codigo", codigo)
            strCommand.Parameters.AddWithValue("@descripcion", descripcion)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Monedas(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal moneda As String _
                                , ByVal estatus As String _
                                , ByVal sw_pdv As String _
                                , ByVal sw_fin As String _
                                , ByRef dtDatos As DataTable _
                                , ByRef Moneda_Nacional As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_MONEDAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@sw_pdv", sw_pdv)
            strCommand.Parameters.AddWithValue("@sw_fin", sw_fin)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                Moneda_Nacional = dts.Tables(1).Rows(0).Item("moneda_nacional")
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Gastos(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal concepto As String _
                                , ByVal estatus As String _
                                , ByRef dtDatos As DataTable _
                                , ByRef Moneda_Nacional As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_GASTOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@concepto", concepto)
            strCommand.Parameters.AddWithValue("@estatus", estatus)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                Moneda_Nacional = dts.Tables(1).Rows(0).Item("moneda_nacional")
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_MovimientoEntregas(ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_MOVIMIENTOENTREGAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_ParametrosControl(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal id_sistema As String _
                                , ByVal id_usuario As String _
                                , ByVal id_parametro As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_PARAMETROSCONTROL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_sistema", id_sistema)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@id_parametro", id_parametro)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Sucursales(ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_SUCURSALES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Transacciones(ByVal fecha As Date _
, ByVal fecha2 As Date _
, ByVal corte As Integer _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal id_pventa As String _
, ByVal caja As String _
, ByVal cajero As String _
, ByVal vendedor As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef dtDatos1 As DataTable _
                               , ByRef dtDatos2 As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_TRANSACCIONES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@fecha2", fecha2)
            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@vendedor", vendedor)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                dtDatos1 = dts.Tables(1)
                dtDatos2 = dts.Tables(2)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Transacciones_Ticket(ByVal fecha As Date _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal caja As String _
, ByVal cajero As String _
, ByVal autonumsuc As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_TRANSACCIONES_TICKET
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Transacciones_Ticket_Duplicado(ByVal fecha As Date _
, ByVal fecha2 As Date _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal caja As String _
, ByVal cajero As String _
, ByVal autonumsuc As String _
, ByVal transaccion As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_TRANSACCIONES_TICKET_DUPLICADO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@fecha2", fecha2)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@transaccion", transaccion)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_TransaccionesClientes(ByVal id_cliente As Integer _
, ByVal transaccion As Double _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal folio As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_TRANSACCIONESCLIENTES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@transaccion", transaccion)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@folio", folio)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Ventas_Estructura(ByVal fecha As Date _
, ByVal fecha2 As Date _
, ByVal nivel As Integer _
, ByVal corte As Integer _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal caja As String _
, ByVal cajero As String _
, ByVal vendedor As String _
, ByVal division As String _
, ByVal departamento As String _
, ByVal familia As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_VENTAS_ESTRUCTURA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@fecha2", fecha2)
            strCommand.Parameters.AddWithValue("@nivel", nivel)
            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@vendedor", vendedor)
            strCommand.Parameters.AddWithValue("@division", division)
            strCommand.Parameters.AddWithValue("@departamento", departamento)
            strCommand.Parameters.AddWithValue("@familia", familia)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_VentasPorRangoFecha(ByVal fecha_inicio As Date _
, ByVal fecha_fin As Date _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal caja As String _
, ByVal cajero As String _
, ByVal autonumsuc As String _
, ByVal estatus As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_VENTASPORRANGOFECHA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha_inicio", fecha_inicio)
            strCommand.Parameters.AddWithValue("@fecha_fin", fecha_fin)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@estatus", estatus)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Cajas(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal id_pventa As String _
                                , ByVal caja As String _
                                , ByVal nombre As String _
                                , ByVal id_usuario As String _
                                , ByVal usuariofum As String _
                                , ByVal printername1 As String _
                                , ByVal printerport1 As String _
                                , ByVal printerdriver1 As String _
                                , ByVal printername2 As String _
                                , ByVal printerport2 As String _
                                , ByVal printerdriver2 As String _
                                , ByVal printername3 As String _
                                , ByVal printerport3 As String _
                                , ByVal printerdriver3 As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_CAJAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@printername3", printername3)
            strCommand.Parameters.AddWithValue("@printerport3", printerport3)
            strCommand.Parameters.AddWithValue("@printerdriver3", printerdriver3)
            strCommand.Parameters.AddWithValue("@usuariofum", usuariofum)
            strCommand.Parameters.AddWithValue("@printername1", printername1)
            strCommand.Parameters.AddWithValue("@printerport1", printerport1)
            strCommand.Parameters.AddWithValue("@printerdriver1", printerdriver1)
            strCommand.Parameters.AddWithValue("@printername2", printername2)
            strCommand.Parameters.AddWithValue("@printerport2", printerport2)
            strCommand.Parameters.AddWithValue("@printerdriver2", printerdriver2)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Cajeros(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal id_pventa As String _
                                , ByVal cajero As String _
                                , ByVal id_usuario As String _
                                , ByVal nombre As String _
                                , ByVal nomina As String _
                                , ByVal contable As String _
                                , ByVal estatus As String _
                                , ByVal numcorte As String _
                                , ByVal feccorte As Date _
                                , ByVal usuariofum As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_CAJEROS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@nomina", nomina)
            strCommand.Parameters.AddWithValue("@contable", contable)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@numcorte", numcorte)
            strCommand.Parameters.AddWithValue("@feccorte", feccorte)
            strCommand.Parameters.AddWithValue("@usuariofum", usuariofum)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Corte(ByVal movimiento As Integer _
, ByVal entrega As Integer _
, ByVal numero As Integer _
, ByVal importe As Double _
, ByVal total As Double _
, ByVal tipocambio As Double _
, ByVal porcomision As Double _
, ByVal poriva As Double _
, ByVal comision As Double _
, ByVal iva As Double _
, ByVal totcomision As Double _
, ByVal totiva As Double _
, ByVal neto As Double _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal id_pventa As String _
, ByVal caja As String _
, ByVal cajero As String _
, ByVal moneda As String _
, ByVal cobrador As String _
, ByVal id_usuario As String _
, ByVal movto_teso As String _
, ByVal autonumsuc As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_CORTE
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@entrega", entrega)
            strCommand.Parameters.AddWithValue("@numero", numero)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@total", total)
            strCommand.Parameters.AddWithValue("@tipocambio", tipocambio)
            strCommand.Parameters.AddWithValue("@porcomision", porcomision)
            strCommand.Parameters.AddWithValue("@poriva", poriva)
            strCommand.Parameters.AddWithValue("@comision", comision)
            strCommand.Parameters.AddWithValue("@iva", iva)
            strCommand.Parameters.AddWithValue("@totcomision", totcomision)
            strCommand.Parameters.AddWithValue("@totiva", totiva)
            strCommand.Parameters.AddWithValue("@neto", neto)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@cobrador", cobrador)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@movto_teso", movto_teso)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Desglose(ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal autonumsuc As String _
, ByVal caja As String _
, ByVal corte As Integer _
, ByVal cajero As String _
, ByVal tipo As String _
, ByVal moneda As String _
, ByVal referencia As String _
, ByVal tcambio As Double _
, ByVal importe As Double _
, ByVal estatus As String _
, ByVal movimiento As String _
, ByVal autorizacion As String _
, ByVal Id_pventa As String _
, ByVal renglon_detventas As Integer _
, ByVal tipocredito As String _
, ByVal vendedor2 As String _
, ByVal id_clientefinal As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_DESGLOSE
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@tcambio", tcambio)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autorizacion", autorizacion)
            strCommand.Parameters.AddWithValue("@Id_pventa", Id_pventa)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@referencia", referencia)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@renglon_detventas", renglon_detventas)
            strCommand.Parameters.AddWithValue("@tipocredito", tipocredito)
            strCommand.Parameters.AddWithValue("@vendedor2", vendedor2)
            strCommand.Parameters.AddWithValue("@id_clientefinal", id_clientefinal)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_DetVentas(ByVal renglon As Integer _
, ByVal precionormal As Double _
, ByVal precioventa As Double _
, ByVal preciofinal As Double _
, ByVal descuento As Double _
, ByVal impiva As Double _
, ByVal impcanc As Double _
, ByVal preciopublico As Double _
, ByVal financiamiento As Double _
, ByVal totimpuesto0 As Double _
, ByVal totimpuesto1 As Double _
, ByVal totimpuesto2 As Double _
, ByVal ivacanc As Double _
, ByVal descanc As Double _
, ByVal impdev As Double _
, ByVal ivadev As Double _
, ByVal desdev As Double _
, ByVal preciocontable As Double _
, ByVal fecha As Date _
, ByVal transaccion As Double _
, ByVal cantidad As Double _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal tipocredito As String _
, ByVal concepto As String _
, ByVal vendedor As String _
, ByVal lcredito As String _
, ByVal tiponc As String _
, ByVal referencia As String _
, ByVal movimiento As String _
, ByVal porimpuesto1 As String _
, ByVal porimpuesto2 As String _
, ByVal porimpuesto3 As String _
, ByVal id_sucursal As String _
, ByVal articulo As String _
, ByVal division As String _
, ByVal depto As String _
, ByVal familia As String _
, ByVal marca As String _
, ByVal nombre As String _
, ByVal iva As String _
, ByVal tienda As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_DETVENTAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@renglon", renglon)
            strCommand.Parameters.AddWithValue("@precionormal", precionormal)
            strCommand.Parameters.AddWithValue("@precioventa", precioventa)
            strCommand.Parameters.AddWithValue("@preciofinal", preciofinal)
            strCommand.Parameters.AddWithValue("@descuento", descuento)
            strCommand.Parameters.AddWithValue("@impiva", impiva)
            strCommand.Parameters.AddWithValue("@impcanc", impcanc)
            strCommand.Parameters.AddWithValue("@preciopublico", preciopublico)
            strCommand.Parameters.AddWithValue("@financiamiento", financiamiento)
            strCommand.Parameters.AddWithValue("@totimpuesto0", totimpuesto0)
            strCommand.Parameters.AddWithValue("@totimpuesto1", totimpuesto1)
            strCommand.Parameters.AddWithValue("@totimpuesto2", totimpuesto2)
            strCommand.Parameters.AddWithValue("@ivacanc", ivacanc)
            strCommand.Parameters.AddWithValue("@descanc", descanc)
            strCommand.Parameters.AddWithValue("@impdev", impdev)
            strCommand.Parameters.AddWithValue("@ivadev", ivadev)
            strCommand.Parameters.AddWithValue("@desdev", desdev)
            strCommand.Parameters.AddWithValue("@preciocontable", preciocontable)
            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@transaccion", transaccion)
            strCommand.Parameters.AddWithValue("@cantidad", cantidad)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@tipocredito", tipocredito)
            strCommand.Parameters.AddWithValue("@concepto", concepto)
            strCommand.Parameters.AddWithValue("@vendedor", vendedor)
            strCommand.Parameters.AddWithValue("@lcredito", lcredito)
            strCommand.Parameters.AddWithValue("@tiponc", tiponc)
            strCommand.Parameters.AddWithValue("@referencia", referencia)
            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@porimpuesto1", porimpuesto1)
            strCommand.Parameters.AddWithValue("@porimpuesto2", porimpuesto2)
            strCommand.Parameters.AddWithValue("@porimpuesto3", porimpuesto3)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@articulo", articulo)
            strCommand.Parameters.AddWithValue("@division", division)
            strCommand.Parameters.AddWithValue("@depto", depto)
            strCommand.Parameters.AddWithValue("@familia", familia)
            strCommand.Parameters.AddWithValue("@marca", marca)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@iva", iva)
            strCommand.Parameters.AddWithValue("@tienda", tienda)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_DetVentasPromociones(ByVal renglon As Integer _
, ByVal valor As Double _
, ByVal fum As Date _
, ByVal transaccion As Double _
, ByVal id_empresa As String _
, ByVal articulo As String _
, ByVal tipo As String _
, ByVal flujo As String _
, ByVal descripcion As String _
, ByVal id_usuario As String _
, ByVal autonumsuc As String _
, ByVal id_sucursal As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_DETVENTASPROMOCIONES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@renglon", renglon)
            strCommand.Parameters.AddWithValue("@valor", valor)
            strCommand.Parameters.AddWithValue("@fum", fum)
            strCommand.Parameters.AddWithValue("@transaccion", transaccion)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@articulo", articulo)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@flujo", flujo)
            strCommand.Parameters.AddWithValue("@descripcion", descripcion)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_EntregaDinero(ByVal movimiento As Integer _
, ByVal entrega As Integer _
, ByVal corte As Integer _
, ByVal numero As Integer _
, ByVal importe As Double _
, ByVal total As Double _
, ByVal tipocambio As Double _
, ByVal porcomision As Double _
, ByVal poriva As Double _
, ByVal comision As Double _
, ByVal iva As Double _
, ByVal totcomision As Double _
, ByVal totiva As Double _
, ByVal neto As Double _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal id_pventa As String _
, ByVal caja As String _
, ByVal cajero As String _
, ByVal moneda As String _
, ByVal cobrador As String _
, ByVal id_usuario As String _
, ByVal movto_teso As String _
, ByVal autonumsuc As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_ENTREGADINERO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@entrega", entrega)
            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@numero", numero)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@total", total)
            strCommand.Parameters.AddWithValue("@tipocambio", tipocambio)
            strCommand.Parameters.AddWithValue("@porcomision", porcomision)
            strCommand.Parameters.AddWithValue("@poriva", poriva)
            strCommand.Parameters.AddWithValue("@comision", comision)
            strCommand.Parameters.AddWithValue("@iva", iva)
            strCommand.Parameters.AddWithValue("@totcomision", totcomision)
            strCommand.Parameters.AddWithValue("@totiva", totiva)
            strCommand.Parameters.AddWithValue("@neto", neto)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@cobrador", cobrador)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@movto_teso", movto_teso)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Existencias(ByVal nivelcero As Integer _
, ByVal costoultimo As Double _
, ByVal costoprom As Double _
, ByVal saldocosto As Double _
, ByVal minimo As Double _
, ByVal maximo As Double _
, ByVal reorden As Double _
, ByVal costopromant As Double _
, ByVal fecentrega As Date _
, ByVal ultentrada As Date _
, ByVal ultsalida As Date _
, ByVal fechacambio As Date _
, ByVal existencia As Double _
, ByVal disponible As Double _
, ByVal apartado As Double _
, ByVal separado As Double _
, ByVal pedtransit As Double _
, ByVal entanual As Double _
, ByVal salanual As Double _
, ByVal pedprov As Double _
, ByVal cantidadbase As Double _
, ByVal articulo As String _
, ByVal ubicacion As String _
, ByVal resurtir As String _
, ByVal id_empresa As String _
, ByVal tienda As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_EXISTENCIAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@nivelcero", nivelcero)
            strCommand.Parameters.AddWithValue("@costoultimo", costoultimo)
            strCommand.Parameters.AddWithValue("@costoprom", costoprom)
            strCommand.Parameters.AddWithValue("@saldocosto", saldocosto)
            strCommand.Parameters.AddWithValue("@minimo", minimo)
            strCommand.Parameters.AddWithValue("@maximo", maximo)
            strCommand.Parameters.AddWithValue("@reorden", reorden)
            strCommand.Parameters.AddWithValue("@costopromant", costopromant)
            strCommand.Parameters.AddWithValue("@fecentrega", fecentrega)
            strCommand.Parameters.AddWithValue("@ultentrada", ultentrada)
            strCommand.Parameters.AddWithValue("@ultsalida", ultsalida)
            strCommand.Parameters.AddWithValue("@fechacambio", fechacambio)
            strCommand.Parameters.AddWithValue("@existencia", existencia)
            strCommand.Parameters.AddWithValue("@disponible", disponible)
            strCommand.Parameters.AddWithValue("@apartado", apartado)
            strCommand.Parameters.AddWithValue("@separado", separado)
            strCommand.Parameters.AddWithValue("@pedtransit", pedtransit)
            strCommand.Parameters.AddWithValue("@entanual", entanual)
            strCommand.Parameters.AddWithValue("@salanual", salanual)
            strCommand.Parameters.AddWithValue("@pedprov", pedprov)
            strCommand.Parameters.AddWithValue("@cantidadbase", cantidadbase)
            strCommand.Parameters.AddWithValue("@articulo", articulo)
            strCommand.Parameters.AddWithValue("@ubicacion", ubicacion)
            strCommand.Parameters.AddWithValue("@resurtir", resurtir)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@tienda", tienda)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_MovtosInvent(ByVal combustible As Integer _
, ByVal id_cuenta As Integer _
, ByVal folio As Integer _
, ByVal proveedor As Integer _
, ByVal bultos As Integer _
, ByVal bultosrecibidos As Integer _
, ByVal renglones As Integer _
, ByVal numart As Integer _
, ByVal cantart As Integer _
, ByVal plazo As Integer _
, ByVal numpiezas As Integer _
, ByVal totalfactura As Double _
, ByVal tcambio As Double _
, ByVal flete As Double _
, ByVal costo As Double _
, ByVal venta As Double _
, ByVal ivacosto As Double _
, ByVal iva3 As Double _
, ByVal importe4 As Double _
, ByVal iva4 As Double _
, ByVal importe5 As Double _
, ByVal iva5 As Double _
, ByVal ieps_importe As Double _
, ByVal importe1 As Double _
, ByVal iva1 As Double _
, ByVal importe2 As Double _
, ByVal iva2 As Double _
, ByVal importe3 As Double _
, ByVal ivaventa As Double _
, ByVal maniobras As Double _
, ByVal seguros As Double _
, ByVal iva As Double _
, ByVal retencion As Double _
, ByVal folio_aclaracion As Double _
, ByVal fecha As Date _
, ByVal fectalon As Date _
, ByVal fecbultos As Date _
, ByVal fechafactura As Date _
, ByVal fechafrontera As Date _
, ByVal fechacaptura As Date _
, ByVal fum As Date _
, ByVal fechavence As Date _
, ByVal id_empresa As String _
, ByVal movimiento As String _
, ByVal tipo As String _
, ByVal tipofactura As String _
, ByVal numerofactura As String _
, ByVal claveflete As String _
, ByVal pasivo As String _
, ByVal crf As String _
, ByVal formapago As String _
, ByVal movto_teso As String _
, ByVal poliza As String _
, ByVal centrocosto As String _
, ByVal unidadcosto As String _
, ByVal fechabase As String _
, ByVal diapago As String _
, ByVal prorrateo As String _
, ByVal id_sucursal1 As String _
, ByVal cve_sucursal As String _
, ByVal cve_sucursal1 As String _
, ByVal movimiento1 As String _
, ByVal autonumsuc As String _
, ByVal referencia As String _
, ByVal pedido1 As String _
, ByVal pedido2 As String _
, ByVal pedido3 As String _
, ByVal pedido4 As String _
, ByVal pedido5 As String _
, ByVal id_sucursal As String _
, ByVal transporte As String _
, ByVal chofer As String _
, ByVal coment1 As String _
, ByVal coment2 As String _
, ByVal estatus As String _
, ByVal id_usuario As String _
, ByVal talon As String _
, ByVal depto As String _
, ByVal inventario As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_MOVTOSINVENT
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@combustible", combustible)
            strCommand.Parameters.AddWithValue("@id_cuenta", id_cuenta)
            strCommand.Parameters.AddWithValue("@folio", folio)
            strCommand.Parameters.AddWithValue("@proveedor", proveedor)
            strCommand.Parameters.AddWithValue("@bultos", bultos)
            strCommand.Parameters.AddWithValue("@bultosrecibidos", bultosrecibidos)
            strCommand.Parameters.AddWithValue("@renglones", renglones)
            strCommand.Parameters.AddWithValue("@numart", numart)
            strCommand.Parameters.AddWithValue("@cantart", cantart)
            strCommand.Parameters.AddWithValue("@plazo", plazo)
            strCommand.Parameters.AddWithValue("@numpiezas", numpiezas)
            strCommand.Parameters.AddWithValue("@totalfactura", totalfactura)
            strCommand.Parameters.AddWithValue("@tcambio", tcambio)
            strCommand.Parameters.AddWithValue("@flete", flete)
            strCommand.Parameters.AddWithValue("@costo", costo)
            strCommand.Parameters.AddWithValue("@venta", venta)
            strCommand.Parameters.AddWithValue("@ivacosto", ivacosto)
            strCommand.Parameters.AddWithValue("@iva3", iva3)
            strCommand.Parameters.AddWithValue("@importe4", importe4)
            strCommand.Parameters.AddWithValue("@iva4", iva4)
            strCommand.Parameters.AddWithValue("@importe5", importe5)
            strCommand.Parameters.AddWithValue("@iva5", iva5)
            strCommand.Parameters.AddWithValue("@ieps_importe", ieps_importe)
            strCommand.Parameters.AddWithValue("@importe1", importe1)
            strCommand.Parameters.AddWithValue("@iva1", iva1)
            strCommand.Parameters.AddWithValue("@importe2", importe2)
            strCommand.Parameters.AddWithValue("@iva2", iva2)
            strCommand.Parameters.AddWithValue("@importe3", importe3)
            strCommand.Parameters.AddWithValue("@ivaventa", ivaventa)
            strCommand.Parameters.AddWithValue("@maniobras", maniobras)
            strCommand.Parameters.AddWithValue("@seguros", seguros)
            strCommand.Parameters.AddWithValue("@iva", iva)
            strCommand.Parameters.AddWithValue("@retencion", retencion)
            strCommand.Parameters.AddWithValue("@folio_aclaracion", folio_aclaracion)
            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@fectalon", fectalon)
            strCommand.Parameters.AddWithValue("@fecbultos", fecbultos)
            strCommand.Parameters.AddWithValue("@fechafactura", fechafactura)
            strCommand.Parameters.AddWithValue("@fechafrontera", fechafrontera)
            strCommand.Parameters.AddWithValue("@fechacaptura", fechacaptura)
            strCommand.Parameters.AddWithValue("@fum", fum)
            strCommand.Parameters.AddWithValue("@fechavence", fechavence)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@tipofactura", tipofactura)
            strCommand.Parameters.AddWithValue("@numerofactura", numerofactura)
            strCommand.Parameters.AddWithValue("@claveflete", claveflete)
            strCommand.Parameters.AddWithValue("@pasivo", pasivo)
            strCommand.Parameters.AddWithValue("@crf", crf)
            strCommand.Parameters.AddWithValue("@formapago", formapago)
            strCommand.Parameters.AddWithValue("@movto_teso", movto_teso)
            strCommand.Parameters.AddWithValue("@poliza", poliza)
            strCommand.Parameters.AddWithValue("@centrocosto", centrocosto)
            strCommand.Parameters.AddWithValue("@unidadcosto", unidadcosto)
            strCommand.Parameters.AddWithValue("@fechabase", fechabase)
            strCommand.Parameters.AddWithValue("@diapago", diapago)
            strCommand.Parameters.AddWithValue("@prorrateo", prorrateo)
            strCommand.Parameters.AddWithValue("@id_sucursal1", id_sucursal1)
            strCommand.Parameters.AddWithValue("@cve_sucursal", cve_sucursal)
            strCommand.Parameters.AddWithValue("@cve_sucursal1", cve_sucursal1)
            strCommand.Parameters.AddWithValue("@movimiento1", movimiento1)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@referencia", referencia)
            strCommand.Parameters.AddWithValue("@pedido1", pedido1)
            strCommand.Parameters.AddWithValue("@pedido2", pedido2)
            strCommand.Parameters.AddWithValue("@pedido3", pedido3)
            strCommand.Parameters.AddWithValue("@pedido4", pedido4)
            strCommand.Parameters.AddWithValue("@pedido5", pedido5)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@transporte", transporte)
            strCommand.Parameters.AddWithValue("@chofer", chofer)
            strCommand.Parameters.AddWithValue("@coment1", coment1)
            strCommand.Parameters.AddWithValue("@coment2", coment2)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@talon", talon)
            strCommand.Parameters.AddWithValue("@depto", depto)
            strCommand.Parameters.AddWithValue("@inventario", inventario)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_MovtosInventDet(ByVal renglon As Integer _
, ByVal folio As Integer _
, ByVal unidades As Double _
, ByVal costofactura As Double _
, ByVal costobruto As Double _
, ByVal costoneto As Double _
, ByVal precio As Double _
, ByVal iva As Double _
, ByVal total As Double _
, ByVal flete As Double _
, ByVal msyo As Double _
, ByVal costoprom As Double _
, ByVal entrada As Double _
, ByVal salida As Double _
, ByVal debe As Double _
, ByVal haber As Double _
, ByVal gastos1 As Double _
, ByVal gastos2 As Double _
, ByVal fecha As Date _
, ByVal fum As Date _
, ByVal cantidad As Double _
, ByVal devuelta As Double _
, ByVal danada As Double _
, ByVal facturada As Double _
, ByVal pedida As Double _
, ByVal horaskm As Double _
, ByVal km_diferencia As Double _
, ByVal km_anterior As Double _
, ByVal cantidad_anterior As Double _
, ByVal orden As Double _
, ByVal id_empresa As String _
, ByVal movimiento As String _
, ByVal id_sucursal As String _
, ByVal cve_sucursal As String _
, ByVal articulo As String _
, ByVal pedido As String _
, ByVal id_empleado As String _
, ByVal responsable As String _
, ByVal unidadcosto As String _
, ByVal esactivo As String _
, ByVal activo As String _
, ByVal folio_recosteo As String _
, ByVal actividad As String _
, ByVal origen_destino As String _
, ByVal unidadcompra As String _
, ByVal id_usuario As String _
, ByVal tipo As String _
, ByVal lcredito As String _
, ByVal comentarios As String _
, ByVal centrocosto As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_MOVTOSINVENTDET
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@renglon", renglon)
            strCommand.Parameters.AddWithValue("@folio", folio)
            strCommand.Parameters.AddWithValue("@unidades", unidades)
            strCommand.Parameters.AddWithValue("@costofactura", costofactura)
            strCommand.Parameters.AddWithValue("@costobruto", costobruto)
            strCommand.Parameters.AddWithValue("@costoneto", costoneto)
            strCommand.Parameters.AddWithValue("@precio", precio)
            strCommand.Parameters.AddWithValue("@iva", iva)
            strCommand.Parameters.AddWithValue("@total", total)
            strCommand.Parameters.AddWithValue("@flete", flete)
            strCommand.Parameters.AddWithValue("@msyo", msyo)
            strCommand.Parameters.AddWithValue("@costoprom", costoprom)
            strCommand.Parameters.AddWithValue("@entrada", entrada)
            strCommand.Parameters.AddWithValue("@salida", salida)
            strCommand.Parameters.AddWithValue("@debe", debe)
            strCommand.Parameters.AddWithValue("@haber", haber)
            strCommand.Parameters.AddWithValue("@gastos1", gastos1)
            strCommand.Parameters.AddWithValue("@gastos2", gastos2)
            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@fum", fum)
            strCommand.Parameters.AddWithValue("@cantidad", cantidad)
            strCommand.Parameters.AddWithValue("@devuelta", devuelta)
            strCommand.Parameters.AddWithValue("@danada", danada)
            strCommand.Parameters.AddWithValue("@facturada", facturada)
            strCommand.Parameters.AddWithValue("@pedida", pedida)
            strCommand.Parameters.AddWithValue("@horaskm", horaskm)
            strCommand.Parameters.AddWithValue("@km_diferencia", km_diferencia)
            strCommand.Parameters.AddWithValue("@km_anterior", km_anterior)
            strCommand.Parameters.AddWithValue("@cantidad_anterior", cantidad_anterior)
            strCommand.Parameters.AddWithValue("@orden", orden)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@movimiento", movimiento)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@cve_sucursal", cve_sucursal)
            strCommand.Parameters.AddWithValue("@articulo", articulo)
            strCommand.Parameters.AddWithValue("@pedido", pedido)
            strCommand.Parameters.AddWithValue("@id_empleado", id_empleado)
            strCommand.Parameters.AddWithValue("@responsable", responsable)
            strCommand.Parameters.AddWithValue("@unidadcosto", unidadcosto)
            strCommand.Parameters.AddWithValue("@esactivo", esactivo)
            strCommand.Parameters.AddWithValue("@activo", activo)
            strCommand.Parameters.AddWithValue("@folio_recosteo", folio_recosteo)
            strCommand.Parameters.AddWithValue("@actividad", actividad)
            strCommand.Parameters.AddWithValue("@origen_destino", origen_destino)
            strCommand.Parameters.AddWithValue("@unidadcompra", unidadcompra)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@lcredito", lcredito)
            strCommand.Parameters.AddWithValue("@comentarios", comentarios)
            strCommand.Parameters.AddWithValue("@centrocosto", centrocosto)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Transaccion(ByRef autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
, ByVal id_pventa As String _
, ByVal fecha As Date _
, ByVal hora As String _
, ByVal caja As String _
, ByVal corte As Integer _
, ByVal cajero As String _
, ByVal tipo As String _
, ByVal pedido As String _
, ByVal vendedor As String _
, ByVal cuenta As String _
, ByVal credito As String _
, ByVal entrada As Double _
, ByVal salida As Double _
, ByVal referencia As String _
, ByVal nombre As String _
, ByVal id_usuario As String _
, ByVal impiva As Double _
, ByVal totalcontado As Double _
, ByVal totalcredito As Double _
, ByVal factura As Double _
, ByVal documentos As String _
, ByVal estatus As String _
, ByVal tipotrans As String _
, ByVal dir1 As String _
, ByVal dir2 As String _
, ByVal dir3 As String _
, ByVal importe As Double _
, ByVal impuesto0 As Double _
, ByVal impuesto1 As Double _
, ByVal impuesto2 As Double _
, ByVal porimpuesto0 As Double _
, ByVal porimpuesto1 As Double _
, ByVal porimpuesto2 As Double _
, ByVal com_banco As Double _
, ByVal ivacom_banco As Double _
, ByVal com_empresa As Double _
, ByVal ivacom_empresa As Double _
, ByVal ticket1 As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_TRANSACCION
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@entrada", entrada)
            strCommand.Parameters.AddWithValue("@salida", salida)
            strCommand.Parameters.AddWithValue("@impiva", impiva)
            strCommand.Parameters.AddWithValue("@totalcontado", totalcontado)
            strCommand.Parameters.AddWithValue("@totalcredito", totalcredito)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@com_banco", com_banco)
            strCommand.Parameters.AddWithValue("@ivacom_banco", ivacom_banco)
            strCommand.Parameters.AddWithValue("@com_empresa", com_empresa)
            strCommand.Parameters.AddWithValue("@ivacom_empresa", ivacom_empresa)
            strCommand.Parameters.AddWithValue("@impuesto0", impuesto0)
            strCommand.Parameters.AddWithValue("@impuesto1", impuesto1)
            strCommand.Parameters.AddWithValue("@impuesto2", impuesto2)
            strCommand.Parameters.AddWithValue("@porimpuesto0", porimpuesto0)
            strCommand.Parameters.AddWithValue("@porimpuesto1", porimpuesto1)
            strCommand.Parameters.AddWithValue("@porimpuesto2", porimpuesto2)
            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@factura", factura)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@hora", hora)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@dir1", dir1)
            strCommand.Parameters.AddWithValue("@dir2", dir2)
            strCommand.Parameters.AddWithValue("@dir3", dir3)
            strCommand.Parameters.AddWithValue("@ticket1", ticket1)
            strCommand.Parameters.AddWithValue("@referencia", referencia)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@documentos", documentos)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@tipotrans", tipotrans)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@pedido", pedido)
            strCommand.Parameters.AddWithValue("@vendedor", vendedor)
            strCommand.Parameters.AddWithValue("@cuenta", cuenta)
            strCommand.Parameters.AddWithValue("@credito", credito)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    autonumsuc = dtDatos.Rows(0).Item("autonumsuc")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_TransaccionesClientes(ByVal id_cliente As Integer _
, ByVal puntos_entrada As Double _
, ByVal puntos_salida As Double _
, ByVal puntos_saldo As Double _
, ByVal dineroe_entrada As Double _
, ByVal dineroe_salida As Double _
, ByVal dineroe_saldo As Double _
, ByVal fum As Date _
, ByVal transaccion As Double _
, ByVal autonumsuc As String _
, ByVal id_sucursal As String _
, ByVal id_empresa As String _
, ByVal folio As String _
, ByVal nombre As String _
, ByVal id_usuario As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_TRANSACCIONESCLIENTES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@puntos_entrada", puntos_entrada)
            strCommand.Parameters.AddWithValue("@puntos_salida", puntos_salida)
            strCommand.Parameters.AddWithValue("@puntos_saldo", puntos_saldo)
            strCommand.Parameters.AddWithValue("@dineroe_entrada", dineroe_entrada)
            strCommand.Parameters.AddWithValue("@dineroe_salida", dineroe_salida)
            strCommand.Parameters.AddWithValue("@dineroe_saldo", dineroe_saldo)
            strCommand.Parameters.AddWithValue("@fum", fum)
            strCommand.Parameters.AddWithValue("@transaccion", transaccion)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@folio", folio)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_DetVentas_Articulo(ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc As String _
                                                    , ByVal codigo As String _
                                                    , ByVal separador As String _
                                                    , ByVal vendedor As String _
                                                    , ByVal lcredito As String _
                                                    , ByVal financiamiento As Double _
                                                    , ByVal tipocredito As String _
                                                    , ByRef renglon As String _
                                                    , ByRef articulo As String _
                                                    , ByRef nombre As String _
                                                    , ByRef cantidad As Integer _
                                                    , ByRef preciofinal As Double _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_DETVENTAS_ARTICULO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@codigo", codigo)
            strCommand.Parameters.AddWithValue("@separador", separador)
            strCommand.Parameters.AddWithValue("@vendedor", vendedor)
            strCommand.Parameters.AddWithValue("@lcredito", lcredito)
            strCommand.Parameters.AddWithValue("@financiamiento", financiamiento)
            strCommand.Parameters.AddWithValue("@tipocredito", tipocredito)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    autonumsuc = dtDatos.Rows(0).Item("autonumsuc")
                    articulo = dtDatos.Rows(0).Item("articulo")
                    nombre = dtDatos.Rows(0).Item("nombre")
                    cantidad = dtDatos.Rows(0).Item("cantidad")
                    preciofinal = dtDatos.Rows(0).Item("preciofinal")
                    renglon = dtDatos.Rows(0).Item("renglon")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Transacciones_Estatus(ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc As String _
                                                    , ByVal estatus As String _
                                                    , ByRef autonumsuc1 As String _
                                                    , ByRef autonumsuc2 As String _
                                                    , ByRef autonumsuc3 As String _
                                                    , ByRef autonumsuc4 As String _
                                                    , ByRef autonumsuc5 As String _
                                                    , ByRef autonumsuc6 As String _
                                                    , ByRef autonumsuc7 As String _
                                                    , ByRef despedida As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_TRANSACCIONES_ESTATUS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    autonumsuc1 = dtDatos.Rows(0).Item("autonumsuc1")
                    autonumsuc2 = dtDatos.Rows(0).Item("autonumsuc2")
                    autonumsuc3 = dtDatos.Rows(0).Item("autonumsuc3")
                    autonumsuc4 = dtDatos.Rows(0).Item("autonumsuc4")
                    autonumsuc5 = dtDatos.Rows(0).Item("autonumsuc5")
                    autonumsuc6 = dtDatos.Rows(0).Item("autonumsuc6")
                    autonumsuc7 = dtDatos.Rows(0).Item("autonumsuc7")
                    despedida = dtDatos.Rows(0).Item("despedida")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Vendedores(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal id_vendedor As String _
                                , ByVal estatus As String _
                                , ByVal tipo As String _
                                , ByVal sw_blanco As Boolean _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_VENDEDORES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_vendedor", id_vendedor)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@sw_blanco", sw_blanco)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Elimina_DetVentas(ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc As String _
                                                    , ByVal articulo As String _
                                                    , ByVal renglon As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_ELIMINA_DETVENTAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@articulo", articulo)
            strCommand.Parameters.AddWithValue("@renglon", renglon)
            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_TmpTransacciones(ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc_ori As String _
                                                    , ByVal autonumsuc_des As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_TMPTRANSACCIONES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc_ori", autonumsuc_ori)
            strCommand.Parameters.AddWithValue("@autonumsuc_des", autonumsuc_des)
            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_TCambios(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByRef dtDatos As DataTable _
                                , ByRef Moneda_Nacional As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_TCAMBIOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                Moneda_Nacional = ""
                If dts.Tables(1).Rows.Count > 0 Then
                    Moneda_Nacional = dts.Tables(1).Rows(0).Item("moneda_nacional")
                End If
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_entregas_denominacion(ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_ENTREGAS_DENOMINACION
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            ''strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            ''strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            ''strCommand.Parameters.AddWithValue("@id_vendedor", id_vendedor)
            ''strCommand.Parameters.AddWithValue("@estatus", estatus)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_entregas(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal id_pventa As String _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal cobrador As String _
                                , ByVal corte As String _
                                , ByVal moneda As String _
                                , ByVal tipocambio As Double _
                                , ByVal id_usuario As String _
                                , ByVal tipo_corte As String _
                                , ByVal detalle As DataTable _
                                , ByVal concepto As String _
                                , ByRef NoCorte As Integer _
                                , ByRef Movimiento As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_ENTREGAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_pventa", id_pventa)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@cobrador", cobrador)
            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@tipocambio", tipocambio)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@tipo_corte", tipo_corte)
            strCommand.Parameters.AddWithValue("@detalle", detalle)
            strCommand.Parameters.AddWithValue("@concepto", concepto)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            NoCorte = 0
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    Movimiento = dtDatos.Rows(0).Item("movimiento")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    NoCorte = 0
                End If
                NoCorte = dtDatos.Rows(0).Item("corte")
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_DetVentas_Descuentos(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal sw_preciodirecto As Boolean _
                                , ByVal descto_global As Double _
                                , ByVal renglon As Integer _
                                , ByVal descto As Double _
                                , ByVal precio As Double _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_DETVENTAS_DESCUENTOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@sw_preciodirecto", sw_preciodirecto)
            strCommand.Parameters.AddWithValue("@descto_global", descto_global)
            strCommand.Parameters.AddWithValue("@renglon", renglon)
            strCommand.Parameters.AddWithValue("@descto", descto)
            strCommand.Parameters.AddWithValue("@precio", precio)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Inserta_DetVentas_Cotizacion(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal quincenas As Double _
                                , ByVal precio As Double _
                                , ByVal articulo As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_DETVENTAS_COTIZACION
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@quincenas", quincenas)
            strCommand.Parameters.AddWithValue("@precio", precio)
            strCommand.Parameters.AddWithValue("@articulo", articulo)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Tmp_Transacciones(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal fecha_ini As Date _
                                , ByVal fecha_fin As Date _
                                , ByVal autonumsuc As String _
                                , ByVal tipo As String _
                                , ByVal estatus As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_TMP_TRANSACCIONES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@fecha_ini", fecha_ini)
            strCommand.Parameters.AddWithValue("@fecha_fin", fecha_fin)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@estatus", estatus)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Cotizaciones(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal articulo As String _
                                , ByVal tipo As String _
                                , ByVal cliente As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_COTIZACIONES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@articulo", articulo)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@cliente", cliente)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Empleados(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal id_empleado As String _
                                , ByVal nombre As String _
                                , ByVal estatus As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_EMPLEADOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_empleado", id_empleado)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@estatus", estatus)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Cuentas(ByVal cuenta As String _
                                , ByVal tarjeta As String _
                                , ByVal empresa As String _
                                , ByVal nombre As String _
                                , ByVal rfc As String _
                                , ByVal estatus As String _
                                , ByVal tipo As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CUENTAS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@cuenta", cuenta)
            strCommand.Parameters.AddWithValue("@tarjeta", tarjeta)
            strCommand.Parameters.AddWithValue("@empresa", empresa)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@rfc", rfc)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@tipo", tipo)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_CuentasFinal(ByVal cuenta As String _
                                , ByVal tarjeta As String _
                                , ByVal empresa As String _
                                , ByVal nombre As String _
                                , ByVal rfc As String _
                                , ByVal estatus As String _
                                , ByVal tipo As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CUENTASFINAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@cuenta", cuenta)
            strCommand.Parameters.AddWithValue("@tarjeta", tarjeta)
            strCommand.Parameters.AddWithValue("@empresa", empresa)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@rfc", rfc)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@tipo", tipo)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_cliedocta(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal cuenta As String _
                                , ByVal fecha_hoy As Date _
                                , ByVal codigo As String _
                                , ByVal sw_todo As Boolean _
                                , ByVal desembolso As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CLIEDOCTA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@cuenta", cuenta)
            strCommand.Parameters.AddWithValue("@fecha_hoy", fecha_hoy)
            strCommand.Parameters.AddWithValue("@codigo", codigo)
            strCommand.Parameters.AddWithValue("@sw_todo", sw_todo)
            strCommand.Parameters.AddWithValue("@desembolso", desembolso)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Pago_cliedocta(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal cuenta As String _
                                , ByVal autonumsuc As String _
                                , ByVal moneda As String _
                                , ByVal importe As Double _
                                , ByVal tcambio As Double _
                                , ByVal id_usuario As String _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal fecha_hoy As Date _
                                , ByVal FormaPago As String _
                                , ByRef as_pago As String _
                                , ByRef as_credito As String _
                                , ByRef codigo As String _
                                , ByRef tipo As String _
                                , ByRef plazo As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_PAGO_CLIEDOCTA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@cuenta", cuenta)
            strCommand.Parameters.AddWithValue("@autonumsucs", autonumsuc)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@tcambio", tcambio)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@fecha_hoy", fecha_hoy)
            strCommand.Parameters.AddWithValue("@forma_pago", FormaPago)
            strCommand.Parameters.AddWithValue("@codigo", codigo)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@plazo", plazo)
            strCommand.CommandTimeout = 0

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            as_pago = ""
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    as_pago = dtDatos.Rows(0).Item("autonumsuc")
                    as_credito = dtDatos.Rows(0).Item("autonumsuc1")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Recupera_cliedocta_Final(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal cuenta As String _
                                , ByVal id_clientefinal As String _
                                , ByVal fecha_hoy As Date _
                                , ByVal codigo As String _
                                , ByVal forma_pago As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CLIEDOCTA_FINAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@cuenta", cuenta)
            strCommand.Parameters.AddWithValue("@id_clientefinal", id_clientefinal)
            strCommand.Parameters.AddWithValue("@fecha_hoy", fecha_hoy)
            strCommand.Parameters.AddWithValue("@codigo", codigo)
            strCommand.Parameters.AddWithValue("@forma_pago", forma_pago)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_Pago_cliedocta_Final(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal cuenta As String _
                                , ByVal id_clientefinal As String _
                                , ByVal autonumsuc As String _
                                , ByVal moneda As String _
                                , ByVal importe As Double _
                                , ByVal tcambio As Double _
                                , ByVal id_usuario As String _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal fecha_hoy As Date _
                                , ByVal FormaPago As String _
                                , ByRef as_pago As String _
                                , ByRef as_credito As String _
                                , ByRef codigo As String _
                                , ByRef tipo As String _
                                , ByRef plazo As String _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_PAGO_CLIEDOCTA_FINAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@cuenta", cuenta)
            strCommand.Parameters.AddWithValue("@id_clientefinal", id_clientefinal)
            strCommand.Parameters.AddWithValue("@autonumsucs", autonumsuc)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@tcambio", tcambio)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@fecha_hoy", fecha_hoy)
            strCommand.Parameters.AddWithValue("@forma_pago", FormaPago)
            strCommand.Parameters.AddWithValue("@codigo", codigo)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@plazo", plazo)
            strCommand.CommandTimeout = 0

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            as_pago = ""
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    as_pago = dtDatos.Rows(0).Item("autonumsuc")
                    as_credito = dtDatos.Rows(0).Item("autonumsuc1")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PV_Recupera_CorteCaja(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal corte As String _
                                , ByVal fecha As Date _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PV_RECUPERA_CORTECAJA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@fecha", fecha)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PV_Recupera_EntregaParcialCaja(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByVal corte As String _
                                , ByVal fecha As Date _
                                , ByVal movimiento As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PV_RECUPERA_ENTREGAPARCIALCAJA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)
            strCommand.Parameters.AddWithValue("@corte", corte)
            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@movimiento", movimiento)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Bancos(ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_BANCOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            'strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            'strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            'strCommand.Parameters.AddWithValue("@caja", caja)
            'strCommand.Parameters.AddWithValue("@cajero", cajero)
            'strCommand.Parameters.AddWithValue("@corte", corte)
            'strCommand.Parameters.AddWithValue("@fecha", fecha)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Corte_Estatus(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal caja As String _
                                , ByVal cajero As String _
                                , ByRef corte As Integer _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable = Nothing

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CORTE_ESTATUS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@cajero", cajero)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Resultado = False
            Excepcion = "NO_ERROR"
            corte = 0
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    corte = dtDatos.Rows(0).Item("corte")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_SatUsoCDFI(ByVal regimen As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_SATUSOCDFI
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@regimen", regimen)
            'strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            'strCommand.Parameters.AddWithValue("@caja", caja)
            'strCommand.Parameters.AddWithValue("@cajero", cajero)
            'strCommand.Parameters.AddWithValue("@corte", corte)
            'strCommand.Parameters.AddWithValue("@fecha", fecha)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_SatFormaPago(ByVal regimen As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_SATFORMAPAGO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@regimen", regimen)
            'strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            'strCommand.Parameters.AddWithValue("@caja", caja)
            'strCommand.Parameters.AddWithValue("@cajero", cajero)
            'strCommand.Parameters.AddWithValue("@corte", corte)
            'strCommand.Parameters.AddWithValue("@fecha", fecha)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_SatInfo(ByVal Id_empresa As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_SATINFO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", Id_empresa)
            'strCommand.Parameters.AddWithValue("@fecha", fecha)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_SatRegimenFiscal(ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_SATREGIMENFISCAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            'strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            'strCommand.Parameters.AddWithValue("@caja", caja)
            'strCommand.Parameters.AddWithValue("@cajero", cajero)
            'strCommand.Parameters.AddWithValue("@corte", corte)
            'strCommand.Parameters.AddWithValue("@fecha", fecha)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_SatRegimenFiscal40(ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_SATREGIMENFISCAL40
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            'strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            'strCommand.Parameters.AddWithValue("@caja", caja)
            'strCommand.Parameters.AddWithValue("@cajero", cajero)
            'strCommand.Parameters.AddWithValue("@corte", corte)
            'strCommand.Parameters.AddWithValue("@fecha", fecha)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_CuentasLugarEntrega(ByVal id_empresa As String _
                                , ByVal cuenta As String _
                                , ByVal id_dir As Integer _
                                , ByVal direccion As String _
                                , ByVal numext As String _
                                , ByVal numint As String _
                                , ByVal colonia As String _
                                , ByVal codigo_postal As String _
                                , ByVal municipio As String _
                                , ByVal estado As String _
                                , ByRef IdDir As Integer _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_CUENTASLUGARENTREGA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)

            strCommand.Parameters.AddWithValue("cuenta", cuenta)
            strCommand.Parameters.AddWithValue("id_dir", id_dir)
            strCommand.Parameters.AddWithValue("direccion", direccion)
            strCommand.Parameters.AddWithValue("numext", numext)
            strCommand.Parameters.AddWithValue("numint", numint)
            strCommand.Parameters.AddWithValue("colonia", colonia)
            strCommand.Parameters.AddWithValue("codigo_postal", codigo_postal)
            strCommand.Parameters.AddWithValue("municipio", municipio)
            strCommand.Parameters.AddWithValue("estado", estado)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    id_dir = dtDatos.Rows(0).Item("id_dir")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Recupera_CuentasLugarEntrega(ByVal Id_empresa As String _
                                                , ByVal cuenta As String _
                                                , ByRef dtDatos As DataTable _
                                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_RECUPERA_CUENTASLUGARENTREGA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", Id_empresa)
            strCommand.Parameters.AddWithValue("@cuenta", cuenta)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_TicketFactura(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal serie As String _
                                , ByVal moneda As String _
                                , ByVal tcambio As String _
                                , ByVal id_usuario As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_TICKETFACTURA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("serie", serie)
            strCommand.Parameters.AddWithValue("moneda", moneda)
            strCommand.Parameters.AddWithValue("tcambio", tcambio)
            strCommand.Parameters.AddWithValue("id_usuario", id_usuario)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_FacturaGlobal(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal fecha_ini As Date _
                                , ByVal fecha_fin As Date _
                                , ByVal serie As String _
                                , ByVal moneda As String _
                                , ByVal tcambio As String _
                                , ByVal id_usuario As String _
                                , ByRef movimiento As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_FACTURAGLOBAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("fecha_ini", fecha_ini)
            strCommand.Parameters.AddWithValue("fecha_fin", fecha_fin)
            strCommand.Parameters.AddWithValue("serie", serie)
            strCommand.Parameters.AddWithValue("moneda", moneda)
            strCommand.Parameters.AddWithValue("tcambio", tcambio)
            strCommand.Parameters.AddWithValue("id_usuario", id_usuario)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    movimiento = dtDatos.Rows(0).Item("movimiento")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    movimiento = ""
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Recupera_TicketFactura(ByVal cn As System.Data.SqlClient.SqlConnection _
                                , ByVal ControlTransaccion As System.Data.SqlClient.SqlTransaction _
                                , ByVal id_empresa As String _
                                                , ByVal id_sucursal As String _
                                                , ByVal autonumsuc As String _
                                                , ByRef dtDatos As DataTable _
                                                , ByRef dtDetalle As DataTable _
                                                , ByRef dtOtros As DataTable _
                                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            If cn Is Nothing Then
                cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            End If
            strCommand.CommandText = SP_RECUPERA_TICKETFACTURA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            If Not ControlTransaccion Is Nothing Then
                strCommand.Transaction = ControlTransaccion
            End If

            If cn.State <> ConnectionState.Open Then
                cn.Open()
            End If

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                dtDetalle = dts.Tables(1)
                dtOtros = dts.Tables(2)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If ControlTransaccion Is Nothing Then
                If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                    cn.Close()
                    cn.Dispose()
                End If
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing

            If ControlTransaccion Is Nothing Then
                cn = Nothing
            End If
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_TicketFactura_DirEntrega(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal id_dir As Integer _
                                , ByVal fecha_entrega As Date _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_ACTUALIZA_TICKETFACTURA_DIRENTREGA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("id_dir", id_dir)
            strCommand.Parameters.AddWithValue("fecha_entrega", fecha_entrega)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_TicketFactura_DatosCFDI(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal regimenfiscal As String _
                                , ByVal usocfdi As String _
                                , ByVal formapago As String _
                                , ByVal folio As Integer _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_ACTUALIZA_TICKETFACTURA_DATOSCFDI
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("usocfdi", usocfdi)
            strCommand.Parameters.AddWithValue("regimenfiscal", regimenfiscal)
            strCommand.Parameters.AddWithValue("formapago", formapago)
            strCommand.Parameters.AddWithValue("folio", folio)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_Cuenta(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
        , ByRef cuenta As String _
        , ByVal tarjeta As String _
        , ByVal tipo As String _
        , ByVal estatus As String _
        , ByVal nombre As String _
        , ByVal nomcorto As String _
        , ByVal direccion As String _
        , ByVal entrecalles As String _
        , ByVal colonia As String _
        , ByVal municipio As String _
        , ByVal ciudad As String _
        , ByVal edo As String _
        , ByVal pais As String _
        , ByVal cpostal As String _
        , ByVal rfc As String _
        , ByVal nomfac As String _
        , ByVal telefono As String _
        , ByVal telefono2 As String _
        , ByVal email As String _
        , ByVal id_usuario As String _
        , ByVal descuento As String _
        , ByVal uso_cfdi As String _
        , ByVal regimen_fiscal As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_CUENTA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("cuenta", cuenta)
            strCommand.Parameters.AddWithValue("tarjeta", tarjeta)
            strCommand.Parameters.AddWithValue("tipo", tipo)
            strCommand.Parameters.AddWithValue("estatus", estatus)
            strCommand.Parameters.AddWithValue("nombre", nombre)
            strCommand.Parameters.AddWithValue("nomcorto", nomcorto)
            strCommand.Parameters.AddWithValue("direccion", direccion)
            strCommand.Parameters.AddWithValue("entrecalles", entrecalles)
            strCommand.Parameters.AddWithValue("colonia", colonia)
            strCommand.Parameters.AddWithValue("municipio", municipio)
            strCommand.Parameters.AddWithValue("ciudad", ciudad)
            strCommand.Parameters.AddWithValue("edo", edo)
            strCommand.Parameters.AddWithValue("pais", pais)
            strCommand.Parameters.AddWithValue("cpostal", cpostal)
            strCommand.Parameters.AddWithValue("rfc", rfc)
            strCommand.Parameters.AddWithValue("nomfac", nomfac)
            strCommand.Parameters.AddWithValue("telefono", telefono)
            strCommand.Parameters.AddWithValue("telefono2", telefono2)
            strCommand.Parameters.AddWithValue("email", email)
            strCommand.Parameters.AddWithValue("id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("descuento", descuento)
            strCommand.Parameters.AddWithValue("uso_cfdi", uso_cfdi)
            strCommand.Parameters.AddWithValue("regimen_fiscal", regimen_fiscal)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    cuenta = dtDatos.Rows(0).Item("cuenta")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    cuenta = ""
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_TicketFactura_Cuenta(ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc As String _
                                                    , ByVal cuenta As String _
                                                    , ByVal nombre As String _
                                                    , ByVal rfc As String _
                                                    , ByVal direccion As String _
                                                    , ByVal codigopostal As String _
                                                    , ByVal colonia As String _
                                                    , ByVal municipio As String _
                                                    , ByVal estado As String _
                                                    , ByVal correos As String _
                                                    , ByVal pedido_mov As String _
                                                    , ByVal id_plancredito As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_ACTUALIZA_TICKETFACTURA_CUENTA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("cuenta", cuenta)
            strCommand.Parameters.AddWithValue("nombre", nombre)
            strCommand.Parameters.AddWithValue("rfc", rfc)
            strCommand.Parameters.AddWithValue("direccion", direccion)
            strCommand.Parameters.AddWithValue("codigopostal", codigopostal)
            strCommand.Parameters.AddWithValue("colonia", colonia)
            strCommand.Parameters.AddWithValue("municipio", municipio)
            strCommand.Parameters.AddWithValue("estado", estado)
            strCommand.Parameters.AddWithValue("correos", correos)
            strCommand.Parameters.AddWithValue("pedido_mov", pedido_mov)
            strCommand.Parameters.AddWithValue("id_plancredito", id_plancredito)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function ACtualiza_TicketFactura_DatosCuenta(ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_ACTUALIZA_TICKETFACTURA_DATOSCUENTA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_TicketFactura_CFDI_Gen(ByVal cn As SqlConnection _
                                                    , ByVal ControlTransaccion As System.Data.SqlClient.SqlTransaction _
                                                    , ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc As String _
                                                    , ByVal email_enviado As Integer _
                                                    , ByVal estatus As String _
                                                    , ByVal subtotal As Decimal _
                                                    , ByVal total As Decimal _
                                                    , ByVal uuid As String _
                                                    , ByVal xml As String _
                                                    , ByVal noCertificadoSAT As String _
                                                    , ByVal selloCFD As String _
                                                    , ByVal selloSAT As String _
                                                    , ByVal CadenaOriginal As String _
                                                    , ByVal version As String _
                                                    , ByVal fechatimbrado As String _
                                                    , ByVal rfcprovcertif As String _
                                                    , ByVal nocertificado As String _
                                                    , ByVal periocidad As String _
                                                    , ByVal tipo_relacion As String _
                                                    , ByVal cfdi_relacionado As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            If cn Is Nothing Then
                cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            End If
            strCommand.CommandText = SP_ACTUALIZA_TICKETFACTURA_CFDI_GEN
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("email_enviado", email_enviado)
            strCommand.Parameters.AddWithValue("estatus", estatus)
            strCommand.Parameters.AddWithValue("total", total)
            strCommand.Parameters.AddWithValue("subtotal", subtotal)
            strCommand.Parameters.AddWithValue("uuid", uuid)
            strCommand.Parameters.AddWithValue("xml", xml)
            strCommand.Parameters.AddWithValue("noCertificadoSAT", noCertificadoSAT)
            strCommand.Parameters.AddWithValue("selloCFD", selloCFD)
            strCommand.Parameters.AddWithValue("selloSAT", selloSAT)
            strCommand.Parameters.AddWithValue("cadenaoriginal", CadenaOriginal)
            strCommand.Parameters.AddWithValue("version", version)
            strCommand.Parameters.AddWithValue("fechatimbrado", fechatimbrado)
            strCommand.Parameters.AddWithValue("rfcprovcertif", rfcprovcertif)
            strCommand.Parameters.AddWithValue("nocertificado", nocertificado)
            strCommand.Parameters.AddWithValue("periocidad", periocidad)
            strCommand.Parameters.AddWithValue("tipo_relacion", tipo_relacion)
            strCommand.Parameters.AddWithValue("cfdi_relacionado", cfdi_relacionado)
            If Not ControlTransaccion Is Nothing Then
                strCommand.Transaction = ControlTransaccion
            End If

            If cn.State <> ConnectionState.Open Then
                cn.Open()
            End If

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally
            If ControlTransaccion Is Nothing Then
                If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                    cn.Close()
                    cn.Dispose()
                End If
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing

            If ControlTransaccion Is Nothing Then
                cn = Nothing
            End If
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_TicketFactura_Estatus(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal estado As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_ACTUALIZA_TICKETFACTURA_ESTATUS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("estado", estado)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Caja_Valida_Efectivo(ByVal id_empresa As String _
                                                , ByVal id_sucursal As String _
                                                , ByVal caja As String _
                                                , ByRef dtDatos As DataTable _
                                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_CAJA_VALIDA_EFECTIVO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@caja", caja)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_Cuenta(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
        , ByRef cuenta As String _
        , ByVal tarjeta As String _
        , ByVal tipo As String _
        , ByVal estatus As String _
        , ByVal nombre As String _
        , ByVal nomcorto As String _
        , ByVal direccion As String _
        , ByVal entrecalles As String _
        , ByVal colonia As String _
        , ByVal municipio As String _
        , ByVal ciudad As String _
        , ByVal edo As String _
        , ByVal pais As String _
        , ByVal cpostal As String _
        , ByVal rfc As String _
        , ByVal nomfac As String _
        , ByVal telefono As String _
        , ByVal telefono2 As String _
        , ByVal email As String _
        , ByVal id_usuario As String _
        , ByVal descuento As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_CUENTA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("cuenta", cuenta)
            strCommand.Parameters.AddWithValue("tarjeta", tarjeta)
            strCommand.Parameters.AddWithValue("tipo", tipo)
            strCommand.Parameters.AddWithValue("estatus", estatus)
            strCommand.Parameters.AddWithValue("nombre", nombre)
            strCommand.Parameters.AddWithValue("nomcorto", nomcorto)
            strCommand.Parameters.AddWithValue("direccion", direccion)
            strCommand.Parameters.AddWithValue("entrecalles", entrecalles)
            strCommand.Parameters.AddWithValue("colonia", colonia)
            strCommand.Parameters.AddWithValue("municipio", municipio)
            strCommand.Parameters.AddWithValue("ciudad", ciudad)
            strCommand.Parameters.AddWithValue("edo", edo)
            strCommand.Parameters.AddWithValue("pais", pais)
            strCommand.Parameters.AddWithValue("cpostal", cpostal)
            strCommand.Parameters.AddWithValue("rfc", rfc)
            strCommand.Parameters.AddWithValue("nomfac", nomfac)
            strCommand.Parameters.AddWithValue("telefono", telefono)
            strCommand.Parameters.AddWithValue("telefono2", telefono2)
            strCommand.Parameters.AddWithValue("email", email)
            strCommand.Parameters.AddWithValue("id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("descuento", descuento)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    cuenta = dtDatos.Rows(0).Item("cuenta")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    cuenta = ""
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Actualiza_Caja_Abrir(ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal id_caja As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_ACTUALIZA_CAJA_ABRIR
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("id_caja", id_caja)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_Detventas_Cancelacion(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal as_venta As String _
                                , ByVal as_cance As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_DETVENTAS_CANCELACION
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("as_venta", as_venta)
            strCommand.Parameters.AddWithValue("as_cance", as_cance)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_Detventas_Cantidades(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal renglon As Integer _
                                , ByVal cantidad As Double _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_ACTUALIZA_DETVENTAS_CANTIDADES
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("renglon", renglon)
            strCommand.Parameters.AddWithValue("cantidad", cantidad)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Transacciones_Cancelacion_Estatus(ByVal cn As SqlConnection _
                                , ByVal ControlTransaccion As System.Data.SqlClient.SqlTransaction _
                                , ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal as_venta As String _
                                , ByVal estatus As String _
                                , ByVal tipo_relacion As String _
                                , ByRef Autonumsuc_Cancela As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        ''Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            ''cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_TRANSACCIONES_CANCELACION_ESTATUS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("as_venta", as_venta)
            strCommand.Parameters.AddWithValue("estatus", estatus)
            strCommand.Parameters.AddWithValue("tipo_relacion", tipo_relacion)
            strCommand.Transaction = ControlTransaccion
            strCommand.CommandTimeout = 0
            ''cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    Autonumsuc_Cancela = dtDatos.Rows(0).Item("autonumsuc")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            ''If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
            ''    cn.Close()
            ''    cn.Dispose()
            ''End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            ''cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_negados_motivos(ByVal estatus As String _
                                , ByRef dtDatos As DataTable _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_NEGADOS_MOTIVOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("estatus", estatus)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                Resultado = True
                dtDatos = dts.Tables(0)
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Inserta_articulos_negados(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal articulo As Integer _
                                , ByVal descripcion As String _
                                , ByVal cantidad As Double _
                                , ByVal precio As Double _
                                , ByVal cve_motivo As Integer _
                                , ByVal id_usuario As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_INSERTA_ARTICULOS_NEGADOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("articulo", articulo)
            strCommand.Parameters.AddWithValue("descripcion", descripcion)
            strCommand.Parameters.AddWithValue("cantidad", cantidad)
            strCommand.Parameters.AddWithValue("precio", precio)
            strCommand.Parameters.AddWithValue("cve_motivo", cve_motivo)
            strCommand.Parameters.AddWithValue("id_usuario", id_usuario)
            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Recupera_Pagare(ByVal fecha As Date _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_PAGARE
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_DetVentas_Nombre(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal renglon As Integer _
                                , ByVal nombre As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_ACTUALIZA_DETVENTAS_NOMBRE
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("renglon", renglon)
            strCommand.Parameters.AddWithValue("nombre", nombre)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Voucher_Inserta(P_id As Integer _
               , P_UrlBanorte As String _
               , P_Puerto As String _
               , P_User As String _
               , P_Pswd As String _
               , P_IdMerchant As String _
               , P_Modo As String _
               , P_Caja As String _
               , P_id_empresa As String _
               , P_empresa As String _
               , P_rfc As String _
               , P_direccion As String _
               , P_direccion2 As String _
               , P_tipo As String _
               , P_transaccion As Integer _
               , P_id_sucursal As String _
               , P_fecha As Date _
               , P_hora As String _
               , P_Banco As String _
               , P_Respuesta As String _
               , P_Afiliacion As String _
               , P_TerminalId As String _
               , P_NumControl As String _
               , P_NumTarjeta As String _
               , P_TipTarjeta As String _
               , P_VigTarjeta As String _
               , P_TitTarjeta As String _
               , P_BancoEmisor As String _
               , P_CodigoAut As String _
               , P_Referencia As String _
               , P_Importe As Double _
               , P_MesesDiferido As String _
               , P_NumPagos As String _
               , P_TipoPlan As String _
               , ByRef IdVoucher As Integer _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_VOUCHER_INSERTA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id", P_id)
            strCommand.Parameters.AddWithValue("UrlBanorte", P_UrlBanorte)
            strCommand.Parameters.AddWithValue("Puerto", P_Puerto)
            strCommand.Parameters.AddWithValue("User1", P_User)
            strCommand.Parameters.AddWithValue("Pswd", P_Pswd)
            strCommand.Parameters.AddWithValue("IdMerchant", P_IdMerchant)
            strCommand.Parameters.AddWithValue("Modo", P_Modo)
            strCommand.Parameters.AddWithValue("Caja", P_Caja)
            strCommand.Parameters.AddWithValue("id_empresa", P_id_empresa)
            strCommand.Parameters.AddWithValue("empresa", P_empresa)
            strCommand.Parameters.AddWithValue("rfc", P_rfc)
            strCommand.Parameters.AddWithValue("direccion", P_direccion)
            strCommand.Parameters.AddWithValue("direccion2", P_direccion2)
            strCommand.Parameters.AddWithValue("tipo", P_tipo)
            strCommand.Parameters.AddWithValue("transaccion", P_transaccion)
            strCommand.Parameters.AddWithValue("id_sucursal", P_id_sucursal)
            strCommand.Parameters.AddWithValue("fecha", P_fecha)
            strCommand.Parameters.AddWithValue("hora", P_hora)
            strCommand.Parameters.AddWithValue("Banco", P_Banco)
            strCommand.Parameters.AddWithValue("Respuesta", P_Respuesta)
            strCommand.Parameters.AddWithValue("Afiliacion", P_Afiliacion)
            strCommand.Parameters.AddWithValue("TerminalId", P_TerminalId)
            strCommand.Parameters.AddWithValue("NumControl", P_NumControl)
            strCommand.Parameters.AddWithValue("NumTarjeta", P_NumTarjeta)
            strCommand.Parameters.AddWithValue("TipTarjeta", P_TipTarjeta)
            strCommand.Parameters.AddWithValue("VigTarjeta", P_VigTarjeta)
            strCommand.Parameters.AddWithValue("TitTarjeta", P_TitTarjeta)
            strCommand.Parameters.AddWithValue("BancoEmisor", P_BancoEmisor)
            strCommand.Parameters.AddWithValue("CodigoAut", P_CodigoAut)
            strCommand.Parameters.AddWithValue("Referencia", P_Referencia)
            strCommand.Parameters.AddWithValue("Importe", P_Importe)
            strCommand.Parameters.AddWithValue("MesesDiferido", P_MesesDiferido)
            strCommand.Parameters.AddWithValue("NumPagos", P_NumPagos)
            strCommand.Parameters.AddWithValue("TipoPlan", P_TipoPlan)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            IdVoucher = 0
            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    IdVoucher = dtDatos.Rows(0).Item("id_voucher")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Voucher_Actualiza(P_id As Integer _
               , P_Id_Sucursal As String _
               , P_Referencia As String _
               , P_NumTarjeta As String _
               , P_Importe As Double _
               , P_Respuesta As String _
               , P_CodigoAut As String _
               , P_BancoEmisor As String _
               , P_TipTarjeta As String _
               , ByRef IdVoucher As Integer _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_VOUCHER_ACTUALIZA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id", P_id)
            strCommand.Parameters.AddWithValue("id_sucursal", P_Id_Sucursal)
            strCommand.Parameters.AddWithValue("Respuesta", P_Respuesta)
            strCommand.Parameters.AddWithValue("NumTarjeta", P_NumTarjeta)
            strCommand.Parameters.AddWithValue("TipTarjeta", P_TipTarjeta)
            strCommand.Parameters.AddWithValue("BancoEmisor", P_BancoEmisor)
            strCommand.Parameters.AddWithValue("CodigoAut", P_CodigoAut)
            strCommand.Parameters.AddWithValue("Referencia", P_Referencia)
            strCommand.Parameters.AddWithValue("Importe", P_Importe)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            IdVoucher = 0
            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    IdVoucher = dtDatos.Rows(0).Item("id_voucher")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Voucher_Sel(ByVal id_empresa As String _
                           , ByVal banco As String _
                           , ByVal fecha_ini As Date _
                           , ByVal fecha_fin As Date _
                           , ByVal sw_pendientes As Boolean _
                           , ByRef dtDatos As DataTable _
                           , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_VOUCHER_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@banco", banco)
            strCommand.Parameters.AddWithValue("@fecha_ini", fecha_ini)
            strCommand.Parameters.AddWithValue("@fecha_fin", fecha_fin)
            strCommand.Parameters.AddWithValue("@sw_pendientes", sw_pendientes)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Actualiza_Nube_Bitacora_Autorizacione(ByVal autonumsuc As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_ACTUALIZA_NUBE_BITACORA_AUTORIZACIONE
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn
            strCommand.CommandTimeout = 0

            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Recupera_tiposcredito(ByVal id_empresa As String _
                                , ByVal tipo As String _
                                , ByVal tipoventa As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_RECUPERA_TIPOSCREDITO
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@tipoventa", tipoventa)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Pedidos_Sel(ByVal id_empresa As String _
                           , ByVal pedido_id As Integer _
                           , ByRef dtDatos1 As DataTable _
                           , ByRef dtDatos2 As DataTable _
                           , ByRef dtDatos3 As DataTable _
                           , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_PEDIDOS_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_pedido", pedido_id)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos1 = dts.Tables(0)
                dtDatos2 = dts.Tables(1)
                dtDatos3 = dts.Tables(2)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_PedidosNotificacion_Sel(ByVal id_empresa As String _
                           , ByVal id_sucursal As String _
                           , ByRef Registros As Integer _
                           , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_PEDIDOSNOTIFICACION_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                Registros = dtDatos.Rows(0).Item("regresa")
                If Registros > 0 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
            Registros = 0
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Inserta_ClienteFinal(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByRef id_cliente As String _
                                , ByVal cliente_final As String _
                                , ByVal referencia As String _
                                , ByVal tipo As String _
                                , ByVal estatus As String _
                                , ByVal nombre_cli As String _
                                , ByVal nombre As String _
                                , ByVal adicional As String _
                                , ByVal apaterno As String _
                                , ByVal amaterno As String _
                                , ByVal nomcorto As String _
                                , ByVal direccion As String _
                                , ByVal entrecalles As String _
                                , ByVal colonia As String _
                                , ByVal municipio As String _
                                , ByVal ciudad As String _
                                , ByVal edo As String _
                                , ByVal pais As String _
                                , ByVal cpostal As String _
                                , ByVal rfc As String _
                                , ByVal telefono As String _
                                , ByVal telefono2 As String _
                                , ByVal email As String _
                                , ByVal internet As String _
                                , ByVal fax As String _
                                , ByVal contacto As String _
                                , ByVal observaciones As String _
                                , ByVal id_usuario As String _
                                , ByVal id_banco As String _
                                , ByVal tipo_banco As String _
                                , ByVal cuenta_banco As String _
                                , ByVal curp As String _
                                , ByVal beneficiario As String _
                                , ByVal parentesco As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_INSERTA_CLIENTEFINAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@cliente_final", cliente_final)
            strCommand.Parameters.AddWithValue("@referencia", referencia)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@estatus", estatus)
            strCommand.Parameters.AddWithValue("@nombre_cli", nombre_cli)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@adicional", adicional)
            strCommand.Parameters.AddWithValue("@apaterno", apaterno)
            strCommand.Parameters.AddWithValue("@amaterno", amaterno)
            strCommand.Parameters.AddWithValue("@nomcorto", nomcorto)
            strCommand.Parameters.AddWithValue("@direccion", direccion)
            strCommand.Parameters.AddWithValue("@entrecalles", entrecalles)
            strCommand.Parameters.AddWithValue("@colonia", colonia)
            strCommand.Parameters.AddWithValue("@municipio", municipio)
            strCommand.Parameters.AddWithValue("@ciudad", ciudad)
            strCommand.Parameters.AddWithValue("@edo", edo)
            strCommand.Parameters.AddWithValue("@pais", pais)
            strCommand.Parameters.AddWithValue("@cpostal", cpostal)
            strCommand.Parameters.AddWithValue("@rfc", rfc)
            strCommand.Parameters.AddWithValue("@telefono", telefono)
            strCommand.Parameters.AddWithValue("@telefono2", telefono2)
            strCommand.Parameters.AddWithValue("@email", email)
            strCommand.Parameters.AddWithValue("@internet", internet)
            strCommand.Parameters.AddWithValue("@fax", fax)
            strCommand.Parameters.AddWithValue("@contacto", contacto)
            strCommand.Parameters.AddWithValue("@observaciones", observaciones)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@id_banco", id_banco)
            strCommand.Parameters.AddWithValue("@tipo_banco", tipo_banco)
            strCommand.Parameters.AddWithValue("@cuenta_banco", cuenta_banco)
            strCommand.Parameters.AddWithValue("@curp", curp)
            strCommand.Parameters.AddWithValue("@beneficiario", beneficiario)
            strCommand.Parameters.AddWithValue("@parentesco", parentesco)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    id_cliente = dtDatos.Rows(0).Item("id_cliente")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    id_cliente = ""
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Geo_Estados_Sel(ByVal id_empresa As String _
                           , ByRef dtDatos As DataTable _
                           , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_GEO_ESTADOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            ''strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            ''strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                Resultado = True
            Else
                Resultado = False
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Geo_Municipios_Sel(ByVal id_empresa As String _
                           , ByVal estado As String _
                           , ByRef dtDatos As DataTable _
                           , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_GEO_MUNICIPIOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@estado", estado)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                Resultado = True
            Else
                Resultado = False
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function


    Public Function Geo_Asentamientos_Sel(ByVal id_empresa As String _
                           , ByVal estado As String _
                           , ByVal municipio As String _
                           , ByVal cp As Integer _
                           , ByRef dtDatos As DataTable _
                           , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_GEO_ASENTAMIENTOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@estado", estado)
            strCommand.Parameters.AddWithValue("@municipio", municipio)
            strCommand.Parameters.AddWithValue("@cp", cp)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                Resultado = True
            Else
                Resultado = False
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Recupera_ClienteFinal(ByVal id_cliente As String _
                                , ByVal nombre As String _
                                , ByVal rfc As String _
                                , ByVal curp As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CLIENTEFINAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@nombre", nombre)
            strCommand.Parameters.AddWithValue("@rfc", rfc)
            strCommand.Parameters.AddWithValue("@curp", curp)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function



    Public Function Credito_tasas_Plazo_Sel(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal tipo As String _
                                , ByVal tipo_cliente As String _
                                , ByVal importe As Double _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_CREDITO_TASAS_PLAZO_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@tipo_cliente", tipo_cliente)
            strCommand.Parameters.AddWithValue("@importe", importe)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Credito_Vales_Ins(ByVal id_empresa As String _
                                    , ByVal id_sucursal As String _
                                    , ByVal autonumsuc As String _
                                    , ByVal caja As String _
                                    , ByVal id_usuario As String _
                                    , ByVal id_cliente As String _
                                    , ByVal id_cliente_final As String _
                                    , ByVal vale_serie As String _
                                    , ByVal vale_numero As Integer _
                                    , ByVal importe As Double _
                                    , ByVal plazo As Integer _
                                    , ByVal desembolso As String _
                                    , ByVal moneda As String _
                                    , ByVal tipo_tasa As String _
                                    , ByVal tipo As String _
                                    , ByVal Token As String _
                                    , ByRef autonumsuc1 As String _
                                    , ByRef despedida As String _
                                    , ByRef cliente_disponeg2 As Double _
                                    , ByRef importe2 As Double _
                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_CREDITO_VALES_INS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@caja", caja)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@id_cliente_final", id_cliente_final)
            strCommand.Parameters.AddWithValue("@vale_serie", vale_serie)
            strCommand.Parameters.AddWithValue("@vale_numero", vale_numero)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@plazo", plazo)
            strCommand.Parameters.AddWithValue("@desembolso", desembolso)
            strCommand.Parameters.AddWithValue("@moneda", moneda)
            strCommand.Parameters.AddWithValue("@tipo_tasa", tipo_tasa)
            strCommand.Parameters.AddWithValue("@tipo", tipo)
            strCommand.Parameters.AddWithValue("@token", Token)
            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    autonumsuc1 = dtDatos.Rows(0).Item("autonumsuc1")
                    despedida = dtDatos.Rows(0).Item("despedida")
                    cliente_disponeg2 = dtDatos.Rows(0).Item("cliente_disponeg2")
                    importe2 = dtDatos.Rows(0).Item("importe2")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Recupera_Canje_Impresion(ByVal fecha As Date _
, ByVal autonumsuc As String _
, ByVal id_empresa As String _
, ByVal id_sucursal As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef dtDatos1 As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUPERA_CANJE_IMPRESION
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@fecha", fecha)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                dtDatos1 = dts.Tables(1)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function EDOCTACLIENTES_SEL(ByVal id_empresa As String, cliente As String _
                                 , ByRef dtDatos As DataTable _
                                 , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_EDOCTACLIENTES_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@cliente", cliente)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function


    Public Function RecuperaAntiguedadSaldos(ByVal id_empresa As String, sucursal As String, cliente As String, tipo As String, grupo As String, moneda As String, fecha As Date, orden As Int16,
                                             sw_concepto As Boolean, cordinador As String, vendedor As String, cobrador As String, vencimiento As String _
                                , ByRef dtDatos As DataTable _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_RECUPERA_ANTIGUEDAD_SALDOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@Id_sucursal", sucursal)
            strCommand.Parameters.AddWithValue("@id_Cliente", cliente)
            strCommand.Parameters.AddWithValue("@Tipo", tipo)
            strCommand.Parameters.AddWithValue("@Grupo", grupo)
            strCommand.Parameters.AddWithValue("@Moneda", moneda)
            strCommand.Parameters.AddWithValue("@Fecha", fecha)
            strCommand.Parameters.AddWithValue("@Orden", orden)
            strCommand.Parameters.AddWithValue("@Sw_concepto", sw_concepto)
            strCommand.Parameters.AddWithValue("@Id_coordinador", cordinador)
            strCommand.Parameters.AddWithValue("@Vendedor", vendedor)
            strCommand.Parameters.AddWithValue("@Cobrador", cobrador)
            strCommand.Parameters.AddWithValue("@Mostar_Vencimiento", vencimiento)


            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function


    Public Function CLI_Aplicaciones_Sel(ByVal id_empresa As String _
                                 , ByVal id_sucursal As String _
                                 , ByVal id_cliente As String _
                                 , ByVal fecha_ini As Date _
                                 , ByVal fecha_fin As Date _
                                 , ByVal id_usuario As String _
                                 , ByVal sw_detalle As String _
                                 , ByRef dtDatos As DataTable _
                                 , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_CLI_APLICACIONES_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@fecha_ini", fecha_ini)
            strCommand.Parameters.AddWithValue("@fecha_fin", fecha_fin)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@sw_detalle", sw_detalle)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_ImagenesApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String _
                                   , ByVal fecha As Date _
                                   , ByVal id_usuario As String _
                                   , ByVal oImagen As Image _
                                   , ByRef Mensaje As String) As Boolean
        '' As List(Of proyVOficinaBaseAppApi.clsRegesa)
        Dim Resultado As Boolean = False
        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim MS As System.IO.MemoryStream
        Dim img_byte As Byte()
        Try
            oDatos = New Datos_Viscoi
            ''img_byte = Convert.FromBase64String(fotobase64)
            ''MS = New System.IO.MemoryStream(img_byte)
            MS = New System.IO.MemoryStream
            oImagen.Save(MS, System.Drawing.Imaging.ImageFormat.Jpeg)
            img_byte = MS.ToArray()

            MS.Close()
            MS.Dispose()
            MS = Nothing
            oImagen.Dispose()
            oImagen = Nothing
            GC.Collect()

            If oDatos.Inserta_ImagenesApp(id_empresa, tabla, movimiento, renglon, fecha, id_usuario, img_byte, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                    Mensaje = "Exito"
                Else
                    Resultado = False
                    Mensaje = "No se encontro información"
                End If
            Else
                Resultado = False
                Mensaje = Msj
            End If
        Catch ex As Exception
            Resultado = False
            Mensaje = ex.Message
        End Try

        Return Resultado
    End Function

    Public Function Inserta_ImagenesByteApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String _
                                   , ByVal fecha As Date _
                                   , ByVal id_usuario As String _
                                   , ByVal oImagen As Byte() _
                                   , ByRef Mensaje As String) As Boolean
        '' As List(Of proyVOficinaBaseAppApi.clsRegesa)
        Dim Resultado As Boolean = False
        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim MS As System.IO.MemoryStream
        Dim img_byte As Byte()
        Try
            oDatos = New Datos_Viscoi
            ''img_byte = Convert.FromBase64String(fotobase64)
            ''MS = New System.IO.MemoryStream(img_byte)


            If oDatos.Inserta_ImagenesApp(id_empresa, tabla, movimiento, renglon, fecha, id_usuario, oImagen, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                    Mensaje = "Exito"
                Else
                    Resultado = False
                    Mensaje = "No se encontro información"
                End If
            Else
                Resultado = False
                Mensaje = Msj
            End If
        Catch ex As Exception
            Resultado = False
            Mensaje = ex.Message
        End Try

        Return Resultado
    End Function

    Public Function VerificaIdentidad_Sel(ByVal id_empresa As String _
                                , ByVal id_cliente As String _
                                , ByRef dtDatos As DataTable _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim Msj As String = ""

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_VERIFICAIDENTIDAD_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_cliente", id_cliente)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function VerificaIdentidad_Upd(ByVal id_empresa As String _
                                , ByVal id_cliente As String _
                                , ByVal response As String _
                                , ByRef sw_verificamex As Boolean _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dtDatos As DataTable
        Dim dtRenglon As DataRow
        Dim dts As New DataSet
        Dim Msj As String = ""

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_VERIFICAIDENTIDAD_UPD
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("response", response)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            sw_verificamex = False
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows.Count > 0 Then
                    dtRenglon = dtDatos.Rows(0)
                    If (dtRenglon.Item("regresa") > 0) Then
                        If (dtRenglon.Item("sw_verificamex")) Then
                            Resultado = True
                            sw_verificamex = True
                            Msj = "Exito"
                        Else
                            Resultado = True
                            sw_verificamex = False
                            Msj = dtRenglon.Item("msj_verificamex")
                        End If
                    Else
                        Resultado = False
                        sw_verificamex = False
                        Msj = dtRenglon.Item("msj")
                    End If
                Else
                    Resultado = False
                    sw_verificamex = False
                    Msj = "No se encontro información"
                End If
            End If
            Resultado = True
            Excepcion = Msj
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Inserta_VideoApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String _
                                   , ByVal fecha As Date _
                                   , ByVal id_usuario As String _
                                   , ByVal oImagen As Byte() _
                                   , ByRef Mensaje As String) As Boolean
        '' As List(Of proyVOficinaBaseAppApi.clsRegesa)
        Dim Resultado As Boolean = False
        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""
        Dim dtDatos As DataTable = Nothing

        Try
            oDatos = New Datos_Viscoi
            ''img_byte = Convert.FromBase64String(fotobase64)
            ''MS = New System.IO.MemoryStream(img_byte)

            GC.Collect()

            If oDatos.Inserta_ImagenesApp(id_empresa, tabla, movimiento, renglon, fecha, id_usuario, oImagen, dtDatos, Msj) Then
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                    Mensaje = "Exito"
                Else
                    Resultado = False
                    Mensaje = "No se encontro información"
                End If
            Else
                Resultado = False
                Mensaje = Msj
            End If
        Catch ex As Exception
            Resultado = False
            Mensaje = ex.Message
        End Try

        Return Resultado
    End Function
    Public Sub Recupera_ImagenesApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String _
                                   , ByRef PicBox1 As PictureBox)
        '' As List(Of proyVOficinaBaseAppApi.clsSolicitudesDepositoEntrega)

        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim img_byte As Byte()
        Dim ms As System.IO.MemoryStream
        Dim base64String As String
        Try
            oDatos = New Datos_Viscoi

            If oDatos.Recupera_ImagenesApp(id_empresa, tabla, movimiento, renglon, dtDatos, Msj) Then
                For Each dtRenglon As DataRow In dtDatos.Rows
                    img_byte = dtRenglon.Item("foto")
                    ms = New System.IO.MemoryStream(img_byte)
                    base64String = Convert.ToBase64String(img_byte, 0, img_byte.Length)

                    PicBox1.Image = Image.FromStream(ms)
                    PicBox1.SizeMode = PictureBoxSizeMode.Zoom
                    PicBox1.Refresh()
                Next
            Else
                PicBox1.Image = Nothing
            End If
        Catch ex As Exception
            PicBox1.Image = Nothing
        End Try

    End Sub

    Public Function Recupera_VideoApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String) As Byte()
        '' As List(Of proyVOficinaBaseAppApi.clsSolicitudesDepositoEntrega)

        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim img_byte As Byte()
        Dim ms As System.IO.MemoryStream
        Dim base64String As String
        Try
            oDatos = New Datos_Viscoi

            If oDatos.Recupera_ImagenesApp(id_empresa, tabla, movimiento, renglon, dtDatos, Msj) Then
                For Each dtRenglon As DataRow In dtDatos.Rows
                    img_byte = dtRenglon.Item("foto")

                Next
            End If
        Catch ex As Exception
        End Try

        Return img_byte

    End Function
    Public Function Recupera_DocumentosApp(ByVal id_empresa As String _
                                   , ByVal tabla As String _
                                   , ByVal movimiento As String _
                                   , ByVal renglon As String _
                                   , ByRef PicBox1 As PictureBox)
        '' As List(Of proyVOficinaBaseAppApi.clsSolicitudesDepositoEntrega)

        Dim oDatos As Datos_Viscoi
        Dim Msj As String = ""
        Dim dtDatos As DataTable = Nothing
        Dim img_byte As Byte()
        Dim ms As System.IO.MemoryStream
        Dim base64String As String
        Try
            oDatos = New Datos_Viscoi

            If oDatos.Recupera_Documentos(id_empresa, dtDatos, Msj) Then
                For Each dtRenglon As DataRow In dtDatos.Rows
                    img_byte = dtRenglon.Item("foto")
                    ms = New System.IO.MemoryStream(img_byte)
                    base64String = Convert.ToBase64String(img_byte, 0, img_byte.Length)

                    PicBox1.Image = Image.FromStream(ms)
                    PicBox1.SizeMode = PictureBoxSizeMode.Zoom
                    PicBox1.Refresh()
                Next
            Else
                PicBox1.Image = Nothing
            End If
        Catch ex As Exception
            PicBox1.Image = Nothing
        End Try

        Return img_byte

    End Function
    Public Function Credito_Vales_Series_Sel(ByVal id_empresa As String _
                               , ByVal id_sucursal As String _
                               , ByVal id_cliente As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_CREDITO_VALES_SERIES_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Crm_Tipos_Sel(ByVal id_empresa As String _
                               , ByVal grupo As String _
                               , ByVal sw_todos As Boolean _
                               , ByVal estatus As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_CRM_TIPOS_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@grupo", grupo)
            strCommand.Parameters.AddWithValue("@sw_todos", sw_todos)
            strCommand.Parameters.AddWithValue("@estatus", estatus)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Recuepera_FolioDigital_Sel(ByVal id_empresa As String _
                               , ByVal FolioDigital As String _
                               , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_RECUEPERA_FOLIODIGITAL_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@folio_digital", FolioDigital)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_ctabancos_BOVEDA_Sel(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal id_usuario As String _
                                , ByRef dtDatos As DataTable _
                               , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_ctabancos_BOVEDA_Sel
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_ContravaleRpt(ByVal id_empresa As String _
                          , ByVal id_sucursal As String _
                          , ByVal fecha_ini As Date _
                          , ByVal fecha_fin As Date _
                          , ByRef dtDatos As DataTable _
                          , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_CONTRAVALERPT
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@fecha_ini", fecha_ini)
            strCommand.Parameters.AddWithValue("@fecha_fin", fecha_fin)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally
            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Traspaso_Dinero_INS(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal origen As String _
                                , ByVal destino As String _
                                , ByVal id_usuario As String _
                                , ByVal total As Double _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_TRASPASO_DINERO_INS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@origen", origen)
            strCommand.Parameters.AddWithValue("@destino", destino)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
            strCommand.Parameters.AddWithValue("@monto", total)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function CAR_Genera_Folio_Digital_NAU(ByVal Autonumsuc As String _
                                , ByVal id_empresa As String _
                                , ByVal id_cliente As String _
                                , ByVal id_cliente_final As String _
                                , ByVal importe As String _
                                , ByVal plazo As String _
                                , ByRef folio_digital As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_CAR_GENERA_FOLIO_DIGITAL_NAU
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@autonumsuc", Autonumsuc)
            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@id_cliente_final", id_cliente_final)
            strCommand.Parameters.AddWithValue("@importe", importe)
            strCommand.Parameters.AddWithValue("@plazo", plazo)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    folio_digital = dtDatos.Rows(0).Item("folio_digital")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                    folio_digital = ""
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function Inserta_TicketFactura_Refactura(ByVal cn As System.Data.SqlClient.SqlConnection _
                                , ByVal ControlTransaccion As System.Data.SqlClient.SqlTransaction _
                                , ByVal id_empresa As String _
                                , ByVal movimiento As String _
                                , ByVal tipo As Integer _
                                , ByVal uso_cfdi As String _
                                , ByVal regimen_fiscal As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            If cn Is Nothing Then
                cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            End If
            strCommand.CommandText = SP_INSERTA_TICKETFACTURA_REFACTURA
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("movimiento", movimiento)
            strCommand.Parameters.AddWithValue("tipo", tipo)
            strCommand.Parameters.AddWithValue("uso_cfdi", uso_cfdi)
            strCommand.Parameters.AddWithValue("regimen_fiscal", regimen_fiscal)
            If Not ControlTransaccion Is Nothing Then
                strCommand.Transaction = ControlTransaccion
            End If
            If cn.State <> ConnectionState.Open Then
                cn.Open()
            End If

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally
            If ControlTransaccion Is Nothing Then
                If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                    cn.Close()
                    cn.Dispose()
                End If
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing

            If ControlTransaccion Is Nothing Then
                cn = Nothing
            End If
        End Try
        Return Resultado
    End Function
    Public Function Tim_Canc_Actualiza_TicketFactura_CFDI_Gen(ByVal cn As SqlConnection _
                                                    , ByVal ControlTransaccion As System.Data.SqlClient.SqlTransaction _
                                                    , ByVal id_empresa As String _
                                                    , ByVal id_sucursal As String _
                                                    , ByVal autonumsuc As String _
                                                    , ByVal email_enviado As Integer _
                                                    , ByVal estatus As String _
                                                    , ByVal subtotal As Decimal _
                                                    , ByVal total As Decimal _
                                                    , ByVal uuid As String _
                                                    , ByVal xml As String _
                                                    , ByVal noCertificadoSAT As String _
                                                    , ByVal selloCFD As String _
                                                    , ByVal selloSAT As String _
                                                    , ByVal CadenaOriginal As String _
                                                    , ByVal version As String _
                                                    , ByVal fechatimbrado As String _
                                                    , ByVal rfcprovcertif As String _
                                                    , ByVal nocertificado As String _
                                                    , ByVal periocidad As String _
                                                    , ByVal tipo_relacion As String _
                                                    , ByVal cfdi_relacionado As String _
                                                    , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Try
            If cn Is Nothing Then
                cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            End If
            strCommand.CommandText = SP_TIM_CANC_ACTUALIZA_TICKETFACTURA_CFDI_GEN
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("email_enviado", email_enviado)
            strCommand.Parameters.AddWithValue("estatus", estatus)
            strCommand.Parameters.AddWithValue("total", total)
            strCommand.Parameters.AddWithValue("subtotal", subtotal)
            strCommand.Parameters.AddWithValue("uuid", uuid)
            strCommand.Parameters.AddWithValue("xml", xml)
            strCommand.Parameters.AddWithValue("noCertificadoSAT", noCertificadoSAT)
            strCommand.Parameters.AddWithValue("selloCFD", selloCFD)
            strCommand.Parameters.AddWithValue("selloSAT", selloSAT)
            strCommand.Parameters.AddWithValue("cadenaoriginal", CadenaOriginal)
            strCommand.Parameters.AddWithValue("version", version)
            strCommand.Parameters.AddWithValue("fechatimbrado", fechatimbrado)
            strCommand.Parameters.AddWithValue("rfcprovcertif", rfcprovcertif)
            strCommand.Parameters.AddWithValue("nocertificado", nocertificado)
            strCommand.Parameters.AddWithValue("periocidad", periocidad)
            strCommand.Parameters.AddWithValue("tipo_relacion", tipo_relacion)
            strCommand.Parameters.AddWithValue("cfdi_relacionado", cfdi_relacionado)
            If Not ControlTransaccion Is Nothing Then
                strCommand.Transaction = ControlTransaccion
            End If

            If cn.State <> ConnectionState.Open Then
                cn.Open()
            End If

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If
        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally
            If ControlTransaccion Is Nothing Then
                If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                    cn.Close()
                    cn.Dispose()
                End If
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing

            If ControlTransaccion Is Nothing Then
                cn = Nothing
            End If
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Transacciones_PUBMAY(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal referencia As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_TRANSACCIONES_PUBMAY
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@referencia", referencia)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function PVTA_Sat_CFDI_FechaActual(ByRef dtDatos As DataTable, ByRef Excepcion As String)
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_SAT_CFDI_FECHAACTUAL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

    Public Function PVTA_Monedero_Electronico_Sel(ByVal id_empresa As String _
                                , ByVal id_cliente As String _
                                , ByVal folio As String _
                                , ByRef dtDatos As DataTable _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_MONEDERO_ELECTRONICO_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            strCommand.Parameters.AddWithValue("@folio", folio)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function Clientes_Finales_Datos_Bancarios_Ins(ByVal id_empresa As String _
                                                        , ByVal id_cliente_final As String _
                                                        , ByVal oImagen As Image _
                                                        , ByVal extension As String _
                                                        , ByVal observacion As String _
                                                        , ByVal id_usuario As String _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable
        Dim MS As System.IO.MemoryStream
        Dim img_byte As Byte()
        Try
            MS = New System.IO.MemoryStream
            If oImagen IsNot Nothing Then
                oImagen.Save(MS, System.Drawing.Imaging.ImageFormat.Jpeg)
                img_byte = MS.ToArray()

                MS.Close()
                MS.Dispose()
                MS = Nothing
                oImagen.Dispose()
                oImagen = Nothing
                GC.Collect()
            Else
                img_byte = {0}
            End If

            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_CLIENTES_FINALES_DATOS_BANCARIOS_INS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_cliente_final", id_cliente_final)
            strCommand.Parameters.AddWithValue("@archivo", img_byte)
            strCommand.Parameters.AddWithValue("@extension", extension)
            strCommand.Parameters.AddWithValue("@observacion", observacion)
            strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function


    Public Function LstPrecios(ByVal id_empresa As String _
                                , ByVal id_sucursal As String _
                                , ByVal autonumsuc As String _
                                , ByVal renglon As Integer _
                                , ByVal tipo As Integer _
                                , ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oViscoi.ObtenerConexion()
            strCommand.CommandText = SP_PVTA_LSTPRECIOS
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
            strCommand.Parameters.AddWithValue("@id_sucursal", id_sucursal)
            strCommand.Parameters.AddWithValue("@autonumsuc", autonumsuc)
            strCommand.Parameters.AddWithValue("@renglon", renglon)
            strCommand.Parameters.AddWithValue("@tipo", tipo)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then

                dtDatos = dts.Tables(0)
                If dtDatos.Rows(0).Item("regresa") = 1 Then
                    Resultado = True
                    Excepcion = dtDatos.Rows(0).Item("msj")
                Else
                    Resultado = False
                    Excepcion = dtDatos.Rows(0).Item("msj")
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

End Class