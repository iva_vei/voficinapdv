﻿Imports System.Data.SqlClient
Public Class Datos_Control
    Private Const PA_WMS_VERSIONES_SEL As String = "pa_WMS_Versiones_Sel"
    Private Const PA_SEGURIDAD_USUARIOS_ABC As String = "pa_seguridad_Usuarios_ABC"
    Private Const FN_OPCIONES_PROCESO_STR_SEL As String = "fn_Opciones_Proceso_Str_Sel"
    Private Const FN_OPCIONES_PROCESO_NUM_SEL As String = "fn_Opciones_Proceso_Num_Sel"

    Public Function Test(ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.oControl.ObtenerConexion()
            strCommand.CommandText = "SELECT valor = 1"
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Resultado = False
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                Else
                    Resultado = False
                End If
            End If
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function WMS_Versiones_Sel(ByVal sistema As String,
                                      ByVal producto As String,
                                      ByRef dtDatos As DataTable,
                                      ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet

        Try
            cn = Globales.oAmbientes.oControl.ObtenerConexion()
            strCommand.CommandText = PA_WMS_VERSIONES_SEL
            strCommand.CommandType = CommandType.StoredProcedure
            strCommand.Connection = cn

            strCommand.Parameters.AddWithValue("@sistema", sistema)
            strCommand.Parameters.AddWithValue("@producto", producto)

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                '_usuarios_permisos = dts.Tables(3)
            End If
            Resultado = True
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    'Public Function Opciones_Proceso_Str_Sel(ByVal id_empresa As Integer _
    '                                       , ByVal esquema As String _
    '                                       , ByVal proceso As String _
    '                                       , ByRef Excepcion As String) As String
    '    Dim Resultado As String = ""
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = "SELECT valor = " & ESQUEMA_GENERALES & FN_OPCIONES_PROCESO_STR_SEL & "(" & id_empresa.ToString & ",'" & esquema & "','" & proceso & "')"
    '        strCommand.CommandType = CommandType.Text
    '        strCommand.Connection = cn
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows.Count > 0 Then
    '                Resultado = dtDatos.Rows(0).Item("valor")
    '            Else
    '                Resultado = ""
    '            End If
    '        End If
    '
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = ""
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Opciones_Proceso_Num_Sel(ByVal id_empresa As Integer _
    '                                       , ByVal esquema As String _
    '                                       , ByVal proceso As String _
    '                                       , ByRef Excepcion As String) As Double
    '    Dim Resultado As Double = 0.0
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = "SELECT valor = " & ESQUEMA_GENERALES & FN_OPCIONES_PROCESO_NUM_SEL & "(" & id_empresa.ToString & ",'" & esquema & "','" & proceso & "')"
    '        strCommand.CommandType = CommandType.Text
    '        strCommand.Connection = cn
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows.Count > 0 Then
    '                Resultado = dtDatos.Rows(0).Item("valor")
    '            Else
    '                Resultado = 0.0
    '            End If
    '        End If
    '
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = ""
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Usuarios_Sel(ByVal id_empresa As Integer, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_USUARIOS_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Permisos_Estructura_Sel(ByVal id_empresa As Integer, ByVal id_usuario As String, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERMISOS_ESTRUCTURA_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Perfil_Sel(ByVal id_empresa As Integer, ByVal id_perfil As Integer, ByVal sw_todos As Boolean, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERFIL_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@id_perfil", id_perfil)
    '        strCommand.Parameters.AddWithValue("@sw_todos", sw_todos)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    ''Public Function Segurdad_Permisos_Pantallas_Sel(ByVal id_usuario As Integer, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    ''    Dim Resultado As Boolean = False
    ''    Dim cn As SqlConnection = Nothing
    ''    Dim strCommand As New SqlCommand()
    ''    Dim sADa As New SqlDataAdapter
    ''    Dim dts As New DataSet
    '
    ''    Try
    ''        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    ''        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERMISOS_ESTRUCTURA_SEL
    ''        strCommand.CommandType = CommandType.StoredProcedure
    ''        strCommand.Connection = cn
    '
    ''        strCommand.Parameters.Add("@id_usuario", SqlDbType.Int)
    ''        strCommand.Parameters("@id_usuario").Value = id_usuario
    '
    ''        cn.Open()
    '
    ''        sADa.SelectCommand = strCommand
    ''        sADa.Fill(dts)
    '
    ''        If dts.Tables.Count > 0 Then
    ''            dtDatos = dts.Tables(0)
    ''            '_usuarios_permisos = dts.Tables(3)
    ''        End If
    ''        Resultado = True
    ''        Excepcion = "NO_ERROR"
    '
    ''    Catch ex As Exception
    ''        Excepcion = ex.Message
    ''        Resultado = False
    ''    Finally
    '
    ''        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    ''            cn.Close()
    ''            cn.Dispose()
    ''        End If
    ''        If strCommand IsNot Nothing Then
    ''            strCommand.Dispose()
    ''        End If
    '
    ''        strCommand = Nothing
    ''        cn = Nothing
    ''    End Try
    ''    Return Resultado
    ''End Function
    'Public Function Seguridad_Usuarios_ABC(ByVal id_empresa As Integer _
    '                                     , ByVal tipo As String _
    '                                     , ByRef id_usuario As Integer _
    '                                     , ByVal usuario As String _
    '                                     , ByVal nombre_usuario As String _
    '                                     , ByVal pass As String _
    '                                     , ByVal id_tipo_perfil As Integer _
    '                                     , ByVal fecha_alta As Date _
    '                                     , ByVal fecha_vence As Date _
    '                                     , ByVal id_empleado As Integer _
    '                                     , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_USUARIOS_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@tipo", tipo)
    '        strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
    '        strCommand.Parameters.AddWithValue("@usuario", usuario)
    '        strCommand.Parameters.AddWithValue("@nombre_usuario", nombre_usuario)
    '        strCommand.Parameters.AddWithValue("@pass", pass)
    '        strCommand.Parameters.AddWithValue("@id_tipo_perfil", id_tipo_perfil)
    '        strCommand.Parameters.AddWithValue("@fecha_alta", fecha_alta)
    '        strCommand.Parameters.AddWithValue("@fecha_vence", fecha_vence)
    '        strCommand.Parameters.AddWithValue("@id_empleado", id_empleado)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '                id_usuario = dtDatos.Rows(0).Item("id_usuario")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Permisos_Estructura_ABC(ByVal id_empresa As Integer _
    '                                     , ByVal id_usuario As Integer _
    '                                     , ByVal estructura As DataTable _
    '                                     , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERMISOS_ESTRUCTURA_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@id_usuario", id_usuario)
    '        strCommand.Parameters.AddWithValue("@estructura", estructura)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Pantallas_SEL(ByVal sistema As String, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PANTALLAS_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@sistema", sistema)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Pantallas_ABC(ByVal id_empresa As Integer _
    '                                     , ByVal sistema As String _
    '                                     , ByVal pantallas As DataTable _
    '                                     , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PANTALLAS_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@sistema", sistema)
    '        strCommand.Parameters.AddWithValue("@pantallas", pantallas)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Jerarquias_SEL(ByVal id_empresa As Integer, ByVal nivel As Integer, ByVal sw_estatus As Integer, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_JERARQUIAS_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@nivel", nivel)
    '        strCommand.Parameters.AddWithValue("@sw_estatus", sw_estatus)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Jerarquias_ABC(ByVal id_empresa As Integer _
    '                                     , ByVal nivel As Integer _
    '                                     , ByVal jerarquias As DataTable _
    '                                     , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_JERARQUIAS_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@nivel", nivel)
    '        strCommand.Parameters.AddWithValue("@jerarquias", jerarquias)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Estatus_SEL(ByVal Grupo As String, ByVal Sw_Todos As Boolean, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_ESTATUS_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("grupo", Grupo)
    '        strCommand.Parameters.AddWithValue("sw_todos", Sw_Todos)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Estatus_ABC(ByVal id_empresa As Integer _
    '                                     , ByVal Grupo As String _
    '                                     , ByVal permisos As DataTable _
    '                                     , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_ESTATUS_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("grupo", Grupo)
    '        strCommand.Parameters.AddWithValue("@permisos", permisos)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Perfiles_SEL(ByVal id_empresa As Integer, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERFILES_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Perfiles_ABC(ByVal id_empresa As Integer _
    '                                     , ByVal tipo1 As String _
    '                                     , ByRef id_tipo As Integer _
    '                                     , ByVal grupo As String _
    '                                     , ByVal tipo As String _
    '                                     , ByVal sw_estatus As Integer _
    '                                     , ByVal fum As DateTime _
    '                                     , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERFILES_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@tipo1", tipo1)
    '        strCommand.Parameters.AddWithValue("@id_tipo", id_tipo)
    '        strCommand.Parameters.AddWithValue("@grupo", grupo)
    '        strCommand.Parameters.AddWithValue("@tipo", tipo)
    '        strCommand.Parameters.AddWithValue("@sw_estatus", sw_estatus)
    '        strCommand.Parameters.AddWithValue("@fum", fum)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '                id_tipo = dtDatos.Rows(0).Item("id_tipo")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Permisos_Pantallas_Sel(ByVal id_empresa As Integer, ByVal id_perfil As Integer, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERMISOS_PANTALLAS_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@id_tipo_perfil", id_perfil)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Permisos_Pantallas_ABC(ByVal id_empresa As Integer _
    '                                     , ByVal id_tipo_perfil As Integer _
    '                                     , ByVal pantallas As DataTable _
    '                                     , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_PERMISOS_PANTALLAS_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@id_tipo_perfil", id_tipo_perfil)
    '        strCommand.Parameters.AddWithValue("@pantallas", pantallas)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Estructura_SEL(ByVal id_empresa As Integer, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_ESTRUCTURA_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            '_usuarios_permisos = dts.Tables(3)
    '        End If
    '        Resultado = True
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Seguridad_Estructura_ABC(ByVal id_empresa As Integer _
    '                                       , ByVal estructuras As DataTable _
    '                                       , ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_SEGURIDAD_ESTRUCTURA_ABC
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@estructuras", estructuras)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                Resultado = True
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            Else
    '                Resultado = False
    '                Excepcion = dtDatos.Rows(0).Item("msj")
    '            End If
    '        End If
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function
    'Public Function Busca_Estructura_Sel(ByVal id_empresa As Integer _
    '                                   , ByRef id_jerarquia01 As Integer _
    '                                   , ByRef id_jerarquia02 As Integer _
    '                                   , ByRef id_jerarquia03 As Integer _
    '                                   , ByRef id_jerarquia04 As Integer _
    '                                   , ByRef id_jerarquia05 As Integer _
    '                                   , ByRef id_jerarquia06 As Integer _
    '                                   , ByRef id_jerarquia07 As Integer _
    '                                   , ByRef id_jerarquia08 As Integer _
    '                                   , ByRef id_jerarquia09 As Integer _
    '                                   , ByRef id_jerarquia10 As Integer _
    '                                   , ByRef id_estructura As Integer _
    '                                   , ByRef jerarquia01 As String _
    '                                   , ByRef jerarquia02 As String _
    '                                   , ByRef jerarquia03 As String _
    '                                   , ByRef jerarquia04 As String _
    '                                   , ByRef jerarquia05 As String _
    '                                   , ByRef jerarquia06 As String _
    '                                   , ByRef jerarquia07 As String _
    '                                   , ByRef jerarquia08 As String _
    '                                   , ByRef jerarquia09 As String _
    '                                   , ByRef jerarquia10 As String _
    '                                   , ByRef estructura As String, ByRef Excepcion As String) As Boolean
    '    Dim Resultado As Boolean = False
    '    Dim cn As SqlConnection = Nothing
    '    Dim strCommand As New SqlCommand()
    '    Dim sADa As New SqlDataAdapter
    '    Dim dts As New DataSet
    '    Dim dtDatos As DataTable
    '    Try
    '        cn = Globales.oAmbientes.oControl.ObtenerConexion()
    '        strCommand.CommandText = ESQUEMA & PA_BUSCA_ESTRUCTURA_SEL
    '        strCommand.CommandType = CommandType.StoredProcedure
    '        strCommand.Connection = cn
    '
    '        strCommand.Parameters.AddWithValue("@id_empresa", id_empresa)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia01", id_jerarquia01)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia02", id_jerarquia02)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia03", id_jerarquia03)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia04", id_jerarquia04)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia05", id_jerarquia05)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia06", id_jerarquia06)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia07", id_jerarquia07)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia08", id_jerarquia08)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia09", id_jerarquia09)
    '        strCommand.Parameters.AddWithValue("@id_jerarquia10", id_jerarquia10)
    '        strCommand.Parameters.AddWithValue("@id_estructura", id_estructura)
    '
    '        cn.Open()
    '
    '        sADa.SelectCommand = strCommand
    '        sADa.Fill(dts)
    '
    '        If dts.Tables.Count > 0 Then
    '            dtDatos = dts.Tables(0)
    '            If dtDatos.Rows(0).Item("regresa") = 1 Then
    '                id_jerarquia01 = dtDatos.Rows(0).Item("id_jerarquia01")
    '                id_jerarquia02 = dtDatos.Rows(0).Item("id_jerarquia02")
    '                id_jerarquia03 = dtDatos.Rows(0).Item("id_jerarquia03")
    '                id_jerarquia04 = dtDatos.Rows(0).Item("id_jerarquia04")
    '                id_jerarquia05 = dtDatos.Rows(0).Item("id_jerarquia05")
    '                id_jerarquia06 = dtDatos.Rows(0).Item("id_jerarquia06")
    '                id_jerarquia07 = dtDatos.Rows(0).Item("id_jerarquia07")
    '                id_jerarquia08 = dtDatos.Rows(0).Item("id_jerarquia08")
    '                id_jerarquia09 = dtDatos.Rows(0).Item("id_jerarquia09")
    '                id_jerarquia10 = dtDatos.Rows(0).Item("id_jerarquia10")
    '                id_estructura = dtDatos.Rows(0).Item("id_estructura")
    '                jerarquia01 = dtDatos.Rows(0).Item("jerarquia01")
    '                jerarquia02 = dtDatos.Rows(0).Item("jerarquia02")
    '                jerarquia03 = dtDatos.Rows(0).Item("jerarquia03")
    '                jerarquia04 = dtDatos.Rows(0).Item("jerarquia04")
    '                jerarquia05 = dtDatos.Rows(0).Item("jerarquia05")
    '                jerarquia06 = dtDatos.Rows(0).Item("jerarquia06")
    '                jerarquia07 = dtDatos.Rows(0).Item("jerarquia07")
    '                jerarquia08 = dtDatos.Rows(0).Item("jerarquia08")
    '                jerarquia09 = dtDatos.Rows(0).Item("jerarquia09")
    '                jerarquia10 = dtDatos.Rows(0).Item("jerarquia10")
    '                estructura = dtDatos.Rows(0).Item("estructura")
    '                Resultado = True
    '            Else
    '                id_jerarquia01 = 0
    '                id_jerarquia02 = 0
    '                id_jerarquia03 = 0
    '                id_jerarquia04 = 0
    '                id_jerarquia05 = 0
    '                id_jerarquia06 = 0
    '                id_jerarquia07 = 0
    '                id_jerarquia08 = 0
    '                id_jerarquia09 = 0
    '                id_jerarquia10 = 0
    '                id_estructura = 0
    '                jerarquia01 = ""
    '                jerarquia02 = ""
    '                jerarquia03 = ""
    '                jerarquia04 = ""
    '                jerarquia05 = ""
    '                jerarquia06 = ""
    '                jerarquia07 = ""
    '                jerarquia08 = ""
    '                jerarquia09 = ""
    '                jerarquia10 = ""
    '                estructura = ""
    '                Resultado = False
    '            End If
    '        End If
    '        Excepcion = "NO_ERROR"
    '
    '    Catch ex As Exception
    '        Excepcion = ex.Message
    '        Resultado = False
    '    Finally
    '
    '        If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
    '            cn.Close()
    '            cn.Dispose()
    '        End If
    '        If strCommand IsNot Nothing Then
    '            strCommand.Dispose()
    '        End If
    '
    '        strCommand = Nothing
    '        cn = Nothing
    '    End Try
    '    Return Resultado
    'End Function

End Class